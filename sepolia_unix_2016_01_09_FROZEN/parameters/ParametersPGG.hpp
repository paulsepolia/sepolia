
#ifndef PARAMETERS_PGG_H
#define PARAMETERS_PGG_H

#include <string>

namespace pgg {

// PGG

const std::string PGG_LIB = "PGG";

} // pgg

#endif // PARAMETERS_PGG_H
