
#ifndef PARAMETERS_MKL_H
#define PARAMETERS_MKL_H

#include <string>

namespace pgg {

// MKL

const std::string MKL_LIB = "MKL";

#define MKL_SEPOLIA_H   // use it if MKL lib is available
#undef MKL_SEPOLIA_H  // uncomment it if MKL is not available

} // pgg

#endif // PARAMETERS_MKL_H
