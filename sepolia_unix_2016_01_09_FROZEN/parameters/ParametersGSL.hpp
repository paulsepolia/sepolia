
#ifndef PARAMETERS_GSL_H
#define PARAMETERS_GSL_H

#include <string>

namespace pgg {

// GSL

typedef int GSL_INT;
const std::string GSL_LIB = "GSL";

} // pgg

#endif // PARAMETERS_GSL_H
