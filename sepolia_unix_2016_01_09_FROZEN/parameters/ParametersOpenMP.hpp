
#ifndef PARAMETERS_OPENMP_H
#define PARAMETERS_OPENMP_H

namespace pgg {

// OpenMP

const int NT_2D = 4;
const int NT_1D = 4;

} // pgg

#endif // PARAMETERS_OPENMP_H
