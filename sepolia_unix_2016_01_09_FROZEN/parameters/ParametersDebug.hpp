
#ifndef PARAMETERS_DEBUG_H
#define PARAMETERS_DEBUG_H

namespace pgg {

#define DEBUG_ALLOC_H // use it if you want allocation type checking
#undef DEBUG_ALLOC_H // uncomment it if you do not want allocation type checking

#define DEBUG_COMPAT_H // use it if you want compatibility type checking
#undef DEBUG_COMPAT_H // uncomment it if do not want compatibility type checking

} // pgg

#endif // PARAMETERS_DEBUG_H
