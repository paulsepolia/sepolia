
#ifndef PARAMETERS_H
#define PARAMETERS_H

#include "ParametersAuxiliary.hpp"
#include "ParametersBLAS.hpp"
#include "ParametersDebug.hpp"
#include "ParametersFloatingPointTypes.hpp"
#include "ParametersGSL.hpp"
#include "ParametersLAPACK.hpp"
#include "ParametersMatrixDense.hpp"
#include "ParametersMemory.hpp"
#include "ParametersMessages.hpp"
#include "ParametersMKL.hpp"
#include "ParametersOpenMP.hpp"
#include "ParametersPGG.hpp"
#include "ParametersVectorDense.hpp"

#endif // PARAMETERS_H
