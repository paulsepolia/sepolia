
#ifndef PARAMETERS_AUXILIARY_H
#define PARAMETERS_AUXILIARY_H

#include <string>

namespace pgg {

// Auxiliary

const std::string LEN_ROW = "LengthOfRow";
const std::string LEN_COL = "LengthOfColumn";

//const std::string TO_COLS = "ToColumns";
//const std::string TO_ROWS = "ToRows";

const std::string WITH_ROWS = "WithRows";
const std::string WITH_COLS = "WithColumns";

} // pgg

#endif // PARAMETERS_AUXILIARY_H
