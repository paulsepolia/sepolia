
#ifndef PARAMETERS_MEMORY_H
#define PARAMETERS_MEMORY_H

namespace pgg {

// Memory

const bool TRUE_ALLOC = true;
const bool FALSE_ALLOC = false;

} // pgg

#endif // PARAMETERS_MEMORY_H
