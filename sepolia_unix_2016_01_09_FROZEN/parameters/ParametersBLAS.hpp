
#ifndef PARAMETERS_BLAS_H
#define PARAMETERS_BLAS_H

#include <string>

namespace pgg {

// BLAS

typedef int BLAS_INT;
const std::string BLAS_LIB = "BLAS";

} // pgg

#endif // PARAMETERS_BLAS_H
