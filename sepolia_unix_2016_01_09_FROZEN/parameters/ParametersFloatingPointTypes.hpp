
#ifndef PARAMETERS_FLOATING_POINT_TYPES_H
#define PARAMETERS_FLOATING_POINT_TYPES_H

#include <complex>

namespace pgg {

// Floating point types

// double

const double ZERO_DBL = static_cast<double>(0.0);
const double ONE_DBL = static_cast<double>(1.0);
const long double ZERO_DBL_MAX = static_cast<long double>(0.0L);
const long double ONE_DBL_MAX = static_cast<long double>(1.0L);

// complex

const std::complex<double> ZERO_CMPLX = {0.0, 0.0};
const std::complex<double> ONE_CMPLX = {1.0, 0.0};
const std::complex<long double> ZERO_CMPLX_MAX = {0.0L, 0.0L};
const std::complex<long double> ONE_CMPLX_MAX = {1.0L, 0.0L};

} // pgg

#endif // PARAMETERS_FLOATING_POINT_TYPES_H
