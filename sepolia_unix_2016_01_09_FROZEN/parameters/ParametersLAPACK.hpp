
#ifndef PARAMETERS_LAPACK_H
#define PARAMETERS_LAPACK_H

#include <string>
#include <complex>

namespace pgg {

// LAPACK

typedef int LAPACK_INT;
typedef bool LAPACK_LOGICAL;
typedef std::complex<double> LAPACK_COMPLEX_DOUBLE;
const std::string LAPACK_LIB = "LAPACK";

// real symmetric diagonalizers

const std::string DIAG_DSYEVD = "DSYEVD";
const std::string DIAG_DSYEVR = "DSYEVR";
const std::string DIAG_DSYEVX = "DSYEVX";
const std::string DIAG_DSYEV = "DSYEV";

// complex hermitian diagonalizers

const std::string DIAG_ZHEEVD = "ZHEEVD";
const std::string DIAG_ZHEEVR = "ZHEEVR";
const std::string DIAG_ZHEEVX = "ZHEEVX";
const std::string DIAG_ZHEEV = "ZHEEV";

} // pgg

#endif // PARAMETERS_LAPACK_H
