//=============//
// test driver //
//=============//

// sepolia

#include "../../../head/sepolia.h"

using pgg::sep::MatrixDense;
using pgg::sep::VectorDense;
using std::cos;
using std::sin;

// Google Test

#include "gtest/gtest.h"

//==========//
// test # 1 //
//==========//

TEST(MatrixTest1, op_equal)
{
     MatrixDense<double> matA;
     MatrixDense<double> matB;
     MatrixDense<double> matC;

     const int ELEMS = 1000;

     matA.Allocate(ELEMS);
     matB.Allocate(ELEMS);
     matC.Allocate(ELEMS);

     matA = 10.0;
     matB = matA;
     matC = matB;

     // # 1

     EXPECT_TRUE(matA == matB);
     EXPECT_TRUE(matB == matC);
}

//==========//
// test # 2 //
//==========//

TEST(MatrixTest2, op_equal)
{
     MatrixDense<double> matA;
     MatrixDense<double> matB;
     MatrixDense<double> matC;

     const int ELEMS = 1000;

     matA.Allocate(ELEMS);
     matB.Allocate(ELEMS);
     matC.Allocate(ELEMS);

     matA = 11.0;
     matB = 12.0;
     matC = 13.0;

     // # 1

     EXPECT_TRUE(matA == 11.0);
     EXPECT_TRUE(matB == 12.0);
     EXPECT_TRUE(matC == 13.0);

}

//==========//
// test # 3 //
//==========//

TEST(MatrixTest3, op_plus)
{
     MatrixDense<double> matA;
     MatrixDense<double> matB;
     MatrixDense<double> matC;

     const int ELEMS = 1000;

     matA.Allocate(ELEMS);
     matB.Allocate(ELEMS);
     matC.Allocate(ELEMS);

     matA = 11.0;
     matB = 12.0;
     matC = 13.0;

     // # 1

     EXPECT_TRUE(matA+matB == 11.0+12.0);
     EXPECT_TRUE(matB+matA == 12.0+11.0);
     EXPECT_TRUE(matC+matA+matB == 13.0+12.0+11.0);

}

//==========//
// test # 4 //
//==========//

TEST(MatrixTest4, op_times)
{
     MatrixDense<double> matA;
     MatrixDense<double> matB;
     MatrixDense<double> matC;

     const int ELEMS = 1000;

     matA.Allocate(ELEMS);
     matB.Allocate(ELEMS);
     matC.Allocate(ELEMS);

     matA = 11.0;
     matB = 12.0;
     matC = 13.0;

     // # 1

     EXPECT_TRUE(matA*matB == 11.0*12.0);
     EXPECT_TRUE(matB*matA == 12.0*11.0);
     EXPECT_TRUE(matC*matA*matB == 13.0*12.0*11.0);

}

//==========//
// test # 5 //
//==========//

TEST(MatrixTest5, op_divide)
{
     MatrixDense<double> matA;
     MatrixDense<double> matB;
     MatrixDense<double> matC;

     const int ELEMS = 1000;

     matA.Allocate(ELEMS);
     matB.Allocate(ELEMS);
     matC.Allocate(ELEMS);

     matA = 11.0;
     matB = 12.0;
     matC = 13.0;

     // # 1

     EXPECT_TRUE(matA/matB == 11.0/12.0);
     EXPECT_TRUE(matB/matA == 12.0/11.0);
     EXPECT_TRUE(matC/matA/matB == 13.0/11.0/12.0);

}

//==========//
// test # 6 //
//==========//

TEST(MatrixTest6, op_subtract)
{
     MatrixDense<double> matA;
     MatrixDense<double> matB;
     MatrixDense<double> matC;

     const int ELEMS = 1000;

     matA.Allocate(ELEMS);
     matB.Allocate(ELEMS);
     matC.Allocate(ELEMS);

     matA = 11.0;
     matB = 12.0;
     matC = 13.0;

     // # 1

     EXPECT_TRUE(matA-matB == 11.0-12.0);
     EXPECT_TRUE(matB-matA == 12.0-11.0);
     EXPECT_TRUE(matC-matA-matB == 13.0-11.0-12.0);

}

//==========//
// test # 7 //
//==========//

TEST(MatrixTest7, op_all)
{
     MatrixDense<double> matA;
     MatrixDense<double> matB;
     MatrixDense<double> matC;

     const int ELEMS = 1000;

     matA.Allocate(ELEMS);
     matB.Allocate(ELEMS);
     matC.Allocate(ELEMS);

     matA = 11.0;
     matB = 12.0;
     matC = 13.0;

     // # 1

     EXPECT_TRUE(matA-matB+matA/matC*matB == 11.0-12.0+11.0/13.0*12.0);
}

//==========//
// test # 8 //
//==========//

TEST(MatrixTest8, op_all)
{
     MatrixDense<double> matA;
     MatrixDense<double> matB;
     MatrixDense<double> matC;

     const int ELEMS = 1000;

     matA.Allocate(ELEMS);
     matB.Allocate(ELEMS);
     matC.Allocate(ELEMS);

     matA = 11.0;
     matB = 12.0;
     matC = 13.0;

     matA = matA/matA/matA/matA/matA;
     matB = matB/matB/matB/matB/matB;
     matC = matC/matC/matC/matC/matC;

     // # 1

     EXPECT_TRUE(matA == 11.0/11.0/11.0/11.0/11.0);
     EXPECT_TRUE(matB == 12.0/12.0/12.0/12.0/12.0);
     EXPECT_TRUE(matC == 13.0/13.0/13.0/13.0/13.0);
}

//==========//
// test # 9 //
//==========//

TEST(MatrixTest9, op_various)
{
     MatrixDense<double> matA;
     MatrixDense<double> matB;
     MatrixDense<double> matC;

     const int ELEMS = 1000;

     matA.Allocate(ELEMS);
     matB.Allocate(ELEMS);
     matC.Allocate(ELEMS);

     matA = 11.0;
     matB = 12.0;
     matC = 13.0;

     matA = matA/matA/matA/matA/matA;
     matB = matB/matB/matB/matB/matB;
     matC = matC/matC/matC/matC/matC;

     // # 1

     EXPECT_FALSE(matA != 11.0/11.0/11.0/11.0/11.0);
     EXPECT_FALSE(matB != 12.0/12.0/12.0/12.0/12.0);
     EXPECT_FALSE(matC != 13.0/13.0/13.0/13.0/13.0);
}

//===========//
// test # 10 //
//===========//

TEST(MatrixTest10, op_various)
{
     MatrixDense<double> matA;
     MatrixDense<double> matB;
     MatrixDense<double> matC;

     const int ELEMS = 1000;

     matA.Allocate(ELEMS);
     matB.Allocate(ELEMS);
     matC.Allocate(ELEMS);

     matA = 11.0;
     matB = 12.0;
     matC = 13.0;

     matA += matA;
     matB += matB;
     matC += matC;

     // # 1

     EXPECT_TRUE(matA == 11.0+11.0);
     EXPECT_TRUE(matB == 12.0+12.0);
     EXPECT_TRUE(matC == 13.0+13.0);

     EXPECT_FALSE(matA != 11.0+11.0);
     EXPECT_FALSE(matB != 12.0+12.0);
     EXPECT_FALSE(matC != 13.0+13.0);
}

//===========//
// test # 11 //
//===========//

TEST(MatrixTest11, op_various)
{
     MatrixDense<double> matA;
     MatrixDense<double> matB;
     MatrixDense<double> matC;

     const int ELEMS = 1000;

     matA.Allocate(ELEMS);
     matB.Allocate(ELEMS);
     matC.Allocate(ELEMS);

     matA = 11.0;
     matB = 12.0;
     matC = 13.0;

     matA /= matA;
     matB /= matB;
     matC /= matC;

     // # 1

     EXPECT_TRUE(matA == 11.0/11.0);
     EXPECT_TRUE(matB == 12.0/12.0);
     EXPECT_TRUE(matC == 13.0/13.0);

     EXPECT_FALSE(matA != 11.0/11.0);
     EXPECT_FALSE(matB != 12.0/12.0);
     EXPECT_FALSE(matC != 13.0/13.0);
}

//===========//
// test # 12 //
//===========//

TEST(MatrixTest12, op_various)
{
     MatrixDense<double> matA;
     MatrixDense<double> matB;
     MatrixDense<double> matC;

     const int ELEMS = 1000;

     matA.Allocate(ELEMS);
     matB.Allocate(ELEMS);
     matC.Allocate(ELEMS);

     matA = 11.0;
     matB = 12.0;
     matC = 13.0;

     matA *= matA;
     matB *= matB;
     matC *= matC;

     // # 1

     EXPECT_TRUE(matA == 11.0*11.0);
     EXPECT_TRUE(matB == 12.0*12.0);
     EXPECT_TRUE(matC == 13.0*13.0);

     EXPECT_FALSE(matA != 11.0*11.0);
     EXPECT_FALSE(matB != 12.0*12.0);
     EXPECT_FALSE(matC != 13.0*13.0);
}

//===========//
// test # 13 //
//===========//

TEST(MatrixTest13, op_various)
{
     MatrixDense<double> matA;
     MatrixDense<double> matB;
     MatrixDense<double> matC;

     const int ELEMS = 1000;

     matA.Allocate(ELEMS);
     matB.Allocate(ELEMS);
     matC.Allocate(ELEMS);

     matA = 11.0;
     matB = 12.0;
     matC = 13.0;

     matA -= matA;
     matB -= matB;
     matC -= matC;

     // # 1

     EXPECT_TRUE(matA == 11.0-11.0);
     EXPECT_TRUE(matB == 12.0-12.0);
     EXPECT_TRUE(matC == 13.0-13.0);

     EXPECT_FALSE(matA != 11.0-11.0);
     EXPECT_FALSE(matB != 12.0-12.0);
     EXPECT_FALSE(matC != 13.0-13.0);
}

//===========//
// test # 14 //
//===========//

TEST(MatrixTest14, op_various)
{
     MatrixDense<double> matA;
     MatrixDense<double> matB;
     MatrixDense<double> matC;

     const int ELEMS = 1000;

     matA.Allocate(ELEMS);
     matB.Allocate(ELEMS);
     matC.Allocate(ELEMS);

     matA = 11.0;
     matB = 12.0;
     matC = 13.0;

     matA++;
     ++matB;
     matC++;

     // # 1

     EXPECT_TRUE(matA == 12.0);
     EXPECT_TRUE(matB == 13.0);
     EXPECT_TRUE(matC == 14.0);

     EXPECT_FALSE(matA != 12.0);
     EXPECT_FALSE(matB != 13.0);
     EXPECT_FALSE(matC != 14.0);

     // # 2

     matA--;
     --matB;
     matC--;

     EXPECT_TRUE(matA == 11.0);
     EXPECT_TRUE(matB == 12.0);
     EXPECT_TRUE(matC == 13.0);

     EXPECT_FALSE(matA != 11.0);
     EXPECT_FALSE(matB != 12.0);
     EXPECT_FALSE(matC != 13.0);

     // # 3

     (matA--);
     (--matB);
     (matC--);

     EXPECT_TRUE(matA == 10.0);
     EXPECT_TRUE(matB == 11.0);
     EXPECT_TRUE(matC == 12.0);

     EXPECT_FALSE(matA != 10.0);
     EXPECT_FALSE(matB != 11.0);
     EXPECT_FALSE(matC != 12.0);
}

//===========//
// test # 15 //
//===========//

TEST(MatrixTest15, op_various)
{
     MatrixDense<double> matA;

     const int ELEMS = 1000;

     matA.Allocate(ELEMS);

     // # 1

     matA.Range(10);

     for (int i = 0; i != ELEMS*ELEMS; ++i) {
          EXPECT_EQ(matA(i), 10+static_cast<double>(i));
     }

     // # 2

     matA.Range(21.1);

     for (int i = 0; i != ELEMS*ELEMS; ++i) {
          EXPECT_EQ(matA(i), 21.1+static_cast<double>(i));
     }

     // # 3

     EXPECT_EQ(matA[0], 21.1);
     EXPECT_EQ(matA[1], 21.1+ELEMS);
     EXPECT_EQ(matA[2], 21.1+2*ELEMS);
     EXPECT_EQ(matA[3], 21.1+3*ELEMS);

     int j = 0;

     for(int i = 0; i != ELEMS*ELEMS; ++i) {
          EXPECT_EQ(matA[i], 21.1+j*ELEMS+i/ELEMS);
          j = j + 1;
          if (j%ELEMS == 0) {
               j = 0;
          }
     }

}

//===========//
// test # 16 //
//===========//

TEST(MatrixTest16, SlowWayA)
{
     // local parameters

     const auto DIM = 1 *static_cast<pgg::MAI_MAX>(pow(10.0, 2.0));
     const auto K_MAX_LOC = 10;
     const auto ONE_DOUBLE = 1.0;
     const auto TWO_DOUBLE = 2.0;
     const auto THREE_DOUBLE = 3.0;

     MatrixDense<double> matA;
     MatrixDense<double> matB;
     MatrixDense<double> matC;

     // allocate heap space

     matA.Allocate(DIM);
     matB.Allocate(DIM);
     matC.Allocate(DIM);

     // build matrices

     matA = ONE_DOUBLE;
     matB = TWO_DOUBLE;
     matC = THREE_DOUBLE;

     // test equality

     EXPECT_TRUE(matA == ONE_DOUBLE);
     EXPECT_TRUE(matB == TWO_DOUBLE);
     EXPECT_TRUE(matC == THREE_DOUBLE);

     // add matrices

     for (int i = 0; i != K_MAX_LOC; ++i) {
          matC = matA + matB;
     }

     EXPECT_TRUE(matA == ONE_DOUBLE);
     EXPECT_TRUE(matB == TWO_DOUBLE);
     EXPECT_TRUE(matC == THREE_DOUBLE);

     // subtract matrices

     for (int i = 0; i != K_MAX_LOC; ++i) {
          matC = matA - matB;
     }

     EXPECT_TRUE(matA == ONE_DOUBLE);
     EXPECT_TRUE(matB == TWO_DOUBLE);
     EXPECT_TRUE(matC == -ONE_DOUBLE);

     // multiply matrices

     for (int i = 0; i != K_MAX_LOC; ++i) {
          matC = matA * matB;
     }

     EXPECT_TRUE(matA == ONE_DOUBLE);
     EXPECT_TRUE(matB == TWO_DOUBLE);
     EXPECT_TRUE(matC == TWO_DOUBLE);

     // divide matrices

     for (int i = 0; i != K_MAX_LOC; ++i) {
          matC = matA / matB;
     }

     EXPECT_TRUE(matA == ONE_DOUBLE);
     EXPECT_TRUE(matB == TWO_DOUBLE);
     EXPECT_TRUE(matC == (ONE_DOUBLE/TWO_DOUBLE));

     // swap matrices

     for (int i = 0; i != K_MAX_LOC; ++i) {

          matA = ONE_DOUBLE;
          matB = TWO_DOUBLE;
          matC = THREE_DOUBLE;

          matA = matB;
          matB = matC;
          matC = ONE_DOUBLE;
     }

     EXPECT_TRUE(matA == TWO_DOUBLE);
     EXPECT_TRUE(matB == THREE_DOUBLE);
     EXPECT_TRUE(matC == ONE_DOUBLE);

     // advance matrices

     matA = ONE_DOUBLE;
     matB = TWO_DOUBLE;
     matC = THREE_DOUBLE;

     for (int i = 0; i != K_MAX_LOC; ++i) {
          matA = matA + ONE_DOUBLE;
          matB = matB + ONE_DOUBLE;
          matC = matC + ONE_DOUBLE;
     }

     EXPECT_TRUE(matA == (ONE_DOUBLE+ONE_DOUBLE*K_MAX_LOC));
     EXPECT_TRUE(matB == (TWO_DOUBLE+ONE_DOUBLE*K_MAX_LOC));
     EXPECT_TRUE(matC == (THREE_DOUBLE+ONE_DOUBLE*K_MAX_LOC));

     // advance matrices

     matA = ONE_DOUBLE;
     matB = TWO_DOUBLE;
     matC = THREE_DOUBLE;

     for (int i = 0; i != K_MAX_LOC; ++i) {
          matA = matA - ONE_DOUBLE;
          matB = matB - ONE_DOUBLE;
          matC = matC - ONE_DOUBLE;
     }

     EXPECT_TRUE(matA == (ONE_DOUBLE-ONE_DOUBLE*K_MAX_LOC));
     EXPECT_TRUE(matB == (TWO_DOUBLE-ONE_DOUBLE*K_MAX_LOC));
     EXPECT_TRUE(matC == (THREE_DOUBLE-ONE_DOUBLE*K_MAX_LOC));

     // advance matrices

     matA = ONE_DOUBLE;
     matB = TWO_DOUBLE;
     matC = THREE_DOUBLE;

     for (int i = 0; i != K_MAX_LOC; ++i) {
          matA = matA * ONE_DOUBLE;
          matB = matB * ONE_DOUBLE;
          matC = matC * ONE_DOUBLE;
     }

     EXPECT_TRUE(matA == ONE_DOUBLE);
     EXPECT_TRUE(matB == TWO_DOUBLE);
     EXPECT_TRUE(matC == THREE_DOUBLE);

     // advance matrices

     matA = ONE_DOUBLE;
     matB = TWO_DOUBLE;
     matC = THREE_DOUBLE;

     for (int i = 0; i != K_MAX_LOC; ++i) {
          matA = matA / ONE_DOUBLE;
          matB = matB / ONE_DOUBLE;
          matC = matC / ONE_DOUBLE;
     }

     EXPECT_TRUE(matA == ONE_DOUBLE);
     EXPECT_TRUE(matB == TWO_DOUBLE);
     EXPECT_TRUE(matC ==THREE_DOUBLE);

     // advance matrices

     matA = ONE_DOUBLE;
     matB = TWO_DOUBLE;
     matC = THREE_DOUBLE;

     for (int i = 0; i != K_MAX_LOC; ++i) {
          matA = matA + matA;
          matB = matB + matB;
          matC = matC + matC;
     }

     EXPECT_TRUE(matA == 1.0*pow(TWO_DOUBLE, K_MAX_LOC));
     EXPECT_TRUE(matB == 2.0*pow(TWO_DOUBLE, K_MAX_LOC));
     EXPECT_TRUE(matC == 3.0*pow(TWO_DOUBLE, K_MAX_LOC));

     // advance matrices

     matA = ONE_DOUBLE;
     matB = TWO_DOUBLE;
     matC = THREE_DOUBLE;

     for (int i = 0; i != K_MAX_LOC; ++i) {
          matA = matA - matA;
          matB = matB - matB;
          matC = matC - matC;
     }

     EXPECT_TRUE(matA == pgg::ZERO_DBL);
     EXPECT_TRUE(matB == pgg::ZERO_DBL);
     EXPECT_TRUE(matC == pgg::ZERO_DBL);

     // advance matrices

     matA = ONE_DOUBLE;
     matB = TWO_DOUBLE;
     matC = THREE_DOUBLE;

     for (int i = 0; i != K_MAX_LOC; ++i) {
          matA = matA / matA;
          matB = matB / matB;
          matC = matC / matC;
     }

     EXPECT_TRUE(matA == pgg::ONE_DBL);
     EXPECT_TRUE(matB == pgg::ONE_DBL);
     EXPECT_TRUE(matC == pgg::ONE_DBL);

     // advance matrices

     matA = ONE_DOUBLE;
     matB = TWO_DOUBLE;
     matC = THREE_DOUBLE;

     for (int i = 0; i != K_MAX_LOC; ++i) {
          matA = matA * matA;
          matB = matB * matB;
          matB = matB / TWO_DOUBLE;
          matC = matC * matC;
          matC = matC / THREE_DOUBLE;
     }

     EXPECT_TRUE(matA == ONE_DOUBLE);
     EXPECT_TRUE(matB == TWO_DOUBLE);
     EXPECT_TRUE(matC == THREE_DOUBLE);

     // deallocate heap space

     matA.Deallocate();
     matB.Deallocate();
     matC.Deallocate();

}

//===========//
// test # 17 //
//===========//

TEST(MatrixTest17, GreaterAll)
{
     MatrixDense<double> matA;
     MatrixDense<double> matB;

     const int ELEM = 100;

     matA.Allocate(ELEM);
     matB.Allocate(ELEM);

     matA.Range(1.0, 2.0);
     matB.Range(2.0, 4.0);

     EXPECT_TRUE(matB > matA);
     EXPECT_TRUE(matB >= matA);
}

//===========//
// test # 18 //
//===========//

TEST(MatrixTest18, LessAll)
{
     MatrixDense<double> matA;
     MatrixDense<double> matB;

     const int ELEM = 100;

     matA.Allocate(ELEM);
     matB.Allocate(ELEM);

     matB.Range(1.0, 2.0);
     matA.Range(2.0, 4.0);

     EXPECT_TRUE(matB < matA);
     EXPECT_TRUE(matB <= matA);
}

//===========//
// test # 19 //
//===========//

TEST(MatrixTest19, GreaterAll)
{
     MatrixDense<double> matA;

     const int ELEM = 100;

     matA.Allocate(ELEM);

     matA.Range(1.0, 2.0);

     EXPECT_TRUE(matA >= -1.0);
     EXPECT_TRUE(matA >= 1.0);
}

//===========//
// test # 20 //
//===========//

TEST(MatrixTest20, LessAllA)
{
     MatrixDense<double> matA;

     const int ELEM = 100;

     matA.Allocate(ELEM);

     matA.Range(3.0, 4.0);

     EXPECT_TRUE(matA < 4.001);
     EXPECT_TRUE(matA <= 4.0);

     matA.Set(10.0);

     EXPECT_TRUE(matA <= 10.0);
     EXPECT_TRUE(matA >= 10.0);
     EXPECT_TRUE(matA == 10.0);
}

//===========//
// test # 21 //
//===========//

TEST(MatrixTest21, LessAllB)
{
     MatrixDense<double> matA;
     MatrixDense<double> matB;

     const int ELEM = 100;

     matA.Allocate(ELEM);
     matB.Allocate(ELEM);

     matA.Range(3.0, 4.0);
     matB.Range(3.0, 5.0);

     EXPECT_TRUE(matA <= matB);
     EXPECT_FALSE(matA < matB);

     EXPECT_TRUE(matA >= 3.0);
     EXPECT_FALSE(matA > 3.0);
     EXPECT_FALSE(matA < 3.0);
     EXPECT_FALSE(matA <= 3.0);

     EXPECT_TRUE(matA != matB);
     EXPECT_FALSE(matA == matB);
}

//===========//
// test # 22 //
//===========//

TEST(MatrixTest22, LessAllC)
{
     MatrixDense<std::complex<double>> matA;
     MatrixDense<std::complex<double>> matB;

     const int ELEM = 100;

     matA.Allocate(ELEM);
     matB.Allocate(ELEM);

     matA = {3.0, 4.0};
     matB = {3.0, 5.0};

     EXPECT_TRUE(matA <= matB);
     EXPECT_FALSE(matA < matB);

     EXPECT_TRUE(matA >= 3.0);
     EXPECT_FALSE(matA > 3.0);
     EXPECT_FALSE(matA < 3.0);
     EXPECT_FALSE(matA <= 3.0);

     EXPECT_TRUE(matA != matB);
     EXPECT_FALSE(matA == matB);
}

//===========//
// test # 23 //
//===========//

TEST(MatrixTest23, LessAllD)
{
     MatrixDense<std::complex<long double>> matA;
     MatrixDense<std::complex<long double>> matB;

     const int ELEM = 100;

     matA.Allocate(ELEM);
     matB.Allocate(ELEM);

     matA = {3.0L, 4.0L};
     matB = {3.0L, 5.0L};

     EXPECT_TRUE(matA <= matB);
     EXPECT_FALSE(matA < matB);

     EXPECT_TRUE(matA >= 3.0L);
     EXPECT_FALSE(matA > 3.0L);
     EXPECT_FALSE(matA < 3.0L);
     EXPECT_FALSE(matA <= 3.0L);

     EXPECT_TRUE(matA != matB);
     EXPECT_FALSE(matA == matB);
}

// END
