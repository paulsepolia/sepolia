//=============//
// test driver //
//=============//

// sepolia

#include "../../../head/sepolia.h"

using pgg::sep::MatrixDense;

// C++

#include <cmath>

using std::cos;
using std::sin;

// Google Test

#include "gtest/gtest.h"

//==========//
// test # 1 //
//==========//

TEST(MatrixTest1, Memory)
{
     // This test is named "Memory",
     // and belongs to the "MatrixTest1"

     MatrixDense<double> matA;

     // test
     // AllocatedQ()
     // DeallocatedQ()

     EXPECT_EQ(matA.AllocatedQ(), false);
     EXPECT_EQ(matA.DeallocatedQ(), true);
     EXPECT_EQ(matA.DeallocatedQ(), !matA.AllocatedQ());

     // test
     // AllocatedQ()
     // DeallocatedQ()

     matA.Allocate(10, 10);

     EXPECT_EQ(matA.AllocatedQ(), true);
     EXPECT_EQ(matA.DeallocatedQ(), false);
     EXPECT_EQ(matA.DeallocatedQ(), !matA.AllocatedQ());

     // test
     // AllocatedQ()
     // DeallocatedQ()

     matA.Deallocate();

     EXPECT_EQ(matA.AllocatedQ(), false);
     EXPECT_EQ(matA.DeallocatedQ(), true);
     EXPECT_EQ(matA.DeallocatedQ(), !matA.AllocatedQ());

     // test
     // AllocatedQ()
     // DeallocatedQ()

     matA.Allocate(10, 11);

     EXPECT_EQ(matA.AllocatedQ(), true);
     EXPECT_EQ(matA.DeallocatedQ(), false);
     EXPECT_EQ(matA.DeallocatedQ(), !matA.AllocatedQ());

     // test
     // AllocatedQ()
     // DeallocatedQ()

     matA.Deallocate();

     EXPECT_EQ(matA.AllocatedQ(), false);
     EXPECT_EQ(matA.DeallocatedQ(), true);
     EXPECT_EQ(matA.DeallocatedQ(), !matA.AllocatedQ());

     // test
     // AllocatedQ()
     // DeallocatedQ()

     matA.Allocate(10);

     EXPECT_EQ(matA.AllocatedQ(), true);
     EXPECT_EQ(matA.DeallocatedQ(), false);
     EXPECT_EQ(matA.DeallocatedQ(), !matA.AllocatedQ());

     // test
     // AllocatedQ()
     // DeallocatedQ()

     matA.Deallocate();

     EXPECT_EQ(matA.AllocatedQ(), false);
     EXPECT_EQ(matA.DeallocatedQ(), true);
     EXPECT_EQ(matA.DeallocatedQ(), !matA.AllocatedQ());

     // end test
}

//==========//
// test # 2 //
//==========//

TEST(MatrixTest2, Memory)
{
     // This test is named "Memory",
     // and belongs to the "MatrixTest2"

     MatrixDense<double> matA;

     matA.Allocate(10);

     MatrixDense<double> matB(matA);

     // test
     // AllocatedQ()
     // DeallocatedQ()

     EXPECT_EQ(matB.AllocatedQ(), true);
     EXPECT_EQ(matB.DeallocatedQ(), false);
     EXPECT_EQ(matB.DeallocatedQ(), !matB.AllocatedQ());

     // test
     // AllocatedQ()
     // DeallocatedQ()

     matB.Deallocate();
     matB.Allocate(10, 10);

     EXPECT_EQ(matB.AllocatedQ(), true);
     EXPECT_EQ(matB.DeallocatedQ(), false);
     EXPECT_EQ(matB.DeallocatedQ(), !matB.AllocatedQ());

     // test
     // AllocatedQ()
     // DeallocatedQ()

     matB.Deallocate();

     EXPECT_EQ(matB.AllocatedQ(), false);
     EXPECT_EQ(matB.DeallocatedQ(), true);
     EXPECT_EQ(matB.DeallocatedQ(), !matB.AllocatedQ());

     // test
     // AllocatedQ()
     // DeallocatedQ()

     matB.Allocate(10, 11);

     EXPECT_EQ(matB.AllocatedQ(), true);
     EXPECT_EQ(matB.DeallocatedQ(), false);
     EXPECT_EQ(matB.DeallocatedQ(), !matB.AllocatedQ());

     // test
     // AllocatedQ()
     // DeallocatedQ()

     matB.Deallocate();

     EXPECT_EQ(matB.AllocatedQ(), false);
     EXPECT_EQ(matB.DeallocatedQ(), true);
     EXPECT_EQ(matB.DeallocatedQ(), !matB.AllocatedQ());

     // test
     // AllocatedQ()
     // DeallocatedQ()

     matB.Allocate(10);
     matB.Allocate(11);
     matB.Allocate(12);

     EXPECT_EQ(matB.AllocatedQ(), true);
     EXPECT_EQ(matB.DeallocatedQ(), false);
     EXPECT_EQ(matB.DeallocatedQ(), !matB.AllocatedQ());

     // test
     // AllocatedQ()
     // DeallocatedQ()

     matB.Deallocate();
     matB.Deallocate();
     matB.Deallocate();

     EXPECT_EQ(matB.AllocatedQ(), false);
     EXPECT_EQ(matB.DeallocatedQ(), true);
     EXPECT_EQ(matB.DeallocatedQ(), !matB.AllocatedQ());

     // end test
}

//==========//
// test # 3 //
//==========//

TEST(MatrixTest3, Memory)
{
     // This test is named "Memory",
     // and belongs to the "MatrixTest3"

     MatrixDense<double> matA;

     matA.Allocate(100);

     MatrixDense<double> matB(matA);

     // test
     // AllocatedQ()
     // DeallocatedQ()

     EXPECT_TRUE(matB.AllocatedQ());
     EXPECT_FALSE(matB.DeallocatedQ());

     // test
     // AllocatedQ()
     // DeallocatedQ()

     matB.Deallocate();

     EXPECT_FALSE(matB.AllocatedQ());
     EXPECT_TRUE(matB.DeallocatedQ());

     // test
     // AllocatedQ()
     // DeallocatedQ()

     matB.Allocate(1, 100);
     matB.Allocate(1, 200);
     matB.Allocate(200, 100);

     EXPECT_TRUE(matB.AllocatedQ());
     EXPECT_FALSE(matB.DeallocatedQ());

     // end test
}

//==========//
// test # 4 //
//==========//

TEST(MatrixTest4, Memory)
{
     // This test is named "Memory",
     // and belongs to the "MatrixTest4"

     MatrixDense<double> matA;
     MatrixDense<double> matB;

     // local scope

     {
          MatrixDense<double> matA;

          matA.Allocate(100);
          matB.Allocate(8,9);

          // test
          // AllocatedQ()
          // DeallocatedQ()

          EXPECT_TRUE(matA.AllocatedQ());
          EXPECT_FALSE(matA.DeallocatedQ());

          EXPECT_TRUE(matB.AllocatedQ());
          EXPECT_FALSE(matB.DeallocatedQ());
     }

     // test
     // AllocatedQ()
     // DeallocatedQ()

     EXPECT_FALSE(matA.AllocatedQ());
     EXPECT_TRUE(matA.DeallocatedQ());

     EXPECT_TRUE(matB.AllocatedQ());
     EXPECT_FALSE(matB.DeallocatedQ());

     matB.Deallocate();

     EXPECT_FALSE(matB.AllocatedQ());
     EXPECT_TRUE(matB.DeallocatedQ());

     // end test
}

// END
