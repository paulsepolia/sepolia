//=============//
// test driver //
//=============//

// sepolia

#include "../../../head/sepolia.h"

using pgg::sep::MatrixDense;
using pgg::sep::VectorDense;
using pgg::functions::Cos;
using pgg::functions::Sin;
using std::cos;
using std::sin;

// Google Test

#include "gtest/gtest.h"

//==========//
// test # 1 //
//==========//

TEST(MatrixTest1, Divide)
{
     // This test is named "Divide",
     // and belongs to the "MatrixTest1"

     MatrixDense<double> matA;
     const int ELEMS = 1000;

     matA.Allocate(ELEMS);

     // # 1

     const double ELEM_A = 10.0;

     matA = ELEM_A;

     EXPECT_EQ(matA, ELEM_A);

     // # 2

     const double ELEM_B = 11.0;

     matA.Divide(ELEM_B);

     EXPECT_EQ(matA, ELEM_A/ELEM_B);

     // # 3

     MatrixDense<double> matB;

     matB.Allocate(ELEMS);

     matB = matA;

     EXPECT_EQ(matA, matB);

     // # 4

     matA.Divide(matB);

     EXPECT_EQ(matA, 1.0);

     // # 5

     matB.Divide(matB);

     EXPECT_EQ(matB, 1.0);

     // # 6

     const double ELEM_C = 0.123456;
     const double ELEM_D = 1.234567;

     matA = ELEM_C;
     matB = ELEM_D;

     EXPECT_EQ(matA/matB, ELEM_C/ELEM_D);

     // # 7

     EXPECT_EQ(matA/matB + matB/matA, ELEM_C/ELEM_D + ELEM_D/ELEM_C);

     // # 8

     matA = Sin(ELEM_C);
     matB = Cos(ELEM_D);

     matA.Divide(matA, matB);

     EXPECT_EQ(matA, sin(ELEM_C)/cos(ELEM_D));

     // # 9

     for (int i = 0; i != ELEMS; ++i) {
          matA.SetElement(i, i, Cos(static_cast<double>(i)));
          matB.SetElement(i, i, Sin(static_cast<double>(i))+0.1);
     }

     matA = matA / matB;

     const double THRES_A = pow(10.0, -11.0);

     for (int i = 0; i != ELEMS; ++i) {
          EXPECT_TRUE((matA(i,i) -  Cos(static_cast<double>(i)) /
                       (Sin(static_cast<double>(i))+0.1)) <
                      THRES_A);
     }

     // # 10

     const double  TINY_A = pow(10.0, -10.0);

     for (int i = 0; i != ELEMS; ++i) {
          matA.SetElement(i, i, Cos(static_cast<double>(i)));
          matB.SetElement(i, i, Sin(static_cast<double>(i))+ TINY_A);
     }

     matA = matA / matB;

     const double THRES_B = pow(10.0, -10.0);

     for (int i = 0; i != ELEMS; ++i) {
          EXPECT_TRUE((matA(i,i) -  Cos(static_cast<double>(i)) /
                       (Sin(static_cast<double>(i))+ TINY_A)) <
                      THRES_B);
     }
}

//==========//
// test # 2 //
//==========//

TEST(MatrixTest2, Plus)
{
     // This test is named "Plus",
     // and belongs to the "MatrixTest2"

     MatrixDense<double> matA;
     const int ELEMS = 1000;

     matA.Allocate(ELEMS);

     // # 1

     const double ELEM_A = 10.0;

     matA = ELEM_A;

     EXPECT_EQ(matA, ELEM_A);

     // # 2

     const double ELEM_B = 11.0;

     matA.Plus(ELEM_B);

     EXPECT_EQ(matA, ELEM_A+ELEM_B);

     // # 3

     MatrixDense<double> matB;

     matB.Allocate(ELEMS);

     matB = matA;

     EXPECT_EQ(matA, matB);

     // # 4

     matA.Plus(matB);

     EXPECT_EQ(matA, 2*(ELEM_A+ELEM_B));

     // # 5

     matB.Plus(matB);

     EXPECT_EQ(matB, matA);

     // # 6

     const double ELEM_C = 0.123456;
     const double ELEM_D = 1.234567;

     matA = ELEM_C;
     matB = ELEM_D;

     EXPECT_EQ(matA+matB, ELEM_C+ELEM_D);

     // # 7

     EXPECT_EQ(matA+matB + matB+matA, ELEM_C+ELEM_D + ELEM_D+ELEM_C);

     // # 8

     matA = Sin(ELEM_C);
     matB = Cos(ELEM_D);

     matA.Plus(matA, matB);

     EXPECT_EQ(matA, sin(ELEM_C)+cos(ELEM_D));

     // # 9

     for (int i = 0; i != ELEMS; ++i) {
          matA.SetElement(i,i, Cos(static_cast<double>(i)));
          matB.SetElement(i,i, Sin(static_cast<double>(i)));
     }

     matA = matA + matB;

     const double THRES_A = pow(10.0, -11.0);

     for (int i = 0; i != ELEMS; ++i) {
          EXPECT_TRUE(matA(i,i) - (Cos(static_cast<double>(i)) +
                                   Sin(static_cast<double>(i))) <
                      THRES_A);
     }

     // # 10

     matA = 20.1234;
     matB = 12.3456;
     MatrixDense<double> matC;
     matC.Allocate(ELEMS);

     matC.Plus(matA, matB);

     for (int i = 0; i != ELEMS; ++i) {
          EXPECT_EQ(matC(i,i), matA(i,i) + matB(i,i));
     }

     EXPECT_EQ(matC, matA+matB);

}

//==========//
// test # 3 //
//==========//

TEST(MatrixTest3, Subtract)
{
     // This test is named "Subtract",
     // and belongs to the "MatrixTest3"

     MatrixDense<double> matA;
     const int ELEMS = 1000;

     matA.Allocate(ELEMS);

     // # 1

     const double ELEM_A = 10.0;

     matA = ELEM_A;

     EXPECT_EQ(matA, ELEM_A);

     // # 2

     const double ELEM_B = 11.0;

     matA.Subtract(ELEM_B);

     EXPECT_EQ(matA, ELEM_A-ELEM_B);

     // # 3

     MatrixDense<double> matB;

     matB.Allocate(ELEMS);

     matB = matA;

     EXPECT_EQ(matA, matB);

     // # 4

     matA = ELEM_A;
     matB = ELEM_B;

     matA.Subtract(matB);

     EXPECT_EQ(matA, ELEM_A-ELEM_B);

     // # 5

     matB.Subtract(matB);

     EXPECT_EQ(matB, 0.0);

     // # 6

     const double ELEM_C = 0.123456;
     const double ELEM_D = 1.234567;

     matA = ELEM_C;
     matB = ELEM_D;

     EXPECT_EQ(matA-matB, ELEM_C-ELEM_D);

     // # 7

     EXPECT_EQ(matA-matB + matB-matA, ELEM_C-ELEM_D + ELEM_D-ELEM_C);

     // # 8

     matA = Sin(ELEM_C);
     matB = Cos(ELEM_D);

     matA.Subtract(matA, matB);

     EXPECT_EQ(matA, sin(ELEM_C)-cos(ELEM_D));

     // # 9

     for (int i = 0; i != ELEMS; ++i) {
          matA.SetElement(i,i, Cos(static_cast<double>(i)));
          matB.SetElement(i,i, Sin(static_cast<double>(i)));
     }

     matA = matA - matB;

     const double THRES_A = pow(10.0, -11.0);

     for (int i = 0; i != ELEMS; ++i) {
          EXPECT_TRUE(matA(i,i) - (Cos(static_cast<double>(i)) -
                                   Sin(static_cast<double>(i))) <
                      THRES_A);
     }

     // # 10

     matA = 20.1234;
     matB = 12.3456;
     MatrixDense<double> matC;
     matC.Allocate(ELEMS);

     matC.Subtract(matA, matB);

     for (int i = 0; i != ELEMS; ++i) {
          EXPECT_EQ(matC(i,i), matA(i,i) - matB(i,i));
     }

     EXPECT_EQ(matC, matA-matB);

}

//==========//
// test # 4 //
//==========//

TEST(MatrixTest4, Times)
{
     // This test is named "Times",
     // and belongs to the "MatrixTest4"

     MatrixDense<double> matA;
     const int ELEMS = 1000;

     matA.Allocate(ELEMS);

     // # 1

     const double ELEM_A = 10.0;

     matA = ELEM_A;

     EXPECT_EQ(matA, ELEM_A);

     // # 2

     const double ELEM_B = 11.0;

     matA.Times(ELEM_B);

     EXPECT_EQ(matA, ELEM_A*ELEM_B);

     // # 3

     MatrixDense<double> matB;

     matB.Allocate(ELEMS);

     matB = matA;

     EXPECT_EQ(matA, matB);

     // # 4

     matA = ELEM_A;
     matB = ELEM_B;

     matA.Times(matB);

     EXPECT_EQ(matA, ELEM_A*ELEM_B);

     // # 5

     matB.Times(matB);

     EXPECT_EQ(matB, ELEM_B*ELEM_B);

     // # 6

     const double ELEM_C = 0.123456;
     const double ELEM_D = 1.234567;

     matA = ELEM_C;
     matB = ELEM_D;

     EXPECT_EQ(matA*matB, ELEM_C*ELEM_D);

     // # 7

     EXPECT_EQ(matA*matB + matB*matA, ELEM_C*ELEM_D + ELEM_D*ELEM_C);

     // # 8

     matA = Sin(ELEM_C);
     matB = Cos(ELEM_D);

     matA.Times(matA, matB);

     EXPECT_EQ(matA, sin(ELEM_C)*cos(ELEM_D));

     // # 9

     for (int i = 0; i != ELEMS; ++i) {
          matA.SetElement(i,i, Cos(static_cast<double>(i)));
          matB.SetElement(i,i, Sin(static_cast<double>(i)));
     }

     matA = matA * matB;

     const double THRES_A = pow(10.0, -11.0);

     for (int i = 0; i != ELEMS; ++i) {
          EXPECT_TRUE(matA(i,i) - (Cos(static_cast<double>(i)) *
                                   Sin(static_cast<double>(i))) <
                      THRES_A);
     }

     // # 10

     matA = 20.1234;
     matB = 12.3456;
     MatrixDense<double> matC;
     matC.Allocate(ELEMS);

     matC.Times(matA, matB);

     for (int i = 0; i != ELEMS; ++i) {
          EXPECT_EQ(matC(i,i), matA(i,i) * matB(i,i));
     }

     EXPECT_EQ(matC, matA*matB);

}

//==========//
// test # 5 //
//==========//

TEST(MatrixTest5, Negate)
{
     // This test is named "Negate",
     // and belongs to the "MatrixTest5"

     MatrixDense<double> matA;
     const int ELEMS = 1000;

     matA.Allocate(ELEMS);

     // # 1

     const double ELEM_A = 10.0;

     matA = ELEM_A;

     EXPECT_EQ(matA, ELEM_A);

     // # 2

     matA.Negate();

     EXPECT_EQ(matA, -ELEM_A);

     // # 3

     matA.Negate();

     EXPECT_EQ(matA, ELEM_A);

     // # 4

     MatrixDense<double> matB;

     matB.Allocate(ELEMS);

     (matA+matA).Negate(); // DOES NOT AFFECT matA

     matB = ELEM_A;

     EXPECT_EQ(matB, matA);

     // # 5

     matA = ELEM_A;
     matB = ELEM_A;

     (2.0*matA).Negate(); // DOES NOT AFFECT matA

     (2.0*matB).Negate(); // DOES NOT AFFECT matB


     EXPECT_EQ(matB, matA);

     EXPECT_EQ(0.0*matB, 0.0*matA);

     EXPECT_EQ(-12345.678*matB, -12345.678*matA);

     EXPECT_EQ(-0.001*matB, -0.001*matA);

     EXPECT_EQ(0.000002*matB, 0.000002*matA);

     EXPECT_EQ(0.1*matB*matA, matA*matB*0.1);
}

//==========//
// test # 6 //
//==========//

TEST(MatrixTest6, Minus)
{
     // This test is named "Minus",
     // and belongs to the "MatrixTest6"

     MatrixDense<double> matA;
     const int ELEMS = 1000;

     matA.Allocate(ELEMS);

     const double ELEM_A = 10.0;

     matA = ELEM_A;

     MatrixDense<double> matB(matA);

     // # 1

     EXPECT_EQ(matA, ELEM_A);

     // # 2

     EXPECT_EQ(matA, matB);

     // # 3

     matA.Minus(matB);

     EXPECT_EQ(matA, -matB);

     EXPECT_EQ(matA, -(-(-matB)));

     EXPECT_EQ(-matA, -(-matB));

     EXPECT_EQ(-matA, -(-(-matA)));

     // # 4

     matA = 10.0;
     matB = 11.1;

     matA.Minus(matA);
     matB.Minus(matB);

     EXPECT_EQ(matA, -10.0);
     EXPECT_EQ(matB, -11.1);

     // # 5

     matA = 10.0;
     matB = 11.1;

     matA.Minus(matA+matB);
     matB.Minus(matB+matA);

     EXPECT_EQ(matA, -21.1);

     const double THRES_A = pow(10.0, -15.0);
     const double THRES_B = pow(10.0, -16.0);
     const double THRES_C = pow(10.0, -17.0);

     for (int i = 0; i != ELEMS; ++i) {
          EXPECT_TRUE((matB(i,i) - (+21.1-11.1)) < THRES_A);
          EXPECT_TRUE((matB(i,i) - (+21.1-11.1)) < THRES_B);
          EXPECT_TRUE((matB(i,i) - (+21.1-11.1)) < THRES_C);
     }
}

//==========//
// test # 7 //
//==========//

TEST(MatrixTest7, Transpose)
{
     const unsigned long int ELEMS_A = 8 *static_cast<unsigned long int>(pow(10.0, 2.0));
     const unsigned long int ELEMS_B = 7 *static_cast<unsigned long int>(pow(10.0, 2.0));
     MatrixDense<double> matA;
     MatrixDense<double> matB;
     MatrixDense<double> matC;

     matA.Allocate(ELEMS_A, ELEMS_B);
     matB.Allocate(ELEMS_A, ELEMS_B);
     matC.Allocate(ELEMS_B, ELEMS_A);

     matA = 0.0;
     matB = 0.0;
     matC.Range(1.0);

     matA.TransposeGSL(matC);
     matB.TransposePGG(matC);

     EXPECT_TRUE(matA == matB);
     EXPECT_TRUE(matA.Equal(matB));
     EXPECT_TRUE(matB.Equal(matA));

     matA.Deallocate();
     matB.Deallocate();
     matC.Deallocate();
}

//==========//
// test # 8 //
//==========//

TEST(MatrixTest8, Transpose)
{
     const unsigned long int ELEMS = 1 *static_cast<unsigned long int>(pow(10.0, 2.0));
     MatrixDense<double> matA;
     MatrixDense<double> matB;

     matA.Allocate(ELEMS);
     matB.Allocate(ELEMS);

     matA.Range(1.0);
     matB.Range(1.0);

     matA.Transpose();
     matB.Transpose("GSL");

     EXPECT_TRUE(matA == matB);
     EXPECT_TRUE(matA.Equal(matB));
     EXPECT_TRUE(matB.Equal(matA));

     matA.Deallocate();
     matB.Deallocate();
}

//==========//
// test # 9 //
//==========//

TEST(MatrixTest9, Transpose)
{
     const unsigned long int ELEMS = 1 *static_cast<unsigned long int>(pow(10.0, 2.0));
     MatrixDense<double> matA;
     MatrixDense<double> matB;
     MatrixDense<double> matC;

     matA.Allocate(ELEMS);
     matB.Allocate(ELEMS);
     matC.Allocate(ELEMS);


     matA.Range(1.0);
     matB.Range(1.0);
     matC.Range(1.0);

     matA.Transpose("PGG"); // default case
     matB.Transpose("GSL");
     matC.Transpose("MKL");

     EXPECT_TRUE(matA == matB);
     EXPECT_TRUE(matB == matC);

     matA.Deallocate();
     matB.Deallocate();
     matC.Deallocate();
}

//===========//
// test # 10 //
//===========//

TEST(MatrixTest10, Transpose)
{
     const unsigned long int ELEMS = 1 *static_cast<unsigned long int>(pow(10.0, 2.0));
     MatrixDense<double> matA;
     MatrixDense<double> matB;
     MatrixDense<double> matC;
     MatrixDense<double> matD;

     matA.Allocate(ELEMS);
     matB.Allocate(ELEMS);
     matC.Allocate(ELEMS);
     matD.Allocate(ELEMS);

     matA.Range(1.0);
     matB = 0.0;
     matC = 0.0;
     matD = 0.0;

     matB.Transpose(matA, pgg::GSL_LIB);
     matC.Transpose(matA, pgg::MKL_LIB);
     matD.Transpose(matA, pgg::PGG_LIB);

     EXPECT_TRUE(matB == matC);
     EXPECT_TRUE(matC == matD);

     matA.Deallocate();
     matB.Deallocate();
     matC.Deallocate();
     matD.Deallocate();
}

//===========//
// test # 11 //
//===========//

TEST(MatrixTest11, Transpose)
{
     const unsigned long int ELEMS_A = 1 *static_cast<unsigned long int>(pow(10.0, 3.0));
     const unsigned long int ELEMS_B = 8 *static_cast<unsigned long int>(pow(10.0, 2.0));

     MatrixDense<double> matA;
     MatrixDense<double> matB;
     MatrixDense<double> matC;
     MatrixDense<double> matD;

     matA.Allocate(ELEMS_A, ELEMS_B);
     matB.Allocate(ELEMS_B, ELEMS_A);
     matC.Allocate(ELEMS_B, ELEMS_A);
     matD.Allocate(ELEMS_B, ELEMS_A);

     matA.Range(1.0);
     matB = 0.0;
     matC = 0.0;
     matD = 0.0;

     matB.Transpose(matA, pgg::GSL_LIB);
     matC.Transpose(matA, pgg::MKL_LIB);
     matD.Transpose(matA, pgg::PGG_LIB);

     EXPECT_TRUE(matB == matC);
     EXPECT_TRUE(matC == matD);

     matA.Deallocate();
     matB.Deallocate();
     matC.Deallocate();
     matD.Deallocate();
}

//===========//
// test # 12 //
//===========//

TEST(MatrixTest12, ComplexDouble)
{
     const unsigned long int ELEMS = 8 *static_cast<unsigned long int>(pow(10.0, 2.0));

     MatrixDense<std::complex<double>> matA;
     MatrixDense<std::complex<double>> matB;

     matA.Allocate(ELEMS);
     matB.Allocate(ELEMS);

     matA = {1.1, 4.0};
     matB = {1.1, 4.0};

     EXPECT_TRUE(matA == matB);
     EXPECT_EQ(matA, matB);

     const std::complex<double> coA = {1.1, 2.2};
     const std::complex<double> coB = {1.2, 2.3};

     matA = coA;
     EXPECT_EQ(matA, coA);

     matB = coB;
     EXPECT_EQ(matB, coB);

     matA.Deallocate();
     matB.Deallocate();
}

//===========//
// test # 13 //
//===========//

TEST(MatrixTest13, ComplexLongDouble)
{
     const unsigned long int ELEMS = 4 *static_cast<unsigned long int>(pow(10.0, 2.0));

     MatrixDense<std::complex<long double>> matA;
     MatrixDense<std::complex<long double>> matB;

     matA.Allocate(ELEMS);
     matB.Allocate(ELEMS);

     matA = {1.1L, 4.0L};
     matB = {1.1L, 4.0L};

     EXPECT_TRUE(matA == matB);
     EXPECT_EQ(matA, matB);

     const std::complex<long double> coA = {1.1L, 2.2L};
     const std::complex<long double> coB = {1.2L, 2.3L};

     matA = coA;
     EXPECT_EQ(matA, coA);

     matB = coB;
     EXPECT_EQ(matB, coB);

     matA.Deallocate();
     matB.Deallocate();
}

//===========//
// test # 14 //
//===========//

TEST(MatrixTest14, FastWayA)
{
     // local parameters

     const auto DIM = 8 *static_cast<pgg::MAI_MAX>(pow(10.0, 2.0));
     const auto K_MAX_LOC = 2;
     const auto ONE_DOUBLE = 1.0;
     const auto TWO_DOUBLE = 2.0;
     const auto THREE_DOUBLE = 3.0;

     MatrixDense<double> matA;
     MatrixDense<double> matB;
     MatrixDense<double> matC;

     matA.Allocate(DIM);
     matB.Allocate(DIM);
     matC.Allocate(DIM);

     matA.Set(ONE_DOUBLE);
     matB.Set(TWO_DOUBLE);
     matC.Set(THREE_DOUBLE);


     EXPECT_TRUE(matA.Equal(ONE_DOUBLE));
     EXPECT_TRUE(matB.Equal(TWO_DOUBLE));
     EXPECT_TRUE(matC.Equal(THREE_DOUBLE));

     // add matrices

     for (int i = 0; i != K_MAX_LOC; ++i) {
          matC.Plus(matA, matB);
     }

     EXPECT_TRUE(matA.Equal(ONE_DOUBLE));
     EXPECT_TRUE(matB.Equal(TWO_DOUBLE));
     EXPECT_TRUE(matC.Equal(THREE_DOUBLE));

     // subtract matrices

     for (int i = 0; i != K_MAX_LOC; ++i) {
          matC.Subtract(matA, matB);
     }

     EXPECT_TRUE(matA.Equal(ONE_DOUBLE));
     EXPECT_TRUE(matB.Equal(TWO_DOUBLE));
     EXPECT_TRUE(matC.Equal(-ONE_DOUBLE));

     // multiply matrices


     for (int i = 0; i != K_MAX_LOC; ++i) {
          matC.Times(matA, matB);
     }

     EXPECT_TRUE(matA.Equal(ONE_DOUBLE));
     EXPECT_TRUE(matB.Equal(TWO_DOUBLE));
     EXPECT_TRUE(matC.Equal(TWO_DOUBLE));

     // divide matrices

     for (int i = 0; i != K_MAX_LOC; ++i) {
          matC.Divide(matA, matB);
     }

     EXPECT_TRUE(matA.Equal(ONE_DOUBLE));
     EXPECT_TRUE(matB.Equal(TWO_DOUBLE));
     EXPECT_TRUE(matC.Equal(ONE_DOUBLE/TWO_DOUBLE));

     // swap matrices

     for (int i = 0; i != K_MAX_LOC; ++i) {

          matA.Set(ONE_DOUBLE);
          matB.Set(TWO_DOUBLE);
          matC.Set(THREE_DOUBLE);

          matA.Set(matB);
          matB.Set(matC);
          matC.Set(ONE_DOUBLE);
     }

     EXPECT_TRUE(matA.Equal(TWO_DOUBLE));
     EXPECT_TRUE(matB.Equal(THREE_DOUBLE));
     EXPECT_TRUE(matC.Equal(ONE_DOUBLE));

     // advance matrices

     matA.Set(ONE_DOUBLE);
     matB.Set(TWO_DOUBLE);
     matC.Set(THREE_DOUBLE);

     for (int i = 0; i != K_MAX_LOC; ++i) {
          matA.Plus(matA, ONE_DOUBLE);
          matB.Plus(matB, ONE_DOUBLE);
          matC.Plus(matC, ONE_DOUBLE);
     }

     EXPECT_TRUE(matA.Equal(ONE_DOUBLE+ONE_DOUBLE*K_MAX_LOC));
     EXPECT_TRUE(matB.Equal(TWO_DOUBLE+ONE_DOUBLE*K_MAX_LOC));
     EXPECT_TRUE(matC.Equal(THREE_DOUBLE+ONE_DOUBLE*K_MAX_LOC));

     // advance matrices

     matA.Set(ONE_DOUBLE);
     matB.Set(TWO_DOUBLE);
     matC.Set(THREE_DOUBLE);

     for (int i = 0; i != K_MAX_LOC; ++i) {
          matA.Subtract(matA, ONE_DOUBLE);
          matB.Subtract(matB, ONE_DOUBLE);
          matC.Subtract(matC, ONE_DOUBLE);
     }

     EXPECT_TRUE(matA.Equal(ONE_DOUBLE-ONE_DOUBLE*K_MAX_LOC));
     EXPECT_TRUE(matB.Equal(TWO_DOUBLE-ONE_DOUBLE*K_MAX_LOC));
     EXPECT_TRUE(matC.Equal(THREE_DOUBLE-ONE_DOUBLE*K_MAX_LOC));

     // advance matrices

     matA.Set(ONE_DOUBLE);
     matB.Set(TWO_DOUBLE);
     matC.Set(THREE_DOUBLE);

     for (int i = 0; i != K_MAX_LOC; ++i) {
          matA.Times(matA, ONE_DOUBLE);
          matB.Times(matB, ONE_DOUBLE);
          matC.Times(matC, ONE_DOUBLE);
     }

     EXPECT_TRUE(matA.Equal(ONE_DOUBLE));
     EXPECT_TRUE(matB.Equal(TWO_DOUBLE));
     EXPECT_TRUE(matC.Equal(THREE_DOUBLE));

     // advance matrices

     matA.Set(ONE_DOUBLE);
     matB.Set(TWO_DOUBLE);
     matC.Set(THREE_DOUBLE);

     for (int i = 0; i != K_MAX_LOC; ++i) {
          matA.Divide(matA, ONE_DOUBLE);
          matB.Divide(matB, ONE_DOUBLE);
          matC.Divide(matC, ONE_DOUBLE);
     }

     EXPECT_TRUE(matA.Equal(ONE_DOUBLE));
     EXPECT_TRUE(matB.Equal(TWO_DOUBLE));
     EXPECT_TRUE(matC.Equal(THREE_DOUBLE));

     // advance matrices

     matA.Set(ONE_DOUBLE);
     matB.Set(TWO_DOUBLE);
     matC.Set(THREE_DOUBLE);

     for (int i = 0; i != K_MAX_LOC; ++i) {
          matA.Plus(matA);
          matB.Plus(matB);
          matC.Plus(matC);
     }

     EXPECT_TRUE(matA.Equal(1.0*pow(TWO_DOUBLE, K_MAX_LOC)));
     EXPECT_TRUE(matB.Equal(2.0*pow(TWO_DOUBLE, K_MAX_LOC)));
     EXPECT_TRUE(matC.Equal(3.0*pow(TWO_DOUBLE, K_MAX_LOC)));

     // advance matrices

     matA.Set(ONE_DOUBLE);
     matB.Set(TWO_DOUBLE);
     matC.Set(THREE_DOUBLE);

     for (int i = 0; i != K_MAX_LOC; ++i) {
          matA.Subtract(matA);
          matB.Subtract(matB);
          matC.Subtract(matC);
     }

     EXPECT_TRUE(matA.Equal(pgg::ZERO_DBL));
     EXPECT_TRUE(matB.Equal(pgg::ZERO_DBL));
     EXPECT_TRUE(matC.Equal(pgg::ZERO_DBL));

     // advance matrices

     matA.Set(ONE_DOUBLE);
     matB.Set(TWO_DOUBLE);
     matC.Set(THREE_DOUBLE);

     for (int i = 0; i != K_MAX_LOC; ++i) {
          matA.Divide(matA);
          matB.Divide(matB);
          matC.Divide(matC);
     }

     EXPECT_TRUE(matA.Equal(pgg::ONE_DBL));
     EXPECT_TRUE(matB.Equal(pgg::ONE_DBL));
     EXPECT_TRUE(matC.Equal(pgg::ONE_DBL));

     // advance matrices

     matA.Set(ONE_DOUBLE);
     matB.Set(TWO_DOUBLE);
     matC.Set(THREE_DOUBLE);

     for (int i = 0; i != K_MAX_LOC; ++i) {
          matA.Times(matA);
          matB.Times(matB);
          matB.Divide(TWO_DOUBLE);
          matC.Times(matC);
          matC.Divide(THREE_DOUBLE);
     }

     EXPECT_TRUE(matA.Equal(ONE_DOUBLE));
     EXPECT_TRUE(matB.Equal(TWO_DOUBLE));
     EXPECT_TRUE(matC.Equal(THREE_DOUBLE));

     // deallocate heap space

     matA.Deallocate();
     matB.Deallocate();
     matC.Deallocate();

}

// END
