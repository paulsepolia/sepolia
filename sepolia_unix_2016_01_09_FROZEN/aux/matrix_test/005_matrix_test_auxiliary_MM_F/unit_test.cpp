//=============//
// test driver //
//=============//

// sepolia

#include "../../../head/sepolia.h"

using pgg::sep::MatrixDense;
using pgg::sep::VectorDense;
using pgg::functions::Cos;
using pgg::functions::Sin;
using std::cos;
using std::sin;

// Google Test

#include "gtest/gtest.h"

//==========//
// test # 1 //
//==========//

TEST(MatrixTest1, CheckCompatibility)
{
     MatrixDense<double> matA;
     MatrixDense<double> matB;

     const int ELEM = 100;

     // # 1

     matA.Allocate(ELEM);
     matB.Allocate(ELEM);

     EXPECT_TRUE(matA.CheckCompatibility(matB));
     EXPECT_TRUE(matB.CheckCompatibility(matA));

     // # 2

     matA.Deallocate();
     matB.Deallocate();

     matA.Allocate(ELEM, ELEM+1);
     matB.Allocate(ELEM, ELEM+1);

     EXPECT_TRUE(matA.CheckCompatibility(matB));
     EXPECT_TRUE(matB.CheckCompatibility(matA));

     // # 3

     matA.Deallocate();
     matB.Deallocate();

     matA.Allocate(10, ELEM);
     matB.Allocate(10, ELEM);

     EXPECT_TRUE(matA.CheckCompatibility(matB));
     EXPECT_TRUE(matB.CheckCompatibility(matA));

     // # 4

     matA.Deallocate();
     matB.Deallocate();

     matA.Allocate(10, ELEM);
     matB.Allocate(11, ELEM);

     EXPECT_TRUE(matA.CheckCompatibility(matA));
     EXPECT_TRUE(matB.CheckCompatibility(matB));

}

//==========//
// test # 2 //
//==========//

TEST(MatrixTest2, CheckCompatibilityDot)
{
     MatrixDense<double> matA;
     MatrixDense<double> matB;

     const int ELEM = 100;

     // # 1

     matA.Allocate(ELEM);
     matB.Allocate(ELEM);

     EXPECT_TRUE(matA.CheckCompatibilityDot(matA, matB));
     EXPECT_TRUE(matB.CheckCompatibilityDot(matB, matA));

     // # 2

     matA.Deallocate();
     matB.Deallocate();

     matA.Allocate(ELEM, ELEM);
     matB.Allocate(ELEM, ELEM);

     EXPECT_TRUE(matA.CheckCompatibilityDot(matB, matA));
     EXPECT_TRUE(matB.CheckCompatibilityDot(matA, matB));

     // # 3

     matA.Deallocate();
     matB.Deallocate();

     matA.Allocate(10, ELEM);
     matB.Allocate(ELEM,10);

     MatrixDense<double> matC;
     MatrixDense<double> matD;

     matC.Allocate(10,10);
     matD.Allocate(ELEM,ELEM);

     EXPECT_TRUE(matC.CheckCompatibilityDot(matA, matB));
     EXPECT_TRUE(matD.CheckCompatibilityDot(matB, matA));

}

//==========//
// test # 3 //
//==========//

TEST(MatrixTest3, Swap)
{
     MatrixDense<double> matA;
     MatrixDense<double> matB;

     const int ELEM = 100;

     // # 1

     matA.Allocate(ELEM);
     matB.Allocate(ELEM);

     matA = 11.1111;
     matB = 12.3456;

     EXPECT_EQ(matA, 11.1111);
     EXPECT_EQ(matB, 12.3456);

     // # 2

     matA.Swap(matB);

     EXPECT_EQ(matB, 11.1111);
     EXPECT_EQ(matA, 12.3456);

     // # 3

     matA.Deallocate();
     matB.Deallocate();
     matA.Allocate(101, 102);
     matB.Allocate(101, 102);

     matA = 0.0;
     matB = -1.0;

     EXPECT_EQ(matA, 0.0);
     EXPECT_EQ(matB, -1.0);

     matA.Swap(matB);

     EXPECT_EQ(matB, 0.0);
     EXPECT_EQ(matA, -1.0);

     // # 4

     matA.Random(0.0, 2.0, 20);
     matB.Random(1.0, 2.0, 10);

     MatrixDense<double > matC;
     matC.Allocate(101,102);
     matC.Random(0.0, 2.0, 20);

     MatrixDense<double > matD;
     matD.Allocate(101,102);
     matD.Random(1.0, 2.0, 10);

     EXPECT_EQ(matA, matC);
     EXPECT_EQ(matB, matD);

     matA.Swap(matB);

     EXPECT_EQ(matA, matD);
     EXPECT_EQ(matB, matC);
}

//==========//
// test # 4 //
//==========//

TEST(MatrixTest4, Equal)
{
     MatrixDense<double> matA;
     MatrixDense<double> matB;

     const int ELEM = 100;

     // # 1

     matA.Allocate(ELEM);
     matB.Allocate(ELEM);

     matA = 11.1111;
     matB = 12.3456;

     EXPECT_TRUE(matA.Equal(11.1111));
     EXPECT_TRUE(matB.Equal(12.3456));

     EXPECT_FALSE(matA.Equal(11.11111));
     EXPECT_FALSE(matB.Equal(12.34567));

     EXPECT_TRUE(matA.Equal(11.11111, 0.01));
     EXPECT_TRUE(matB.Equal(12.34567, 0.01));

     EXPECT_FALSE(matA.Equal(11.11111, 0.000000001));
     EXPECT_FALSE(matB.Equal(12.34567, 0.000000001));

     EXPECT_TRUE(matA.Equal(matA));
     EXPECT_TRUE(matB.Equal(matB));

     EXPECT_TRUE(matA.Equal(matA, 10.0));
     EXPECT_TRUE(matB.Equal(matB, 20.0));

     EXPECT_FALSE(matA.Equal(matA, -10.0));
     EXPECT_FALSE(matB.Equal(matB, -20.0));

     EXPECT_TRUE(matA.Equal(matB, 100.0));
     EXPECT_TRUE(matB.Equal(matA, 200.0));

     matA = 10.00000000000000000;
     matB = 10.00000000000000001;

     EXPECT_TRUE(matA.Equal(matB));
     EXPECT_TRUE(matB.Equal(matA));

     matA = 10.0000000000000000;
     matB = 10.0000000000000001;

     EXPECT_TRUE(matA.Equal(matB));
     EXPECT_TRUE(matB.Equal(matA));

     matA = 10.000000000000000; // 15 digits accuracy for double
     matB = 10.000000000000001;

     EXPECT_FALSE(matA.Equal(matB));
     EXPECT_FALSE(matB.Equal(matA));

     // # 2

     MatrixDense<long double> matC;
     MatrixDense<long double> matD;

     matC.Allocate(1000);
     matD.Allocate(1000);

     matC = 10.00000000000000000L;
     matD = 10.00000000000000001L;

     EXPECT_FALSE(matC.Equal(matD));
     EXPECT_FALSE(matD.Equal(matC));

     matC = 10.0000000000000000000L;
     matD = 10.0000000000000000001L; // 19 digits accuracy for long double

     EXPECT_TRUE(matC.Equal(matD));
     EXPECT_TRUE(matD.Equal(matC));
}

//==========//
// test # 5 //
//==========//

TEST(MatrixTest5, Map)
{
     MatrixDense<double> matA;
     MatrixDense<double> matB;

     matA.Allocate(100);
     matB.Allocate(100);

     matA = 10.0;
     matB = 20.0;

     // # 1

     matA.Map(cos);
     matB.Map(sin);

     EXPECT_EQ(matA, cos(10.0));
     EXPECT_EQ(matB, sin(20.0));

     // # 2

     matA.Map(cos);
     matB.Map(sin);

     EXPECT_EQ(matA, cos(cos(10.0)));
     EXPECT_EQ(matB, sin(sin(20.0)));

     // # 3

     matA.Map(cos);
     matB.Map(sin);

     EXPECT_EQ(matA, cos(cos(cos(10.0))));
     EXPECT_EQ(matB, sin(sin(sin(20.0))));

     // # 4

     matA.Map(sin);
     matB.Map(cos);

     matA.Map(sin);
     matB.Map(cos);

     EXPECT_EQ(matA, sin(sin(cos(cos(cos(10.0))))));
     EXPECT_EQ(matB, cos(cos(sin(sin(sin(20.0))))));
}

//==========//
// test # 6 //
//==========//

TEST(MatrixTest6, Nest)
{
     MatrixDense<double> matA;
     MatrixDense<double> matB;

     matA.Allocate(100);
     matB.Allocate(100);

     matA = 10.0;
     matB = 20.0;

     // # 1

     matA.Map(cos);
     matB.Map(sin);

     EXPECT_EQ(matA, Cos<double>(10.0));
     EXPECT_EQ(matB, Sin<double>(20.0));

     // # 2

     matA.Nest(Cos<double>, 4);
     matB.Nest(Sin<double>, 4);

     EXPECT_EQ(matA, cos(cos(cos(cos(cos(10.0))))));
     EXPECT_EQ(matB, sin(sin(sin(sin(sin(20.0))))));
}

//==========//
// test # 7 //
//==========//

TEST(MatrixTest7, Mean)
{
     MatrixDense<double> matA;
     MatrixDense<double> matB;

     const int ELEM_A = 100;
     const int ELEM_B = 200;

     matA.Allocate(ELEM_A);
     matB.Allocate(ELEM_B);

     matA.Random(0.0, 1.0, 20);
     matB.Random(0.0, 1.0, 20);

     double tmpA = matA.Mean();
     double tmpB = matB.Mean();

     // # 1

     pgg::functions::SeedRandomGenerator(20);

     double tmp_val = 0.0;
     for (int i = 0; i != ELEM_A*ELEM_A; ++i) {
          tmp_val = tmp_val + pgg::functions::RandomNumber(0.0, 1.0);
     }

     tmp_val = tmp_val/(ELEM_A*ELEM_A);

     EXPECT_TRUE((tmp_val - tmpA) < pow(10.0, -14.0));

     // # 2

     pgg::functions::SeedRandomGenerator(20);

     tmp_val = 0.0;
     for (int i = 0; i != ELEM_B*ELEM_B; ++i) {
          tmp_val = tmp_val + pgg::functions::RandomNumber(0.0, 1.0);
     }

     tmp_val = tmp_val/(ELEM_B*ELEM_B);

     EXPECT_TRUE((tmp_val - tmpB) < pow(10.0, -14.0));
}

//==========//
// test # 8 //
//==========//

TEST(MatrixTest8, Range)
{
     MatrixDense<double> matA;
     MatrixDense<double> matB;

     const int ELEM_A = 1000;
     const int ELEM_B = 2000;

     matA.Allocate(ELEM_A);
     matB.Allocate(ELEM_B);

     matA.Range(1.0);
     matB.Range(2.0);

     for(int i = 0; i != ELEM_A*ELEM_A; ++i) {
          EXPECT_EQ(matA(i), 1.0 + i);
     }

     for(int i = 0; i != ELEM_B*ELEM_B; ++i) {
          EXPECT_EQ(matB(i), 2.0 + i);
     }
}

//==========//
// test # 9 //
//==========//

TEST(MatrixTest9, Range)
{
     MatrixDense<double> matA;
     MatrixDense<double> matB;

     const int ELEM_A = 1000;
     const int ELEM_B = 2000;

     matA.Allocate(ELEM_A);
     matB.Allocate(ELEM_B);

     matA.Range(1.0, 2.0);
     matB.Range(2.0, 4.0);

     const double STEP_A = (2.0-1.0)/(ELEM_A*ELEM_A);
     const double STEP_B = (4.0-2.0)/(ELEM_B*ELEM_B);

     for(int i = 0; i != ELEM_A*ELEM_A; ++i) {
          EXPECT_EQ(matA(i), STEP_A*i+1.0);
     }

     for(int i = 0; i != ELEM_B*ELEM_B; ++i) {
          EXPECT_EQ(matB(i), STEP_B*i+2.0);
     }
}

//===========//
// test # 10 //
//===========//

TEST(MatrixTest10, GreaterAll)
{
     MatrixDense<double> matA;
     MatrixDense<double> matB;

     const int ELEM = 100;

     matA.Allocate(ELEM);
     matB.Allocate(ELEM);

     matA.Range(1.0, 2.0);
     matB.Range(3.0, 4.0);

     EXPECT_TRUE(matB.Greater(matA));
     EXPECT_TRUE(matB > matA);
     EXPECT_TRUE(matB.GreaterEqual(matA));
     EXPECT_TRUE(matB >= matA);
}

//===========//
// test # 11 //
//===========//

TEST(MatrixTest11, LessAll)
{
     MatrixDense<double> matA;
     MatrixDense<double> matB;

     const int ELEM = 100;

     matA.Allocate(ELEM);
     matB.Allocate(ELEM);

     matB.Range(1.0, 2.0);
     matA.Range(3.0, 4.0);

     EXPECT_TRUE(matB.Less(matA));
     EXPECT_TRUE(matB < matA);
     EXPECT_TRUE(matB.LessEqual(matA));
     EXPECT_TRUE(matB <= matA);
}

//===========//
// test # 12 //
//===========//

TEST(MatrixTest12, GreaterAll)
{
     MatrixDense<double> matA;

     const int ELEM = 100;

     matA.Allocate(ELEM);

     matA.Range(1.0, 2.0);

     EXPECT_TRUE(matA.Greater(0.0));
     EXPECT_TRUE(matA > -1.0);
     EXPECT_TRUE(matA.GreaterEqual(1.0));
     EXPECT_TRUE(matA >= 1.0);
}

//===========//
// test # 13 //
//===========//

TEST(MatrixTest13, LessAll)
{
     MatrixDense<double> matA;

     const int ELEM = 100;

     matA.Allocate(ELEM);

     matA.Range(3.0, 4.0);

     EXPECT_TRUE(matA.Less(10.0));
     EXPECT_TRUE(matA < 4.001);
     EXPECT_TRUE(matA.LessEqual(4.0));
     EXPECT_TRUE(matA <= 4.0);
}

//===========//
// test # 14 //
//===========//

TEST(MatrixTest14, CompareAllComplexA)
{
     MatrixDense<std::complex<double>> matA;
     MatrixDense<std::complex<double>> matB;

     const int ELEM = 100;

     matA.Allocate(ELEM);
     matB.Allocate(ELEM);

     matA.Set( {1.0, 0.0});
     matB.Set(1.0);

     EXPECT_TRUE(matA.Equal(matB));
     EXPECT_TRUE(matA == matB);

     EXPECT_TRUE(matA.Less( {2.0,2.0}));
     EXPECT_TRUE(matA.LessEqual(4.0));

     matA.Set( {2.0, 3.0});
     matB.Set( {3.0, 2.0});

     EXPECT_FALSE(matA.Equal(matB));
     EXPECT_FALSE(matA == matB);
     EXPECT_TRUE(matA.Less( {2.01,3.1}));
     EXPECT_TRUE(matA.LessEqual( {2.0,3.0}));
     EXPECT_TRUE(matB.Less( {3.01,2.001}));
     EXPECT_TRUE(matB.LessEqual( {3.0,2.0}));
}

//===========//
// test # 15 //
//===========//

TEST(MatrixTest15, CompareAllComplexB)
{
     MatrixDense<std::complex<long double>> matA;
     MatrixDense<std::complex<long double>> matB;

     const int ELEM = 100;

     matA.Allocate(ELEM);
     matB.Allocate(ELEM);

     matA.Set( {1.0L, 0.0L});
     matB.Set(1.0L);

     EXPECT_TRUE(matA.Equal(matB));
     EXPECT_TRUE(matA == matB);
     EXPECT_FALSE(matA.Less(2.0L));
     EXPECT_TRUE(matA.LessEqual(4.0L));

     matA.Set( {2.0L, 3.0L});
     matB.Set( {3.0L, 2.0L});

     EXPECT_FALSE(matA.Equal(matB));
     EXPECT_FALSE(matA == matB);
     EXPECT_FALSE(matA.Less(5.0L));
     EXPECT_TRUE(matA.LessEqual( {5.0L,3.0L}));
     EXPECT_FALSE(matB.Less(5.0L));
     EXPECT_FALSE(matB.LessEqual(5.0L));

}

// END
