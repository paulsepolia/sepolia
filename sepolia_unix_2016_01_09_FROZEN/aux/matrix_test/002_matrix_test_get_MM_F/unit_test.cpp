//=============//
// test driver //
//=============//

// sepolia

#include "../../../head/sepolia.h"

using pgg::sep::MatrixDense;
using pgg::sep::VectorDense;

// C++

#include <cmath>

using std::cos;
using std::sin;

// Google Test

#include "gtest/gtest.h"

//==========//
// test # 1 //
//==========//

TEST(MatrixTest1, Get)
{
     // This test is named "Get",
     // and belongs to the "MatrixTest1"

     MatrixDense<double> matA;

     // test
     // AllocatedQ()
     // DeallocatedQ()

     EXPECT_EQ(matA.AllocatedQ(), false);
     EXPECT_EQ(matA.DeallocatedQ(), true);

     // test
     // AllocatedQ()
     // DeallocatedQ()

     const int NUM_ROWS = 100;
     const int NUM_COLS = 1111;

     matA.Allocate(NUM_ROWS, NUM_COLS);

     EXPECT_EQ(matA.AllocatedQ(), true);
     EXPECT_EQ(matA.DeallocatedQ(), false);

     // test
     // = operator
     // (i,j) operator

     const double ELEM_A = 10.0;

     matA = ELEM_A;

     EXPECT_EQ(matA, ELEM_A);
     EXPECT_EQ(matA(0,0), ELEM_A);

     for (int i = 0; i != NUM_ROWS; ++i) {
          for (int j = 0; j != NUM_COLS; ++j) {
               EXPECT_EQ(matA(i,j), ELEM_A);
          }
     }

     // test
     // Set
     // GetElement(i,j)

     const double ELEM_B = 11.0;

     matA.Set(ELEM_B);

     EXPECT_EQ(matA, ELEM_B);
     EXPECT_EQ(matA.GetElement(0,0), ELEM_B);

     for (int i = 0; i != NUM_ROWS; ++i) {
          for (int j = 0; j != NUM_COLS; ++j) {
               EXPECT_EQ(matA.GetElement(i,j), ELEM_B);
          }
     }

     // test
     // GetNumberOfRows()
     // GetNUmberOfColumns()

     EXPECT_EQ(matA.GetNumberOfRows(), NUM_ROWS);
     EXPECT_EQ(matA.GetNumberOfColumns(), NUM_COLS);

     // test
     // GetColumn

     VectorDense<double> vecA;

     EXPECT_FALSE(vecA.AllocatedQ());
     EXPECT_TRUE(vecA.DeallocatedQ());

     vecA.Allocate(NUM_ROWS);

     EXPECT_TRUE(vecA.AllocatedQ());
     EXPECT_FALSE(vecA.DeallocatedQ());

     matA.GetColumn(20, vecA);

     EXPECT_EQ(vecA, ELEM_B);

     // test
     // GetRow

     vecA.Deallocate();

     EXPECT_FALSE(vecA.AllocatedQ());
     EXPECT_TRUE(vecA.DeallocatedQ());

     vecA.Allocate(NUM_COLS);

     EXPECT_TRUE(vecA.AllocatedQ());
     EXPECT_FALSE(vecA.DeallocatedQ());

     matA.GetRow(22, vecA);

     EXPECT_EQ(vecA, ELEM_B);

     vecA.Deallocate();

     EXPECT_FALSE(vecA.AllocatedQ());
     EXPECT_TRUE(vecA.DeallocatedQ());

     // end test
}

// END
