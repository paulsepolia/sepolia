#!/bin/bash

  # 1. compile

  g++-5   -c                     \
		-O3                    \
		-Wall                  \
          -std=gnu++14           \
          -fopenmp               \
          -isystem               \
          /opt/gtest/170/include \
          unit_test.cpp          \
          driver_program.cpp

  # 2. link

  g++-5	unit_test.o                                              \
         	driver_program.o                                         \
          /opt/gsl/116/gnu_492/lib/libgsl.a                        \
          /opt/mkl/libs_pgg/libmkl_core_thread_lp64_gnu_2015_pgg.a \
          /opt/gtest/170/libgtest.a                                \
          -static                                                  \
          -fopenmp                                                 \
          -ldl                                                     \
          -o x_gnu_521

  # 3. clean

  rm *.o

