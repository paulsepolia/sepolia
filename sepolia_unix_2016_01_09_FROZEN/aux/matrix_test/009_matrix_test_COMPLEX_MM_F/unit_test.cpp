//=============//
// test driver //
//=============//

// sepolia

#include "../../../head/sepolia.h"

using pgg::sep::MatrixDense;
using pgg::sep::VectorDense;

// C++

#include <complex>
#include <cmath>

using std::complex;
using std::pow;

// Google Test

#include "gtest/gtest.h"

//==========//
// test # 1 //
//==========//

TEST(MatrixTestCOMPLEX1, RealImaginaryPart1)
{
     MatrixDense<complex<double>> matA;
     MatrixDense<double> matB;
     MatrixDense<double> matC;
     const int K_MAX = 10;

     // allocations

     const pgg::MAI DIM = 1*static_cast<pgg::MAI>(pow(10.0, 2.0));

     matA.Allocate(DIM);
     matB.Allocate(DIM);
     matC.Allocate(DIM);

     matA = {-1.23456, +3.45678};

     for (int k = 0; k != K_MAX; ++k) {
          matB.RealPart(matA);
          matC.ImaginaryPart(matA);
     }

     EXPECT_TRUE(matB == -1.23456);
     EXPECT_TRUE(matC == +3.45678);
}

//==========//
// test # 2 //
//==========//

TEST(MatrixTestCOMPLEX2, RealImaginaryPart2)
{
     MatrixDense<complex<double>> matA;
     MatrixDense<double> matB;
     MatrixDense<double> matC;
     const int K_MAX = 10;

     // allocations

     const pgg::MAI DIM = 1*static_cast<pgg::MAI>(pow(10.0, 2.0));

     matA.Allocate(DIM);
     matB.Allocate(DIM);
     matC.Allocate(DIM);

     // build matrix

     for (pgg::MAI_MAX i = 0; i != DIM; ++i) {
          for (pgg::MAI_MAX j = 0; j != DIM; ++j) {
               const complex<double> tmp_elem = { static_cast<double>(i),
                                                  static_cast<double>(j)
                                                };

               matA.SetElement(i,j, tmp_elem);
          }
     }

     // get real and imaginary parts

     for (int k = 0; k != K_MAX; ++k) {
          matB.RealPart(matA);
          matC.ImaginaryPart(matA);
     }

     // test

     for (pgg::MAI_MAX i = 0; i != DIM; ++i) {
          for (pgg::MAI_MAX j = 0; j != DIM; ++j) {
               EXPECT_TRUE(matB(i,j) == static_cast<double>(i));
               EXPECT_TRUE(matC(i,j) == static_cast<double>(j));
          }
     }
}

// END
