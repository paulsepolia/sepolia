//=============//
// test driver //
//=============//

// Google Test

#include "gtest/gtest.h"

// the main code

int main(int argc, char **argv)
{
     ::testing::InitGoogleTest(&argc, argv);

     int res = RUN_ALL_TESTS();

     return res;
}

// END
