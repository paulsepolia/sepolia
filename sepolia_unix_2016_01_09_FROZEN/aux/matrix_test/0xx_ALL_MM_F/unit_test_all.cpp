//=============//
// test driver //
//=============//

// sepolia

#include "../../../head/sepolia.hpp"

using pgg::sep::MatrixDense;
using pgg::sep::VectorDense;

// C++

#include <cmath>
#include <complex>
#include <iostream>

using std::cos;
using std::sin;
using std::abs;
using std::pow;
using std::complex;
using std::cout;
using std::endl;

// Google Test

#include "gtest/gtest.h"

//==========//
// test # 1 //
//==========//

TEST(MatrixTest1, Memory)
{
     // This test is named "Memory",
     // and belongs to the "MatrixTest1"

     MatrixDense<double> matA;

     // test
     // AllocatedQ()
     // DeallocatedQ()

     EXPECT_EQ(matA.AllocatedQ(), false);
     EXPECT_EQ(matA.DeallocatedQ(), true);
     EXPECT_EQ(matA.DeallocatedQ(), !matA.AllocatedQ());

     // test
     // AllocatedQ()
     // DeallocatedQ()

     matA.Allocate(10, 10);

     EXPECT_EQ(matA.AllocatedQ(), true);
     EXPECT_EQ(matA.DeallocatedQ(), false);
     EXPECT_EQ(matA.DeallocatedQ(), !matA.AllocatedQ());

     // test
     // AllocatedQ()
     // DeallocatedQ()

     matA.Deallocate();

     EXPECT_EQ(matA.AllocatedQ(), false);
     EXPECT_EQ(matA.DeallocatedQ(), true);
     EXPECT_EQ(matA.DeallocatedQ(), !matA.AllocatedQ());

     // test
     // AllocatedQ()
     // DeallocatedQ()

     matA.Allocate(10, 11);

     EXPECT_EQ(matA.AllocatedQ(), true);
     EXPECT_EQ(matA.DeallocatedQ(), false);
     EXPECT_EQ(matA.DeallocatedQ(), !matA.AllocatedQ());

     // test
     // AllocatedQ()
     // DeallocatedQ()

     matA.Deallocate();

     EXPECT_EQ(matA.AllocatedQ(), false);
     EXPECT_EQ(matA.DeallocatedQ(), true);
     EXPECT_EQ(matA.DeallocatedQ(), !matA.AllocatedQ());

     // test
     // AllocatedQ()
     // DeallocatedQ()

     matA.Allocate(10);

     EXPECT_EQ(matA.AllocatedQ(), true);
     EXPECT_EQ(matA.DeallocatedQ(), false);
     EXPECT_EQ(matA.DeallocatedQ(), !matA.AllocatedQ());

     // test
     // AllocatedQ()
     // DeallocatedQ()

     matA.Deallocate();

     EXPECT_EQ(matA.AllocatedQ(), false);
     EXPECT_EQ(matA.DeallocatedQ(), true);
     EXPECT_EQ(matA.DeallocatedQ(), !matA.AllocatedQ());

     // end test
}

//==========//
// test # 2 //
//==========//

TEST(MatrixTest2, Memory)
{
     // This test is named "Memory",
     // and belongs to the "MatrixTest2"

     MatrixDense<double> matA;

     matA.Allocate(10);

     MatrixDense<double> matB(matA);

     // test
     // AllocatedQ()
     // DeallocatedQ()

     EXPECT_EQ(matB.AllocatedQ(), true);
     EXPECT_EQ(matB.DeallocatedQ(), false);
     EXPECT_EQ(matB.DeallocatedQ(), !matB.AllocatedQ());

     // test
     // AllocatedQ()
     // DeallocatedQ()

     matB.Deallocate();
     matB.Allocate(10, 10);

     EXPECT_EQ(matB.AllocatedQ(), true);
     EXPECT_EQ(matB.DeallocatedQ(), false);
     EXPECT_EQ(matB.DeallocatedQ(), !matB.AllocatedQ());

     // test
     // AllocatedQ()
     // DeallocatedQ()

     matB.Deallocate();

     EXPECT_EQ(matB.AllocatedQ(), false);
     EXPECT_EQ(matB.DeallocatedQ(), true);
     EXPECT_EQ(matB.DeallocatedQ(), !matB.AllocatedQ());

     // test
     // AllocatedQ()
     // DeallocatedQ()

     matB.Allocate(10, 11);

     EXPECT_EQ(matB.AllocatedQ(), true);
     EXPECT_EQ(matB.DeallocatedQ(), false);
     EXPECT_EQ(matB.DeallocatedQ(), !matB.AllocatedQ());

     // test
     // AllocatedQ()
     // DeallocatedQ()

     matB.Deallocate();

     EXPECT_EQ(matB.AllocatedQ(), false);
     EXPECT_EQ(matB.DeallocatedQ(), true);
     EXPECT_EQ(matB.DeallocatedQ(), !matB.AllocatedQ());

     // test
     // AllocatedQ()
     // DeallocatedQ()

     matB.Allocate(10);
     matB.Allocate(11);
     matB.Allocate(12);

     EXPECT_EQ(matB.AllocatedQ(), true);
     EXPECT_EQ(matB.DeallocatedQ(), false);
     EXPECT_EQ(matB.DeallocatedQ(), !matB.AllocatedQ());

     // test
     // AllocatedQ()
     // DeallocatedQ()

     matB.Deallocate();
     matB.Deallocate();
     matB.Deallocate();

     EXPECT_EQ(matB.AllocatedQ(), false);
     EXPECT_EQ(matB.DeallocatedQ(), true);
     EXPECT_EQ(matB.DeallocatedQ(), !matB.AllocatedQ());

     // end test
}

//==========//
// test # 3 //
//==========//

TEST(MatrixTest3, Memory)
{
     // This test is named "Memory",
     // and belongs to the "MatrixTest3"

     MatrixDense<double> matA;

     matA.Allocate(100);

     MatrixDense<double> matB(matA);

     // test
     // AllocatedQ()
     // DeallocatedQ()

     EXPECT_TRUE(matB.AllocatedQ());
     EXPECT_FALSE(matB.DeallocatedQ());

     // test
     // AllocatedQ()
     // DeallocatedQ()

     matB.Deallocate();

     EXPECT_FALSE(matB.AllocatedQ());
     EXPECT_TRUE(matB.DeallocatedQ());

     // test
     // AllocatedQ()
     // DeallocatedQ()

     matB.Allocate(1, 100);
     matB.Allocate(1, 200);
     matB.Allocate(200, 100);

     EXPECT_TRUE(matB.AllocatedQ());
     EXPECT_FALSE(matB.DeallocatedQ());

     // end test
}

//==========//
// test # 4 //
//==========//

TEST(MatrixTest4, Memory)
{
     // This test is named "Memory",
     // and belongs to the "MatrixTest4"

     MatrixDense<double> matA;
     MatrixDense<double> matB;

     // local scope

     {
          MatrixDense<double> matA;

          matA.Allocate(100);
          matB.Allocate(8,9);

          // test
          // AllocatedQ()
          // DeallocatedQ()

          EXPECT_TRUE(matA.AllocatedQ());
          EXPECT_FALSE(matA.DeallocatedQ());

          EXPECT_TRUE(matB.AllocatedQ());
          EXPECT_FALSE(matB.DeallocatedQ());
     }

     // test
     // AllocatedQ()
     // DeallocatedQ()

     EXPECT_FALSE(matA.AllocatedQ());
     EXPECT_TRUE(matA.DeallocatedQ());

     EXPECT_TRUE(matB.AllocatedQ());
     EXPECT_FALSE(matB.DeallocatedQ());

     matB.Deallocate();

     EXPECT_FALSE(matB.AllocatedQ());
     EXPECT_TRUE(matB.DeallocatedQ());

     // end test
}

//==========//
// test # 5 //
//==========//

TEST(MatrixTest5, Get)
{
     // This test is named "Get",
     // and belongs to the "MatrixTest1"

     MatrixDense<double> matA;

     // test
     // AllocatedQ()
     // DeallocatedQ()

     EXPECT_EQ(matA.AllocatedQ(), false);
     EXPECT_EQ(matA.DeallocatedQ(), true);

     // test
     // AllocatedQ()
     // DeallocatedQ()

     const int NUM_ROWS = 100;
     const int NUM_COLS = 1111;

     matA.Allocate(NUM_ROWS, NUM_COLS);

     EXPECT_EQ(matA.AllocatedQ(), true);
     EXPECT_EQ(matA.DeallocatedQ(), false);

     // test
     // = operator
     // (i,j) operator

     const double ELEM_A = 10.0;

     matA = ELEM_A;

     EXPECT_EQ(matA, ELEM_A);
     EXPECT_EQ(matA(0,0), ELEM_A);

     for (int i = 0; i != NUM_ROWS; ++i) {
          for (int j = 0; j != NUM_COLS; ++j) {
               EXPECT_EQ(matA(i,j), ELEM_A);
          }
     }

     // test
     // Set
     // GetElement(i,j)

     const double ELEM_B = 11.0;

     matA.Set(ELEM_B);

     EXPECT_EQ(matA, ELEM_B);
     EXPECT_EQ(matA.GetElement(0,0), ELEM_B);

     for (int i = 0; i != NUM_ROWS; ++i) {
          for (int j = 0; j != NUM_COLS; ++j) {
               EXPECT_EQ(matA.GetElement(i,j), ELEM_B);
          }
     }

     // test
     // GetNumberOfRows()
     // GetNUmberOfColumns()

     EXPECT_EQ(matA.GetNumberOfRows(), NUM_ROWS);
     EXPECT_EQ(matA.GetNumberOfColumns(), NUM_COLS);

     // test
     // GetColumn

     VectorDense<double> vecA;

     EXPECT_FALSE(vecA.AllocatedQ());
     EXPECT_TRUE(vecA.DeallocatedQ());

     vecA.Allocate(NUM_ROWS);

     EXPECT_TRUE(vecA.AllocatedQ());
     EXPECT_FALSE(vecA.DeallocatedQ());

     matA.GetColumn(20, vecA);

     EXPECT_EQ(vecA, ELEM_B);

     // test
     // GetRow

     vecA.Deallocate();

     EXPECT_FALSE(vecA.AllocatedQ());
     EXPECT_TRUE(vecA.DeallocatedQ());

     vecA.Allocate(NUM_COLS);

     EXPECT_TRUE(vecA.AllocatedQ());
     EXPECT_FALSE(vecA.DeallocatedQ());

     matA.GetRow(22, vecA);

     EXPECT_EQ(vecA, ELEM_B);

     vecA.Deallocate();

     EXPECT_FALSE(vecA.AllocatedQ());
     EXPECT_TRUE(vecA.DeallocatedQ());

     // end test
}

//==========//
// test # 6 //
//==========//

TEST(MatrixTest6, Get1)
{
     // This test is named "Get",
     // and belongs to the "MatrixTest1"

     MatrixDense<double> matA;

     // test
     // AllocatedQ()
     // DeallocatedQ()

     EXPECT_EQ(matA.AllocatedQ(), false);
     EXPECT_EQ(matA.DeallocatedQ(), true);

     // test
     // AllocatedQ()
     // DeallocatedQ()

     const int NUM_ELEMS = 1000;

     matA.Allocate(NUM_ELEMS);

     EXPECT_EQ(matA.AllocatedQ(), true);
     EXPECT_EQ(matA.DeallocatedQ(), false);

     // test
     // Set
     // (i) operator

     const double ELEM_A = 12.34567;

     matA.Set(ELEM_A);

     EXPECT_EQ(matA, ELEM_A);
     EXPECT_EQ(matA(0), ELEM_A);

     for (int i = 0; i != NUM_ELEMS; ++i) {
          EXPECT_EQ(matA(i), ELEM_A);
     }

     // test
     // SetElement
     // GetElement

     const double ELEM_B = 11.2233445566;

     for (int i = 0; i != NUM_ELEMS; ++i) {
          matA.SetElement(i, i, ELEM_B);
     }

     for (int i = 0; i != NUM_ELEMS; ++i) {
          EXPECT_EQ(matA.GetElement(i, i), ELEM_B);
     }

     // test
     // SetElement
     // GetElement

     for (int i = 0; i != NUM_ELEMS; ++i) {
          matA.SetElement(i, i, cos(i));
     }

     const double THRES_A = pow(10.0, -10.0);

     for (int i = 0; i != NUM_ELEMS; ++i) {
          EXPECT_TRUE((matA.GetElement(i, i) - cos(i)) < THRES_A);
     }

     const double THRES_B = pow(10.0, -15.0);

     for (int i = 0; i != NUM_ELEMS; ++i) {
          EXPECT_TRUE((matA.GetElement(i, i) - cos(i)) < THRES_B);
     }

     // test
     // SetElement
     // GetElement

     MatrixDense<long double> matB;

     matB.Allocate(NUM_ELEMS);

     for (int i = 0; i != NUM_ELEMS; ++i) {
          matB.SetElement(i, i, pgg::functions::Cos(static_cast<long double>(i)));
     }

     for (int i = 0; i != NUM_ELEMS; ++i) {
          EXPECT_TRUE((matB.GetElement(i, i) -
                       pgg::functions::Cos(static_cast<long double>(i))) < THRES_A);
     }

     const double THRES_C = pow(10.0, -16.0);

     for (int i = 0; i != NUM_ELEMS; ++i) {
          EXPECT_TRUE((matB.GetElement(i, i) -
                       pgg::functions::Cos(static_cast<long double>(i))) < THRES_C);
     }

     const double THRES_D = pow(10.0, -20.0);

     for (int i = 0; i != NUM_ELEMS; ++i) {
          EXPECT_TRUE((matB.GetElement(i, i) -
                       pgg::functions::Cos(static_cast<long double>(i))) < THRES_D);
     }

     // end test
}

//==========//
// test # 7 //
//==========//

TEST(MatrixTest7, Get2)
{
     // This test is named "Get",
     // and belongs to the "VectorTest1"

     MatrixDense<long double> matA;

     // test
     // AllocatedQ()
     // DeallocatedQ()

     EXPECT_EQ(matA.AllocatedQ(), false);
     EXPECT_EQ(matA.DeallocatedQ(), true);

     // test
     // AllocatedQ()
     // DeallocatedQ()

     const int NUM_ELEMS = 1000;

     matA.Allocate(NUM_ELEMS);

     EXPECT_EQ(matA.AllocatedQ(), true);
     EXPECT_EQ(matA.DeallocatedQ(), false);

     // test
     // Set
     // (i) operator

     const long double ELEM_A = 12.34567L;

     matA.Set(pgg::functions::Cos(ELEM_A));

     EXPECT_EQ(matA, pgg::functions::Cos(ELEM_A));
     EXPECT_EQ(matA(0), pgg::functions::Cos(ELEM_A));

     for (int i = 0; i != NUM_ELEMS; ++i) {
          EXPECT_EQ(matA(i), pgg::functions::Cos(ELEM_A));
     }

     // test
     // SetElement
     // GetElement

     const long double ELEM_B = pgg::functions::Sin(11.2233445566L);

     for (int i = 0; i != NUM_ELEMS; ++i) {
          matA.SetElement(i, i, pgg::functions::Sin(ELEM_B));
     }

     for (int i = 0; i != NUM_ELEMS; ++i) {
          EXPECT_EQ(matA.GetElement(i, i), pgg::functions::Sin(ELEM_B));
     }

     // test
     // SetElement
     // GetElement

     for (int i = 0; i != NUM_ELEMS; ++i) {
          matA.SetElement(i, i, pgg::functions::Sin(static_cast<long double>(i)));
     }

     const double THRES_A = pow(10.0, -10.0);

     for (int i = 0; i != NUM_ELEMS; ++i) {
          EXPECT_TRUE((matA.GetElement(i, i) - sin(i)) < THRES_A);
     }

     const double THRES_B = pow(10.0, -14.0);

     for (int i = 0; i != NUM_ELEMS; ++i) {
          EXPECT_TRUE((matA.GetElement(i, i) - sin(i)) < THRES_B);
     }

     const double THRES_C = pow(10.0, -16.0);

     for (int i = 0; i != NUM_ELEMS; ++i) {
          EXPECT_TRUE((matA.GetElement(i,i) -
                       pgg::functions::Sin(static_cast<long double>(i))) < THRES_C);
     }

     const double THRES_D = pow(10.0, -20.0);

     for (int i = 0; i != NUM_ELEMS; ++i) {
          EXPECT_TRUE((matA.GetElement(i,i) -
                       pgg::functions::Sin(static_cast<long double>(i))) < THRES_D);
     }

     const double THRES_E = pow(10.0, -14.0);

     for (int i = 0; i != NUM_ELEMS; ++i) {
          EXPECT_TRUE((matA.GetElement(i,i) -
                       sin(static_cast<long double>(i))) < THRES_E);
     }

     // end test
}

//==========//
// test # 8 //
//==========//

TEST(MatrixTest8, Get3)
{
     MatrixDense<double> matA;

     const int ELEMS = 1000;

     matA.Allocate(ELEMS);

     const double ELEM_A = 12.34;

     matA = pgg::functions::Sin(ELEM_A);

     EXPECT_TRUE(matA.Equal(pgg::functions::Sin(ELEM_A)));

     EXPECT_TRUE(matA.Equal(sin(ELEM_A)));

     EXPECT_TRUE(matA == sin(ELEM_A));

     matA.Map(pgg::functions::Cos<double>);

     EXPECT_TRUE(matA == cos(sin(ELEM_A)));

     matA.Map(pgg::functions::Sin<double>);

     EXPECT_TRUE(matA == sin(cos(sin(ELEM_A))));
}

//==========//
// test # 9 //
//==========//

TEST(MatrixTest9, GetSet)
{
     MatrixDense<double> matA;
     MatrixDense<double> matB;

     const int ELEMS = 100;

     matA.Allocate(ELEMS);
     matB.Allocate(ELEMS);

     const double ELEM_A = 12.34;

     // # 1

     matA = pgg::functions::Sin(ELEM_A);
     matB = matA;

     EXPECT_TRUE(matA == matB);

     // # 2

     matA.Map(sin);
     matB.Map(pgg::functions::Sin<double>);

     EXPECT_TRUE(matA == matB);

     // # 3

     matA.Map(asin);
     matB.Map(pgg::functions::ArcSin<double>);

     EXPECT_TRUE(matA == matB);

     // # 4

     const int NEST_NUM = 50;

     matA.Nest(asin, NEST_NUM);
     matB.Nest(pgg::functions::ArcSin<double>, NEST_NUM);

     EXPECT_TRUE(matA == matB);

     // # 5

     const int NEST_NUM_B = 200;

     matA.Nest(sin, NEST_NUM_B);
     matB.Nest(pgg::functions::Sin<double>, NEST_NUM_B);

     EXPECT_TRUE(matA == matB);

     // # 6

     const int NEST_NUM_C = 1;

     matA.Nest(acos, NEST_NUM_C);
     matB.Nest(pgg::functions::ArcCos<double>, NEST_NUM_C);

     EXPECT_TRUE(matA == matB);

     // # 7

     const int NEST_NUM_D = 10;

     matA.Nest(cos, NEST_NUM_D);
     matB.Nest(pgg::functions::Cos<double>, NEST_NUM_D);

     EXPECT_TRUE(matA == matB);

     // # 8

     const int NEST_NUM_E = 10;

     matA.Nest(cos, NEST_NUM_E);
     matB.Nest(pgg::functions::Cos<double>, NEST_NUM_E);
     const double THRES_E = pow(10.0, -15.0);

     for (int i = 0; i != ELEMS; ++i) {
          EXPECT_TRUE((matA(i) - matB(i)) < THRES_E);
     }
}

//===========//
// test # 10 //
//===========//

TEST(MatrixTest10, Divide)
{
     // This test is named "Divide",
     // and belongs to the "MatrixTest1"

     MatrixDense<double> matA;
     const int ELEMS = 1000;

     matA.Allocate(ELEMS);

     // # 1

     const double ELEM_A = 10.0;

     matA = ELEM_A;

     EXPECT_EQ(matA, ELEM_A);

     // # 2

     const double ELEM_B = 11.0;

     matA.Divide(ELEM_B);

     EXPECT_EQ(matA, ELEM_A/ELEM_B);

     // # 3

     MatrixDense<double> matB;

     matB.Allocate(ELEMS);

     matB = matA;

     EXPECT_EQ(matA, matB);

     // # 4

     matA.Divide(matB);

     EXPECT_EQ(matA, 1.0);

     // # 5

     matB.Divide(matB);

     EXPECT_EQ(matB, 1.0);

     // # 6

     const double ELEM_C = 0.123456;
     const double ELEM_D = 1.234567;

     matA = ELEM_C;
     matB = ELEM_D;

     EXPECT_EQ(matA/matB, ELEM_C/ELEM_D);

     // # 7

     EXPECT_EQ(matA/matB + matB/matA, ELEM_C/ELEM_D + ELEM_D/ELEM_C);

     // # 8

     matA = pgg::functions::Sin(ELEM_C);
     matB = pgg::functions::Cos(ELEM_D);

     matA.Divide(matA, matB);

     EXPECT_EQ(matA, sin(ELEM_C)/cos(ELEM_D));

     // # 9

     for (int i = 0; i != ELEMS; ++i) {
          matA.SetElement(i, i, pgg::functions::Cos(static_cast<double>(i)));
          matB.SetElement(i, i, pgg::functions::Sin(static_cast<double>(i))+0.1);
     }

     matA = matA / matB;

     const double THRES_A = pow(10.0, -11.0);

     for (int i = 0; i != ELEMS; ++i) {
          EXPECT_TRUE((matA(i,i) -  pgg::functions::Cos(static_cast<double>(i)) /
                       (pgg::functions::Sin(static_cast<double>(i))+0.1)) <
                      THRES_A);
     }

     // # 10

     const double  TINY_A = pow(10.0, -10.0);

     for (int i = 0; i != ELEMS; ++i) {
          matA.SetElement(i, i, pgg::functions::Cos(static_cast<double>(i)));
          matB.SetElement(i, i, pgg::functions::Sin(static_cast<double>(i))+ TINY_A);
     }

     matA = matA / matB;

     const double THRES_B = pow(10.0, -10.0);

     for (int i = 0; i != ELEMS; ++i) {
          EXPECT_TRUE((matA(i,i) -  pgg::functions::Cos(static_cast<double>(i)) /
                       (pgg::functions::Sin(static_cast<double>(i))+ TINY_A)) <
                      THRES_B);
     }
}

//===========//
// test # 11 //
//===========//

TEST(MatrixTest11, Plus)
{
     // This test is named "Plus",
     // and belongs to the "MatrixTest2"

     MatrixDense<double> matA;
     const int ELEMS = 1000;

     matA.Allocate(ELEMS);

     // # 1

     const double ELEM_A = 10.0;

     matA = ELEM_A;

     EXPECT_EQ(matA, ELEM_A);

     // # 2

     const double ELEM_B = 11.0;

     matA.Plus(ELEM_B);

     EXPECT_EQ(matA, ELEM_A+ELEM_B);

     // # 3

     MatrixDense<double> matB;

     matB.Allocate(ELEMS);

     matB = matA;

     EXPECT_EQ(matA, matB);

     // # 4

     matA.Plus(matB);

     EXPECT_EQ(matA, 2*(ELEM_A+ELEM_B));

     // # 5

     matB.Plus(matB);

     EXPECT_EQ(matB, matA);

     // # 6

     const double ELEM_C = 0.123456;
     const double ELEM_D = 1.234567;

     matA = ELEM_C;
     matB = ELEM_D;

     EXPECT_EQ(matA+matB, ELEM_C+ELEM_D);

     // # 7

     EXPECT_EQ(matA+matB + matB+matA, ELEM_C+ELEM_D + ELEM_D+ELEM_C);

     // # 8

     matA = pgg::functions::Sin(ELEM_C);
     matB = pgg::functions::Cos(ELEM_D);

     matA.Plus(matA, matB);

     EXPECT_EQ(matA, sin(ELEM_C)+cos(ELEM_D));

     // # 9

     for (int i = 0; i != ELEMS; ++i) {
          matA.SetElement(i,i, pgg::functions::Cos(static_cast<double>(i)));
          matB.SetElement(i,i, pgg::functions::Sin(static_cast<double>(i)));
     }

     matA = matA + matB;

     const double THRES_A = pow(10.0, -11.0);

     for (int i = 0; i != ELEMS; ++i) {
          EXPECT_TRUE(matA(i,i) - (pgg::functions::Cos(static_cast<double>(i)) +
                                   pgg::functions::Sin(static_cast<double>(i))) <
                      THRES_A);
     }

     // # 10

     matA = 20.1234;
     matB = 12.3456;
     MatrixDense<double> matC;
     matC.Allocate(ELEMS);

     matC.Plus(matA, matB);

     for (int i = 0; i != ELEMS; ++i) {
          EXPECT_EQ(matC(i,i), matA(i,i) + matB(i,i));
     }

     EXPECT_EQ(matC, matA+matB);
}

//===========//
// test # 12 //
//===========//

TEST(MatrixTest12, Subtract)
{
     // This test is named "Subtract",
     // and belongs to the "MatrixTest3"

     MatrixDense<double> matA;
     const int ELEMS = 1000;

     matA.Allocate(ELEMS);

     // # 1

     const double ELEM_A = 10.0;

     matA = ELEM_A;

     EXPECT_EQ(matA, ELEM_A);

     // # 2

     const double ELEM_B = 11.0;

     matA.Subtract(ELEM_B);

     EXPECT_EQ(matA, ELEM_A-ELEM_B);

     // # 3

     MatrixDense<double> matB;

     matB.Allocate(ELEMS);

     matB = matA;

     EXPECT_EQ(matA, matB);

     // # 4

     matA = ELEM_A;
     matB = ELEM_B;

     matA.Subtract(matB);

     EXPECT_EQ(matA, ELEM_A-ELEM_B);

     // # 5

     matB.Subtract(matB);

     EXPECT_EQ(matB, 0.0);

     // # 6

     const double ELEM_C = 0.123456;
     const double ELEM_D = 1.234567;

     matA = ELEM_C;
     matB = ELEM_D;

     EXPECT_EQ(matA-matB, ELEM_C-ELEM_D);

     // # 7

     EXPECT_EQ(matA-matB + matB-matA, ELEM_C-ELEM_D + ELEM_D-ELEM_C);

     // # 8

     matA = pgg::functions::Sin(ELEM_C);
     matB = pgg::functions::Cos(ELEM_D);

     matA.Subtract(matA, matB);

     EXPECT_EQ(matA, sin(ELEM_C)-cos(ELEM_D));

     // # 9

     for (int i = 0; i != ELEMS; ++i) {
          matA.SetElement(i,i, pgg::functions::Cos(static_cast<double>(i)));
          matB.SetElement(i,i, pgg::functions::Sin(static_cast<double>(i)));
     }

     matA = matA - matB;

     const double THRES_A = pow(10.0, -11.0);

     for (int i = 0; i != ELEMS; ++i) {
          EXPECT_TRUE(matA(i,i) - (pgg::functions::Cos(static_cast<double>(i)) -
                                   pgg::functions::Sin(static_cast<double>(i))) <
                      THRES_A);
     }

     // # 10

     matA = 20.1234;
     matB = 12.3456;
     MatrixDense<double> matC;
     matC.Allocate(ELEMS);

     matC.Subtract(matA, matB);

     for (int i = 0; i != ELEMS; ++i) {
          EXPECT_EQ(matC(i,i), matA(i,i) - matB(i,i));
     }

     EXPECT_EQ(matC, matA-matB);
}

//===========//
// test # 13 //
//===========//

TEST(MatrixTest13, Times)
{
     // This test is named "Times",
     // and belongs to the "MatrixTest4"

     MatrixDense<double> matA;
     const int ELEMS = 1000;

     matA.Allocate(ELEMS);

     // # 1

     const double ELEM_A = 10.0;

     matA = ELEM_A;

     EXPECT_EQ(matA, ELEM_A);

     // # 2

     const double ELEM_B = 11.0;

     matA.Times(ELEM_B);

     EXPECT_EQ(matA, ELEM_A*ELEM_B);

     // # 3

     MatrixDense<double> matB;

     matB.Allocate(ELEMS);

     matB = matA;

     EXPECT_EQ(matA, matB);

     // # 4

     matA = ELEM_A;
     matB = ELEM_B;

     matA.Times(matB);

     EXPECT_EQ(matA, ELEM_A*ELEM_B);

     // # 5

     matB.Times(matB);

     EXPECT_EQ(matB, ELEM_B*ELEM_B);

     // # 6

     const double ELEM_C = 0.123456;
     const double ELEM_D = 1.234567;

     matA = ELEM_C;
     matB = ELEM_D;

     EXPECT_EQ(matA*matB, ELEM_C*ELEM_D);

     // # 7

     EXPECT_EQ(matA*matB + matB*matA, ELEM_C*ELEM_D + ELEM_D*ELEM_C);

     // # 8

     matA = pgg::functions::Sin(ELEM_C);
     matB = pgg::functions::Cos(ELEM_D);

     matA.Times(matA, matB);

     EXPECT_EQ(matA, sin(ELEM_C)*cos(ELEM_D));

     // # 9

     for (int i = 0; i != ELEMS; ++i) {
          matA.SetElement(i,i, pgg::functions::Cos(static_cast<double>(i)));
          matB.SetElement(i,i, pgg::functions::Sin(static_cast<double>(i)));
     }

     matA = matA * matB;

     const double THRES_A = pow(10.0, -11.0);

     for (int i = 0; i != ELEMS; ++i) {
          EXPECT_TRUE(matA(i,i) - (pgg::functions::Cos(static_cast<double>(i)) *
                                   pgg::functions::Sin(static_cast<double>(i))) <
                      THRES_A);
     }

     // # 10

     matA = 20.1234;
     matB = 12.3456;
     MatrixDense<double> matC;
     matC.Allocate(ELEMS);

     matC.Times(matA, matB);

     for (int i = 0; i != ELEMS; ++i) {
          EXPECT_EQ(matC(i,i), matA(i,i) * matB(i,i));
     }

     EXPECT_EQ(matC, matA*matB);
}

//===========//
// test # 14 //
//===========//

TEST(MatrixTest14, Negate)
{
     // This test is named "Negate",
     // and belongs to the "MatrixTest5"

     MatrixDense<double> matA;
     const int ELEMS = 1000;

     matA.Allocate(ELEMS);

     // # 1

     const double ELEM_A = 10.0;

     matA = ELEM_A;

     EXPECT_EQ(matA, ELEM_A);

     // # 2

     matA.Negate();

     EXPECT_EQ(matA, -ELEM_A);

     // # 3

     matA.Negate();

     EXPECT_EQ(matA, ELEM_A);

     // # 4

     MatrixDense<double> matB;

     matB.Allocate(ELEMS);

     (matA+matA).Negate(); // DOES NOT AFFECT matA

     matB = ELEM_A;

     EXPECT_EQ(matB, matA);

     // # 5

     matA = ELEM_A;
     matB = ELEM_A;

     (2.0*matA).Negate(); // DOES NOT AFFECT matA

     (2.0*matB).Negate(); // DOES NOT AFFECT matB


     EXPECT_EQ(matB, matA);

     EXPECT_EQ(0.0*matB, 0.0*matA);

     EXPECT_EQ(-12345.678*matB, -12345.678*matA);

     EXPECT_EQ(-0.001*matB, -0.001*matA);

     EXPECT_EQ(0.000002*matB, 0.000002*matA);

     EXPECT_EQ(0.1*matB*matA, matA*matB*0.1);
}

//===========//
// test # 15 //
//===========//

TEST(MatrixTest15, Minus)
{
     // This test is named "Minus",
     // and belongs to the "MatrixTest6"

     MatrixDense<double> matA;
     const int ELEMS = 1000;

     matA.Allocate(ELEMS);

     const double ELEM_A = 10.0;

     matA = ELEM_A;

     MatrixDense<double> matB(matA);

     // # 1

     EXPECT_EQ(matA, ELEM_A);

     // # 2

     EXPECT_EQ(matA, matB);

     // # 3

     matA.Minus(matB);

     EXPECT_EQ(matA, -matB);

     EXPECT_EQ(matA, -(-(-matB)));

     EXPECT_EQ(-matA, -(-matB));

     EXPECT_EQ(-matA, -(-(-matA)));

     // # 4

     matA = 10.0;
     matB = 11.1;

     matA.Minus(matA);
     matB.Minus(matB);

     EXPECT_EQ(matA, -10.0);
     EXPECT_EQ(matB, -11.1);

     // # 5

     matA = 10.0;
     matB = 11.1;

     matA.Minus(matA+matB);
     matB.Minus(matB+matA);

     EXPECT_EQ(matA, -21.1);

     const double THRES_A = pow(10.0, -15.0);
     const double THRES_B = pow(10.0, -16.0);
     const double THRES_C = pow(10.0, -17.0);

     for (int i = 0; i != ELEMS; ++i) {
          EXPECT_TRUE((matB(i,i) - (+21.1-11.1)) < THRES_A);
          EXPECT_TRUE((matB(i,i) - (+21.1-11.1)) < THRES_B);
          EXPECT_TRUE((matB(i,i) - (+21.1-11.1)) < THRES_C);
     }
}

//===========//
// test # 16 //
//===========//

TEST(MatrixTest16, Transpose)
{
     const unsigned long int ELEMS_A = 8 *static_cast<unsigned long int>(pow(10.0, 2.0));
     const unsigned long int ELEMS_B = 7 *static_cast<unsigned long int>(pow(10.0, 2.0));
     MatrixDense<double> matA;
     MatrixDense<double> matB;
     MatrixDense<double> matC;

     matA.Allocate(ELEMS_A, ELEMS_B);
     matB.Allocate(ELEMS_A, ELEMS_B);
     matC.Allocate(ELEMS_B, ELEMS_A);

     matA = 0.0;
     matB = 0.0;
     matC.Range(1.0);

     matA.TransposeGSL(matC);
     matB.TransposePGG(matC);

     EXPECT_TRUE(matA == matB);
     EXPECT_TRUE(matA.Equal(matB));
     EXPECT_TRUE(matB.Equal(matA));

     matA.Deallocate();
     matB.Deallocate();
     matC.Deallocate();
}

//===========//
// test # 17 //
//===========//

TEST(MatrixTest17, Transpose)
{
     const unsigned long int ELEMS = 1 *static_cast<unsigned long int>(pow(10.0, 2.0));
     MatrixDense<double> matA;
     MatrixDense<double> matB;

     matA.Allocate(ELEMS);
     matB.Allocate(ELEMS);

     matA.Range(1.0);
     matB.Range(1.0);

     matA.Transpose();
     matB.Transpose("GSL");

     EXPECT_TRUE(matA == matB);
     EXPECT_TRUE(matA.Equal(matB));
     EXPECT_TRUE(matB.Equal(matA));

     matA.Deallocate();
     matB.Deallocate();
}

//===========//
// test # 18 //
//===========//

TEST(MatrixTest18, Transpose)
{
     const unsigned long int ELEMS = 1 *static_cast<unsigned long int>(pow(10.0, 2.0));
     MatrixDense<double> matA;
     MatrixDense<double> matB;
     MatrixDense<double> matC;

     matA.Allocate(ELEMS);
     matB.Allocate(ELEMS);
     matC.Allocate(ELEMS);

     matA.Range(1.0);
     matB.Range(1.0);
     matC.Range(1.0);

     matA.Transpose("PGG"); // default case
     matB.Transpose("GSL");
     matC.Transpose("MKL");

     EXPECT_TRUE(matA == matB);
     EXPECT_TRUE(matB == matC);

     matA.Deallocate();
     matB.Deallocate();
     matC.Deallocate();
}

//===========//
// test # 19 //
//===========//

TEST(MatrixTest19, Transpose)
{
     const unsigned long int ELEMS = 1 *static_cast<unsigned long int>(pow(10.0, 2.0));
     MatrixDense<double> matA;
     MatrixDense<double> matB;
     MatrixDense<double> matC;
     MatrixDense<double> matD;

     matA.Allocate(ELEMS);
     matB.Allocate(ELEMS);
     matC.Allocate(ELEMS);
     matD.Allocate(ELEMS);

     matA.Range(1.0);
     matB = 0.0;
     matC = 0.0;
     matD = 0.0;

     matB.Transpose(matA, pgg::GSL_LIB);
     matC.Transpose(matA, pgg::MKL_LIB);
     matD.Transpose(matA, pgg::PGG_LIB);

     EXPECT_TRUE(matB == matC);
     EXPECT_TRUE(matC == matD);

     matA.Deallocate();
     matB.Deallocate();
     matC.Deallocate();
     matD.Deallocate();
}

//===========//
// test # 20 //
//===========//

TEST(MatrixTest20, Transpose)
{
     const unsigned long int ELEMS_A = 1 *static_cast<unsigned long int>(pow(10.0, 3.0));
     const unsigned long int ELEMS_B = 8 *static_cast<unsigned long int>(pow(10.0, 2.0));

     MatrixDense<double> matA;
     MatrixDense<double> matB;
     MatrixDense<double> matC;
     MatrixDense<double> matD;

     matA.Allocate(ELEMS_A, ELEMS_B);
     matB.Allocate(ELEMS_B, ELEMS_A);
     matC.Allocate(ELEMS_B, ELEMS_A);
     matD.Allocate(ELEMS_B, ELEMS_A);

     matA.Range(1.0);
     matB = 0.0;
     matC = 0.0;
     matD = 0.0;

     matB.Transpose(matA, pgg::GSL_LIB);
     matC.Transpose(matA, pgg::MKL_LIB);
     matD.Transpose(matA, pgg::PGG_LIB);

     EXPECT_TRUE(matB == matC);
     EXPECT_TRUE(matC == matD);

     matA.Deallocate();
     matB.Deallocate();
     matC.Deallocate();
     matD.Deallocate();
}

//===========//
// test # 21 //
//===========//

TEST(MatrixTest21, ComplexDouble)
{
     const unsigned long int ELEMS = 8 *static_cast<unsigned long int>(pow(10.0, 2.0));

     MatrixDense<std::complex<double>> matA;
     MatrixDense<std::complex<double>> matB;

     matA.Allocate(ELEMS);
     matB.Allocate(ELEMS);

     matA = {1.1, 4.0};
     matB = {1.1, 4.0};

     EXPECT_TRUE(matA == matB);
     EXPECT_EQ(matA, matB);

     const std::complex<double> coA = {1.1, 2.2};
     const std::complex<double> coB = {1.2, 2.3};

     matA = coA;
     EXPECT_EQ(matA, coA);

     matB = coB;
     EXPECT_EQ(matB, coB);

     matA.Deallocate();
     matB.Deallocate();
}

//===========//
// test # 22 //
//===========//

TEST(MatrixTest22, ComplexLongDouble)
{
     const unsigned long int ELEMS = 4 *static_cast<unsigned long int>(pow(10.0, 2.0));

     MatrixDense<std::complex<long double>> matA;
     MatrixDense<std::complex<long double>> matB;

     matA.Allocate(ELEMS);
     matB.Allocate(ELEMS);

     matA = {1.1L, 4.0L};
     matB = {1.1L, 4.0L};

     EXPECT_TRUE(matA == matB);
     EXPECT_EQ(matA, matB);

     const std::complex<long double> coA = {1.1L, 2.2L};
     const std::complex<long double> coB = {1.2L, 2.3L};

     matA = coA;
     EXPECT_EQ(matA, coA);

     matB = coB;
     EXPECT_EQ(matB, coB);

     matA.Deallocate();
     matB.Deallocate();
}

//===========//
// test # 23 //
//===========//

TEST(MatrixTest23, FastWayA)
{
     // local parameters

     const auto DIM = 8 *static_cast<pgg::MAI_MAX>(pow(10.0, 2.0));
     const auto K_MAX_LOC = 2;
     const auto ONE_DOUBLE = 1.0;
     const auto TWO_DOUBLE = 2.0;
     const auto THREE_DOUBLE = 3.0;

     MatrixDense<double> matA;
     MatrixDense<double> matB;
     MatrixDense<double> matC;

     matA.Allocate(DIM);
     matB.Allocate(DIM);
     matC.Allocate(DIM);

     matA.Set(ONE_DOUBLE);
     matB.Set(TWO_DOUBLE);
     matC.Set(THREE_DOUBLE);


     EXPECT_TRUE(matA.Equal(ONE_DOUBLE));
     EXPECT_TRUE(matB.Equal(TWO_DOUBLE));
     EXPECT_TRUE(matC.Equal(THREE_DOUBLE));

     // add matrices

     for (int i = 0; i != K_MAX_LOC; ++i) {
          matC.Plus(matA, matB);
     }

     EXPECT_TRUE(matA.Equal(ONE_DOUBLE));
     EXPECT_TRUE(matB.Equal(TWO_DOUBLE));
     EXPECT_TRUE(matC.Equal(THREE_DOUBLE));

     // subtract matrices

     for (int i = 0; i != K_MAX_LOC; ++i) {
          matC.Subtract(matA, matB);
     }

     EXPECT_TRUE(matA.Equal(ONE_DOUBLE));
     EXPECT_TRUE(matB.Equal(TWO_DOUBLE));
     EXPECT_TRUE(matC.Equal(-ONE_DOUBLE));

     // multiply matrices


     for (int i = 0; i != K_MAX_LOC; ++i) {
          matC.Times(matA, matB);
     }

     EXPECT_TRUE(matA.Equal(ONE_DOUBLE));
     EXPECT_TRUE(matB.Equal(TWO_DOUBLE));
     EXPECT_TRUE(matC.Equal(TWO_DOUBLE));

     // divide matrices

     for (int i = 0; i != K_MAX_LOC; ++i) {
          matC.Divide(matA, matB);
     }

     EXPECT_TRUE(matA.Equal(ONE_DOUBLE));
     EXPECT_TRUE(matB.Equal(TWO_DOUBLE));
     EXPECT_TRUE(matC.Equal(ONE_DOUBLE/TWO_DOUBLE));

     // swap matrices

     for (int i = 0; i != K_MAX_LOC; ++i) {

          matA.Set(ONE_DOUBLE);
          matB.Set(TWO_DOUBLE);
          matC.Set(THREE_DOUBLE);

          matA.Set(matB);
          matB.Set(matC);
          matC.Set(ONE_DOUBLE);
     }

     EXPECT_TRUE(matA.Equal(TWO_DOUBLE));
     EXPECT_TRUE(matB.Equal(THREE_DOUBLE));
     EXPECT_TRUE(matC.Equal(ONE_DOUBLE));

     // advance matrices

     matA.Set(ONE_DOUBLE);
     matB.Set(TWO_DOUBLE);
     matC.Set(THREE_DOUBLE);

     for (int i = 0; i != K_MAX_LOC; ++i) {
          matA.Plus(matA, ONE_DOUBLE);
          matB.Plus(matB, ONE_DOUBLE);
          matC.Plus(matC, ONE_DOUBLE);
     }

     EXPECT_TRUE(matA.Equal(ONE_DOUBLE+ONE_DOUBLE*K_MAX_LOC));
     EXPECT_TRUE(matB.Equal(TWO_DOUBLE+ONE_DOUBLE*K_MAX_LOC));
     EXPECT_TRUE(matC.Equal(THREE_DOUBLE+ONE_DOUBLE*K_MAX_LOC));

     // advance matrices

     matA.Set(ONE_DOUBLE);
     matB.Set(TWO_DOUBLE);
     matC.Set(THREE_DOUBLE);

     for (int i = 0; i != K_MAX_LOC; ++i) {
          matA.Subtract(matA, ONE_DOUBLE);
          matB.Subtract(matB, ONE_DOUBLE);
          matC.Subtract(matC, ONE_DOUBLE);
     }

     EXPECT_TRUE(matA.Equal(ONE_DOUBLE-ONE_DOUBLE*K_MAX_LOC));
     EXPECT_TRUE(matB.Equal(TWO_DOUBLE-ONE_DOUBLE*K_MAX_LOC));
     EXPECT_TRUE(matC.Equal(THREE_DOUBLE-ONE_DOUBLE*K_MAX_LOC));

     // advance matrices

     matA.Set(ONE_DOUBLE);
     matB.Set(TWO_DOUBLE);
     matC.Set(THREE_DOUBLE);

     for (int i = 0; i != K_MAX_LOC; ++i) {
          matA.Times(matA, ONE_DOUBLE);
          matB.Times(matB, ONE_DOUBLE);
          matC.Times(matC, ONE_DOUBLE);
     }

     EXPECT_TRUE(matA.Equal(ONE_DOUBLE));
     EXPECT_TRUE(matB.Equal(TWO_DOUBLE));
     EXPECT_TRUE(matC.Equal(THREE_DOUBLE));

     // advance matrices

     matA.Set(ONE_DOUBLE);
     matB.Set(TWO_DOUBLE);
     matC.Set(THREE_DOUBLE);

     for (int i = 0; i != K_MAX_LOC; ++i) {
          matA.Divide(matA, ONE_DOUBLE);
          matB.Divide(matB, ONE_DOUBLE);
          matC.Divide(matC, ONE_DOUBLE);
     }

     EXPECT_TRUE(matA.Equal(ONE_DOUBLE));
     EXPECT_TRUE(matB.Equal(TWO_DOUBLE));
     EXPECT_TRUE(matC.Equal(THREE_DOUBLE));

     // advance matrices

     matA.Set(ONE_DOUBLE);
     matB.Set(TWO_DOUBLE);
     matC.Set(THREE_DOUBLE);

     for (int i = 0; i != K_MAX_LOC; ++i) {
          matA.Plus(matA);
          matB.Plus(matB);
          matC.Plus(matC);
     }

     EXPECT_TRUE(matA.Equal(1.0*pow(TWO_DOUBLE, K_MAX_LOC)));
     EXPECT_TRUE(matB.Equal(2.0*pow(TWO_DOUBLE, K_MAX_LOC)));
     EXPECT_TRUE(matC.Equal(3.0*pow(TWO_DOUBLE, K_MAX_LOC)));

     // advance matrices

     matA.Set(ONE_DOUBLE);
     matB.Set(TWO_DOUBLE);
     matC.Set(THREE_DOUBLE);

     for (int i = 0; i != K_MAX_LOC; ++i) {
          matA.Subtract(matA);
          matB.Subtract(matB);
          matC.Subtract(matC);
     }

     EXPECT_TRUE(matA.Equal(pgg::ZERO_DBL));
     EXPECT_TRUE(matB.Equal(pgg::ZERO_DBL));
     EXPECT_TRUE(matC.Equal(pgg::ZERO_DBL));

     // advance matrices

     matA.Set(ONE_DOUBLE);
     matB.Set(TWO_DOUBLE);
     matC.Set(THREE_DOUBLE);

     for (int i = 0; i != K_MAX_LOC; ++i) {
          matA.Divide(matA);
          matB.Divide(matB);
          matC.Divide(matC);
     }

     EXPECT_TRUE(matA.Equal(pgg::ONE_DBL));
     EXPECT_TRUE(matB.Equal(pgg::ONE_DBL));
     EXPECT_TRUE(matC.Equal(pgg::ONE_DBL));

     // advance matrices

     matA.Set(ONE_DOUBLE);
     matB.Set(TWO_DOUBLE);
     matC.Set(THREE_DOUBLE);

     for (int i = 0; i != K_MAX_LOC; ++i) {
          matA.Times(matA);
          matB.Times(matB);
          matB.Divide(TWO_DOUBLE);
          matC.Times(matC);
          matC.Divide(THREE_DOUBLE);
     }

     EXPECT_TRUE(matA.Equal(ONE_DOUBLE));
     EXPECT_TRUE(matB.Equal(TWO_DOUBLE));
     EXPECT_TRUE(matC.Equal(THREE_DOUBLE));

     // deallocate heap space

     matA.Deallocate();
     matB.Deallocate();
     matC.Deallocate();
}

//===========//
// test # 24 //
//===========//

TEST(MatrixTest24, CheckCompatibility)
{
     MatrixDense<double> matA;
     MatrixDense<double> matB;

     const int ELEM = 100;

     // # 1

     matA.Allocate(ELEM);
     matB.Allocate(ELEM);

     EXPECT_TRUE(matA.CheckCompatibility(matB));
     EXPECT_TRUE(matB.CheckCompatibility(matA));

     // # 2

     matA.Deallocate();
     matB.Deallocate();

     matA.Allocate(ELEM, ELEM+1);
     matB.Allocate(ELEM, ELEM+1);

     EXPECT_TRUE(matA.CheckCompatibility(matB));
     EXPECT_TRUE(matB.CheckCompatibility(matA));

     // # 3

     matA.Deallocate();
     matB.Deallocate();

     matA.Allocate(10, ELEM);
     matB.Allocate(10, ELEM);

     EXPECT_TRUE(matA.CheckCompatibility(matB));
     EXPECT_TRUE(matB.CheckCompatibility(matA));

     // # 4

     matA.Deallocate();
     matB.Deallocate();

     matA.Allocate(10, ELEM);
     matB.Allocate(11, ELEM);

     EXPECT_TRUE(matA.CheckCompatibility(matA));
     EXPECT_TRUE(matB.CheckCompatibility(matB));
}

//===========//
// test # 25 //
//===========//

TEST(MatrixTest25, CheckCompatibilityDot)
{
     MatrixDense<double> matA;
     MatrixDense<double> matB;

     const int ELEM = 100;

     // # 1

     matA.Allocate(ELEM);
     matB.Allocate(ELEM);

     EXPECT_TRUE(matA.CheckCompatibilityDot(matA, matB));
     EXPECT_TRUE(matB.CheckCompatibilityDot(matB, matA));

     // # 2

     matA.Deallocate();
     matB.Deallocate();

     matA.Allocate(ELEM, ELEM);
     matB.Allocate(ELEM, ELEM);

     EXPECT_TRUE(matA.CheckCompatibilityDot(matB, matA));
     EXPECT_TRUE(matB.CheckCompatibilityDot(matA, matB));

     // # 3

     matA.Deallocate();
     matB.Deallocate();

     matA.Allocate(10, ELEM);
     matB.Allocate(ELEM,10);

     MatrixDense<double> matC;
     MatrixDense<double> matD;

     matC.Allocate(10,10);
     matD.Allocate(ELEM,ELEM);

     EXPECT_TRUE(matC.CheckCompatibilityDot(matA, matB));
     EXPECT_TRUE(matD.CheckCompatibilityDot(matB, matA));
}

//===========//
// test # 26 //
//===========//

TEST(MatrixTest26, Swap)
{
     MatrixDense<double> matA;
     MatrixDense<double> matB;

     const int ELEM = 100;

     // # 1

     matA.Allocate(ELEM);
     matB.Allocate(ELEM);

     matA = 11.1111;
     matB = 12.3456;

     EXPECT_EQ(matA, 11.1111);
     EXPECT_EQ(matB, 12.3456);

     // # 2

     matA.Swap(matB);

     EXPECT_EQ(matB, 11.1111);
     EXPECT_EQ(matA, 12.3456);

     // # 3

     matA.Deallocate();
     matB.Deallocate();
     matA.Allocate(101, 102);
     matB.Allocate(101, 102);

     matA = 0.0;
     matB = -1.0;

     EXPECT_EQ(matA, 0.0);
     EXPECT_EQ(matB, -1.0);

     matA.Swap(matB);

     EXPECT_EQ(matB, 0.0);
     EXPECT_EQ(matA, -1.0);

     // # 4

     matA.Random(0.0, 2.0, 20);
     matB.Random(1.0, 2.0, 10);

     MatrixDense<double > matC;
     matC.Allocate(101,102);
     matC.Random(0.0, 2.0, 20);

     MatrixDense<double > matD;
     matD.Allocate(101,102);
     matD.Random(1.0, 2.0, 10);

     EXPECT_EQ(matA, matC);
     EXPECT_EQ(matB, matD);

     matA.Swap(matB);

     EXPECT_EQ(matA, matD);
     EXPECT_EQ(matB, matC);
}

//===========//
// test # 27 //
//===========//

TEST(MatrixTest27, Equal)
{
     MatrixDense<double> matA;
     MatrixDense<double> matB;

     const int ELEM = 100;

     // # 1

     matA.Allocate(ELEM);
     matB.Allocate(ELEM);

     matA = 11.1111;
     matB = 12.3456;

     EXPECT_TRUE(matA.Equal(11.1111));
     EXPECT_TRUE(matB.Equal(12.3456));

     EXPECT_FALSE(matA.Equal(11.11111));
     EXPECT_FALSE(matB.Equal(12.34567));

     EXPECT_TRUE(matA.Equal(11.11111, 0.01));
     EXPECT_TRUE(matB.Equal(12.34567, 0.01));

     EXPECT_FALSE(matA.Equal(11.11111, 0.000000001));
     EXPECT_FALSE(matB.Equal(12.34567, 0.000000001));

     EXPECT_TRUE(matA.Equal(matA));
     EXPECT_TRUE(matB.Equal(matB));

     EXPECT_TRUE(matA.Equal(matA, 10.0));
     EXPECT_TRUE(matB.Equal(matB, 20.0));

     EXPECT_FALSE(matA.Equal(matA, -10.0));
     EXPECT_FALSE(matB.Equal(matB, -20.0));

     EXPECT_TRUE(matA.Equal(matB, 100.0));
     EXPECT_TRUE(matB.Equal(matA, 200.0));

     matA = 10.00000000000000000;
     matB = 10.00000000000000001;

     EXPECT_TRUE(matA.Equal(matB));
     EXPECT_TRUE(matB.Equal(matA));

     matA = 10.0000000000000000;
     matB = 10.0000000000000001;

     EXPECT_TRUE(matA.Equal(matB));
     EXPECT_TRUE(matB.Equal(matA));

     matA = 10.000000000000000; // 15 digits accuracy for double
     matB = 10.000000000000001;

     EXPECT_FALSE(matA.Equal(matB));
     EXPECT_FALSE(matB.Equal(matA));

     // # 2

     MatrixDense<long double> matC;
     MatrixDense<long double> matD;

     matC.Allocate(1000);
     matD.Allocate(1000);

     matC = 10.00000000000000000L;
     matD = 10.00000000000000001L;

     EXPECT_FALSE(matC.Equal(matD));
     EXPECT_FALSE(matD.Equal(matC));

     matC = 10.0000000000000000000L;
     matD = 10.0000000000000000001L; // 19 digits accuracy for long double

     EXPECT_TRUE(matC.Equal(matD));
     EXPECT_TRUE(matD.Equal(matC));
}

//===========//
// test # 28 //
//===========//

TEST(MatrixTest28, Map)
{
     MatrixDense<double> matA;
     MatrixDense<double> matB;

     matA.Allocate(100);
     matB.Allocate(100);

     matA = 10.0;
     matB = 20.0;

     // # 1

     matA.Map(cos);
     matB.Map(sin);

     EXPECT_EQ(matA, cos(10.0));
     EXPECT_EQ(matB, sin(20.0));

     // # 2

     matA.Map(cos);
     matB.Map(sin);

     EXPECT_EQ(matA, cos(cos(10.0)));
     EXPECT_EQ(matB, sin(sin(20.0)));

     // # 3

     matA.Map(cos);
     matB.Map(sin);

     EXPECT_EQ(matA, cos(cos(cos(10.0))));
     EXPECT_EQ(matB, sin(sin(sin(20.0))));

     // # 4

     matA.Map(sin);
     matB.Map(cos);

     matA.Map(sin);
     matB.Map(cos);

     EXPECT_EQ(matA, sin(sin(cos(cos(cos(10.0))))));
     EXPECT_EQ(matB, cos(cos(sin(sin(sin(20.0))))));
}

//===========//
// test # 29 //
//===========//

TEST(MatrixTest29, Nest)
{
     MatrixDense<double> matA;
     MatrixDense<double> matB;

     matA.Allocate(100);
     matB.Allocate(100);

     matA = 10.0;
     matB = 20.0;

     // # 1

     matA.Map(cos);
     matB.Map(sin);

     EXPECT_EQ(matA, pgg::functions::Cos<double>(10.0));
     EXPECT_EQ(matB, pgg::functions::Sin<double>(20.0));

     // # 2

     matA.Nest(pgg::functions::Cos<double>, 4);
     matB.Nest(pgg::functions::Sin<double>, 4);

     EXPECT_EQ(matA, cos(cos(cos(cos(cos(10.0))))));
     EXPECT_EQ(matB, sin(sin(sin(sin(sin(20.0))))));
}

//===========//
// test # 30 //
//===========//

TEST(MatrixTest30, Mean)
{
     MatrixDense<double> matA;
     MatrixDense<double> matB;

     const int ELEM_A = 100;
     const int ELEM_B = 200;

     matA.Allocate(ELEM_A);
     matB.Allocate(ELEM_B);

     matA.Random(0.0, 1.0, 20);
     matB.Random(0.0, 1.0, 20);

     double tmpA = matA.Mean();
     double tmpB = matB.Mean();

     // # 1

     pgg::functions::SeedRandomGenerator(20);

     double tmp_val = 0.0;
     for (int i = 0; i != ELEM_A*ELEM_A; ++i) {
          tmp_val = tmp_val + pgg::functions::RandomNumber(0.0, 1.0);
     }

     tmp_val = tmp_val/(ELEM_A*ELEM_A);

     EXPECT_TRUE((tmp_val - tmpA) < pow(10.0, -14.0));

     // # 2

     pgg::functions::SeedRandomGenerator(20);

     tmp_val = 0.0;
     for (int i = 0; i != ELEM_B*ELEM_B; ++i) {
          tmp_val = tmp_val + pgg::functions::RandomNumber(0.0, 1.0);
     }

     tmp_val = tmp_val/(ELEM_B*ELEM_B);

     EXPECT_TRUE((tmp_val - tmpB) < pow(10.0, -14.0));
}

//===========//
// test # 31 //
//===========//

TEST(MatrixTest31, Range)
{
     MatrixDense<double> matA;
     MatrixDense<double> matB;

     const int ELEM_A = 1000;
     const int ELEM_B = 2000;

     matA.Allocate(ELEM_A);
     matB.Allocate(ELEM_B);

     matA.Range(1.0);
     matB.Range(2.0);

     for(int i = 0; i != ELEM_A*ELEM_A; ++i) {
          EXPECT_EQ(matA(i), 1.0 + i);
     }

     for(int i = 0; i != ELEM_B*ELEM_B; ++i) {
          EXPECT_EQ(matB(i), 2.0 + i);
     }
}

//===========//
// test # 32 //
//===========//

TEST(MatrixTest32, Range)
{
     MatrixDense<double> matA;
     MatrixDense<double> matB;

     const int ELEM_A = 1000;
     const int ELEM_B = 2000;

     matA.Allocate(ELEM_A);
     matB.Allocate(ELEM_B);

     matA.Range(1.0, 2.0);
     matB.Range(2.0, 4.0);

     const double STEP_A = (2.0-1.0)/(ELEM_A*ELEM_A);
     const double STEP_B = (4.0-2.0)/(ELEM_B*ELEM_B);

     for(int i = 0; i != ELEM_A*ELEM_A; ++i) {
          EXPECT_EQ(matA(i), STEP_A*i+1.0);
     }

     for(int i = 0; i != ELEM_B*ELEM_B; ++i) {
          EXPECT_EQ(matB(i), STEP_B*i+2.0);
     }
}

//===========//
// test # 33 //
//===========//

TEST(MatrixTest33, GreaterAll)
{
     MatrixDense<double> matA;
     MatrixDense<double> matB;

     const int ELEM = 100;

     matA.Allocate(ELEM);
     matB.Allocate(ELEM);

     matA.Range(1.0, 2.0);
     matB.Range(3.0, 4.0);

     EXPECT_TRUE(matB.Greater(matA));
     EXPECT_TRUE(matB > matA);
     EXPECT_TRUE(matB.GreaterEqual(matA));
     EXPECT_TRUE(matB >= matA);
}

//===========//
// test # 34 //
//===========//

TEST(MatrixTest34, LessAll)
{
     MatrixDense<double> matA;
     MatrixDense<double> matB;

     const int ELEM = 100;

     matA.Allocate(ELEM);
     matB.Allocate(ELEM);

     matB.Range(1.0, 2.0);
     matA.Range(3.0, 4.0);

     EXPECT_TRUE(matB.Less(matA));
     EXPECT_TRUE(matB < matA);
     EXPECT_TRUE(matB.LessEqual(matA));
     EXPECT_TRUE(matB <= matA);
}

//===========//
// test # 35 //
//===========//

TEST(MatrixTest35, GreaterAll)
{
     MatrixDense<double> matA;

     const int ELEM = 100;

     matA.Allocate(ELEM);

     matA.Range(1.0, 2.0);

     EXPECT_TRUE(matA.Greater(0.0));
     EXPECT_TRUE(matA > -1.0);
     EXPECT_TRUE(matA.GreaterEqual(1.0));
     EXPECT_TRUE(matA >= 1.0);
}

//===========//
// test # 36 //
//===========//

TEST(MatrixTest36, LessAll)
{
     MatrixDense<double> matA;

     const int ELEM = 100;

     matA.Allocate(ELEM);

     matA.Range(3.0, 4.0);

     EXPECT_TRUE(matA.Less(10.0));
     EXPECT_TRUE(matA < 4.001);
     EXPECT_TRUE(matA.LessEqual(4.0));
     EXPECT_TRUE(matA <= 4.0);
}

//===========//
// test # 37 //
//===========//

TEST(MatrixTest37, CompareAllComplexA)
{
     MatrixDense<std::complex<double>> matA;
     MatrixDense<std::complex<double>> matB;

     const int ELEM = 100;

     matA.Allocate(ELEM);
     matB.Allocate(ELEM);

     matA.Set( {1.0, 0.0});
     matB.Set(1.0);

     EXPECT_TRUE(matA.Equal(matB));
     EXPECT_TRUE(matA == matB);

     EXPECT_TRUE(matA.Less( {2.0,2.0}));
     EXPECT_TRUE(matA.LessEqual(4.0));

     matA.Set( {2.0, 3.0});
     matB.Set( {3.0, 2.0});

     EXPECT_FALSE(matA.Equal(matB));
     EXPECT_FALSE(matA == matB);
     EXPECT_TRUE(matA.Less( {2.01,3.1}));
     EXPECT_TRUE(matA.LessEqual( {2.0,3.0}));
     EXPECT_TRUE(matB.Less( {3.01,2.001}));
     EXPECT_TRUE(matB.LessEqual( {3.0,2.0}));
}

//===========//
// test # 38 //
//===========//

TEST(MatrixTest38, CompareAllComplexB)
{
     MatrixDense<std::complex<long double>> matA;
     MatrixDense<std::complex<long double>> matB;

     const int ELEM = 100;

     matA.Allocate(ELEM);
     matB.Allocate(ELEM);

     matA.Set( {1.0L, 0.0L});
     matB.Set(1.0L);

     EXPECT_TRUE(matA.Equal(matB));
     EXPECT_TRUE(matA == matB);
     EXPECT_FALSE(matA.Less(2.0L));
     EXPECT_TRUE(matA.LessEqual(4.0L));

     matA.Set( {2.0L, 3.0L});
     matB.Set( {3.0L, 2.0L});

     EXPECT_FALSE(matA.Equal(matB));
     EXPECT_FALSE(matA == matB);
     EXPECT_FALSE(matA.Less(5.0L));
     EXPECT_TRUE(matA.LessEqual( {5.0L,3.0L}));
     EXPECT_FALSE(matB.Less(5.0L));
     EXPECT_FALSE(matB.LessEqual(5.0L));
}

//===========//
// test # 39 //
//===========//

TEST(MatrixTest39, Dot)
{
     MatrixDense<double> matA;
     MatrixDense<double> matB;
     MatrixDense<double> matC;
     MatrixDense<double> matD;

     const int ELEM = 100;

     // # 1

     matA.Allocate(ELEM);
     matB.Allocate(ELEM);
     matC.Allocate(ELEM);
     matD.Allocate(ELEM);

     matC.CheckCompatibilityDot(matA, matB);
     matD.CheckCompatibilityDot(matB, matA);

     matA = +0.1;
     matB = -0.2;

     matC.Dot(matA, matB);
     matD.Dot(matB, matA);

     EXPECT_EQ(matC, matD);

     // # 2

     matA.Random(0.1, 0.5, 20);
     matB.Random(0.1, 0.5, 20);

     matC.Dot(matA, matB);
     matD.Dot(matB, matA);

     EXPECT_TRUE(matC.Equal(matD));

     // # 3

     matA.Deallocate();
     matB.Deallocate();
     matC.Deallocate();
     matD.Deallocate();

     const int ELEM_B = 100;

     matA.Allocate(1,ELEM_B);
     matB.Allocate(ELEM_B,1);
     matC.Allocate(1,1);
     matD.Allocate(1,1);

     for(int i = 0; i != ELEM_B; ++i) {
          matA.SetElement(i, sin(static_cast<double>(i)));
          matB.SetElement(i, cos(static_cast<double>(i)));
     }

     matC.Dot(matA, matB);

     const double VAL_100 = 0.300642576113028;

     EXPECT_TRUE((matC(0,0)-VAL_100) < pow(10.0, -14.0));
}

//===========//
// test # 40 //
//===========//

TEST(MatrixTest40, Dot)
{
     MatrixDense<double> matA;
     MatrixDense<double> matB;
     MatrixDense<double> matC;

     // # 1

     const int ELEM_B = 100;

     matA.Allocate(ELEM_B,1);
     matB.Allocate(1, ELEM_B);
     matC.Allocate(ELEM_B, ELEM_B);

     for(int i = 0; i != ELEM_B; ++i) {
          matA.SetElement(i, sin(static_cast<double>(i)));
          matB.SetElement(i, cos(static_cast<double>(i)));
     }

     matC.Dot(matA, matB);

     const double c10 = +0.841470984807897;
     const double c11 = +0.454648713412841;
     const double c12 = -0.350175488374015;
     const double c13 = -0.833049961066805;

     const double c20 = +0.909297426825682;
     const double c21 = +0.491295496433882;
     const double c22 = -0.378401247653964;
     const double c23 = -0.900197629735517;

     const double c9999 = -0.0397892958321418;
     const double c9898 = +0.469765027784966;
     const double c9797 = -0.351193164634246;
     const double c9696 = -0.177469178825923;

     EXPECT_TRUE((matC(1,0)-c10) < pow(10.0, -14.0));
     EXPECT_TRUE((matC(1,1)-c11) < pow(10.0, -14.0));
     EXPECT_TRUE((matC(1,2)-c12) < pow(10.0, -14.0));
     EXPECT_TRUE((matC(1,3)-c13) < pow(10.0, -14.0));

     EXPECT_TRUE((matC(2,0)-c20) < pow(10.0, -14.0));
     EXPECT_TRUE((matC(2,1)-c21) < pow(10.0, -14.0));
     EXPECT_TRUE((matC(2,2)-c22) < pow(10.0, -14.0));
     EXPECT_TRUE((matC(2,3)-c23) < pow(10.0, -14.0));

     EXPECT_TRUE((matC(99,99)-c9999) < pow(10.0, -14.0));
     EXPECT_TRUE((matC(98,98)-c9898) < pow(10.0, -14.0));
     EXPECT_TRUE((matC(97,97)-c9797) < pow(10.0, -14.0));
     EXPECT_TRUE((matC(96,96)-c9696) < pow(10.0, -14.0));
}

//===========//
// test # 41 //
//===========//

TEST(MatrixTest41, Dot)
{
     MatrixDense<double> matA;
     MatrixDense<double> matB;
     MatrixDense<double> matC;

     // # 1

     const int ELEM_B = 100;

     matA.Allocate(3,ELEM_B);
     matB.Allocate(ELEM_B,3);
     matC.Allocate(3, 3);

     for(int i = 0; i != 3*ELEM_B; ++i) {
          matA.SetElement(i, sin(static_cast<double>(i+1)));
          matB.SetElement(i, cos(static_cast<double>(i+1)));
     }

     matC.Dot(matA, matB);

     const double c00 = +0.11871865092994290369;
     const double c01 = -0.12263594200048068732;
     const double c02 = -0.25123961542028522069;

     const double c10 = +0.22690435834954134246;
     const double c11 = +0.26832096951487606806;
     const double c12 = +0.063044518733783381564;

     const double c20 = +0.27260916988833111755;
     const double c21 = +0.58539241368689258801;
     const double c22 = +0.35996857201715691880;

     EXPECT_TRUE((matC(0,0)-c00) < pow(10.0, -14.0));
     EXPECT_TRUE((matC(0,1)-c01) < pow(10.0, -14.0));
     EXPECT_TRUE((matC(0,2)-c02) < pow(10.0, -14.0));

     EXPECT_TRUE((matC(1,0)-c10) < pow(10.0, -14.0));
     EXPECT_TRUE((matC(1,1)-c11) < pow(10.0, -14.0));
     EXPECT_TRUE((matC(1,2)-c12) < pow(10.0, -14.0));

     EXPECT_TRUE((matC(2,0)-c20) < pow(10.0, -14.0));
     EXPECT_TRUE((matC(2,1)-c21) < pow(10.0, -14.0));
     EXPECT_TRUE((matC(2,2)-c22) < pow(10.0, -14.0));
}

//===========//
// test # 42 //
//===========//

TEST(MatrixTest42, Dot)
{
     MatrixDense<double> matA;
     MatrixDense<double> matB;
     MatrixDense<double> matC;

     // # 1

     const int ELEM_B = 100;
     const int ELEM_A = 4;

     matA.Allocate(ELEM_A, ELEM_B);
     matB.Allocate(ELEM_B, ELEM_A);
     matC.Allocate(ELEM_A, ELEM_A);

     for(int i = 0; i != ELEM_A*ELEM_B; ++i) {
          matA.SetElement(i, sin(static_cast<double>(i+1)));
          matB.SetElement(i, cos(static_cast<double>(i+1)));
     }

     matC.Dot(matA, matB);

     const double L1[ELEM_A*ELEM_A] = { +0.51622730506406953160, +0.25199009694310594293,
                                        -0.24392564419547711885, -0.51557727298148133836,
                                        +0.22052996343464981636, +0.54432798041615115680,
                                        +0.36767336250013832031, -0.14701844928591686420,
                                        -0.13589300631484672594, +0.68677848351106999898,
                                        +0.87802900283815731006, +0.26202370619404957496,
                                        -0.45489617134905329685, +0.64011611440927207726,
                                        +1.1466085966264205756,  +0.59891442296170137064
                                      };

     for(int i = 0; i != ELEM_A*ELEM_A; ++i) {
          EXPECT_TRUE((matC(i)-L1[i])< pow(10.0, -14.0));
     }
}

//===========//
// test # 43 //
//===========//

TEST(MatrixTest43, Dot)
{
     MatrixDense<double> matA;
     MatrixDense<double> matB;
     MatrixDense<double> matC;

     // # 1

     const int ELEM_B = 100;
     const int ELEM_A = 5;

     matA.Allocate(ELEM_A, ELEM_B);
     matB.Allocate(ELEM_B, ELEM_A);
     matC.Allocate(ELEM_A, ELEM_A);

     for(int i = 0; i != ELEM_A*ELEM_B; ++i) {
          matA.SetElement(i, sin(static_cast<double>(i+1)));
          matB.SetElement(i, cos(static_cast<double>(i+1)));
     }

     matC.Dot(matA, matB);

     const double L1[ELEM_A*ELEM_A] = {
          1.8093247822323773844, 3.1179100461293086970, 1.5599031925938293423,
          -1.4322714623502710185, -3.1076223400677971249,
          -0.21252966270910540710, 2.5343188543725372619,
          2.9511263043542735377, 0.65468183992893370798,
          -2.2436740889070750855, -2.1758614603823726009,
          1.2528719069105741384, 3.5297206209047655991, 2.5613604741797592636,
          -0.76190268018709425371, -3.5400431388332158227,
          -0.37356867459644443184, 3.1363631062640884729,
          3.7627371113049410746, 0.92966796896327703299,
          -3.9294305542744506020, -1.8971425433105955857,
          1.8793695728519267674, 3.9279979708912292377, 2.3652431493838834522
     };

     for(int i = 0; i != ELEM_A*ELEM_A; ++i) {
          EXPECT_TRUE((matC(i)-L1[i])< pow(10.0, -14.0));
     }
}

//===========//
// test # 44 //
//===========//

TEST(MatrixTest44, Dot)
{
     MatrixDense<double> matA;
     MatrixDense<double> matB;
     MatrixDense<double> matC;

     // # 1

     const int ELEM_B = 100;
     const int ELEM_A = 10;

     matA.Allocate(ELEM_A, ELEM_B);
     matB.Allocate(ELEM_B, ELEM_A);
     matC.Allocate(ELEM_A, ELEM_A);

     for(int i = 0; i != ELEM_A*ELEM_B; ++i) {
          matA.SetElement(i, sin(static_cast<double>(i+1)));
          matB.SetElement(i, cos(static_cast<double>(i+1)));
     }

     matC.Dot(matA, matB);

     const double L1[ELEM_A*ELEM_A] = { 0.17785030569806217747, -0.015946366947653687898,
                                        -0.19508202336213573028, -0.19486016716431478695,
                                        -0.015484771919325156901, 0.17812725121660758328,
                                        0.20796990105989810302, 0.046605982971056055241,
                                        -0.15760726092687242339, -0.21691711597175751476,
                                        -0.070102298261901270629, -0.22838940757629300761,
                                        -0.17669634883675771160, 0.037450518146330250306,
                                        0.21716555145759539915, 0.19721957826899963847,
                                        -0.0040491656554302718384, -0.20159512534994174481,
                                        -0.21379545650127011113, -0.029433230913593862516,
                                        -0.29875137526201731590, -0.37794262581962871918,
                                        -0.10965516917039251528, 0.25944874431338067651,
                                        0.39001667878461847814, 0.16200507743534518666,
                                        -0.21495324498329140821, -0.39428454527196815405,
                                        -0.21111245297393923025, 0.16615545499337100145,
                                        -0.44513559975877362386, -0.42342471019616366048,
                                        -0.012419094802298448472, 0.41000457907920989957,
                                        0.45547193378828473993, 0.082180493088851906161,
                                        -0.36666731396170993043, -0.47840288352882993712,
                                        -0.15029704824747785574, 0.31599120006225527135,
                                        -0.46894628153615769206, -0.35231161137056174654,
                                        0.088236729520888844995, 0.44766062821536094260,
                                        0.39550740982139020493, -0.020273497186496028556,
                                        -0.41741504437714029937, -0.43078712477554551489,
                                        -0.048095509331926224810, 0.37881489558766077242,
                                        -0.36362685755675085491, -0.18418519262567547886,
                                        0.16459548899191096594, 0.36204783710132248396,
                                        0.22663507344892336130, -0.11714493155122548297,
                                        -0.35322242672470840489, -0.26454885173617445569,
                                        0.067349717509061053537, 0.33732726707560147636,
                                        -0.15817832194754566417, 0.034658876176437110572,
                                        0.19563086338140027317, 0.17674073699145405777,
                                        -0.0046440079067661808074, -0.18175907335244533837,
                                        -0.19176568498279894776, -0.025463810212733790418,
                                        0.16424937423454144313, 0.20295244148537730576,
                                        0.090826553152419290416, 0.24395919866412292689,
                                        0.17279688199951912556, -0.057234091081792869413,
                                        -0.23464430477103876263, -0.19632362677144469284,
                                        0.022496088289023535457, 0.22063300352259003311,
                                        0.21592095281871404012, 0.012692173863803962834,
                                        0.31482122372388883760, 0.38608236597606995186,
                                        0.10238116145990637425, -0.27544881074757848853,
                                        -0.40003241665101334073, -0.15682806352951534692,
                                        0.23056328795134884427, 0.40597581578682265905,
                                        0.20813605084128995833, -0.18106303937914800702,
                                        0.45212601207520564973, 0.42189302221316810074,
                                        0.0037735333877003807885, -0.41781532463187824354,
                                        -0.45526670003899871942, -0.074147971000221161078,
                                        0.37514206062527182456, 0.47952821176814082220,
                                        0.14303833646903237153, -0.32496032572461879728
                                      };

     // test

     for(int i = 0; i != ELEM_A*ELEM_A; ++i) {
          EXPECT_TRUE((matC(i)-L1[i])< pow(10.0, -14.0));
     }
}

//===========//
// test # 45 //
//===========//

TEST(MatrixTest45, Dot)
{
     MatrixDense<double> matA;
     MatrixDense<double> matB;
     MatrixDense<double> matC;

     // # 1

     const int ELEM_B = 100;
     const int ELEM_A = 10;

     matA.Allocate(ELEM_A, ELEM_B);
     matB.Allocate(ELEM_B, ELEM_A);
     matC.Allocate(ELEM_A, ELEM_A);

     for(int i = 0; i != ELEM_A*ELEM_B; ++i) {
          matA.SetElement(i, sin(static_cast<double>(i+1)));
          matB.SetElement(i, cos(static_cast<double>(i+1)));
     }

     matC.Dot(matA, matB, "GSL");

     const double L1[ELEM_A*ELEM_A] = { 0.17785030569806217747, -0.015946366947653687898,
                                        -0.19508202336213573028, -0.19486016716431478695,
                                        -0.015484771919325156901, 0.17812725121660758328,
                                        0.20796990105989810302, 0.046605982971056055241,
                                        -0.15760726092687242339, -0.21691711597175751476,
                                        -0.070102298261901270629, -0.22838940757629300761,
                                        -0.17669634883675771160, 0.037450518146330250306,
                                        0.21716555145759539915, 0.19721957826899963847,
                                        -0.0040491656554302718384, -0.20159512534994174481,
                                        -0.21379545650127011113, -0.029433230913593862516,
                                        -0.29875137526201731590, -0.37794262581962871918,
                                        -0.10965516917039251528, 0.25944874431338067651,
                                        0.39001667878461847814, 0.16200507743534518666,
                                        -0.21495324498329140821, -0.39428454527196815405,
                                        -0.21111245297393923025, 0.16615545499337100145,
                                        -0.44513559975877362386, -0.42342471019616366048,
                                        -0.012419094802298448472, 0.41000457907920989957,
                                        0.45547193378828473993, 0.082180493088851906161,
                                        -0.36666731396170993043, -0.47840288352882993712,
                                        -0.15029704824747785574, 0.31599120006225527135,
                                        -0.46894628153615769206, -0.35231161137056174654,
                                        0.088236729520888844995, 0.44766062821536094260,
                                        0.39550740982139020493, -0.020273497186496028556,
                                        -0.41741504437714029937, -0.43078712477554551489,
                                        -0.048095509331926224810, 0.37881489558766077242,
                                        -0.36362685755675085491, -0.18418519262567547886,
                                        0.16459548899191096594, 0.36204783710132248396,
                                        0.22663507344892336130, -0.11714493155122548297,
                                        -0.35322242672470840489, -0.26454885173617445569,
                                        0.067349717509061053537, 0.33732726707560147636,
                                        -0.15817832194754566417, 0.034658876176437110572,
                                        0.19563086338140027317, 0.17674073699145405777,
                                        -0.0046440079067661808074, -0.18175907335244533837,
                                        -0.19176568498279894776, -0.025463810212733790418,
                                        0.16424937423454144313, 0.20295244148537730576,
                                        0.090826553152419290416, 0.24395919866412292689,
                                        0.17279688199951912556, -0.057234091081792869413,
                                        -0.23464430477103876263, -0.19632362677144469284,
                                        0.022496088289023535457, 0.22063300352259003311,
                                        0.21592095281871404012, 0.012692173863803962834,
                                        0.31482122372388883760, 0.38608236597606995186,
                                        0.10238116145990637425, -0.27544881074757848853,
                                        -0.40003241665101334073, -0.15682806352951534692,
                                        0.23056328795134884427, 0.40597581578682265905,
                                        0.20813605084128995833, -0.18106303937914800702,
                                        0.45212601207520564973, 0.42189302221316810074,
                                        0.0037735333877003807885, -0.41781532463187824354,
                                        -0.45526670003899871942, -0.074147971000221161078,
                                        0.37514206062527182456, 0.47952821176814082220,
                                        0.14303833646903237153, -0.32496032572461879728
                                      };

     // test

     for(int i = 0; i != ELEM_A*ELEM_A; ++i) {
          EXPECT_TRUE((matC(i)-L1[i])< pow(10.0, -14.0));
     }
}

//===========//
// test # 46 //
//===========//

TEST(MatrixTest46, Dot)
{
     MatrixDense<double> matA;
     MatrixDense<double> matB;
     MatrixDense<double> matC;

     // # 1

     const int ELEM_B = 100;
     const int ELEM_A = 20;

     matA.Allocate(ELEM_A, ELEM_B);
     matB.Allocate(ELEM_B, ELEM_A);
     matC.Allocate(ELEM_A, ELEM_A);

     for(int i = 0; i != ELEM_A*ELEM_B; ++i) {
          matA.SetElement(i, sin(static_cast<double>(i+1)));
          matB.SetElement(i, cos(static_cast<double>(i+1)));
     }

     matC.Dot(matA, matB, "GSL");

     const double L1[ELEM_A*ELEM_A] = {
          -5.4076380585206169363, -5.0363912418165668001,
          -0.034709543894572461144, 4.9988839486128289774,
          5.4365265923000579562, 0.87585175885353347230,
          -4.4900771424855979744, -5.7278498260751261052,
          -1.6994637949040293599, 3.8914014118229934341, 5.9045301066370248127,
          2.4890610515446813715, -3.2148392554446889859,
          -5.9630311769690391751, -3.2288397343152689756,
          2.4739320695106162977, 5.9021821377507187662, 3.9039931678503028981,
          -1.6835091163847546136, -5.7232008829157375244,
          -5.9124061181381509048, -2.3994197522141690530,
          3.3195820684043989476, 5.9865754443690199776, 3.1495389652879304103,
          -2.5831691136357726643, -5.9409234223774636209,
          -3.8366201345573948465, 1.7950540114944778014, 5.7763637776940359127,
          4.4469113257480942106, -0.97101089110835292680,
          -5.4961901727259346340, -4.9681975565189102719,
          0.12753298113468539657, 5.1060102840815272347, 5.3900452794166831993,
          0.71849750232350351260, -4.6136335648849071550,
          -5.7040112093994253821, -4.7891206940787696247,
          0.89826137206833425203, 5.7597860752803695999, 5.3257900234940382915,
          -0.0047128147536438227307, -5.3308827128510845840,
          -5.7558636293784464459, -0.88893006958038372929,
          4.7952816966788320236, 6.0707335855861002975, 1.7647810125278273885,
          -4.1637030847439092891, -6.2640977678026684745,
          -2.6053098515105879665, 3.4487879272583657376, 6.3320859906063821967,
          3.3936933961015782445, -2.6648452559600606658,
          -6.2733374692555657906, -4.1141521442955038312,
          -2.3470921941970786634, 3.9485952189772763820, 6.6139623977037476820,
          3.1984830498317336712, -3.1576668634952562107,
          -6.6106724248515402826, -3.9858562454771713658,
          2.3035377842710570259, 6.4750697984692463596, 4.6934525012691120636,
          -1.4033033806326679569, -6.2098686060458845751,
          -5.3071092733368559399, 0.47498185028970327397,
          5.8203768512509403518, 5.8145442172151477667, 0.46284644501626323377,
          -5.3143902142048314273, -6.2056010190521579959,
          -1.3914108655782857634, 0.74123690596826855507,
          5.9116349806297164882, 5.6469031170017140280, 0.19043456963030682071,
          -5.4411186428251908549, -6.0701324680714538845,
          -1.1182944960229461929, 4.8616982783897598271, 6.3718680765212507779,
          2.0237717504742800290, -4.1849709898571408211,
          -6.5460707020964487066, -2.8887431995800262547,
          3.4244814785084568620, 6.5892536780617374297, 3.6958964339052981211,
          -2.5954509470840030053, -6.5005526968595659293,
          -4.4290762759771524229, 1.7144724473069087133, 3.6254573399022173782,
          6.2468336007688063780, 3.1248998578377030517, -2.8700524031753404695,
          -6.2262917206337675658, -3.8581071441569231534,
          2.0572033480851097512, 6.0811305693770072894, 4.5140943897541511016,
          -1.2031793539958055866, -5.8142555484278961079,
          -5.0797320054486295376, 0.32507371695572769603,
          5.4310081631452428614, 5.5436987505164016235, 0.55953827273943074680,
          -4.9390591125512206338, -5.8967083274003753892,
          -1.4329511001013477294, 4.3482547602382840394, 5.5113636637749044656,
          4.8618900313378217410, -0.25758287415661014924,
          -5.1402352730557403774, -5.2969790673169164842,
          -0.58370473535745572756, 4.6662250383973452991,
          5.6260490312489246419, 1.4133094906246699586, -4.0988202778692543013,
          -5.8425135855683644893, -2.2146268468277860221,
          3.4493776016112839878, 5.9420401907487663199, 2.9716184316341544332,
          -2.7308956092043692349, -5.9226368211107521455,
          -3.6691330533270073664, 1.9577547226115718839, 5.7846918352295524466,
          5.8796484587251699453, 2.1381654572511191141, -3.5691370049643999913,
          -5.9949913647342618123, -2.9090783110866149664,
          2.8514279258720787246, 5.9903444778175971739, 3.6217659427465714449,
          -2.0766474974562582848, -5.8658008054484077640,
          -4.2619639044376735693, 1.2603029552594979682, 5.6238530900759489228,
          4.8168586296038981526, -0.41873344084428152626,
          -5.2693439168684293240, -5.2753438965482534425,
          -0.43121702623644755247, 4.8093687893379440127,
          5.6282431195355568238, 4.6288999929769105443, -1.1743291796152915296,
          -5.8978855201656762020, -5.1989531129683634755,
          0.27987281009137647368, 5.5013849622526968187, 5.6649491510555052664,
          0.62018521562940324826, -4.9947741469157132292,
          -6.0175611933676638779, -1.5078302300426535299,
          4.3881928930681973572, 6.2497317074805137147, 2.3652960121496978848,
          -3.6937819286301193472, -6.3568137989755334355,
          -3.1754203784916604305, 2.9254398937760832278, 6.3366642190633873995,
          3.9219886843680828471, 2.1035271850274652211, -4.1634578849718775623,
          -6.6025789762978521386, -2.9713194061685842850,
          3.3917575230505767439, 6.6364682274682576127, 3.7796406491929143641,
          -2.5521711112444892414, -6.5375285219438144900,
          -4.5123123589254591846, 1.6615029772943534966, 6.3077401386032963503,
          5.1546701061144069834, -0.73757984995693127810,
          -5.9517022935016199490, -5.6938570960823105755,
          -0.20110594309226477900, 5.4765410865292354868,
          6.1190814975589712082, 1.1357665993231279117, -1.0010776129381703953,
          -6.0061274367571387805, -5.4891713938973927725,
          0.074503513900754878133, 5.5696802346091064953,
          5.9441186335142483985, 0.85356177347394176614,
          -5.0217558446965094575, -6.2800942982666053366,
          -1.7645430161490978569, 4.3733209774088459943, 6.4903738329401099680,
          2.6402069183587093014, -3.6373540610236580873,
          -6.5707484912185586028, -3.4630270611463163330,
          2.8285854783763146204, 6.5196095737700320223, 4.2165346937595810886,
          -1.9632027381474079914, -3.8300234215500445151,
          -6.1949361911891896794, -2.8642531960609755433,
          3.0998109783453217458, 6.2139232347716784472, 3.6149831261241734036,
          -2.3075557973330643758, -6.1085385625610701491,
          -4.2933591443393313167, 1.4691148713478621354, 5.8808914494881819924,
          4.8858035500895203481, -0.60126960122396016496,
          -5.5355382540689653882, -5.3804585645655563226,
          -0.27861008405653697663, 5.0793912228578300057,
          5.7674236642894879770, 1.1529133866103421768, -4.5215801417858601437,
          -5.6043253424747317716, -4.6778933438037052154,
          0.54937222195000296975, 5.2715475004026853050, 5.1470863179719951418,
          0.29041771180255861911, -4.8332595992682525876,
          -5.5132603244904742372, -1.1243949330788102943,
          4.2982339743926065410, 5.7690863881290181281, 1.9358673821246057763,
          -3.6771791672953308077, -5.9094441484845120449,
          -2.7085934323550037677, 2.9825255941630967431, 5.9315243440291330531,
          3.4271069666207941439, -2.2281767509851714322,
          -5.8348850394989299051, -5.8354075969621529895,
          -1.8727352346325583830, 3.8117212658571870700, 5.9916988131710835776,
          2.6629361037902779915, -3.1141177786562695136,
          -6.0280661368961810899, -3.3998382887250421287,
          2.3541852029423202409, 5.9437816759058222986, 4.0686926871951031497,
          -1.5471335943851184263, -5.7405323842597884794,
          -4.6561121738674687657, 0.70911609641716838851,
          5.4223862979122891046, 5.1503395437224624102, 0.14309436504192896842,
          -4.9957111129446793204, -5.5414828325921305846,
          -4.4596388542280430427, 1.4481034725601873771, 6.0244661449479023038,
          5.0619624269196007353, -0.55448620198281238823,
          -5.6611427739263619322, -5.5629707872195278921,
          -0.35022911369726076426, 5.1845115917939581583,
          5.9526362493900109765, 1.2479345912854372936, -4.6041123749017384444,
          -6.2231596565563297857, -2.1206626495441923779,
          3.9315618175219981569, 6.3691264808847322930, 2.9509456304537037640,
          -3.1803210236334374027, -6.3876151953938411609,
          -3.7221654145058830226, -1.8558538982143769258,
          4.3701891414605180532, 6.5783004386364243996, 2.7383526499167920345,
          -3.6192239365760774195, -6.6493027266872330007,
          -3.5660432547127641693, 2.7958199399935682467, 6.5872191754540620522,
          4.3223594795197432371, -1.9164575883030030501,
          -6.3932923876369563081, -4.9921636499559410361,
          0.99873732495234904701, 6.0714038092126045248, 5.5620496309960054469,
          -0.061027327252249757645, -5.6279960422667256611,
          -6.0206111508547026287, -0.87790413281773476580,
          1.2589631729501956920, 6.0888896717360437532, 5.3207190866811738087,
          -0.33929608891512260672, -5.6873640051069380178,
          -5.8064956836263513186, -0.58716200864649777830,
          5.1720057092466086385, 6.1760552299847498495, 1.5018680546128799627,
          -4.5531296839506773795, -6.4220009889233303858,
          -2.3865142012548206193, 3.8431227370732479114, 6.5394103544047251938,
          3.2233942499324756529, -3.0561956624834866681,
          -6.5259333771805450729, -3.9957580407815230556,
          2.2080988109297107435, 4.0271093053146512053, 6.1309398089705844790,
          2.5980125265365064434, -3.3235154914466125618,
          -6.1894186937706831456, -3.3647988930087263196,
          2.5534014924203255942, 6.1240163213324287145, 4.0642387867597418143,
          -1.7321811451623905240, -5.9360417205848507128,
          -4.6823329135605582160, 0.87629118050674164573,
          5.6292572044399717301, 5.2067101152607677913,
          -0.0028622419152510065172, -5.2098030670742929101,
          -5.6268749786030430493, -0.87062398446763407661,
          4.6860746858991027507, 5.6863415365261408689, 4.4847605325343217930,
          -0.84008862253670027357, -5.3925641723146586015,
          -4.9871410911503524550, 0.0034365098380870204591,
          4.9908545995296663843, 5.3897039869188925094, 0.83328438462830074963,
          -4.4892530380017230425, -5.6843919207440655848,
          -1.6533270864707616499, 3.8977990463952540964, 5.8653067116267454339,
          2.4402784354363590409, -3.2283305803736232300,
          -5.9288273487973558211, -3.1783875949249784243,
          2.4942470558360425148, 5.8736824662710432383, 5.7797699371250241877,
          1.6036474808200339676, -4.0468610737516287603,
          -5.9767042201720756588, -2.4115930695499998153,
          3.3707256275850957812, 6.0540147276161190712, 3.1712706065962380671,
          -2.6271250850645158709, -6.0101540891250193224,
          -3.8674751408896378202, 1.8309426161042596409, 5.8460001756763890395,
          4.4862721339427461042, -0.99811381823389807818,
          -5.5648385289640029219, -5.0152763597323366991,
          0.14530776550530031552, 5.1722966012584580845, 5.4438997950824735115
     };

     // test

     for(int i = 0; i != ELEM_A*ELEM_A; ++i) {
          EXPECT_TRUE((matC(i)-L1[i])< pow(10.0, -14.0));
     }
}

//===========//
// test # 47 //
//===========//

TEST(MatrixTest47, Dot)
{
     MatrixDense<double> matA;
     MatrixDense<double> matB;
     MatrixDense<double> matC;

     // # 1

     const int ELEM_B = 100;
     const int ELEM_A = 5;

     matA.Allocate(ELEM_A, ELEM_B);
     matB.Allocate(ELEM_B, ELEM_A+1);
     matC.Allocate(ELEM_A, ELEM_A+1);

     for(int i = 0; i != ELEM_A*ELEM_B; ++i) {
          matA.SetElement(i, sin(static_cast<double>(i+1)));
     }

     for(int i = 0; i != (ELEM_A+1)*ELEM_B; ++i) {
          matB.SetElement(i, cos(static_cast<double>(i+1)));
     }
     matC.Dot(matA, matB);

     const double L1[ELEM_A*(ELEM_A+1)] = {
          0.80750538509088871410, -1.2149727108840668650,
          -2.1204104996059406757, -1.0763526537641409564,
          0.95729885809382712825, 2.1108142146302046840, 1.0545024923016520141,
          -0.94770423869733807185, -2.0785960632000153092,
          -1.2984362531334734211, 0.67549986001840882130,
          2.0283845170965771934, 1.0111294148813366339,
          -0.41947378986742633355, -1.4644147267145725336,
          -1.1629795173148640744, 0.20769369694929817422,
          1.3874142840668329125, 0.68932946125310780189,
          0.22426390783189812063, -0.44698884820395877655,
          -0.70728211859578373954, -0.31730347094925125608,
          0.36440252456809426497, 0.17771419244357664113,
          0.80624779006028928611, 0.69352088769775559661,
          -0.056825920478656246841, -0.75492723943315062065,
          -0.75895193597814474125
     };

     // test

     for(int i = 0; i != ELEM_A*(ELEM_A+1); ++i) {
          EXPECT_TRUE((matC(i)-L1[i])< pow(10.0, -14.0));
     }
}

//===========//
// test # 48 //
//===========//

TEST(MatrixTest48, Dot)
{
     MatrixDense<double> matA;
     MatrixDense<double> matB;
     MatrixDense<double> matC;

     // # 1

     const int ELEM_B = 1000;
     const int ELEM_A = 2;

     matA.Allocate(ELEM_A, ELEM_B);
     matB.Allocate(ELEM_B, ELEM_A+1);
     matC.Allocate(ELEM_A, ELEM_A+1);

     for(int i = 0; i != ELEM_A*ELEM_B; ++i) {
          matA.SetElement(i, sin(static_cast<double>(i+1)));
     }

     for(int i = 0; i != (ELEM_A+1)*ELEM_B; ++i) {
          matB.SetElement(i, cos(static_cast<double>(i+1)));
     }

     matC.Dot(matA, matB);

     const double L1[ELEM_A*(ELEM_A+1)] = {
          0.48862832217863019005, -0.30741794197760175290,
          -0.82082556801010261152, 0.52553473998709032096,
          -0.35930301234050287457, -0.91379923213297504812
     };

     // test

     for(int i = 0; i != ELEM_A*(ELEM_A+1); ++i) {
          EXPECT_TRUE((matC(i)-L1[i])< pow(10.0, -14.0));
     }
}

//===========//
// test # 49 //
//===========//

TEST(MatrixTest49, Dot)
{
     MatrixDense<double> matA;
     MatrixDense<double> matB;
     MatrixDense<double> matC;

     // # 1

     const int ELEM_B = 5000;
     const int ELEM_A = 3;

     matA.Allocate(ELEM_A, ELEM_B);
     matB.Allocate(ELEM_B, ELEM_A+1);
     matC.Allocate(ELEM_A, ELEM_A+1);

     for(int i = 0; i != ELEM_A*ELEM_B; ++i) {
          matA.SetElement(i, sin(static_cast<double>(i+1)));
     }

     for(int i = 0; i != (ELEM_A+1)*ELEM_B; ++i) {
          matB.SetElement(i, cos(static_cast<double>(i+1)));
     }

     matC.Dot(matA, matB);

     const double L1[ELEM_A*(ELEM_A+1)] = {
          0.45023765068265543713, -0.24456853165489390390,
          -0.71451953387450403066, -0.52754457182555188552,
          -0.10478093662647744556, -0.086417901817094359213,
          0.011397353386352278471, 0.098733934447974732708,
          -0.48265025161494171030, 0.21783629337582535372,
          0.71804515484039572643, 0.55808661237959711809
     };

     // test

     for(int i = 0; i != ELEM_A*(ELEM_A+1); ++i) {
          EXPECT_TRUE((matC(i)-L1[i])< pow(10.0, -14.0));
     }
}

//===========//
// test # 50 //
//===========//

TEST(MatrixTest50, Dot)
{
     MatrixDense<double> matA;
     MatrixDense<double> matB;

     // # 1

     const int ELEMS = 10;

     matA.Allocate(ELEMS);
     matB.Allocate(ELEMS);

     // # 2

     for(int i = 0; i != ELEMS*ELEMS; ++i) {
          matA.SetElement(i, sin(static_cast<double>(i+1)));
     }

     for(int i = 0; i != ELEMS*ELEMS; ++i) {
          matB.SetElement(i, cos(static_cast<double>(i+1)));
     }

     matA.Dot(matA, matB);

     const double L1[ELEMS] = {
          0.81425238926196575632, 0.29148629494075228957,
          -0.49927095469106749591, -0.83100079108589484249,
          -0.39871233251284696073, 0.40015040581638350916,
          0.83111670642617471470, 0.49795814003880835750,
          -0.29302084384861834674, -0.81459781523648155026
     };

     // test

     for(int i = 0; i != ELEMS; ++i) {
          EXPECT_TRUE((matA(i)-L1[i])< pow(10.0, -10.0));
     }

     // # 3

     for(int i = 0; i != ELEMS*ELEMS; ++i) {
          matA.SetElement(i, sin(static_cast<double>(i+1)));
     }

     for(int i = 0; i != ELEMS*ELEMS; ++i) {
          matB.SetElement(i, cos(static_cast<double>(i+1)));
     }

     matA.Dot(matA, matB, "BLAS");

     // test

     for(int i = 0; i != ELEMS; ++i) {
          EXPECT_TRUE((matA(i)-L1[i])< pow(10.0, -10.0));
     }

     // # 4

     for(int i = 0; i != ELEMS*ELEMS; ++i) {
          matA.SetElement(i, sin(static_cast<double>(i+1)));
     }

     for(int i = 0; i != ELEMS*ELEMS; ++i) {
          matB.SetElement(i, cos(static_cast<double>(i+1)));
     }

     matA.Dot(matA, matB, "GSL");

     // test

     for(int i = 0; i != ELEMS; ++i) {
          EXPECT_TRUE((matA(i)-L1[i])< pow(10.0, -10.0));
     }
}

//===========//
// test # 51 //
//===========//

TEST(MatrixTest51, Dot)
{
     MatrixDense<double> matA;
     MatrixDense<double> matB;

     // # 1

     const int ELEMS = 200;

     matA.Allocate(ELEMS);
     matB.Allocate(ELEMS);

     // # 2

     for(int i = 0; i != ELEMS*ELEMS; ++i) {
          matA.SetElement(i, sin(static_cast<double>(i+1)));
     }

     for(int i = 0; i != ELEMS*ELEMS; ++i) {
          matB.SetElement(i, cos(static_cast<double>(i+1)));
     }

     matA.Dot(matA, matB);

     const double L1[ELEMS] = {
          -0.804158, -0.0518092, 0.748173, 0.860288, 0.181459, -0.664203,
          -0.899199, -0.307476, 0.566939, 0.920113, 0.427339, -0.458329,
          -0.922611, -0.538649, 0.340544, 0.906643, 0.639178, -0.215944,
          -0.872528, -0.726914, 0.0870214, 0.82095, 0.800101, 0.0436428,
          -0.75294, -0.857274, -0.173433, 0.669861, 0.897288, 0.299753,
          -0.573374, -0.919343, -0.420073, 0.465411, 0.922998, 0.531985,
          -0.348132, -0.908178, -0.633249, 0.223886, 0.875182, 0.721839,
          -0.0951589, -0.824668, -0.795981, -0.035473, 0.757649, 0.854192,
          0.165395, -0.675466, -0.895306, -0.292006, 0.579763, 0.918501,
          0.412773, -0.472456, -0.923312, -0.525279, 0.355693, 0.909642,
          0.62727, -0.231811, -0.877766, -0.716707, 0.103289, 0.828322, 0.7918,
          0.0273004, -0.762299, -0.851044, -0.157343, 0.681018, 0.893254,
          0.284237, -0.586107, -0.917587, -0.405441, 0.479465, 0.923553,
          0.518531, -0.363226, -0.911035, -0.621242, 0.239717, 0.880282,
          0.71152, -0.111411, -0.831911, -0.787556, -0.0191256, 0.766888,
          0.847829, 0.149279, -0.686517, -0.891133, -0.276445, 0.592405,
          0.9166, 0.398078, -0.486436, -0.923722, -0.511743, 0.37073, 0.912356,
          0.615166, -0.247605, -0.882729, -0.706276, 0.119524, 0.835434,
          0.78325, 0.0109494, -0.771418, -0.844547, -0.141204, 0.691962,
          0.888941, 0.268632, -0.598656, -0.915542, -0.390683, 0.493368,
          0.923819, 0.504915, -0.378206, -0.913606, -0.609041, 0.255474,
          0.885107, 0.700977, -0.127628, -0.838892, -0.778883, -0.00277231,
          0.775887, 0.8412, 0.133117, -0.697353, -0.88668, -0.260797, 0.604861,
          0.914413, 0.383258, -0.500263, -0.923844, -0.498047, 0.385652,
          0.914784, 0.602868, -0.263322, -0.887415, -0.695623, 0.135722,
          0.842285, 0.774455, -0.005405, -0.780296, -0.837786, -0.12502,
          0.702689, 0.884349, 0.252942, -0.611018, -0.913211, -0.375802,
          0.507118, 0.923796, 0.491141, -0.393067, -0.915891, -0.596649,
          0.27115, 0.889654, 0.690215, -0.143805, -0.845611, -0.769966,
          0.0135819, 0.784643, 0.834307, 0.116913, -0.70797, -0.881949,
          -0.245068, 0.617128, 0.911939, 0.368318, -0.513933, -0.923676,
          -0.484195, 0.400452, 0.916926, 0.590382, -0.278956, -0.891823,
          -0.684752, 0.151877, 0.848871, 0.765417, -0.0217577
     };

     // test

     for(int i = 0; i != ELEMS; ++i) {
          EXPECT_TRUE((matA(i)-L1[i])< pow(10.0, -6.0));
     }

     // # 3

     for(int i = 0; i != ELEMS*ELEMS; ++i) {
          matA.SetElement(i, sin(static_cast<double>(i+1)));
     }

     for(int i = 0; i != ELEMS*ELEMS; ++i) {
          matB.SetElement(i, cos(static_cast<double>(i+1)));
     }

     matA.Dot(matA, matB, "BLAS");

     // test

     for(int i = 0; i != ELEMS; ++i) {
          EXPECT_TRUE((matA(i)-L1[i])< pow(10.0, -6.0));
     }

     // # 4

     for(int i = 0; i != ELEMS*ELEMS; ++i) {
          matA.SetElement(i, sin(static_cast<double>(i+1)));
     }

     for(int i = 0; i != ELEMS*ELEMS; ++i) {
          matB.SetElement(i, cos(static_cast<double>(i+1)));
     }

     matA.Dot(matA, matB, "GSL");

     // test

     for(int i = 0; i != ELEMS; ++i) {
          EXPECT_TRUE((matA(i)-L1[i])< pow(10.0, -6.0));
     }
}

//===========//
// test # 52 //
//===========//

TEST(MatrixTest52, op_equal)
{
     MatrixDense<double> matA;
     MatrixDense<double> matB;
     MatrixDense<double> matC;

     const int ELEMS = 1000;

     matA.Allocate(ELEMS);
     matB.Allocate(ELEMS);
     matC.Allocate(ELEMS);

     matA = 10.0;
     matB = matA;
     matC = matB;

     // # 1

     EXPECT_TRUE(matA == matB);
     EXPECT_TRUE(matB == matC);
}

//===========//
// test # 53 //
//===========//

TEST(MatrixTest53, op_equal)
{
     MatrixDense<double> matA;
     MatrixDense<double> matB;
     MatrixDense<double> matC;

     const int ELEMS = 1000;

     matA.Allocate(ELEMS);
     matB.Allocate(ELEMS);
     matC.Allocate(ELEMS);

     matA = 11.0;
     matB = 12.0;
     matC = 13.0;

     // # 1

     EXPECT_TRUE(matA == 11.0);
     EXPECT_TRUE(matB == 12.0);
     EXPECT_TRUE(matC == 13.0);
}

//===========//
// test # 54 //
//===========//

TEST(MatrixTest54, op_plus)
{
     MatrixDense<double> matA;
     MatrixDense<double> matB;
     MatrixDense<double> matC;

     const int ELEMS = 1000;

     matA.Allocate(ELEMS);
     matB.Allocate(ELEMS);
     matC.Allocate(ELEMS);

     matA = 11.0;
     matB = 12.0;
     matC = 13.0;

     // # 1

     EXPECT_TRUE(matA+matB == 11.0+12.0);
     EXPECT_TRUE(matB+matA == 12.0+11.0);
     EXPECT_TRUE(matC+matA+matB == 13.0+12.0+11.0);
}

//===========//
// test # 55 //
//===========//

TEST(MatrixTest55, op_times)
{
     MatrixDense<double> matA;
     MatrixDense<double> matB;
     MatrixDense<double> matC;

     const int ELEMS = 1000;

     matA.Allocate(ELEMS);
     matB.Allocate(ELEMS);
     matC.Allocate(ELEMS);

     matA = 11.0;
     matB = 12.0;
     matC = 13.0;

     // # 1

     EXPECT_TRUE(matA*matB == 11.0*12.0);
     EXPECT_TRUE(matB*matA == 12.0*11.0);
     EXPECT_TRUE(matC*matA*matB == 13.0*12.0*11.0);
}

//===========//
// test # 56 //
//===========//

TEST(MatrixTest56, op_divide)
{
     MatrixDense<double> matA;
     MatrixDense<double> matB;
     MatrixDense<double> matC;

     const int ELEMS = 1000;

     matA.Allocate(ELEMS);
     matB.Allocate(ELEMS);
     matC.Allocate(ELEMS);

     matA = 11.0;
     matB = 12.0;
     matC = 13.0;

     // # 1

     EXPECT_TRUE(matA/matB == 11.0/12.0);
     EXPECT_TRUE(matB/matA == 12.0/11.0);
     EXPECT_TRUE(matC/matA/matB == 13.0/11.0/12.0);
}

//===========//
// test # 57 //
//===========//

TEST(MatrixTest57, op_subtract)
{
     MatrixDense<double> matA;
     MatrixDense<double> matB;
     MatrixDense<double> matC;

     const int ELEMS = 1000;

     matA.Allocate(ELEMS);
     matB.Allocate(ELEMS);
     matC.Allocate(ELEMS);

     matA = 11.0;
     matB = 12.0;
     matC = 13.0;

     // # 1

     EXPECT_TRUE(matA-matB == 11.0-12.0);
     EXPECT_TRUE(matB-matA == 12.0-11.0);
     EXPECT_TRUE(matC-matA-matB == 13.0-11.0-12.0);
}

//===========//
// test # 58 //
//===========//

TEST(MatrixTest58, op_all)
{
     MatrixDense<double> matA;
     MatrixDense<double> matB;
     MatrixDense<double> matC;

     const int ELEMS = 1000;

     matA.Allocate(ELEMS);
     matB.Allocate(ELEMS);
     matC.Allocate(ELEMS);

     matA = 11.0;
     matB = 12.0;
     matC = 13.0;

     // # 1

     EXPECT_TRUE(matA-matB+matA/matC*matB == 11.0-12.0+11.0/13.0*12.0);
}

//===========//
// test # 59 //
//===========//

TEST(MatrixTest59, op_all)
{
     MatrixDense<double> matA;
     MatrixDense<double> matB;
     MatrixDense<double> matC;

     const int ELEMS = 1000;

     matA.Allocate(ELEMS);
     matB.Allocate(ELEMS);
     matC.Allocate(ELEMS);

     matA = 11.0;
     matB = 12.0;
     matC = 13.0;

     matA = matA/matA/matA/matA/matA;
     matB = matB/matB/matB/matB/matB;
     matC = matC/matC/matC/matC/matC;

     // # 1

     EXPECT_TRUE(matA == 11.0/11.0/11.0/11.0/11.0);
     EXPECT_TRUE(matB == 12.0/12.0/12.0/12.0/12.0);
     EXPECT_TRUE(matC == 13.0/13.0/13.0/13.0/13.0);
}

//===========//
// test # 60 //
//===========//

TEST(MatrixTest60, op_various)
{
     MatrixDense<double> matA;
     MatrixDense<double> matB;
     MatrixDense<double> matC;

     const int ELEMS = 1000;

     matA.Allocate(ELEMS);
     matB.Allocate(ELEMS);
     matC.Allocate(ELEMS);

     matA = 11.0;
     matB = 12.0;
     matC = 13.0;

     matA = matA/matA/matA/matA/matA;
     matB = matB/matB/matB/matB/matB;
     matC = matC/matC/matC/matC/matC;

     // # 1

     EXPECT_FALSE(matA != 11.0/11.0/11.0/11.0/11.0);
     EXPECT_FALSE(matB != 12.0/12.0/12.0/12.0/12.0);
     EXPECT_FALSE(matC != 13.0/13.0/13.0/13.0/13.0);
}

//===========//
// test # 61 //
//===========//

TEST(MatrixTest61, op_various)
{
     MatrixDense<double> matA;
     MatrixDense<double> matB;
     MatrixDense<double> matC;

     const int ELEMS = 1000;

     matA.Allocate(ELEMS);
     matB.Allocate(ELEMS);
     matC.Allocate(ELEMS);

     matA = 11.0;
     matB = 12.0;
     matC = 13.0;

     matA += matA;
     matB += matB;
     matC += matC;

     // # 1

     EXPECT_TRUE(matA == 11.0+11.0);
     EXPECT_TRUE(matB == 12.0+12.0);
     EXPECT_TRUE(matC == 13.0+13.0);

     EXPECT_FALSE(matA != 11.0+11.0);
     EXPECT_FALSE(matB != 12.0+12.0);
     EXPECT_FALSE(matC != 13.0+13.0);
}

//===========//
// test # 62 //
//===========//

TEST(MatrixTest62, op_various)
{
     MatrixDense<double> matA;
     MatrixDense<double> matB;
     MatrixDense<double> matC;

     const int ELEMS = 1000;

     matA.Allocate(ELEMS);
     matB.Allocate(ELEMS);
     matC.Allocate(ELEMS);

     matA = 11.0;
     matB = 12.0;
     matC = 13.0;

     matA /= matA;
     matB /= matB;
     matC /= matC;

     // # 1

     EXPECT_TRUE(matA == 11.0/11.0);
     EXPECT_TRUE(matB == 12.0/12.0);
     EXPECT_TRUE(matC == 13.0/13.0);

     EXPECT_FALSE(matA != 11.0/11.0);
     EXPECT_FALSE(matB != 12.0/12.0);
     EXPECT_FALSE(matC != 13.0/13.0);
}

//===========//
// test # 63 //
//===========//

TEST(MatrixTest63, op_various)
{
     MatrixDense<double> matA;
     MatrixDense<double> matB;
     MatrixDense<double> matC;

     const int ELEMS = 1000;

     matA.Allocate(ELEMS);
     matB.Allocate(ELEMS);
     matC.Allocate(ELEMS);

     matA = 11.0;
     matB = 12.0;
     matC = 13.0;

     matA *= matA;
     matB *= matB;
     matC *= matC;

     // # 1

     EXPECT_TRUE(matA == 11.0*11.0);
     EXPECT_TRUE(matB == 12.0*12.0);
     EXPECT_TRUE(matC == 13.0*13.0);

     EXPECT_FALSE(matA != 11.0*11.0);
     EXPECT_FALSE(matB != 12.0*12.0);
     EXPECT_FALSE(matC != 13.0*13.0);
}

//===========//
// test # 64 //
//===========//

TEST(MatrixTest64, op_various)
{
     MatrixDense<double> matA;
     MatrixDense<double> matB;
     MatrixDense<double> matC;

     const int ELEMS = 1000;

     matA.Allocate(ELEMS);
     matB.Allocate(ELEMS);
     matC.Allocate(ELEMS);

     matA = 11.0;
     matB = 12.0;
     matC = 13.0;

     matA -= matA;
     matB -= matB;
     matC -= matC;

     // # 1

     EXPECT_TRUE(matA == 11.0-11.0);
     EXPECT_TRUE(matB == 12.0-12.0);
     EXPECT_TRUE(matC == 13.0-13.0);

     EXPECT_FALSE(matA != 11.0-11.0);
     EXPECT_FALSE(matB != 12.0-12.0);
     EXPECT_FALSE(matC != 13.0-13.0);
}

//===========//
// test # 65 //
//===========//

TEST(MatrixTest65, op_various)
{
     MatrixDense<double> matA;
     MatrixDense<double> matB;
     MatrixDense<double> matC;

     const int ELEMS = 1000;

     matA.Allocate(ELEMS);
     matB.Allocate(ELEMS);
     matC.Allocate(ELEMS);

     matA = 11.0;
     matB = 12.0;
     matC = 13.0;

     matA++;
     ++matB;
     matC++;

     // # 1

     EXPECT_TRUE(matA == 12.0);
     EXPECT_TRUE(matB == 13.0);
     EXPECT_TRUE(matC == 14.0);

     EXPECT_FALSE(matA != 12.0);
     EXPECT_FALSE(matB != 13.0);
     EXPECT_FALSE(matC != 14.0);

     // # 2

     matA--;
     --matB;
     matC--;

     EXPECT_TRUE(matA == 11.0);
     EXPECT_TRUE(matB == 12.0);
     EXPECT_TRUE(matC == 13.0);

     EXPECT_FALSE(matA != 11.0);
     EXPECT_FALSE(matB != 12.0);
     EXPECT_FALSE(matC != 13.0);

     // # 3

     (matA--);
     (--matB);
     (matC--);

     EXPECT_TRUE(matA == 10.0);
     EXPECT_TRUE(matB == 11.0);
     EXPECT_TRUE(matC == 12.0);

     EXPECT_FALSE(matA != 10.0);
     EXPECT_FALSE(matB != 11.0);
     EXPECT_FALSE(matC != 12.0);
}

//===========//
// test # 66 //
//===========//

TEST(MatrixTest66, op_various)
{
     MatrixDense<double> matA;

     const int ELEMS = 1000;

     matA.Allocate(ELEMS);

     // # 1

     matA.Range(10);

     for (int i = 0; i != ELEMS*ELEMS; ++i) {
          EXPECT_EQ(matA(i), 10+static_cast<double>(i));
     }

     // # 2

     matA.Range(21.1);

     for (int i = 0; i != ELEMS*ELEMS; ++i) {
          EXPECT_EQ(matA(i), 21.1+static_cast<double>(i));
     }

     // # 3

     EXPECT_EQ(matA[0], 21.1);
     EXPECT_EQ(matA[1], 21.1+ELEMS);
     EXPECT_EQ(matA[2], 21.1+2*ELEMS);
     EXPECT_EQ(matA[3], 21.1+3*ELEMS);

     int j = 0;

     for(int i = 0; i != ELEMS*ELEMS; ++i) {
          EXPECT_EQ(matA[i], 21.1+j*ELEMS+i/ELEMS);
          j = j + 1;
          if (j%ELEMS == 0) {
               j = 0;
          }
     }
}

//===========//
// test # 67 //
//===========//

TEST(MatrixTest67, SlowWayA)
{
     // local parameters

     const auto DIM = 1 *static_cast<pgg::MAI_MAX>(pow(10.0, 2.0));
     const auto K_MAX_LOC = 10;
     const auto ONE_DOUBLE = 1.0;
     const auto TWO_DOUBLE = 2.0;
     const auto THREE_DOUBLE = 3.0;

     MatrixDense<double> matA;
     MatrixDense<double> matB;
     MatrixDense<double> matC;

     // allocate heap space

     matA.Allocate(DIM);
     matB.Allocate(DIM);
     matC.Allocate(DIM);

     // build matrices

     matA = ONE_DOUBLE;
     matB = TWO_DOUBLE;
     matC = THREE_DOUBLE;

     // test equality

     EXPECT_TRUE(matA == ONE_DOUBLE);
     EXPECT_TRUE(matB == TWO_DOUBLE);
     EXPECT_TRUE(matC == THREE_DOUBLE);

     // add matrices

     for (int i = 0; i != K_MAX_LOC; ++i) {
          matC = matA + matB;
     }

     EXPECT_TRUE(matA == ONE_DOUBLE);
     EXPECT_TRUE(matB == TWO_DOUBLE);
     EXPECT_TRUE(matC == THREE_DOUBLE);

     // subtract matrices

     for (int i = 0; i != K_MAX_LOC; ++i) {
          matC = matA - matB;
     }

     EXPECT_TRUE(matA == ONE_DOUBLE);
     EXPECT_TRUE(matB == TWO_DOUBLE);
     EXPECT_TRUE(matC == -ONE_DOUBLE);

     // multiply matrices

     for (int i = 0; i != K_MAX_LOC; ++i) {
          matC = matA * matB;
     }

     EXPECT_TRUE(matA == ONE_DOUBLE);
     EXPECT_TRUE(matB == TWO_DOUBLE);
     EXPECT_TRUE(matC == TWO_DOUBLE);

     // divide matrices

     for (int i = 0; i != K_MAX_LOC; ++i) {
          matC = matA / matB;
     }

     EXPECT_TRUE(matA == ONE_DOUBLE);
     EXPECT_TRUE(matB == TWO_DOUBLE);
     EXPECT_TRUE(matC == (ONE_DOUBLE/TWO_DOUBLE));

     // swap matrices

     for (int i = 0; i != K_MAX_LOC; ++i) {

          matA = ONE_DOUBLE;
          matB = TWO_DOUBLE;
          matC = THREE_DOUBLE;

          matA = matB;
          matB = matC;
          matC = ONE_DOUBLE;
     }

     EXPECT_TRUE(matA == TWO_DOUBLE);
     EXPECT_TRUE(matB == THREE_DOUBLE);
     EXPECT_TRUE(matC == ONE_DOUBLE);

     // advance matrices

     matA = ONE_DOUBLE;
     matB = TWO_DOUBLE;
     matC = THREE_DOUBLE;

     for (int i = 0; i != K_MAX_LOC; ++i) {
          matA = matA + ONE_DOUBLE;
          matB = matB + ONE_DOUBLE;
          matC = matC + ONE_DOUBLE;
     }

     EXPECT_TRUE(matA == (ONE_DOUBLE+ONE_DOUBLE*K_MAX_LOC));
     EXPECT_TRUE(matB == (TWO_DOUBLE+ONE_DOUBLE*K_MAX_LOC));
     EXPECT_TRUE(matC == (THREE_DOUBLE+ONE_DOUBLE*K_MAX_LOC));

     // advance matrices

     matA = ONE_DOUBLE;
     matB = TWO_DOUBLE;
     matC = THREE_DOUBLE;

     for (int i = 0; i != K_MAX_LOC; ++i) {
          matA = matA - ONE_DOUBLE;
          matB = matB - ONE_DOUBLE;
          matC = matC - ONE_DOUBLE;
     }

     EXPECT_TRUE(matA == (ONE_DOUBLE-ONE_DOUBLE*K_MAX_LOC));
     EXPECT_TRUE(matB == (TWO_DOUBLE-ONE_DOUBLE*K_MAX_LOC));
     EXPECT_TRUE(matC == (THREE_DOUBLE-ONE_DOUBLE*K_MAX_LOC));

     // advance matrices

     matA = ONE_DOUBLE;
     matB = TWO_DOUBLE;
     matC = THREE_DOUBLE;

     for (int i = 0; i != K_MAX_LOC; ++i) {
          matA = matA * ONE_DOUBLE;
          matB = matB * ONE_DOUBLE;
          matC = matC * ONE_DOUBLE;
     }

     EXPECT_TRUE(matA == ONE_DOUBLE);
     EXPECT_TRUE(matB == TWO_DOUBLE);
     EXPECT_TRUE(matC == THREE_DOUBLE);

     // advance matrices

     matA = ONE_DOUBLE;
     matB = TWO_DOUBLE;
     matC = THREE_DOUBLE;

     for (int i = 0; i != K_MAX_LOC; ++i) {
          matA = matA / ONE_DOUBLE;
          matB = matB / ONE_DOUBLE;
          matC = matC / ONE_DOUBLE;
     }

     EXPECT_TRUE(matA == ONE_DOUBLE);
     EXPECT_TRUE(matB == TWO_DOUBLE);
     EXPECT_TRUE(matC ==THREE_DOUBLE);

     // advance matrices

     matA = ONE_DOUBLE;
     matB = TWO_DOUBLE;
     matC = THREE_DOUBLE;

     for (int i = 0; i != K_MAX_LOC; ++i) {
          matA = matA + matA;
          matB = matB + matB;
          matC = matC + matC;
     }

     EXPECT_TRUE(matA == 1.0*pow(TWO_DOUBLE, K_MAX_LOC));
     EXPECT_TRUE(matB == 2.0*pow(TWO_DOUBLE, K_MAX_LOC));
     EXPECT_TRUE(matC == 3.0*pow(TWO_DOUBLE, K_MAX_LOC));

     // advance matrices

     matA = ONE_DOUBLE;
     matB = TWO_DOUBLE;
     matC = THREE_DOUBLE;

     for (int i = 0; i != K_MAX_LOC; ++i) {
          matA = matA - matA;
          matB = matB - matB;
          matC = matC - matC;
     }

     EXPECT_TRUE(matA == pgg::ZERO_DBL);
     EXPECT_TRUE(matB == pgg::ZERO_DBL);
     EXPECT_TRUE(matC == pgg::ZERO_DBL);

     // advance matrices

     matA = ONE_DOUBLE;
     matB = TWO_DOUBLE;
     matC = THREE_DOUBLE;

     for (int i = 0; i != K_MAX_LOC; ++i) {
          matA = matA / matA;
          matB = matB / matB;
          matC = matC / matC;
     }

     EXPECT_TRUE(matA == pgg::ONE_DBL);
     EXPECT_TRUE(matB == pgg::ONE_DBL);
     EXPECT_TRUE(matC == pgg::ONE_DBL);

     // advance matrices

     matA = ONE_DOUBLE;
     matB = TWO_DOUBLE;
     matC = THREE_DOUBLE;

     for (int i = 0; i != K_MAX_LOC; ++i) {
          matA = matA * matA;
          matB = matB * matB;
          matB = matB / TWO_DOUBLE;
          matC = matC * matC;
          matC = matC / THREE_DOUBLE;
     }

     EXPECT_TRUE(matA == ONE_DOUBLE);
     EXPECT_TRUE(matB == TWO_DOUBLE);
     EXPECT_TRUE(matC == THREE_DOUBLE);

     // deallocate heap space

     matA.Deallocate();
     matB.Deallocate();
     matC.Deallocate();
}

//===========//
// test # 68 //
//===========//

TEST(MatrixTest68, GreaterAll)
{
     MatrixDense<double> matA;
     MatrixDense<double> matB;

     const int ELEM = 100;

     matA.Allocate(ELEM);
     matB.Allocate(ELEM);

     matA.Range(1.0, 2.0);
     matB.Range(2.0, 4.0);

     EXPECT_TRUE(matB > matA);
     EXPECT_TRUE(matB >= matA);
}

//===========//
// test # 69 //
//===========//

TEST(MatrixTest69, LessAll)
{
     MatrixDense<double> matA;
     MatrixDense<double> matB;

     const int ELEM = 100;

     matA.Allocate(ELEM);
     matB.Allocate(ELEM);

     matB.Range(1.0, 2.0);
     matA.Range(2.0, 4.0);

     EXPECT_TRUE(matB < matA);
     EXPECT_TRUE(matB <= matA);
}

//===========//
// test # 70 //
//===========//

TEST(MatrixTest70, GreaterAll)
{
     MatrixDense<double> matA;

     const int ELEM = 100;

     matA.Allocate(ELEM);

     matA.Range(1.0, 2.0);

     EXPECT_TRUE(matA >= -1.0);
     EXPECT_TRUE(matA >= 1.0);
}

//===========//
// test # 71 //
//===========//

TEST(MatrixTest71, LessAllA)
{
     MatrixDense<double> matA;

     const int ELEM = 100;

     matA.Allocate(ELEM);

     matA.Range(3.0, 4.0);

     EXPECT_TRUE(matA < 4.001);
     EXPECT_TRUE(matA <= 4.0);

     matA.Set(10.0);

     EXPECT_TRUE(matA <= 10.0);
     EXPECT_TRUE(matA >= 10.0);
     EXPECT_TRUE(matA == 10.0);
}

//===========//
// test # 72 //
//===========//

TEST(MatrixTest72, LessAllB)
{
     MatrixDense<double> matA;
     MatrixDense<double> matB;

     const int ELEM = 100;

     matA.Allocate(ELEM);
     matB.Allocate(ELEM);

     matA.Range(3.0, 4.0);
     matB.Range(3.0, 5.0);

     EXPECT_TRUE(matA <= matB);
     EXPECT_FALSE(matA < matB);

     EXPECT_TRUE(matA >= 3.0);
     EXPECT_FALSE(matA > 3.0);
     EXPECT_FALSE(matA < 3.0);
     EXPECT_FALSE(matA <= 3.0);

     EXPECT_TRUE(matA != matB);
     EXPECT_FALSE(matA == matB);
}

//===========//
// test # 73 //
//===========//

TEST(MatrixTest73, LessAllC)
{
     MatrixDense<std::complex<double>> matA;
     MatrixDense<std::complex<double>> matB;

     const int ELEM = 100;

     matA.Allocate(ELEM);
     matB.Allocate(ELEM);

     matA = {3.0, 4.0};
     matB = {3.0, 5.0};

     EXPECT_TRUE(matA <= matB);
     EXPECT_FALSE(matA < matB);

     EXPECT_TRUE(matA >= 3.0);
     EXPECT_FALSE(matA > 3.0);
     EXPECT_FALSE(matA < 3.0);
     EXPECT_FALSE(matA <= 3.0);

     EXPECT_TRUE(matA != matB);
     EXPECT_FALSE(matA == matB);
}

//===========//
// test # 74 //
//===========//

TEST(MatrixTest74, LessAllD)
{
     MatrixDense<std::complex<long double>> matA;
     MatrixDense<std::complex<long double>> matB;

     const int ELEM = 100;

     matA.Allocate(ELEM);
     matB.Allocate(ELEM);

     matA = {3.0L, 4.0L};
     matB = {3.0L, 5.0L};

     EXPECT_TRUE(matA <= matB);
     EXPECT_FALSE(matA < matB);

     EXPECT_TRUE(matA >= 3.0L);
     EXPECT_FALSE(matA > 3.0L);
     EXPECT_FALSE(matA < 3.0L);
     EXPECT_FALSE(matA <= 3.0L);

     EXPECT_TRUE(matA != matB);
     EXPECT_FALSE(matA == matB);
}

//===========//
// test # 75 //
//===========//

TEST(MatrixTest75, sinSin)
{
     MatrixDense<long double> matA;
     MatrixDense<long double> matB;

     const int ELEM = 100;
     const auto VAL_A = 11.11111L;
     const auto VAL_B = 12.345678901234567890123456789L;

     matA.Allocate(ELEM);
     matB.Allocate(ELEM);

     matA.Set(VAL_A);
     matB.Set(VAL_B);

     EXPECT_EQ(matA, VAL_A);
     EXPECT_EQ(matB, VAL_B);

     matA.Set(sin(VAL_A));
     matB.Set(pgg::functions::Sin<long double>(VAL_A));

     EXPECT_TRUE(matA.Equal(matA, std::pow(10.0, -14.0)));
     EXPECT_TRUE(abs(sin(VAL_A)-pgg::functions::Sin<long double>(VAL_A)) < std::pow(10.0, -14.0));
     EXPECT_EQ(matB, pgg::functions::Sin<long double>(VAL_A));
     EXPECT_EQ(matA, sin(VAL_A));
     EXPECT_TRUE(matA.Equal(matB));
     EXPECT_TRUE(abs(sin(VAL_A)-pgg::functions::Sin<long double>(VAL_A)) < std::pow(10.0, -18.0));
     EXPECT_EQ(matA, pgg::functions::Sin<long double>(VAL_A));
     EXPECT_EQ(matB, sin(VAL_A));
}

//===========//
// test # 76 //
//===========//

TEST(MatrixTest76, cosCos)
{
     MatrixDense<long double> matA;
     MatrixDense<long double> matB;

     const int ELEM = 100;
     const auto VAL_A = 11.0L;
     const auto VAL_B = 12.0L;

     matA.Allocate(ELEM);
     matB.Allocate(ELEM);

     matA.Set(VAL_A);
     matB.Set(VAL_B);

     EXPECT_EQ(matA, VAL_A);
     EXPECT_EQ(matB, VAL_B);

     matA.Set(cos(VAL_A));
     matB.Set(pgg::functions::Cos<long double>(VAL_A));

     EXPECT_TRUE(matA.Equal(matB, std::pow(10.0, -14.0)));
     EXPECT_TRUE(matA.Equal(matB, std::pow(10.0, -17.0)));
     EXPECT_TRUE(abs(cos(VAL_A)-pgg::functions::Cos(VAL_A)) <= std::pow(10.0, -14.0));
     EXPECT_EQ(matB, pgg::functions::Cos<long double>(VAL_A));
     EXPECT_EQ(matA, cos(VAL_A));
}

//===========//
// test # 77 //
//===========//

TEST(MatrixTest77, EigensystemDSYEVD1)
{
     MatrixDense<double> matA;
     MatrixDense<double> eigenvectorsA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 5) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 5) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     matA.EigensystemDSYEVD(eigenvectorsA, eigenvaluesA);

     // test

     const double arr_values[100] = {-323.38478082263765, -318.95649570052416, -259.5812044929057,
                                     -255.47902023149175, -210.8574832531544, -207.046095983574,
                                     -172.17668982103106, -169.63822144706373, -167.11362728657673,
                                     -164.65335127248554, -137.93078894722365, -135.63554168942136,
                                     -133.27655590262873, -130.90313825750582, -111.16491214977447,
                                     -108.69201485977163, -104.86333072392988, -102.09654131372476,
                                     -89.52930445237439, -87.11011352113846, -79.24098389717554,
                                     -76.62676551546362, -71.01471245774239, -68.78074078937429,
                                     -57.39730933358366, -55.597048251741185, -53.807888693814036,
                                     -52.082337027364105, -42.001162570408376, -40.10702874995469,
                                     -36.13941170041208, -34.10866137118017, -29.537625276581757,
                                     -27.807512581350473, -20.493485216120316, -19.180726411630474,
                                     -17.86587524596272, -16.594313970872253, -10.527129255160773,
                                     -9.235915164234662, -6.274431175607891, -4.736234460618425,
                                     -3.113902006580256, -2.3717281394985243, -0.07129875210819334,
                                     2.4178759119876623, 3.3310614360036803, 4.154955748257782,
                                     8.633411865685325, 10.366556352869488, 12.095377640289747,
                                     14.018873556316715, 17.48114915683801, 21.917608573061386,
                                     23.316848960689295, 27.074595655585338, 31.322004721507344,
                                     34.9238763246869, 36.70168716147582, 39.12924266179792,
                                     48.291435480841905, 50.073858853444456, 52.12562986320187,
                                     57.27293729356433, 64.70819943681298, 66.93714122637961,
                                     68.82639047739008, 80.9665198923797, 83.5742202822415,
                                     86.58950836310686, 89.44951473838972, 102.06705367044331,
                                     107.13994334023343, 109.44821746324266, 117.55286230732882,
                                     126.41194103599305, 133.5454916018076, 135.893724579225,
                                     151.52771532059967, 157.8972863072518, 165.22044899155756,
                                     167.77180305861472, 190.84742611813815, 205.77684982675112,
                                     208.5174101551291, 234.35823130936174, 279.92197126989515,
                                     330.06056030550724, 384.6942438609073, 444.0868835363212,
                                     508.62422446622827, 578.8097468653175, 655.2951900145777,
                                     738.9379634908137, 830.8998363959786, 932.8239020042909,
                                     1047.1805607335104, 1178.0384559097015, 1333.1755507750008,
                                     1532.5774637949444
                                    };

     // test

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -10.0));
     }
}

//===========//
// test # 78 //
//===========//

TEST(MatrixTest78, EigensystemDSYEVD2)
{
     MatrixDense<double> matA;
     MatrixDense<double> eigenvectorsA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 20) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 20) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     matA.EigensystemDSYEVD(eigenvectorsA, eigenvaluesA);

     // test

     const double arr_values[100] = {-890.0679762687605,-833.96613152625,-387.9183497645402,
                                     -374.28858505936836,-325.97417429278767,-290.39317003462327,
                                     -243.09349980476634,-237.9671765884564,-184.65425879239723,
                                     -181.6433889406905,-152.42678092373745,-150.01440312367828,
                                     -140.20043836510496,-134.26239413119825,-130.50508513840435,
                                     -128.1365612192653,-119.6243787096752,-117.25234191992651,
                                     -112.47792700587912,-108.89915545206071,-104.74759545668331,
                                     -100.22968339474058,-94.96995783385485,-89.69665862307328,
                                     -86.4677796222291,-68.14673474241698,-66.07309844841615,
                                     -56.302362373674626,-54.63592515665142,-49.54128744936952,
                                     -47.77783428990662,-45.159157747842364,-43.112766536500985,
                                     -40.904881657341534,-38.624975888999664,-36.21821364136139,
                                     -33.61319247902676,-30.618705849873916,-26.992630355951256,
                                     -19.965626399359778,-9.761220034345175,-0.009675776079236078,
                                     -8.793180471674402e-7,-4.675655622579791e-14,-3.291874702847949e-14,
                                     -3.0408119421278934e-14,-2.2328449483329286e-14,
                                     -1.0693016765542536e-14,-7.240806196368183e-15,
                                     -5.761950647030487e-15,-5.957409950992936e-16,3.2491920424847875e-15,
                                     9.219910025371442e-15,1.2799168101873962e-14,1.7108898977066272e-14,
                                     2.855032832850196e-14,4.1048723593164446e-14,9.99499970701814,
                                     18.470571911228987,30.51830887197781,33.54443547978839,
                                     36.1607309907248,38.57320314001222,40.85924266944485,
                                     43.035391586290125,45.287513370945284,47.151147140980484,
                                     50.73276704593593,52.39952223577173,59.50068014643575,
                                     61.31379159039717,74.51253995296709,77.13729578952395,
                                     94.37203226002364,99.43109411760354,102.6487394625419,
                                     105.06218043709777,107.8179491492106,110.12126119755328,
                                     113.57945167126782,116.37801423876415,122.99832759829658,
                                     125.12209476580418,138.05692785144052,140.21814403606146,
                                     161.31660187081715,164.54676840029904,175.43647569692263,
                                     192.3282320950376,207.53405143733352,212.25537253729775,
                                     284.3628999051823,294.6884623476941,341.98290548531804,
                                     507.44043882416463,534.3382764375148,967.6392677402239,
                                     1875.507871717115,3171.7801027349824,5101.18005605357
                                    };

     // test

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -10.0));
     }
}

//===========//
// test # 79 //
//===========//

TEST(MatrixTest79, EigenvaluesDSYEVD1)
{
     MatrixDense<double> matA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 5) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 5) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     matA.EigenvaluesDSYEVD(eigenvaluesA);

     // test

     const double arr_values[100] = {-323.38478082263765, -318.95649570052416, -259.5812044929057,
                                     -255.47902023149175, -210.8574832531544, -207.046095983574,
                                     -172.17668982103106, -169.63822144706373, -167.11362728657673,
                                     -164.65335127248554, -137.93078894722365, -135.63554168942136,
                                     -133.27655590262873, -130.90313825750582, -111.16491214977447,
                                     -108.69201485977163, -104.86333072392988, -102.09654131372476,
                                     -89.52930445237439, -87.11011352113846, -79.24098389717554,
                                     -76.62676551546362, -71.01471245774239, -68.78074078937429,
                                     -57.39730933358366, -55.597048251741185, -53.807888693814036,
                                     -52.082337027364105, -42.001162570408376, -40.10702874995469,
                                     -36.13941170041208, -34.10866137118017, -29.537625276581757,
                                     -27.807512581350473, -20.493485216120316, -19.180726411630474,
                                     -17.86587524596272, -16.594313970872253, -10.527129255160773,
                                     -9.235915164234662, -6.274431175607891, -4.736234460618425,
                                     -3.113902006580256, -2.3717281394985243, -0.07129875210819334,
                                     2.4178759119876623, 3.3310614360036803, 4.154955748257782,
                                     8.633411865685325, 10.366556352869488, 12.095377640289747,
                                     14.018873556316715, 17.48114915683801, 21.917608573061386,
                                     23.316848960689295, 27.074595655585338, 31.322004721507344,
                                     34.9238763246869, 36.70168716147582, 39.12924266179792,
                                     48.291435480841905, 50.073858853444456, 52.12562986320187,
                                     57.27293729356433, 64.70819943681298, 66.93714122637961,
                                     68.82639047739008, 80.9665198923797, 83.5742202822415,
                                     86.58950836310686, 89.44951473838972, 102.06705367044331,
                                     107.13994334023343, 109.44821746324266, 117.55286230732882,
                                     126.41194103599305, 133.5454916018076, 135.893724579225,
                                     151.52771532059967, 157.8972863072518, 165.22044899155756,
                                     167.77180305861472, 190.84742611813815, 205.77684982675112,
                                     208.5174101551291, 234.35823130936174, 279.92197126989515,
                                     330.06056030550724, 384.6942438609073, 444.0868835363212,
                                     508.62422446622827, 578.8097468653175, 655.2951900145777,
                                     738.9379634908137, 830.8998363959786, 932.8239020042909,
                                     1047.1805607335104, 1178.0384559097015, 1333.1755507750008,
                                     1532.5774637949444
                                    };

     // test

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -10.0));
     }
}

//===========//
// test # 80 //
//===========//

TEST(MatrixTest80, EigenvaluesDSYEVD2)
{
     MatrixDense<double> matA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 20) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 20) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     matA.EigenvaluesDSYEVD(eigenvaluesA);

     // test

     const double arr_values[100] = {-890.0679762687605,-833.96613152625,-387.9183497645402,
                                     -374.28858505936836,-325.97417429278767,-290.39317003462327,
                                     -243.09349980476634,-237.9671765884564,-184.65425879239723,
                                     -181.6433889406905,-152.42678092373745,-150.01440312367828,
                                     -140.20043836510496,-134.26239413119825,-130.50508513840435,
                                     -128.1365612192653,-119.6243787096752,-117.25234191992651,
                                     -112.47792700587912,-108.89915545206071,-104.74759545668331,
                                     -100.22968339474058,-94.96995783385485,-89.69665862307328,
                                     -86.4677796222291,-68.14673474241698,-66.07309844841615,
                                     -56.302362373674626,-54.63592515665142,-49.54128744936952,
                                     -47.77783428990662,-45.159157747842364,-43.112766536500985,
                                     -40.904881657341534,-38.624975888999664,-36.21821364136139,
                                     -33.61319247902676,-30.618705849873916,-26.992630355951256,
                                     -19.965626399359778,-9.761220034345175,-0.009675776079236078,
                                     -8.793180471674402e-7,-4.675655622579791e-14,-3.291874702847949e-14,
                                     -3.0408119421278934e-14,-2.2328449483329286e-14,
                                     -1.0693016765542536e-14,-7.240806196368183e-15,
                                     -5.761950647030487e-15,-5.957409950992936e-16,3.2491920424847875e-15,
                                     9.219910025371442e-15,1.2799168101873962e-14,1.7108898977066272e-14,
                                     2.855032832850196e-14,4.1048723593164446e-14,9.99499970701814,
                                     18.470571911228987,30.51830887197781,33.54443547978839,
                                     36.1607309907248,38.57320314001222,40.85924266944485,
                                     43.035391586290125,45.287513370945284,47.151147140980484,
                                     50.73276704593593,52.39952223577173,59.50068014643575,
                                     61.31379159039717,74.51253995296709,77.13729578952395,
                                     94.37203226002364,99.43109411760354,102.6487394625419,
                                     105.06218043709777,107.8179491492106,110.12126119755328,
                                     113.57945167126782,116.37801423876415,122.99832759829658,
                                     125.12209476580418,138.05692785144052,140.21814403606146,
                                     161.31660187081715,164.54676840029904,175.43647569692263,
                                     192.3282320950376,207.53405143733352,212.25537253729775,
                                     284.3628999051823,294.6884623476941,341.98290548531804,
                                     507.44043882416463,534.3382764375148,967.6392677402239,
                                     1875.507871717115,3171.7801027349824,5101.18005605357
                                    };

     // test

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -10.0));
     }
}

//===========//
// test # 81 //
//===========//

TEST(MatrixTest81, EigenvectorsDSYEVD1)
{
     MatrixDense<double> matA;
     MatrixDense<double> eigenvectorsA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 5) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 5) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     matA.EigenvectorsDSYEVD(eigenvectorsA);

     // test

     const double arr_values99[100] = {3.6942314255214093e-22,
                                       1.9280574390556084e-21,
                                       8.336207484683114e-21,
                                       3.201738809930229e-20,
                                       1.128792365433472e-19,
                                       3.7219958073502635e-19,
                                       1.1616570563876596e-18,
                                       3.4603856776191363e-18,
                                       9.898106164891213e-18,
                                       2.7313034552506115e-17,
                                       7.297235551812733e-17,
                                       1.8931798526470812e-16,
                                       4.781008453916819e-16,
                                       1.1776583982769647e-15,
                                       2.8342405979299284e-15,
                                       6.674396729414062e-15,
                                       1.5399303294661802e-14,
                                       3.4849026635393604e-14,
                                       7.743020050094637e-14,
                                       1.6906046576799098e-13,
                                       3.630170569572799e-13,
                                       7.671355827326599e-13,
                                       1.5964469231753074e-12,
                                       3.2735991022182145e-12,
                                       6.617806646334326e-12,
                                       1.3195608156949319e-11,
                                       2.596350061894761e-11,
                                       5.0430333328453936e-11,
                                       9.67336578559975e-11,
                                       1.8330342627363303e-10,
                                       3.4324925188064113e-10,
                                       6.353651498186134e-10,
                                       1.1628708815038037e-9,
                                       2.104965971919157e-9,
                                       3.7693601315726985e-9,
                                       6.6787563519161844e-9,
                                       1.1711672807554324e-8,
                                       2.03292597117336e-8,
                                       3.4936698501777865e-8,
                                       5.9452893015691615e-8,
                                       1.0019897315247485e-7,
                                       1.672693861786821e-7,
                                       2.7662540139461026e-7,
                                       4.5325802559019753e-7,
                                       7.359163073250683e-7,
                                       1.1841012055800893e-6,
                                       1.8883009979218705e-6,
                                       2.984811021145573e-6,
                                       4.676960526639804e-6,
                                       7.265186654807506e-6,
                                       0.000011189174362740128,
                                       0.00001708624363917233,
                                       0.000025871327250800013,
                                       0.00003884524735147213,
                                       0.000057839551835632114,
                                       0.0000854078666854305,
                                       0.00012507547298611522,
                                       0.0001816604867751283,
                                       0.0002616813987570242,
                                       0.00037386653150919884,
                                       0.0005297808051872514,
                                       0.0007445835839285321,
                                       0.0010379277320564163,
                                       0.0014350037062010874,
                                       0.001967722735545796,
                                       0.0026760192519511626,
                                       0.0036092342363378766,
                                       0.00482751783582054,
                                       0.00640315989292051,
                                       0.008421723355192411,
                                       0.010982819682174948,
                                       0.014200329986124536,
                                       0.018201824732492763,
                                       0.023126912983358,
                                       0.029124247016656085,
                                       0.03634692535629019,
                                       0.04494587964943125,
                                       0.055061139884829774,
                                       0.0668110106541383,
                                       0.08027936243852593,
                                       0.09549945152005936,
                                       0.11243731017783884,
                                       0.13097574227719463,
                                       0.15090013051721174,
                                       0.17186879268938443,
                                       0.19341147439162046,
                                       0.21493488140894385,
                                       0.23573601738701488,
                                       0.2548580930562397,
                                       0.27133687410309687,
                                       0.28424352285526244,
                                       0.29273026481733555,
                                       0.29466646943807095,
                                       0.28996990773813897,
                                       0.27878021480447934,
                                       0.26146670556930435,
                                       0.22712948196564564,
                                       0.19198046679049208,
                                       0.15683996268683814,
                                       0.1225409827758915
                                      };

     // test

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,99)) - abs(arr_values99[i])) < pow(10.0, -10.0));
     }
}

//===========//
// test # 82 //
//===========//

TEST(MatrixTest82, EigenvectorsDSYEVD2)
{
     MatrixDense<double> matA;
     MatrixDense<double> eigenvectorsA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 20) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 20) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     matA.EigenvectorsDSYEVD(eigenvectorsA);

     // test data # 1

     const double arr_values99[100] = {-0.00002379437463800071,-0.0000321126535355192,
                                       -0.00004261209009056319,-0.00005574228105018768,
                                       -0.0000720258070448845,-0.00009206699311457821,
                                       -0.00011656127426176481,-0.00014630515846035676,
                                       -0.0001822067730093074,-0.00022529697290878234,
                                       -0.00027674098203948964,-0.0003378505293342626,
                                       -0.00041009643284298355,-0.0004951215746125915,
                                       -0.0005947541986465926,-0.0007110214528956958,-0.0008461630842956044,
                                       -0.001002645183353377,-0.0011831738617460526,-0.0013907087329034266,
                                       -0.0016283827619966063,-0.0018997449209395513,-0.002208573606467658,
                                       -0.002558944309118129,-0.0029552087127267114,-0.0034019962501085446,
                                       -0.003904213875493434,-0.00446704386484559,-0.00509593944935361,
                                       -0.005796618082542783,-0.006575052137809326,-0.007437456830885415,
                                       -0.00839027516100404,-0.009440159665541611,-0.010593950785877937,
                                       -0.01185865164733851,-0.013241399063586366,-0.014749430585927434,
                                       -0.0163900474309,-0.018170573135449803,-0.0200983089054282,
                                       -0.022180481856340313,-0.024424190854787288,-0.026834080780647522,
                                       -0.02941440937535356,-0.03216899598488147,-0.035101168178730414,
                                       -0.03821370638490854,-0.04150878671030463,-0.04498792214706611,
                                       -0.04865190239866414,-0.05250073259412269,-0.056533571195309565,
                                       -0.06074866744009344,-0.0651432987033876,-0.06971370819842077,
                                       -0.07445504348175375,-0.07936129626731185,-0.08442524409670342,
                                       -0.08963839445497533,-0.09499093194079421,-0.10047166921101545,
                                       -0.10595789789842808,-0.11143628534678302,-0.11689273862688677,
                                       -0.1223124096438104,-0.12767970465965733,-0.13297829857254914,
                                       -0.13819115429332646,-0.1433005475596976,-0.14828809752292466,
                                       -0.15313480343431787,-0.15782108774752324,-0.16232684593753477,
                                       -0.16663150331824728,-0.17071407911689127,-0.17455325803557586,
                                       -0.17812746949713273,-0.18141497473424503,-0.18439396183722762,
                                       -0.18704264882716626,-0.18541168339207653,-0.18356676120058393,
                                       -0.18150272926411598,-0.17921485423076686,-0.17669886945503793,
                                       -0.17395102341224067,-0.17096812928970723,-0.1677476155625255,
                                       -0.1642875773360043,-0.16058682821057293,-0.15664495239745393,
                                       -0.15246235678535205,-0.14804032262974395,-0.14338105650732136,
                                       -0.13848774014895246,-0.1333645787354236,-0.128016847211483,
                                       -0.1224509341456258,-0.11667438263597216
                                      };

     // test data # 2

     const double arr_values98[100] = {0.0005490815006598427,0.0007217004212979288,0.0009319727820738912,
                                       0.0011856764796110383,0.0014891369301256077,0.0018492353084341568,
                                       0.002273410759398609,0.002769655716932461,0.003346503443557062,
                                       0.00401300689217423,0.0047787079930774546,0.00565359648519078,
                                       0.006648057443072511,0.007772806702325777,0.009038813457661714,
                                       0.010457209401826704,0.012039183891666605,0.013795864771304657,
                                       0.01573818465305239,0.01787673265523282,0.020218129530799774,
                                       0.022773522732384158,0.02555088599547039,0.02855699048006569,
                                       0.03179740706616532,0.03527631554658473,0.038996305859039175,
                                       0.04295817276333242,0.047160705650423344,0.05160047546600458,
                                       0.056271621039461366,0.061165637426456754,0.06627116919601259,
                                       0.07157381191620707,0.0770559254111312,0.08269646266941802,
                                       0.08847081857459133,0.09435070289203276,0.10030404217816305,
                                       0.10629491546542182,0.11228359420772163,0.11822646848984367,
                                       0.12407615562938958,0.12979275139874066,0.13533385216700752,
                                       0.14065467360130016,0.14570820649639804,0.1504454123825699,
                                       0.15481546139101132,0.15876601462676446,0.16224355300490953,
                                       0.1651937541424425,0.16756191846119972,0.16929344514268863,
                                       0.17033435798074814,0.17063188050057534,0.17013505895196374,
                                       0.16879543094112387,0.16656773654130375,0.16341066772151153,
                                       0.1592876487959431,0.15416764632985688,0.14836208495033895,
                                       0.14186022973422263,0.1346552528267162,0.12674464039743252,
                                       0.118130599994188,0.10882046380600134,0.09882708283296263,
                                       0.08816920644738636,0.07687184132594177,0.06496658324539006,
                                       0.052491914775380344,0.039493461481783246,0.026024198885649517,
                                       0.012144602119448178,-0.002077270001960313,-0.0165657649080691,
                                       -0.0312377086295896,-0.04600256891175273,-0.06076271707085829,
                                       -0.06795927022946352,-0.07503806683258157,-0.0819579039270416,
                                       -0.08867572326732134,-0.09514679596043041,-0.1013249415936529,
                                       -0.10716278331612594,-0.1126120400798519,-0.11762385692798413,
                                       -0.12214917384544385,-0.12613913325700105,-0.12954552576940606,
                                       -0.1323212732062947,-0.13442094737761986,-0.13580132236057105,
                                       -0.13642195734879647,-0.13624580635507777,-0.1352398502347542,
                                       -0.13337574564008783
                                      };

     // test data # 3

     const double arr_values97[100] = {-0.0048844047935863765,-0.006236486880389232,-0.007815274325331554,
                                       -0.009640183890712983,-0.011730050533458039,-0.014102771342375649,
                                       -0.016774922205544347,-0.019761352037216788,-0.023074760743478643,
                                       -0.026725268499719908,-0.03071998531275152,-0.03506259120033407,
                                       -0.03975293858755401,-0.04478668963230909,-0.050155002084304526,
                                       -0.05584427788164485,-0.06183598892150303,-0.06810659423095557,
                                       -0.07462756203769172,-0.08136550893055443,-0.08823038013976417,
                                       -0.09520849071940404,-0.1022476308045662,-0.10929156383600795,
                                       -0.11627847844623534,-0.12314131678294159,-0.1298082479646105,
                                       -0.13620329680129123,-0.14224713548726084,-0.14785804289917964,
                                       -0.15295303238959757,-0.1574491445459654,-0.1612648963069822,
                                       -0.16432187213530286,-0.16654643671328404,-0.16787154196597193,
                                       -0.16823859426980445,-0.16759934066305365,-0.16591772596198778,
                                       -0.1631716661722737,-0.15935634408053131,-0.15448321146294608,
                                       -0.14858218139776455,-0.1416381780493364,-0.1336490652093021,
                                       -0.12462715724623677,-0.11460064260169803,-0.10361487512764765,
                                       -0.09173348472751607,-0.07903925562502323,-0.06563471836746396,
                                       -0.051642400613860905,-0.03720468211196078,-0.022483201281379775,
                                       -0.007657764735299888,0.007075282886553215,0.021505263042677962,
                                       0.03540996481701064,0.04855905391412624,0.06071808324036558,
                                       0.07165316959606502,0.08113608035225883,0.09009849834439126,
                                       0.09838663571146652,0.10584187751947657,0.11230295059241002,
                                       0.11760842867321217,0.12159956720711124,0.12412345180969263,
                                       0.1250364341022301,0.12420781716730643,0.12152374055295379,
                                       0.11689120174753254,0.11024213763900717,0.10153747600764697,
                                       0.09077105399613189,0.07797328823931185,0.06321447046527728,
                                       0.04660755350441126,0.028310286413503014,0.008526547885299433,
                                       0.0025719295116293645,-0.004200598749307477,-0.011762440439929252,
                                       -0.020069965854842452,-0.029063538293552106,-0.038666727752430484,
                                       -0.048785756486008615,-0.05930922217827011,-0.07010814475633873,
                                       -0.08103638195706307,-0.09193145642839495,-0.10261583322770457,
                                       -0.11289868090088843,-0.12257814174844638,-0.1314441273038225,
                                       -0.139281643409791,-0.14587463558538188,-0.15101032970590494,
                                       -0.15448402553323257
                                      };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,99)) - abs(arr_values99[i])) < pow(10.0, -10.0));
     }

     // test # 2

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,98)) - abs(arr_values98[i])) < pow(10.0, -10.0));
     }

     // test # 3

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,97)) - abs(arr_values97[i])) < pow(10.0, -10.0));
     }
}

//===========//
// test # 83 //
//===========//

TEST(MatrixTest83, EigensystemDSYEVR1)
{
     MatrixDense<double> matA;
     MatrixDense<double> eigenvectorsA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 5) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 5) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigensystemDSYEVR(eigenvectorsA, eigenvaluesA);

     // test

     const double arr_values[100] = {-323.38478082263765, -318.95649570052416, -259.5812044929057,
                                     -255.47902023149175, -210.8574832531544, -207.046095983574,
                                     -172.17668982103106, -169.63822144706373, -167.11362728657673,
                                     -164.65335127248554, -137.93078894722365, -135.63554168942136,
                                     -133.27655590262873, -130.90313825750582, -111.16491214977447,
                                     -108.69201485977163, -104.86333072392988, -102.09654131372476,
                                     -89.52930445237439, -87.11011352113846, -79.24098389717554,
                                     -76.62676551546362, -71.01471245774239, -68.78074078937429,
                                     -57.39730933358366, -55.597048251741185, -53.807888693814036,
                                     -52.082337027364105, -42.001162570408376, -40.10702874995469,
                                     -36.13941170041208, -34.10866137118017, -29.537625276581757,
                                     -27.807512581350473, -20.493485216120316, -19.180726411630474,
                                     -17.86587524596272, -16.594313970872253, -10.527129255160773,
                                     -9.235915164234662, -6.274431175607891, -4.736234460618425,
                                     -3.113902006580256, -2.3717281394985243, -0.07129875210819334,
                                     2.4178759119876623, 3.3310614360036803, 4.154955748257782,
                                     8.633411865685325, 10.366556352869488, 12.095377640289747,
                                     14.018873556316715, 17.48114915683801, 21.917608573061386,
                                     23.316848960689295, 27.074595655585338, 31.322004721507344,
                                     34.9238763246869, 36.70168716147582, 39.12924266179792,
                                     48.291435480841905, 50.073858853444456, 52.12562986320187,
                                     57.27293729356433, 64.70819943681298, 66.93714122637961,
                                     68.82639047739008, 80.9665198923797, 83.5742202822415,
                                     86.58950836310686, 89.44951473838972, 102.06705367044331,
                                     107.13994334023343, 109.44821746324266, 117.55286230732882,
                                     126.41194103599305, 133.5454916018076, 135.893724579225,
                                     151.52771532059967, 157.8972863072518, 165.22044899155756,
                                     167.77180305861472, 190.84742611813815, 205.77684982675112,
                                     208.5174101551291, 234.35823130936174, 279.92197126989515,
                                     330.06056030550724, 384.6942438609073, 444.0868835363212,
                                     508.62422446622827, 578.8097468653175, 655.2951900145777,
                                     738.9379634908137, 830.8998363959786, 932.8239020042909,
                                     1047.1805607335104, 1178.0384559097015, 1333.1755507750008,
                                     1532.5774637949444
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -10.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//===========//
// test # 84 //
//===========//

TEST(MatrixTest84, EigensystemDSYEVR2)
{
     MatrixDense<double> matA;
     MatrixDense<double> eigenvectorsA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 20) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 20) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigensystemDSYEVR(eigenvectorsA, eigenvaluesA);

     // test

     const double arr_values[100] = {-890.0679762687605,-833.96613152625,-387.9183497645402,
                                     -374.28858505936836,-325.97417429278767,-290.39317003462327,
                                     -243.09349980476634,-237.9671765884564,-184.65425879239723,
                                     -181.6433889406905,-152.42678092373745,-150.01440312367828,
                                     -140.20043836510496,-134.26239413119825,-130.50508513840435,
                                     -128.1365612192653,-119.6243787096752,-117.25234191992651,
                                     -112.47792700587912,-108.89915545206071,-104.74759545668331,
                                     -100.22968339474058,-94.96995783385485,-89.69665862307328,
                                     -86.4677796222291,-68.14673474241698,-66.07309844841615,
                                     -56.302362373674626,-54.63592515665142,-49.54128744936952,
                                     -47.77783428990662,-45.159157747842364,-43.112766536500985,
                                     -40.904881657341534,-38.624975888999664,-36.21821364136139,
                                     -33.61319247902676,-30.618705849873916,-26.992630355951256,
                                     -19.965626399359778,-9.761220034345175,-0.009675776079236078,
                                     -8.793180471674402e-7,-4.675655622579791e-14,-3.291874702847949e-14,
                                     -3.0408119421278934e-14,-2.2328449483329286e-14,
                                     -1.0693016765542536e-14,-7.240806196368183e-15,
                                     -5.761950647030487e-15,-5.957409950992936e-16,3.2491920424847875e-15,
                                     9.219910025371442e-15,1.2799168101873962e-14,1.7108898977066272e-14,
                                     2.855032832850196e-14,4.1048723593164446e-14,9.99499970701814,
                                     18.470571911228987,30.51830887197781,33.54443547978839,
                                     36.1607309907248,38.57320314001222,40.85924266944485,
                                     43.035391586290125,45.287513370945284,47.151147140980484,
                                     50.73276704593593,52.39952223577173,59.50068014643575,
                                     61.31379159039717,74.51253995296709,77.13729578952395,
                                     94.37203226002364,99.43109411760354,102.6487394625419,
                                     105.06218043709777,107.8179491492106,110.12126119755328,
                                     113.57945167126782,116.37801423876415,122.99832759829658,
                                     125.12209476580418,138.05692785144052,140.21814403606146,
                                     161.31660187081715,164.54676840029904,175.43647569692263,
                                     192.3282320950376,207.53405143733352,212.25537253729775,
                                     284.3628999051823,294.6884623476941,341.98290548531804,
                                     507.44043882416463,534.3382764375148,967.6392677402239,
                                     1875.507871717115,3171.7801027349824,5101.18005605357
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -10.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//===========//
// test # 85 //
//===========//

TEST(MatrixTest85, EigenvaluesDSYEVR1)
{
     MatrixDense<double> matA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 5) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 5) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvaluesDSYEVR(eigenvaluesA);

     // test

     const double arr_values[100] = {-323.38478082263765, -318.95649570052416, -259.5812044929057,
                                     -255.47902023149175, -210.8574832531544, -207.046095983574,
                                     -172.17668982103106, -169.63822144706373, -167.11362728657673,
                                     -164.65335127248554, -137.93078894722365, -135.63554168942136,
                                     -133.27655590262873, -130.90313825750582, -111.16491214977447,
                                     -108.69201485977163, -104.86333072392988, -102.09654131372476,
                                     -89.52930445237439, -87.11011352113846, -79.24098389717554,
                                     -76.62676551546362, -71.01471245774239, -68.78074078937429,
                                     -57.39730933358366, -55.597048251741185, -53.807888693814036,
                                     -52.082337027364105, -42.001162570408376, -40.10702874995469,
                                     -36.13941170041208, -34.10866137118017, -29.537625276581757,
                                     -27.807512581350473, -20.493485216120316, -19.180726411630474,
                                     -17.86587524596272, -16.594313970872253, -10.527129255160773,
                                     -9.235915164234662, -6.274431175607891, -4.736234460618425,
                                     -3.113902006580256, -2.3717281394985243, -0.07129875210819334,
                                     2.4178759119876623, 3.3310614360036803, 4.154955748257782,
                                     8.633411865685325, 10.366556352869488, 12.095377640289747,
                                     14.018873556316715, 17.48114915683801, 21.917608573061386,
                                     23.316848960689295, 27.074595655585338, 31.322004721507344,
                                     34.9238763246869, 36.70168716147582, 39.12924266179792,
                                     48.291435480841905, 50.073858853444456, 52.12562986320187,
                                     57.27293729356433, 64.70819943681298, 66.93714122637961,
                                     68.82639047739008, 80.9665198923797, 83.5742202822415,
                                     86.58950836310686, 89.44951473838972, 102.06705367044331,
                                     107.13994334023343, 109.44821746324266, 117.55286230732882,
                                     126.41194103599305, 133.5454916018076, 135.893724579225,
                                     151.52771532059967, 157.8972863072518, 165.22044899155756,
                                     167.77180305861472, 190.84742611813815, 205.77684982675112,
                                     208.5174101551291, 234.35823130936174, 279.92197126989515,
                                     330.06056030550724, 384.6942438609073, 444.0868835363212,
                                     508.62422446622827, 578.8097468653175, 655.2951900145777,
                                     738.9379634908137, 830.8998363959786, 932.8239020042909,
                                     1047.1805607335104, 1178.0384559097015, 1333.1755507750008,
                                     1532.5774637949444
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -5.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//===========//
// test # 86 //
//===========//

TEST(MatrixTest86, EigenvaluesDSYEVR2)
{
     MatrixDense<double> matA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 20) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 20) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvaluesDSYEVR(eigenvaluesA);

     // test

     const double arr_values[100] = {-890.0679762687605,-833.96613152625,-387.9183497645402,
                                     -374.28858505936836,-325.97417429278767,-290.39317003462327,
                                     -243.09349980476634,-237.9671765884564,-184.65425879239723,
                                     -181.6433889406905,-152.42678092373745,-150.01440312367828,
                                     -140.20043836510496,-134.26239413119825,-130.50508513840435,
                                     -128.1365612192653,-119.6243787096752,-117.25234191992651,
                                     -112.47792700587912,-108.89915545206071,-104.74759545668331,
                                     -100.22968339474058,-94.96995783385485,-89.69665862307328,
                                     -86.4677796222291,-68.14673474241698,-66.07309844841615,
                                     -56.302362373674626,-54.63592515665142,-49.54128744936952,
                                     -47.77783428990662,-45.159157747842364,-43.112766536500985,
                                     -40.904881657341534,-38.624975888999664,-36.21821364136139,
                                     -33.61319247902676,-30.618705849873916,-26.992630355951256,
                                     -19.965626399359778,-9.761220034345175,-0.009675776079236078,
                                     -8.793180471674402e-7,-4.675655622579791e-14,-3.291874702847949e-14,
                                     -3.0408119421278934e-14,-2.2328449483329286e-14,
                                     -1.0693016765542536e-14,-7.240806196368183e-15,
                                     -5.761950647030487e-15,-5.957409950992936e-16,3.2491920424847875e-15,
                                     9.219910025371442e-15,1.2799168101873962e-14,1.7108898977066272e-14,
                                     2.855032832850196e-14,4.1048723593164446e-14,9.99499970701814,
                                     18.470571911228987,30.51830887197781,33.54443547978839,
                                     36.1607309907248,38.57320314001222,40.85924266944485,
                                     43.035391586290125,45.287513370945284,47.151147140980484,
                                     50.73276704593593,52.39952223577173,59.50068014643575,
                                     61.31379159039717,74.51253995296709,77.13729578952395,
                                     94.37203226002364,99.43109411760354,102.6487394625419,
                                     105.06218043709777,107.8179491492106,110.12126119755328,
                                     113.57945167126782,116.37801423876415,122.99832759829658,
                                     125.12209476580418,138.05692785144052,140.21814403606146,
                                     161.31660187081715,164.54676840029904,175.43647569692263,
                                     192.3282320950376,207.53405143733352,212.25537253729775,
                                     284.3628999051823,294.6884623476941,341.98290548531804,
                                     507.44043882416463,534.3382764375148,967.6392677402239,
                                     1875.507871717115,3171.7801027349824,5101.18005605357
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -5.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//===========//
// test # 87 //
//===========//

TEST(MatrixTest87, EigenvectorsDSYEVR1)
{
     MatrixDense<double> matA;
     MatrixDense<double> eigenvectorsA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 5) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 5) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvectorsDSYEVR(eigenvectorsA);

     // test

     const double arr_values99[100] = {3.6942314255214093e-22,
                                       1.9280574390556084e-21,
                                       8.336207484683114e-21,
                                       3.201738809930229e-20,
                                       1.128792365433472e-19,
                                       3.7219958073502635e-19,
                                       1.1616570563876596e-18,
                                       3.4603856776191363e-18,
                                       9.898106164891213e-18,
                                       2.7313034552506115e-17,
                                       7.297235551812733e-17,
                                       1.8931798526470812e-16,
                                       4.781008453916819e-16,
                                       1.1776583982769647e-15,
                                       2.8342405979299284e-15,
                                       6.674396729414062e-15,
                                       1.5399303294661802e-14,
                                       3.4849026635393604e-14,
                                       7.743020050094637e-14,
                                       1.6906046576799098e-13,
                                       3.630170569572799e-13,
                                       7.671355827326599e-13,
                                       1.5964469231753074e-12,
                                       3.2735991022182145e-12,
                                       6.617806646334326e-12,
                                       1.3195608156949319e-11,
                                       2.596350061894761e-11,
                                       5.0430333328453936e-11,
                                       9.67336578559975e-11,
                                       1.8330342627363303e-10,
                                       3.4324925188064113e-10,
                                       6.353651498186134e-10,
                                       1.1628708815038037e-9,
                                       2.104965971919157e-9,
                                       3.7693601315726985e-9,
                                       6.6787563519161844e-9,
                                       1.1711672807554324e-8,
                                       2.03292597117336e-8,
                                       3.4936698501777865e-8,
                                       5.9452893015691615e-8,
                                       1.0019897315247485e-7,
                                       1.672693861786821e-7,
                                       2.7662540139461026e-7,
                                       4.5325802559019753e-7,
                                       7.359163073250683e-7,
                                       1.1841012055800893e-6,
                                       1.8883009979218705e-6,
                                       2.984811021145573e-6,
                                       4.676960526639804e-6,
                                       7.265186654807506e-6,
                                       0.000011189174362740128,
                                       0.00001708624363917233,
                                       0.000025871327250800013,
                                       0.00003884524735147213,
                                       0.000057839551835632114,
                                       0.0000854078666854305,
                                       0.00012507547298611522,
                                       0.0001816604867751283,
                                       0.0002616813987570242,
                                       0.00037386653150919884,
                                       0.0005297808051872514,
                                       0.0007445835839285321,
                                       0.0010379277320564163,
                                       0.0014350037062010874,
                                       0.001967722735545796,
                                       0.0026760192519511626,
                                       0.0036092342363378766,
                                       0.00482751783582054,
                                       0.00640315989292051,
                                       0.008421723355192411,
                                       0.010982819682174948,
                                       0.014200329986124536,
                                       0.018201824732492763,
                                       0.023126912983358,
                                       0.029124247016656085,
                                       0.03634692535629019,
                                       0.04494587964943125,
                                       0.055061139884829774,
                                       0.0668110106541383,
                                       0.08027936243852593,
                                       0.09549945152005936,
                                       0.11243731017783884,
                                       0.13097574227719463,
                                       0.15090013051721174,
                                       0.17186879268938443,
                                       0.19341147439162046,
                                       0.21493488140894385,
                                       0.23573601738701488,
                                       0.2548580930562397,
                                       0.27133687410309687,
                                       0.28424352285526244,
                                       0.29273026481733555,
                                       0.29466646943807095,
                                       0.28996990773813897,
                                       0.27878021480447934,
                                       0.26146670556930435,
                                       0.22712948196564564,
                                       0.19198046679049208,
                                       0.15683996268683814,
                                       0.1225409827758915
                                      };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,99)) - abs(arr_values99[i])) < pow(10.0, -10.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//===========//
// test # 88 //
//===========//

TEST(MatrixTest88, EigenvectorsDSYEVR2)
{
     MatrixDense<double> matA;
     MatrixDense<double> eigenvectorsA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 20) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 20) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvectorsDSYEVR(eigenvectorsA);

     // test data # 1

     const double arr_values99[100] = {-0.00002379437463800071,-0.0000321126535355192,
                                       -0.00004261209009056319,-0.00005574228105018768,
                                       -0.0000720258070448845,-0.00009206699311457821,
                                       -0.00011656127426176481,-0.00014630515846035676,
                                       -0.0001822067730093074,-0.00022529697290878234,
                                       -0.00027674098203948964,-0.0003378505293342626,
                                       -0.00041009643284298355,-0.0004951215746125915,
                                       -0.0005947541986465926,-0.0007110214528956958,-0.0008461630842956044,
                                       -0.001002645183353377,-0.0011831738617460526,-0.0013907087329034266,
                                       -0.0016283827619966063,-0.0018997449209395513,-0.002208573606467658,
                                       -0.002558944309118129,-0.0029552087127267114,-0.0034019962501085446,
                                       -0.003904213875493434,-0.00446704386484559,-0.00509593944935361,
                                       -0.005796618082542783,-0.006575052137809326,-0.007437456830885415,
                                       -0.00839027516100404,-0.009440159665541611,-0.010593950785877937,
                                       -0.01185865164733851,-0.013241399063586366,-0.014749430585927434,
                                       -0.0163900474309,-0.018170573135449803,-0.0200983089054282,
                                       -0.022180481856340313,-0.024424190854787288,-0.026834080780647522,
                                       -0.02941440937535356,-0.03216899598488147,-0.035101168178730414,
                                       -0.03821370638490854,-0.04150878671030463,-0.04498792214706611,
                                       -0.04865190239866414,-0.05250073259412269,-0.056533571195309565,
                                       -0.06074866744009344,-0.0651432987033876,-0.06971370819842077,
                                       -0.07445504348175375,-0.07936129626731185,-0.08442524409670342,
                                       -0.08963839445497533,-0.09499093194079421,-0.10047166921101545,
                                       -0.10595789789842808,-0.11143628534678302,-0.11689273862688677,
                                       -0.1223124096438104,-0.12767970465965733,-0.13297829857254914,
                                       -0.13819115429332646,-0.1433005475596976,-0.14828809752292466,
                                       -0.15313480343431787,-0.15782108774752324,-0.16232684593753477,
                                       -0.16663150331824728,-0.17071407911689127,-0.17455325803557586,
                                       -0.17812746949713273,-0.18141497473424503,-0.18439396183722762,
                                       -0.18704264882716626,-0.18541168339207653,-0.18356676120058393,
                                       -0.18150272926411598,-0.17921485423076686,-0.17669886945503793,
                                       -0.17395102341224067,-0.17096812928970723,-0.1677476155625255,
                                       -0.1642875773360043,-0.16058682821057293,-0.15664495239745393,
                                       -0.15246235678535205,-0.14804032262974395,-0.14338105650732136,
                                       -0.13848774014895246,-0.1333645787354236,-0.128016847211483,
                                       -0.1224509341456258,-0.11667438263597216
                                      };

     // test data # 2

     const double arr_values98[100] = {0.0005490815006598427,0.0007217004212979288,0.0009319727820738912,
                                       0.0011856764796110383,0.0014891369301256077,0.0018492353084341568,
                                       0.002273410759398609,0.002769655716932461,0.003346503443557062,
                                       0.00401300689217423,0.0047787079930774546,0.00565359648519078,
                                       0.006648057443072511,0.007772806702325777,0.009038813457661714,
                                       0.010457209401826704,0.012039183891666605,0.013795864771304657,
                                       0.01573818465305239,0.01787673265523282,0.020218129530799774,
                                       0.022773522732384158,0.02555088599547039,0.02855699048006569,
                                       0.03179740706616532,0.03527631554658473,0.038996305859039175,
                                       0.04295817276333242,0.047160705650423344,0.05160047546600458,
                                       0.056271621039461366,0.061165637426456754,0.06627116919601259,
                                       0.07157381191620707,0.0770559254111312,0.08269646266941802,
                                       0.08847081857459133,0.09435070289203276,0.10030404217816305,
                                       0.10629491546542182,0.11228359420772163,0.11822646848984367,
                                       0.12407615562938958,0.12979275139874066,0.13533385216700752,
                                       0.14065467360130016,0.14570820649639804,0.1504454123825699,
                                       0.15481546139101132,0.15876601462676446,0.16224355300490953,
                                       0.1651937541424425,0.16756191846119972,0.16929344514268863,
                                       0.17033435798074814,0.17063188050057534,0.17013505895196374,
                                       0.16879543094112387,0.16656773654130375,0.16341066772151153,
                                       0.1592876487959431,0.15416764632985688,0.14836208495033895,
                                       0.14186022973422263,0.1346552528267162,0.12674464039743252,
                                       0.118130599994188,0.10882046380600134,0.09882708283296263,
                                       0.08816920644738636,0.07687184132594177,0.06496658324539006,
                                       0.052491914775380344,0.039493461481783246,0.026024198885649517,
                                       0.012144602119448178,-0.002077270001960313,-0.0165657649080691,
                                       -0.0312377086295896,-0.04600256891175273,-0.06076271707085829,
                                       -0.06795927022946352,-0.07503806683258157,-0.0819579039270416,
                                       -0.08867572326732134,-0.09514679596043041,-0.1013249415936529,
                                       -0.10716278331612594,-0.1126120400798519,-0.11762385692798413,
                                       -0.12214917384544385,-0.12613913325700105,-0.12954552576940606,
                                       -0.1323212732062947,-0.13442094737761986,-0.13580132236057105,
                                       -0.13642195734879647,-0.13624580635507777,-0.1352398502347542,
                                       -0.13337574564008783
                                      };

     // test data # 3

     const double arr_values97[100] = {-0.0048844047935863765,-0.006236486880389232,-0.007815274325331554,
                                       -0.009640183890712983,-0.011730050533458039,-0.014102771342375649,
                                       -0.016774922205544347,-0.019761352037216788,-0.023074760743478643,
                                       -0.026725268499719908,-0.03071998531275152,-0.03506259120033407,
                                       -0.03975293858755401,-0.04478668963230909,-0.050155002084304526,
                                       -0.05584427788164485,-0.06183598892150303,-0.06810659423095557,
                                       -0.07462756203769172,-0.08136550893055443,-0.08823038013976417,
                                       -0.09520849071940404,-0.1022476308045662,-0.10929156383600795,
                                       -0.11627847844623534,-0.12314131678294159,-0.1298082479646105,
                                       -0.13620329680129123,-0.14224713548726084,-0.14785804289917964,
                                       -0.15295303238959757,-0.1574491445459654,-0.1612648963069822,
                                       -0.16432187213530286,-0.16654643671328404,-0.16787154196597193,
                                       -0.16823859426980445,-0.16759934066305365,-0.16591772596198778,
                                       -0.1631716661722737,-0.15935634408053131,-0.15448321146294608,
                                       -0.14858218139776455,-0.1416381780493364,-0.1336490652093021,
                                       -0.12462715724623677,-0.11460064260169803,-0.10361487512764765,
                                       -0.09173348472751607,-0.07903925562502323,-0.06563471836746396,
                                       -0.051642400613860905,-0.03720468211196078,-0.022483201281379775,
                                       -0.007657764735299888,0.007075282886553215,0.021505263042677962,
                                       0.03540996481701064,0.04855905391412624,0.06071808324036558,
                                       0.07165316959606502,0.08113608035225883,0.09009849834439126,
                                       0.09838663571146652,0.10584187751947657,0.11230295059241002,
                                       0.11760842867321217,0.12159956720711124,0.12412345180969263,
                                       0.1250364341022301,0.12420781716730643,0.12152374055295379,
                                       0.11689120174753254,0.11024213763900717,0.10153747600764697,
                                       0.09077105399613189,0.07797328823931185,0.06321447046527728,
                                       0.04660755350441126,0.028310286413503014,0.008526547885299433,
                                       0.0025719295116293645,-0.004200598749307477,-0.011762440439929252,
                                       -0.020069965854842452,-0.029063538293552106,-0.038666727752430484,
                                       -0.048785756486008615,-0.05930922217827011,-0.07010814475633873,
                                       -0.08103638195706307,-0.09193145642839495,-0.10261583322770457,
                                       -0.11289868090088843,-0.12257814174844638,-0.1314441273038225,
                                       -0.139281643409791,-0.14587463558538188,-0.15101032970590494,
                                       -0.15448402553323257
                                      };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,99)) - abs(arr_values99[i])) < pow(10.0, -10.0));
     }

     // test # 2

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,98)) - abs(arr_values98[i])) < pow(10.0, -10.0));
     }

     // test # 3

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,97)) - abs(arr_values97[i])) < pow(10.0, -10.0));
     }

     // test # 4

     EXPECT_TRUE(res == 0);
}

//===========//
// test # 89 //
//===========//

TEST(MatrixTest89, EigensystemDSYEVX1)
{
     MatrixDense<double> matA;
     MatrixDense<double> eigenvectorsA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 5) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 5) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigensystemDSYEVX(eigenvectorsA, eigenvaluesA);

     // test

     const double arr_values[100] = {-323.38478082263765, -318.95649570052416, -259.5812044929057,
                                     -255.47902023149175, -210.8574832531544, -207.046095983574,
                                     -172.17668982103106, -169.63822144706373, -167.11362728657673,
                                     -164.65335127248554, -137.93078894722365, -135.63554168942136,
                                     -133.27655590262873, -130.90313825750582, -111.16491214977447,
                                     -108.69201485977163, -104.86333072392988, -102.09654131372476,
                                     -89.52930445237439, -87.11011352113846, -79.24098389717554,
                                     -76.62676551546362, -71.01471245774239, -68.78074078937429,
                                     -57.39730933358366, -55.597048251741185, -53.807888693814036,
                                     -52.082337027364105, -42.001162570408376, -40.10702874995469,
                                     -36.13941170041208, -34.10866137118017, -29.537625276581757,
                                     -27.807512581350473, -20.493485216120316, -19.180726411630474,
                                     -17.86587524596272, -16.594313970872253, -10.527129255160773,
                                     -9.235915164234662, -6.274431175607891, -4.736234460618425,
                                     -3.113902006580256, -2.3717281394985243, -0.07129875210819334,
                                     2.4178759119876623, 3.3310614360036803, 4.154955748257782,
                                     8.633411865685325, 10.366556352869488, 12.095377640289747,
                                     14.018873556316715, 17.48114915683801, 21.917608573061386,
                                     23.316848960689295, 27.074595655585338, 31.322004721507344,
                                     34.9238763246869, 36.70168716147582, 39.12924266179792,
                                     48.291435480841905, 50.073858853444456, 52.12562986320187,
                                     57.27293729356433, 64.70819943681298, 66.93714122637961,
                                     68.82639047739008, 80.9665198923797, 83.5742202822415,
                                     86.58950836310686, 89.44951473838972, 102.06705367044331,
                                     107.13994334023343, 109.44821746324266, 117.55286230732882,
                                     126.41194103599305, 133.5454916018076, 135.893724579225,
                                     151.52771532059967, 157.8972863072518, 165.22044899155756,
                                     167.77180305861472, 190.84742611813815, 205.77684982675112,
                                     208.5174101551291, 234.35823130936174, 279.92197126989515,
                                     330.06056030550724, 384.6942438609073, 444.0868835363212,
                                     508.62422446622827, 578.8097468653175, 655.2951900145777,
                                     738.9379634908137, 830.8998363959786, 932.8239020042909,
                                     1047.1805607335104, 1178.0384559097015, 1333.1755507750008,
                                     1532.5774637949444
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -10.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//===========//
// test # 90 //
//===========//

TEST(MatrixTest90, EigensystemDSYEVX2)
{
     MatrixDense<double> matA;
     MatrixDense<double> eigenvectorsA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 20) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 20) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigensystemDSYEVX(eigenvectorsA, eigenvaluesA);

     // test

     const double arr_values[100] = {-890.0679762687605,-833.96613152625,-387.9183497645402,
                                     -374.28858505936836,-325.97417429278767,-290.39317003462327,
                                     -243.09349980476634,-237.9671765884564,-184.65425879239723,
                                     -181.6433889406905,-152.42678092373745,-150.01440312367828,
                                     -140.20043836510496,-134.26239413119825,-130.50508513840435,
                                     -128.1365612192653,-119.6243787096752,-117.25234191992651,
                                     -112.47792700587912,-108.89915545206071,-104.74759545668331,
                                     -100.22968339474058,-94.96995783385485,-89.69665862307328,
                                     -86.4677796222291,-68.14673474241698,-66.07309844841615,
                                     -56.302362373674626,-54.63592515665142,-49.54128744936952,
                                     -47.77783428990662,-45.159157747842364,-43.112766536500985,
                                     -40.904881657341534,-38.624975888999664,-36.21821364136139,
                                     -33.61319247902676,-30.618705849873916,-26.992630355951256,
                                     -19.965626399359778,-9.761220034345175,-0.009675776079236078,
                                     -8.793180471674402e-7,-4.675655622579791e-14,-3.291874702847949e-14,
                                     -3.0408119421278934e-14,-2.2328449483329286e-14,
                                     -1.0693016765542536e-14,-7.240806196368183e-15,
                                     -5.761950647030487e-15,-5.957409950992936e-16,3.2491920424847875e-15,
                                     9.219910025371442e-15,1.2799168101873962e-14,1.7108898977066272e-14,
                                     2.855032832850196e-14,4.1048723593164446e-14,9.99499970701814,
                                     18.470571911228987,30.51830887197781,33.54443547978839,
                                     36.1607309907248,38.57320314001222,40.85924266944485,
                                     43.035391586290125,45.287513370945284,47.151147140980484,
                                     50.73276704593593,52.39952223577173,59.50068014643575,
                                     61.31379159039717,74.51253995296709,77.13729578952395,
                                     94.37203226002364,99.43109411760354,102.6487394625419,
                                     105.06218043709777,107.8179491492106,110.12126119755328,
                                     113.57945167126782,116.37801423876415,122.99832759829658,
                                     125.12209476580418,138.05692785144052,140.21814403606146,
                                     161.31660187081715,164.54676840029904,175.43647569692263,
                                     192.3282320950376,207.53405143733352,212.25537253729775,
                                     284.3628999051823,294.6884623476941,341.98290548531804,
                                     507.44043882416463,534.3382764375148,967.6392677402239,
                                     1875.507871717115,3171.7801027349824,5101.18005605357
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -10.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//===========//
// test # 91 //
//===========//

TEST(MatrixTest91, EigenvaluesDSYEVX1)
{
     MatrixDense<double> matA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 5) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 5) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvaluesDSYEVX(eigenvaluesA);

     // test

     const double arr_values[100] = {-323.38478082263765, -318.95649570052416, -259.5812044929057,
                                     -255.47902023149175, -210.8574832531544, -207.046095983574,
                                     -172.17668982103106, -169.63822144706373, -167.11362728657673,
                                     -164.65335127248554, -137.93078894722365, -135.63554168942136,
                                     -133.27655590262873, -130.90313825750582, -111.16491214977447,
                                     -108.69201485977163, -104.86333072392988, -102.09654131372476,
                                     -89.52930445237439, -87.11011352113846, -79.24098389717554,
                                     -76.62676551546362, -71.01471245774239, -68.78074078937429,
                                     -57.39730933358366, -55.597048251741185, -53.807888693814036,
                                     -52.082337027364105, -42.001162570408376, -40.10702874995469,
                                     -36.13941170041208, -34.10866137118017, -29.537625276581757,
                                     -27.807512581350473, -20.493485216120316, -19.180726411630474,
                                     -17.86587524596272, -16.594313970872253, -10.527129255160773,
                                     -9.235915164234662, -6.274431175607891, -4.736234460618425,
                                     -3.113902006580256, -2.3717281394985243, -0.07129875210819334,
                                     2.4178759119876623, 3.3310614360036803, 4.154955748257782,
                                     8.633411865685325, 10.366556352869488, 12.095377640289747,
                                     14.018873556316715, 17.48114915683801, 21.917608573061386,
                                     23.316848960689295, 27.074595655585338, 31.322004721507344,
                                     34.9238763246869, 36.70168716147582, 39.12924266179792,
                                     48.291435480841905, 50.073858853444456, 52.12562986320187,
                                     57.27293729356433, 64.70819943681298, 66.93714122637961,
                                     68.82639047739008, 80.9665198923797, 83.5742202822415,
                                     86.58950836310686, 89.44951473838972, 102.06705367044331,
                                     107.13994334023343, 109.44821746324266, 117.55286230732882,
                                     126.41194103599305, 133.5454916018076, 135.893724579225,
                                     151.52771532059967, 157.8972863072518, 165.22044899155756,
                                     167.77180305861472, 190.84742611813815, 205.77684982675112,
                                     208.5174101551291, 234.35823130936174, 279.92197126989515,
                                     330.06056030550724, 384.6942438609073, 444.0868835363212,
                                     508.62422446622827, 578.8097468653175, 655.2951900145777,
                                     738.9379634908137, 830.8998363959786, 932.8239020042909,
                                     1047.1805607335104, 1178.0384559097015, 1333.1755507750008,
                                     1532.5774637949444
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -5.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//===========//
// test # 92 //
//===========//

TEST(MatrixTest92, EigenvaluesDSYEVX2)
{
     MatrixDense<double> matA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 20) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 20) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvaluesDSYEVX(eigenvaluesA);

     // test

     const double arr_values[100] = {-890.0679762687605,-833.96613152625,-387.9183497645402,
                                     -374.28858505936836,-325.97417429278767,-290.39317003462327,
                                     -243.09349980476634,-237.9671765884564,-184.65425879239723,
                                     -181.6433889406905,-152.42678092373745,-150.01440312367828,
                                     -140.20043836510496,-134.26239413119825,-130.50508513840435,
                                     -128.1365612192653,-119.6243787096752,-117.25234191992651,
                                     -112.47792700587912,-108.89915545206071,-104.74759545668331,
                                     -100.22968339474058,-94.96995783385485,-89.69665862307328,
                                     -86.4677796222291,-68.14673474241698,-66.07309844841615,
                                     -56.302362373674626,-54.63592515665142,-49.54128744936952,
                                     -47.77783428990662,-45.159157747842364,-43.112766536500985,
                                     -40.904881657341534,-38.624975888999664,-36.21821364136139,
                                     -33.61319247902676,-30.618705849873916,-26.992630355951256,
                                     -19.965626399359778,-9.761220034345175,-0.009675776079236078,
                                     -8.793180471674402e-7,-4.675655622579791e-14,-3.291874702847949e-14,
                                     -3.0408119421278934e-14,-2.2328449483329286e-14,
                                     -1.0693016765542536e-14,-7.240806196368183e-15,
                                     -5.761950647030487e-15,-5.957409950992936e-16,3.2491920424847875e-15,
                                     9.219910025371442e-15,1.2799168101873962e-14,1.7108898977066272e-14,
                                     2.855032832850196e-14,4.1048723593164446e-14,9.99499970701814,
                                     18.470571911228987,30.51830887197781,33.54443547978839,
                                     36.1607309907248,38.57320314001222,40.85924266944485,
                                     43.035391586290125,45.287513370945284,47.151147140980484,
                                     50.73276704593593,52.39952223577173,59.50068014643575,
                                     61.31379159039717,74.51253995296709,77.13729578952395,
                                     94.37203226002364,99.43109411760354,102.6487394625419,
                                     105.06218043709777,107.8179491492106,110.12126119755328,
                                     113.57945167126782,116.37801423876415,122.99832759829658,
                                     125.12209476580418,138.05692785144052,140.21814403606146,
                                     161.31660187081715,164.54676840029904,175.43647569692263,
                                     192.3282320950376,207.53405143733352,212.25537253729775,
                                     284.3628999051823,294.6884623476941,341.98290548531804,
                                     507.44043882416463,534.3382764375148,967.6392677402239,
                                     1875.507871717115,3171.7801027349824,5101.18005605357
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -5.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//===========//
// test # 93 //
//===========//

TEST(MatrixTest93, EigenvectorsDSYEVX1)
{
     MatrixDense<double> matA;
     MatrixDense<double> eigenvectorsA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 5) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 5) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvectorsDSYEVX(eigenvectorsA);

     // test

     const double arr_values99[100] = {3.6942314255214093e-22,
                                       1.9280574390556084e-21,
                                       8.336207484683114e-21,
                                       3.201738809930229e-20,
                                       1.128792365433472e-19,
                                       3.7219958073502635e-19,
                                       1.1616570563876596e-18,
                                       3.4603856776191363e-18,
                                       9.898106164891213e-18,
                                       2.7313034552506115e-17,
                                       7.297235551812733e-17,
                                       1.8931798526470812e-16,
                                       4.781008453916819e-16,
                                       1.1776583982769647e-15,
                                       2.8342405979299284e-15,
                                       6.674396729414062e-15,
                                       1.5399303294661802e-14,
                                       3.4849026635393604e-14,
                                       7.743020050094637e-14,
                                       1.6906046576799098e-13,
                                       3.630170569572799e-13,
                                       7.671355827326599e-13,
                                       1.5964469231753074e-12,
                                       3.2735991022182145e-12,
                                       6.617806646334326e-12,
                                       1.3195608156949319e-11,
                                       2.596350061894761e-11,
                                       5.0430333328453936e-11,
                                       9.67336578559975e-11,
                                       1.8330342627363303e-10,
                                       3.4324925188064113e-10,
                                       6.353651498186134e-10,
                                       1.1628708815038037e-9,
                                       2.104965971919157e-9,
                                       3.7693601315726985e-9,
                                       6.6787563519161844e-9,
                                       1.1711672807554324e-8,
                                       2.03292597117336e-8,
                                       3.4936698501777865e-8,
                                       5.9452893015691615e-8,
                                       1.0019897315247485e-7,
                                       1.672693861786821e-7,
                                       2.7662540139461026e-7,
                                       4.5325802559019753e-7,
                                       7.359163073250683e-7,
                                       1.1841012055800893e-6,
                                       1.8883009979218705e-6,
                                       2.984811021145573e-6,
                                       4.676960526639804e-6,
                                       7.265186654807506e-6,
                                       0.000011189174362740128,
                                       0.00001708624363917233,
                                       0.000025871327250800013,
                                       0.00003884524735147213,
                                       0.000057839551835632114,
                                       0.0000854078666854305,
                                       0.00012507547298611522,
                                       0.0001816604867751283,
                                       0.0002616813987570242,
                                       0.00037386653150919884,
                                       0.0005297808051872514,
                                       0.0007445835839285321,
                                       0.0010379277320564163,
                                       0.0014350037062010874,
                                       0.001967722735545796,
                                       0.0026760192519511626,
                                       0.0036092342363378766,
                                       0.00482751783582054,
                                       0.00640315989292051,
                                       0.008421723355192411,
                                       0.010982819682174948,
                                       0.014200329986124536,
                                       0.018201824732492763,
                                       0.023126912983358,
                                       0.029124247016656085,
                                       0.03634692535629019,
                                       0.04494587964943125,
                                       0.055061139884829774,
                                       0.0668110106541383,
                                       0.08027936243852593,
                                       0.09549945152005936,
                                       0.11243731017783884,
                                       0.13097574227719463,
                                       0.15090013051721174,
                                       0.17186879268938443,
                                       0.19341147439162046,
                                       0.21493488140894385,
                                       0.23573601738701488,
                                       0.2548580930562397,
                                       0.27133687410309687,
                                       0.28424352285526244,
                                       0.29273026481733555,
                                       0.29466646943807095,
                                       0.28996990773813897,
                                       0.27878021480447934,
                                       0.26146670556930435,
                                       0.22712948196564564,
                                       0.19198046679049208,
                                       0.15683996268683814,
                                       0.1225409827758915
                                      };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,99)) - abs(arr_values99[i])) < pow(10.0, -10.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//===========//
// test # 94 //
//===========//

TEST(MatrixTest94, EigenvectorsDSYEVX2)
{
     MatrixDense<double> matA;
     MatrixDense<double> eigenvectorsA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 20) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 20) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvectorsDSYEVX(eigenvectorsA);

     // test data # 1

     const double arr_values99[100] = {-0.00002379437463800071,-0.0000321126535355192,
                                       -0.00004261209009056319,-0.00005574228105018768,
                                       -0.0000720258070448845,-0.00009206699311457821,
                                       -0.00011656127426176481,-0.00014630515846035676,
                                       -0.0001822067730093074,-0.00022529697290878234,
                                       -0.00027674098203948964,-0.0003378505293342626,
                                       -0.00041009643284298355,-0.0004951215746125915,
                                       -0.0005947541986465926,-0.0007110214528956958,-0.0008461630842956044,
                                       -0.001002645183353377,-0.0011831738617460526,-0.0013907087329034266,
                                       -0.0016283827619966063,-0.0018997449209395513,-0.002208573606467658,
                                       -0.002558944309118129,-0.0029552087127267114,-0.0034019962501085446,
                                       -0.003904213875493434,-0.00446704386484559,-0.00509593944935361,
                                       -0.005796618082542783,-0.006575052137809326,-0.007437456830885415,
                                       -0.00839027516100404,-0.009440159665541611,-0.010593950785877937,
                                       -0.01185865164733851,-0.013241399063586366,-0.014749430585927434,
                                       -0.0163900474309,-0.018170573135449803,-0.0200983089054282,
                                       -0.022180481856340313,-0.024424190854787288,-0.026834080780647522,
                                       -0.02941440937535356,-0.03216899598488147,-0.035101168178730414,
                                       -0.03821370638490854,-0.04150878671030463,-0.04498792214706611,
                                       -0.04865190239866414,-0.05250073259412269,-0.056533571195309565,
                                       -0.06074866744009344,-0.0651432987033876,-0.06971370819842077,
                                       -0.07445504348175375,-0.07936129626731185,-0.08442524409670342,
                                       -0.08963839445497533,-0.09499093194079421,-0.10047166921101545,
                                       -0.10595789789842808,-0.11143628534678302,-0.11689273862688677,
                                       -0.1223124096438104,-0.12767970465965733,-0.13297829857254914,
                                       -0.13819115429332646,-0.1433005475596976,-0.14828809752292466,
                                       -0.15313480343431787,-0.15782108774752324,-0.16232684593753477,
                                       -0.16663150331824728,-0.17071407911689127,-0.17455325803557586,
                                       -0.17812746949713273,-0.18141497473424503,-0.18439396183722762,
                                       -0.18704264882716626,-0.18541168339207653,-0.18356676120058393,
                                       -0.18150272926411598,-0.17921485423076686,-0.17669886945503793,
                                       -0.17395102341224067,-0.17096812928970723,-0.1677476155625255,
                                       -0.1642875773360043,-0.16058682821057293,-0.15664495239745393,
                                       -0.15246235678535205,-0.14804032262974395,-0.14338105650732136,
                                       -0.13848774014895246,-0.1333645787354236,-0.128016847211483,
                                       -0.1224509341456258,-0.11667438263597216
                                      };

     // test data # 2

     const double arr_values98[100] = {0.0005490815006598427,0.0007217004212979288,0.0009319727820738912,
                                       0.0011856764796110383,0.0014891369301256077,0.0018492353084341568,
                                       0.002273410759398609,0.002769655716932461,0.003346503443557062,
                                       0.00401300689217423,0.0047787079930774546,0.00565359648519078,
                                       0.006648057443072511,0.007772806702325777,0.009038813457661714,
                                       0.010457209401826704,0.012039183891666605,0.013795864771304657,
                                       0.01573818465305239,0.01787673265523282,0.020218129530799774,
                                       0.022773522732384158,0.02555088599547039,0.02855699048006569,
                                       0.03179740706616532,0.03527631554658473,0.038996305859039175,
                                       0.04295817276333242,0.047160705650423344,0.05160047546600458,
                                       0.056271621039461366,0.061165637426456754,0.06627116919601259,
                                       0.07157381191620707,0.0770559254111312,0.08269646266941802,
                                       0.08847081857459133,0.09435070289203276,0.10030404217816305,
                                       0.10629491546542182,0.11228359420772163,0.11822646848984367,
                                       0.12407615562938958,0.12979275139874066,0.13533385216700752,
                                       0.14065467360130016,0.14570820649639804,0.1504454123825699,
                                       0.15481546139101132,0.15876601462676446,0.16224355300490953,
                                       0.1651937541424425,0.16756191846119972,0.16929344514268863,
                                       0.17033435798074814,0.17063188050057534,0.17013505895196374,
                                       0.16879543094112387,0.16656773654130375,0.16341066772151153,
                                       0.1592876487959431,0.15416764632985688,0.14836208495033895,
                                       0.14186022973422263,0.1346552528267162,0.12674464039743252,
                                       0.118130599994188,0.10882046380600134,0.09882708283296263,
                                       0.08816920644738636,0.07687184132594177,0.06496658324539006,
                                       0.052491914775380344,0.039493461481783246,0.026024198885649517,
                                       0.012144602119448178,-0.002077270001960313,-0.0165657649080691,
                                       -0.0312377086295896,-0.04600256891175273,-0.06076271707085829,
                                       -0.06795927022946352,-0.07503806683258157,-0.0819579039270416,
                                       -0.08867572326732134,-0.09514679596043041,-0.1013249415936529,
                                       -0.10716278331612594,-0.1126120400798519,-0.11762385692798413,
                                       -0.12214917384544385,-0.12613913325700105,-0.12954552576940606,
                                       -0.1323212732062947,-0.13442094737761986,-0.13580132236057105,
                                       -0.13642195734879647,-0.13624580635507777,-0.1352398502347542,
                                       -0.13337574564008783
                                      };

     // test data # 3

     const double arr_values97[100] = {-0.0048844047935863765,-0.006236486880389232,-0.007815274325331554,
                                       -0.009640183890712983,-0.011730050533458039,-0.014102771342375649,
                                       -0.016774922205544347,-0.019761352037216788,-0.023074760743478643,
                                       -0.026725268499719908,-0.03071998531275152,-0.03506259120033407,
                                       -0.03975293858755401,-0.04478668963230909,-0.050155002084304526,
                                       -0.05584427788164485,-0.06183598892150303,-0.06810659423095557,
                                       -0.07462756203769172,-0.08136550893055443,-0.08823038013976417,
                                       -0.09520849071940404,-0.1022476308045662,-0.10929156383600795,
                                       -0.11627847844623534,-0.12314131678294159,-0.1298082479646105,
                                       -0.13620329680129123,-0.14224713548726084,-0.14785804289917964,
                                       -0.15295303238959757,-0.1574491445459654,-0.1612648963069822,
                                       -0.16432187213530286,-0.16654643671328404,-0.16787154196597193,
                                       -0.16823859426980445,-0.16759934066305365,-0.16591772596198778,
                                       -0.1631716661722737,-0.15935634408053131,-0.15448321146294608,
                                       -0.14858218139776455,-0.1416381780493364,-0.1336490652093021,
                                       -0.12462715724623677,-0.11460064260169803,-0.10361487512764765,
                                       -0.09173348472751607,-0.07903925562502323,-0.06563471836746396,
                                       -0.051642400613860905,-0.03720468211196078,-0.022483201281379775,
                                       -0.007657764735299888,0.007075282886553215,0.021505263042677962,
                                       0.03540996481701064,0.04855905391412624,0.06071808324036558,
                                       0.07165316959606502,0.08113608035225883,0.09009849834439126,
                                       0.09838663571146652,0.10584187751947657,0.11230295059241002,
                                       0.11760842867321217,0.12159956720711124,0.12412345180969263,
                                       0.1250364341022301,0.12420781716730643,0.12152374055295379,
                                       0.11689120174753254,0.11024213763900717,0.10153747600764697,
                                       0.09077105399613189,0.07797328823931185,0.06321447046527728,
                                       0.04660755350441126,0.028310286413503014,0.008526547885299433,
                                       0.0025719295116293645,-0.004200598749307477,-0.011762440439929252,
                                       -0.020069965854842452,-0.029063538293552106,-0.038666727752430484,
                                       -0.048785756486008615,-0.05930922217827011,-0.07010814475633873,
                                       -0.08103638195706307,-0.09193145642839495,-0.10261583322770457,
                                       -0.11289868090088843,-0.12257814174844638,-0.1314441273038225,
                                       -0.139281643409791,-0.14587463558538188,-0.15101032970590494,
                                       -0.15448402553323257
                                      };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,99)) - abs(arr_values99[i])) < pow(10.0, -10.0));
     }

     // test # 2

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,98)) - abs(arr_values98[i])) < pow(10.0, -10.0));
     }

     // test # 3

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,97)) - abs(arr_values97[i])) < pow(10.0, -10.0));
     }

     // test # 4

     EXPECT_TRUE(res == 0);
}

//===========//
// test # 95 //
//===========//

TEST(MatrixTest95, EigensystemDSYEV1)
{
     MatrixDense<double> matA;
     MatrixDense<double> eigenvectorsA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 5) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 5) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigensystemDSYEV(eigenvectorsA, eigenvaluesA);

     // test

     const double arr_values[100] = {-323.38478082263765, -318.95649570052416, -259.5812044929057,
                                     -255.47902023149175, -210.8574832531544, -207.046095983574,
                                     -172.17668982103106, -169.63822144706373, -167.11362728657673,
                                     -164.65335127248554, -137.93078894722365, -135.63554168942136,
                                     -133.27655590262873, -130.90313825750582, -111.16491214977447,
                                     -108.69201485977163, -104.86333072392988, -102.09654131372476,
                                     -89.52930445237439, -87.11011352113846, -79.24098389717554,
                                     -76.62676551546362, -71.01471245774239, -68.78074078937429,
                                     -57.39730933358366, -55.597048251741185, -53.807888693814036,
                                     -52.082337027364105, -42.001162570408376, -40.10702874995469,
                                     -36.13941170041208, -34.10866137118017, -29.537625276581757,
                                     -27.807512581350473, -20.493485216120316, -19.180726411630474,
                                     -17.86587524596272, -16.594313970872253, -10.527129255160773,
                                     -9.235915164234662, -6.274431175607891, -4.736234460618425,
                                     -3.113902006580256, -2.3717281394985243, -0.07129875210819334,
                                     2.4178759119876623, 3.3310614360036803, 4.154955748257782,
                                     8.633411865685325, 10.366556352869488, 12.095377640289747,
                                     14.018873556316715, 17.48114915683801, 21.917608573061386,
                                     23.316848960689295, 27.074595655585338, 31.322004721507344,
                                     34.9238763246869, 36.70168716147582, 39.12924266179792,
                                     48.291435480841905, 50.073858853444456, 52.12562986320187,
                                     57.27293729356433, 64.70819943681298, 66.93714122637961,
                                     68.82639047739008, 80.9665198923797, 83.5742202822415,
                                     86.58950836310686, 89.44951473838972, 102.06705367044331,
                                     107.13994334023343, 109.44821746324266, 117.55286230732882,
                                     126.41194103599305, 133.5454916018076, 135.893724579225,
                                     151.52771532059967, 157.8972863072518, 165.22044899155756,
                                     167.77180305861472, 190.84742611813815, 205.77684982675112,
                                     208.5174101551291, 234.35823130936174, 279.92197126989515,
                                     330.06056030550724, 384.6942438609073, 444.0868835363212,
                                     508.62422446622827, 578.8097468653175, 655.2951900145777,
                                     738.9379634908137, 830.8998363959786, 932.8239020042909,
                                     1047.1805607335104, 1178.0384559097015, 1333.1755507750008,
                                     1532.5774637949444
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -10.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//===========//
// test # 96 //
//===========//

TEST(MatrixTest96, EigensystemDSYEV2)
{
     MatrixDense<double> matA;
     MatrixDense<double> eigenvectorsA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 20) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 20) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigensystemDSYEV(eigenvectorsA, eigenvaluesA);

     // test

     const double arr_values[100] = {-890.0679762687605,-833.96613152625,-387.9183497645402,
                                     -374.28858505936836,-325.97417429278767,-290.39317003462327,
                                     -243.09349980476634,-237.9671765884564,-184.65425879239723,
                                     -181.6433889406905,-152.42678092373745,-150.01440312367828,
                                     -140.20043836510496,-134.26239413119825,-130.50508513840435,
                                     -128.1365612192653,-119.6243787096752,-117.25234191992651,
                                     -112.47792700587912,-108.89915545206071,-104.74759545668331,
                                     -100.22968339474058,-94.96995783385485,-89.69665862307328,
                                     -86.4677796222291,-68.14673474241698,-66.07309844841615,
                                     -56.302362373674626,-54.63592515665142,-49.54128744936952,
                                     -47.77783428990662,-45.159157747842364,-43.112766536500985,
                                     -40.904881657341534,-38.624975888999664,-36.21821364136139,
                                     -33.61319247902676,-30.618705849873916,-26.992630355951256,
                                     -19.965626399359778,-9.761220034345175,-0.009675776079236078,
                                     -8.793180471674402e-7,-4.675655622579791e-14,-3.291874702847949e-14,
                                     -3.0408119421278934e-14,-2.2328449483329286e-14,
                                     -1.0693016765542536e-14,-7.240806196368183e-15,
                                     -5.761950647030487e-15,-5.957409950992936e-16,3.2491920424847875e-15,
                                     9.219910025371442e-15,1.2799168101873962e-14,1.7108898977066272e-14,
                                     2.855032832850196e-14,4.1048723593164446e-14,9.99499970701814,
                                     18.470571911228987,30.51830887197781,33.54443547978839,
                                     36.1607309907248,38.57320314001222,40.85924266944485,
                                     43.035391586290125,45.287513370945284,47.151147140980484,
                                     50.73276704593593,52.39952223577173,59.50068014643575,
                                     61.31379159039717,74.51253995296709,77.13729578952395,
                                     94.37203226002364,99.43109411760354,102.6487394625419,
                                     105.06218043709777,107.8179491492106,110.12126119755328,
                                     113.57945167126782,116.37801423876415,122.99832759829658,
                                     125.12209476580418,138.05692785144052,140.21814403606146,
                                     161.31660187081715,164.54676840029904,175.43647569692263,
                                     192.3282320950376,207.53405143733352,212.25537253729775,
                                     284.3628999051823,294.6884623476941,341.98290548531804,
                                     507.44043882416463,534.3382764375148,967.6392677402239,
                                     1875.507871717115,3171.7801027349824,5101.18005605357
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -10.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//===========//
// test # 97 //
//===========//

TEST(MatrixTest97, EigenvaluesDSYEV1)
{
     MatrixDense<double> matA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 5) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 5) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvaluesDSYEV(eigenvaluesA);

     // test

     const double arr_values[100] = {-323.38478082263765, -318.95649570052416, -259.5812044929057,
                                     -255.47902023149175, -210.8574832531544, -207.046095983574,
                                     -172.17668982103106, -169.63822144706373, -167.11362728657673,
                                     -164.65335127248554, -137.93078894722365, -135.63554168942136,
                                     -133.27655590262873, -130.90313825750582, -111.16491214977447,
                                     -108.69201485977163, -104.86333072392988, -102.09654131372476,
                                     -89.52930445237439, -87.11011352113846, -79.24098389717554,
                                     -76.62676551546362, -71.01471245774239, -68.78074078937429,
                                     -57.39730933358366, -55.597048251741185, -53.807888693814036,
                                     -52.082337027364105, -42.001162570408376, -40.10702874995469,
                                     -36.13941170041208, -34.10866137118017, -29.537625276581757,
                                     -27.807512581350473, -20.493485216120316, -19.180726411630474,
                                     -17.86587524596272, -16.594313970872253, -10.527129255160773,
                                     -9.235915164234662, -6.274431175607891, -4.736234460618425,
                                     -3.113902006580256, -2.3717281394985243, -0.07129875210819334,
                                     2.4178759119876623, 3.3310614360036803, 4.154955748257782,
                                     8.633411865685325, 10.366556352869488, 12.095377640289747,
                                     14.018873556316715, 17.48114915683801, 21.917608573061386,
                                     23.316848960689295, 27.074595655585338, 31.322004721507344,
                                     34.9238763246869, 36.70168716147582, 39.12924266179792,
                                     48.291435480841905, 50.073858853444456, 52.12562986320187,
                                     57.27293729356433, 64.70819943681298, 66.93714122637961,
                                     68.82639047739008, 80.9665198923797, 83.5742202822415,
                                     86.58950836310686, 89.44951473838972, 102.06705367044331,
                                     107.13994334023343, 109.44821746324266, 117.55286230732882,
                                     126.41194103599305, 133.5454916018076, 135.893724579225,
                                     151.52771532059967, 157.8972863072518, 165.22044899155756,
                                     167.77180305861472, 190.84742611813815, 205.77684982675112,
                                     208.5174101551291, 234.35823130936174, 279.92197126989515,
                                     330.06056030550724, 384.6942438609073, 444.0868835363212,
                                     508.62422446622827, 578.8097468653175, 655.2951900145777,
                                     738.9379634908137, 830.8998363959786, 932.8239020042909,
                                     1047.1805607335104, 1178.0384559097015, 1333.1755507750008,
                                     1532.5774637949444
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -5.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//===========//
// test # 98 //
//===========//

TEST(MatrixTest98, EigenvaluesDSYEV2)
{
     MatrixDense<double> matA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 20) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 20) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvaluesDSYEV(eigenvaluesA);

     // test

     const double arr_values[100] = {-890.0679762687605,-833.96613152625,-387.9183497645402,
                                     -374.28858505936836,-325.97417429278767,-290.39317003462327,
                                     -243.09349980476634,-237.9671765884564,-184.65425879239723,
                                     -181.6433889406905,-152.42678092373745,-150.01440312367828,
                                     -140.20043836510496,-134.26239413119825,-130.50508513840435,
                                     -128.1365612192653,-119.6243787096752,-117.25234191992651,
                                     -112.47792700587912,-108.89915545206071,-104.74759545668331,
                                     -100.22968339474058,-94.96995783385485,-89.69665862307328,
                                     -86.4677796222291,-68.14673474241698,-66.07309844841615,
                                     -56.302362373674626,-54.63592515665142,-49.54128744936952,
                                     -47.77783428990662,-45.159157747842364,-43.112766536500985,
                                     -40.904881657341534,-38.624975888999664,-36.21821364136139,
                                     -33.61319247902676,-30.618705849873916,-26.992630355951256,
                                     -19.965626399359778,-9.761220034345175,-0.009675776079236078,
                                     -8.793180471674402e-7,-4.675655622579791e-14,-3.291874702847949e-14,
                                     -3.0408119421278934e-14,-2.2328449483329286e-14,
                                     -1.0693016765542536e-14,-7.240806196368183e-15,
                                     -5.761950647030487e-15,-5.957409950992936e-16,3.2491920424847875e-15,
                                     9.219910025371442e-15,1.2799168101873962e-14,1.7108898977066272e-14,
                                     2.855032832850196e-14,4.1048723593164446e-14,9.99499970701814,
                                     18.470571911228987,30.51830887197781,33.54443547978839,
                                     36.1607309907248,38.57320314001222,40.85924266944485,
                                     43.035391586290125,45.287513370945284,47.151147140980484,
                                     50.73276704593593,52.39952223577173,59.50068014643575,
                                     61.31379159039717,74.51253995296709,77.13729578952395,
                                     94.37203226002364,99.43109411760354,102.6487394625419,
                                     105.06218043709777,107.8179491492106,110.12126119755328,
                                     113.57945167126782,116.37801423876415,122.99832759829658,
                                     125.12209476580418,138.05692785144052,140.21814403606146,
                                     161.31660187081715,164.54676840029904,175.43647569692263,
                                     192.3282320950376,207.53405143733352,212.25537253729775,
                                     284.3628999051823,294.6884623476941,341.98290548531804,
                                     507.44043882416463,534.3382764375148,967.6392677402239,
                                     1875.507871717115,3171.7801027349824,5101.18005605357
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -5.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//===========//
// test # 99 //
//===========//

TEST(MatrixTest99, EigenvectorsDSYEV1)
{
     MatrixDense<double> matA;
     MatrixDense<double> eigenvectorsA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 5) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 5) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvectorsDSYEV(eigenvectorsA);

     // test

     const double arr_values99[100] = {3.6942314255214093e-22,
                                       1.9280574390556084e-21,
                                       8.336207484683114e-21,
                                       3.201738809930229e-20,
                                       1.128792365433472e-19,
                                       3.7219958073502635e-19,
                                       1.1616570563876596e-18,
                                       3.4603856776191363e-18,
                                       9.898106164891213e-18,
                                       2.7313034552506115e-17,
                                       7.297235551812733e-17,
                                       1.8931798526470812e-16,
                                       4.781008453916819e-16,
                                       1.1776583982769647e-15,
                                       2.8342405979299284e-15,
                                       6.674396729414062e-15,
                                       1.5399303294661802e-14,
                                       3.4849026635393604e-14,
                                       7.743020050094637e-14,
                                       1.6906046576799098e-13,
                                       3.630170569572799e-13,
                                       7.671355827326599e-13,
                                       1.5964469231753074e-12,
                                       3.2735991022182145e-12,
                                       6.617806646334326e-12,
                                       1.3195608156949319e-11,
                                       2.596350061894761e-11,
                                       5.0430333328453936e-11,
                                       9.67336578559975e-11,
                                       1.8330342627363303e-10,
                                       3.4324925188064113e-10,
                                       6.353651498186134e-10,
                                       1.1628708815038037e-9,
                                       2.104965971919157e-9,
                                       3.7693601315726985e-9,
                                       6.6787563519161844e-9,
                                       1.1711672807554324e-8,
                                       2.03292597117336e-8,
                                       3.4936698501777865e-8,
                                       5.9452893015691615e-8,
                                       1.0019897315247485e-7,
                                       1.672693861786821e-7,
                                       2.7662540139461026e-7,
                                       4.5325802559019753e-7,
                                       7.359163073250683e-7,
                                       1.1841012055800893e-6,
                                       1.8883009979218705e-6,
                                       2.984811021145573e-6,
                                       4.676960526639804e-6,
                                       7.265186654807506e-6,
                                       0.000011189174362740128,
                                       0.00001708624363917233,
                                       0.000025871327250800013,
                                       0.00003884524735147213,
                                       0.000057839551835632114,
                                       0.0000854078666854305,
                                       0.00012507547298611522,
                                       0.0001816604867751283,
                                       0.0002616813987570242,
                                       0.00037386653150919884,
                                       0.0005297808051872514,
                                       0.0007445835839285321,
                                       0.0010379277320564163,
                                       0.0014350037062010874,
                                       0.001967722735545796,
                                       0.0026760192519511626,
                                       0.0036092342363378766,
                                       0.00482751783582054,
                                       0.00640315989292051,
                                       0.008421723355192411,
                                       0.010982819682174948,
                                       0.014200329986124536,
                                       0.018201824732492763,
                                       0.023126912983358,
                                       0.029124247016656085,
                                       0.03634692535629019,
                                       0.04494587964943125,
                                       0.055061139884829774,
                                       0.0668110106541383,
                                       0.08027936243852593,
                                       0.09549945152005936,
                                       0.11243731017783884,
                                       0.13097574227719463,
                                       0.15090013051721174,
                                       0.17186879268938443,
                                       0.19341147439162046,
                                       0.21493488140894385,
                                       0.23573601738701488,
                                       0.2548580930562397,
                                       0.27133687410309687,
                                       0.28424352285526244,
                                       0.29273026481733555,
                                       0.29466646943807095,
                                       0.28996990773813897,
                                       0.27878021480447934,
                                       0.26146670556930435,
                                       0.22712948196564564,
                                       0.19198046679049208,
                                       0.15683996268683814,
                                       0.1225409827758915
                                      };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,99)) - abs(arr_values99[i])) < pow(10.0, -10.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//============//
// test # 100 //
//============//

TEST(MatrixTest100, EigenvectorsDSYEV2)
{
     MatrixDense<double> matA;
     MatrixDense<double> eigenvectorsA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 20) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 20) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvectorsDSYEV(eigenvectorsA);

     // test data # 1

     const double arr_values99[100] = {-0.00002379437463800071,-0.0000321126535355192,
                                       -0.00004261209009056319,-0.00005574228105018768,
                                       -0.0000720258070448845,-0.00009206699311457821,
                                       -0.00011656127426176481,-0.00014630515846035676,
                                       -0.0001822067730093074,-0.00022529697290878234,
                                       -0.00027674098203948964,-0.0003378505293342626,
                                       -0.00041009643284298355,-0.0004951215746125915,
                                       -0.0005947541986465926,-0.0007110214528956958,-0.0008461630842956044,
                                       -0.001002645183353377,-0.0011831738617460526,-0.0013907087329034266,
                                       -0.0016283827619966063,-0.0018997449209395513,-0.002208573606467658,
                                       -0.002558944309118129,-0.0029552087127267114,-0.0034019962501085446,
                                       -0.003904213875493434,-0.00446704386484559,-0.00509593944935361,
                                       -0.005796618082542783,-0.006575052137809326,-0.007437456830885415,
                                       -0.00839027516100404,-0.009440159665541611,-0.010593950785877937,
                                       -0.01185865164733851,-0.013241399063586366,-0.014749430585927434,
                                       -0.0163900474309,-0.018170573135449803,-0.0200983089054282,
                                       -0.022180481856340313,-0.024424190854787288,-0.026834080780647522,
                                       -0.02941440937535356,-0.03216899598488147,-0.035101168178730414,
                                       -0.03821370638490854,-0.04150878671030463,-0.04498792214706611,
                                       -0.04865190239866414,-0.05250073259412269,-0.056533571195309565,
                                       -0.06074866744009344,-0.0651432987033876,-0.06971370819842077,
                                       -0.07445504348175375,-0.07936129626731185,-0.08442524409670342,
                                       -0.08963839445497533,-0.09499093194079421,-0.10047166921101545,
                                       -0.10595789789842808,-0.11143628534678302,-0.11689273862688677,
                                       -0.1223124096438104,-0.12767970465965733,-0.13297829857254914,
                                       -0.13819115429332646,-0.1433005475596976,-0.14828809752292466,
                                       -0.15313480343431787,-0.15782108774752324,-0.16232684593753477,
                                       -0.16663150331824728,-0.17071407911689127,-0.17455325803557586,
                                       -0.17812746949713273,-0.18141497473424503,-0.18439396183722762,
                                       -0.18704264882716626,-0.18541168339207653,-0.18356676120058393,
                                       -0.18150272926411598,-0.17921485423076686,-0.17669886945503793,
                                       -0.17395102341224067,-0.17096812928970723,-0.1677476155625255,
                                       -0.1642875773360043,-0.16058682821057293,-0.15664495239745393,
                                       -0.15246235678535205,-0.14804032262974395,-0.14338105650732136,
                                       -0.13848774014895246,-0.1333645787354236,-0.128016847211483,
                                       -0.1224509341456258,-0.11667438263597216
                                      };

     // test data # 2

     const double arr_values98[100] = {0.0005490815006598427,0.0007217004212979288,0.0009319727820738912,
                                       0.0011856764796110383,0.0014891369301256077,0.0018492353084341568,
                                       0.002273410759398609,0.002769655716932461,0.003346503443557062,
                                       0.00401300689217423,0.0047787079930774546,0.00565359648519078,
                                       0.006648057443072511,0.007772806702325777,0.009038813457661714,
                                       0.010457209401826704,0.012039183891666605,0.013795864771304657,
                                       0.01573818465305239,0.01787673265523282,0.020218129530799774,
                                       0.022773522732384158,0.02555088599547039,0.02855699048006569,
                                       0.03179740706616532,0.03527631554658473,0.038996305859039175,
                                       0.04295817276333242,0.047160705650423344,0.05160047546600458,
                                       0.056271621039461366,0.061165637426456754,0.06627116919601259,
                                       0.07157381191620707,0.0770559254111312,0.08269646266941802,
                                       0.08847081857459133,0.09435070289203276,0.10030404217816305,
                                       0.10629491546542182,0.11228359420772163,0.11822646848984367,
                                       0.12407615562938958,0.12979275139874066,0.13533385216700752,
                                       0.14065467360130016,0.14570820649639804,0.1504454123825699,
                                       0.15481546139101132,0.15876601462676446,0.16224355300490953,
                                       0.1651937541424425,0.16756191846119972,0.16929344514268863,
                                       0.17033435798074814,0.17063188050057534,0.17013505895196374,
                                       0.16879543094112387,0.16656773654130375,0.16341066772151153,
                                       0.1592876487959431,0.15416764632985688,0.14836208495033895,
                                       0.14186022973422263,0.1346552528267162,0.12674464039743252,
                                       0.118130599994188,0.10882046380600134,0.09882708283296263,
                                       0.08816920644738636,0.07687184132594177,0.06496658324539006,
                                       0.052491914775380344,0.039493461481783246,0.026024198885649517,
                                       0.012144602119448178,-0.002077270001960313,-0.0165657649080691,
                                       -0.0312377086295896,-0.04600256891175273,-0.06076271707085829,
                                       -0.06795927022946352,-0.07503806683258157,-0.0819579039270416,
                                       -0.08867572326732134,-0.09514679596043041,-0.1013249415936529,
                                       -0.10716278331612594,-0.1126120400798519,-0.11762385692798413,
                                       -0.12214917384544385,-0.12613913325700105,-0.12954552576940606,
                                       -0.1323212732062947,-0.13442094737761986,-0.13580132236057105,
                                       -0.13642195734879647,-0.13624580635507777,-0.1352398502347542,
                                       -0.13337574564008783
                                      };

     // test data # 3

     const double arr_values97[100] = {-0.0048844047935863765,-0.006236486880389232,-0.007815274325331554,
                                       -0.009640183890712983,-0.011730050533458039,-0.014102771342375649,
                                       -0.016774922205544347,-0.019761352037216788,-0.023074760743478643,
                                       -0.026725268499719908,-0.03071998531275152,-0.03506259120033407,
                                       -0.03975293858755401,-0.04478668963230909,-0.050155002084304526,
                                       -0.05584427788164485,-0.06183598892150303,-0.06810659423095557,
                                       -0.07462756203769172,-0.08136550893055443,-0.08823038013976417,
                                       -0.09520849071940404,-0.1022476308045662,-0.10929156383600795,
                                       -0.11627847844623534,-0.12314131678294159,-0.1298082479646105,
                                       -0.13620329680129123,-0.14224713548726084,-0.14785804289917964,
                                       -0.15295303238959757,-0.1574491445459654,-0.1612648963069822,
                                       -0.16432187213530286,-0.16654643671328404,-0.16787154196597193,
                                       -0.16823859426980445,-0.16759934066305365,-0.16591772596198778,
                                       -0.1631716661722737,-0.15935634408053131,-0.15448321146294608,
                                       -0.14858218139776455,-0.1416381780493364,-0.1336490652093021,
                                       -0.12462715724623677,-0.11460064260169803,-0.10361487512764765,
                                       -0.09173348472751607,-0.07903925562502323,-0.06563471836746396,
                                       -0.051642400613860905,-0.03720468211196078,-0.022483201281379775,
                                       -0.007657764735299888,0.007075282886553215,0.021505263042677962,
                                       0.03540996481701064,0.04855905391412624,0.06071808324036558,
                                       0.07165316959606502,0.08113608035225883,0.09009849834439126,
                                       0.09838663571146652,0.10584187751947657,0.11230295059241002,
                                       0.11760842867321217,0.12159956720711124,0.12412345180969263,
                                       0.1250364341022301,0.12420781716730643,0.12152374055295379,
                                       0.11689120174753254,0.11024213763900717,0.10153747600764697,
                                       0.09077105399613189,0.07797328823931185,0.06321447046527728,
                                       0.04660755350441126,0.028310286413503014,0.008526547885299433,
                                       0.0025719295116293645,-0.004200598749307477,-0.011762440439929252,
                                       -0.020069965854842452,-0.029063538293552106,-0.038666727752430484,
                                       -0.048785756486008615,-0.05930922217827011,-0.07010814475633873,
                                       -0.08103638195706307,-0.09193145642839495,-0.10261583322770457,
                                       -0.11289868090088843,-0.12257814174844638,-0.1314441273038225,
                                       -0.139281643409791,-0.14587463558538188,-0.15101032970590494,
                                       -0.15448402553323257
                                      };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,99)) - abs(arr_values99[i])) < pow(10.0, -10.0));
     }

     // test # 2

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,98)) - abs(arr_values98[i])) < pow(10.0, -10.0));
     }

     // test # 3

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,97)) - abs(arr_values97[i])) < pow(10.0, -10.0));
     }

     // test # 4

     EXPECT_TRUE(res == 0);
}

//============//
// test # 101 //
//============//

TEST(MatrixTest101, LAPACK_DSYEVD1)
{
     MatrixDense<double> matA;
     MatrixDense<double> eigenvectorsA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 5) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 5) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigensystemRealSymmetric(eigenvectorsA, eigenvaluesA);

     // test

     const double arr_values[100] = {-323.38478082263765, -318.95649570052416, -259.5812044929057,
                                     -255.47902023149175, -210.8574832531544, -207.046095983574,
                                     -172.17668982103106, -169.63822144706373, -167.11362728657673,
                                     -164.65335127248554, -137.93078894722365, -135.63554168942136,
                                     -133.27655590262873, -130.90313825750582, -111.16491214977447,
                                     -108.69201485977163, -104.86333072392988, -102.09654131372476,
                                     -89.52930445237439, -87.11011352113846, -79.24098389717554,
                                     -76.62676551546362, -71.01471245774239, -68.78074078937429,
                                     -57.39730933358366, -55.597048251741185, -53.807888693814036,
                                     -52.082337027364105, -42.001162570408376, -40.10702874995469,
                                     -36.13941170041208, -34.10866137118017, -29.537625276581757,
                                     -27.807512581350473, -20.493485216120316, -19.180726411630474,
                                     -17.86587524596272, -16.594313970872253, -10.527129255160773,
                                     -9.235915164234662, -6.274431175607891, -4.736234460618425,
                                     -3.113902006580256, -2.3717281394985243, -0.07129875210819334,
                                     2.4178759119876623, 3.3310614360036803, 4.154955748257782,
                                     8.633411865685325, 10.366556352869488, 12.095377640289747,
                                     14.018873556316715, 17.48114915683801, 21.917608573061386,
                                     23.316848960689295, 27.074595655585338, 31.322004721507344,
                                     34.9238763246869, 36.70168716147582, 39.12924266179792,
                                     48.291435480841905, 50.073858853444456, 52.12562986320187,
                                     57.27293729356433, 64.70819943681298, 66.93714122637961,
                                     68.82639047739008, 80.9665198923797, 83.5742202822415,
                                     86.58950836310686, 89.44951473838972, 102.06705367044331,
                                     107.13994334023343, 109.44821746324266, 117.55286230732882,
                                     126.41194103599305, 133.5454916018076, 135.893724579225,
                                     151.52771532059967, 157.8972863072518, 165.22044899155756,
                                     167.77180305861472, 190.84742611813815, 205.77684982675112,
                                     208.5174101551291, 234.35823130936174, 279.92197126989515,
                                     330.06056030550724, 384.6942438609073, 444.0868835363212,
                                     508.62422446622827, 578.8097468653175, 655.2951900145777,
                                     738.9379634908137, 830.8998363959786, 932.8239020042909,
                                     1047.1805607335104, 1178.0384559097015, 1333.1755507750008,
                                     1532.5774637949444
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -10.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//============//
// test # 102 //
//============//

TEST(MatrixTest102, LAPACK_DESYVD2)
{
     MatrixDense<double> matA;
     MatrixDense<double> eigenvectorsA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 20) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 20) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigensystemRealSymmetric(eigenvectorsA, eigenvaluesA);

     // test

     const double arr_values[100] = {-890.0679762687605,-833.96613152625,-387.9183497645402,
                                     -374.28858505936836,-325.97417429278767,-290.39317003462327,
                                     -243.09349980476634,-237.9671765884564,-184.65425879239723,
                                     -181.6433889406905,-152.42678092373745,-150.01440312367828,
                                     -140.20043836510496,-134.26239413119825,-130.50508513840435,
                                     -128.1365612192653,-119.6243787096752,-117.25234191992651,
                                     -112.47792700587912,-108.89915545206071,-104.74759545668331,
                                     -100.22968339474058,-94.96995783385485,-89.69665862307328,
                                     -86.4677796222291,-68.14673474241698,-66.07309844841615,
                                     -56.302362373674626,-54.63592515665142,-49.54128744936952,
                                     -47.77783428990662,-45.159157747842364,-43.112766536500985,
                                     -40.904881657341534,-38.624975888999664,-36.21821364136139,
                                     -33.61319247902676,-30.618705849873916,-26.992630355951256,
                                     -19.965626399359778,-9.761220034345175,-0.009675776079236078,
                                     -8.793180471674402e-7,-4.675655622579791e-14,-3.291874702847949e-14,
                                     -3.0408119421278934e-14,-2.2328449483329286e-14,
                                     -1.0693016765542536e-14,-7.240806196368183e-15,
                                     -5.761950647030487e-15,-5.957409950992936e-16,3.2491920424847875e-15,
                                     9.219910025371442e-15,1.2799168101873962e-14,1.7108898977066272e-14,
                                     2.855032832850196e-14,4.1048723593164446e-14,9.99499970701814,
                                     18.470571911228987,30.51830887197781,33.54443547978839,
                                     36.1607309907248,38.57320314001222,40.85924266944485,
                                     43.035391586290125,45.287513370945284,47.151147140980484,
                                     50.73276704593593,52.39952223577173,59.50068014643575,
                                     61.31379159039717,74.51253995296709,77.13729578952395,
                                     94.37203226002364,99.43109411760354,102.6487394625419,
                                     105.06218043709777,107.8179491492106,110.12126119755328,
                                     113.57945167126782,116.37801423876415,122.99832759829658,
                                     125.12209476580418,138.05692785144052,140.21814403606146,
                                     161.31660187081715,164.54676840029904,175.43647569692263,
                                     192.3282320950376,207.53405143733352,212.25537253729775,
                                     284.3628999051823,294.6884623476941,341.98290548531804,
                                     507.44043882416463,534.3382764375148,967.6392677402239,
                                     1875.507871717115,3171.7801027349824,5101.18005605357
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -10.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//============//
// test # 103 //
//============//

TEST(MatrixTest103, LAPACK_DSYEVD3)
{
     MatrixDense<double> matA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 5) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 5) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvaluesRealSymmetric(eigenvaluesA);

     // test

     const double arr_values[100] = {-323.38478082263765, -318.95649570052416, -259.5812044929057,
                                     -255.47902023149175, -210.8574832531544, -207.046095983574,
                                     -172.17668982103106, -169.63822144706373, -167.11362728657673,
                                     -164.65335127248554, -137.93078894722365, -135.63554168942136,
                                     -133.27655590262873, -130.90313825750582, -111.16491214977447,
                                     -108.69201485977163, -104.86333072392988, -102.09654131372476,
                                     -89.52930445237439, -87.11011352113846, -79.24098389717554,
                                     -76.62676551546362, -71.01471245774239, -68.78074078937429,
                                     -57.39730933358366, -55.597048251741185, -53.807888693814036,
                                     -52.082337027364105, -42.001162570408376, -40.10702874995469,
                                     -36.13941170041208, -34.10866137118017, -29.537625276581757,
                                     -27.807512581350473, -20.493485216120316, -19.180726411630474,
                                     -17.86587524596272, -16.594313970872253, -10.527129255160773,
                                     -9.235915164234662, -6.274431175607891, -4.736234460618425,
                                     -3.113902006580256, -2.3717281394985243, -0.07129875210819334,
                                     2.4178759119876623, 3.3310614360036803, 4.154955748257782,
                                     8.633411865685325, 10.366556352869488, 12.095377640289747,
                                     14.018873556316715, 17.48114915683801, 21.917608573061386,
                                     23.316848960689295, 27.074595655585338, 31.322004721507344,
                                     34.9238763246869, 36.70168716147582, 39.12924266179792,
                                     48.291435480841905, 50.073858853444456, 52.12562986320187,
                                     57.27293729356433, 64.70819943681298, 66.93714122637961,
                                     68.82639047739008, 80.9665198923797, 83.5742202822415,
                                     86.58950836310686, 89.44951473838972, 102.06705367044331,
                                     107.13994334023343, 109.44821746324266, 117.55286230732882,
                                     126.41194103599305, 133.5454916018076, 135.893724579225,
                                     151.52771532059967, 157.8972863072518, 165.22044899155756,
                                     167.77180305861472, 190.84742611813815, 205.77684982675112,
                                     208.5174101551291, 234.35823130936174, 279.92197126989515,
                                     330.06056030550724, 384.6942438609073, 444.0868835363212,
                                     508.62422446622827, 578.8097468653175, 655.2951900145777,
                                     738.9379634908137, 830.8998363959786, 932.8239020042909,
                                     1047.1805607335104, 1178.0384559097015, 1333.1755507750008,
                                     1532.5774637949444
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -5.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//============//
// test # 104 //
//============//

TEST(MatrixTest104, LAPACK_DSYEVD5)
{
     MatrixDense<double> matA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 20) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 20) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvaluesRealSymmetric(eigenvaluesA);

     // test

     const double arr_values[100] = {-890.0679762687605,-833.96613152625,-387.9183497645402,
                                     -374.28858505936836,-325.97417429278767,-290.39317003462327,
                                     -243.09349980476634,-237.9671765884564,-184.65425879239723,
                                     -181.6433889406905,-152.42678092373745,-150.01440312367828,
                                     -140.20043836510496,-134.26239413119825,-130.50508513840435,
                                     -128.1365612192653,-119.6243787096752,-117.25234191992651,
                                     -112.47792700587912,-108.89915545206071,-104.74759545668331,
                                     -100.22968339474058,-94.96995783385485,-89.69665862307328,
                                     -86.4677796222291,-68.14673474241698,-66.07309844841615,
                                     -56.302362373674626,-54.63592515665142,-49.54128744936952,
                                     -47.77783428990662,-45.159157747842364,-43.112766536500985,
                                     -40.904881657341534,-38.624975888999664,-36.21821364136139,
                                     -33.61319247902676,-30.618705849873916,-26.992630355951256,
                                     -19.965626399359778,-9.761220034345175,-0.009675776079236078,
                                     -8.793180471674402e-7,-4.675655622579791e-14,-3.291874702847949e-14,
                                     -3.0408119421278934e-14,-2.2328449483329286e-14,
                                     -1.0693016765542536e-14,-7.240806196368183e-15,
                                     -5.761950647030487e-15,-5.957409950992936e-16,3.2491920424847875e-15,
                                     9.219910025371442e-15,1.2799168101873962e-14,1.7108898977066272e-14,
                                     2.855032832850196e-14,4.1048723593164446e-14,9.99499970701814,
                                     18.470571911228987,30.51830887197781,33.54443547978839,
                                     36.1607309907248,38.57320314001222,40.85924266944485,
                                     43.035391586290125,45.287513370945284,47.151147140980484,
                                     50.73276704593593,52.39952223577173,59.50068014643575,
                                     61.31379159039717,74.51253995296709,77.13729578952395,
                                     94.37203226002364,99.43109411760354,102.6487394625419,
                                     105.06218043709777,107.8179491492106,110.12126119755328,
                                     113.57945167126782,116.37801423876415,122.99832759829658,
                                     125.12209476580418,138.05692785144052,140.21814403606146,
                                     161.31660187081715,164.54676840029904,175.43647569692263,
                                     192.3282320950376,207.53405143733352,212.25537253729775,
                                     284.3628999051823,294.6884623476941,341.98290548531804,
                                     507.44043882416463,534.3382764375148,967.6392677402239,
                                     1875.507871717115,3171.7801027349824,5101.18005605357
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -5.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//============//
// test # 105 //
//============//

TEST(MatrixTest105, LAPACK_DSYEVD6)
{
     MatrixDense<double> matA;
     MatrixDense<double> eigenvectorsA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 5) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 5) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvectorsRealSymmetric(eigenvectorsA);

     // test

     const double arr_values99[100] = {3.6942314255214093e-22,
                                       1.9280574390556084e-21,
                                       8.336207484683114e-21,
                                       3.201738809930229e-20,
                                       1.128792365433472e-19,
                                       3.7219958073502635e-19,
                                       1.1616570563876596e-18,
                                       3.4603856776191363e-18,
                                       9.898106164891213e-18,
                                       2.7313034552506115e-17,
                                       7.297235551812733e-17,
                                       1.8931798526470812e-16,
                                       4.781008453916819e-16,
                                       1.1776583982769647e-15,
                                       2.8342405979299284e-15,
                                       6.674396729414062e-15,
                                       1.5399303294661802e-14,
                                       3.4849026635393604e-14,
                                       7.743020050094637e-14,
                                       1.6906046576799098e-13,
                                       3.630170569572799e-13,
                                       7.671355827326599e-13,
                                       1.5964469231753074e-12,
                                       3.2735991022182145e-12,
                                       6.617806646334326e-12,
                                       1.3195608156949319e-11,
                                       2.596350061894761e-11,
                                       5.0430333328453936e-11,
                                       9.67336578559975e-11,
                                       1.8330342627363303e-10,
                                       3.4324925188064113e-10,
                                       6.353651498186134e-10,
                                       1.1628708815038037e-9,
                                       2.104965971919157e-9,
                                       3.7693601315726985e-9,
                                       6.6787563519161844e-9,
                                       1.1711672807554324e-8,
                                       2.03292597117336e-8,
                                       3.4936698501777865e-8,
                                       5.9452893015691615e-8,
                                       1.0019897315247485e-7,
                                       1.672693861786821e-7,
                                       2.7662540139461026e-7,
                                       4.5325802559019753e-7,
                                       7.359163073250683e-7,
                                       1.1841012055800893e-6,
                                       1.8883009979218705e-6,
                                       2.984811021145573e-6,
                                       4.676960526639804e-6,
                                       7.265186654807506e-6,
                                       0.000011189174362740128,
                                       0.00001708624363917233,
                                       0.000025871327250800013,
                                       0.00003884524735147213,
                                       0.000057839551835632114,
                                       0.0000854078666854305,
                                       0.00012507547298611522,
                                       0.0001816604867751283,
                                       0.0002616813987570242,
                                       0.00037386653150919884,
                                       0.0005297808051872514,
                                       0.0007445835839285321,
                                       0.0010379277320564163,
                                       0.0014350037062010874,
                                       0.001967722735545796,
                                       0.0026760192519511626,
                                       0.0036092342363378766,
                                       0.00482751783582054,
                                       0.00640315989292051,
                                       0.008421723355192411,
                                       0.010982819682174948,
                                       0.014200329986124536,
                                       0.018201824732492763,
                                       0.023126912983358,
                                       0.029124247016656085,
                                       0.03634692535629019,
                                       0.04494587964943125,
                                       0.055061139884829774,
                                       0.0668110106541383,
                                       0.08027936243852593,
                                       0.09549945152005936,
                                       0.11243731017783884,
                                       0.13097574227719463,
                                       0.15090013051721174,
                                       0.17186879268938443,
                                       0.19341147439162046,
                                       0.21493488140894385,
                                       0.23573601738701488,
                                       0.2548580930562397,
                                       0.27133687410309687,
                                       0.28424352285526244,
                                       0.29273026481733555,
                                       0.29466646943807095,
                                       0.28996990773813897,
                                       0.27878021480447934,
                                       0.26146670556930435,
                                       0.22712948196564564,
                                       0.19198046679049208,
                                       0.15683996268683814,
                                       0.1225409827758915
                                      };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,99)) - abs(arr_values99[i])) < pow(10.0, -10.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//============//
// test # 106 //
//============//

TEST(MatrixTest106, LAPACK_DSYEVD7)
{
     MatrixDense<double> matA;
     MatrixDense<double> eigenvectorsA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 20) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 20) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvectorsRealSymmetric(eigenvectorsA);

     // test data # 1

     const double arr_values99[100] = {-0.00002379437463800071,-0.0000321126535355192,
                                       -0.00004261209009056319,-0.00005574228105018768,
                                       -0.0000720258070448845,-0.00009206699311457821,
                                       -0.00011656127426176481,-0.00014630515846035676,
                                       -0.0001822067730093074,-0.00022529697290878234,
                                       -0.00027674098203948964,-0.0003378505293342626,
                                       -0.00041009643284298355,-0.0004951215746125915,
                                       -0.0005947541986465926,-0.0007110214528956958,-0.0008461630842956044,
                                       -0.001002645183353377,-0.0011831738617460526,-0.0013907087329034266,
                                       -0.0016283827619966063,-0.0018997449209395513,-0.002208573606467658,
                                       -0.002558944309118129,-0.0029552087127267114,-0.0034019962501085446,
                                       -0.003904213875493434,-0.00446704386484559,-0.00509593944935361,
                                       -0.005796618082542783,-0.006575052137809326,-0.007437456830885415,
                                       -0.00839027516100404,-0.009440159665541611,-0.010593950785877937,
                                       -0.01185865164733851,-0.013241399063586366,-0.014749430585927434,
                                       -0.0163900474309,-0.018170573135449803,-0.0200983089054282,
                                       -0.022180481856340313,-0.024424190854787288,-0.026834080780647522,
                                       -0.02941440937535356,-0.03216899598488147,-0.035101168178730414,
                                       -0.03821370638490854,-0.04150878671030463,-0.04498792214706611,
                                       -0.04865190239866414,-0.05250073259412269,-0.056533571195309565,
                                       -0.06074866744009344,-0.0651432987033876,-0.06971370819842077,
                                       -0.07445504348175375,-0.07936129626731185,-0.08442524409670342,
                                       -0.08963839445497533,-0.09499093194079421,-0.10047166921101545,
                                       -0.10595789789842808,-0.11143628534678302,-0.11689273862688677,
                                       -0.1223124096438104,-0.12767970465965733,-0.13297829857254914,
                                       -0.13819115429332646,-0.1433005475596976,-0.14828809752292466,
                                       -0.15313480343431787,-0.15782108774752324,-0.16232684593753477,
                                       -0.16663150331824728,-0.17071407911689127,-0.17455325803557586,
                                       -0.17812746949713273,-0.18141497473424503,-0.18439396183722762,
                                       -0.18704264882716626,-0.18541168339207653,-0.18356676120058393,
                                       -0.18150272926411598,-0.17921485423076686,-0.17669886945503793,
                                       -0.17395102341224067,-0.17096812928970723,-0.1677476155625255,
                                       -0.1642875773360043,-0.16058682821057293,-0.15664495239745393,
                                       -0.15246235678535205,-0.14804032262974395,-0.14338105650732136,
                                       -0.13848774014895246,-0.1333645787354236,-0.128016847211483,
                                       -0.1224509341456258,-0.11667438263597216
                                      };

     // test data # 2

     const double arr_values98[100] = {0.0005490815006598427,0.0007217004212979288,0.0009319727820738912,
                                       0.0011856764796110383,0.0014891369301256077,0.0018492353084341568,
                                       0.002273410759398609,0.002769655716932461,0.003346503443557062,
                                       0.00401300689217423,0.0047787079930774546,0.00565359648519078,
                                       0.006648057443072511,0.007772806702325777,0.009038813457661714,
                                       0.010457209401826704,0.012039183891666605,0.013795864771304657,
                                       0.01573818465305239,0.01787673265523282,0.020218129530799774,
                                       0.022773522732384158,0.02555088599547039,0.02855699048006569,
                                       0.03179740706616532,0.03527631554658473,0.038996305859039175,
                                       0.04295817276333242,0.047160705650423344,0.05160047546600458,
                                       0.056271621039461366,0.061165637426456754,0.06627116919601259,
                                       0.07157381191620707,0.0770559254111312,0.08269646266941802,
                                       0.08847081857459133,0.09435070289203276,0.10030404217816305,
                                       0.10629491546542182,0.11228359420772163,0.11822646848984367,
                                       0.12407615562938958,0.12979275139874066,0.13533385216700752,
                                       0.14065467360130016,0.14570820649639804,0.1504454123825699,
                                       0.15481546139101132,0.15876601462676446,0.16224355300490953,
                                       0.1651937541424425,0.16756191846119972,0.16929344514268863,
                                       0.17033435798074814,0.17063188050057534,0.17013505895196374,
                                       0.16879543094112387,0.16656773654130375,0.16341066772151153,
                                       0.1592876487959431,0.15416764632985688,0.14836208495033895,
                                       0.14186022973422263,0.1346552528267162,0.12674464039743252,
                                       0.118130599994188,0.10882046380600134,0.09882708283296263,
                                       0.08816920644738636,0.07687184132594177,0.06496658324539006,
                                       0.052491914775380344,0.039493461481783246,0.026024198885649517,
                                       0.012144602119448178,-0.002077270001960313,-0.0165657649080691,
                                       -0.0312377086295896,-0.04600256891175273,-0.06076271707085829,
                                       -0.06795927022946352,-0.07503806683258157,-0.0819579039270416,
                                       -0.08867572326732134,-0.09514679596043041,-0.1013249415936529,
                                       -0.10716278331612594,-0.1126120400798519,-0.11762385692798413,
                                       -0.12214917384544385,-0.12613913325700105,-0.12954552576940606,
                                       -0.1323212732062947,-0.13442094737761986,-0.13580132236057105,
                                       -0.13642195734879647,-0.13624580635507777,-0.1352398502347542,
                                       -0.13337574564008783
                                      };

     // test data # 3

     const double arr_values97[100] = {-0.0048844047935863765,-0.006236486880389232,-0.007815274325331554,
                                       -0.009640183890712983,-0.011730050533458039,-0.014102771342375649,
                                       -0.016774922205544347,-0.019761352037216788,-0.023074760743478643,
                                       -0.026725268499719908,-0.03071998531275152,-0.03506259120033407,
                                       -0.03975293858755401,-0.04478668963230909,-0.050155002084304526,
                                       -0.05584427788164485,-0.06183598892150303,-0.06810659423095557,
                                       -0.07462756203769172,-0.08136550893055443,-0.08823038013976417,
                                       -0.09520849071940404,-0.1022476308045662,-0.10929156383600795,
                                       -0.11627847844623534,-0.12314131678294159,-0.1298082479646105,
                                       -0.13620329680129123,-0.14224713548726084,-0.14785804289917964,
                                       -0.15295303238959757,-0.1574491445459654,-0.1612648963069822,
                                       -0.16432187213530286,-0.16654643671328404,-0.16787154196597193,
                                       -0.16823859426980445,-0.16759934066305365,-0.16591772596198778,
                                       -0.1631716661722737,-0.15935634408053131,-0.15448321146294608,
                                       -0.14858218139776455,-0.1416381780493364,-0.1336490652093021,
                                       -0.12462715724623677,-0.11460064260169803,-0.10361487512764765,
                                       -0.09173348472751607,-0.07903925562502323,-0.06563471836746396,
                                       -0.051642400613860905,-0.03720468211196078,-0.022483201281379775,
                                       -0.007657764735299888,0.007075282886553215,0.021505263042677962,
                                       0.03540996481701064,0.04855905391412624,0.06071808324036558,
                                       0.07165316959606502,0.08113608035225883,0.09009849834439126,
                                       0.09838663571146652,0.10584187751947657,0.11230295059241002,
                                       0.11760842867321217,0.12159956720711124,0.12412345180969263,
                                       0.1250364341022301,0.12420781716730643,0.12152374055295379,
                                       0.11689120174753254,0.11024213763900717,0.10153747600764697,
                                       0.09077105399613189,0.07797328823931185,0.06321447046527728,
                                       0.04660755350441126,0.028310286413503014,0.008526547885299433,
                                       0.0025719295116293645,-0.004200598749307477,-0.011762440439929252,
                                       -0.020069965854842452,-0.029063538293552106,-0.038666727752430484,
                                       -0.048785756486008615,-0.05930922217827011,-0.07010814475633873,
                                       -0.08103638195706307,-0.09193145642839495,-0.10261583322770457,
                                       -0.11289868090088843,-0.12257814174844638,-0.1314441273038225,
                                       -0.139281643409791,-0.14587463558538188,-0.15101032970590494,
                                       -0.15448402553323257
                                      };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,99)) - abs(arr_values99[i])) < pow(10.0, -10.0));
     }

     // test # 2

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,98)) - abs(arr_values98[i])) < pow(10.0, -10.0));
     }

     // test # 3

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,97)) - abs(arr_values97[i])) < pow(10.0, -10.0));
     }

     // test # 4

     EXPECT_TRUE(res == 0);
}

//============//
// test # 107 //
//============//

TEST(MatrixTest107, LAPACK_DSYEVR1)
{
     MatrixDense<double> matA;
     MatrixDense<double> eigenvectorsA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 5) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 5) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigensystemRealSymmetric(eigenvectorsA, eigenvaluesA, "DSYEVR");

     // test

     const double arr_values[100] = {-323.38478082263765, -318.95649570052416, -259.5812044929057,
                                     -255.47902023149175, -210.8574832531544, -207.046095983574,
                                     -172.17668982103106, -169.63822144706373, -167.11362728657673,
                                     -164.65335127248554, -137.93078894722365, -135.63554168942136,
                                     -133.27655590262873, -130.90313825750582, -111.16491214977447,
                                     -108.69201485977163, -104.86333072392988, -102.09654131372476,
                                     -89.52930445237439, -87.11011352113846, -79.24098389717554,
                                     -76.62676551546362, -71.01471245774239, -68.78074078937429,
                                     -57.39730933358366, -55.597048251741185, -53.807888693814036,
                                     -52.082337027364105, -42.001162570408376, -40.10702874995469,
                                     -36.13941170041208, -34.10866137118017, -29.537625276581757,
                                     -27.807512581350473, -20.493485216120316, -19.180726411630474,
                                     -17.86587524596272, -16.594313970872253, -10.527129255160773,
                                     -9.235915164234662, -6.274431175607891, -4.736234460618425,
                                     -3.113902006580256, -2.3717281394985243, -0.07129875210819334,
                                     2.4178759119876623, 3.3310614360036803, 4.154955748257782,
                                     8.633411865685325, 10.366556352869488, 12.095377640289747,
                                     14.018873556316715, 17.48114915683801, 21.917608573061386,
                                     23.316848960689295, 27.074595655585338, 31.322004721507344,
                                     34.9238763246869, 36.70168716147582, 39.12924266179792,
                                     48.291435480841905, 50.073858853444456, 52.12562986320187,
                                     57.27293729356433, 64.70819943681298, 66.93714122637961,
                                     68.82639047739008, 80.9665198923797, 83.5742202822415,
                                     86.58950836310686, 89.44951473838972, 102.06705367044331,
                                     107.13994334023343, 109.44821746324266, 117.55286230732882,
                                     126.41194103599305, 133.5454916018076, 135.893724579225,
                                     151.52771532059967, 157.8972863072518, 165.22044899155756,
                                     167.77180305861472, 190.84742611813815, 205.77684982675112,
                                     208.5174101551291, 234.35823130936174, 279.92197126989515,
                                     330.06056030550724, 384.6942438609073, 444.0868835363212,
                                     508.62422446622827, 578.8097468653175, 655.2951900145777,
                                     738.9379634908137, 830.8998363959786, 932.8239020042909,
                                     1047.1805607335104, 1178.0384559097015, 1333.1755507750008,
                                     1532.5774637949444
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -10.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//============//
// test # 108 //
//============//

TEST(MatrixTest108, LAPACK_DSYEVR2)
{
     MatrixDense<double> matA;
     MatrixDense<double> eigenvectorsA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 20) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 20) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigensystemRealSymmetric(eigenvectorsA, eigenvaluesA, "DSYEVR");

     // test

     const double arr_values[100] = {-890.0679762687605,-833.96613152625,-387.9183497645402,
                                     -374.28858505936836,-325.97417429278767,-290.39317003462327,
                                     -243.09349980476634,-237.9671765884564,-184.65425879239723,
                                     -181.6433889406905,-152.42678092373745,-150.01440312367828,
                                     -140.20043836510496,-134.26239413119825,-130.50508513840435,
                                     -128.1365612192653,-119.6243787096752,-117.25234191992651,
                                     -112.47792700587912,-108.89915545206071,-104.74759545668331,
                                     -100.22968339474058,-94.96995783385485,-89.69665862307328,
                                     -86.4677796222291,-68.14673474241698,-66.07309844841615,
                                     -56.302362373674626,-54.63592515665142,-49.54128744936952,
                                     -47.77783428990662,-45.159157747842364,-43.112766536500985,
                                     -40.904881657341534,-38.624975888999664,-36.21821364136139,
                                     -33.61319247902676,-30.618705849873916,-26.992630355951256,
                                     -19.965626399359778,-9.761220034345175,-0.009675776079236078,
                                     -8.793180471674402e-7,-4.675655622579791e-14,-3.291874702847949e-14,
                                     -3.0408119421278934e-14,-2.2328449483329286e-14,
                                     -1.0693016765542536e-14,-7.240806196368183e-15,
                                     -5.761950647030487e-15,-5.957409950992936e-16,3.2491920424847875e-15,
                                     9.219910025371442e-15,1.2799168101873962e-14,1.7108898977066272e-14,
                                     2.855032832850196e-14,4.1048723593164446e-14,9.99499970701814,
                                     18.470571911228987,30.51830887197781,33.54443547978839,
                                     36.1607309907248,38.57320314001222,40.85924266944485,
                                     43.035391586290125,45.287513370945284,47.151147140980484,
                                     50.73276704593593,52.39952223577173,59.50068014643575,
                                     61.31379159039717,74.51253995296709,77.13729578952395,
                                     94.37203226002364,99.43109411760354,102.6487394625419,
                                     105.06218043709777,107.8179491492106,110.12126119755328,
                                     113.57945167126782,116.37801423876415,122.99832759829658,
                                     125.12209476580418,138.05692785144052,140.21814403606146,
                                     161.31660187081715,164.54676840029904,175.43647569692263,
                                     192.3282320950376,207.53405143733352,212.25537253729775,
                                     284.3628999051823,294.6884623476941,341.98290548531804,
                                     507.44043882416463,534.3382764375148,967.6392677402239,
                                     1875.507871717115,3171.7801027349824,5101.18005605357
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -10.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//============//
// test # 109 //
//============//

TEST(MatrixTest109, LAPACK_DSYEVR3)
{
     MatrixDense<double> matA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 5) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 5) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvaluesRealSymmetric(eigenvaluesA, "DSYEVR");

     // test

     const double arr_values[100] = {-323.38478082263765, -318.95649570052416, -259.5812044929057,
                                     -255.47902023149175, -210.8574832531544, -207.046095983574,
                                     -172.17668982103106, -169.63822144706373, -167.11362728657673,
                                     -164.65335127248554, -137.93078894722365, -135.63554168942136,
                                     -133.27655590262873, -130.90313825750582, -111.16491214977447,
                                     -108.69201485977163, -104.86333072392988, -102.09654131372476,
                                     -89.52930445237439, -87.11011352113846, -79.24098389717554,
                                     -76.62676551546362, -71.01471245774239, -68.78074078937429,
                                     -57.39730933358366, -55.597048251741185, -53.807888693814036,
                                     -52.082337027364105, -42.001162570408376, -40.10702874995469,
                                     -36.13941170041208, -34.10866137118017, -29.537625276581757,
                                     -27.807512581350473, -20.493485216120316, -19.180726411630474,
                                     -17.86587524596272, -16.594313970872253, -10.527129255160773,
                                     -9.235915164234662, -6.274431175607891, -4.736234460618425,
                                     -3.113902006580256, -2.3717281394985243, -0.07129875210819334,
                                     2.4178759119876623, 3.3310614360036803, 4.154955748257782,
                                     8.633411865685325, 10.366556352869488, 12.095377640289747,
                                     14.018873556316715, 17.48114915683801, 21.917608573061386,
                                     23.316848960689295, 27.074595655585338, 31.322004721507344,
                                     34.9238763246869, 36.70168716147582, 39.12924266179792,
                                     48.291435480841905, 50.073858853444456, 52.12562986320187,
                                     57.27293729356433, 64.70819943681298, 66.93714122637961,
                                     68.82639047739008, 80.9665198923797, 83.5742202822415,
                                     86.58950836310686, 89.44951473838972, 102.06705367044331,
                                     107.13994334023343, 109.44821746324266, 117.55286230732882,
                                     126.41194103599305, 133.5454916018076, 135.893724579225,
                                     151.52771532059967, 157.8972863072518, 165.22044899155756,
                                     167.77180305861472, 190.84742611813815, 205.77684982675112,
                                     208.5174101551291, 234.35823130936174, 279.92197126989515,
                                     330.06056030550724, 384.6942438609073, 444.0868835363212,
                                     508.62422446622827, 578.8097468653175, 655.2951900145777,
                                     738.9379634908137, 830.8998363959786, 932.8239020042909,
                                     1047.1805607335104, 1178.0384559097015, 1333.1755507750008,
                                     1532.5774637949444
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -5.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//============//
// test # 110 //
//============//

TEST(MatrixTest110, LAPACK_DSYEVR4)
{
     MatrixDense<double> matA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 20) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 20) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvaluesRealSymmetric(eigenvaluesA, "DSYEVR");

     // test

     const double arr_values[100] = {-890.0679762687605,-833.96613152625,-387.9183497645402,
                                     -374.28858505936836,-325.97417429278767,-290.39317003462327,
                                     -243.09349980476634,-237.9671765884564,-184.65425879239723,
                                     -181.6433889406905,-152.42678092373745,-150.01440312367828,
                                     -140.20043836510496,-134.26239413119825,-130.50508513840435,
                                     -128.1365612192653,-119.6243787096752,-117.25234191992651,
                                     -112.47792700587912,-108.89915545206071,-104.74759545668331,
                                     -100.22968339474058,-94.96995783385485,-89.69665862307328,
                                     -86.4677796222291,-68.14673474241698,-66.07309844841615,
                                     -56.302362373674626,-54.63592515665142,-49.54128744936952,
                                     -47.77783428990662,-45.159157747842364,-43.112766536500985,
                                     -40.904881657341534,-38.624975888999664,-36.21821364136139,
                                     -33.61319247902676,-30.618705849873916,-26.992630355951256,
                                     -19.965626399359778,-9.761220034345175,-0.009675776079236078,
                                     -8.793180471674402e-7,-4.675655622579791e-14,-3.291874702847949e-14,
                                     -3.0408119421278934e-14,-2.2328449483329286e-14,
                                     -1.0693016765542536e-14,-7.240806196368183e-15,
                                     -5.761950647030487e-15,-5.957409950992936e-16,3.2491920424847875e-15,
                                     9.219910025371442e-15,1.2799168101873962e-14,1.7108898977066272e-14,
                                     2.855032832850196e-14,4.1048723593164446e-14,9.99499970701814,
                                     18.470571911228987,30.51830887197781,33.54443547978839,
                                     36.1607309907248,38.57320314001222,40.85924266944485,
                                     43.035391586290125,45.287513370945284,47.151147140980484,
                                     50.73276704593593,52.39952223577173,59.50068014643575,
                                     61.31379159039717,74.51253995296709,77.13729578952395,
                                     94.37203226002364,99.43109411760354,102.6487394625419,
                                     105.06218043709777,107.8179491492106,110.12126119755328,
                                     113.57945167126782,116.37801423876415,122.99832759829658,
                                     125.12209476580418,138.05692785144052,140.21814403606146,
                                     161.31660187081715,164.54676840029904,175.43647569692263,
                                     192.3282320950376,207.53405143733352,212.25537253729775,
                                     284.3628999051823,294.6884623476941,341.98290548531804,
                                     507.44043882416463,534.3382764375148,967.6392677402239,
                                     1875.507871717115,3171.7801027349824,5101.18005605357
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -5.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//============//
// test # 111 //
//============//

TEST(MatrixTest111, LAPACK_DSYEVR5)
{
     MatrixDense<double> matA;
     MatrixDense<double> eigenvectorsA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 5) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 5) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvectorsRealSymmetric(eigenvectorsA, "DSYEVR");

     // test

     const double arr_values99[100] = {3.6942314255214093e-22,
                                       1.9280574390556084e-21,
                                       8.336207484683114e-21,
                                       3.201738809930229e-20,
                                       1.128792365433472e-19,
                                       3.7219958073502635e-19,
                                       1.1616570563876596e-18,
                                       3.4603856776191363e-18,
                                       9.898106164891213e-18,
                                       2.7313034552506115e-17,
                                       7.297235551812733e-17,
                                       1.8931798526470812e-16,
                                       4.781008453916819e-16,
                                       1.1776583982769647e-15,
                                       2.8342405979299284e-15,
                                       6.674396729414062e-15,
                                       1.5399303294661802e-14,
                                       3.4849026635393604e-14,
                                       7.743020050094637e-14,
                                       1.6906046576799098e-13,
                                       3.630170569572799e-13,
                                       7.671355827326599e-13,
                                       1.5964469231753074e-12,
                                       3.2735991022182145e-12,
                                       6.617806646334326e-12,
                                       1.3195608156949319e-11,
                                       2.596350061894761e-11,
                                       5.0430333328453936e-11,
                                       9.67336578559975e-11,
                                       1.8330342627363303e-10,
                                       3.4324925188064113e-10,
                                       6.353651498186134e-10,
                                       1.1628708815038037e-9,
                                       2.104965971919157e-9,
                                       3.7693601315726985e-9,
                                       6.6787563519161844e-9,
                                       1.1711672807554324e-8,
                                       2.03292597117336e-8,
                                       3.4936698501777865e-8,
                                       5.9452893015691615e-8,
                                       1.0019897315247485e-7,
                                       1.672693861786821e-7,
                                       2.7662540139461026e-7,
                                       4.5325802559019753e-7,
                                       7.359163073250683e-7,
                                       1.1841012055800893e-6,
                                       1.8883009979218705e-6,
                                       2.984811021145573e-6,
                                       4.676960526639804e-6,
                                       7.265186654807506e-6,
                                       0.000011189174362740128,
                                       0.00001708624363917233,
                                       0.000025871327250800013,
                                       0.00003884524735147213,
                                       0.000057839551835632114,
                                       0.0000854078666854305,
                                       0.00012507547298611522,
                                       0.0001816604867751283,
                                       0.0002616813987570242,
                                       0.00037386653150919884,
                                       0.0005297808051872514,
                                       0.0007445835839285321,
                                       0.0010379277320564163,
                                       0.0014350037062010874,
                                       0.001967722735545796,
                                       0.0026760192519511626,
                                       0.0036092342363378766,
                                       0.00482751783582054,
                                       0.00640315989292051,
                                       0.008421723355192411,
                                       0.010982819682174948,
                                       0.014200329986124536,
                                       0.018201824732492763,
                                       0.023126912983358,
                                       0.029124247016656085,
                                       0.03634692535629019,
                                       0.04494587964943125,
                                       0.055061139884829774,
                                       0.0668110106541383,
                                       0.08027936243852593,
                                       0.09549945152005936,
                                       0.11243731017783884,
                                       0.13097574227719463,
                                       0.15090013051721174,
                                       0.17186879268938443,
                                       0.19341147439162046,
                                       0.21493488140894385,
                                       0.23573601738701488,
                                       0.2548580930562397,
                                       0.27133687410309687,
                                       0.28424352285526244,
                                       0.29273026481733555,
                                       0.29466646943807095,
                                       0.28996990773813897,
                                       0.27878021480447934,
                                       0.26146670556930435,
                                       0.22712948196564564,
                                       0.19198046679049208,
                                       0.15683996268683814,
                                       0.1225409827758915
                                      };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,99)) - abs(arr_values99[i])) < pow(10.0, -10.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//============//
// test # 112 //
//============//

TEST(MatrixTest112, LAPACK_DSYEVR6)
{
     MatrixDense<double> matA;
     MatrixDense<double> eigenvectorsA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 20) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 20) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvectorsRealSymmetric(eigenvectorsA, "DSYEVR");

     // test data # 1

     const double arr_values99[100] = {-0.00002379437463800071,-0.0000321126535355192,
                                       -0.00004261209009056319,-0.00005574228105018768,
                                       -0.0000720258070448845,-0.00009206699311457821,
                                       -0.00011656127426176481,-0.00014630515846035676,
                                       -0.0001822067730093074,-0.00022529697290878234,
                                       -0.00027674098203948964,-0.0003378505293342626,
                                       -0.00041009643284298355,-0.0004951215746125915,
                                       -0.0005947541986465926,-0.0007110214528956958,-0.0008461630842956044,
                                       -0.001002645183353377,-0.0011831738617460526,-0.0013907087329034266,
                                       -0.0016283827619966063,-0.0018997449209395513,-0.002208573606467658,
                                       -0.002558944309118129,-0.0029552087127267114,-0.0034019962501085446,
                                       -0.003904213875493434,-0.00446704386484559,-0.00509593944935361,
                                       -0.005796618082542783,-0.006575052137809326,-0.007437456830885415,
                                       -0.00839027516100404,-0.009440159665541611,-0.010593950785877937,
                                       -0.01185865164733851,-0.013241399063586366,-0.014749430585927434,
                                       -0.0163900474309,-0.018170573135449803,-0.0200983089054282,
                                       -0.022180481856340313,-0.024424190854787288,-0.026834080780647522,
                                       -0.02941440937535356,-0.03216899598488147,-0.035101168178730414,
                                       -0.03821370638490854,-0.04150878671030463,-0.04498792214706611,
                                       -0.04865190239866414,-0.05250073259412269,-0.056533571195309565,
                                       -0.06074866744009344,-0.0651432987033876,-0.06971370819842077,
                                       -0.07445504348175375,-0.07936129626731185,-0.08442524409670342,
                                       -0.08963839445497533,-0.09499093194079421,-0.10047166921101545,
                                       -0.10595789789842808,-0.11143628534678302,-0.11689273862688677,
                                       -0.1223124096438104,-0.12767970465965733,-0.13297829857254914,
                                       -0.13819115429332646,-0.1433005475596976,-0.14828809752292466,
                                       -0.15313480343431787,-0.15782108774752324,-0.16232684593753477,
                                       -0.16663150331824728,-0.17071407911689127,-0.17455325803557586,
                                       -0.17812746949713273,-0.18141497473424503,-0.18439396183722762,
                                       -0.18704264882716626,-0.18541168339207653,-0.18356676120058393,
                                       -0.18150272926411598,-0.17921485423076686,-0.17669886945503793,
                                       -0.17395102341224067,-0.17096812928970723,-0.1677476155625255,
                                       -0.1642875773360043,-0.16058682821057293,-0.15664495239745393,
                                       -0.15246235678535205,-0.14804032262974395,-0.14338105650732136,
                                       -0.13848774014895246,-0.1333645787354236,-0.128016847211483,
                                       -0.1224509341456258,-0.11667438263597216
                                      };

     // test data # 2

     const double arr_values98[100] = {0.0005490815006598427,0.0007217004212979288,0.0009319727820738912,
                                       0.0011856764796110383,0.0014891369301256077,0.0018492353084341568,
                                       0.002273410759398609,0.002769655716932461,0.003346503443557062,
                                       0.00401300689217423,0.0047787079930774546,0.00565359648519078,
                                       0.006648057443072511,0.007772806702325777,0.009038813457661714,
                                       0.010457209401826704,0.012039183891666605,0.013795864771304657,
                                       0.01573818465305239,0.01787673265523282,0.020218129530799774,
                                       0.022773522732384158,0.02555088599547039,0.02855699048006569,
                                       0.03179740706616532,0.03527631554658473,0.038996305859039175,
                                       0.04295817276333242,0.047160705650423344,0.05160047546600458,
                                       0.056271621039461366,0.061165637426456754,0.06627116919601259,
                                       0.07157381191620707,0.0770559254111312,0.08269646266941802,
                                       0.08847081857459133,0.09435070289203276,0.10030404217816305,
                                       0.10629491546542182,0.11228359420772163,0.11822646848984367,
                                       0.12407615562938958,0.12979275139874066,0.13533385216700752,
                                       0.14065467360130016,0.14570820649639804,0.1504454123825699,
                                       0.15481546139101132,0.15876601462676446,0.16224355300490953,
                                       0.1651937541424425,0.16756191846119972,0.16929344514268863,
                                       0.17033435798074814,0.17063188050057534,0.17013505895196374,
                                       0.16879543094112387,0.16656773654130375,0.16341066772151153,
                                       0.1592876487959431,0.15416764632985688,0.14836208495033895,
                                       0.14186022973422263,0.1346552528267162,0.12674464039743252,
                                       0.118130599994188,0.10882046380600134,0.09882708283296263,
                                       0.08816920644738636,0.07687184132594177,0.06496658324539006,
                                       0.052491914775380344,0.039493461481783246,0.026024198885649517,
                                       0.012144602119448178,-0.002077270001960313,-0.0165657649080691,
                                       -0.0312377086295896,-0.04600256891175273,-0.06076271707085829,
                                       -0.06795927022946352,-0.07503806683258157,-0.0819579039270416,
                                       -0.08867572326732134,-0.09514679596043041,-0.1013249415936529,
                                       -0.10716278331612594,-0.1126120400798519,-0.11762385692798413,
                                       -0.12214917384544385,-0.12613913325700105,-0.12954552576940606,
                                       -0.1323212732062947,-0.13442094737761986,-0.13580132236057105,
                                       -0.13642195734879647,-0.13624580635507777,-0.1352398502347542,
                                       -0.13337574564008783
                                      };

     // test data # 3

     const double arr_values97[100] = {-0.0048844047935863765,-0.006236486880389232,-0.007815274325331554,
                                       -0.009640183890712983,-0.011730050533458039,-0.014102771342375649,
                                       -0.016774922205544347,-0.019761352037216788,-0.023074760743478643,
                                       -0.026725268499719908,-0.03071998531275152,-0.03506259120033407,
                                       -0.03975293858755401,-0.04478668963230909,-0.050155002084304526,
                                       -0.05584427788164485,-0.06183598892150303,-0.06810659423095557,
                                       -0.07462756203769172,-0.08136550893055443,-0.08823038013976417,
                                       -0.09520849071940404,-0.1022476308045662,-0.10929156383600795,
                                       -0.11627847844623534,-0.12314131678294159,-0.1298082479646105,
                                       -0.13620329680129123,-0.14224713548726084,-0.14785804289917964,
                                       -0.15295303238959757,-0.1574491445459654,-0.1612648963069822,
                                       -0.16432187213530286,-0.16654643671328404,-0.16787154196597193,
                                       -0.16823859426980445,-0.16759934066305365,-0.16591772596198778,
                                       -0.1631716661722737,-0.15935634408053131,-0.15448321146294608,
                                       -0.14858218139776455,-0.1416381780493364,-0.1336490652093021,
                                       -0.12462715724623677,-0.11460064260169803,-0.10361487512764765,
                                       -0.09173348472751607,-0.07903925562502323,-0.06563471836746396,
                                       -0.051642400613860905,-0.03720468211196078,-0.022483201281379775,
                                       -0.007657764735299888,0.007075282886553215,0.021505263042677962,
                                       0.03540996481701064,0.04855905391412624,0.06071808324036558,
                                       0.07165316959606502,0.08113608035225883,0.09009849834439126,
                                       0.09838663571146652,0.10584187751947657,0.11230295059241002,
                                       0.11760842867321217,0.12159956720711124,0.12412345180969263,
                                       0.1250364341022301,0.12420781716730643,0.12152374055295379,
                                       0.11689120174753254,0.11024213763900717,0.10153747600764697,
                                       0.09077105399613189,0.07797328823931185,0.06321447046527728,
                                       0.04660755350441126,0.028310286413503014,0.008526547885299433,
                                       0.0025719295116293645,-0.004200598749307477,-0.011762440439929252,
                                       -0.020069965854842452,-0.029063538293552106,-0.038666727752430484,
                                       -0.048785756486008615,-0.05930922217827011,-0.07010814475633873,
                                       -0.08103638195706307,-0.09193145642839495,-0.10261583322770457,
                                       -0.11289868090088843,-0.12257814174844638,-0.1314441273038225,
                                       -0.139281643409791,-0.14587463558538188,-0.15101032970590494,
                                       -0.15448402553323257
                                      };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,99)) - abs(arr_values99[i])) < pow(10.0, -10.0));
     }

     // test # 2

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,98)) - abs(arr_values98[i])) < pow(10.0, -10.0));
     }

     // test # 3

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,97)) - abs(arr_values97[i])) < pow(10.0, -10.0));
     }

     // test # 4

     EXPECT_TRUE(res == 0);
}

//============//
// test # 113 //
//============//

TEST(MatrixTest113, LAPACK_DSYEVX1)
{
     MatrixDense<double> matA;
     MatrixDense<double> eigenvectorsA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 5) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 5) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigensystemRealSymmetric(eigenvectorsA, eigenvaluesA, "DSYEVX");

     // test

     const double arr_values[100] = {-323.38478082263765, -318.95649570052416, -259.5812044929057,
                                     -255.47902023149175, -210.8574832531544, -207.046095983574,
                                     -172.17668982103106, -169.63822144706373, -167.11362728657673,
                                     -164.65335127248554, -137.93078894722365, -135.63554168942136,
                                     -133.27655590262873, -130.90313825750582, -111.16491214977447,
                                     -108.69201485977163, -104.86333072392988, -102.09654131372476,
                                     -89.52930445237439, -87.11011352113846, -79.24098389717554,
                                     -76.62676551546362, -71.01471245774239, -68.78074078937429,
                                     -57.39730933358366, -55.597048251741185, -53.807888693814036,
                                     -52.082337027364105, -42.001162570408376, -40.10702874995469,
                                     -36.13941170041208, -34.10866137118017, -29.537625276581757,
                                     -27.807512581350473, -20.493485216120316, -19.180726411630474,
                                     -17.86587524596272, -16.594313970872253, -10.527129255160773,
                                     -9.235915164234662, -6.274431175607891, -4.736234460618425,
                                     -3.113902006580256, -2.3717281394985243, -0.07129875210819334,
                                     2.4178759119876623, 3.3310614360036803, 4.154955748257782,
                                     8.633411865685325, 10.366556352869488, 12.095377640289747,
                                     14.018873556316715, 17.48114915683801, 21.917608573061386,
                                     23.316848960689295, 27.074595655585338, 31.322004721507344,
                                     34.9238763246869, 36.70168716147582, 39.12924266179792,
                                     48.291435480841905, 50.073858853444456, 52.12562986320187,
                                     57.27293729356433, 64.70819943681298, 66.93714122637961,
                                     68.82639047739008, 80.9665198923797, 83.5742202822415,
                                     86.58950836310686, 89.44951473838972, 102.06705367044331,
                                     107.13994334023343, 109.44821746324266, 117.55286230732882,
                                     126.41194103599305, 133.5454916018076, 135.893724579225,
                                     151.52771532059967, 157.8972863072518, 165.22044899155756,
                                     167.77180305861472, 190.84742611813815, 205.77684982675112,
                                     208.5174101551291, 234.35823130936174, 279.92197126989515,
                                     330.06056030550724, 384.6942438609073, 444.0868835363212,
                                     508.62422446622827, 578.8097468653175, 655.2951900145777,
                                     738.9379634908137, 830.8998363959786, 932.8239020042909,
                                     1047.1805607335104, 1178.0384559097015, 1333.1755507750008,
                                     1532.5774637949444
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -10.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//============//
// test # 114 //
//============//

TEST(MatrixTest114, LAPACK_DSYEVX2)
{
     MatrixDense<double> matA;
     MatrixDense<double> eigenvectorsA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 20) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 20) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigensystemRealSymmetric(eigenvectorsA, eigenvaluesA, "DSYEVX");

     // test

     const double arr_values[100] = {-890.0679762687605,-833.96613152625,-387.9183497645402,
                                     -374.28858505936836,-325.97417429278767,-290.39317003462327,
                                     -243.09349980476634,-237.9671765884564,-184.65425879239723,
                                     -181.6433889406905,-152.42678092373745,-150.01440312367828,
                                     -140.20043836510496,-134.26239413119825,-130.50508513840435,
                                     -128.1365612192653,-119.6243787096752,-117.25234191992651,
                                     -112.47792700587912,-108.89915545206071,-104.74759545668331,
                                     -100.22968339474058,-94.96995783385485,-89.69665862307328,
                                     -86.4677796222291,-68.14673474241698,-66.07309844841615,
                                     -56.302362373674626,-54.63592515665142,-49.54128744936952,
                                     -47.77783428990662,-45.159157747842364,-43.112766536500985,
                                     -40.904881657341534,-38.624975888999664,-36.21821364136139,
                                     -33.61319247902676,-30.618705849873916,-26.992630355951256,
                                     -19.965626399359778,-9.761220034345175,-0.009675776079236078,
                                     -8.793180471674402e-7,-4.675655622579791e-14,-3.291874702847949e-14,
                                     -3.0408119421278934e-14,-2.2328449483329286e-14,
                                     -1.0693016765542536e-14,-7.240806196368183e-15,
                                     -5.761950647030487e-15,-5.957409950992936e-16,3.2491920424847875e-15,
                                     9.219910025371442e-15,1.2799168101873962e-14,1.7108898977066272e-14,
                                     2.855032832850196e-14,4.1048723593164446e-14,9.99499970701814,
                                     18.470571911228987,30.51830887197781,33.54443547978839,
                                     36.1607309907248,38.57320314001222,40.85924266944485,
                                     43.035391586290125,45.287513370945284,47.151147140980484,
                                     50.73276704593593,52.39952223577173,59.50068014643575,
                                     61.31379159039717,74.51253995296709,77.13729578952395,
                                     94.37203226002364,99.43109411760354,102.6487394625419,
                                     105.06218043709777,107.8179491492106,110.12126119755328,
                                     113.57945167126782,116.37801423876415,122.99832759829658,
                                     125.12209476580418,138.05692785144052,140.21814403606146,
                                     161.31660187081715,164.54676840029904,175.43647569692263,
                                     192.3282320950376,207.53405143733352,212.25537253729775,
                                     284.3628999051823,294.6884623476941,341.98290548531804,
                                     507.44043882416463,534.3382764375148,967.6392677402239,
                                     1875.507871717115,3171.7801027349824,5101.18005605357
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -10.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//============//
// test # 115 //
//============//

TEST(MatrixTest115, LAPACK_DSYEVX3)
{
     MatrixDense<double> matA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 5) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 5) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvaluesRealSymmetric(eigenvaluesA, "DSYEVX");

     // test

     const double arr_values[100] = {-323.38478082263765, -318.95649570052416, -259.5812044929057,
                                     -255.47902023149175, -210.8574832531544, -207.046095983574,
                                     -172.17668982103106, -169.63822144706373, -167.11362728657673,
                                     -164.65335127248554, -137.93078894722365, -135.63554168942136,
                                     -133.27655590262873, -130.90313825750582, -111.16491214977447,
                                     -108.69201485977163, -104.86333072392988, -102.09654131372476,
                                     -89.52930445237439, -87.11011352113846, -79.24098389717554,
                                     -76.62676551546362, -71.01471245774239, -68.78074078937429,
                                     -57.39730933358366, -55.597048251741185, -53.807888693814036,
                                     -52.082337027364105, -42.001162570408376, -40.10702874995469,
                                     -36.13941170041208, -34.10866137118017, -29.537625276581757,
                                     -27.807512581350473, -20.493485216120316, -19.180726411630474,
                                     -17.86587524596272, -16.594313970872253, -10.527129255160773,
                                     -9.235915164234662, -6.274431175607891, -4.736234460618425,
                                     -3.113902006580256, -2.3717281394985243, -0.07129875210819334,
                                     2.4178759119876623, 3.3310614360036803, 4.154955748257782,
                                     8.633411865685325, 10.366556352869488, 12.095377640289747,
                                     14.018873556316715, 17.48114915683801, 21.917608573061386,
                                     23.316848960689295, 27.074595655585338, 31.322004721507344,
                                     34.9238763246869, 36.70168716147582, 39.12924266179792,
                                     48.291435480841905, 50.073858853444456, 52.12562986320187,
                                     57.27293729356433, 64.70819943681298, 66.93714122637961,
                                     68.82639047739008, 80.9665198923797, 83.5742202822415,
                                     86.58950836310686, 89.44951473838972, 102.06705367044331,
                                     107.13994334023343, 109.44821746324266, 117.55286230732882,
                                     126.41194103599305, 133.5454916018076, 135.893724579225,
                                     151.52771532059967, 157.8972863072518, 165.22044899155756,
                                     167.77180305861472, 190.84742611813815, 205.77684982675112,
                                     208.5174101551291, 234.35823130936174, 279.92197126989515,
                                     330.06056030550724, 384.6942438609073, 444.0868835363212,
                                     508.62422446622827, 578.8097468653175, 655.2951900145777,
                                     738.9379634908137, 830.8998363959786, 932.8239020042909,
                                     1047.1805607335104, 1178.0384559097015, 1333.1755507750008,
                                     1532.5774637949444
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -5.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//============//
// test # 116 //
//============//

TEST(MatrixTest116, LAPACK_DSYEVX4)
{
     MatrixDense<double> matA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 20) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 20) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvaluesRealSymmetric(eigenvaluesA, "DSYEVX");

     // test

     const double arr_values[100] = {-890.0679762687605,-833.96613152625,-387.9183497645402,
                                     -374.28858505936836,-325.97417429278767,-290.39317003462327,
                                     -243.09349980476634,-237.9671765884564,-184.65425879239723,
                                     -181.6433889406905,-152.42678092373745,-150.01440312367828,
                                     -140.20043836510496,-134.26239413119825,-130.50508513840435,
                                     -128.1365612192653,-119.6243787096752,-117.25234191992651,
                                     -112.47792700587912,-108.89915545206071,-104.74759545668331,
                                     -100.22968339474058,-94.96995783385485,-89.69665862307328,
                                     -86.4677796222291,-68.14673474241698,-66.07309844841615,
                                     -56.302362373674626,-54.63592515665142,-49.54128744936952,
                                     -47.77783428990662,-45.159157747842364,-43.112766536500985,
                                     -40.904881657341534,-38.624975888999664,-36.21821364136139,
                                     -33.61319247902676,-30.618705849873916,-26.992630355951256,
                                     -19.965626399359778,-9.761220034345175,-0.009675776079236078,
                                     -8.793180471674402e-7,-4.675655622579791e-14,-3.291874702847949e-14,
                                     -3.0408119421278934e-14,-2.2328449483329286e-14,
                                     -1.0693016765542536e-14,-7.240806196368183e-15,
                                     -5.761950647030487e-15,-5.957409950992936e-16,3.2491920424847875e-15,
                                     9.219910025371442e-15,1.2799168101873962e-14,1.7108898977066272e-14,
                                     2.855032832850196e-14,4.1048723593164446e-14,9.99499970701814,
                                     18.470571911228987,30.51830887197781,33.54443547978839,
                                     36.1607309907248,38.57320314001222,40.85924266944485,
                                     43.035391586290125,45.287513370945284,47.151147140980484,
                                     50.73276704593593,52.39952223577173,59.50068014643575,
                                     61.31379159039717,74.51253995296709,77.13729578952395,
                                     94.37203226002364,99.43109411760354,102.6487394625419,
                                     105.06218043709777,107.8179491492106,110.12126119755328,
                                     113.57945167126782,116.37801423876415,122.99832759829658,
                                     125.12209476580418,138.05692785144052,140.21814403606146,
                                     161.31660187081715,164.54676840029904,175.43647569692263,
                                     192.3282320950376,207.53405143733352,212.25537253729775,
                                     284.3628999051823,294.6884623476941,341.98290548531804,
                                     507.44043882416463,534.3382764375148,967.6392677402239,
                                     1875.507871717115,3171.7801027349824,5101.18005605357
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -5.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//============//
// test # 117 //
//============//

TEST(MatrixTest117, LAPACK_DSYEVX5)
{
     MatrixDense<double> matA;
     MatrixDense<double> eigenvectorsA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 5) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 5) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvectorsRealSymmetric(eigenvectorsA, "DSYEVX");

     // test

     const double arr_values99[100] = {3.6942314255214093e-22,
                                       1.9280574390556084e-21,
                                       8.336207484683114e-21,
                                       3.201738809930229e-20,
                                       1.128792365433472e-19,
                                       3.7219958073502635e-19,
                                       1.1616570563876596e-18,
                                       3.4603856776191363e-18,
                                       9.898106164891213e-18,
                                       2.7313034552506115e-17,
                                       7.297235551812733e-17,
                                       1.8931798526470812e-16,
                                       4.781008453916819e-16,
                                       1.1776583982769647e-15,
                                       2.8342405979299284e-15,
                                       6.674396729414062e-15,
                                       1.5399303294661802e-14,
                                       3.4849026635393604e-14,
                                       7.743020050094637e-14,
                                       1.6906046576799098e-13,
                                       3.630170569572799e-13,
                                       7.671355827326599e-13,
                                       1.5964469231753074e-12,
                                       3.2735991022182145e-12,
                                       6.617806646334326e-12,
                                       1.3195608156949319e-11,
                                       2.596350061894761e-11,
                                       5.0430333328453936e-11,
                                       9.67336578559975e-11,
                                       1.8330342627363303e-10,
                                       3.4324925188064113e-10,
                                       6.353651498186134e-10,
                                       1.1628708815038037e-9,
                                       2.104965971919157e-9,
                                       3.7693601315726985e-9,
                                       6.6787563519161844e-9,
                                       1.1711672807554324e-8,
                                       2.03292597117336e-8,
                                       3.4936698501777865e-8,
                                       5.9452893015691615e-8,
                                       1.0019897315247485e-7,
                                       1.672693861786821e-7,
                                       2.7662540139461026e-7,
                                       4.5325802559019753e-7,
                                       7.359163073250683e-7,
                                       1.1841012055800893e-6,
                                       1.8883009979218705e-6,
                                       2.984811021145573e-6,
                                       4.676960526639804e-6,
                                       7.265186654807506e-6,
                                       0.000011189174362740128,
                                       0.00001708624363917233,
                                       0.000025871327250800013,
                                       0.00003884524735147213,
                                       0.000057839551835632114,
                                       0.0000854078666854305,
                                       0.00012507547298611522,
                                       0.0001816604867751283,
                                       0.0002616813987570242,
                                       0.00037386653150919884,
                                       0.0005297808051872514,
                                       0.0007445835839285321,
                                       0.0010379277320564163,
                                       0.0014350037062010874,
                                       0.001967722735545796,
                                       0.0026760192519511626,
                                       0.0036092342363378766,
                                       0.00482751783582054,
                                       0.00640315989292051,
                                       0.008421723355192411,
                                       0.010982819682174948,
                                       0.014200329986124536,
                                       0.018201824732492763,
                                       0.023126912983358,
                                       0.029124247016656085,
                                       0.03634692535629019,
                                       0.04494587964943125,
                                       0.055061139884829774,
                                       0.0668110106541383,
                                       0.08027936243852593,
                                       0.09549945152005936,
                                       0.11243731017783884,
                                       0.13097574227719463,
                                       0.15090013051721174,
                                       0.17186879268938443,
                                       0.19341147439162046,
                                       0.21493488140894385,
                                       0.23573601738701488,
                                       0.2548580930562397,
                                       0.27133687410309687,
                                       0.28424352285526244,
                                       0.29273026481733555,
                                       0.29466646943807095,
                                       0.28996990773813897,
                                       0.27878021480447934,
                                       0.26146670556930435,
                                       0.22712948196564564,
                                       0.19198046679049208,
                                       0.15683996268683814,
                                       0.1225409827758915
                                      };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,99)) - abs(arr_values99[i])) < pow(10.0, -10.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//============//
// test # 118 //
//============//

TEST(MatrixTest118, LAPACK_DSYEVX6)
{
     MatrixDense<double> matA;
     MatrixDense<double> eigenvectorsA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 20) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 20) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvectorsRealSymmetric(eigenvectorsA, "DSYEVX");

     // test data # 1

     const double arr_values99[100] = {-0.00002379437463800071,-0.0000321126535355192,
                                       -0.00004261209009056319,-0.00005574228105018768,
                                       -0.0000720258070448845,-0.00009206699311457821,
                                       -0.00011656127426176481,-0.00014630515846035676,
                                       -0.0001822067730093074,-0.00022529697290878234,
                                       -0.00027674098203948964,-0.0003378505293342626,
                                       -0.00041009643284298355,-0.0004951215746125915,
                                       -0.0005947541986465926,-0.0007110214528956958,-0.0008461630842956044,
                                       -0.001002645183353377,-0.0011831738617460526,-0.0013907087329034266,
                                       -0.0016283827619966063,-0.0018997449209395513,-0.002208573606467658,
                                       -0.002558944309118129,-0.0029552087127267114,-0.0034019962501085446,
                                       -0.003904213875493434,-0.00446704386484559,-0.00509593944935361,
                                       -0.005796618082542783,-0.006575052137809326,-0.007437456830885415,
                                       -0.00839027516100404,-0.009440159665541611,-0.010593950785877937,
                                       -0.01185865164733851,-0.013241399063586366,-0.014749430585927434,
                                       -0.0163900474309,-0.018170573135449803,-0.0200983089054282,
                                       -0.022180481856340313,-0.024424190854787288,-0.026834080780647522,
                                       -0.02941440937535356,-0.03216899598488147,-0.035101168178730414,
                                       -0.03821370638490854,-0.04150878671030463,-0.04498792214706611,
                                       -0.04865190239866414,-0.05250073259412269,-0.056533571195309565,
                                       -0.06074866744009344,-0.0651432987033876,-0.06971370819842077,
                                       -0.07445504348175375,-0.07936129626731185,-0.08442524409670342,
                                       -0.08963839445497533,-0.09499093194079421,-0.10047166921101545,
                                       -0.10595789789842808,-0.11143628534678302,-0.11689273862688677,
                                       -0.1223124096438104,-0.12767970465965733,-0.13297829857254914,
                                       -0.13819115429332646,-0.1433005475596976,-0.14828809752292466,
                                       -0.15313480343431787,-0.15782108774752324,-0.16232684593753477,
                                       -0.16663150331824728,-0.17071407911689127,-0.17455325803557586,
                                       -0.17812746949713273,-0.18141497473424503,-0.18439396183722762,
                                       -0.18704264882716626,-0.18541168339207653,-0.18356676120058393,
                                       -0.18150272926411598,-0.17921485423076686,-0.17669886945503793,
                                       -0.17395102341224067,-0.17096812928970723,-0.1677476155625255,
                                       -0.1642875773360043,-0.16058682821057293,-0.15664495239745393,
                                       -0.15246235678535205,-0.14804032262974395,-0.14338105650732136,
                                       -0.13848774014895246,-0.1333645787354236,-0.128016847211483,
                                       -0.1224509341456258,-0.11667438263597216
                                      };

     // test data # 2

     const double arr_values98[100] = {0.0005490815006598427,0.0007217004212979288,0.0009319727820738912,
                                       0.0011856764796110383,0.0014891369301256077,0.0018492353084341568,
                                       0.002273410759398609,0.002769655716932461,0.003346503443557062,
                                       0.00401300689217423,0.0047787079930774546,0.00565359648519078,
                                       0.006648057443072511,0.007772806702325777,0.009038813457661714,
                                       0.010457209401826704,0.012039183891666605,0.013795864771304657,
                                       0.01573818465305239,0.01787673265523282,0.020218129530799774,
                                       0.022773522732384158,0.02555088599547039,0.02855699048006569,
                                       0.03179740706616532,0.03527631554658473,0.038996305859039175,
                                       0.04295817276333242,0.047160705650423344,0.05160047546600458,
                                       0.056271621039461366,0.061165637426456754,0.06627116919601259,
                                       0.07157381191620707,0.0770559254111312,0.08269646266941802,
                                       0.08847081857459133,0.09435070289203276,0.10030404217816305,
                                       0.10629491546542182,0.11228359420772163,0.11822646848984367,
                                       0.12407615562938958,0.12979275139874066,0.13533385216700752,
                                       0.14065467360130016,0.14570820649639804,0.1504454123825699,
                                       0.15481546139101132,0.15876601462676446,0.16224355300490953,
                                       0.1651937541424425,0.16756191846119972,0.16929344514268863,
                                       0.17033435798074814,0.17063188050057534,0.17013505895196374,
                                       0.16879543094112387,0.16656773654130375,0.16341066772151153,
                                       0.1592876487959431,0.15416764632985688,0.14836208495033895,
                                       0.14186022973422263,0.1346552528267162,0.12674464039743252,
                                       0.118130599994188,0.10882046380600134,0.09882708283296263,
                                       0.08816920644738636,0.07687184132594177,0.06496658324539006,
                                       0.052491914775380344,0.039493461481783246,0.026024198885649517,
                                       0.012144602119448178,-0.002077270001960313,-0.0165657649080691,
                                       -0.0312377086295896,-0.04600256891175273,-0.06076271707085829,
                                       -0.06795927022946352,-0.07503806683258157,-0.0819579039270416,
                                       -0.08867572326732134,-0.09514679596043041,-0.1013249415936529,
                                       -0.10716278331612594,-0.1126120400798519,-0.11762385692798413,
                                       -0.12214917384544385,-0.12613913325700105,-0.12954552576940606,
                                       -0.1323212732062947,-0.13442094737761986,-0.13580132236057105,
                                       -0.13642195734879647,-0.13624580635507777,-0.1352398502347542,
                                       -0.13337574564008783
                                      };

     // test data # 3

     const double arr_values97[100] = {-0.0048844047935863765,-0.006236486880389232,-0.007815274325331554,
                                       -0.009640183890712983,-0.011730050533458039,-0.014102771342375649,
                                       -0.016774922205544347,-0.019761352037216788,-0.023074760743478643,
                                       -0.026725268499719908,-0.03071998531275152,-0.03506259120033407,
                                       -0.03975293858755401,-0.04478668963230909,-0.050155002084304526,
                                       -0.05584427788164485,-0.06183598892150303,-0.06810659423095557,
                                       -0.07462756203769172,-0.08136550893055443,-0.08823038013976417,
                                       -0.09520849071940404,-0.1022476308045662,-0.10929156383600795,
                                       -0.11627847844623534,-0.12314131678294159,-0.1298082479646105,
                                       -0.13620329680129123,-0.14224713548726084,-0.14785804289917964,
                                       -0.15295303238959757,-0.1574491445459654,-0.1612648963069822,
                                       -0.16432187213530286,-0.16654643671328404,-0.16787154196597193,
                                       -0.16823859426980445,-0.16759934066305365,-0.16591772596198778,
                                       -0.1631716661722737,-0.15935634408053131,-0.15448321146294608,
                                       -0.14858218139776455,-0.1416381780493364,-0.1336490652093021,
                                       -0.12462715724623677,-0.11460064260169803,-0.10361487512764765,
                                       -0.09173348472751607,-0.07903925562502323,-0.06563471836746396,
                                       -0.051642400613860905,-0.03720468211196078,-0.022483201281379775,
                                       -0.007657764735299888,0.007075282886553215,0.021505263042677962,
                                       0.03540996481701064,0.04855905391412624,0.06071808324036558,
                                       0.07165316959606502,0.08113608035225883,0.09009849834439126,
                                       0.09838663571146652,0.10584187751947657,0.11230295059241002,
                                       0.11760842867321217,0.12159956720711124,0.12412345180969263,
                                       0.1250364341022301,0.12420781716730643,0.12152374055295379,
                                       0.11689120174753254,0.11024213763900717,0.10153747600764697,
                                       0.09077105399613189,0.07797328823931185,0.06321447046527728,
                                       0.04660755350441126,0.028310286413503014,0.008526547885299433,
                                       0.0025719295116293645,-0.004200598749307477,-0.011762440439929252,
                                       -0.020069965854842452,-0.029063538293552106,-0.038666727752430484,
                                       -0.048785756486008615,-0.05930922217827011,-0.07010814475633873,
                                       -0.08103638195706307,-0.09193145642839495,-0.10261583322770457,
                                       -0.11289868090088843,-0.12257814174844638,-0.1314441273038225,
                                       -0.139281643409791,-0.14587463558538188,-0.15101032970590494,
                                       -0.15448402553323257
                                      };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,99)) - abs(arr_values99[i])) < pow(10.0, -10.0));
     }

     // test # 2

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,98)) - abs(arr_values98[i])) < pow(10.0, -10.0));
     }

     // test # 3

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,97)) - abs(arr_values97[i])) < pow(10.0, -10.0));
     }

     // test # 4

     EXPECT_TRUE(res == 0);
}

//============//
// test # 119 //
//============//

TEST(MatrixTest119, LAPACK_DSYEV1)
{
     MatrixDense<double> matA;
     MatrixDense<double> eigenvectorsA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 5) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 5) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigensystemRealSymmetric(eigenvectorsA, eigenvaluesA, "DSYEV");

     // test

     const double arr_values[100] = {-323.38478082263765, -318.95649570052416, -259.5812044929057,
                                     -255.47902023149175, -210.8574832531544, -207.046095983574,
                                     -172.17668982103106, -169.63822144706373, -167.11362728657673,
                                     -164.65335127248554, -137.93078894722365, -135.63554168942136,
                                     -133.27655590262873, -130.90313825750582, -111.16491214977447,
                                     -108.69201485977163, -104.86333072392988, -102.09654131372476,
                                     -89.52930445237439, -87.11011352113846, -79.24098389717554,
                                     -76.62676551546362, -71.01471245774239, -68.78074078937429,
                                     -57.39730933358366, -55.597048251741185, -53.807888693814036,
                                     -52.082337027364105, -42.001162570408376, -40.10702874995469,
                                     -36.13941170041208, -34.10866137118017, -29.537625276581757,
                                     -27.807512581350473, -20.493485216120316, -19.180726411630474,
                                     -17.86587524596272, -16.594313970872253, -10.527129255160773,
                                     -9.235915164234662, -6.274431175607891, -4.736234460618425,
                                     -3.113902006580256, -2.3717281394985243, -0.07129875210819334,
                                     2.4178759119876623, 3.3310614360036803, 4.154955748257782,
                                     8.633411865685325, 10.366556352869488, 12.095377640289747,
                                     14.018873556316715, 17.48114915683801, 21.917608573061386,
                                     23.316848960689295, 27.074595655585338, 31.322004721507344,
                                     34.9238763246869, 36.70168716147582, 39.12924266179792,
                                     48.291435480841905, 50.073858853444456, 52.12562986320187,
                                     57.27293729356433, 64.70819943681298, 66.93714122637961,
                                     68.82639047739008, 80.9665198923797, 83.5742202822415,
                                     86.58950836310686, 89.44951473838972, 102.06705367044331,
                                     107.13994334023343, 109.44821746324266, 117.55286230732882,
                                     126.41194103599305, 133.5454916018076, 135.893724579225,
                                     151.52771532059967, 157.8972863072518, 165.22044899155756,
                                     167.77180305861472, 190.84742611813815, 205.77684982675112,
                                     208.5174101551291, 234.35823130936174, 279.92197126989515,
                                     330.06056030550724, 384.6942438609073, 444.0868835363212,
                                     508.62422446622827, 578.8097468653175, 655.2951900145777,
                                     738.9379634908137, 830.8998363959786, 932.8239020042909,
                                     1047.1805607335104, 1178.0384559097015, 1333.1755507750008,
                                     1532.5774637949444
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -10.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//============//
// test # 120 //
//============//

TEST(MatrixTest120, LAPACK_DSYEV2)
{
     MatrixDense<double> matA;
     MatrixDense<double> eigenvectorsA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 20) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 20) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigensystemRealSymmetric(eigenvectorsA, eigenvaluesA, "DSYEV");

     // test

     const double arr_values[100] = {-890.0679762687605,-833.96613152625,-387.9183497645402,
                                     -374.28858505936836,-325.97417429278767,-290.39317003462327,
                                     -243.09349980476634,-237.9671765884564,-184.65425879239723,
                                     -181.6433889406905,-152.42678092373745,-150.01440312367828,
                                     -140.20043836510496,-134.26239413119825,-130.50508513840435,
                                     -128.1365612192653,-119.6243787096752,-117.25234191992651,
                                     -112.47792700587912,-108.89915545206071,-104.74759545668331,
                                     -100.22968339474058,-94.96995783385485,-89.69665862307328,
                                     -86.4677796222291,-68.14673474241698,-66.07309844841615,
                                     -56.302362373674626,-54.63592515665142,-49.54128744936952,
                                     -47.77783428990662,-45.159157747842364,-43.112766536500985,
                                     -40.904881657341534,-38.624975888999664,-36.21821364136139,
                                     -33.61319247902676,-30.618705849873916,-26.992630355951256,
                                     -19.965626399359778,-9.761220034345175,-0.009675776079236078,
                                     -8.793180471674402e-7,-4.675655622579791e-14,-3.291874702847949e-14,
                                     -3.0408119421278934e-14,-2.2328449483329286e-14,
                                     -1.0693016765542536e-14,-7.240806196368183e-15,
                                     -5.761950647030487e-15,-5.957409950992936e-16,3.2491920424847875e-15,
                                     9.219910025371442e-15,1.2799168101873962e-14,1.7108898977066272e-14,
                                     2.855032832850196e-14,4.1048723593164446e-14,9.99499970701814,
                                     18.470571911228987,30.51830887197781,33.54443547978839,
                                     36.1607309907248,38.57320314001222,40.85924266944485,
                                     43.035391586290125,45.287513370945284,47.151147140980484,
                                     50.73276704593593,52.39952223577173,59.50068014643575,
                                     61.31379159039717,74.51253995296709,77.13729578952395,
                                     94.37203226002364,99.43109411760354,102.6487394625419,
                                     105.06218043709777,107.8179491492106,110.12126119755328,
                                     113.57945167126782,116.37801423876415,122.99832759829658,
                                     125.12209476580418,138.05692785144052,140.21814403606146,
                                     161.31660187081715,164.54676840029904,175.43647569692263,
                                     192.3282320950376,207.53405143733352,212.25537253729775,
                                     284.3628999051823,294.6884623476941,341.98290548531804,
                                     507.44043882416463,534.3382764375148,967.6392677402239,
                                     1875.507871717115,3171.7801027349824,5101.18005605357
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -10.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//============//
// test # 121 //
//============//

TEST(MatrixTest121, LAPACK_DSYEV3)
{
     MatrixDense<double> matA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 5) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 5) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvaluesRealSymmetric(eigenvaluesA, "DSYEV");

     // test

     const double arr_values[100] = {-323.38478082263765, -318.95649570052416, -259.5812044929057,
                                     -255.47902023149175, -210.8574832531544, -207.046095983574,
                                     -172.17668982103106, -169.63822144706373, -167.11362728657673,
                                     -164.65335127248554, -137.93078894722365, -135.63554168942136,
                                     -133.27655590262873, -130.90313825750582, -111.16491214977447,
                                     -108.69201485977163, -104.86333072392988, -102.09654131372476,
                                     -89.52930445237439, -87.11011352113846, -79.24098389717554,
                                     -76.62676551546362, -71.01471245774239, -68.78074078937429,
                                     -57.39730933358366, -55.597048251741185, -53.807888693814036,
                                     -52.082337027364105, -42.001162570408376, -40.10702874995469,
                                     -36.13941170041208, -34.10866137118017, -29.537625276581757,
                                     -27.807512581350473, -20.493485216120316, -19.180726411630474,
                                     -17.86587524596272, -16.594313970872253, -10.527129255160773,
                                     -9.235915164234662, -6.274431175607891, -4.736234460618425,
                                     -3.113902006580256, -2.3717281394985243, -0.07129875210819334,
                                     2.4178759119876623, 3.3310614360036803, 4.154955748257782,
                                     8.633411865685325, 10.366556352869488, 12.095377640289747,
                                     14.018873556316715, 17.48114915683801, 21.917608573061386,
                                     23.316848960689295, 27.074595655585338, 31.322004721507344,
                                     34.9238763246869, 36.70168716147582, 39.12924266179792,
                                     48.291435480841905, 50.073858853444456, 52.12562986320187,
                                     57.27293729356433, 64.70819943681298, 66.93714122637961,
                                     68.82639047739008, 80.9665198923797, 83.5742202822415,
                                     86.58950836310686, 89.44951473838972, 102.06705367044331,
                                     107.13994334023343, 109.44821746324266, 117.55286230732882,
                                     126.41194103599305, 133.5454916018076, 135.893724579225,
                                     151.52771532059967, 157.8972863072518, 165.22044899155756,
                                     167.77180305861472, 190.84742611813815, 205.77684982675112,
                                     208.5174101551291, 234.35823130936174, 279.92197126989515,
                                     330.06056030550724, 384.6942438609073, 444.0868835363212,
                                     508.62422446622827, 578.8097468653175, 655.2951900145777,
                                     738.9379634908137, 830.8998363959786, 932.8239020042909,
                                     1047.1805607335104, 1178.0384559097015, 1333.1755507750008,
                                     1532.5774637949444
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -5.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//============//
// test # 122 //
//============//

TEST(MatrixTest122, LAPACK_DSYEV4)
{
     MatrixDense<double> matA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 20) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 20) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvaluesRealSymmetric(eigenvaluesA, "DSYEV");

     // test

     const double arr_values[100] = {-890.0679762687605,-833.96613152625,-387.9183497645402,
                                     -374.28858505936836,-325.97417429278767,-290.39317003462327,
                                     -243.09349980476634,-237.9671765884564,-184.65425879239723,
                                     -181.6433889406905,-152.42678092373745,-150.01440312367828,
                                     -140.20043836510496,-134.26239413119825,-130.50508513840435,
                                     -128.1365612192653,-119.6243787096752,-117.25234191992651,
                                     -112.47792700587912,-108.89915545206071,-104.74759545668331,
                                     -100.22968339474058,-94.96995783385485,-89.69665862307328,
                                     -86.4677796222291,-68.14673474241698,-66.07309844841615,
                                     -56.302362373674626,-54.63592515665142,-49.54128744936952,
                                     -47.77783428990662,-45.159157747842364,-43.112766536500985,
                                     -40.904881657341534,-38.624975888999664,-36.21821364136139,
                                     -33.61319247902676,-30.618705849873916,-26.992630355951256,
                                     -19.965626399359778,-9.761220034345175,-0.009675776079236078,
                                     -8.793180471674402e-7,-4.675655622579791e-14,-3.291874702847949e-14,
                                     -3.0408119421278934e-14,-2.2328449483329286e-14,
                                     -1.0693016765542536e-14,-7.240806196368183e-15,
                                     -5.761950647030487e-15,-5.957409950992936e-16,3.2491920424847875e-15,
                                     9.219910025371442e-15,1.2799168101873962e-14,1.7108898977066272e-14,
                                     2.855032832850196e-14,4.1048723593164446e-14,9.99499970701814,
                                     18.470571911228987,30.51830887197781,33.54443547978839,
                                     36.1607309907248,38.57320314001222,40.85924266944485,
                                     43.035391586290125,45.287513370945284,47.151147140980484,
                                     50.73276704593593,52.39952223577173,59.50068014643575,
                                     61.31379159039717,74.51253995296709,77.13729578952395,
                                     94.37203226002364,99.43109411760354,102.6487394625419,
                                     105.06218043709777,107.8179491492106,110.12126119755328,
                                     113.57945167126782,116.37801423876415,122.99832759829658,
                                     125.12209476580418,138.05692785144052,140.21814403606146,
                                     161.31660187081715,164.54676840029904,175.43647569692263,
                                     192.3282320950376,207.53405143733352,212.25537253729775,
                                     284.3628999051823,294.6884623476941,341.98290548531804,
                                     507.44043882416463,534.3382764375148,967.6392677402239,
                                     1875.507871717115,3171.7801027349824,5101.18005605357
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -5.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//============//
// test # 123 //
//============//

TEST(MatrixTest123, LAPACK_DSYEV5)
{
     MatrixDense<double> matA;
     MatrixDense<double> eigenvectorsA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 5) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 5) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvectorsRealSymmetric(eigenvectorsA, "DSYEV");

     // test

     const double arr_values99[100] = {3.6942314255214093e-22,
                                       1.9280574390556084e-21,
                                       8.336207484683114e-21,
                                       3.201738809930229e-20,
                                       1.128792365433472e-19,
                                       3.7219958073502635e-19,
                                       1.1616570563876596e-18,
                                       3.4603856776191363e-18,
                                       9.898106164891213e-18,
                                       2.7313034552506115e-17,
                                       7.297235551812733e-17,
                                       1.8931798526470812e-16,
                                       4.781008453916819e-16,
                                       1.1776583982769647e-15,
                                       2.8342405979299284e-15,
                                       6.674396729414062e-15,
                                       1.5399303294661802e-14,
                                       3.4849026635393604e-14,
                                       7.743020050094637e-14,
                                       1.6906046576799098e-13,
                                       3.630170569572799e-13,
                                       7.671355827326599e-13,
                                       1.5964469231753074e-12,
                                       3.2735991022182145e-12,
                                       6.617806646334326e-12,
                                       1.3195608156949319e-11,
                                       2.596350061894761e-11,
                                       5.0430333328453936e-11,
                                       9.67336578559975e-11,
                                       1.8330342627363303e-10,
                                       3.4324925188064113e-10,
                                       6.353651498186134e-10,
                                       1.1628708815038037e-9,
                                       2.104965971919157e-9,
                                       3.7693601315726985e-9,
                                       6.6787563519161844e-9,
                                       1.1711672807554324e-8,
                                       2.03292597117336e-8,
                                       3.4936698501777865e-8,
                                       5.9452893015691615e-8,
                                       1.0019897315247485e-7,
                                       1.672693861786821e-7,
                                       2.7662540139461026e-7,
                                       4.5325802559019753e-7,
                                       7.359163073250683e-7,
                                       1.1841012055800893e-6,
                                       1.8883009979218705e-6,
                                       2.984811021145573e-6,
                                       4.676960526639804e-6,
                                       7.265186654807506e-6,
                                       0.000011189174362740128,
                                       0.00001708624363917233,
                                       0.000025871327250800013,
                                       0.00003884524735147213,
                                       0.000057839551835632114,
                                       0.0000854078666854305,
                                       0.00012507547298611522,
                                       0.0001816604867751283,
                                       0.0002616813987570242,
                                       0.00037386653150919884,
                                       0.0005297808051872514,
                                       0.0007445835839285321,
                                       0.0010379277320564163,
                                       0.0014350037062010874,
                                       0.001967722735545796,
                                       0.0026760192519511626,
                                       0.0036092342363378766,
                                       0.00482751783582054,
                                       0.00640315989292051,
                                       0.008421723355192411,
                                       0.010982819682174948,
                                       0.014200329986124536,
                                       0.018201824732492763,
                                       0.023126912983358,
                                       0.029124247016656085,
                                       0.03634692535629019,
                                       0.04494587964943125,
                                       0.055061139884829774,
                                       0.0668110106541383,
                                       0.08027936243852593,
                                       0.09549945152005936,
                                       0.11243731017783884,
                                       0.13097574227719463,
                                       0.15090013051721174,
                                       0.17186879268938443,
                                       0.19341147439162046,
                                       0.21493488140894385,
                                       0.23573601738701488,
                                       0.2548580930562397,
                                       0.27133687410309687,
                                       0.28424352285526244,
                                       0.29273026481733555,
                                       0.29466646943807095,
                                       0.28996990773813897,
                                       0.27878021480447934,
                                       0.26146670556930435,
                                       0.22712948196564564,
                                       0.19198046679049208,
                                       0.15683996268683814,
                                       0.1225409827758915
                                      };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,99)) - abs(arr_values99[i])) < pow(10.0, -10.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//============//
// test # 124 //
//============//

TEST(MatrixTest124, LAPACK_DSYEV6)
{
     MatrixDense<double> matA;
     MatrixDense<double> eigenvectorsA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 20) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 20) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvectorsRealSymmetric(eigenvectorsA, "DSYEV");

     // test data # 1

     const double arr_values99[100] = {-0.00002379437463800071,-0.0000321126535355192,
                                       -0.00004261209009056319,-0.00005574228105018768,
                                       -0.0000720258070448845,-0.00009206699311457821,
                                       -0.00011656127426176481,-0.00014630515846035676,
                                       -0.0001822067730093074,-0.00022529697290878234,
                                       -0.00027674098203948964,-0.0003378505293342626,
                                       -0.00041009643284298355,-0.0004951215746125915,
                                       -0.0005947541986465926,-0.0007110214528956958,-0.0008461630842956044,
                                       -0.001002645183353377,-0.0011831738617460526,-0.0013907087329034266,
                                       -0.0016283827619966063,-0.0018997449209395513,-0.002208573606467658,
                                       -0.002558944309118129,-0.0029552087127267114,-0.0034019962501085446,
                                       -0.003904213875493434,-0.00446704386484559,-0.00509593944935361,
                                       -0.005796618082542783,-0.006575052137809326,-0.007437456830885415,
                                       -0.00839027516100404,-0.009440159665541611,-0.010593950785877937,
                                       -0.01185865164733851,-0.013241399063586366,-0.014749430585927434,
                                       -0.0163900474309,-0.018170573135449803,-0.0200983089054282,
                                       -0.022180481856340313,-0.024424190854787288,-0.026834080780647522,
                                       -0.02941440937535356,-0.03216899598488147,-0.035101168178730414,
                                       -0.03821370638490854,-0.04150878671030463,-0.04498792214706611,
                                       -0.04865190239866414,-0.05250073259412269,-0.056533571195309565,
                                       -0.06074866744009344,-0.0651432987033876,-0.06971370819842077,
                                       -0.07445504348175375,-0.07936129626731185,-0.08442524409670342,
                                       -0.08963839445497533,-0.09499093194079421,-0.10047166921101545,
                                       -0.10595789789842808,-0.11143628534678302,-0.11689273862688677,
                                       -0.1223124096438104,-0.12767970465965733,-0.13297829857254914,
                                       -0.13819115429332646,-0.1433005475596976,-0.14828809752292466,
                                       -0.15313480343431787,-0.15782108774752324,-0.16232684593753477,
                                       -0.16663150331824728,-0.17071407911689127,-0.17455325803557586,
                                       -0.17812746949713273,-0.18141497473424503,-0.18439396183722762,
                                       -0.18704264882716626,-0.18541168339207653,-0.18356676120058393,
                                       -0.18150272926411598,-0.17921485423076686,-0.17669886945503793,
                                       -0.17395102341224067,-0.17096812928970723,-0.1677476155625255,
                                       -0.1642875773360043,-0.16058682821057293,-0.15664495239745393,
                                       -0.15246235678535205,-0.14804032262974395,-0.14338105650732136,
                                       -0.13848774014895246,-0.1333645787354236,-0.128016847211483,
                                       -0.1224509341456258,-0.11667438263597216
                                      };

     // test data # 2

     const double arr_values98[100] = {0.0005490815006598427,0.0007217004212979288,0.0009319727820738912,
                                       0.0011856764796110383,0.0014891369301256077,0.0018492353084341568,
                                       0.002273410759398609,0.002769655716932461,0.003346503443557062,
                                       0.00401300689217423,0.0047787079930774546,0.00565359648519078,
                                       0.006648057443072511,0.007772806702325777,0.009038813457661714,
                                       0.010457209401826704,0.012039183891666605,0.013795864771304657,
                                       0.01573818465305239,0.01787673265523282,0.020218129530799774,
                                       0.022773522732384158,0.02555088599547039,0.02855699048006569,
                                       0.03179740706616532,0.03527631554658473,0.038996305859039175,
                                       0.04295817276333242,0.047160705650423344,0.05160047546600458,
                                       0.056271621039461366,0.061165637426456754,0.06627116919601259,
                                       0.07157381191620707,0.0770559254111312,0.08269646266941802,
                                       0.08847081857459133,0.09435070289203276,0.10030404217816305,
                                       0.10629491546542182,0.11228359420772163,0.11822646848984367,
                                       0.12407615562938958,0.12979275139874066,0.13533385216700752,
                                       0.14065467360130016,0.14570820649639804,0.1504454123825699,
                                       0.15481546139101132,0.15876601462676446,0.16224355300490953,
                                       0.1651937541424425,0.16756191846119972,0.16929344514268863,
                                       0.17033435798074814,0.17063188050057534,0.17013505895196374,
                                       0.16879543094112387,0.16656773654130375,0.16341066772151153,
                                       0.1592876487959431,0.15416764632985688,0.14836208495033895,
                                       0.14186022973422263,0.1346552528267162,0.12674464039743252,
                                       0.118130599994188,0.10882046380600134,0.09882708283296263,
                                       0.08816920644738636,0.07687184132594177,0.06496658324539006,
                                       0.052491914775380344,0.039493461481783246,0.026024198885649517,
                                       0.012144602119448178,-0.002077270001960313,-0.0165657649080691,
                                       -0.0312377086295896,-0.04600256891175273,-0.06076271707085829,
                                       -0.06795927022946352,-0.07503806683258157,-0.0819579039270416,
                                       -0.08867572326732134,-0.09514679596043041,-0.1013249415936529,
                                       -0.10716278331612594,-0.1126120400798519,-0.11762385692798413,
                                       -0.12214917384544385,-0.12613913325700105,-0.12954552576940606,
                                       -0.1323212732062947,-0.13442094737761986,-0.13580132236057105,
                                       -0.13642195734879647,-0.13624580635507777,-0.1352398502347542,
                                       -0.13337574564008783
                                      };

     // test data # 3

     const double arr_values97[100] = {-0.0048844047935863765,-0.006236486880389232,-0.007815274325331554,
                                       -0.009640183890712983,-0.011730050533458039,-0.014102771342375649,
                                       -0.016774922205544347,-0.019761352037216788,-0.023074760743478643,
                                       -0.026725268499719908,-0.03071998531275152,-0.03506259120033407,
                                       -0.03975293858755401,-0.04478668963230909,-0.050155002084304526,
                                       -0.05584427788164485,-0.06183598892150303,-0.06810659423095557,
                                       -0.07462756203769172,-0.08136550893055443,-0.08823038013976417,
                                       -0.09520849071940404,-0.1022476308045662,-0.10929156383600795,
                                       -0.11627847844623534,-0.12314131678294159,-0.1298082479646105,
                                       -0.13620329680129123,-0.14224713548726084,-0.14785804289917964,
                                       -0.15295303238959757,-0.1574491445459654,-0.1612648963069822,
                                       -0.16432187213530286,-0.16654643671328404,-0.16787154196597193,
                                       -0.16823859426980445,-0.16759934066305365,-0.16591772596198778,
                                       -0.1631716661722737,-0.15935634408053131,-0.15448321146294608,
                                       -0.14858218139776455,-0.1416381780493364,-0.1336490652093021,
                                       -0.12462715724623677,-0.11460064260169803,-0.10361487512764765,
                                       -0.09173348472751607,-0.07903925562502323,-0.06563471836746396,
                                       -0.051642400613860905,-0.03720468211196078,-0.022483201281379775,
                                       -0.007657764735299888,0.007075282886553215,0.021505263042677962,
                                       0.03540996481701064,0.04855905391412624,0.06071808324036558,
                                       0.07165316959606502,0.08113608035225883,0.09009849834439126,
                                       0.09838663571146652,0.10584187751947657,0.11230295059241002,
                                       0.11760842867321217,0.12159956720711124,0.12412345180969263,
                                       0.1250364341022301,0.12420781716730643,0.12152374055295379,
                                       0.11689120174753254,0.11024213763900717,0.10153747600764697,
                                       0.09077105399613189,0.07797328823931185,0.06321447046527728,
                                       0.04660755350441126,0.028310286413503014,0.008526547885299433,
                                       0.0025719295116293645,-0.004200598749307477,-0.011762440439929252,
                                       -0.020069965854842452,-0.029063538293552106,-0.038666727752430484,
                                       -0.048785756486008615,-0.05930922217827011,-0.07010814475633873,
                                       -0.08103638195706307,-0.09193145642839495,-0.10261583322770457,
                                       -0.11289868090088843,-0.12257814174844638,-0.1314441273038225,
                                       -0.139281643409791,-0.14587463558538188,-0.15101032970590494,
                                       -0.15448402553323257
                                      };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,99)) - abs(arr_values99[i])) < pow(10.0, -10.0));
     }

     // test # 2

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,98)) - abs(arr_values98[i])) < pow(10.0, -10.0));
     }

     // test # 3

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,97)) - abs(arr_values97[i])) < pow(10.0, -10.0));
     }

     // test # 4

     EXPECT_TRUE(res == 0);
}

//============//
// test # 125 //
//============//

TEST(MatrixTest125, LAPACK_ZHEEV1)
{
     MatrixDense<complex<double>> matA;
     MatrixDense<complex<double>> eigenvectorsA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 5) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 5) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigensystemZHEEV(eigenvectorsA, eigenvaluesA);

     // test

     const double arr_values[100] = {-323.38478082263765, -318.95649570052416, -259.5812044929057,
                                     -255.47902023149175, -210.8574832531544, -207.046095983574,
                                     -172.17668982103106, -169.63822144706373, -167.11362728657673,
                                     -164.65335127248554, -137.93078894722365, -135.63554168942136,
                                     -133.27655590262873, -130.90313825750582, -111.16491214977447,
                                     -108.69201485977163, -104.86333072392988, -102.09654131372476,
                                     -89.52930445237439, -87.11011352113846, -79.24098389717554,
                                     -76.62676551546362, -71.01471245774239, -68.78074078937429,
                                     -57.39730933358366, -55.597048251741185, -53.807888693814036,
                                     -52.082337027364105, -42.001162570408376, -40.10702874995469,
                                     -36.13941170041208, -34.10866137118017, -29.537625276581757,
                                     -27.807512581350473, -20.493485216120316, -19.180726411630474,
                                     -17.86587524596272, -16.594313970872253, -10.527129255160773,
                                     -9.235915164234662, -6.274431175607891, -4.736234460618425,
                                     -3.113902006580256, -2.3717281394985243, -0.07129875210819334,
                                     2.4178759119876623, 3.3310614360036803, 4.154955748257782,
                                     8.633411865685325, 10.366556352869488, 12.095377640289747,
                                     14.018873556316715, 17.48114915683801, 21.917608573061386,
                                     23.316848960689295, 27.074595655585338, 31.322004721507344,
                                     34.9238763246869, 36.70168716147582, 39.12924266179792,
                                     48.291435480841905, 50.073858853444456, 52.12562986320187,
                                     57.27293729356433, 64.70819943681298, 66.93714122637961,
                                     68.82639047739008, 80.9665198923797, 83.5742202822415,
                                     86.58950836310686, 89.44951473838972, 102.06705367044331,
                                     107.13994334023343, 109.44821746324266, 117.55286230732882,
                                     126.41194103599305, 133.5454916018076, 135.893724579225,
                                     151.52771532059967, 157.8972863072518, 165.22044899155756,
                                     167.77180305861472, 190.84742611813815, 205.77684982675112,
                                     208.5174101551291, 234.35823130936174, 279.92197126989515,
                                     330.06056030550724, 384.6942438609073, 444.0868835363212,
                                     508.62422446622827, 578.8097468653175, 655.2951900145777,
                                     738.9379634908137, 830.8998363959786, 932.8239020042909,
                                     1047.1805607335104, 1178.0384559097015, 1333.1755507750008,
                                     1532.5774637949444
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -10.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//============//
// test # 126 //
//============//

TEST(MatrixTest126, LAPACK_ZHEEV2)
{
     MatrixDense<complex<double>> matA;
     MatrixDense<complex<double>> eigenvectorsA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 20) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 20) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigensystemZHEEV(eigenvectorsA, eigenvaluesA);

     // test

     const double arr_values[100] = {-890.0679762687605,-833.96613152625,-387.9183497645402,
                                     -374.28858505936836,-325.97417429278767,-290.39317003462327,
                                     -243.09349980476634,-237.9671765884564,-184.65425879239723,
                                     -181.6433889406905,-152.42678092373745,-150.01440312367828,
                                     -140.20043836510496,-134.26239413119825,-130.50508513840435,
                                     -128.1365612192653,-119.6243787096752,-117.25234191992651,
                                     -112.47792700587912,-108.89915545206071,-104.74759545668331,
                                     -100.22968339474058,-94.96995783385485,-89.69665862307328,
                                     -86.4677796222291,-68.14673474241698,-66.07309844841615,
                                     -56.302362373674626,-54.63592515665142,-49.54128744936952,
                                     -47.77783428990662,-45.159157747842364,-43.112766536500985,
                                     -40.904881657341534,-38.624975888999664,-36.21821364136139,
                                     -33.61319247902676,-30.618705849873916,-26.992630355951256,
                                     -19.965626399359778,-9.761220034345175,-0.009675776079236078,
                                     -8.793180471674402e-7,-4.675655622579791e-14,-3.291874702847949e-14,
                                     -3.0408119421278934e-14,-2.2328449483329286e-14,
                                     -1.0693016765542536e-14,-7.240806196368183e-15,
                                     -5.761950647030487e-15,-5.957409950992936e-16,3.2491920424847875e-15,
                                     9.219910025371442e-15,1.2799168101873962e-14,1.7108898977066272e-14,
                                     2.855032832850196e-14,4.1048723593164446e-14,9.99499970701814,
                                     18.470571911228987,30.51830887197781,33.54443547978839,
                                     36.1607309907248,38.57320314001222,40.85924266944485,
                                     43.035391586290125,45.287513370945284,47.151147140980484,
                                     50.73276704593593,52.39952223577173,59.50068014643575,
                                     61.31379159039717,74.51253995296709,77.13729578952395,
                                     94.37203226002364,99.43109411760354,102.6487394625419,
                                     105.06218043709777,107.8179491492106,110.12126119755328,
                                     113.57945167126782,116.37801423876415,122.99832759829658,
                                     125.12209476580418,138.05692785144052,140.21814403606146,
                                     161.31660187081715,164.54676840029904,175.43647569692263,
                                     192.3282320950376,207.53405143733352,212.25537253729775,
                                     284.3628999051823,294.6884623476941,341.98290548531804,
                                     507.44043882416463,534.3382764375148,967.6392677402239,
                                     1875.507871717115,3171.7801027349824,5101.18005605357
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -10.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//============//
// test # 127 //
//============//

TEST(MatrixTest127, LAPACK_ZHEEV3)
{
     MatrixDense<complex<double>> matA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 5) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 5) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvaluesZHEEV(eigenvaluesA);

     // test

     const double arr_values[100] = {-323.38478082263765, -318.95649570052416, -259.5812044929057,
                                     -255.47902023149175, -210.8574832531544, -207.046095983574,
                                     -172.17668982103106, -169.63822144706373, -167.11362728657673,
                                     -164.65335127248554, -137.93078894722365, -135.63554168942136,
                                     -133.27655590262873, -130.90313825750582, -111.16491214977447,
                                     -108.69201485977163, -104.86333072392988, -102.09654131372476,
                                     -89.52930445237439, -87.11011352113846, -79.24098389717554,
                                     -76.62676551546362, -71.01471245774239, -68.78074078937429,
                                     -57.39730933358366, -55.597048251741185, -53.807888693814036,
                                     -52.082337027364105, -42.001162570408376, -40.10702874995469,
                                     -36.13941170041208, -34.10866137118017, -29.537625276581757,
                                     -27.807512581350473, -20.493485216120316, -19.180726411630474,
                                     -17.86587524596272, -16.594313970872253, -10.527129255160773,
                                     -9.235915164234662, -6.274431175607891, -4.736234460618425,
                                     -3.113902006580256, -2.3717281394985243, -0.07129875210819334,
                                     2.4178759119876623, 3.3310614360036803, 4.154955748257782,
                                     8.633411865685325, 10.366556352869488, 12.095377640289747,
                                     14.018873556316715, 17.48114915683801, 21.917608573061386,
                                     23.316848960689295, 27.074595655585338, 31.322004721507344,
                                     34.9238763246869, 36.70168716147582, 39.12924266179792,
                                     48.291435480841905, 50.073858853444456, 52.12562986320187,
                                     57.27293729356433, 64.70819943681298, 66.93714122637961,
                                     68.82639047739008, 80.9665198923797, 83.5742202822415,
                                     86.58950836310686, 89.44951473838972, 102.06705367044331,
                                     107.13994334023343, 109.44821746324266, 117.55286230732882,
                                     126.41194103599305, 133.5454916018076, 135.893724579225,
                                     151.52771532059967, 157.8972863072518, 165.22044899155756,
                                     167.77180305861472, 190.84742611813815, 205.77684982675112,
                                     208.5174101551291, 234.35823130936174, 279.92197126989515,
                                     330.06056030550724, 384.6942438609073, 444.0868835363212,
                                     508.62422446622827, 578.8097468653175, 655.2951900145777,
                                     738.9379634908137, 830.8998363959786, 932.8239020042909,
                                     1047.1805607335104, 1178.0384559097015, 1333.1755507750008,
                                     1532.5774637949444
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -5.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//============//
// test # 128 //
//============//

TEST(MatrixTest128, LAPACK_ZHEEV4)
{
     MatrixDense<complex<double>> matA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 20) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 20) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvaluesZHEEV(eigenvaluesA);

     // test

     const double arr_values[100] = {-890.0679762687605,-833.96613152625,-387.9183497645402,
                                     -374.28858505936836,-325.97417429278767,-290.39317003462327,
                                     -243.09349980476634,-237.9671765884564,-184.65425879239723,
                                     -181.6433889406905,-152.42678092373745,-150.01440312367828,
                                     -140.20043836510496,-134.26239413119825,-130.50508513840435,
                                     -128.1365612192653,-119.6243787096752,-117.25234191992651,
                                     -112.47792700587912,-108.89915545206071,-104.74759545668331,
                                     -100.22968339474058,-94.96995783385485,-89.69665862307328,
                                     -86.4677796222291,-68.14673474241698,-66.07309844841615,
                                     -56.302362373674626,-54.63592515665142,-49.54128744936952,
                                     -47.77783428990662,-45.159157747842364,-43.112766536500985,
                                     -40.904881657341534,-38.624975888999664,-36.21821364136139,
                                     -33.61319247902676,-30.618705849873916,-26.992630355951256,
                                     -19.965626399359778,-9.761220034345175,-0.009675776079236078,
                                     -8.793180471674402e-7,-4.675655622579791e-14,-3.291874702847949e-14,
                                     -3.0408119421278934e-14,-2.2328449483329286e-14,
                                     -1.0693016765542536e-14,-7.240806196368183e-15,
                                     -5.761950647030487e-15,-5.957409950992936e-16,3.2491920424847875e-15,
                                     9.219910025371442e-15,1.2799168101873962e-14,1.7108898977066272e-14,
                                     2.855032832850196e-14,4.1048723593164446e-14,9.99499970701814,
                                     18.470571911228987,30.51830887197781,33.54443547978839,
                                     36.1607309907248,38.57320314001222,40.85924266944485,
                                     43.035391586290125,45.287513370945284,47.151147140980484,
                                     50.73276704593593,52.39952223577173,59.50068014643575,
                                     61.31379159039717,74.51253995296709,77.13729578952395,
                                     94.37203226002364,99.43109411760354,102.6487394625419,
                                     105.06218043709777,107.8179491492106,110.12126119755328,
                                     113.57945167126782,116.37801423876415,122.99832759829658,
                                     125.12209476580418,138.05692785144052,140.21814403606146,
                                     161.31660187081715,164.54676840029904,175.43647569692263,
                                     192.3282320950376,207.53405143733352,212.25537253729775,
                                     284.3628999051823,294.6884623476941,341.98290548531804,
                                     507.44043882416463,534.3382764375148,967.6392677402239,
                                     1875.507871717115,3171.7801027349824,5101.18005605357
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -5.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//============//
// test # 129 //
//============//

TEST(MatrixTest129, LAPACK_ZHEEV5)
{
     MatrixDense<complex<double>> matA;
     MatrixDense<complex<double>> eigenvectorsA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 5) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 5) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvectorsZHEEV(eigenvectorsA);

     // test

     const double arr_values99[100] = {3.6942314255214093e-22,
                                       1.9280574390556084e-21,
                                       8.336207484683114e-21,
                                       3.201738809930229e-20,
                                       1.128792365433472e-19,
                                       3.7219958073502635e-19,
                                       1.1616570563876596e-18,
                                       3.4603856776191363e-18,
                                       9.898106164891213e-18,
                                       2.7313034552506115e-17,
                                       7.297235551812733e-17,
                                       1.8931798526470812e-16,
                                       4.781008453916819e-16,
                                       1.1776583982769647e-15,
                                       2.8342405979299284e-15,
                                       6.674396729414062e-15,
                                       1.5399303294661802e-14,
                                       3.4849026635393604e-14,
                                       7.743020050094637e-14,
                                       1.6906046576799098e-13,
                                       3.630170569572799e-13,
                                       7.671355827326599e-13,
                                       1.5964469231753074e-12,
                                       3.2735991022182145e-12,
                                       6.617806646334326e-12,
                                       1.3195608156949319e-11,
                                       2.596350061894761e-11,
                                       5.0430333328453936e-11,
                                       9.67336578559975e-11,
                                       1.8330342627363303e-10,
                                       3.4324925188064113e-10,
                                       6.353651498186134e-10,
                                       1.1628708815038037e-9,
                                       2.104965971919157e-9,
                                       3.7693601315726985e-9,
                                       6.6787563519161844e-9,
                                       1.1711672807554324e-8,
                                       2.03292597117336e-8,
                                       3.4936698501777865e-8,
                                       5.9452893015691615e-8,
                                       1.0019897315247485e-7,
                                       1.672693861786821e-7,
                                       2.7662540139461026e-7,
                                       4.5325802559019753e-7,
                                       7.359163073250683e-7,
                                       1.1841012055800893e-6,
                                       1.8883009979218705e-6,
                                       2.984811021145573e-6,
                                       4.676960526639804e-6,
                                       7.265186654807506e-6,
                                       0.000011189174362740128,
                                       0.00001708624363917233,
                                       0.000025871327250800013,
                                       0.00003884524735147213,
                                       0.000057839551835632114,
                                       0.0000854078666854305,
                                       0.00012507547298611522,
                                       0.0001816604867751283,
                                       0.0002616813987570242,
                                       0.00037386653150919884,
                                       0.0005297808051872514,
                                       0.0007445835839285321,
                                       0.0010379277320564163,
                                       0.0014350037062010874,
                                       0.001967722735545796,
                                       0.0026760192519511626,
                                       0.0036092342363378766,
                                       0.00482751783582054,
                                       0.00640315989292051,
                                       0.008421723355192411,
                                       0.010982819682174948,
                                       0.014200329986124536,
                                       0.018201824732492763,
                                       0.023126912983358,
                                       0.029124247016656085,
                                       0.03634692535629019,
                                       0.04494587964943125,
                                       0.055061139884829774,
                                       0.0668110106541383,
                                       0.08027936243852593,
                                       0.09549945152005936,
                                       0.11243731017783884,
                                       0.13097574227719463,
                                       0.15090013051721174,
                                       0.17186879268938443,
                                       0.19341147439162046,
                                       0.21493488140894385,
                                       0.23573601738701488,
                                       0.2548580930562397,
                                       0.27133687410309687,
                                       0.28424352285526244,
                                       0.29273026481733555,
                                       0.29466646943807095,
                                       0.28996990773813897,
                                       0.27878021480447934,
                                       0.26146670556930435,
                                       0.22712948196564564,
                                       0.19198046679049208,
                                       0.15683996268683814,
                                       0.1225409827758915
                                      };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,99)) - abs(arr_values99[i])) < pow(10.0, -10.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//============//
// test # 130 //
//============//

TEST(MatrixTest130, LAPACK_ZHEEV6)
{
     MatrixDense<complex<double>> matA;
     MatrixDense<complex<double>> eigenvectorsA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 20) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 20) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvectorsZHEEV(eigenvectorsA);

     // test data # 1

     const double arr_values99[100] = {-0.00002379437463800071,-0.0000321126535355192,
                                       -0.00004261209009056319,-0.00005574228105018768,
                                       -0.0000720258070448845,-0.00009206699311457821,
                                       -0.00011656127426176481,-0.00014630515846035676,
                                       -0.0001822067730093074,-0.00022529697290878234,
                                       -0.00027674098203948964,-0.0003378505293342626,
                                       -0.00041009643284298355,-0.0004951215746125915,
                                       -0.0005947541986465926,-0.0007110214528956958,-0.0008461630842956044,
                                       -0.001002645183353377,-0.0011831738617460526,-0.0013907087329034266,
                                       -0.0016283827619966063,-0.0018997449209395513,-0.002208573606467658,
                                       -0.002558944309118129,-0.0029552087127267114,-0.0034019962501085446,
                                       -0.003904213875493434,-0.00446704386484559,-0.00509593944935361,
                                       -0.005796618082542783,-0.006575052137809326,-0.007437456830885415,
                                       -0.00839027516100404,-0.009440159665541611,-0.010593950785877937,
                                       -0.01185865164733851,-0.013241399063586366,-0.014749430585927434,
                                       -0.0163900474309,-0.018170573135449803,-0.0200983089054282,
                                       -0.022180481856340313,-0.024424190854787288,-0.026834080780647522,
                                       -0.02941440937535356,-0.03216899598488147,-0.035101168178730414,
                                       -0.03821370638490854,-0.04150878671030463,-0.04498792214706611,
                                       -0.04865190239866414,-0.05250073259412269,-0.056533571195309565,
                                       -0.06074866744009344,-0.0651432987033876,-0.06971370819842077,
                                       -0.07445504348175375,-0.07936129626731185,-0.08442524409670342,
                                       -0.08963839445497533,-0.09499093194079421,-0.10047166921101545,
                                       -0.10595789789842808,-0.11143628534678302,-0.11689273862688677,
                                       -0.1223124096438104,-0.12767970465965733,-0.13297829857254914,
                                       -0.13819115429332646,-0.1433005475596976,-0.14828809752292466,
                                       -0.15313480343431787,-0.15782108774752324,-0.16232684593753477,
                                       -0.16663150331824728,-0.17071407911689127,-0.17455325803557586,
                                       -0.17812746949713273,-0.18141497473424503,-0.18439396183722762,
                                       -0.18704264882716626,-0.18541168339207653,-0.18356676120058393,
                                       -0.18150272926411598,-0.17921485423076686,-0.17669886945503793,
                                       -0.17395102341224067,-0.17096812928970723,-0.1677476155625255,
                                       -0.1642875773360043,-0.16058682821057293,-0.15664495239745393,
                                       -0.15246235678535205,-0.14804032262974395,-0.14338105650732136,
                                       -0.13848774014895246,-0.1333645787354236,-0.128016847211483,
                                       -0.1224509341456258,-0.11667438263597216
                                      };

     // test data # 2

     const double arr_values98[100] = {0.0005490815006598427,0.0007217004212979288,0.0009319727820738912,
                                       0.0011856764796110383,0.0014891369301256077,0.0018492353084341568,
                                       0.002273410759398609,0.002769655716932461,0.003346503443557062,
                                       0.00401300689217423,0.0047787079930774546,0.00565359648519078,
                                       0.006648057443072511,0.007772806702325777,0.009038813457661714,
                                       0.010457209401826704,0.012039183891666605,0.013795864771304657,
                                       0.01573818465305239,0.01787673265523282,0.020218129530799774,
                                       0.022773522732384158,0.02555088599547039,0.02855699048006569,
                                       0.03179740706616532,0.03527631554658473,0.038996305859039175,
                                       0.04295817276333242,0.047160705650423344,0.05160047546600458,
                                       0.056271621039461366,0.061165637426456754,0.06627116919601259,
                                       0.07157381191620707,0.0770559254111312,0.08269646266941802,
                                       0.08847081857459133,0.09435070289203276,0.10030404217816305,
                                       0.10629491546542182,0.11228359420772163,0.11822646848984367,
                                       0.12407615562938958,0.12979275139874066,0.13533385216700752,
                                       0.14065467360130016,0.14570820649639804,0.1504454123825699,
                                       0.15481546139101132,0.15876601462676446,0.16224355300490953,
                                       0.1651937541424425,0.16756191846119972,0.16929344514268863,
                                       0.17033435798074814,0.17063188050057534,0.17013505895196374,
                                       0.16879543094112387,0.16656773654130375,0.16341066772151153,
                                       0.1592876487959431,0.15416764632985688,0.14836208495033895,
                                       0.14186022973422263,0.1346552528267162,0.12674464039743252,
                                       0.118130599994188,0.10882046380600134,0.09882708283296263,
                                       0.08816920644738636,0.07687184132594177,0.06496658324539006,
                                       0.052491914775380344,0.039493461481783246,0.026024198885649517,
                                       0.012144602119448178,-0.002077270001960313,-0.0165657649080691,
                                       -0.0312377086295896,-0.04600256891175273,-0.06076271707085829,
                                       -0.06795927022946352,-0.07503806683258157,-0.0819579039270416,
                                       -0.08867572326732134,-0.09514679596043041,-0.1013249415936529,
                                       -0.10716278331612594,-0.1126120400798519,-0.11762385692798413,
                                       -0.12214917384544385,-0.12613913325700105,-0.12954552576940606,
                                       -0.1323212732062947,-0.13442094737761986,-0.13580132236057105,
                                       -0.13642195734879647,-0.13624580635507777,-0.1352398502347542,
                                       -0.13337574564008783
                                      };

     // test data # 3

     const double arr_values97[100] = {-0.0048844047935863765,-0.006236486880389232,-0.007815274325331554,
                                       -0.009640183890712983,-0.011730050533458039,-0.014102771342375649,
                                       -0.016774922205544347,-0.019761352037216788,-0.023074760743478643,
                                       -0.026725268499719908,-0.03071998531275152,-0.03506259120033407,
                                       -0.03975293858755401,-0.04478668963230909,-0.050155002084304526,
                                       -0.05584427788164485,-0.06183598892150303,-0.06810659423095557,
                                       -0.07462756203769172,-0.08136550893055443,-0.08823038013976417,
                                       -0.09520849071940404,-0.1022476308045662,-0.10929156383600795,
                                       -0.11627847844623534,-0.12314131678294159,-0.1298082479646105,
                                       -0.13620329680129123,-0.14224713548726084,-0.14785804289917964,
                                       -0.15295303238959757,-0.1574491445459654,-0.1612648963069822,
                                       -0.16432187213530286,-0.16654643671328404,-0.16787154196597193,
                                       -0.16823859426980445,-0.16759934066305365,-0.16591772596198778,
                                       -0.1631716661722737,-0.15935634408053131,-0.15448321146294608,
                                       -0.14858218139776455,-0.1416381780493364,-0.1336490652093021,
                                       -0.12462715724623677,-0.11460064260169803,-0.10361487512764765,
                                       -0.09173348472751607,-0.07903925562502323,-0.06563471836746396,
                                       -0.051642400613860905,-0.03720468211196078,-0.022483201281379775,
                                       -0.007657764735299888,0.007075282886553215,0.021505263042677962,
                                       0.03540996481701064,0.04855905391412624,0.06071808324036558,
                                       0.07165316959606502,0.08113608035225883,0.09009849834439126,
                                       0.09838663571146652,0.10584187751947657,0.11230295059241002,
                                       0.11760842867321217,0.12159956720711124,0.12412345180969263,
                                       0.1250364341022301,0.12420781716730643,0.12152374055295379,
                                       0.11689120174753254,0.11024213763900717,0.10153747600764697,
                                       0.09077105399613189,0.07797328823931185,0.06321447046527728,
                                       0.04660755350441126,0.028310286413503014,0.008526547885299433,
                                       0.0025719295116293645,-0.004200598749307477,-0.011762440439929252,
                                       -0.020069965854842452,-0.029063538293552106,-0.038666727752430484,
                                       -0.048785756486008615,-0.05930922217827011,-0.07010814475633873,
                                       -0.08103638195706307,-0.09193145642839495,-0.10261583322770457,
                                       -0.11289868090088843,-0.12257814174844638,-0.1314441273038225,
                                       -0.139281643409791,-0.14587463558538188,-0.15101032970590494,
                                       -0.15448402553323257
                                      };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,99)) - abs(arr_values99[i])) < pow(10.0, -10.0));
     }

     // test # 2

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,98)) - abs(arr_values98[i])) < pow(10.0, -10.0));
     }

     // test # 3

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,97)) - abs(arr_values97[i])) < pow(10.0, -10.0));
     }

     // test # 4

     EXPECT_TRUE(res == 0);
}

//============//
// test # 131 //
//============//

TEST(MatrixTest131, RealImaginaryPart1)
{
     MatrixDense<complex<double>> matA;
     MatrixDense<double> matB;
     MatrixDense<double> matC;
     const int K_MAX = 10;

     // allocations

     const pgg::MAI DIM = 1*static_cast<pgg::MAI>(pow(10.0, 2.0));

     matA.Allocate(DIM);
     matB.Allocate(DIM);
     matC.Allocate(DIM);

     matA = {-1.23456, +3.45678};

     for (int k = 0; k != K_MAX; ++k) {
          matB.RealPart(matA);
          matC.ImaginaryPart(matA);
     }

     EXPECT_TRUE(matB == -1.23456);
     EXPECT_TRUE(matC == +3.45678);
}

//============//
// test # 132 //
//============//

TEST(MatrixTest132, RealImaginaryPart2)
{
     MatrixDense<complex<double>> matA;
     MatrixDense<double> matB;
     MatrixDense<double> matC;
     const int K_MAX = 10;

     // allocations

     const pgg::MAI DIM = 1*static_cast<pgg::MAI>(pow(10.0, 2.0));

     matA.Allocate(DIM);
     matB.Allocate(DIM);
     matC.Allocate(DIM);

     // build matrix

     for (pgg::MAI_MAX i = 0; i != DIM; ++i) {
          for (pgg::MAI_MAX j = 0; j != DIM; ++j) {
               const complex<double> tmp_elem = { static_cast<double>(i),
                                                  static_cast<double>(j)
                                                };

               matA.SetElement(i,j, tmp_elem);
          }
     }

     // get real and imaginary parts

     for (int k = 0; k != K_MAX; ++k) {
          matB.RealPart(matA);
          matC.ImaginaryPart(matA);
     }

     // test

     for (pgg::MAI_MAX i = 0; i != DIM; ++i) {
          for (pgg::MAI_MAX j = 0; j != DIM; ++j) {
               EXPECT_TRUE(matB(i,j) == static_cast<double>(i));
               EXPECT_TRUE(matC(i,j) == static_cast<double>(j));
          }
     }
}

//============//
// test # 133 //
//============//

TEST(MatrixTest133, Abs1)
{
     MatrixDense<double> matA;
     MatrixDense<double> matB;

     const pgg::MAI DIM = static_cast<pgg::MAI>(pow(10.0, 3.0));

     matA.Allocate(DIM);
     matB.Allocate(DIM);

     // # 1

     matA = -1.0;
     matB = +1.0;

     matA.Abs();

     EXPECT_TRUE(matA == matB);

     // # 2

     matA = -2.234567;
     matB = +2.234567;

     matA.Abs();

     EXPECT_TRUE(matA == matB);

     // # 3

     matA = -2;
     matB = +2;

     matA.Abs();

     EXPECT_TRUE(matA == matB);
}

//============//
// test # 134 //
//============//

TEST(MatrixTest134, Abs2)
{
     MatrixDense<int> matA;
     MatrixDense<int> matB;

     const pgg::MAI DIM = static_cast<pgg::MAI>(pow(10.0, 3.0));

     matA.Allocate(DIM);
     matB.Allocate(DIM);

     // # 1

     matA = -1;
     matB = +1;

     matA.Abs();

     EXPECT_TRUE(matA == matB);

     // # 2

     matA = -2;
     matB = +2;

     matA.Abs();

     EXPECT_TRUE(matA == matB);

     // # 3

     matA = -3;
     matB = +3;

     matA.Abs();

     EXPECT_TRUE(matA == matB);
}

//============//
// test # 135 //
//============//

TEST(MatrixTest135, LAPACK_ZHEEVD1)
{
     MatrixDense<complex<double>> matA;
     MatrixDense<complex<double>> eigenvectorsA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 5) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 5) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigensystemZHEEVD(eigenvectorsA, eigenvaluesA);

     // test

     const double arr_values[100] = {-323.38478082263765, -318.95649570052416, -259.5812044929057,
                                     -255.47902023149175, -210.8574832531544, -207.046095983574,
                                     -172.17668982103106, -169.63822144706373, -167.11362728657673,
                                     -164.65335127248554, -137.93078894722365, -135.63554168942136,
                                     -133.27655590262873, -130.90313825750582, -111.16491214977447,
                                     -108.69201485977163, -104.86333072392988, -102.09654131372476,
                                     -89.52930445237439, -87.11011352113846, -79.24098389717554,
                                     -76.62676551546362, -71.01471245774239, -68.78074078937429,
                                     -57.39730933358366, -55.597048251741185, -53.807888693814036,
                                     -52.082337027364105, -42.001162570408376, -40.10702874995469,
                                     -36.13941170041208, -34.10866137118017, -29.537625276581757,
                                     -27.807512581350473, -20.493485216120316, -19.180726411630474,
                                     -17.86587524596272, -16.594313970872253, -10.527129255160773,
                                     -9.235915164234662, -6.274431175607891, -4.736234460618425,
                                     -3.113902006580256, -2.3717281394985243, -0.07129875210819334,
                                     2.4178759119876623, 3.3310614360036803, 4.154955748257782,
                                     8.633411865685325, 10.366556352869488, 12.095377640289747,
                                     14.018873556316715, 17.48114915683801, 21.917608573061386,
                                     23.316848960689295, 27.074595655585338, 31.322004721507344,
                                     34.9238763246869, 36.70168716147582, 39.12924266179792,
                                     48.291435480841905, 50.073858853444456, 52.12562986320187,
                                     57.27293729356433, 64.70819943681298, 66.93714122637961,
                                     68.82639047739008, 80.9665198923797, 83.5742202822415,
                                     86.58950836310686, 89.44951473838972, 102.06705367044331,
                                     107.13994334023343, 109.44821746324266, 117.55286230732882,
                                     126.41194103599305, 133.5454916018076, 135.893724579225,
                                     151.52771532059967, 157.8972863072518, 165.22044899155756,
                                     167.77180305861472, 190.84742611813815, 205.77684982675112,
                                     208.5174101551291, 234.35823130936174, 279.92197126989515,
                                     330.06056030550724, 384.6942438609073, 444.0868835363212,
                                     508.62422446622827, 578.8097468653175, 655.2951900145777,
                                     738.9379634908137, 830.8998363959786, 932.8239020042909,
                                     1047.1805607335104, 1178.0384559097015, 1333.1755507750008,
                                     1532.5774637949444
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -10.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//============//
// test # 136 //
//============//

TEST(MatrixTest136, LAPACK_ZHEEVD2)
{
     MatrixDense<complex<double>> matA;
     MatrixDense<complex<double>> eigenvectorsA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 20) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 20) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigensystemZHEEVD(eigenvectorsA, eigenvaluesA);

     // test

     const double arr_values[100] = {-890.0679762687605,-833.96613152625,-387.9183497645402,
                                     -374.28858505936836,-325.97417429278767,-290.39317003462327,
                                     -243.09349980476634,-237.9671765884564,-184.65425879239723,
                                     -181.6433889406905,-152.42678092373745,-150.01440312367828,
                                     -140.20043836510496,-134.26239413119825,-130.50508513840435,
                                     -128.1365612192653,-119.6243787096752,-117.25234191992651,
                                     -112.47792700587912,-108.89915545206071,-104.74759545668331,
                                     -100.22968339474058,-94.96995783385485,-89.69665862307328,
                                     -86.4677796222291,-68.14673474241698,-66.07309844841615,
                                     -56.302362373674626,-54.63592515665142,-49.54128744936952,
                                     -47.77783428990662,-45.159157747842364,-43.112766536500985,
                                     -40.904881657341534,-38.624975888999664,-36.21821364136139,
                                     -33.61319247902676,-30.618705849873916,-26.992630355951256,
                                     -19.965626399359778,-9.761220034345175,-0.009675776079236078,
                                     -8.793180471674402e-7,-4.675655622579791e-14,-3.291874702847949e-14,
                                     -3.0408119421278934e-14,-2.2328449483329286e-14,
                                     -1.0693016765542536e-14,-7.240806196368183e-15,
                                     -5.761950647030487e-15,-5.957409950992936e-16,3.2491920424847875e-15,
                                     9.219910025371442e-15,1.2799168101873962e-14,1.7108898977066272e-14,
                                     2.855032832850196e-14,4.1048723593164446e-14,9.99499970701814,
                                     18.470571911228987,30.51830887197781,33.54443547978839,
                                     36.1607309907248,38.57320314001222,40.85924266944485,
                                     43.035391586290125,45.287513370945284,47.151147140980484,
                                     50.73276704593593,52.39952223577173,59.50068014643575,
                                     61.31379159039717,74.51253995296709,77.13729578952395,
                                     94.37203226002364,99.43109411760354,102.6487394625419,
                                     105.06218043709777,107.8179491492106,110.12126119755328,
                                     113.57945167126782,116.37801423876415,122.99832759829658,
                                     125.12209476580418,138.05692785144052,140.21814403606146,
                                     161.31660187081715,164.54676840029904,175.43647569692263,
                                     192.3282320950376,207.53405143733352,212.25537253729775,
                                     284.3628999051823,294.6884623476941,341.98290548531804,
                                     507.44043882416463,534.3382764375148,967.6392677402239,
                                     1875.507871717115,3171.7801027349824,5101.18005605357
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -10.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//============//
// test # 137 //
//============//

TEST(MatrixTest137, LAPACK_ZHEEVD3)
{
     MatrixDense<complex<double>> matA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 5) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 5) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvaluesZHEEVD(eigenvaluesA);

     // test

     const double arr_values[100] = {-323.38478082263765, -318.95649570052416, -259.5812044929057,
                                     -255.47902023149175, -210.8574832531544, -207.046095983574,
                                     -172.17668982103106, -169.63822144706373, -167.11362728657673,
                                     -164.65335127248554, -137.93078894722365, -135.63554168942136,
                                     -133.27655590262873, -130.90313825750582, -111.16491214977447,
                                     -108.69201485977163, -104.86333072392988, -102.09654131372476,
                                     -89.52930445237439, -87.11011352113846, -79.24098389717554,
                                     -76.62676551546362, -71.01471245774239, -68.78074078937429,
                                     -57.39730933358366, -55.597048251741185, -53.807888693814036,
                                     -52.082337027364105, -42.001162570408376, -40.10702874995469,
                                     -36.13941170041208, -34.10866137118017, -29.537625276581757,
                                     -27.807512581350473, -20.493485216120316, -19.180726411630474,
                                     -17.86587524596272, -16.594313970872253, -10.527129255160773,
                                     -9.235915164234662, -6.274431175607891, -4.736234460618425,
                                     -3.113902006580256, -2.3717281394985243, -0.07129875210819334,
                                     2.4178759119876623, 3.3310614360036803, 4.154955748257782,
                                     8.633411865685325, 10.366556352869488, 12.095377640289747,
                                     14.018873556316715, 17.48114915683801, 21.917608573061386,
                                     23.316848960689295, 27.074595655585338, 31.322004721507344,
                                     34.9238763246869, 36.70168716147582, 39.12924266179792,
                                     48.291435480841905, 50.073858853444456, 52.12562986320187,
                                     57.27293729356433, 64.70819943681298, 66.93714122637961,
                                     68.82639047739008, 80.9665198923797, 83.5742202822415,
                                     86.58950836310686, 89.44951473838972, 102.06705367044331,
                                     107.13994334023343, 109.44821746324266, 117.55286230732882,
                                     126.41194103599305, 133.5454916018076, 135.893724579225,
                                     151.52771532059967, 157.8972863072518, 165.22044899155756,
                                     167.77180305861472, 190.84742611813815, 205.77684982675112,
                                     208.5174101551291, 234.35823130936174, 279.92197126989515,
                                     330.06056030550724, 384.6942438609073, 444.0868835363212,
                                     508.62422446622827, 578.8097468653175, 655.2951900145777,
                                     738.9379634908137, 830.8998363959786, 932.8239020042909,
                                     1047.1805607335104, 1178.0384559097015, 1333.1755507750008,
                                     1532.5774637949444
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -5.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//============//
// test # 138 //
//============//

TEST(MatrixTest138, LAPACK_ZHEEVD4)
{
     MatrixDense<complex<double>> matA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 20) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 20) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvaluesZHEEVD(eigenvaluesA);

     // test

     const double arr_values[100] = {-890.0679762687605,-833.96613152625,-387.9183497645402,
                                     -374.28858505936836,-325.97417429278767,-290.39317003462327,
                                     -243.09349980476634,-237.9671765884564,-184.65425879239723,
                                     -181.6433889406905,-152.42678092373745,-150.01440312367828,
                                     -140.20043836510496,-134.26239413119825,-130.50508513840435,
                                     -128.1365612192653,-119.6243787096752,-117.25234191992651,
                                     -112.47792700587912,-108.89915545206071,-104.74759545668331,
                                     -100.22968339474058,-94.96995783385485,-89.69665862307328,
                                     -86.4677796222291,-68.14673474241698,-66.07309844841615,
                                     -56.302362373674626,-54.63592515665142,-49.54128744936952,
                                     -47.77783428990662,-45.159157747842364,-43.112766536500985,
                                     -40.904881657341534,-38.624975888999664,-36.21821364136139,
                                     -33.61319247902676,-30.618705849873916,-26.992630355951256,
                                     -19.965626399359778,-9.761220034345175,-0.009675776079236078,
                                     -8.793180471674402e-7,-4.675655622579791e-14,-3.291874702847949e-14,
                                     -3.0408119421278934e-14,-2.2328449483329286e-14,
                                     -1.0693016765542536e-14,-7.240806196368183e-15,
                                     -5.761950647030487e-15,-5.957409950992936e-16,3.2491920424847875e-15,
                                     9.219910025371442e-15,1.2799168101873962e-14,1.7108898977066272e-14,
                                     2.855032832850196e-14,4.1048723593164446e-14,9.99499970701814,
                                     18.470571911228987,30.51830887197781,33.54443547978839,
                                     36.1607309907248,38.57320314001222,40.85924266944485,
                                     43.035391586290125,45.287513370945284,47.151147140980484,
                                     50.73276704593593,52.39952223577173,59.50068014643575,
                                     61.31379159039717,74.51253995296709,77.13729578952395,
                                     94.37203226002364,99.43109411760354,102.6487394625419,
                                     105.06218043709777,107.8179491492106,110.12126119755328,
                                     113.57945167126782,116.37801423876415,122.99832759829658,
                                     125.12209476580418,138.05692785144052,140.21814403606146,
                                     161.31660187081715,164.54676840029904,175.43647569692263,
                                     192.3282320950376,207.53405143733352,212.25537253729775,
                                     284.3628999051823,294.6884623476941,341.98290548531804,
                                     507.44043882416463,534.3382764375148,967.6392677402239,
                                     1875.507871717115,3171.7801027349824,5101.18005605357
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -5.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//============//
// test # 139 //
//============//

TEST(MatrixTest139, LAPACK_ZHEEVD5)
{
     MatrixDense<complex<double>> matA;
     MatrixDense<complex<double>> eigenvectorsA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 5) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 5) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvectorsZHEEVD(eigenvectorsA);

     // test

     const double arr_values99[100] = {3.6942314255214093e-22,
                                       1.9280574390556084e-21,
                                       8.336207484683114e-21,
                                       3.201738809930229e-20,
                                       1.128792365433472e-19,
                                       3.7219958073502635e-19,
                                       1.1616570563876596e-18,
                                       3.4603856776191363e-18,
                                       9.898106164891213e-18,
                                       2.7313034552506115e-17,
                                       7.297235551812733e-17,
                                       1.8931798526470812e-16,
                                       4.781008453916819e-16,
                                       1.1776583982769647e-15,
                                       2.8342405979299284e-15,
                                       6.674396729414062e-15,
                                       1.5399303294661802e-14,
                                       3.4849026635393604e-14,
                                       7.743020050094637e-14,
                                       1.6906046576799098e-13,
                                       3.630170569572799e-13,
                                       7.671355827326599e-13,
                                       1.5964469231753074e-12,
                                       3.2735991022182145e-12,
                                       6.617806646334326e-12,
                                       1.3195608156949319e-11,
                                       2.596350061894761e-11,
                                       5.0430333328453936e-11,
                                       9.67336578559975e-11,
                                       1.8330342627363303e-10,
                                       3.4324925188064113e-10,
                                       6.353651498186134e-10,
                                       1.1628708815038037e-9,
                                       2.104965971919157e-9,
                                       3.7693601315726985e-9,
                                       6.6787563519161844e-9,
                                       1.1711672807554324e-8,
                                       2.03292597117336e-8,
                                       3.4936698501777865e-8,
                                       5.9452893015691615e-8,
                                       1.0019897315247485e-7,
                                       1.672693861786821e-7,
                                       2.7662540139461026e-7,
                                       4.5325802559019753e-7,
                                       7.359163073250683e-7,
                                       1.1841012055800893e-6,
                                       1.8883009979218705e-6,
                                       2.984811021145573e-6,
                                       4.676960526639804e-6,
                                       7.265186654807506e-6,
                                       0.000011189174362740128,
                                       0.00001708624363917233,
                                       0.000025871327250800013,
                                       0.00003884524735147213,
                                       0.000057839551835632114,
                                       0.0000854078666854305,
                                       0.00012507547298611522,
                                       0.0001816604867751283,
                                       0.0002616813987570242,
                                       0.00037386653150919884,
                                       0.0005297808051872514,
                                       0.0007445835839285321,
                                       0.0010379277320564163,
                                       0.0014350037062010874,
                                       0.001967722735545796,
                                       0.0026760192519511626,
                                       0.0036092342363378766,
                                       0.00482751783582054,
                                       0.00640315989292051,
                                       0.008421723355192411,
                                       0.010982819682174948,
                                       0.014200329986124536,
                                       0.018201824732492763,
                                       0.023126912983358,
                                       0.029124247016656085,
                                       0.03634692535629019,
                                       0.04494587964943125,
                                       0.055061139884829774,
                                       0.0668110106541383,
                                       0.08027936243852593,
                                       0.09549945152005936,
                                       0.11243731017783884,
                                       0.13097574227719463,
                                       0.15090013051721174,
                                       0.17186879268938443,
                                       0.19341147439162046,
                                       0.21493488140894385,
                                       0.23573601738701488,
                                       0.2548580930562397,
                                       0.27133687410309687,
                                       0.28424352285526244,
                                       0.29273026481733555,
                                       0.29466646943807095,
                                       0.28996990773813897,
                                       0.27878021480447934,
                                       0.26146670556930435,
                                       0.22712948196564564,
                                       0.19198046679049208,
                                       0.15683996268683814,
                                       0.1225409827758915
                                      };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,99)) - abs(arr_values99[i])) < pow(10.0, -10.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//============//
// test # 140 //
//============//

TEST(MatrixTest140, LAPACK_ZHEEVD6)
{
     MatrixDense<complex<double>> matA;
     MatrixDense<complex<double>> eigenvectorsA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 20) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 20) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvectorsZHEEVD(eigenvectorsA);

     // test data # 1

     const double arr_values99[100] = {-0.00002379437463800071,-0.0000321126535355192,
                                       -0.00004261209009056319,-0.00005574228105018768,
                                       -0.0000720258070448845,-0.00009206699311457821,
                                       -0.00011656127426176481,-0.00014630515846035676,
                                       -0.0001822067730093074,-0.00022529697290878234,
                                       -0.00027674098203948964,-0.0003378505293342626,
                                       -0.00041009643284298355,-0.0004951215746125915,
                                       -0.0005947541986465926,-0.0007110214528956958,-0.0008461630842956044,
                                       -0.001002645183353377,-0.0011831738617460526,-0.0013907087329034266,
                                       -0.0016283827619966063,-0.0018997449209395513,-0.002208573606467658,
                                       -0.002558944309118129,-0.0029552087127267114,-0.0034019962501085446,
                                       -0.003904213875493434,-0.00446704386484559,-0.00509593944935361,
                                       -0.005796618082542783,-0.006575052137809326,-0.007437456830885415,
                                       -0.00839027516100404,-0.009440159665541611,-0.010593950785877937,
                                       -0.01185865164733851,-0.013241399063586366,-0.014749430585927434,
                                       -0.0163900474309,-0.018170573135449803,-0.0200983089054282,
                                       -0.022180481856340313,-0.024424190854787288,-0.026834080780647522,
                                       -0.02941440937535356,-0.03216899598488147,-0.035101168178730414,
                                       -0.03821370638490854,-0.04150878671030463,-0.04498792214706611,
                                       -0.04865190239866414,-0.05250073259412269,-0.056533571195309565,
                                       -0.06074866744009344,-0.0651432987033876,-0.06971370819842077,
                                       -0.07445504348175375,-0.07936129626731185,-0.08442524409670342,
                                       -0.08963839445497533,-0.09499093194079421,-0.10047166921101545,
                                       -0.10595789789842808,-0.11143628534678302,-0.11689273862688677,
                                       -0.1223124096438104,-0.12767970465965733,-0.13297829857254914,
                                       -0.13819115429332646,-0.1433005475596976,-0.14828809752292466,
                                       -0.15313480343431787,-0.15782108774752324,-0.16232684593753477,
                                       -0.16663150331824728,-0.17071407911689127,-0.17455325803557586,
                                       -0.17812746949713273,-0.18141497473424503,-0.18439396183722762,
                                       -0.18704264882716626,-0.18541168339207653,-0.18356676120058393,
                                       -0.18150272926411598,-0.17921485423076686,-0.17669886945503793,
                                       -0.17395102341224067,-0.17096812928970723,-0.1677476155625255,
                                       -0.1642875773360043,-0.16058682821057293,-0.15664495239745393,
                                       -0.15246235678535205,-0.14804032262974395,-0.14338105650732136,
                                       -0.13848774014895246,-0.1333645787354236,-0.128016847211483,
                                       -0.1224509341456258,-0.11667438263597216
                                      };

     // test data # 2

     const double arr_values98[100] = {0.0005490815006598427,0.0007217004212979288,0.0009319727820738912,
                                       0.0011856764796110383,0.0014891369301256077,0.0018492353084341568,
                                       0.002273410759398609,0.002769655716932461,0.003346503443557062,
                                       0.00401300689217423,0.0047787079930774546,0.00565359648519078,
                                       0.006648057443072511,0.007772806702325777,0.009038813457661714,
                                       0.010457209401826704,0.012039183891666605,0.013795864771304657,
                                       0.01573818465305239,0.01787673265523282,0.020218129530799774,
                                       0.022773522732384158,0.02555088599547039,0.02855699048006569,
                                       0.03179740706616532,0.03527631554658473,0.038996305859039175,
                                       0.04295817276333242,0.047160705650423344,0.05160047546600458,
                                       0.056271621039461366,0.061165637426456754,0.06627116919601259,
                                       0.07157381191620707,0.0770559254111312,0.08269646266941802,
                                       0.08847081857459133,0.09435070289203276,0.10030404217816305,
                                       0.10629491546542182,0.11228359420772163,0.11822646848984367,
                                       0.12407615562938958,0.12979275139874066,0.13533385216700752,
                                       0.14065467360130016,0.14570820649639804,0.1504454123825699,
                                       0.15481546139101132,0.15876601462676446,0.16224355300490953,
                                       0.1651937541424425,0.16756191846119972,0.16929344514268863,
                                       0.17033435798074814,0.17063188050057534,0.17013505895196374,
                                       0.16879543094112387,0.16656773654130375,0.16341066772151153,
                                       0.1592876487959431,0.15416764632985688,0.14836208495033895,
                                       0.14186022973422263,0.1346552528267162,0.12674464039743252,
                                       0.118130599994188,0.10882046380600134,0.09882708283296263,
                                       0.08816920644738636,0.07687184132594177,0.06496658324539006,
                                       0.052491914775380344,0.039493461481783246,0.026024198885649517,
                                       0.012144602119448178,-0.002077270001960313,-0.0165657649080691,
                                       -0.0312377086295896,-0.04600256891175273,-0.06076271707085829,
                                       -0.06795927022946352,-0.07503806683258157,-0.0819579039270416,
                                       -0.08867572326732134,-0.09514679596043041,-0.1013249415936529,
                                       -0.10716278331612594,-0.1126120400798519,-0.11762385692798413,
                                       -0.12214917384544385,-0.12613913325700105,-0.12954552576940606,
                                       -0.1323212732062947,-0.13442094737761986,-0.13580132236057105,
                                       -0.13642195734879647,-0.13624580635507777,-0.1352398502347542,
                                       -0.13337574564008783
                                      };

     // test data # 3

     const double arr_values97[100] = {-0.0048844047935863765,-0.006236486880389232,-0.007815274325331554,
                                       -0.009640183890712983,-0.011730050533458039,-0.014102771342375649,
                                       -0.016774922205544347,-0.019761352037216788,-0.023074760743478643,
                                       -0.026725268499719908,-0.03071998531275152,-0.03506259120033407,
                                       -0.03975293858755401,-0.04478668963230909,-0.050155002084304526,
                                       -0.05584427788164485,-0.06183598892150303,-0.06810659423095557,
                                       -0.07462756203769172,-0.08136550893055443,-0.08823038013976417,
                                       -0.09520849071940404,-0.1022476308045662,-0.10929156383600795,
                                       -0.11627847844623534,-0.12314131678294159,-0.1298082479646105,
                                       -0.13620329680129123,-0.14224713548726084,-0.14785804289917964,
                                       -0.15295303238959757,-0.1574491445459654,-0.1612648963069822,
                                       -0.16432187213530286,-0.16654643671328404,-0.16787154196597193,
                                       -0.16823859426980445,-0.16759934066305365,-0.16591772596198778,
                                       -0.1631716661722737,-0.15935634408053131,-0.15448321146294608,
                                       -0.14858218139776455,-0.1416381780493364,-0.1336490652093021,
                                       -0.12462715724623677,-0.11460064260169803,-0.10361487512764765,
                                       -0.09173348472751607,-0.07903925562502323,-0.06563471836746396,
                                       -0.051642400613860905,-0.03720468211196078,-0.022483201281379775,
                                       -0.007657764735299888,0.007075282886553215,0.021505263042677962,
                                       0.03540996481701064,0.04855905391412624,0.06071808324036558,
                                       0.07165316959606502,0.08113608035225883,0.09009849834439126,
                                       0.09838663571146652,0.10584187751947657,0.11230295059241002,
                                       0.11760842867321217,0.12159956720711124,0.12412345180969263,
                                       0.1250364341022301,0.12420781716730643,0.12152374055295379,
                                       0.11689120174753254,0.11024213763900717,0.10153747600764697,
                                       0.09077105399613189,0.07797328823931185,0.06321447046527728,
                                       0.04660755350441126,0.028310286413503014,0.008526547885299433,
                                       0.0025719295116293645,-0.004200598749307477,-0.011762440439929252,
                                       -0.020069965854842452,-0.029063538293552106,-0.038666727752430484,
                                       -0.048785756486008615,-0.05930922217827011,-0.07010814475633873,
                                       -0.08103638195706307,-0.09193145642839495,-0.10261583322770457,
                                       -0.11289868090088843,-0.12257814174844638,-0.1314441273038225,
                                       -0.139281643409791,-0.14587463558538188,-0.15101032970590494,
                                       -0.15448402553323257
                                      };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,99)) - abs(arr_values99[i])) < pow(10.0, -10.0));
     }

     // test # 2

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,98)) - abs(arr_values98[i])) < pow(10.0, -10.0));
     }

     // test # 3

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,97)) - abs(arr_values97[i])) < pow(10.0, -10.0));
     }

     // test # 4

     EXPECT_TRUE(res == 0);
}

//============//
// test # 141 //
//============//

TEST(MatrixTest141, LAPACK_ZHEEVR1)
{
     MatrixDense<complex<double>> matA;
     MatrixDense<complex<double>> eigenvectorsA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 5) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 5) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigensystemZHEEVR(eigenvectorsA, eigenvaluesA);

     // test

     const double arr_values[100] = {-323.38478082263765, -318.95649570052416, -259.5812044929057,
                                     -255.47902023149175, -210.8574832531544, -207.046095983574,
                                     -172.17668982103106, -169.63822144706373, -167.11362728657673,
                                     -164.65335127248554, -137.93078894722365, -135.63554168942136,
                                     -133.27655590262873, -130.90313825750582, -111.16491214977447,
                                     -108.69201485977163, -104.86333072392988, -102.09654131372476,
                                     -89.52930445237439, -87.11011352113846, -79.24098389717554,
                                     -76.62676551546362, -71.01471245774239, -68.78074078937429,
                                     -57.39730933358366, -55.597048251741185, -53.807888693814036,
                                     -52.082337027364105, -42.001162570408376, -40.10702874995469,
                                     -36.13941170041208, -34.10866137118017, -29.537625276581757,
                                     -27.807512581350473, -20.493485216120316, -19.180726411630474,
                                     -17.86587524596272, -16.594313970872253, -10.527129255160773,
                                     -9.235915164234662, -6.274431175607891, -4.736234460618425,
                                     -3.113902006580256, -2.3717281394985243, -0.07129875210819334,
                                     2.4178759119876623, 3.3310614360036803, 4.154955748257782,
                                     8.633411865685325, 10.366556352869488, 12.095377640289747,
                                     14.018873556316715, 17.48114915683801, 21.917608573061386,
                                     23.316848960689295, 27.074595655585338, 31.322004721507344,
                                     34.9238763246869, 36.70168716147582, 39.12924266179792,
                                     48.291435480841905, 50.073858853444456, 52.12562986320187,
                                     57.27293729356433, 64.70819943681298, 66.93714122637961,
                                     68.82639047739008, 80.9665198923797, 83.5742202822415,
                                     86.58950836310686, 89.44951473838972, 102.06705367044331,
                                     107.13994334023343, 109.44821746324266, 117.55286230732882,
                                     126.41194103599305, 133.5454916018076, 135.893724579225,
                                     151.52771532059967, 157.8972863072518, 165.22044899155756,
                                     167.77180305861472, 190.84742611813815, 205.77684982675112,
                                     208.5174101551291, 234.35823130936174, 279.92197126989515,
                                     330.06056030550724, 384.6942438609073, 444.0868835363212,
                                     508.62422446622827, 578.8097468653175, 655.2951900145777,
                                     738.9379634908137, 830.8998363959786, 932.8239020042909,
                                     1047.1805607335104, 1178.0384559097015, 1333.1755507750008,
                                     1532.5774637949444
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -10.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//============//
// test # 142 //
//============//

TEST(MatrixTest142, LAPACK_ZHEEVR2)
{
     MatrixDense<complex<double>> matA;
     MatrixDense<complex<double>> eigenvectorsA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 20) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 20) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigensystemZHEEVR(eigenvectorsA, eigenvaluesA);

     // test

     const double arr_values[100] = {-890.0679762687605,-833.96613152625,-387.9183497645402,
                                     -374.28858505936836,-325.97417429278767,-290.39317003462327,
                                     -243.09349980476634,-237.9671765884564,-184.65425879239723,
                                     -181.6433889406905,-152.42678092373745,-150.01440312367828,
                                     -140.20043836510496,-134.26239413119825,-130.50508513840435,
                                     -128.1365612192653,-119.6243787096752,-117.25234191992651,
                                     -112.47792700587912,-108.89915545206071,-104.74759545668331,
                                     -100.22968339474058,-94.96995783385485,-89.69665862307328,
                                     -86.4677796222291,-68.14673474241698,-66.07309844841615,
                                     -56.302362373674626,-54.63592515665142,-49.54128744936952,
                                     -47.77783428990662,-45.159157747842364,-43.112766536500985,
                                     -40.904881657341534,-38.624975888999664,-36.21821364136139,
                                     -33.61319247902676,-30.618705849873916,-26.992630355951256,
                                     -19.965626399359778,-9.761220034345175,-0.009675776079236078,
                                     -8.793180471674402e-7,-4.675655622579791e-14,-3.291874702847949e-14,
                                     -3.0408119421278934e-14,-2.2328449483329286e-14,
                                     -1.0693016765542536e-14,-7.240806196368183e-15,
                                     -5.761950647030487e-15,-5.957409950992936e-16,3.2491920424847875e-15,
                                     9.219910025371442e-15,1.2799168101873962e-14,1.7108898977066272e-14,
                                     2.855032832850196e-14,4.1048723593164446e-14,9.99499970701814,
                                     18.470571911228987,30.51830887197781,33.54443547978839,
                                     36.1607309907248,38.57320314001222,40.85924266944485,
                                     43.035391586290125,45.287513370945284,47.151147140980484,
                                     50.73276704593593,52.39952223577173,59.50068014643575,
                                     61.31379159039717,74.51253995296709,77.13729578952395,
                                     94.37203226002364,99.43109411760354,102.6487394625419,
                                     105.06218043709777,107.8179491492106,110.12126119755328,
                                     113.57945167126782,116.37801423876415,122.99832759829658,
                                     125.12209476580418,138.05692785144052,140.21814403606146,
                                     161.31660187081715,164.54676840029904,175.43647569692263,
                                     192.3282320950376,207.53405143733352,212.25537253729775,
                                     284.3628999051823,294.6884623476941,341.98290548531804,
                                     507.44043882416463,534.3382764375148,967.6392677402239,
                                     1875.507871717115,3171.7801027349824,5101.18005605357
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -10.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//============//
// test # 143 //
//============//

TEST(MatrixTest143, LAPACK_ZHEEVR3)
{
     MatrixDense<complex<double>> matA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 5) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 5) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvaluesZHEEVR(eigenvaluesA);

     // test

     const double arr_values[100] = {-323.38478082263765, -318.95649570052416, -259.5812044929057,
                                     -255.47902023149175, -210.8574832531544, -207.046095983574,
                                     -172.17668982103106, -169.63822144706373, -167.11362728657673,
                                     -164.65335127248554, -137.93078894722365, -135.63554168942136,
                                     -133.27655590262873, -130.90313825750582, -111.16491214977447,
                                     -108.69201485977163, -104.86333072392988, -102.09654131372476,
                                     -89.52930445237439, -87.11011352113846, -79.24098389717554,
                                     -76.62676551546362, -71.01471245774239, -68.78074078937429,
                                     -57.39730933358366, -55.597048251741185, -53.807888693814036,
                                     -52.082337027364105, -42.001162570408376, -40.10702874995469,
                                     -36.13941170041208, -34.10866137118017, -29.537625276581757,
                                     -27.807512581350473, -20.493485216120316, -19.180726411630474,
                                     -17.86587524596272, -16.594313970872253, -10.527129255160773,
                                     -9.235915164234662, -6.274431175607891, -4.736234460618425,
                                     -3.113902006580256, -2.3717281394985243, -0.07129875210819334,
                                     2.4178759119876623, 3.3310614360036803, 4.154955748257782,
                                     8.633411865685325, 10.366556352869488, 12.095377640289747,
                                     14.018873556316715, 17.48114915683801, 21.917608573061386,
                                     23.316848960689295, 27.074595655585338, 31.322004721507344,
                                     34.9238763246869, 36.70168716147582, 39.12924266179792,
                                     48.291435480841905, 50.073858853444456, 52.12562986320187,
                                     57.27293729356433, 64.70819943681298, 66.93714122637961,
                                     68.82639047739008, 80.9665198923797, 83.5742202822415,
                                     86.58950836310686, 89.44951473838972, 102.06705367044331,
                                     107.13994334023343, 109.44821746324266, 117.55286230732882,
                                     126.41194103599305, 133.5454916018076, 135.893724579225,
                                     151.52771532059967, 157.8972863072518, 165.22044899155756,
                                     167.77180305861472, 190.84742611813815, 205.77684982675112,
                                     208.5174101551291, 234.35823130936174, 279.92197126989515,
                                     330.06056030550724, 384.6942438609073, 444.0868835363212,
                                     508.62422446622827, 578.8097468653175, 655.2951900145777,
                                     738.9379634908137, 830.8998363959786, 932.8239020042909,
                                     1047.1805607335104, 1178.0384559097015, 1333.1755507750008,
                                     1532.5774637949444
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -5.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//============//
// test # 144 //
//============//

TEST(MatrixTest144, LAPACK_ZHEEVR4)
{
     MatrixDense<complex<double>> matA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 20) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 20) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvaluesZHEEVR(eigenvaluesA);

     // test

     const double arr_values[100] = {-890.0679762687605,-833.96613152625,-387.9183497645402,
                                     -374.28858505936836,-325.97417429278767,-290.39317003462327,
                                     -243.09349980476634,-237.9671765884564,-184.65425879239723,
                                     -181.6433889406905,-152.42678092373745,-150.01440312367828,
                                     -140.20043836510496,-134.26239413119825,-130.50508513840435,
                                     -128.1365612192653,-119.6243787096752,-117.25234191992651,
                                     -112.47792700587912,-108.89915545206071,-104.74759545668331,
                                     -100.22968339474058,-94.96995783385485,-89.69665862307328,
                                     -86.4677796222291,-68.14673474241698,-66.07309844841615,
                                     -56.302362373674626,-54.63592515665142,-49.54128744936952,
                                     -47.77783428990662,-45.159157747842364,-43.112766536500985,
                                     -40.904881657341534,-38.624975888999664,-36.21821364136139,
                                     -33.61319247902676,-30.618705849873916,-26.992630355951256,
                                     -19.965626399359778,-9.761220034345175,-0.009675776079236078,
                                     -8.793180471674402e-7,-4.675655622579791e-14,-3.291874702847949e-14,
                                     -3.0408119421278934e-14,-2.2328449483329286e-14,
                                     -1.0693016765542536e-14,-7.240806196368183e-15,
                                     -5.761950647030487e-15,-5.957409950992936e-16,3.2491920424847875e-15,
                                     9.219910025371442e-15,1.2799168101873962e-14,1.7108898977066272e-14,
                                     2.855032832850196e-14,4.1048723593164446e-14,9.99499970701814,
                                     18.470571911228987,30.51830887197781,33.54443547978839,
                                     36.1607309907248,38.57320314001222,40.85924266944485,
                                     43.035391586290125,45.287513370945284,47.151147140980484,
                                     50.73276704593593,52.39952223577173,59.50068014643575,
                                     61.31379159039717,74.51253995296709,77.13729578952395,
                                     94.37203226002364,99.43109411760354,102.6487394625419,
                                     105.06218043709777,107.8179491492106,110.12126119755328,
                                     113.57945167126782,116.37801423876415,122.99832759829658,
                                     125.12209476580418,138.05692785144052,140.21814403606146,
                                     161.31660187081715,164.54676840029904,175.43647569692263,
                                     192.3282320950376,207.53405143733352,212.25537253729775,
                                     284.3628999051823,294.6884623476941,341.98290548531804,
                                     507.44043882416463,534.3382764375148,967.6392677402239,
                                     1875.507871717115,3171.7801027349824,5101.18005605357
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -5.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//============//
// test # 145 //
//============//

TEST(MatrixTest145, LAPACK_ZHEEVR5)
{
     MatrixDense<complex<double>> matA;
     MatrixDense<complex<double>> eigenvectorsA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 5) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 5) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvectorsZHEEVR(eigenvectorsA);

     // test

     const double arr_values99[100] = {3.6942314255214093e-22,
                                       1.9280574390556084e-21,
                                       8.336207484683114e-21,
                                       3.201738809930229e-20,
                                       1.128792365433472e-19,
                                       3.7219958073502635e-19,
                                       1.1616570563876596e-18,
                                       3.4603856776191363e-18,
                                       9.898106164891213e-18,
                                       2.7313034552506115e-17,
                                       7.297235551812733e-17,
                                       1.8931798526470812e-16,
                                       4.781008453916819e-16,
                                       1.1776583982769647e-15,
                                       2.8342405979299284e-15,
                                       6.674396729414062e-15,
                                       1.5399303294661802e-14,
                                       3.4849026635393604e-14,
                                       7.743020050094637e-14,
                                       1.6906046576799098e-13,
                                       3.630170569572799e-13,
                                       7.671355827326599e-13,
                                       1.5964469231753074e-12,
                                       3.2735991022182145e-12,
                                       6.617806646334326e-12,
                                       1.3195608156949319e-11,
                                       2.596350061894761e-11,
                                       5.0430333328453936e-11,
                                       9.67336578559975e-11,
                                       1.8330342627363303e-10,
                                       3.4324925188064113e-10,
                                       6.353651498186134e-10,
                                       1.1628708815038037e-9,
                                       2.104965971919157e-9,
                                       3.7693601315726985e-9,
                                       6.6787563519161844e-9,
                                       1.1711672807554324e-8,
                                       2.03292597117336e-8,
                                       3.4936698501777865e-8,
                                       5.9452893015691615e-8,
                                       1.0019897315247485e-7,
                                       1.672693861786821e-7,
                                       2.7662540139461026e-7,
                                       4.5325802559019753e-7,
                                       7.359163073250683e-7,
                                       1.1841012055800893e-6,
                                       1.8883009979218705e-6,
                                       2.984811021145573e-6,
                                       4.676960526639804e-6,
                                       7.265186654807506e-6,
                                       0.000011189174362740128,
                                       0.00001708624363917233,
                                       0.000025871327250800013,
                                       0.00003884524735147213,
                                       0.000057839551835632114,
                                       0.0000854078666854305,
                                       0.00012507547298611522,
                                       0.0001816604867751283,
                                       0.0002616813987570242,
                                       0.00037386653150919884,
                                       0.0005297808051872514,
                                       0.0007445835839285321,
                                       0.0010379277320564163,
                                       0.0014350037062010874,
                                       0.001967722735545796,
                                       0.0026760192519511626,
                                       0.0036092342363378766,
                                       0.00482751783582054,
                                       0.00640315989292051,
                                       0.008421723355192411,
                                       0.010982819682174948,
                                       0.014200329986124536,
                                       0.018201824732492763,
                                       0.023126912983358,
                                       0.029124247016656085,
                                       0.03634692535629019,
                                       0.04494587964943125,
                                       0.055061139884829774,
                                       0.0668110106541383,
                                       0.08027936243852593,
                                       0.09549945152005936,
                                       0.11243731017783884,
                                       0.13097574227719463,
                                       0.15090013051721174,
                                       0.17186879268938443,
                                       0.19341147439162046,
                                       0.21493488140894385,
                                       0.23573601738701488,
                                       0.2548580930562397,
                                       0.27133687410309687,
                                       0.28424352285526244,
                                       0.29273026481733555,
                                       0.29466646943807095,
                                       0.28996990773813897,
                                       0.27878021480447934,
                                       0.26146670556930435,
                                       0.22712948196564564,
                                       0.19198046679049208,
                                       0.15683996268683814,
                                       0.1225409827758915
                                      };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,99)) - abs(arr_values99[i])) < pow(10.0, -10.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//============//
// test # 146 //
//============//

TEST(MatrixTest146, LAPACK_ZHEEVR6)
{
     MatrixDense<complex<double>> matA;
     MatrixDense<complex<double>> eigenvectorsA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 20) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 20) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvectorsZHEEVR(eigenvectorsA);

     // test data # 1

     const double arr_values99[100] = {-0.00002379437463800071,-0.0000321126535355192,
                                       -0.00004261209009056319,-0.00005574228105018768,
                                       -0.0000720258070448845,-0.00009206699311457821,
                                       -0.00011656127426176481,-0.00014630515846035676,
                                       -0.0001822067730093074,-0.00022529697290878234,
                                       -0.00027674098203948964,-0.0003378505293342626,
                                       -0.00041009643284298355,-0.0004951215746125915,
                                       -0.0005947541986465926,-0.0007110214528956958,-0.0008461630842956044,
                                       -0.001002645183353377,-0.0011831738617460526,-0.0013907087329034266,
                                       -0.0016283827619966063,-0.0018997449209395513,-0.002208573606467658,
                                       -0.002558944309118129,-0.0029552087127267114,-0.0034019962501085446,
                                       -0.003904213875493434,-0.00446704386484559,-0.00509593944935361,
                                       -0.005796618082542783,-0.006575052137809326,-0.007437456830885415,
                                       -0.00839027516100404,-0.009440159665541611,-0.010593950785877937,
                                       -0.01185865164733851,-0.013241399063586366,-0.014749430585927434,
                                       -0.0163900474309,-0.018170573135449803,-0.0200983089054282,
                                       -0.022180481856340313,-0.024424190854787288,-0.026834080780647522,
                                       -0.02941440937535356,-0.03216899598488147,-0.035101168178730414,
                                       -0.03821370638490854,-0.04150878671030463,-0.04498792214706611,
                                       -0.04865190239866414,-0.05250073259412269,-0.056533571195309565,
                                       -0.06074866744009344,-0.0651432987033876,-0.06971370819842077,
                                       -0.07445504348175375,-0.07936129626731185,-0.08442524409670342,
                                       -0.08963839445497533,-0.09499093194079421,-0.10047166921101545,
                                       -0.10595789789842808,-0.11143628534678302,-0.11689273862688677,
                                       -0.1223124096438104,-0.12767970465965733,-0.13297829857254914,
                                       -0.13819115429332646,-0.1433005475596976,-0.14828809752292466,
                                       -0.15313480343431787,-0.15782108774752324,-0.16232684593753477,
                                       -0.16663150331824728,-0.17071407911689127,-0.17455325803557586,
                                       -0.17812746949713273,-0.18141497473424503,-0.18439396183722762,
                                       -0.18704264882716626,-0.18541168339207653,-0.18356676120058393,
                                       -0.18150272926411598,-0.17921485423076686,-0.17669886945503793,
                                       -0.17395102341224067,-0.17096812928970723,-0.1677476155625255,
                                       -0.1642875773360043,-0.16058682821057293,-0.15664495239745393,
                                       -0.15246235678535205,-0.14804032262974395,-0.14338105650732136,
                                       -0.13848774014895246,-0.1333645787354236,-0.128016847211483,
                                       -0.1224509341456258,-0.11667438263597216
                                      };

     // test data # 2

     const double arr_values98[100] = {0.0005490815006598427,0.0007217004212979288,0.0009319727820738912,
                                       0.0011856764796110383,0.0014891369301256077,0.0018492353084341568,
                                       0.002273410759398609,0.002769655716932461,0.003346503443557062,
                                       0.00401300689217423,0.0047787079930774546,0.00565359648519078,
                                       0.006648057443072511,0.007772806702325777,0.009038813457661714,
                                       0.010457209401826704,0.012039183891666605,0.013795864771304657,
                                       0.01573818465305239,0.01787673265523282,0.020218129530799774,
                                       0.022773522732384158,0.02555088599547039,0.02855699048006569,
                                       0.03179740706616532,0.03527631554658473,0.038996305859039175,
                                       0.04295817276333242,0.047160705650423344,0.05160047546600458,
                                       0.056271621039461366,0.061165637426456754,0.06627116919601259,
                                       0.07157381191620707,0.0770559254111312,0.08269646266941802,
                                       0.08847081857459133,0.09435070289203276,0.10030404217816305,
                                       0.10629491546542182,0.11228359420772163,0.11822646848984367,
                                       0.12407615562938958,0.12979275139874066,0.13533385216700752,
                                       0.14065467360130016,0.14570820649639804,0.1504454123825699,
                                       0.15481546139101132,0.15876601462676446,0.16224355300490953,
                                       0.1651937541424425,0.16756191846119972,0.16929344514268863,
                                       0.17033435798074814,0.17063188050057534,0.17013505895196374,
                                       0.16879543094112387,0.16656773654130375,0.16341066772151153,
                                       0.1592876487959431,0.15416764632985688,0.14836208495033895,
                                       0.14186022973422263,0.1346552528267162,0.12674464039743252,
                                       0.118130599994188,0.10882046380600134,0.09882708283296263,
                                       0.08816920644738636,0.07687184132594177,0.06496658324539006,
                                       0.052491914775380344,0.039493461481783246,0.026024198885649517,
                                       0.012144602119448178,-0.002077270001960313,-0.0165657649080691,
                                       -0.0312377086295896,-0.04600256891175273,-0.06076271707085829,
                                       -0.06795927022946352,-0.07503806683258157,-0.0819579039270416,
                                       -0.08867572326732134,-0.09514679596043041,-0.1013249415936529,
                                       -0.10716278331612594,-0.1126120400798519,-0.11762385692798413,
                                       -0.12214917384544385,-0.12613913325700105,-0.12954552576940606,
                                       -0.1323212732062947,-0.13442094737761986,-0.13580132236057105,
                                       -0.13642195734879647,-0.13624580635507777,-0.1352398502347542,
                                       -0.13337574564008783
                                      };

     // test data # 3

     const double arr_values97[100] = {-0.0048844047935863765,-0.006236486880389232,-0.007815274325331554,
                                       -0.009640183890712983,-0.011730050533458039,-0.014102771342375649,
                                       -0.016774922205544347,-0.019761352037216788,-0.023074760743478643,
                                       -0.026725268499719908,-0.03071998531275152,-0.03506259120033407,
                                       -0.03975293858755401,-0.04478668963230909,-0.050155002084304526,
                                       -0.05584427788164485,-0.06183598892150303,-0.06810659423095557,
                                       -0.07462756203769172,-0.08136550893055443,-0.08823038013976417,
                                       -0.09520849071940404,-0.1022476308045662,-0.10929156383600795,
                                       -0.11627847844623534,-0.12314131678294159,-0.1298082479646105,
                                       -0.13620329680129123,-0.14224713548726084,-0.14785804289917964,
                                       -0.15295303238959757,-0.1574491445459654,-0.1612648963069822,
                                       -0.16432187213530286,-0.16654643671328404,-0.16787154196597193,
                                       -0.16823859426980445,-0.16759934066305365,-0.16591772596198778,
                                       -0.1631716661722737,-0.15935634408053131,-0.15448321146294608,
                                       -0.14858218139776455,-0.1416381780493364,-0.1336490652093021,
                                       -0.12462715724623677,-0.11460064260169803,-0.10361487512764765,
                                       -0.09173348472751607,-0.07903925562502323,-0.06563471836746396,
                                       -0.051642400613860905,-0.03720468211196078,-0.022483201281379775,
                                       -0.007657764735299888,0.007075282886553215,0.021505263042677962,
                                       0.03540996481701064,0.04855905391412624,0.06071808324036558,
                                       0.07165316959606502,0.08113608035225883,0.09009849834439126,
                                       0.09838663571146652,0.10584187751947657,0.11230295059241002,
                                       0.11760842867321217,0.12159956720711124,0.12412345180969263,
                                       0.1250364341022301,0.12420781716730643,0.12152374055295379,
                                       0.11689120174753254,0.11024213763900717,0.10153747600764697,
                                       0.09077105399613189,0.07797328823931185,0.06321447046527728,
                                       0.04660755350441126,0.028310286413503014,0.008526547885299433,
                                       0.0025719295116293645,-0.004200598749307477,-0.011762440439929252,
                                       -0.020069965854842452,-0.029063538293552106,-0.038666727752430484,
                                       -0.048785756486008615,-0.05930922217827011,-0.07010814475633873,
                                       -0.08103638195706307,-0.09193145642839495,-0.10261583322770457,
                                       -0.11289868090088843,-0.12257814174844638,-0.1314441273038225,
                                       -0.139281643409791,-0.14587463558538188,-0.15101032970590494,
                                       -0.15448402553323257
                                      };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,99)) - abs(arr_values99[i])) < pow(10.0, -10.0));
     }

     // test # 2

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,98)) - abs(arr_values98[i])) < pow(10.0, -10.0));
     }

     // test # 3

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,97)) - abs(arr_values97[i])) < pow(10.0, -10.0));
     }

     // test # 4

     EXPECT_TRUE(res == 0);
}

//============//
// test # 147 //
//============//

TEST(MatrixTest147, LAPACK_ZHEEVX1)
{
     MatrixDense<complex<double>> matA;
     MatrixDense<complex<double>> eigenvectorsA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 5) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 5) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigensystemZHEEVX(eigenvectorsA, eigenvaluesA);

     // test

     const double arr_values[100] = {-323.38478082263765, -318.95649570052416, -259.5812044929057,
                                     -255.47902023149175, -210.8574832531544, -207.046095983574,
                                     -172.17668982103106, -169.63822144706373, -167.11362728657673,
                                     -164.65335127248554, -137.93078894722365, -135.63554168942136,
                                     -133.27655590262873, -130.90313825750582, -111.16491214977447,
                                     -108.69201485977163, -104.86333072392988, -102.09654131372476,
                                     -89.52930445237439, -87.11011352113846, -79.24098389717554,
                                     -76.62676551546362, -71.01471245774239, -68.78074078937429,
                                     -57.39730933358366, -55.597048251741185, -53.807888693814036,
                                     -52.082337027364105, -42.001162570408376, -40.10702874995469,
                                     -36.13941170041208, -34.10866137118017, -29.537625276581757,
                                     -27.807512581350473, -20.493485216120316, -19.180726411630474,
                                     -17.86587524596272, -16.594313970872253, -10.527129255160773,
                                     -9.235915164234662, -6.274431175607891, -4.736234460618425,
                                     -3.113902006580256, -2.3717281394985243, -0.07129875210819334,
                                     2.4178759119876623, 3.3310614360036803, 4.154955748257782,
                                     8.633411865685325, 10.366556352869488, 12.095377640289747,
                                     14.018873556316715, 17.48114915683801, 21.917608573061386,
                                     23.316848960689295, 27.074595655585338, 31.322004721507344,
                                     34.9238763246869, 36.70168716147582, 39.12924266179792,
                                     48.291435480841905, 50.073858853444456, 52.12562986320187,
                                     57.27293729356433, 64.70819943681298, 66.93714122637961,
                                     68.82639047739008, 80.9665198923797, 83.5742202822415,
                                     86.58950836310686, 89.44951473838972, 102.06705367044331,
                                     107.13994334023343, 109.44821746324266, 117.55286230732882,
                                     126.41194103599305, 133.5454916018076, 135.893724579225,
                                     151.52771532059967, 157.8972863072518, 165.22044899155756,
                                     167.77180305861472, 190.84742611813815, 205.77684982675112,
                                     208.5174101551291, 234.35823130936174, 279.92197126989515,
                                     330.06056030550724, 384.6942438609073, 444.0868835363212,
                                     508.62422446622827, 578.8097468653175, 655.2951900145777,
                                     738.9379634908137, 830.8998363959786, 932.8239020042909,
                                     1047.1805607335104, 1178.0384559097015, 1333.1755507750008,
                                     1532.5774637949444
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -10.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//============//
// test # 148 //
//============//

TEST(MatrixTest148, LAPACK_ZHEEVX2)
{
     MatrixDense<complex<double>> matA;
     MatrixDense<complex<double>> eigenvectorsA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 20) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 20) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigensystemZHEEVX(eigenvectorsA, eigenvaluesA);

     // test

     const double arr_values[100] = {-890.0679762687605,-833.96613152625,-387.9183497645402,
                                     -374.28858505936836,-325.97417429278767,-290.39317003462327,
                                     -243.09349980476634,-237.9671765884564,-184.65425879239723,
                                     -181.6433889406905,-152.42678092373745,-150.01440312367828,
                                     -140.20043836510496,-134.26239413119825,-130.50508513840435,
                                     -128.1365612192653,-119.6243787096752,-117.25234191992651,
                                     -112.47792700587912,-108.89915545206071,-104.74759545668331,
                                     -100.22968339474058,-94.96995783385485,-89.69665862307328,
                                     -86.4677796222291,-68.14673474241698,-66.07309844841615,
                                     -56.302362373674626,-54.63592515665142,-49.54128744936952,
                                     -47.77783428990662,-45.159157747842364,-43.112766536500985,
                                     -40.904881657341534,-38.624975888999664,-36.21821364136139,
                                     -33.61319247902676,-30.618705849873916,-26.992630355951256,
                                     -19.965626399359778,-9.761220034345175,-0.009675776079236078,
                                     -8.793180471674402e-7,-4.675655622579791e-14,-3.291874702847949e-14,
                                     -3.0408119421278934e-14,-2.2328449483329286e-14,
                                     -1.0693016765542536e-14,-7.240806196368183e-15,
                                     -5.761950647030487e-15,-5.957409950992936e-16,3.2491920424847875e-15,
                                     9.219910025371442e-15,1.2799168101873962e-14,1.7108898977066272e-14,
                                     2.855032832850196e-14,4.1048723593164446e-14,9.99499970701814,
                                     18.470571911228987,30.51830887197781,33.54443547978839,
                                     36.1607309907248,38.57320314001222,40.85924266944485,
                                     43.035391586290125,45.287513370945284,47.151147140980484,
                                     50.73276704593593,52.39952223577173,59.50068014643575,
                                     61.31379159039717,74.51253995296709,77.13729578952395,
                                     94.37203226002364,99.43109411760354,102.6487394625419,
                                     105.06218043709777,107.8179491492106,110.12126119755328,
                                     113.57945167126782,116.37801423876415,122.99832759829658,
                                     125.12209476580418,138.05692785144052,140.21814403606146,
                                     161.31660187081715,164.54676840029904,175.43647569692263,
                                     192.3282320950376,207.53405143733352,212.25537253729775,
                                     284.3628999051823,294.6884623476941,341.98290548531804,
                                     507.44043882416463,534.3382764375148,967.6392677402239,
                                     1875.507871717115,3171.7801027349824,5101.18005605357
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -10.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//============//
// test # 149 //
//============//

TEST(MatrixTest149, LAPACK_ZHEEVX3)
{
     MatrixDense<complex<double>> matA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 5) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 5) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvaluesZHEEVX(eigenvaluesA);

     // test

     const double arr_values[100] = {-323.38478082263765, -318.95649570052416, -259.5812044929057,
                                     -255.47902023149175, -210.8574832531544, -207.046095983574,
                                     -172.17668982103106, -169.63822144706373, -167.11362728657673,
                                     -164.65335127248554, -137.93078894722365, -135.63554168942136,
                                     -133.27655590262873, -130.90313825750582, -111.16491214977447,
                                     -108.69201485977163, -104.86333072392988, -102.09654131372476,
                                     -89.52930445237439, -87.11011352113846, -79.24098389717554,
                                     -76.62676551546362, -71.01471245774239, -68.78074078937429,
                                     -57.39730933358366, -55.597048251741185, -53.807888693814036,
                                     -52.082337027364105, -42.001162570408376, -40.10702874995469,
                                     -36.13941170041208, -34.10866137118017, -29.537625276581757,
                                     -27.807512581350473, -20.493485216120316, -19.180726411630474,
                                     -17.86587524596272, -16.594313970872253, -10.527129255160773,
                                     -9.235915164234662, -6.274431175607891, -4.736234460618425,
                                     -3.113902006580256, -2.3717281394985243, -0.07129875210819334,
                                     2.4178759119876623, 3.3310614360036803, 4.154955748257782,
                                     8.633411865685325, 10.366556352869488, 12.095377640289747,
                                     14.018873556316715, 17.48114915683801, 21.917608573061386,
                                     23.316848960689295, 27.074595655585338, 31.322004721507344,
                                     34.9238763246869, 36.70168716147582, 39.12924266179792,
                                     48.291435480841905, 50.073858853444456, 52.12562986320187,
                                     57.27293729356433, 64.70819943681298, 66.93714122637961,
                                     68.82639047739008, 80.9665198923797, 83.5742202822415,
                                     86.58950836310686, 89.44951473838972, 102.06705367044331,
                                     107.13994334023343, 109.44821746324266, 117.55286230732882,
                                     126.41194103599305, 133.5454916018076, 135.893724579225,
                                     151.52771532059967, 157.8972863072518, 165.22044899155756,
                                     167.77180305861472, 190.84742611813815, 205.77684982675112,
                                     208.5174101551291, 234.35823130936174, 279.92197126989515,
                                     330.06056030550724, 384.6942438609073, 444.0868835363212,
                                     508.62422446622827, 578.8097468653175, 655.2951900145777,
                                     738.9379634908137, 830.8998363959786, 932.8239020042909,
                                     1047.1805607335104, 1178.0384559097015, 1333.1755507750008,
                                     1532.5774637949444
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -5.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//============//
// test # 150 //
//============//

TEST(MatrixTest150, LAPACK_ZHEEVX4)
{
     MatrixDense<complex<double>> matA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 20) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 20) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvaluesZHEEVX(eigenvaluesA);

     // test

     const double arr_values[100] = {-890.0679762687605,-833.96613152625,-387.9183497645402,
                                     -374.28858505936836,-325.97417429278767,-290.39317003462327,
                                     -243.09349980476634,-237.9671765884564,-184.65425879239723,
                                     -181.6433889406905,-152.42678092373745,-150.01440312367828,
                                     -140.20043836510496,-134.26239413119825,-130.50508513840435,
                                     -128.1365612192653,-119.6243787096752,-117.25234191992651,
                                     -112.47792700587912,-108.89915545206071,-104.74759545668331,
                                     -100.22968339474058,-94.96995783385485,-89.69665862307328,
                                     -86.4677796222291,-68.14673474241698,-66.07309844841615,
                                     -56.302362373674626,-54.63592515665142,-49.54128744936952,
                                     -47.77783428990662,-45.159157747842364,-43.112766536500985,
                                     -40.904881657341534,-38.624975888999664,-36.21821364136139,
                                     -33.61319247902676,-30.618705849873916,-26.992630355951256,
                                     -19.965626399359778,-9.761220034345175,-0.009675776079236078,
                                     -8.793180471674402e-7,-4.675655622579791e-14,-3.291874702847949e-14,
                                     -3.0408119421278934e-14,-2.2328449483329286e-14,
                                     -1.0693016765542536e-14,-7.240806196368183e-15,
                                     -5.761950647030487e-15,-5.957409950992936e-16,3.2491920424847875e-15,
                                     9.219910025371442e-15,1.2799168101873962e-14,1.7108898977066272e-14,
                                     2.855032832850196e-14,4.1048723593164446e-14,9.99499970701814,
                                     18.470571911228987,30.51830887197781,33.54443547978839,
                                     36.1607309907248,38.57320314001222,40.85924266944485,
                                     43.035391586290125,45.287513370945284,47.151147140980484,
                                     50.73276704593593,52.39952223577173,59.50068014643575,
                                     61.31379159039717,74.51253995296709,77.13729578952395,
                                     94.37203226002364,99.43109411760354,102.6487394625419,
                                     105.06218043709777,107.8179491492106,110.12126119755328,
                                     113.57945167126782,116.37801423876415,122.99832759829658,
                                     125.12209476580418,138.05692785144052,140.21814403606146,
                                     161.31660187081715,164.54676840029904,175.43647569692263,
                                     192.3282320950376,207.53405143733352,212.25537253729775,
                                     284.3628999051823,294.6884623476941,341.98290548531804,
                                     507.44043882416463,534.3382764375148,967.6392677402239,
                                     1875.507871717115,3171.7801027349824,5101.18005605357
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -5.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//============//
// test # 151 //
//============//

TEST(MatrixTest151, LAPACK_ZHEEVX5)
{
     MatrixDense<complex<double>> matA;
     MatrixDense<complex<double>> eigenvectorsA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 5) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 5) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvectorsZHEEVX(eigenvectorsA);

     // test

     const double arr_values99[100] = {3.6942314255214093e-22,
                                       1.9280574390556084e-21,
                                       8.336207484683114e-21,
                                       3.201738809930229e-20,
                                       1.128792365433472e-19,
                                       3.7219958073502635e-19,
                                       1.1616570563876596e-18,
                                       3.4603856776191363e-18,
                                       9.898106164891213e-18,
                                       2.7313034552506115e-17,
                                       7.297235551812733e-17,
                                       1.8931798526470812e-16,
                                       4.781008453916819e-16,
                                       1.1776583982769647e-15,
                                       2.8342405979299284e-15,
                                       6.674396729414062e-15,
                                       1.5399303294661802e-14,
                                       3.4849026635393604e-14,
                                       7.743020050094637e-14,
                                       1.6906046576799098e-13,
                                       3.630170569572799e-13,
                                       7.671355827326599e-13,
                                       1.5964469231753074e-12,
                                       3.2735991022182145e-12,
                                       6.617806646334326e-12,
                                       1.3195608156949319e-11,
                                       2.596350061894761e-11,
                                       5.0430333328453936e-11,
                                       9.67336578559975e-11,
                                       1.8330342627363303e-10,
                                       3.4324925188064113e-10,
                                       6.353651498186134e-10,
                                       1.1628708815038037e-9,
                                       2.104965971919157e-9,
                                       3.7693601315726985e-9,
                                       6.6787563519161844e-9,
                                       1.1711672807554324e-8,
                                       2.03292597117336e-8,
                                       3.4936698501777865e-8,
                                       5.9452893015691615e-8,
                                       1.0019897315247485e-7,
                                       1.672693861786821e-7,
                                       2.7662540139461026e-7,
                                       4.5325802559019753e-7,
                                       7.359163073250683e-7,
                                       1.1841012055800893e-6,
                                       1.8883009979218705e-6,
                                       2.984811021145573e-6,
                                       4.676960526639804e-6,
                                       7.265186654807506e-6,
                                       0.000011189174362740128,
                                       0.00001708624363917233,
                                       0.000025871327250800013,
                                       0.00003884524735147213,
                                       0.000057839551835632114,
                                       0.0000854078666854305,
                                       0.00012507547298611522,
                                       0.0001816604867751283,
                                       0.0002616813987570242,
                                       0.00037386653150919884,
                                       0.0005297808051872514,
                                       0.0007445835839285321,
                                       0.0010379277320564163,
                                       0.0014350037062010874,
                                       0.001967722735545796,
                                       0.0026760192519511626,
                                       0.0036092342363378766,
                                       0.00482751783582054,
                                       0.00640315989292051,
                                       0.008421723355192411,
                                       0.010982819682174948,
                                       0.014200329986124536,
                                       0.018201824732492763,
                                       0.023126912983358,
                                       0.029124247016656085,
                                       0.03634692535629019,
                                       0.04494587964943125,
                                       0.055061139884829774,
                                       0.0668110106541383,
                                       0.08027936243852593,
                                       0.09549945152005936,
                                       0.11243731017783884,
                                       0.13097574227719463,
                                       0.15090013051721174,
                                       0.17186879268938443,
                                       0.19341147439162046,
                                       0.21493488140894385,
                                       0.23573601738701488,
                                       0.2548580930562397,
                                       0.27133687410309687,
                                       0.28424352285526244,
                                       0.29273026481733555,
                                       0.29466646943807095,
                                       0.28996990773813897,
                                       0.27878021480447934,
                                       0.26146670556930435,
                                       0.22712948196564564,
                                       0.19198046679049208,
                                       0.15683996268683814,
                                       0.1225409827758915
                                      };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,99)) - abs(arr_values99[i])) < pow(10.0, -10.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//============//
// test # 152 //
//============//

TEST(MatrixTest152, LAPACK_ZHEEVX6)
{
     MatrixDense<complex<double>> matA;
     MatrixDense<complex<double>> eigenvectorsA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 20) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 20) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvectorsZHEEVX(eigenvectorsA);

     // test data # 1

     const double arr_values99[100] = {-0.00002379437463800071,-0.0000321126535355192,
                                       -0.00004261209009056319,-0.00005574228105018768,
                                       -0.0000720258070448845,-0.00009206699311457821,
                                       -0.00011656127426176481,-0.00014630515846035676,
                                       -0.0001822067730093074,-0.00022529697290878234,
                                       -0.00027674098203948964,-0.0003378505293342626,
                                       -0.00041009643284298355,-0.0004951215746125915,
                                       -0.0005947541986465926,-0.0007110214528956958,-0.0008461630842956044,
                                       -0.001002645183353377,-0.0011831738617460526,-0.0013907087329034266,
                                       -0.0016283827619966063,-0.0018997449209395513,-0.002208573606467658,
                                       -0.002558944309118129,-0.0029552087127267114,-0.0034019962501085446,
                                       -0.003904213875493434,-0.00446704386484559,-0.00509593944935361,
                                       -0.005796618082542783,-0.006575052137809326,-0.007437456830885415,
                                       -0.00839027516100404,-0.009440159665541611,-0.010593950785877937,
                                       -0.01185865164733851,-0.013241399063586366,-0.014749430585927434,
                                       -0.0163900474309,-0.018170573135449803,-0.0200983089054282,
                                       -0.022180481856340313,-0.024424190854787288,-0.026834080780647522,
                                       -0.02941440937535356,-0.03216899598488147,-0.035101168178730414,
                                       -0.03821370638490854,-0.04150878671030463,-0.04498792214706611,
                                       -0.04865190239866414,-0.05250073259412269,-0.056533571195309565,
                                       -0.06074866744009344,-0.0651432987033876,-0.06971370819842077,
                                       -0.07445504348175375,-0.07936129626731185,-0.08442524409670342,
                                       -0.08963839445497533,-0.09499093194079421,-0.10047166921101545,
                                       -0.10595789789842808,-0.11143628534678302,-0.11689273862688677,
                                       -0.1223124096438104,-0.12767970465965733,-0.13297829857254914,
                                       -0.13819115429332646,-0.1433005475596976,-0.14828809752292466,
                                       -0.15313480343431787,-0.15782108774752324,-0.16232684593753477,
                                       -0.16663150331824728,-0.17071407911689127,-0.17455325803557586,
                                       -0.17812746949713273,-0.18141497473424503,-0.18439396183722762,
                                       -0.18704264882716626,-0.18541168339207653,-0.18356676120058393,
                                       -0.18150272926411598,-0.17921485423076686,-0.17669886945503793,
                                       -0.17395102341224067,-0.17096812928970723,-0.1677476155625255,
                                       -0.1642875773360043,-0.16058682821057293,-0.15664495239745393,
                                       -0.15246235678535205,-0.14804032262974395,-0.14338105650732136,
                                       -0.13848774014895246,-0.1333645787354236,-0.128016847211483,
                                       -0.1224509341456258,-0.11667438263597216
                                      };

     // test data # 2

     const double arr_values98[100] = {0.0005490815006598427,0.0007217004212979288,0.0009319727820738912,
                                       0.0011856764796110383,0.0014891369301256077,0.0018492353084341568,
                                       0.002273410759398609,0.002769655716932461,0.003346503443557062,
                                       0.00401300689217423,0.0047787079930774546,0.00565359648519078,
                                       0.006648057443072511,0.007772806702325777,0.009038813457661714,
                                       0.010457209401826704,0.012039183891666605,0.013795864771304657,
                                       0.01573818465305239,0.01787673265523282,0.020218129530799774,
                                       0.022773522732384158,0.02555088599547039,0.02855699048006569,
                                       0.03179740706616532,0.03527631554658473,0.038996305859039175,
                                       0.04295817276333242,0.047160705650423344,0.05160047546600458,
                                       0.056271621039461366,0.061165637426456754,0.06627116919601259,
                                       0.07157381191620707,0.0770559254111312,0.08269646266941802,
                                       0.08847081857459133,0.09435070289203276,0.10030404217816305,
                                       0.10629491546542182,0.11228359420772163,0.11822646848984367,
                                       0.12407615562938958,0.12979275139874066,0.13533385216700752,
                                       0.14065467360130016,0.14570820649639804,0.1504454123825699,
                                       0.15481546139101132,0.15876601462676446,0.16224355300490953,
                                       0.1651937541424425,0.16756191846119972,0.16929344514268863,
                                       0.17033435798074814,0.17063188050057534,0.17013505895196374,
                                       0.16879543094112387,0.16656773654130375,0.16341066772151153,
                                       0.1592876487959431,0.15416764632985688,0.14836208495033895,
                                       0.14186022973422263,0.1346552528267162,0.12674464039743252,
                                       0.118130599994188,0.10882046380600134,0.09882708283296263,
                                       0.08816920644738636,0.07687184132594177,0.06496658324539006,
                                       0.052491914775380344,0.039493461481783246,0.026024198885649517,
                                       0.012144602119448178,-0.002077270001960313,-0.0165657649080691,
                                       -0.0312377086295896,-0.04600256891175273,-0.06076271707085829,
                                       -0.06795927022946352,-0.07503806683258157,-0.0819579039270416,
                                       -0.08867572326732134,-0.09514679596043041,-0.1013249415936529,
                                       -0.10716278331612594,-0.1126120400798519,-0.11762385692798413,
                                       -0.12214917384544385,-0.12613913325700105,-0.12954552576940606,
                                       -0.1323212732062947,-0.13442094737761986,-0.13580132236057105,
                                       -0.13642195734879647,-0.13624580635507777,-0.1352398502347542,
                                       -0.13337574564008783
                                      };

     // test data # 3

     const double arr_values97[100] = {-0.0048844047935863765,-0.006236486880389232,-0.007815274325331554,
                                       -0.009640183890712983,-0.011730050533458039,-0.014102771342375649,
                                       -0.016774922205544347,-0.019761352037216788,-0.023074760743478643,
                                       -0.026725268499719908,-0.03071998531275152,-0.03506259120033407,
                                       -0.03975293858755401,-0.04478668963230909,-0.050155002084304526,
                                       -0.05584427788164485,-0.06183598892150303,-0.06810659423095557,
                                       -0.07462756203769172,-0.08136550893055443,-0.08823038013976417,
                                       -0.09520849071940404,-0.1022476308045662,-0.10929156383600795,
                                       -0.11627847844623534,-0.12314131678294159,-0.1298082479646105,
                                       -0.13620329680129123,-0.14224713548726084,-0.14785804289917964,
                                       -0.15295303238959757,-0.1574491445459654,-0.1612648963069822,
                                       -0.16432187213530286,-0.16654643671328404,-0.16787154196597193,
                                       -0.16823859426980445,-0.16759934066305365,-0.16591772596198778,
                                       -0.1631716661722737,-0.15935634408053131,-0.15448321146294608,
                                       -0.14858218139776455,-0.1416381780493364,-0.1336490652093021,
                                       -0.12462715724623677,-0.11460064260169803,-0.10361487512764765,
                                       -0.09173348472751607,-0.07903925562502323,-0.06563471836746396,
                                       -0.051642400613860905,-0.03720468211196078,-0.022483201281379775,
                                       -0.007657764735299888,0.007075282886553215,0.021505263042677962,
                                       0.03540996481701064,0.04855905391412624,0.06071808324036558,
                                       0.07165316959606502,0.08113608035225883,0.09009849834439126,
                                       0.09838663571146652,0.10584187751947657,0.11230295059241002,
                                       0.11760842867321217,0.12159956720711124,0.12412345180969263,
                                       0.1250364341022301,0.12420781716730643,0.12152374055295379,
                                       0.11689120174753254,0.11024213763900717,0.10153747600764697,
                                       0.09077105399613189,0.07797328823931185,0.06321447046527728,
                                       0.04660755350441126,0.028310286413503014,0.008526547885299433,
                                       0.0025719295116293645,-0.004200598749307477,-0.011762440439929252,
                                       -0.020069965854842452,-0.029063538293552106,-0.038666727752430484,
                                       -0.048785756486008615,-0.05930922217827011,-0.07010814475633873,
                                       -0.08103638195706307,-0.09193145642839495,-0.10261583322770457,
                                       -0.11289868090088843,-0.12257814174844638,-0.1314441273038225,
                                       -0.139281643409791,-0.14587463558538188,-0.15101032970590494,
                                       -0.15448402553323257
                                      };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,99)) - abs(arr_values99[i])) < pow(10.0, -10.0));
     }

     // test # 2

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,98)) - abs(arr_values98[i])) < pow(10.0, -10.0));
     }

     // test # 3

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,97)) - abs(arr_values97[i])) < pow(10.0, -10.0));
     }

     // test # 4

     EXPECT_TRUE(res == 0);
}

//============//
// test # 153 //
//============//

TEST(MatrixTest153, LAPACK_ZHEEV_GENERIC1)
{
     MatrixDense<complex<double>> matA;
     MatrixDense<complex<double>> eigenvectorsA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 5) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 5) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigensystemComplexHermitian(eigenvectorsA, eigenvaluesA);

     // test

     const double arr_values[100] = {-323.38478082263765, -318.95649570052416, -259.5812044929057,
                                     -255.47902023149175, -210.8574832531544, -207.046095983574,
                                     -172.17668982103106, -169.63822144706373, -167.11362728657673,
                                     -164.65335127248554, -137.93078894722365, -135.63554168942136,
                                     -133.27655590262873, -130.90313825750582, -111.16491214977447,
                                     -108.69201485977163, -104.86333072392988, -102.09654131372476,
                                     -89.52930445237439, -87.11011352113846, -79.24098389717554,
                                     -76.62676551546362, -71.01471245774239, -68.78074078937429,
                                     -57.39730933358366, -55.597048251741185, -53.807888693814036,
                                     -52.082337027364105, -42.001162570408376, -40.10702874995469,
                                     -36.13941170041208, -34.10866137118017, -29.537625276581757,
                                     -27.807512581350473, -20.493485216120316, -19.180726411630474,
                                     -17.86587524596272, -16.594313970872253, -10.527129255160773,
                                     -9.235915164234662, -6.274431175607891, -4.736234460618425,
                                     -3.113902006580256, -2.3717281394985243, -0.07129875210819334,
                                     2.4178759119876623, 3.3310614360036803, 4.154955748257782,
                                     8.633411865685325, 10.366556352869488, 12.095377640289747,
                                     14.018873556316715, 17.48114915683801, 21.917608573061386,
                                     23.316848960689295, 27.074595655585338, 31.322004721507344,
                                     34.9238763246869, 36.70168716147582, 39.12924266179792,
                                     48.291435480841905, 50.073858853444456, 52.12562986320187,
                                     57.27293729356433, 64.70819943681298, 66.93714122637961,
                                     68.82639047739008, 80.9665198923797, 83.5742202822415,
                                     86.58950836310686, 89.44951473838972, 102.06705367044331,
                                     107.13994334023343, 109.44821746324266, 117.55286230732882,
                                     126.41194103599305, 133.5454916018076, 135.893724579225,
                                     151.52771532059967, 157.8972863072518, 165.22044899155756,
                                     167.77180305861472, 190.84742611813815, 205.77684982675112,
                                     208.5174101551291, 234.35823130936174, 279.92197126989515,
                                     330.06056030550724, 384.6942438609073, 444.0868835363212,
                                     508.62422446622827, 578.8097468653175, 655.2951900145777,
                                     738.9379634908137, 830.8998363959786, 932.8239020042909,
                                     1047.1805607335104, 1178.0384559097015, 1333.1755507750008,
                                     1532.5774637949444
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -10.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//============//
// test # 154 //
//============//

TEST(MatrixTest154, LAPACK_ZHEEV_GENERIC2)
{
     MatrixDense<complex<double>> matA;
     MatrixDense<complex<double>> eigenvectorsA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 20) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 20) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigensystemComplexHermitian(eigenvectorsA, eigenvaluesA);

     // test

     const double arr_values[100] = {-890.0679762687605,-833.96613152625,-387.9183497645402,
                                     -374.28858505936836,-325.97417429278767,-290.39317003462327,
                                     -243.09349980476634,-237.9671765884564,-184.65425879239723,
                                     -181.6433889406905,-152.42678092373745,-150.01440312367828,
                                     -140.20043836510496,-134.26239413119825,-130.50508513840435,
                                     -128.1365612192653,-119.6243787096752,-117.25234191992651,
                                     -112.47792700587912,-108.89915545206071,-104.74759545668331,
                                     -100.22968339474058,-94.96995783385485,-89.69665862307328,
                                     -86.4677796222291,-68.14673474241698,-66.07309844841615,
                                     -56.302362373674626,-54.63592515665142,-49.54128744936952,
                                     -47.77783428990662,-45.159157747842364,-43.112766536500985,
                                     -40.904881657341534,-38.624975888999664,-36.21821364136139,
                                     -33.61319247902676,-30.618705849873916,-26.992630355951256,
                                     -19.965626399359778,-9.761220034345175,-0.009675776079236078,
                                     -8.793180471674402e-7,-4.675655622579791e-14,-3.291874702847949e-14,
                                     -3.0408119421278934e-14,-2.2328449483329286e-14,
                                     -1.0693016765542536e-14,-7.240806196368183e-15,
                                     -5.761950647030487e-15,-5.957409950992936e-16,3.2491920424847875e-15,
                                     9.219910025371442e-15,1.2799168101873962e-14,1.7108898977066272e-14,
                                     2.855032832850196e-14,4.1048723593164446e-14,9.99499970701814,
                                     18.470571911228987,30.51830887197781,33.54443547978839,
                                     36.1607309907248,38.57320314001222,40.85924266944485,
                                     43.035391586290125,45.287513370945284,47.151147140980484,
                                     50.73276704593593,52.39952223577173,59.50068014643575,
                                     61.31379159039717,74.51253995296709,77.13729578952395,
                                     94.37203226002364,99.43109411760354,102.6487394625419,
                                     105.06218043709777,107.8179491492106,110.12126119755328,
                                     113.57945167126782,116.37801423876415,122.99832759829658,
                                     125.12209476580418,138.05692785144052,140.21814403606146,
                                     161.31660187081715,164.54676840029904,175.43647569692263,
                                     192.3282320950376,207.53405143733352,212.25537253729775,
                                     284.3628999051823,294.6884623476941,341.98290548531804,
                                     507.44043882416463,534.3382764375148,967.6392677402239,
                                     1875.507871717115,3171.7801027349824,5101.18005605357
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -10.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//============//
// test # 155 //
//============//

TEST(MatrixTest155, LAPACK_ZHEEV_GENERIC3)
{
     MatrixDense<complex<double>> matA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 5) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 5) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvaluesComplexHermitian(eigenvaluesA);

     // test

     const double arr_values[100] = {-323.38478082263765, -318.95649570052416, -259.5812044929057,
                                     -255.47902023149175, -210.8574832531544, -207.046095983574,
                                     -172.17668982103106, -169.63822144706373, -167.11362728657673,
                                     -164.65335127248554, -137.93078894722365, -135.63554168942136,
                                     -133.27655590262873, -130.90313825750582, -111.16491214977447,
                                     -108.69201485977163, -104.86333072392988, -102.09654131372476,
                                     -89.52930445237439, -87.11011352113846, -79.24098389717554,
                                     -76.62676551546362, -71.01471245774239, -68.78074078937429,
                                     -57.39730933358366, -55.597048251741185, -53.807888693814036,
                                     -52.082337027364105, -42.001162570408376, -40.10702874995469,
                                     -36.13941170041208, -34.10866137118017, -29.537625276581757,
                                     -27.807512581350473, -20.493485216120316, -19.180726411630474,
                                     -17.86587524596272, -16.594313970872253, -10.527129255160773,
                                     -9.235915164234662, -6.274431175607891, -4.736234460618425,
                                     -3.113902006580256, -2.3717281394985243, -0.07129875210819334,
                                     2.4178759119876623, 3.3310614360036803, 4.154955748257782,
                                     8.633411865685325, 10.366556352869488, 12.095377640289747,
                                     14.018873556316715, 17.48114915683801, 21.917608573061386,
                                     23.316848960689295, 27.074595655585338, 31.322004721507344,
                                     34.9238763246869, 36.70168716147582, 39.12924266179792,
                                     48.291435480841905, 50.073858853444456, 52.12562986320187,
                                     57.27293729356433, 64.70819943681298, 66.93714122637961,
                                     68.82639047739008, 80.9665198923797, 83.5742202822415,
                                     86.58950836310686, 89.44951473838972, 102.06705367044331,
                                     107.13994334023343, 109.44821746324266, 117.55286230732882,
                                     126.41194103599305, 133.5454916018076, 135.893724579225,
                                     151.52771532059967, 157.8972863072518, 165.22044899155756,
                                     167.77180305861472, 190.84742611813815, 205.77684982675112,
                                     208.5174101551291, 234.35823130936174, 279.92197126989515,
                                     330.06056030550724, 384.6942438609073, 444.0868835363212,
                                     508.62422446622827, 578.8097468653175, 655.2951900145777,
                                     738.9379634908137, 830.8998363959786, 932.8239020042909,
                                     1047.1805607335104, 1178.0384559097015, 1333.1755507750008,
                                     1532.5774637949444
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -5.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//============//
// test # 156 //
//============//

TEST(MatrixTest156, LAPACK_ZHEEV_GENERIC4)
{
     MatrixDense<complex<double>> matA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 20) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 20) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvaluesComplexHermitian(eigenvaluesA);

     // test

     const double arr_values[100] = {-890.0679762687605,-833.96613152625,-387.9183497645402,
                                     -374.28858505936836,-325.97417429278767,-290.39317003462327,
                                     -243.09349980476634,-237.9671765884564,-184.65425879239723,
                                     -181.6433889406905,-152.42678092373745,-150.01440312367828,
                                     -140.20043836510496,-134.26239413119825,-130.50508513840435,
                                     -128.1365612192653,-119.6243787096752,-117.25234191992651,
                                     -112.47792700587912,-108.89915545206071,-104.74759545668331,
                                     -100.22968339474058,-94.96995783385485,-89.69665862307328,
                                     -86.4677796222291,-68.14673474241698,-66.07309844841615,
                                     -56.302362373674626,-54.63592515665142,-49.54128744936952,
                                     -47.77783428990662,-45.159157747842364,-43.112766536500985,
                                     -40.904881657341534,-38.624975888999664,-36.21821364136139,
                                     -33.61319247902676,-30.618705849873916,-26.992630355951256,
                                     -19.965626399359778,-9.761220034345175,-0.009675776079236078,
                                     -8.793180471674402e-7,-4.675655622579791e-14,-3.291874702847949e-14,
                                     -3.0408119421278934e-14,-2.2328449483329286e-14,
                                     -1.0693016765542536e-14,-7.240806196368183e-15,
                                     -5.761950647030487e-15,-5.957409950992936e-16,3.2491920424847875e-15,
                                     9.219910025371442e-15,1.2799168101873962e-14,1.7108898977066272e-14,
                                     2.855032832850196e-14,4.1048723593164446e-14,9.99499970701814,
                                     18.470571911228987,30.51830887197781,33.54443547978839,
                                     36.1607309907248,38.57320314001222,40.85924266944485,
                                     43.035391586290125,45.287513370945284,47.151147140980484,
                                     50.73276704593593,52.39952223577173,59.50068014643575,
                                     61.31379159039717,74.51253995296709,77.13729578952395,
                                     94.37203226002364,99.43109411760354,102.6487394625419,
                                     105.06218043709777,107.8179491492106,110.12126119755328,
                                     113.57945167126782,116.37801423876415,122.99832759829658,
                                     125.12209476580418,138.05692785144052,140.21814403606146,
                                     161.31660187081715,164.54676840029904,175.43647569692263,
                                     192.3282320950376,207.53405143733352,212.25537253729775,
                                     284.3628999051823,294.6884623476941,341.98290548531804,
                                     507.44043882416463,534.3382764375148,967.6392677402239,
                                     1875.507871717115,3171.7801027349824,5101.18005605357
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -5.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//============//
// test # 157 //
//============//

TEST(MatrixTest157, LAPACK_ZHEEV_GENERIC5)
{
     MatrixDense<complex<double>> matA;
     MatrixDense<complex<double>> eigenvectorsA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 5) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 5) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvectorsComplexHermitian(eigenvectorsA);

     // test

     const double arr_values99[100] = {3.6942314255214093e-22,
                                       1.9280574390556084e-21,
                                       8.336207484683114e-21,
                                       3.201738809930229e-20,
                                       1.128792365433472e-19,
                                       3.7219958073502635e-19,
                                       1.1616570563876596e-18,
                                       3.4603856776191363e-18,
                                       9.898106164891213e-18,
                                       2.7313034552506115e-17,
                                       7.297235551812733e-17,
                                       1.8931798526470812e-16,
                                       4.781008453916819e-16,
                                       1.1776583982769647e-15,
                                       2.8342405979299284e-15,
                                       6.674396729414062e-15,
                                       1.5399303294661802e-14,
                                       3.4849026635393604e-14,
                                       7.743020050094637e-14,
                                       1.6906046576799098e-13,
                                       3.630170569572799e-13,
                                       7.671355827326599e-13,
                                       1.5964469231753074e-12,
                                       3.2735991022182145e-12,
                                       6.617806646334326e-12,
                                       1.3195608156949319e-11,
                                       2.596350061894761e-11,
                                       5.0430333328453936e-11,
                                       9.67336578559975e-11,
                                       1.8330342627363303e-10,
                                       3.4324925188064113e-10,
                                       6.353651498186134e-10,
                                       1.1628708815038037e-9,
                                       2.104965971919157e-9,
                                       3.7693601315726985e-9,
                                       6.6787563519161844e-9,
                                       1.1711672807554324e-8,
                                       2.03292597117336e-8,
                                       3.4936698501777865e-8,
                                       5.9452893015691615e-8,
                                       1.0019897315247485e-7,
                                       1.672693861786821e-7,
                                       2.7662540139461026e-7,
                                       4.5325802559019753e-7,
                                       7.359163073250683e-7,
                                       1.1841012055800893e-6,
                                       1.8883009979218705e-6,
                                       2.984811021145573e-6,
                                       4.676960526639804e-6,
                                       7.265186654807506e-6,
                                       0.000011189174362740128,
                                       0.00001708624363917233,
                                       0.000025871327250800013,
                                       0.00003884524735147213,
                                       0.000057839551835632114,
                                       0.0000854078666854305,
                                       0.00012507547298611522,
                                       0.0001816604867751283,
                                       0.0002616813987570242,
                                       0.00037386653150919884,
                                       0.0005297808051872514,
                                       0.0007445835839285321,
                                       0.0010379277320564163,
                                       0.0014350037062010874,
                                       0.001967722735545796,
                                       0.0026760192519511626,
                                       0.0036092342363378766,
                                       0.00482751783582054,
                                       0.00640315989292051,
                                       0.008421723355192411,
                                       0.010982819682174948,
                                       0.014200329986124536,
                                       0.018201824732492763,
                                       0.023126912983358,
                                       0.029124247016656085,
                                       0.03634692535629019,
                                       0.04494587964943125,
                                       0.055061139884829774,
                                       0.0668110106541383,
                                       0.08027936243852593,
                                       0.09549945152005936,
                                       0.11243731017783884,
                                       0.13097574227719463,
                                       0.15090013051721174,
                                       0.17186879268938443,
                                       0.19341147439162046,
                                       0.21493488140894385,
                                       0.23573601738701488,
                                       0.2548580930562397,
                                       0.27133687410309687,
                                       0.28424352285526244,
                                       0.29273026481733555,
                                       0.29466646943807095,
                                       0.28996990773813897,
                                       0.27878021480447934,
                                       0.26146670556930435,
                                       0.22712948196564564,
                                       0.19198046679049208,
                                       0.15683996268683814,
                                       0.1225409827758915
                                      };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,99)) - abs(arr_values99[i])) < pow(10.0, -10.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//============//
// test # 158 //
//============//

TEST(MatrixTest158, LAPACK_ZHEEV_GENERIC6)
{
     MatrixDense<complex<double>> matA;
     MatrixDense<complex<double>> eigenvectorsA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 20) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 20) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvectorsComplexHermitian(eigenvectorsA);

     // test data # 1

     const double arr_values99[100] = {-0.00002379437463800071,-0.0000321126535355192,
                                       -0.00004261209009056319,-0.00005574228105018768,
                                       -0.0000720258070448845,-0.00009206699311457821,
                                       -0.00011656127426176481,-0.00014630515846035676,
                                       -0.0001822067730093074,-0.00022529697290878234,
                                       -0.00027674098203948964,-0.0003378505293342626,
                                       -0.00041009643284298355,-0.0004951215746125915,
                                       -0.0005947541986465926,-0.0007110214528956958,-0.0008461630842956044,
                                       -0.001002645183353377,-0.0011831738617460526,-0.0013907087329034266,
                                       -0.0016283827619966063,-0.0018997449209395513,-0.002208573606467658,
                                       -0.002558944309118129,-0.0029552087127267114,-0.0034019962501085446,
                                       -0.003904213875493434,-0.00446704386484559,-0.00509593944935361,
                                       -0.005796618082542783,-0.006575052137809326,-0.007437456830885415,
                                       -0.00839027516100404,-0.009440159665541611,-0.010593950785877937,
                                       -0.01185865164733851,-0.013241399063586366,-0.014749430585927434,
                                       -0.0163900474309,-0.018170573135449803,-0.0200983089054282,
                                       -0.022180481856340313,-0.024424190854787288,-0.026834080780647522,
                                       -0.02941440937535356,-0.03216899598488147,-0.035101168178730414,
                                       -0.03821370638490854,-0.04150878671030463,-0.04498792214706611,
                                       -0.04865190239866414,-0.05250073259412269,-0.056533571195309565,
                                       -0.06074866744009344,-0.0651432987033876,-0.06971370819842077,
                                       -0.07445504348175375,-0.07936129626731185,-0.08442524409670342,
                                       -0.08963839445497533,-0.09499093194079421,-0.10047166921101545,
                                       -0.10595789789842808,-0.11143628534678302,-0.11689273862688677,
                                       -0.1223124096438104,-0.12767970465965733,-0.13297829857254914,
                                       -0.13819115429332646,-0.1433005475596976,-0.14828809752292466,
                                       -0.15313480343431787,-0.15782108774752324,-0.16232684593753477,
                                       -0.16663150331824728,-0.17071407911689127,-0.17455325803557586,
                                       -0.17812746949713273,-0.18141497473424503,-0.18439396183722762,
                                       -0.18704264882716626,-0.18541168339207653,-0.18356676120058393,
                                       -0.18150272926411598,-0.17921485423076686,-0.17669886945503793,
                                       -0.17395102341224067,-0.17096812928970723,-0.1677476155625255,
                                       -0.1642875773360043,-0.16058682821057293,-0.15664495239745393,
                                       -0.15246235678535205,-0.14804032262974395,-0.14338105650732136,
                                       -0.13848774014895246,-0.1333645787354236,-0.128016847211483,
                                       -0.1224509341456258,-0.11667438263597216
                                      };

     // test data # 2

     const double arr_values98[100] = {0.0005490815006598427,0.0007217004212979288,0.0009319727820738912,
                                       0.0011856764796110383,0.0014891369301256077,0.0018492353084341568,
                                       0.002273410759398609,0.002769655716932461,0.003346503443557062,
                                       0.00401300689217423,0.0047787079930774546,0.00565359648519078,
                                       0.006648057443072511,0.007772806702325777,0.009038813457661714,
                                       0.010457209401826704,0.012039183891666605,0.013795864771304657,
                                       0.01573818465305239,0.01787673265523282,0.020218129530799774,
                                       0.022773522732384158,0.02555088599547039,0.02855699048006569,
                                       0.03179740706616532,0.03527631554658473,0.038996305859039175,
                                       0.04295817276333242,0.047160705650423344,0.05160047546600458,
                                       0.056271621039461366,0.061165637426456754,0.06627116919601259,
                                       0.07157381191620707,0.0770559254111312,0.08269646266941802,
                                       0.08847081857459133,0.09435070289203276,0.10030404217816305,
                                       0.10629491546542182,0.11228359420772163,0.11822646848984367,
                                       0.12407615562938958,0.12979275139874066,0.13533385216700752,
                                       0.14065467360130016,0.14570820649639804,0.1504454123825699,
                                       0.15481546139101132,0.15876601462676446,0.16224355300490953,
                                       0.1651937541424425,0.16756191846119972,0.16929344514268863,
                                       0.17033435798074814,0.17063188050057534,0.17013505895196374,
                                       0.16879543094112387,0.16656773654130375,0.16341066772151153,
                                       0.1592876487959431,0.15416764632985688,0.14836208495033895,
                                       0.14186022973422263,0.1346552528267162,0.12674464039743252,
                                       0.118130599994188,0.10882046380600134,0.09882708283296263,
                                       0.08816920644738636,0.07687184132594177,0.06496658324539006,
                                       0.052491914775380344,0.039493461481783246,0.026024198885649517,
                                       0.012144602119448178,-0.002077270001960313,-0.0165657649080691,
                                       -0.0312377086295896,-0.04600256891175273,-0.06076271707085829,
                                       -0.06795927022946352,-0.07503806683258157,-0.0819579039270416,
                                       -0.08867572326732134,-0.09514679596043041,-0.1013249415936529,
                                       -0.10716278331612594,-0.1126120400798519,-0.11762385692798413,
                                       -0.12214917384544385,-0.12613913325700105,-0.12954552576940606,
                                       -0.1323212732062947,-0.13442094737761986,-0.13580132236057105,
                                       -0.13642195734879647,-0.13624580635507777,-0.1352398502347542,
                                       -0.13337574564008783
                                      };

     // test data # 3

     const double arr_values97[100] = {-0.0048844047935863765,-0.006236486880389232,-0.007815274325331554,
                                       -0.009640183890712983,-0.011730050533458039,-0.014102771342375649,
                                       -0.016774922205544347,-0.019761352037216788,-0.023074760743478643,
                                       -0.026725268499719908,-0.03071998531275152,-0.03506259120033407,
                                       -0.03975293858755401,-0.04478668963230909,-0.050155002084304526,
                                       -0.05584427788164485,-0.06183598892150303,-0.06810659423095557,
                                       -0.07462756203769172,-0.08136550893055443,-0.08823038013976417,
                                       -0.09520849071940404,-0.1022476308045662,-0.10929156383600795,
                                       -0.11627847844623534,-0.12314131678294159,-0.1298082479646105,
                                       -0.13620329680129123,-0.14224713548726084,-0.14785804289917964,
                                       -0.15295303238959757,-0.1574491445459654,-0.1612648963069822,
                                       -0.16432187213530286,-0.16654643671328404,-0.16787154196597193,
                                       -0.16823859426980445,-0.16759934066305365,-0.16591772596198778,
                                       -0.1631716661722737,-0.15935634408053131,-0.15448321146294608,
                                       -0.14858218139776455,-0.1416381780493364,-0.1336490652093021,
                                       -0.12462715724623677,-0.11460064260169803,-0.10361487512764765,
                                       -0.09173348472751607,-0.07903925562502323,-0.06563471836746396,
                                       -0.051642400613860905,-0.03720468211196078,-0.022483201281379775,
                                       -0.007657764735299888,0.007075282886553215,0.021505263042677962,
                                       0.03540996481701064,0.04855905391412624,0.06071808324036558,
                                       0.07165316959606502,0.08113608035225883,0.09009849834439126,
                                       0.09838663571146652,0.10584187751947657,0.11230295059241002,
                                       0.11760842867321217,0.12159956720711124,0.12412345180969263,
                                       0.1250364341022301,0.12420781716730643,0.12152374055295379,
                                       0.11689120174753254,0.11024213763900717,0.10153747600764697,
                                       0.09077105399613189,0.07797328823931185,0.06321447046527728,
                                       0.04660755350441126,0.028310286413503014,0.008526547885299433,
                                       0.0025719295116293645,-0.004200598749307477,-0.011762440439929252,
                                       -0.020069965854842452,-0.029063538293552106,-0.038666727752430484,
                                       -0.048785756486008615,-0.05930922217827011,-0.07010814475633873,
                                       -0.08103638195706307,-0.09193145642839495,-0.10261583322770457,
                                       -0.11289868090088843,-0.12257814174844638,-0.1314441273038225,
                                       -0.139281643409791,-0.14587463558538188,-0.15101032970590494,
                                       -0.15448402553323257
                                      };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,99)) - abs(arr_values99[i])) < pow(10.0, -10.0));
     }

     // test # 2

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,98)) - abs(arr_values98[i])) < pow(10.0, -10.0));
     }

     // test # 3

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,97)) - abs(arr_values97[i])) < pow(10.0, -10.0));
     }

     // test # 4

     EXPECT_TRUE(res == 0);
}

//============//
// test # 159 //
//============//

TEST(MatrixTest159, LAPACK_ZHEEV_GENERIC7)
{
     MatrixDense<complex<double>> matA;
     MatrixDense<complex<double>> eigenvectorsA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 5) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 5) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigensystemComplexHermitian(eigenvectorsA, eigenvaluesA, "ZHEEV");

     // test

     const double arr_values[100] = {-323.38478082263765, -318.95649570052416, -259.5812044929057,
                                     -255.47902023149175, -210.8574832531544, -207.046095983574,
                                     -172.17668982103106, -169.63822144706373, -167.11362728657673,
                                     -164.65335127248554, -137.93078894722365, -135.63554168942136,
                                     -133.27655590262873, -130.90313825750582, -111.16491214977447,
                                     -108.69201485977163, -104.86333072392988, -102.09654131372476,
                                     -89.52930445237439, -87.11011352113846, -79.24098389717554,
                                     -76.62676551546362, -71.01471245774239, -68.78074078937429,
                                     -57.39730933358366, -55.597048251741185, -53.807888693814036,
                                     -52.082337027364105, -42.001162570408376, -40.10702874995469,
                                     -36.13941170041208, -34.10866137118017, -29.537625276581757,
                                     -27.807512581350473, -20.493485216120316, -19.180726411630474,
                                     -17.86587524596272, -16.594313970872253, -10.527129255160773,
                                     -9.235915164234662, -6.274431175607891, -4.736234460618425,
                                     -3.113902006580256, -2.3717281394985243, -0.07129875210819334,
                                     2.4178759119876623, 3.3310614360036803, 4.154955748257782,
                                     8.633411865685325, 10.366556352869488, 12.095377640289747,
                                     14.018873556316715, 17.48114915683801, 21.917608573061386,
                                     23.316848960689295, 27.074595655585338, 31.322004721507344,
                                     34.9238763246869, 36.70168716147582, 39.12924266179792,
                                     48.291435480841905, 50.073858853444456, 52.12562986320187,
                                     57.27293729356433, 64.70819943681298, 66.93714122637961,
                                     68.82639047739008, 80.9665198923797, 83.5742202822415,
                                     86.58950836310686, 89.44951473838972, 102.06705367044331,
                                     107.13994334023343, 109.44821746324266, 117.55286230732882,
                                     126.41194103599305, 133.5454916018076, 135.893724579225,
                                     151.52771532059967, 157.8972863072518, 165.22044899155756,
                                     167.77180305861472, 190.84742611813815, 205.77684982675112,
                                     208.5174101551291, 234.35823130936174, 279.92197126989515,
                                     330.06056030550724, 384.6942438609073, 444.0868835363212,
                                     508.62422446622827, 578.8097468653175, 655.2951900145777,
                                     738.9379634908137, 830.8998363959786, 932.8239020042909,
                                     1047.1805607335104, 1178.0384559097015, 1333.1755507750008,
                                     1532.5774637949444
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -10.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//============//
// test # 160 //
//============//

TEST(MatrixTest160, LAPACK_ZHEEV_GENERIC8)
{
     MatrixDense<complex<double>> matA;
     MatrixDense<complex<double>> eigenvectorsA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 20) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 20) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigensystemComplexHermitian(eigenvectorsA, eigenvaluesA, "ZHEEVR");

     // test

     const double arr_values[100] = {-890.0679762687605,-833.96613152625,-387.9183497645402,
                                     -374.28858505936836,-325.97417429278767,-290.39317003462327,
                                     -243.09349980476634,-237.9671765884564,-184.65425879239723,
                                     -181.6433889406905,-152.42678092373745,-150.01440312367828,
                                     -140.20043836510496,-134.26239413119825,-130.50508513840435,
                                     -128.1365612192653,-119.6243787096752,-117.25234191992651,
                                     -112.47792700587912,-108.89915545206071,-104.74759545668331,
                                     -100.22968339474058,-94.96995783385485,-89.69665862307328,
                                     -86.4677796222291,-68.14673474241698,-66.07309844841615,
                                     -56.302362373674626,-54.63592515665142,-49.54128744936952,
                                     -47.77783428990662,-45.159157747842364,-43.112766536500985,
                                     -40.904881657341534,-38.624975888999664,-36.21821364136139,
                                     -33.61319247902676,-30.618705849873916,-26.992630355951256,
                                     -19.965626399359778,-9.761220034345175,-0.009675776079236078,
                                     -8.793180471674402e-7,-4.675655622579791e-14,-3.291874702847949e-14,
                                     -3.0408119421278934e-14,-2.2328449483329286e-14,
                                     -1.0693016765542536e-14,-7.240806196368183e-15,
                                     -5.761950647030487e-15,-5.957409950992936e-16,3.2491920424847875e-15,
                                     9.219910025371442e-15,1.2799168101873962e-14,1.7108898977066272e-14,
                                     2.855032832850196e-14,4.1048723593164446e-14,9.99499970701814,
                                     18.470571911228987,30.51830887197781,33.54443547978839,
                                     36.1607309907248,38.57320314001222,40.85924266944485,
                                     43.035391586290125,45.287513370945284,47.151147140980484,
                                     50.73276704593593,52.39952223577173,59.50068014643575,
                                     61.31379159039717,74.51253995296709,77.13729578952395,
                                     94.37203226002364,99.43109411760354,102.6487394625419,
                                     105.06218043709777,107.8179491492106,110.12126119755328,
                                     113.57945167126782,116.37801423876415,122.99832759829658,
                                     125.12209476580418,138.05692785144052,140.21814403606146,
                                     161.31660187081715,164.54676840029904,175.43647569692263,
                                     192.3282320950376,207.53405143733352,212.25537253729775,
                                     284.3628999051823,294.6884623476941,341.98290548531804,
                                     507.44043882416463,534.3382764375148,967.6392677402239,
                                     1875.507871717115,3171.7801027349824,5101.18005605357
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -10.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//============//
// test # 161 //
//============//

TEST(MatrixTest161, LAPACK_ZHEEV_GENERIC9)
{
     MatrixDense<complex<double>> matA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 5) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 5) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvaluesComplexHermitian(eigenvaluesA, "ZHEEVX");

     // test

     const double arr_values[100] = {-323.38478082263765, -318.95649570052416, -259.5812044929057,
                                     -255.47902023149175, -210.8574832531544, -207.046095983574,
                                     -172.17668982103106, -169.63822144706373, -167.11362728657673,
                                     -164.65335127248554, -137.93078894722365, -135.63554168942136,
                                     -133.27655590262873, -130.90313825750582, -111.16491214977447,
                                     -108.69201485977163, -104.86333072392988, -102.09654131372476,
                                     -89.52930445237439, -87.11011352113846, -79.24098389717554,
                                     -76.62676551546362, -71.01471245774239, -68.78074078937429,
                                     -57.39730933358366, -55.597048251741185, -53.807888693814036,
                                     -52.082337027364105, -42.001162570408376, -40.10702874995469,
                                     -36.13941170041208, -34.10866137118017, -29.537625276581757,
                                     -27.807512581350473, -20.493485216120316, -19.180726411630474,
                                     -17.86587524596272, -16.594313970872253, -10.527129255160773,
                                     -9.235915164234662, -6.274431175607891, -4.736234460618425,
                                     -3.113902006580256, -2.3717281394985243, -0.07129875210819334,
                                     2.4178759119876623, 3.3310614360036803, 4.154955748257782,
                                     8.633411865685325, 10.366556352869488, 12.095377640289747,
                                     14.018873556316715, 17.48114915683801, 21.917608573061386,
                                     23.316848960689295, 27.074595655585338, 31.322004721507344,
                                     34.9238763246869, 36.70168716147582, 39.12924266179792,
                                     48.291435480841905, 50.073858853444456, 52.12562986320187,
                                     57.27293729356433, 64.70819943681298, 66.93714122637961,
                                     68.82639047739008, 80.9665198923797, 83.5742202822415,
                                     86.58950836310686, 89.44951473838972, 102.06705367044331,
                                     107.13994334023343, 109.44821746324266, 117.55286230732882,
                                     126.41194103599305, 133.5454916018076, 135.893724579225,
                                     151.52771532059967, 157.8972863072518, 165.22044899155756,
                                     167.77180305861472, 190.84742611813815, 205.77684982675112,
                                     208.5174101551291, 234.35823130936174, 279.92197126989515,
                                     330.06056030550724, 384.6942438609073, 444.0868835363212,
                                     508.62422446622827, 578.8097468653175, 655.2951900145777,
                                     738.9379634908137, 830.8998363959786, 932.8239020042909,
                                     1047.1805607335104, 1178.0384559097015, 1333.1755507750008,
                                     1532.5774637949444
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -5.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//============//
// test # 162 //
//============//

TEST(MatrixTest162, LAPACK_ZHEEV_GENERIC10)
{
     MatrixDense<complex<double>> matA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 20) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 20) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvaluesComplexHermitian(eigenvaluesA, "ZHEEVX");

     // test

     const double arr_values[100] = {-890.0679762687605,-833.96613152625,-387.9183497645402,
                                     -374.28858505936836,-325.97417429278767,-290.39317003462327,
                                     -243.09349980476634,-237.9671765884564,-184.65425879239723,
                                     -181.6433889406905,-152.42678092373745,-150.01440312367828,
                                     -140.20043836510496,-134.26239413119825,-130.50508513840435,
                                     -128.1365612192653,-119.6243787096752,-117.25234191992651,
                                     -112.47792700587912,-108.89915545206071,-104.74759545668331,
                                     -100.22968339474058,-94.96995783385485,-89.69665862307328,
                                     -86.4677796222291,-68.14673474241698,-66.07309844841615,
                                     -56.302362373674626,-54.63592515665142,-49.54128744936952,
                                     -47.77783428990662,-45.159157747842364,-43.112766536500985,
                                     -40.904881657341534,-38.624975888999664,-36.21821364136139,
                                     -33.61319247902676,-30.618705849873916,-26.992630355951256,
                                     -19.965626399359778,-9.761220034345175,-0.009675776079236078,
                                     -8.793180471674402e-7,-4.675655622579791e-14,-3.291874702847949e-14,
                                     -3.0408119421278934e-14,-2.2328449483329286e-14,
                                     -1.0693016765542536e-14,-7.240806196368183e-15,
                                     -5.761950647030487e-15,-5.957409950992936e-16,3.2491920424847875e-15,
                                     9.219910025371442e-15,1.2799168101873962e-14,1.7108898977066272e-14,
                                     2.855032832850196e-14,4.1048723593164446e-14,9.99499970701814,
                                     18.470571911228987,30.51830887197781,33.54443547978839,
                                     36.1607309907248,38.57320314001222,40.85924266944485,
                                     43.035391586290125,45.287513370945284,47.151147140980484,
                                     50.73276704593593,52.39952223577173,59.50068014643575,
                                     61.31379159039717,74.51253995296709,77.13729578952395,
                                     94.37203226002364,99.43109411760354,102.6487394625419,
                                     105.06218043709777,107.8179491492106,110.12126119755328,
                                     113.57945167126782,116.37801423876415,122.99832759829658,
                                     125.12209476580418,138.05692785144052,140.21814403606146,
                                     161.31660187081715,164.54676840029904,175.43647569692263,
                                     192.3282320950376,207.53405143733352,212.25537253729775,
                                     284.3628999051823,294.6884623476941,341.98290548531804,
                                     507.44043882416463,534.3382764375148,967.6392677402239,
                                     1875.507871717115,3171.7801027349824,5101.18005605357
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -5.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//============//
// test # 163 //
//============//

TEST(MatrixTest163, LAPACK_ZHEEV_GENERIC11)
{
     MatrixDense<complex<double>> matA;
     MatrixDense<complex<double>> eigenvectorsA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 5) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 5) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvectorsComplexHermitian(eigenvectorsA, "ZHEEVD");

     // test

     const double arr_values99[100] = {3.6942314255214093e-22,
                                       1.9280574390556084e-21,
                                       8.336207484683114e-21,
                                       3.201738809930229e-20,
                                       1.128792365433472e-19,
                                       3.7219958073502635e-19,
                                       1.1616570563876596e-18,
                                       3.4603856776191363e-18,
                                       9.898106164891213e-18,
                                       2.7313034552506115e-17,
                                       7.297235551812733e-17,
                                       1.8931798526470812e-16,
                                       4.781008453916819e-16,
                                       1.1776583982769647e-15,
                                       2.8342405979299284e-15,
                                       6.674396729414062e-15,
                                       1.5399303294661802e-14,
                                       3.4849026635393604e-14,
                                       7.743020050094637e-14,
                                       1.6906046576799098e-13,
                                       3.630170569572799e-13,
                                       7.671355827326599e-13,
                                       1.5964469231753074e-12,
                                       3.2735991022182145e-12,
                                       6.617806646334326e-12,
                                       1.3195608156949319e-11,
                                       2.596350061894761e-11,
                                       5.0430333328453936e-11,
                                       9.67336578559975e-11,
                                       1.8330342627363303e-10,
                                       3.4324925188064113e-10,
                                       6.353651498186134e-10,
                                       1.1628708815038037e-9,
                                       2.104965971919157e-9,
                                       3.7693601315726985e-9,
                                       6.6787563519161844e-9,
                                       1.1711672807554324e-8,
                                       2.03292597117336e-8,
                                       3.4936698501777865e-8,
                                       5.9452893015691615e-8,
                                       1.0019897315247485e-7,
                                       1.672693861786821e-7,
                                       2.7662540139461026e-7,
                                       4.5325802559019753e-7,
                                       7.359163073250683e-7,
                                       1.1841012055800893e-6,
                                       1.8883009979218705e-6,
                                       2.984811021145573e-6,
                                       4.676960526639804e-6,
                                       7.265186654807506e-6,
                                       0.000011189174362740128,
                                       0.00001708624363917233,
                                       0.000025871327250800013,
                                       0.00003884524735147213,
                                       0.000057839551835632114,
                                       0.0000854078666854305,
                                       0.00012507547298611522,
                                       0.0001816604867751283,
                                       0.0002616813987570242,
                                       0.00037386653150919884,
                                       0.0005297808051872514,
                                       0.0007445835839285321,
                                       0.0010379277320564163,
                                       0.0014350037062010874,
                                       0.001967722735545796,
                                       0.0026760192519511626,
                                       0.0036092342363378766,
                                       0.00482751783582054,
                                       0.00640315989292051,
                                       0.008421723355192411,
                                       0.010982819682174948,
                                       0.014200329986124536,
                                       0.018201824732492763,
                                       0.023126912983358,
                                       0.029124247016656085,
                                       0.03634692535629019,
                                       0.04494587964943125,
                                       0.055061139884829774,
                                       0.0668110106541383,
                                       0.08027936243852593,
                                       0.09549945152005936,
                                       0.11243731017783884,
                                       0.13097574227719463,
                                       0.15090013051721174,
                                       0.17186879268938443,
                                       0.19341147439162046,
                                       0.21493488140894385,
                                       0.23573601738701488,
                                       0.2548580930562397,
                                       0.27133687410309687,
                                       0.28424352285526244,
                                       0.29273026481733555,
                                       0.29466646943807095,
                                       0.28996990773813897,
                                       0.27878021480447934,
                                       0.26146670556930435,
                                       0.22712948196564564,
                                       0.19198046679049208,
                                       0.15683996268683814,
                                       0.1225409827758915
                                      };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,99)) - abs(arr_values99[i])) < pow(10.0, -10.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//============//
// test # 164 //
//============//

TEST(MatrixTest164, LAPACK_ZHEEV_GENERIC12)
{
     MatrixDense<complex<double>> matA;
     MatrixDense<complex<double>> eigenvectorsA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 20) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 20) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvectorsComplexHermitian(eigenvectorsA, "ZHEEV");

     // test data # 1

     const double arr_values99[100] = {-0.00002379437463800071,-0.0000321126535355192,
                                       -0.00004261209009056319,-0.00005574228105018768,
                                       -0.0000720258070448845,-0.00009206699311457821,
                                       -0.00011656127426176481,-0.00014630515846035676,
                                       -0.0001822067730093074,-0.00022529697290878234,
                                       -0.00027674098203948964,-0.0003378505293342626,
                                       -0.00041009643284298355,-0.0004951215746125915,
                                       -0.0005947541986465926,-0.0007110214528956958,-0.0008461630842956044,
                                       -0.001002645183353377,-0.0011831738617460526,-0.0013907087329034266,
                                       -0.0016283827619966063,-0.0018997449209395513,-0.002208573606467658,
                                       -0.002558944309118129,-0.0029552087127267114,-0.0034019962501085446,
                                       -0.003904213875493434,-0.00446704386484559,-0.00509593944935361,
                                       -0.005796618082542783,-0.006575052137809326,-0.007437456830885415,
                                       -0.00839027516100404,-0.009440159665541611,-0.010593950785877937,
                                       -0.01185865164733851,-0.013241399063586366,-0.014749430585927434,
                                       -0.0163900474309,-0.018170573135449803,-0.0200983089054282,
                                       -0.022180481856340313,-0.024424190854787288,-0.026834080780647522,
                                       -0.02941440937535356,-0.03216899598488147,-0.035101168178730414,
                                       -0.03821370638490854,-0.04150878671030463,-0.04498792214706611,
                                       -0.04865190239866414,-0.05250073259412269,-0.056533571195309565,
                                       -0.06074866744009344,-0.0651432987033876,-0.06971370819842077,
                                       -0.07445504348175375,-0.07936129626731185,-0.08442524409670342,
                                       -0.08963839445497533,-0.09499093194079421,-0.10047166921101545,
                                       -0.10595789789842808,-0.11143628534678302,-0.11689273862688677,
                                       -0.1223124096438104,-0.12767970465965733,-0.13297829857254914,
                                       -0.13819115429332646,-0.1433005475596976,-0.14828809752292466,
                                       -0.15313480343431787,-0.15782108774752324,-0.16232684593753477,
                                       -0.16663150331824728,-0.17071407911689127,-0.17455325803557586,
                                       -0.17812746949713273,-0.18141497473424503,-0.18439396183722762,
                                       -0.18704264882716626,-0.18541168339207653,-0.18356676120058393,
                                       -0.18150272926411598,-0.17921485423076686,-0.17669886945503793,
                                       -0.17395102341224067,-0.17096812928970723,-0.1677476155625255,
                                       -0.1642875773360043,-0.16058682821057293,-0.15664495239745393,
                                       -0.15246235678535205,-0.14804032262974395,-0.14338105650732136,
                                       -0.13848774014895246,-0.1333645787354236,-0.128016847211483,
                                       -0.1224509341456258,-0.11667438263597216
                                      };

     // test data # 2

     const double arr_values98[100] = {0.0005490815006598427,0.0007217004212979288,0.0009319727820738912,
                                       0.0011856764796110383,0.0014891369301256077,0.0018492353084341568,
                                       0.002273410759398609,0.002769655716932461,0.003346503443557062,
                                       0.00401300689217423,0.0047787079930774546,0.00565359648519078,
                                       0.006648057443072511,0.007772806702325777,0.009038813457661714,
                                       0.010457209401826704,0.012039183891666605,0.013795864771304657,
                                       0.01573818465305239,0.01787673265523282,0.020218129530799774,
                                       0.022773522732384158,0.02555088599547039,0.02855699048006569,
                                       0.03179740706616532,0.03527631554658473,0.038996305859039175,
                                       0.04295817276333242,0.047160705650423344,0.05160047546600458,
                                       0.056271621039461366,0.061165637426456754,0.06627116919601259,
                                       0.07157381191620707,0.0770559254111312,0.08269646266941802,
                                       0.08847081857459133,0.09435070289203276,0.10030404217816305,
                                       0.10629491546542182,0.11228359420772163,0.11822646848984367,
                                       0.12407615562938958,0.12979275139874066,0.13533385216700752,
                                       0.14065467360130016,0.14570820649639804,0.1504454123825699,
                                       0.15481546139101132,0.15876601462676446,0.16224355300490953,
                                       0.1651937541424425,0.16756191846119972,0.16929344514268863,
                                       0.17033435798074814,0.17063188050057534,0.17013505895196374,
                                       0.16879543094112387,0.16656773654130375,0.16341066772151153,
                                       0.1592876487959431,0.15416764632985688,0.14836208495033895,
                                       0.14186022973422263,0.1346552528267162,0.12674464039743252,
                                       0.118130599994188,0.10882046380600134,0.09882708283296263,
                                       0.08816920644738636,0.07687184132594177,0.06496658324539006,
                                       0.052491914775380344,0.039493461481783246,0.026024198885649517,
                                       0.012144602119448178,-0.002077270001960313,-0.0165657649080691,
                                       -0.0312377086295896,-0.04600256891175273,-0.06076271707085829,
                                       -0.06795927022946352,-0.07503806683258157,-0.0819579039270416,
                                       -0.08867572326732134,-0.09514679596043041,-0.1013249415936529,
                                       -0.10716278331612594,-0.1126120400798519,-0.11762385692798413,
                                       -0.12214917384544385,-0.12613913325700105,-0.12954552576940606,
                                       -0.1323212732062947,-0.13442094737761986,-0.13580132236057105,
                                       -0.13642195734879647,-0.13624580635507777,-0.1352398502347542,
                                       -0.13337574564008783
                                      };

     // test data # 3

     const double arr_values97[100] = {-0.0048844047935863765,-0.006236486880389232,-0.007815274325331554,
                                       -0.009640183890712983,-0.011730050533458039,-0.014102771342375649,
                                       -0.016774922205544347,-0.019761352037216788,-0.023074760743478643,
                                       -0.026725268499719908,-0.03071998531275152,-0.03506259120033407,
                                       -0.03975293858755401,-0.04478668963230909,-0.050155002084304526,
                                       -0.05584427788164485,-0.06183598892150303,-0.06810659423095557,
                                       -0.07462756203769172,-0.08136550893055443,-0.08823038013976417,
                                       -0.09520849071940404,-0.1022476308045662,-0.10929156383600795,
                                       -0.11627847844623534,-0.12314131678294159,-0.1298082479646105,
                                       -0.13620329680129123,-0.14224713548726084,-0.14785804289917964,
                                       -0.15295303238959757,-0.1574491445459654,-0.1612648963069822,
                                       -0.16432187213530286,-0.16654643671328404,-0.16787154196597193,
                                       -0.16823859426980445,-0.16759934066305365,-0.16591772596198778,
                                       -0.1631716661722737,-0.15935634408053131,-0.15448321146294608,
                                       -0.14858218139776455,-0.1416381780493364,-0.1336490652093021,
                                       -0.12462715724623677,-0.11460064260169803,-0.10361487512764765,
                                       -0.09173348472751607,-0.07903925562502323,-0.06563471836746396,
                                       -0.051642400613860905,-0.03720468211196078,-0.022483201281379775,
                                       -0.007657764735299888,0.007075282886553215,0.021505263042677962,
                                       0.03540996481701064,0.04855905391412624,0.06071808324036558,
                                       0.07165316959606502,0.08113608035225883,0.09009849834439126,
                                       0.09838663571146652,0.10584187751947657,0.11230295059241002,
                                       0.11760842867321217,0.12159956720711124,0.12412345180969263,
                                       0.1250364341022301,0.12420781716730643,0.12152374055295379,
                                       0.11689120174753254,0.11024213763900717,0.10153747600764697,
                                       0.09077105399613189,0.07797328823931185,0.06321447046527728,
                                       0.04660755350441126,0.028310286413503014,0.008526547885299433,
                                       0.0025719295116293645,-0.004200598749307477,-0.011762440439929252,
                                       -0.020069965854842452,-0.029063538293552106,-0.038666727752430484,
                                       -0.048785756486008615,-0.05930922217827011,-0.07010814475633873,
                                       -0.08103638195706307,-0.09193145642839495,-0.10261583322770457,
                                       -0.11289868090088843,-0.12257814174844638,-0.1314441273038225,
                                       -0.139281643409791,-0.14587463558538188,-0.15101032970590494,
                                       -0.15448402553323257
                                      };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,99)) - abs(arr_values99[i])) < pow(10.0, -10.0));
     }

     // test # 2

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,98)) - abs(arr_values98[i])) < pow(10.0, -10.0));
     }

     // test # 3

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,97)) - abs(arr_values97[i])) < pow(10.0, -10.0));
     }

     // test # 4

     EXPECT_TRUE(res == 0);
}

//============//
// test # 165 //
//============//

TEST(MatrixTest165, DOT_GENERIC1)
{
     MatrixDense<double> matA;
     MatrixDense<double> matB;

     // # 1

     const int ELEMS = 200;

     matA.Allocate(ELEMS);
     matB.Allocate(ELEMS);

     // # 2

     for(int i = 0; i != ELEMS*ELEMS; ++i) {
          matA.SetElement(i, sin(static_cast<double>(i+1)));
     }

     for(int i = 0; i != ELEMS*ELEMS; ++i) {
          matB.SetElement(i, cos(static_cast<double>(i+1)));
     }

     matA.Dot(matB);

     const double L1[ELEMS] = {
          -0.804158, -0.0518092, 0.748173, 0.860288, 0.181459, -0.664203,
          -0.899199, -0.307476, 0.566939, 0.920113, 0.427339, -0.458329,
          -0.922611, -0.538649, 0.340544, 0.906643, 0.639178, -0.215944,
          -0.872528, -0.726914, 0.0870214, 0.82095, 0.800101, 0.0436428,
          -0.75294, -0.857274, -0.173433, 0.669861, 0.897288, 0.299753,
          -0.573374, -0.919343, -0.420073, 0.465411, 0.922998, 0.531985,
          -0.348132, -0.908178, -0.633249, 0.223886, 0.875182, 0.721839,
          -0.0951589, -0.824668, -0.795981, -0.035473, 0.757649, 0.854192,
          0.165395, -0.675466, -0.895306, -0.292006, 0.579763, 0.918501,
          0.412773, -0.472456, -0.923312, -0.525279, 0.355693, 0.909642,
          0.62727, -0.231811, -0.877766, -0.716707, 0.103289, 0.828322, 0.7918,
          0.0273004, -0.762299, -0.851044, -0.157343, 0.681018, 0.893254,
          0.284237, -0.586107, -0.917587, -0.405441, 0.479465, 0.923553,
          0.518531, -0.363226, -0.911035, -0.621242, 0.239717, 0.880282,
          0.71152, -0.111411, -0.831911, -0.787556, -0.0191256, 0.766888,
          0.847829, 0.149279, -0.686517, -0.891133, -0.276445, 0.592405,
          0.9166, 0.398078, -0.486436, -0.923722, -0.511743, 0.37073, 0.912356,
          0.615166, -0.247605, -0.882729, -0.706276, 0.119524, 0.835434,
          0.78325, 0.0109494, -0.771418, -0.844547, -0.141204, 0.691962,
          0.888941, 0.268632, -0.598656, -0.915542, -0.390683, 0.493368,
          0.923819, 0.504915, -0.378206, -0.913606, -0.609041, 0.255474,
          0.885107, 0.700977, -0.127628, -0.838892, -0.778883, -0.00277231,
          0.775887, 0.8412, 0.133117, -0.697353, -0.88668, -0.260797, 0.604861,
          0.914413, 0.383258, -0.500263, -0.923844, -0.498047, 0.385652,
          0.914784, 0.602868, -0.263322, -0.887415, -0.695623, 0.135722,
          0.842285, 0.774455, -0.005405, -0.780296, -0.837786, -0.12502,
          0.702689, 0.884349, 0.252942, -0.611018, -0.913211, -0.375802,
          0.507118, 0.923796, 0.491141, -0.393067, -0.915891, -0.596649,
          0.27115, 0.889654, 0.690215, -0.143805, -0.845611, -0.769966,
          0.0135819, 0.784643, 0.834307, 0.116913, -0.70797, -0.881949,
          -0.245068, 0.617128, 0.911939, 0.368318, -0.513933, -0.923676,
          -0.484195, 0.400452, 0.916926, 0.590382, -0.278956, -0.891823,
          -0.684752, 0.151877, 0.848871, 0.765417, -0.0217577
     };

     // test

     for(int i = 0; i != ELEMS; ++i) {
          EXPECT_TRUE((matA(i)-L1[i])< pow(10.0, -6.0));
     }
}

//============//
// test # 166 //
//============//

TEST(MatrixTest166, DOT_GENERIC2)
{
     MatrixDense<double> mat;

     // # 1

     const int ELEMS = 200;

     mat.Allocate(ELEMS);

     // # 2

     for(int i = 0; i != ELEMS*ELEMS; ++i) {
          mat.SetElement(i, sin(static_cast<double>(i+1)));
     }

     mat.Dot(mat);

     const double L1[ELEMS] = { -0.4547740548579048,
                                -0.92239098632159,-0.5419658987851806,
                                0.3367401366905215,0.9058488434496651,
                                0.6421243010771581,
                                -0.21196636239775463,
                                -0.8711761298171293,
                                -0.7294305811172018,
                                0.08295007992042225,0.8190668200230955,
                                0.8021373031167128,
                                0.047726448970537905,
                                -0.750563882257365,-0.8587892415405249,
                                -0.17744773266083447,0.667038403285078,
                                0.8982525074558882,0.30361739877543314,
                                -0.5701621461357871,
                                -0.9197372433272173,
                                -0.42371016058922306,
                                0.46187408975499866,0.9228134320199571,
                                0.5353223606579501,
                                -0.34434162032741267,
                                -0.9074195035965021,
                                -0.6362200800384232,0.219917151027772,
                                0.8738635676389255,0.7243838501911728,
                                -0.09109103845506883,
                                -0.8228172464335676,
                                -0.7980490726571842,
                                -0.039558261871662004,
                                0.7553022324464025,0.8557413375079496,
                                0.16941580331806072,
                                -0.6726698391414552,
                                -0.8963059336702137,
                                -0.29588248630916936,
                                0.5765739544325299,0.9189309604759685,
                                0.41642707932506573,
                                -0.4689379381054516,
                                -0.9231635778599094,
                                -0.5286368815169527,
                                0.35191612575881615,0.9089190699562798,
                                0.630266012930994,-0.2278507097624155,
                                -0.8764825406876439,
                                -0.7192803658109853,
                                0.09922486026095137,0.82650320740784,
                                0.7938983172788,0.031386975493302105,
                                -0.7599814068122905,
                                -0.8526263885284852,
                                -0.16137060071964365,0.678248573192179,
                                0.894289136814667,0.2881243922754051,
                                -0.5829405897681523,
                                -0.9180526819471343,
                                -0.4091113721607794,0.475965046476442,
                                0.9234413964084904,0.5219099851507703,
                                -0.3594630595433629,-0.910347425042154,
                                -0.6242625662394488,
                                0.23576641702948326,0.8790328437740519,
                                0.7141205278204007,
                                -0.10735090807577552,
                                -0.8301244141611595,
                                -0.7896853621816625,
                                -0.023213230032963106,
                                0.7646010387547428,0.8494446386496892,
                                0.1533127551847701,-0.6837741683590182,
                                -0.8922022748996743,
                                -0.2803437244991476,0.5892615533345438,
                                0.917102476551362,0.4017636122616153,
                                -0.482954864313648,-0.9236468658993975,
                                -0.5151421985930099,0.3669818303998238,
                                0.9117044569464832,0.618210210317069,
                                -0.24366365265541162,
                                -0.881514277089004,-0.708904740478344,
                                0.11546854524639802,0.833680582982085,
                                0.7854105374390289,
                                0.015037665880807793,
                                -0.7691607663384753,
                                -0.8461963371527894,
                                -0.14524289802315105,
                                0.6892461917270185,0.8900455114250383,
                                0.2725410925740698,-0.5955363499018653,
                                -0.9160804187345986,
                                -0.39438437530405457,
                                0.48990684398431283,0.923779970234668,
                                0.5083340520808721,-0.3744718492534889,
                                -0.9129900593496048,
                                -0.6121094193490646,0.2515417979137902,
                                0.8839266462191426,0.7036334124272031,
                                -0.12357713577856033,
                                -0.8371714352546789,
                                -0.7810741779715231,
                                -0.006860923569461108,
                                0.7736602323215945,0.8428817385331402,
                                0.1371616614856132,-0.694664214578376,
                                -0.8878190153671653,
                                -0.26471710781454294,
                                0.6017644878572668,0.9149865885722256,
                                0.3869742394307324,-0.4968204408202457,
                                -0.9238406989859405,
                                -0.5014860790136346,0.3819325292822619,
                                0.9142041315281431,0.605960671315407,
                                -0.25940023557390873,
                                -0.8862697621620494,
                                -0.6983069566608208,
                                0.13167604438684563,0.8405966974804419,
                                0.776676623520804,
                                -0.001316356276091779,
                                -0.7780990841834396,-0.839501102480296,
                                -0.129069678714471,0.7000278124261168,
                                0.8855229611658002,0.25687238320800276,
                                -0.6079454792435462,
                                -0.9138210717628001,
                                -0.3795337852051339,0.5036951131604182,
                                0.923829047395298,0.4945988159108795,
                                -0.3893632859626914,
                                -0.9153465783629597,
                                -0.5997644479533387,0.267238349949119,
                                0.8885434413411558,0.6929257904921555,
                                -0.13976463654432725,
                                -0.8439561012995991,
                                -0.7722182186229937,
                                0.009493532988809561,
                                0.7824769741523714,0.8360546938576927,
                                0.12096758369400934,
                                -0.7053365650473533,
                                -0.8831575287104007,
                                -0.24900753336672532,0.614078839797237,
                                0.9125839596212897,0.37206359556607027,
                                -0.5105303223934075,
                                -0.9237450163756002,
                                -0.48767280237047106,
                                0.3967635371157051,0.9164173103465129,
                                0.5935212347196825,
                                -0.27505552694500846,
                                -0.8907475056200106,
                                -0.6874903355205647,
                                0.14784227853237167,0.8472493835122421,
                                0.7676993125816897,
                                -0.01766996590971115,
                                -0.7867935592329294,
                                -0.8325427826818116,
                                -0.1128560112007782,0.7105900565160895,
                                0.8807229033260044,0.24112317447976428,
                                -0.6201640889866735,
                                -0.9112753490719966,
                                -0.3645642557821037,0.5173255329996493,
                                0.9235886125104533
                              };

     // test

     for(int i = 0; i != ELEMS; ++i) {
          EXPECT_TRUE((mat(i)-L1[i])< pow(10.0, -6.0));
     }
}

//============//
// test # 167 //
//============//

TEST(MatrixTest167, DOT_GENERIC3)
{
     MatrixDense<double> mat;

     // # 1

     const int ELEMS = 200;

     mat.Allocate(ELEMS);

     // # 2

     for(int i = 0; i != ELEMS*ELEMS; ++i) {
          mat.SetElement(i, sin(static_cast<double>(i+1)));
     }

     mat.Dot();

     const double L1[ELEMS] = { -0.4547740548579048,
                                -0.92239098632159,-0.5419658987851806,
                                0.3367401366905215,0.9058488434496651,
                                0.6421243010771581,
                                -0.21196636239775463,
                                -0.8711761298171293,
                                -0.7294305811172018,
                                0.08295007992042225,0.8190668200230955,
                                0.8021373031167128,
                                0.047726448970537905,
                                -0.750563882257365,-0.8587892415405249,
                                -0.17744773266083447,0.667038403285078,
                                0.8982525074558882,0.30361739877543314,
                                -0.5701621461357871,
                                -0.9197372433272173,
                                -0.42371016058922306,
                                0.46187408975499866,0.9228134320199571,
                                0.5353223606579501,
                                -0.34434162032741267,
                                -0.9074195035965021,
                                -0.6362200800384232,0.219917151027772,
                                0.8738635676389255,0.7243838501911728,
                                -0.09109103845506883,
                                -0.8228172464335676,
                                -0.7980490726571842,
                                -0.039558261871662004,
                                0.7553022324464025,0.8557413375079496,
                                0.16941580331806072,
                                -0.6726698391414552,
                                -0.8963059336702137,
                                -0.29588248630916936,
                                0.5765739544325299,0.9189309604759685,
                                0.41642707932506573,
                                -0.4689379381054516,
                                -0.9231635778599094,
                                -0.5286368815169527,
                                0.35191612575881615,0.9089190699562798,
                                0.630266012930994,-0.2278507097624155,
                                -0.8764825406876439,
                                -0.7192803658109853,
                                0.09922486026095137,0.82650320740784,
                                0.7938983172788,0.031386975493302105,
                                -0.7599814068122905,
                                -0.8526263885284852,
                                -0.16137060071964365,0.678248573192179,
                                0.894289136814667,0.2881243922754051,
                                -0.5829405897681523,
                                -0.9180526819471343,
                                -0.4091113721607794,0.475965046476442,
                                0.9234413964084904,0.5219099851507703,
                                -0.3594630595433629,-0.910347425042154,
                                -0.6242625662394488,
                                0.23576641702948326,0.8790328437740519,
                                0.7141205278204007,
                                -0.10735090807577552,
                                -0.8301244141611595,
                                -0.7896853621816625,
                                -0.023213230032963106,
                                0.7646010387547428,0.8494446386496892,
                                0.1533127551847701,-0.6837741683590182,
                                -0.8922022748996743,
                                -0.2803437244991476,0.5892615533345438,
                                0.917102476551362,0.4017636122616153,
                                -0.482954864313648,-0.9236468658993975,
                                -0.5151421985930099,0.3669818303998238,
                                0.9117044569464832,0.618210210317069,
                                -0.24366365265541162,
                                -0.881514277089004,-0.708904740478344,
                                0.11546854524639802,0.833680582982085,
                                0.7854105374390289,
                                0.015037665880807793,
                                -0.7691607663384753,
                                -0.8461963371527894,
                                -0.14524289802315105,
                                0.6892461917270185,0.8900455114250383,
                                0.2725410925740698,-0.5955363499018653,
                                -0.9160804187345986,
                                -0.39438437530405457,
                                0.48990684398431283,0.923779970234668,
                                0.5083340520808721,-0.3744718492534889,
                                -0.9129900593496048,
                                -0.6121094193490646,0.2515417979137902,
                                0.8839266462191426,0.7036334124272031,
                                -0.12357713577856033,
                                -0.8371714352546789,
                                -0.7810741779715231,
                                -0.006860923569461108,
                                0.7736602323215945,0.8428817385331402,
                                0.1371616614856132,-0.694664214578376,
                                -0.8878190153671653,
                                -0.26471710781454294,
                                0.6017644878572668,0.9149865885722256,
                                0.3869742394307324,-0.4968204408202457,
                                -0.9238406989859405,
                                -0.5014860790136346,0.3819325292822619,
                                0.9142041315281431,0.605960671315407,
                                -0.25940023557390873,
                                -0.8862697621620494,
                                -0.6983069566608208,
                                0.13167604438684563,0.8405966974804419,
                                0.776676623520804,
                                -0.001316356276091779,
                                -0.7780990841834396,-0.839501102480296,
                                -0.129069678714471,0.7000278124261168,
                                0.8855229611658002,0.25687238320800276,
                                -0.6079454792435462,
                                -0.9138210717628001,
                                -0.3795337852051339,0.5036951131604182,
                                0.923829047395298,0.4945988159108795,
                                -0.3893632859626914,
                                -0.9153465783629597,
                                -0.5997644479533387,0.267238349949119,
                                0.8885434413411558,0.6929257904921555,
                                -0.13976463654432725,
                                -0.8439561012995991,
                                -0.7722182186229937,
                                0.009493532988809561,
                                0.7824769741523714,0.8360546938576927,
                                0.12096758369400934,
                                -0.7053365650473533,
                                -0.8831575287104007,
                                -0.24900753336672532,0.614078839797237,
                                0.9125839596212897,0.37206359556607027,
                                -0.5105303223934075,
                                -0.9237450163756002,
                                -0.48767280237047106,
                                0.3967635371157051,0.9164173103465129,
                                0.5935212347196825,
                                -0.27505552694500846,
                                -0.8907475056200106,
                                -0.6874903355205647,
                                0.14784227853237167,0.8472493835122421,
                                0.7676993125816897,
                                -0.01766996590971115,
                                -0.7867935592329294,
                                -0.8325427826818116,
                                -0.1128560112007782,0.7105900565160895,
                                0.8807229033260044,0.24112317447976428,
                                -0.6201640889866735,
                                -0.9112753490719966,
                                -0.3645642557821037,0.5173255329996493,
                                0.9235886125104533
                              };

     // test

     for(int i = 0; i != ELEMS; ++i) {
          EXPECT_TRUE((mat(i)-L1[i])< pow(10.0, -6.0));
     }
}

//============//
// test # 168 //
//============//

TEST(MatrixTest168, DOT_GENERIC4)
{
     MatrixDense<double> matA;
     MatrixDense<double> matB;
     VectorDense<double> vec;

     // # 1

     const int ELEMS = 200;

     matA.Allocate(ELEMS);
     matB.Allocate(ELEMS, 1);
     vec.Allocate(ELEMS);

     // # 2

     for(int i = 0; i != ELEMS*ELEMS; ++i) {
          matA.SetElement(i, sin(static_cast<double>(i+1)));
     }

     // # 3

     for(int i = 0; i != ELEMS; ++i) {
          vec.SetElement(i, cos(static_cast<double>(i+1)));
     }

     // # 4

     matB.Set(0.0);

     matB.Dot(matA, vec);

     const double L1[ELEMS] = { 0.032115539128716175,
                                -86.86178848311302,-84.66810109520503,
                                4.363277843450885,88.91957147112586,
                                82.27776073182335,-8.750149559686403,
                                -90.80369077171775,-79.72672841857013,
                                13.119931863401371,92.51046662013187,
                                77.01998642954916,-17.46409038476545,
                                -94.0365656108813,-74.16282114645263,
                                21.77414079827998,95.37900720803106,
                                71.16081273403421,-26.041665393024896,
                                -96.53516956631069,-68.01982424181102,
                                30.258329512804472,97.50279465168919,
                                64.74599015327948,-34.41589783408374,
                                -98.27999265141129,-61.34570440500967,
                                38.506250449923876,98.86524566488146,
                                57.825607899016234,-42.521398728504494,
                                -99.25741066818819,-54.19257553279584,
                                46.45350091525942,99.45572174647798,
                                50.45370277236124,-50.29487744815559,
                                -99.45979158981964,-46.616291794495616,
                                54.03802595620268,99.26961224963685,
                                42.68783722529247,-57.67563591190137,
                                -98.88555515423205,-38.67601150283338,
                                61.20060290901283,98.30837038337134,
                                34.58864989259169,-64.60604253776518,
                                -97.53918520334676,-30.43373518482736,
                                67.88530383039686,96.57950186537784,
                                26.219382103859576,-71.03198225077851,
                                -95.43119467165099,-21.9538214596667,
                                74.0399322027431,94.09650631472823,
                                17.64538407276588,-76.90327903269532,
                                -92.57804349747366,-13.302484503767923,
                                79.61643050305877,90.87877184205206,
                                8.933604619384223,-82.17408771415279,
                                -89.00201009794394,-4.547277026981971,
                                84.57125545316768,86.95142366028665,
                                0.1520684100407383,-86.80325195002712,
                                -84.73101741120291,4.243437202942997,
                                88.86571802108293,82.34512789809618,
                                -8.630655203426915,-90.75462558278552,
                                -79.7984148641903,13.001017168926758,
                                92.46628551870148,77.09585214785358,
                                -17.34598759750817,-93.99735488451432,
                                -74.24271796848174,21.657080577983344,
                                95.34484343693592,71.24458461791224,
                                -25.925876363254797,-96.50611947377863,
                                -68.10730757750228,30.144037814437866,
                                97.4789149737808,64.8370140821259,
                                -34.30332668364596,-98.26133002614844,
                                -61.44009115342476,38.39561970363741,
                                98.8518365411613,57.92317312568456,
                                -42.412924452900256,-99.24928123459708,
                                -54.2931286886994,46.347394965189835,
                                99.45288788014388,50.557047472929604,
                                -50.19134705303304,-99.46225882540341,
                                -46.72222620315365,53.93727331527176,
                                99.277375768523,42.796154447655425,
                                -57.577857799335035,-98.8985997939395,
                                -38.78649999078044,61.10599028960435,
                                98.32667066716644,34.70109385742846,
                                -64.51478019396556,-97.56270539000067,
                                -30.547915018730272,67.79757000142706,
                                96.60819601897029,26.335074808779325,
                                -70.94794828452069,-95.46500675128519,
                                -22.07080108285281,73.95976222109087,
                                94.13537028398132,17.763422148062034,
                                -76.82712961101674,-92.6218834533683,
                                -13.421350497816029,79.54445036439806,
                                90.9275021632986,9.053066381865811,
                                -82.10641743876803,-89.05553561216199,
                                -4.667101244017586,84.50802720397148,
                                87.00963982988095,0.2720210598618543,
                                -86.74458921446686,-84.79381053752968,
                                4.123590392939578,88.8117353699646,
                                82.41237534352148,-8.51114829913475,
                                -90.70542844651203,-79.86998529160635,
                                12.88208357238915,92.42196998136278,
                                77.1716057771857,-17.22785959107413,
                                -93.95800749623078,-74.32250684968488,
                                21.539988870650276,95.31054104482266,
                                71.32825291992366,-25.81004964008411,
                                -96.47692907185966,-68.19469189258614,
                                30.029702289923332,97.45489357214673,
                                64.9279437450154,-34.19070565990728,
                                -98.24252453961364,-61.53438857463889,
                                38.284933134301646,98.83828369763705,
                                58.020654138367696,-42.30438851352315,
                                -99.24100750336054,-54.39360290830699,
                                46.241221631055765,99.44990942014246,
                                50.66031866905726,-50.08774368515837,
                                -99.46458145369559,-46.82809268278345,
                                53.83644225541988,99.2849949489173,
                                42.90440944907084,-57.47999597483445,
                                -98.91150064585416,-38.89693208738084,
                                61.01128882874121,98.34482799469151,
                                34.81348737065413,-64.42342405270202,
                                -97.58608373110678,-30.6620504392918,
                                67.7097376021745,96.63674971476749,
                                26.45072922536852,-70.86381116767318,
                                -95.49868003519694,-22.18774861749794,
                                73.87948470999949
                              };

     // test

     for(int i = 0; i != ELEMS; ++i) {
          EXPECT_TRUE((matB(i)-L1[i])< pow(10.0, -6.0));
     }
}

//============//
// test # 169 //
//============//

TEST(MatrixTest169, DOT_GENERIC5)
{
     MatrixDense<double> mat;
     VectorDense<double> vec;

     // # 1

     const int ELEMS = 200;

     mat.Allocate(ELEMS, 1);
     vec.Allocate(ELEMS);

     // # 2

     for(int i = 0; i != ELEMS; ++i) {
          mat.SetElement(i, sin(static_cast<double>(i+1)));
     }

     // # 3

     for(int i = 0; i != ELEMS; ++i) {
          vec.SetElement(i, cos(static_cast<double>(i+1)));
     }

     // # 4

     double tmp_val;

     tmp_val = mat.DotBLAS(vec, pgg::WITH_COLS);

     const double L1[1] = { 0.032115539128716786 };

     // test

     EXPECT_TRUE((tmp_val-L1[0])< pow(10.0, -6.0));
}

//============//
// test # 170 //
//============//

TEST(MatrixTest170, DOT_GENERIC6)
{
     MatrixDense<double> mat;
     VectorDense<double> vec;

     // # 1

     const int ELEMS = 200;

     mat.Allocate(ELEMS, 1);
     vec.Allocate(ELEMS);

     // # 2

     for(int i = 0; i != ELEMS; ++i) {
          mat.SetElement(i, sin(static_cast<double>(i+1)));
     }

     // # 3

     for(int i = 0; i != ELEMS; ++i) {
          vec.SetElement(i, cos(static_cast<double>(i+1)));
     }

     // # 4

     double tmp_val;

     tmp_val = mat.Dot(vec, pgg::WITH_COLS);

     const double L1[1] = { 0.032115539128716786 };

     // test

     EXPECT_TRUE((tmp_val-L1[0])< pow(10.0, -6.0));
}

//============//
// test # 171 //
//============//

TEST(MatrixTest171, DOT_GENERIC7)
{
     MatrixDense<double> matRes;
     MatrixDense<double> mat;
     VectorDense<double> vec;

     // # 1

     const int ELEMS = 200;

     matRes.Allocate(1,1);
     mat.Allocate(ELEMS, 1);
     vec.Allocate(ELEMS);

     // # 2

     for(int i = 0; i != ELEMS; ++i) {
          mat.SetElement(i, sin(static_cast<double>(i+1)));
     }

     // # 3

     for(int i = 0; i != ELEMS; ++i) {
          vec.SetElement(i, cos(static_cast<double>(i+1)));
     }

     // # 4

     matRes.Dot(mat, vec, pgg::WITH_COLS);

     const double L1[1] = { 0.032115539128716786 };

     // test

     EXPECT_TRUE((matRes(0,0)-L1[0]) < pow(10.0, -6.0));
}

//============//
// test # 172 //
//============//

TEST(MatrixTest172, DOT_GENERIC8)
{
     MatrixDense<double> matRes;
     MatrixDense<double> mat;
     VectorDense<double> vec;

     // # 1

     const int ELEMS = 200;

     matRes.Allocate(1,1);
     mat.Allocate(1, ELEMS);
     vec.Allocate(ELEMS);

     // # 2

     for(int i = 0; i != ELEMS; ++i) {
          mat.SetElement(i, sin(static_cast<double>(i+1)));
     }

     // # 3

     for(int i = 0; i != ELEMS; ++i) {
          vec.SetElement(i, cos(static_cast<double>(i+1)));
     }

     // # 4

     matRes.Dot(mat, vec, pgg::WITH_ROWS);

     const double L1[1] = { 0.032115539128716786 };

     // test

     EXPECT_TRUE((matRes(0,0)-L1[0]) < pow(10.0, -6.0));
}

//============//
// test # 173 //
//============//

TEST(MatrixTest173, DOT_GENERIC9)
{
     MatrixDense<double> matRes;
     MatrixDense<double> mat;
     VectorDense<double> vec;

     // # 1

     const int ELEMS = 200;

     matRes.Allocate(1,1);
     mat.Allocate(1, ELEMS);
     vec.Allocate(ELEMS);

     // # 2

     for(int i = 0; i != ELEMS; ++i) {
          mat.SetElement(i, sin(static_cast<double>(i+1)));
     }

     // # 3

     for(int i = 0; i != ELEMS; ++i) {
          vec.SetElement(i, cos(static_cast<double>(i+1)));
     }

     // # 4

     matRes.Dot(mat, vec);

     const double L1[1] = { 0.032115539128716786 };

     // test

     EXPECT_TRUE((matRes(0,0)-L1[0]) < pow(10.0, -6.0));
}

//============//
// test # 174 //
//============//

TEST(MatrixTest174, DotComplex)
{
     MatrixDense<complex<double>> matRes;
     MatrixDense<complex<double>> mat;
     VectorDense<complex<double>> vec;

     // # 1

     const int ELEMS = 200;

     matRes.Allocate(1,1);
     mat.Allocate(1, ELEMS);
     vec.Allocate(ELEMS);

     // # 2

     for(int i = 0; i != ELEMS; ++i) {
          mat.SetElement(i, sin(static_cast<double>(i+1)));
     }

     // # 3

     for(int i = 0; i != ELEMS; ++i) {
          vec.SetElement(i, cos(static_cast<double>(i+1)));
     }

     // # 4

     matRes.DotBLAS(mat, vec);

     const complex<double> L1[1] = { 0.032115539128716786 };

     // test

     EXPECT_TRUE(std::real(matRes(0,0)-L1[0]) < pow(10.0, -6.0));
}

//============//
// test # 175 //
//============//

TEST(MatrixTest175, DotComplex)
{
     MatrixDense<complex<double>> matA;
     MatrixDense<complex<double>> matB;
     MatrixDense<complex<double>> matC;
     MatrixDense<complex<double>> matD;

     const int ELEM = 100;

     // # 1

     matA.Allocate(ELEM);
     matB.Allocate(ELEM);
     matC.Allocate(ELEM);
     matD.Allocate(ELEM);

     matC.CheckCompatibilityDot(matA, matB);
     matD.CheckCompatibilityDot(matB, matA);

     matA = +0.1;
     matB = -0.2;

     matC.Dot(matA, matB);
     matD.Dot(matB, matA);

     EXPECT_EQ(matC, matD);

     // # 2

     matA.Deallocate();
     matB.Deallocate();
     matC.Deallocate();
     matD.Deallocate();

     const int ELEM_B = 100;

     matA.Allocate(1,ELEM_B);
     matB.Allocate(ELEM_B,1);
     matC.Allocate(1,1);
     matD.Allocate(1,1);

     for(int i = 0; i != ELEM_B; ++i) {
          matA.SetElement(i, sin(static_cast<double>(i)));
          matB.SetElement(i, cos(static_cast<double>(i)));
     }

     matC.Dot(matA, matB);

     const double VAL_100 = 0.300642576113028;

     EXPECT_TRUE((std::real(matC(0,0))-VAL_100) < pow(10.0, -14.0));
}

//============//
// test # 176 //
//============//

TEST(MatrixTest176, DotComplex)
{
     MatrixDense<complex<double>> matA;
     MatrixDense<complex<double>> matB;
     MatrixDense<complex<double>> matC;

     // # 1

     const int ELEM_B = 100;

     matA.Allocate(ELEM_B,1);
     matB.Allocate(1, ELEM_B);
     matC.Allocate(ELEM_B, ELEM_B);

     for(int i = 0; i != ELEM_B; ++i) {
          matA.SetElement(i, sin(static_cast<complex<double>>(i)));
          matB.SetElement(i, cos(static_cast<complex<double>>(i)));
     }

     matC.Dot(matA, matB);

     const double c10 = +0.841470984807897;
     const double c11 = +0.454648713412841;
     const double c12 = -0.350175488374015;
     const double c13 = -0.833049961066805;

     const double c20 = +0.909297426825682;
     const double c21 = +0.491295496433882;
     const double c22 = -0.378401247653964;
     const double c23 = -0.900197629735517;

     const double c9999 = -0.0397892958321418;
     const double c9898 = +0.469765027784966;
     const double c9797 = -0.351193164634246;
     const double c9696 = -0.177469178825923;

     EXPECT_TRUE((std::real(matC(1,0))-c10) < pow(10.0, -14.0));
     EXPECT_TRUE((std::real(matC(1,1))-c11) < pow(10.0, -14.0));
     EXPECT_TRUE((std::real(matC(1,2))-c12) < pow(10.0, -14.0));
     EXPECT_TRUE((std::real(matC(1,3))-c13) < pow(10.0, -14.0));

     EXPECT_TRUE((std::real(matC(2,0))-c20) < pow(10.0, -14.0));
     EXPECT_TRUE((std::real(matC(2,1))-c21) < pow(10.0, -14.0));
     EXPECT_TRUE((std::real(matC(2,2))-c22) < pow(10.0, -14.0));
     EXPECT_TRUE((std::real(matC(2,3))-c23) < pow(10.0, -14.0));

     EXPECT_TRUE((std::real(matC(99,99))-c9999) < pow(10.0, -14.0));
     EXPECT_TRUE((std::real(matC(98,98))-c9898) < pow(10.0, -14.0));
     EXPECT_TRUE((std::real(matC(97,97))-c9797) < pow(10.0, -14.0));
     EXPECT_TRUE((std::real(matC(96,96))-c9696) < pow(10.0, -14.0));
}

//============//
// test # 177 //
//============//

TEST(MatrixTest177, DotComplex)
{
     MatrixDense<std::complex<double>> matA;
     MatrixDense<std::complex<double>> matB;
     MatrixDense<std::complex<double>> matC;

     // # 1

     const int ELEM_B = 100;

     matA.Allocate(3,ELEM_B);
     matB.Allocate(ELEM_B,3);
     matC.Allocate(3, 3);

     for(int i = 0; i != 3*ELEM_B; ++i) {
          matA.SetElement(i, sin(static_cast<complex<double>>(i+1)));
          matB.SetElement(i, cos(static_cast<complex<double>>(i+1)));
     }

     matC.Dot(matA, matB);

     const double c00 = +0.11871865092994290369;
     const double c01 = -0.12263594200048068732;
     const double c02 = -0.25123961542028522069;

     const double c10 = +0.22690435834954134246;
     const double c11 = +0.26832096951487606806;
     const double c12 = +0.063044518733783381564;

     const double c20 = +0.27260916988833111755;
     const double c21 = +0.58539241368689258801;
     const double c22 = +0.35996857201715691880;

     EXPECT_TRUE((std::real(matC(0,0))-c00) < pow(10.0, -14.0));
     EXPECT_TRUE((std::real(matC(0,1))-c01) < pow(10.0, -14.0));
     EXPECT_TRUE((std::real(matC(0,2))-c02) < pow(10.0, -14.0));

     EXPECT_TRUE((std::real(matC(1,0))-c10) < pow(10.0, -14.0));
     EXPECT_TRUE((std::real(matC(1,1))-c11) < pow(10.0, -14.0));
     EXPECT_TRUE((std::real(matC(1,2))-c12) < pow(10.0, -14.0));

     EXPECT_TRUE((std::real(matC(2,0))-c20) < pow(10.0, -14.0));
     EXPECT_TRUE((std::real(matC(2,1))-c21) < pow(10.0, -14.0));
     EXPECT_TRUE((std::real(matC(2,2))-c22) < pow(10.0, -14.0));

     EXPECT_TRUE(std::imag(matC(0,0)) < pow(10.0, -14.0));
     EXPECT_TRUE(std::imag(matC(0,1)) < pow(10.0, -14.0));
     EXPECT_TRUE(std::imag(matC(0,2)) < pow(10.0, -14.0));

     EXPECT_TRUE(std::imag(matC(1,0)) < pow(10.0, -14.0));
     EXPECT_TRUE(std::imag(matC(1,1)) < pow(10.0, -14.0));
     EXPECT_TRUE(std::imag(matC(1,2)) < pow(10.0, -14.0));

     EXPECT_TRUE(std::imag(matC(2,0)) < pow(10.0, -14.0));
     EXPECT_TRUE(std::imag(matC(2,1)) < pow(10.0, -14.0));
     EXPECT_TRUE(std::imag(matC(2,2)) < pow(10.0, -14.0));
}

//============//
// test # 178 //
//============//

TEST(MatrixTest178, DotComplex)
{
     MatrixDense<complex<double>> matA;
     MatrixDense<complex<double>> matB;
     MatrixDense<complex<double>> matC;

     // # 1

     const int ELEM_B = 100;
     const int ELEM_A = 4;

     matA.Allocate(ELEM_A, ELEM_B);
     matB.Allocate(ELEM_B, ELEM_A);
     matC.Allocate(ELEM_A, ELEM_A);

     for(int i = 0; i != ELEM_A*ELEM_B; ++i) {
          matA.SetElement(i, sin(static_cast<complex<double>>(i+1)));
          matB.SetElement(i, cos(static_cast<complex<double>>(i+1)));
     }

     matC.Dot(matA, matB);

     const double L1[ELEM_A*ELEM_A] = { +0.51622730506406953160, +0.25199009694310594293,
                                        -0.24392564419547711885, -0.51557727298148133836,
                                        +0.22052996343464981636, +0.54432798041615115680,
                                        +0.36767336250013832031, -0.14701844928591686420,
                                        -0.13589300631484672594, +0.68677848351106999898,
                                        +0.87802900283815731006, +0.26202370619404957496,
                                        -0.45489617134905329685, +0.64011611440927207726,
                                        +1.1466085966264205756,  +0.59891442296170137064
                                      };

     for(int i = 0; i != ELEM_A*ELEM_A; ++i) {
          EXPECT_TRUE((std::real(matC(i))-L1[i])< pow(10.0, -14.0));
     }

     for(int i = 0; i != ELEM_A*ELEM_A; ++i) {
          EXPECT_TRUE(std::imag(matC(i))< pow(10.0, -14.0));
     }
}

//============//
// test # 179 //
//============//

TEST(MatrixTest179, DotComplex)
{
     MatrixDense<complex<double>> matA;
     MatrixDense<complex<double>> matB;
     MatrixDense<complex<double>> matC;

     // # 1

     const int ELEM_B = 100;
     const int ELEM_A = 5;

     matA.Allocate(ELEM_A, ELEM_B);
     matB.Allocate(ELEM_B, ELEM_A);
     matC.Allocate(ELEM_A, ELEM_A);

     for(int i = 0; i != ELEM_A*ELEM_B; ++i) {
          matA.SetElement(i, sin(static_cast<double>(i+1)));
          matB.SetElement(i, cos(static_cast<double>(i+1)));
     }

     matC.Dot(matA, matB);

     const double L1[ELEM_A*ELEM_A] = {
          1.8093247822323773844, 3.1179100461293086970, 1.5599031925938293423,
          -1.4322714623502710185, -3.1076223400677971249,
          -0.21252966270910540710, 2.5343188543725372619,
          2.9511263043542735377, 0.65468183992893370798,
          -2.2436740889070750855, -2.1758614603823726009,
          1.2528719069105741384, 3.5297206209047655991, 2.5613604741797592636,
          -0.76190268018709425371, -3.5400431388332158227,
          -0.37356867459644443184, 3.1363631062640884729,
          3.7627371113049410746, 0.92966796896327703299,
          -3.9294305542744506020, -1.8971425433105955857,
          1.8793695728519267674, 3.9279979708912292377, 2.3652431493838834522
     };

     for(int i = 0; i != ELEM_A*ELEM_A; ++i) {
          EXPECT_TRUE((std::real(matC(i))-L1[i])< pow(10.0, -14.0));
     }

     for(int i = 0; i != ELEM_A*ELEM_A; ++i) {
          EXPECT_TRUE(std::imag(matC(i))< pow(10.0, -14.0));
     }
}

//============//
// test # 180 //
//============//

TEST(MatrixTest180, DotComplex)
{
     MatrixDense<complex<double>> matA;
     MatrixDense<complex<double>> matB;
     MatrixDense<complex<double>> matC;

     // # 1

     const int ELEM_B = 100;
     const int ELEM_A = 10;

     matA.Allocate(ELEM_A, ELEM_B);
     matB.Allocate(ELEM_B, ELEM_A);
     matC.Allocate(ELEM_A, ELEM_A);

     for(int i = 0; i != ELEM_A*ELEM_B; ++i) {
          matA.SetElement(i, sin(static_cast<double>(i+1)));
          matB.SetElement(i, cos(static_cast<double>(i+1)));
     }

     matC.Dot(matA, matB);

     const double L1[ELEM_A*ELEM_A] = { 0.17785030569806217747, -0.015946366947653687898,
                                        -0.19508202336213573028, -0.19486016716431478695,
                                        -0.015484771919325156901, 0.17812725121660758328,
                                        0.20796990105989810302, 0.046605982971056055241,
                                        -0.15760726092687242339, -0.21691711597175751476,
                                        -0.070102298261901270629, -0.22838940757629300761,
                                        -0.17669634883675771160, 0.037450518146330250306,
                                        0.21716555145759539915, 0.19721957826899963847,
                                        -0.0040491656554302718384, -0.20159512534994174481,
                                        -0.21379545650127011113, -0.029433230913593862516,
                                        -0.29875137526201731590, -0.37794262581962871918,
                                        -0.10965516917039251528, 0.25944874431338067651,
                                        0.39001667878461847814, 0.16200507743534518666,
                                        -0.21495324498329140821, -0.39428454527196815405,
                                        -0.21111245297393923025, 0.16615545499337100145,
                                        -0.44513559975877362386, -0.42342471019616366048,
                                        -0.012419094802298448472, 0.41000457907920989957,
                                        0.45547193378828473993, 0.082180493088851906161,
                                        -0.36666731396170993043, -0.47840288352882993712,
                                        -0.15029704824747785574, 0.31599120006225527135,
                                        -0.46894628153615769206, -0.35231161137056174654,
                                        0.088236729520888844995, 0.44766062821536094260,
                                        0.39550740982139020493, -0.020273497186496028556,
                                        -0.41741504437714029937, -0.43078712477554551489,
                                        -0.048095509331926224810, 0.37881489558766077242,
                                        -0.36362685755675085491, -0.18418519262567547886,
                                        0.16459548899191096594, 0.36204783710132248396,
                                        0.22663507344892336130, -0.11714493155122548297,
                                        -0.35322242672470840489, -0.26454885173617445569,
                                        0.067349717509061053537, 0.33732726707560147636,
                                        -0.15817832194754566417, 0.034658876176437110572,
                                        0.19563086338140027317, 0.17674073699145405777,
                                        -0.0046440079067661808074, -0.18175907335244533837,
                                        -0.19176568498279894776, -0.025463810212733790418,
                                        0.16424937423454144313, 0.20295244148537730576,
                                        0.090826553152419290416, 0.24395919866412292689,
                                        0.17279688199951912556, -0.057234091081792869413,
                                        -0.23464430477103876263, -0.19632362677144469284,
                                        0.022496088289023535457, 0.22063300352259003311,
                                        0.21592095281871404012, 0.012692173863803962834,
                                        0.31482122372388883760, 0.38608236597606995186,
                                        0.10238116145990637425, -0.27544881074757848853,
                                        -0.40003241665101334073, -0.15682806352951534692,
                                        0.23056328795134884427, 0.40597581578682265905,
                                        0.20813605084128995833, -0.18106303937914800702,
                                        0.45212601207520564973, 0.42189302221316810074,
                                        0.0037735333877003807885, -0.41781532463187824354,
                                        -0.45526670003899871942, -0.074147971000221161078,
                                        0.37514206062527182456, 0.47952821176814082220,
                                        0.14303833646903237153, -0.32496032572461879728
                                      };

     // test

     for(int i = 0; i != ELEM_A*ELEM_A; ++i) {
          EXPECT_TRUE((std::real(matC(i))-L1[i])< pow(10.0, -14.0));
     }

     for(int i = 0; i != ELEM_A*ELEM_A; ++i) {
          EXPECT_TRUE(std::imag(matC(i))< pow(10.0, -14.0));
     }
}

//============//
// test # 181 //
//============//

TEST(MatrixTest181, DotComplex)
{
     MatrixDense<complex<double>> matA;
     MatrixDense<complex<double>> matB;
     MatrixDense<complex<double>> matC;

     // # 1

     const int ELEM_B = 100;
     const int ELEM_A = 10;

     matA.Allocate(ELEM_A, ELEM_B);
     matB.Allocate(ELEM_B, ELEM_A);
     matC.Allocate(ELEM_A, ELEM_A);

     for(int i = 0; i != ELEM_A*ELEM_B; ++i) {
          matA.SetElement(i, sin(static_cast<double>(i+1)));
          matB.SetElement(i, cos(static_cast<double>(i+1)));
     }

     matC.Dot(matA, matB);

     const double L1[ELEM_A*ELEM_A] = { 0.17785030569806217747, -0.015946366947653687898,
                                        -0.19508202336213573028, -0.19486016716431478695,
                                        -0.015484771919325156901, 0.17812725121660758328,
                                        0.20796990105989810302, 0.046605982971056055241,
                                        -0.15760726092687242339, -0.21691711597175751476,
                                        -0.070102298261901270629, -0.22838940757629300761,
                                        -0.17669634883675771160, 0.037450518146330250306,
                                        0.21716555145759539915, 0.19721957826899963847,
                                        -0.0040491656554302718384, -0.20159512534994174481,
                                        -0.21379545650127011113, -0.029433230913593862516,
                                        -0.29875137526201731590, -0.37794262581962871918,
                                        -0.10965516917039251528, 0.25944874431338067651,
                                        0.39001667878461847814, 0.16200507743534518666,
                                        -0.21495324498329140821, -0.39428454527196815405,
                                        -0.21111245297393923025, 0.16615545499337100145,
                                        -0.44513559975877362386, -0.42342471019616366048,
                                        -0.012419094802298448472, 0.41000457907920989957,
                                        0.45547193378828473993, 0.082180493088851906161,
                                        -0.36666731396170993043, -0.47840288352882993712,
                                        -0.15029704824747785574, 0.31599120006225527135,
                                        -0.46894628153615769206, -0.35231161137056174654,
                                        0.088236729520888844995, 0.44766062821536094260,
                                        0.39550740982139020493, -0.020273497186496028556,
                                        -0.41741504437714029937, -0.43078712477554551489,
                                        -0.048095509331926224810, 0.37881489558766077242,
                                        -0.36362685755675085491, -0.18418519262567547886,
                                        0.16459548899191096594, 0.36204783710132248396,
                                        0.22663507344892336130, -0.11714493155122548297,
                                        -0.35322242672470840489, -0.26454885173617445569,
                                        0.067349717509061053537, 0.33732726707560147636,
                                        -0.15817832194754566417, 0.034658876176437110572,
                                        0.19563086338140027317, 0.17674073699145405777,
                                        -0.0046440079067661808074, -0.18175907335244533837,
                                        -0.19176568498279894776, -0.025463810212733790418,
                                        0.16424937423454144313, 0.20295244148537730576,
                                        0.090826553152419290416, 0.24395919866412292689,
                                        0.17279688199951912556, -0.057234091081792869413,
                                        -0.23464430477103876263, -0.19632362677144469284,
                                        0.022496088289023535457, 0.22063300352259003311,
                                        0.21592095281871404012, 0.012692173863803962834,
                                        0.31482122372388883760, 0.38608236597606995186,
                                        0.10238116145990637425, -0.27544881074757848853,
                                        -0.40003241665101334073, -0.15682806352951534692,
                                        0.23056328795134884427, 0.40597581578682265905,
                                        0.20813605084128995833, -0.18106303937914800702,
                                        0.45212601207520564973, 0.42189302221316810074,
                                        0.0037735333877003807885, -0.41781532463187824354,
                                        -0.45526670003899871942, -0.074147971000221161078,
                                        0.37514206062527182456, 0.47952821176814082220,
                                        0.14303833646903237153, -0.32496032572461879728
                                      };

     // test

     for(int i = 0; i != ELEM_A*ELEM_A; ++i) {
          EXPECT_TRUE((std::real(matC(i))-L1[i])< pow(10.0, -14.0));
     }

     for(int i = 0; i != ELEM_A*ELEM_A; ++i) {
          EXPECT_TRUE(std::imag(matC(i))< pow(10.0, -14.0));
     }
}

//============//
// test # 182 //
//============//

TEST(MatrixTest182, DotComplex)
{
     MatrixDense<complex<double>> matA;
     MatrixDense<complex<double>> matB;
     MatrixDense<complex<double>> matC;

     // # 1

     const int ELEM_B = 100;
     const int ELEM_A = 20;

     matA.Allocate(ELEM_A, ELEM_B);
     matB.Allocate(ELEM_B, ELEM_A);
     matC.Allocate(ELEM_A, ELEM_A);

     for(int i = 0; i != ELEM_A*ELEM_B; ++i) {
          matA.SetElement(i, sin(static_cast<double>(i+1)));
          matB.SetElement(i, cos(static_cast<double>(i+1)));
     }

     matC.Dot(matA, matB);

     const double L1[ELEM_A*ELEM_A] = {
          -5.4076380585206169363, -5.0363912418165668001,
          -0.034709543894572461144, 4.9988839486128289774,
          5.4365265923000579562, 0.87585175885353347230,
          -4.4900771424855979744, -5.7278498260751261052,
          -1.6994637949040293599, 3.8914014118229934341, 5.9045301066370248127,
          2.4890610515446813715, -3.2148392554446889859,
          -5.9630311769690391751, -3.2288397343152689756,
          2.4739320695106162977, 5.9021821377507187662, 3.9039931678503028981,
          -1.6835091163847546136, -5.7232008829157375244,
          -5.9124061181381509048, -2.3994197522141690530,
          3.3195820684043989476, 5.9865754443690199776, 3.1495389652879304103,
          -2.5831691136357726643, -5.9409234223774636209,
          -3.8366201345573948465, 1.7950540114944778014, 5.7763637776940359127,
          4.4469113257480942106, -0.97101089110835292680,
          -5.4961901727259346340, -4.9681975565189102719,
          0.12753298113468539657, 5.1060102840815272347, 5.3900452794166831993,
          0.71849750232350351260, -4.6136335648849071550,
          -5.7040112093994253821, -4.7891206940787696247,
          0.89826137206833425203, 5.7597860752803695999, 5.3257900234940382915,
          -0.0047128147536438227307, -5.3308827128510845840,
          -5.7558636293784464459, -0.88893006958038372929,
          4.7952816966788320236, 6.0707335855861002975, 1.7647810125278273885,
          -4.1637030847439092891, -6.2640977678026684745,
          -2.6053098515105879665, 3.4487879272583657376, 6.3320859906063821967,
          3.3936933961015782445, -2.6648452559600606658,
          -6.2733374692555657906, -4.1141521442955038312,
          -2.3470921941970786634, 3.9485952189772763820, 6.6139623977037476820,
          3.1984830498317336712, -3.1576668634952562107,
          -6.6106724248515402826, -3.9858562454771713658,
          2.3035377842710570259, 6.4750697984692463596, 4.6934525012691120636,
          -1.4033033806326679569, -6.2098686060458845751,
          -5.3071092733368559399, 0.47498185028970327397,
          5.8203768512509403518, 5.8145442172151477667, 0.46284644501626323377,
          -5.3143902142048314273, -6.2056010190521579959,
          -1.3914108655782857634, 0.74123690596826855507,
          5.9116349806297164882, 5.6469031170017140280, 0.19043456963030682071,
          -5.4411186428251908549, -6.0701324680714538845,
          -1.1182944960229461929, 4.8616982783897598271, 6.3718680765212507779,
          2.0237717504742800290, -4.1849709898571408211,
          -6.5460707020964487066, -2.8887431995800262547,
          3.4244814785084568620, 6.5892536780617374297, 3.6958964339052981211,
          -2.5954509470840030053, -6.5005526968595659293,
          -4.4290762759771524229, 1.7144724473069087133, 3.6254573399022173782,
          6.2468336007688063780, 3.1248998578377030517, -2.8700524031753404695,
          -6.2262917206337675658, -3.8581071441569231534,
          2.0572033480851097512, 6.0811305693770072894, 4.5140943897541511016,
          -1.2031793539958055866, -5.8142555484278961079,
          -5.0797320054486295376, 0.32507371695572769603,
          5.4310081631452428614, 5.5436987505164016235, 0.55953827273943074680,
          -4.9390591125512206338, -5.8967083274003753892,
          -1.4329511001013477294, 4.3482547602382840394, 5.5113636637749044656,
          4.8618900313378217410, -0.25758287415661014924,
          -5.1402352730557403774, -5.2969790673169164842,
          -0.58370473535745572756, 4.6662250383973452991,
          5.6260490312489246419, 1.4133094906246699586, -4.0988202778692543013,
          -5.8425135855683644893, -2.2146268468277860221,
          3.4493776016112839878, 5.9420401907487663199, 2.9716184316341544332,
          -2.7308956092043692349, -5.9226368211107521455,
          -3.6691330533270073664, 1.9577547226115718839, 5.7846918352295524466,
          5.8796484587251699453, 2.1381654572511191141, -3.5691370049643999913,
          -5.9949913647342618123, -2.9090783110866149664,
          2.8514279258720787246, 5.9903444778175971739, 3.6217659427465714449,
          -2.0766474974562582848, -5.8658008054484077640,
          -4.2619639044376735693, 1.2603029552594979682, 5.6238530900759489228,
          4.8168586296038981526, -0.41873344084428152626,
          -5.2693439168684293240, -5.2753438965482534425,
          -0.43121702623644755247, 4.8093687893379440127,
          5.6282431195355568238, 4.6288999929769105443, -1.1743291796152915296,
          -5.8978855201656762020, -5.1989531129683634755,
          0.27987281009137647368, 5.5013849622526968187, 5.6649491510555052664,
          0.62018521562940324826, -4.9947741469157132292,
          -6.0175611933676638779, -1.5078302300426535299,
          4.3881928930681973572, 6.2497317074805137147, 2.3652960121496978848,
          -3.6937819286301193472, -6.3568137989755334355,
          -3.1754203784916604305, 2.9254398937760832278, 6.3366642190633873995,
          3.9219886843680828471, 2.1035271850274652211, -4.1634578849718775623,
          -6.6025789762978521386, -2.9713194061685842850,
          3.3917575230505767439, 6.6364682274682576127, 3.7796406491929143641,
          -2.5521711112444892414, -6.5375285219438144900,
          -4.5123123589254591846, 1.6615029772943534966, 6.3077401386032963503,
          5.1546701061144069834, -0.73757984995693127810,
          -5.9517022935016199490, -5.6938570960823105755,
          -0.20110594309226477900, 5.4765410865292354868,
          6.1190814975589712082, 1.1357665993231279117, -1.0010776129381703953,
          -6.0061274367571387805, -5.4891713938973927725,
          0.074503513900754878133, 5.5696802346091064953,
          5.9441186335142483985, 0.85356177347394176614,
          -5.0217558446965094575, -6.2800942982666053366,
          -1.7645430161490978569, 4.3733209774088459943, 6.4903738329401099680,
          2.6402069183587093014, -3.6373540610236580873,
          -6.5707484912185586028, -3.4630270611463163330,
          2.8285854783763146204, 6.5196095737700320223, 4.2165346937595810886,
          -1.9632027381474079914, -3.8300234215500445151,
          -6.1949361911891896794, -2.8642531960609755433,
          3.0998109783453217458, 6.2139232347716784472, 3.6149831261241734036,
          -2.3075557973330643758, -6.1085385625610701491,
          -4.2933591443393313167, 1.4691148713478621354, 5.8808914494881819924,
          4.8858035500895203481, -0.60126960122396016496,
          -5.5355382540689653882, -5.3804585645655563226,
          -0.27861008405653697663, 5.0793912228578300057,
          5.7674236642894879770, 1.1529133866103421768, -4.5215801417858601437,
          -5.6043253424747317716, -4.6778933438037052154,
          0.54937222195000296975, 5.2715475004026853050, 5.1470863179719951418,
          0.29041771180255861911, -4.8332595992682525876,
          -5.5132603244904742372, -1.1243949330788102943,
          4.2982339743926065410, 5.7690863881290181281, 1.9358673821246057763,
          -3.6771791672953308077, -5.9094441484845120449,
          -2.7085934323550037677, 2.9825255941630967431, 5.9315243440291330531,
          3.4271069666207941439, -2.2281767509851714322,
          -5.8348850394989299051, -5.8354075969621529895,
          -1.8727352346325583830, 3.8117212658571870700, 5.9916988131710835776,
          2.6629361037902779915, -3.1141177786562695136,
          -6.0280661368961810899, -3.3998382887250421287,
          2.3541852029423202409, 5.9437816759058222986, 4.0686926871951031497,
          -1.5471335943851184263, -5.7405323842597884794,
          -4.6561121738674687657, 0.70911609641716838851,
          5.4223862979122891046, 5.1503395437224624102, 0.14309436504192896842,
          -4.9957111129446793204, -5.5414828325921305846,
          -4.4596388542280430427, 1.4481034725601873771, 6.0244661449479023038,
          5.0619624269196007353, -0.55448620198281238823,
          -5.6611427739263619322, -5.5629707872195278921,
          -0.35022911369726076426, 5.1845115917939581583,
          5.9526362493900109765, 1.2479345912854372936, -4.6041123749017384444,
          -6.2231596565563297857, -2.1206626495441923779,
          3.9315618175219981569, 6.3691264808847322930, 2.9509456304537037640,
          -3.1803210236334374027, -6.3876151953938411609,
          -3.7221654145058830226, -1.8558538982143769258,
          4.3701891414605180532, 6.5783004386364243996, 2.7383526499167920345,
          -3.6192239365760774195, -6.6493027266872330007,
          -3.5660432547127641693, 2.7958199399935682467, 6.5872191754540620522,
          4.3223594795197432371, -1.9164575883030030501,
          -6.3932923876369563081, -4.9921636499559410361,
          0.99873732495234904701, 6.0714038092126045248, 5.5620496309960054469,
          -0.061027327252249757645, -5.6279960422667256611,
          -6.0206111508547026287, -0.87790413281773476580,
          1.2589631729501956920, 6.0888896717360437532, 5.3207190866811738087,
          -0.33929608891512260672, -5.6873640051069380178,
          -5.8064956836263513186, -0.58716200864649777830,
          5.1720057092466086385, 6.1760552299847498495, 1.5018680546128799627,
          -4.5531296839506773795, -6.4220009889233303858,
          -2.3865142012548206193, 3.8431227370732479114, 6.5394103544047251938,
          3.2233942499324756529, -3.0561956624834866681,
          -6.5259333771805450729, -3.9957580407815230556,
          2.2080988109297107435, 4.0271093053146512053, 6.1309398089705844790,
          2.5980125265365064434, -3.3235154914466125618,
          -6.1894186937706831456, -3.3647988930087263196,
          2.5534014924203255942, 6.1240163213324287145, 4.0642387867597418143,
          -1.7321811451623905240, -5.9360417205848507128,
          -4.6823329135605582160, 0.87629118050674164573,
          5.6292572044399717301, 5.2067101152607677913,
          -0.0028622419152510065172, -5.2098030670742929101,
          -5.6268749786030430493, -0.87062398446763407661,
          4.6860746858991027507, 5.6863415365261408689, 4.4847605325343217930,
          -0.84008862253670027357, -5.3925641723146586015,
          -4.9871410911503524550, 0.0034365098380870204591,
          4.9908545995296663843, 5.3897039869188925094, 0.83328438462830074963,
          -4.4892530380017230425, -5.6843919207440655848,
          -1.6533270864707616499, 3.8977990463952540964, 5.8653067116267454339,
          2.4402784354363590409, -3.2283305803736232300,
          -5.9288273487973558211, -3.1783875949249784243,
          2.4942470558360425148, 5.8736824662710432383, 5.7797699371250241877,
          1.6036474808200339676, -4.0468610737516287603,
          -5.9767042201720756588, -2.4115930695499998153,
          3.3707256275850957812, 6.0540147276161190712, 3.1712706065962380671,
          -2.6271250850645158709, -6.0101540891250193224,
          -3.8674751408896378202, 1.8309426161042596409, 5.8460001756763890395,
          4.4862721339427461042, -0.99811381823389807818,
          -5.5648385289640029219, -5.0152763597323366991,
          0.14530776550530031552, 5.1722966012584580845, 5.4438997950824735115
     };

     // test

     for(int i = 0; i != ELEM_A*ELEM_A; ++i) {
          EXPECT_TRUE((std::real(matC(i))-L1[i])< pow(10.0, -14.0));
     }

     for(int i = 0; i != ELEM_A*ELEM_A; ++i) {
          EXPECT_TRUE(std::imag(matC(i))< pow(10.0, -14.0));
     }
}

//============//
// test # 183 //
//============//

TEST(MatrixTest183, DotComplex)
{
     MatrixDense<complex<double>> matA;
     MatrixDense<complex<double>> matB;
     MatrixDense<complex<double>> matC;

     // # 1

     const int ELEM_B = 100;
     const int ELEM_A = 5;

     matA.Allocate(ELEM_A, ELEM_B);
     matB.Allocate(ELEM_B, ELEM_A+1);
     matC.Allocate(ELEM_A, ELEM_A+1);

     for(int i = 0; i != ELEM_A*ELEM_B; ++i) {
          matA.SetElement(i, sin(static_cast<double>(i+1)));
     }

     for(int i = 0; i != (ELEM_A+1)*ELEM_B; ++i) {
          matB.SetElement(i, cos(static_cast<double>(i+1)));
     }
     matC.Dot(matA, matB);

     const double L1[ELEM_A*(ELEM_A+1)] = {
          0.80750538509088871410, -1.2149727108840668650,
          -2.1204104996059406757, -1.0763526537641409564,
          0.95729885809382712825, 2.1108142146302046840, 1.0545024923016520141,
          -0.94770423869733807185, -2.0785960632000153092,
          -1.2984362531334734211, 0.67549986001840882130,
          2.0283845170965771934, 1.0111294148813366339,
          -0.41947378986742633355, -1.4644147267145725336,
          -1.1629795173148640744, 0.20769369694929817422,
          1.3874142840668329125, 0.68932946125310780189,
          0.22426390783189812063, -0.44698884820395877655,
          -0.70728211859578373954, -0.31730347094925125608,
          0.36440252456809426497, 0.17771419244357664113,
          0.80624779006028928611, 0.69352088769775559661,
          -0.056825920478656246841, -0.75492723943315062065,
          -0.75895193597814474125
     };

     // test

     for(int i = 0; i != ELEM_A*(ELEM_A+1); ++i) {
          EXPECT_TRUE((std::real(matC(i))-L1[i])< pow(10.0, -14.0));
     }

     for(int i = 0; i != ELEM_A*(ELEM_A+1); ++i) {
          EXPECT_TRUE(std::imag(matC(i))< pow(10.0, -14.0));
     }
}

//============//
// test # 184 //
//============//

TEST(MatrixTest184, DotComplex)
{
     MatrixDense<complex<double>> matA;
     MatrixDense<complex<double>> matB;
     MatrixDense<complex<double>> matC;

     // # 1

     const int ELEM_B = 1000;
     const int ELEM_A = 2;

     matA.Allocate(ELEM_A, ELEM_B);
     matB.Allocate(ELEM_B, ELEM_A+1);
     matC.Allocate(ELEM_A, ELEM_A+1);

     for(int i = 0; i != ELEM_A*ELEM_B; ++i) {
          matA.SetElement(i, sin(static_cast<double>(i+1)));
     }

     for(int i = 0; i != (ELEM_A+1)*ELEM_B; ++i) {
          matB.SetElement(i, cos(static_cast<double>(i+1)));
     }

     matC.Dot(matA, matB);

     const double L1[ELEM_A*(ELEM_A+1)] = {
          0.48862832217863019005, -0.30741794197760175290,
          -0.82082556801010261152, 0.52553473998709032096,
          -0.35930301234050287457, -0.91379923213297504812
     };

     // test

     for(int i = 0; i != ELEM_A*(ELEM_A+1); ++i) {
          EXPECT_TRUE((std::real(matC(i))-L1[i])< pow(10.0, -14.0));
     }
}

//============//
// test # 185 //
//============//

TEST(MatrixTest185, DotComplex)
{
     MatrixDense<complex<double>> matA;
     MatrixDense<complex<double>> matB;
     MatrixDense<complex<double>> matC;

     // # 1

     const int ELEM_B = 5000;
     const int ELEM_A = 3;

     matA.Allocate(ELEM_A, ELEM_B);
     matB.Allocate(ELEM_B, ELEM_A+1);
     matC.Allocate(ELEM_A, ELEM_A+1);

     for(int i = 0; i != ELEM_A*ELEM_B; ++i) {
          matA.SetElement(i, sin(static_cast<double>(i+1)));
     }

     for(int i = 0; i != (ELEM_A+1)*ELEM_B; ++i) {
          matB.SetElement(i, cos(static_cast<double>(i+1)));
     }

     matC.Dot(matA, matB);

     const double L1[ELEM_A*(ELEM_A+1)] = {
          0.45023765068265543713, -0.24456853165489390390,
          -0.71451953387450403066, -0.52754457182555188552,
          -0.10478093662647744556, -0.086417901817094359213,
          0.011397353386352278471, 0.098733934447974732708,
          -0.48265025161494171030, 0.21783629337582535372,
          0.71804515484039572643, 0.55808661237959711809
     };

     // test

     for(int i = 0; i != ELEM_A*(ELEM_A+1); ++i) {
          EXPECT_TRUE((std::real(matC(i))-L1[i])< pow(10.0, -14.0));
     }
}

//============//
// test # 186 //
//============//

TEST(MatrixTest186, DotComplex)
{
     MatrixDense<complex<double>> matA;
     MatrixDense<complex<double>> matB;

     // # 1

     const int ELEMS = 10;

     matA.Allocate(ELEMS);
     matB.Allocate(ELEMS);

     // # 2

     for(int i = 0; i != ELEMS*ELEMS; ++i) {
          matA.SetElement(i, sin(static_cast<double>(i+1)));
     }

     for(int i = 0; i != ELEMS*ELEMS; ++i) {
          matB.SetElement(i, cos(static_cast<double>(i+1)));
     }

     matA.Dot(matA, matB);

     const double L1[ELEMS] = {
          0.81425238926196575632, 0.29148629494075228957,
          -0.49927095469106749591, -0.83100079108589484249,
          -0.39871233251284696073, 0.40015040581638350916,
          0.83111670642617471470, 0.49795814003880835750,
          -0.29302084384861834674, -0.81459781523648155026
     };

     // test

     for(int i = 0; i != ELEMS; ++i) {
          EXPECT_TRUE((std::real(matA(i))-L1[i])< pow(10.0, -10.0));
     }

     // # 3

     for(int i = 0; i != ELEMS*ELEMS; ++i) {
          matA.SetElement(i, sin(static_cast<double>(i+1)));
     }

     for(int i = 0; i != ELEMS*ELEMS; ++i) {
          matB.SetElement(i, cos(static_cast<double>(i+1)));
     }

     matA.Dot(matA, matB);

     // test

     for(int i = 0; i != ELEMS; ++i) {
          EXPECT_TRUE((std::real(matA(i))-L1[i])< pow(10.0, -10.0));
     }
}

//============//
// test # 187 //
//============//

TEST(MatrixTest187, DotComplex)
{
     MatrixDense<complex<double>> matA;
     MatrixDense<complex<double>> matB;

     // # 1

     const int ELEMS = 200;

     matA.Allocate(ELEMS);
     matB.Allocate(ELEMS);

     // # 2

     for(int i = 0; i != ELEMS*ELEMS; ++i) {
          matA.SetElement(i, sin(static_cast<double>(i+1)));
     }

     for(int i = 0; i != ELEMS*ELEMS; ++i) {
          matB.SetElement(i, cos(static_cast<double>(i+1)));
     }

     matA.Dot(matA, matB);

     const double L1[ELEMS] = {
          -0.804158, -0.0518092, 0.748173, 0.860288, 0.181459, -0.664203,
          -0.899199, -0.307476, 0.566939, 0.920113, 0.427339, -0.458329,
          -0.922611, -0.538649, 0.340544, 0.906643, 0.639178, -0.215944,
          -0.872528, -0.726914, 0.0870214, 0.82095, 0.800101, 0.0436428,
          -0.75294, -0.857274, -0.173433, 0.669861, 0.897288, 0.299753,
          -0.573374, -0.919343, -0.420073, 0.465411, 0.922998, 0.531985,
          -0.348132, -0.908178, -0.633249, 0.223886, 0.875182, 0.721839,
          -0.0951589, -0.824668, -0.795981, -0.035473, 0.757649, 0.854192,
          0.165395, -0.675466, -0.895306, -0.292006, 0.579763, 0.918501,
          0.412773, -0.472456, -0.923312, -0.525279, 0.355693, 0.909642,
          0.62727, -0.231811, -0.877766, -0.716707, 0.103289, 0.828322, 0.7918,
          0.0273004, -0.762299, -0.851044, -0.157343, 0.681018, 0.893254,
          0.284237, -0.586107, -0.917587, -0.405441, 0.479465, 0.923553,
          0.518531, -0.363226, -0.911035, -0.621242, 0.239717, 0.880282,
          0.71152, -0.111411, -0.831911, -0.787556, -0.0191256, 0.766888,
          0.847829, 0.149279, -0.686517, -0.891133, -0.276445, 0.592405,
          0.9166, 0.398078, -0.486436, -0.923722, -0.511743, 0.37073, 0.912356,
          0.615166, -0.247605, -0.882729, -0.706276, 0.119524, 0.835434,
          0.78325, 0.0109494, -0.771418, -0.844547, -0.141204, 0.691962,
          0.888941, 0.268632, -0.598656, -0.915542, -0.390683, 0.493368,
          0.923819, 0.504915, -0.378206, -0.913606, -0.609041, 0.255474,
          0.885107, 0.700977, -0.127628, -0.838892, -0.778883, -0.00277231,
          0.775887, 0.8412, 0.133117, -0.697353, -0.88668, -0.260797, 0.604861,
          0.914413, 0.383258, -0.500263, -0.923844, -0.498047, 0.385652,
          0.914784, 0.602868, -0.263322, -0.887415, -0.695623, 0.135722,
          0.842285, 0.774455, -0.005405, -0.780296, -0.837786, -0.12502,
          0.702689, 0.884349, 0.252942, -0.611018, -0.913211, -0.375802,
          0.507118, 0.923796, 0.491141, -0.393067, -0.915891, -0.596649,
          0.27115, 0.889654, 0.690215, -0.143805, -0.845611, -0.769966,
          0.0135819, 0.784643, 0.834307, 0.116913, -0.70797, -0.881949,
          -0.245068, 0.617128, 0.911939, 0.368318, -0.513933, -0.923676,
          -0.484195, 0.400452, 0.916926, 0.590382, -0.278956, -0.891823,
          -0.684752, 0.151877, 0.848871, 0.765417, -0.0217577
     };

     // test

     for(int i = 0; i != ELEMS; ++i) {
          EXPECT_TRUE((std::real(matA(i))-L1[i])< pow(10.0, -6.0));
     }
}

//============//
// test # 188 //
//============//

TEST(MatrixTest188, DotComplex)
{
     MatrixDense<complex<double>> matA;
     MatrixDense<complex<double>> matB;

     // # 1

     const int ELEMS = 200;

     matA.Allocate(ELEMS);
     matB.Allocate(ELEMS);

     // # 2

     for(int i = 0; i != ELEMS*ELEMS; ++i) {
          matA.SetElement(i, sin(static_cast<double>(i+1)));
     }

     for(int i = 0; i != ELEMS*ELEMS; ++i) {
          matB.SetElement(i, cos(static_cast<double>(i+1)));
     }

     matA.Dot(matB);

     const double L1[ELEMS] = {
          -0.804158, -0.0518092, 0.748173, 0.860288, 0.181459, -0.664203,
          -0.899199, -0.307476, 0.566939, 0.920113, 0.427339, -0.458329,
          -0.922611, -0.538649, 0.340544, 0.906643, 0.639178, -0.215944,
          -0.872528, -0.726914, 0.0870214, 0.82095, 0.800101, 0.0436428,
          -0.75294, -0.857274, -0.173433, 0.669861, 0.897288, 0.299753,
          -0.573374, -0.919343, -0.420073, 0.465411, 0.922998, 0.531985,
          -0.348132, -0.908178, -0.633249, 0.223886, 0.875182, 0.721839,
          -0.0951589, -0.824668, -0.795981, -0.035473, 0.757649, 0.854192,
          0.165395, -0.675466, -0.895306, -0.292006, 0.579763, 0.918501,
          0.412773, -0.472456, -0.923312, -0.525279, 0.355693, 0.909642,
          0.62727, -0.231811, -0.877766, -0.716707, 0.103289, 0.828322, 0.7918,
          0.0273004, -0.762299, -0.851044, -0.157343, 0.681018, 0.893254,
          0.284237, -0.586107, -0.917587, -0.405441, 0.479465, 0.923553,
          0.518531, -0.363226, -0.911035, -0.621242, 0.239717, 0.880282,
          0.71152, -0.111411, -0.831911, -0.787556, -0.0191256, 0.766888,
          0.847829, 0.149279, -0.686517, -0.891133, -0.276445, 0.592405,
          0.9166, 0.398078, -0.486436, -0.923722, -0.511743, 0.37073, 0.912356,
          0.615166, -0.247605, -0.882729, -0.706276, 0.119524, 0.835434,
          0.78325, 0.0109494, -0.771418, -0.844547, -0.141204, 0.691962,
          0.888941, 0.268632, -0.598656, -0.915542, -0.390683, 0.493368,
          0.923819, 0.504915, -0.378206, -0.913606, -0.609041, 0.255474,
          0.885107, 0.700977, -0.127628, -0.838892, -0.778883, -0.00277231,
          0.775887, 0.8412, 0.133117, -0.697353, -0.88668, -0.260797, 0.604861,
          0.914413, 0.383258, -0.500263, -0.923844, -0.498047, 0.385652,
          0.914784, 0.602868, -0.263322, -0.887415, -0.695623, 0.135722,
          0.842285, 0.774455, -0.005405, -0.780296, -0.837786, -0.12502,
          0.702689, 0.884349, 0.252942, -0.611018, -0.913211, -0.375802,
          0.507118, 0.923796, 0.491141, -0.393067, -0.915891, -0.596649,
          0.27115, 0.889654, 0.690215, -0.143805, -0.845611, -0.769966,
          0.0135819, 0.784643, 0.834307, 0.116913, -0.70797, -0.881949,
          -0.245068, 0.617128, 0.911939, 0.368318, -0.513933, -0.923676,
          -0.484195, 0.400452, 0.916926, 0.590382, -0.278956, -0.891823,
          -0.684752, 0.151877, 0.848871, 0.765417, -0.0217577
     };

     // test

     for(int i = 0; i != ELEMS; ++i) {
          EXPECT_TRUE((std::real(matA(i))-L1[i])< pow(10.0, -6.0));
     }
}

//============//
// test # 189 //
//============//

TEST(MatrixTest189, DotComplex)
{
     MatrixDense<complex<double>> mat;

     // # 1

     const int ELEMS = 200;

     mat.Allocate(ELEMS);

     // # 2

     for(int i = 0; i != ELEMS*ELEMS; ++i) {
          mat.SetElement(i, sin(static_cast<double>(i+1)));
     }

     mat.Dot(mat);

     const double L1[ELEMS] = { -0.4547740548579048,
                                -0.92239098632159,-0.5419658987851806,
                                0.3367401366905215,0.9058488434496651,
                                0.6421243010771581,
                                -0.21196636239775463,
                                -0.8711761298171293,
                                -0.7294305811172018,
                                0.08295007992042225,0.8190668200230955,
                                0.8021373031167128,
                                0.047726448970537905,
                                -0.750563882257365,-0.8587892415405249,
                                -0.17744773266083447,0.667038403285078,
                                0.8982525074558882,0.30361739877543314,
                                -0.5701621461357871,
                                -0.9197372433272173,
                                -0.42371016058922306,
                                0.46187408975499866,0.9228134320199571,
                                0.5353223606579501,
                                -0.34434162032741267,
                                -0.9074195035965021,
                                -0.6362200800384232,0.219917151027772,
                                0.8738635676389255,0.7243838501911728,
                                -0.09109103845506883,
                                -0.8228172464335676,
                                -0.7980490726571842,
                                -0.039558261871662004,
                                0.7553022324464025,0.8557413375079496,
                                0.16941580331806072,
                                -0.6726698391414552,
                                -0.8963059336702137,
                                -0.29588248630916936,
                                0.5765739544325299,0.9189309604759685,
                                0.41642707932506573,
                                -0.4689379381054516,
                                -0.9231635778599094,
                                -0.5286368815169527,
                                0.35191612575881615,0.9089190699562798,
                                0.630266012930994,-0.2278507097624155,
                                -0.8764825406876439,
                                -0.7192803658109853,
                                0.09922486026095137,0.82650320740784,
                                0.7938983172788,0.031386975493302105,
                                -0.7599814068122905,
                                -0.8526263885284852,
                                -0.16137060071964365,0.678248573192179,
                                0.894289136814667,0.2881243922754051,
                                -0.5829405897681523,
                                -0.9180526819471343,
                                -0.4091113721607794,0.475965046476442,
                                0.9234413964084904,0.5219099851507703,
                                -0.3594630595433629,-0.910347425042154,
                                -0.6242625662394488,
                                0.23576641702948326,0.8790328437740519,
                                0.7141205278204007,
                                -0.10735090807577552,
                                -0.8301244141611595,
                                -0.7896853621816625,
                                -0.023213230032963106,
                                0.7646010387547428,0.8494446386496892,
                                0.1533127551847701,-0.6837741683590182,
                                -0.8922022748996743,
                                -0.2803437244991476,0.5892615533345438,
                                0.917102476551362,0.4017636122616153,
                                -0.482954864313648,-0.9236468658993975,
                                -0.5151421985930099,0.3669818303998238,
                                0.9117044569464832,0.618210210317069,
                                -0.24366365265541162,
                                -0.881514277089004,-0.708904740478344,
                                0.11546854524639802,0.833680582982085,
                                0.7854105374390289,
                                0.015037665880807793,
                                -0.7691607663384753,
                                -0.8461963371527894,
                                -0.14524289802315105,
                                0.6892461917270185,0.8900455114250383,
                                0.2725410925740698,-0.5955363499018653,
                                -0.9160804187345986,
                                -0.39438437530405457,
                                0.48990684398431283,0.923779970234668,
                                0.5083340520808721,-0.3744718492534889,
                                -0.9129900593496048,
                                -0.6121094193490646,0.2515417979137902,
                                0.8839266462191426,0.7036334124272031,
                                -0.12357713577856033,
                                -0.8371714352546789,
                                -0.7810741779715231,
                                -0.006860923569461108,
                                0.7736602323215945,0.8428817385331402,
                                0.1371616614856132,-0.694664214578376,
                                -0.8878190153671653,
                                -0.26471710781454294,
                                0.6017644878572668,0.9149865885722256,
                                0.3869742394307324,-0.4968204408202457,
                                -0.9238406989859405,
                                -0.5014860790136346,0.3819325292822619,
                                0.9142041315281431,0.605960671315407,
                                -0.25940023557390873,
                                -0.8862697621620494,
                                -0.6983069566608208,
                                0.13167604438684563,0.8405966974804419,
                                0.776676623520804,
                                -0.001316356276091779,
                                -0.7780990841834396,-0.839501102480296,
                                -0.129069678714471,0.7000278124261168,
                                0.8855229611658002,0.25687238320800276,
                                -0.6079454792435462,
                                -0.9138210717628001,
                                -0.3795337852051339,0.5036951131604182,
                                0.923829047395298,0.4945988159108795,
                                -0.3893632859626914,
                                -0.9153465783629597,
                                -0.5997644479533387,0.267238349949119,
                                0.8885434413411558,0.6929257904921555,
                                -0.13976463654432725,
                                -0.8439561012995991,
                                -0.7722182186229937,
                                0.009493532988809561,
                                0.7824769741523714,0.8360546938576927,
                                0.12096758369400934,
                                -0.7053365650473533,
                                -0.8831575287104007,
                                -0.24900753336672532,0.614078839797237,
                                0.9125839596212897,0.37206359556607027,
                                -0.5105303223934075,
                                -0.9237450163756002,
                                -0.48767280237047106,
                                0.3967635371157051,0.9164173103465129,
                                0.5935212347196825,
                                -0.27505552694500846,
                                -0.8907475056200106,
                                -0.6874903355205647,
                                0.14784227853237167,0.8472493835122421,
                                0.7676993125816897,
                                -0.01766996590971115,
                                -0.7867935592329294,
                                -0.8325427826818116,
                                -0.1128560112007782,0.7105900565160895,
                                0.8807229033260044,0.24112317447976428,
                                -0.6201640889866735,
                                -0.9112753490719966,
                                -0.3645642557821037,0.5173255329996493,
                                0.9235886125104533
                              };

     // test

     for(int i = 0; i != ELEMS; ++i) {
          EXPECT_TRUE((std::real(mat(i))-L1[i])< pow(10.0, -6.0));
     }
}

//============//
// test # 190 //
//============//

TEST(MatrixTest190, DotComplex)
{
     MatrixDense<complex<double>> mat;

     // # 1

     const int ELEMS = 200;

     mat.Allocate(ELEMS);

     // # 2

     for(int i = 0; i != ELEMS*ELEMS; ++i) {
          mat.SetElement(i, sin(static_cast<double>(i+1)));
     }

     mat.Dot();

     const double L1[ELEMS] = { -0.4547740548579048,
                                -0.92239098632159,-0.5419658987851806,
                                0.3367401366905215,0.9058488434496651,
                                0.6421243010771581,
                                -0.21196636239775463,
                                -0.8711761298171293,
                                -0.7294305811172018,
                                0.08295007992042225,0.8190668200230955,
                                0.8021373031167128,
                                0.047726448970537905,
                                -0.750563882257365,-0.8587892415405249,
                                -0.17744773266083447,0.667038403285078,
                                0.8982525074558882,0.30361739877543314,
                                -0.5701621461357871,
                                -0.9197372433272173,
                                -0.42371016058922306,
                                0.46187408975499866,0.9228134320199571,
                                0.5353223606579501,
                                -0.34434162032741267,
                                -0.9074195035965021,
                                -0.6362200800384232,0.219917151027772,
                                0.8738635676389255,0.7243838501911728,
                                -0.09109103845506883,
                                -0.8228172464335676,
                                -0.7980490726571842,
                                -0.039558261871662004,
                                0.7553022324464025,0.8557413375079496,
                                0.16941580331806072,
                                -0.6726698391414552,
                                -0.8963059336702137,
                                -0.29588248630916936,
                                0.5765739544325299,0.9189309604759685,
                                0.41642707932506573,
                                -0.4689379381054516,
                                -0.9231635778599094,
                                -0.5286368815169527,
                                0.35191612575881615,0.9089190699562798,
                                0.630266012930994,-0.2278507097624155,
                                -0.8764825406876439,
                                -0.7192803658109853,
                                0.09922486026095137,0.82650320740784,
                                0.7938983172788,0.031386975493302105,
                                -0.7599814068122905,
                                -0.8526263885284852,
                                -0.16137060071964365,0.678248573192179,
                                0.894289136814667,0.2881243922754051,
                                -0.5829405897681523,
                                -0.9180526819471343,
                                -0.4091113721607794,0.475965046476442,
                                0.9234413964084904,0.5219099851507703,
                                -0.3594630595433629,-0.910347425042154,
                                -0.6242625662394488,
                                0.23576641702948326,0.8790328437740519,
                                0.7141205278204007,
                                -0.10735090807577552,
                                -0.8301244141611595,
                                -0.7896853621816625,
                                -0.023213230032963106,
                                0.7646010387547428,0.8494446386496892,
                                0.1533127551847701,-0.6837741683590182,
                                -0.8922022748996743,
                                -0.2803437244991476,0.5892615533345438,
                                0.917102476551362,0.4017636122616153,
                                -0.482954864313648,-0.9236468658993975,
                                -0.5151421985930099,0.3669818303998238,
                                0.9117044569464832,0.618210210317069,
                                -0.24366365265541162,
                                -0.881514277089004,-0.708904740478344,
                                0.11546854524639802,0.833680582982085,
                                0.7854105374390289,
                                0.015037665880807793,
                                -0.7691607663384753,
                                -0.8461963371527894,
                                -0.14524289802315105,
                                0.6892461917270185,0.8900455114250383,
                                0.2725410925740698,-0.5955363499018653,
                                -0.9160804187345986,
                                -0.39438437530405457,
                                0.48990684398431283,0.923779970234668,
                                0.5083340520808721,-0.3744718492534889,
                                -0.9129900593496048,
                                -0.6121094193490646,0.2515417979137902,
                                0.8839266462191426,0.7036334124272031,
                                -0.12357713577856033,
                                -0.8371714352546789,
                                -0.7810741779715231,
                                -0.006860923569461108,
                                0.7736602323215945,0.8428817385331402,
                                0.1371616614856132,-0.694664214578376,
                                -0.8878190153671653,
                                -0.26471710781454294,
                                0.6017644878572668,0.9149865885722256,
                                0.3869742394307324,-0.4968204408202457,
                                -0.9238406989859405,
                                -0.5014860790136346,0.3819325292822619,
                                0.9142041315281431,0.605960671315407,
                                -0.25940023557390873,
                                -0.8862697621620494,
                                -0.6983069566608208,
                                0.13167604438684563,0.8405966974804419,
                                0.776676623520804,
                                -0.001316356276091779,
                                -0.7780990841834396,-0.839501102480296,
                                -0.129069678714471,0.7000278124261168,
                                0.8855229611658002,0.25687238320800276,
                                -0.6079454792435462,
                                -0.9138210717628001,
                                -0.3795337852051339,0.5036951131604182,
                                0.923829047395298,0.4945988159108795,
                                -0.3893632859626914,
                                -0.9153465783629597,
                                -0.5997644479533387,0.267238349949119,
                                0.8885434413411558,0.6929257904921555,
                                -0.13976463654432725,
                                -0.8439561012995991,
                                -0.7722182186229937,
                                0.009493532988809561,
                                0.7824769741523714,0.8360546938576927,
                                0.12096758369400934,
                                -0.7053365650473533,
                                -0.8831575287104007,
                                -0.24900753336672532,0.614078839797237,
                                0.9125839596212897,0.37206359556607027,
                                -0.5105303223934075,
                                -0.9237450163756002,
                                -0.48767280237047106,
                                0.3967635371157051,0.9164173103465129,
                                0.5935212347196825,
                                -0.27505552694500846,
                                -0.8907475056200106,
                                -0.6874903355205647,
                                0.14784227853237167,0.8472493835122421,
                                0.7676993125816897,
                                -0.01766996590971115,
                                -0.7867935592329294,
                                -0.8325427826818116,
                                -0.1128560112007782,0.7105900565160895,
                                0.8807229033260044,0.24112317447976428,
                                -0.6201640889866735,
                                -0.9112753490719966,
                                -0.3645642557821037,0.5173255329996493,
                                0.9235886125104533
                              };

     // test

     for(int i = 0; i != ELEMS; ++i) {
          EXPECT_TRUE((std::real(mat(i))-L1[i])< pow(10.0, -6.0));
     }

     for(int i = 0; i != ELEMS; ++i) {
          EXPECT_TRUE(std::imag(mat(i))< pow(10.0, -6.0));
     }
}

//============//
// test # 191 //
//============//

TEST(MatrixTest191, DotComplex)
{
     MatrixDense<complex<double>> matA;
     MatrixDense<complex<double>> matB;
     VectorDense<complex<double>> vec;

     // # 1

     const int ELEMS = 200;

     matA.Allocate(ELEMS);
     matB.Allocate(ELEMS, 1);
     vec.Allocate(ELEMS);

     // # 2

     for(int i = 0; i != ELEMS*ELEMS; ++i) {
          matA.SetElement(i, sin(static_cast<double>(i+1)));
     }

     // # 3

     for(int i = 0; i != ELEMS; ++i) {
          vec.SetElement(i, cos(static_cast<double>(i+1)));
     }

     // # 4

     matB.Set(0.0);

     matB.Dot(matA, vec);

     const double L1[ELEMS] = { 0.032115539128716175,
                                -86.86178848311302,-84.66810109520503,
                                4.363277843450885,88.91957147112586,
                                82.27776073182335,-8.750149559686403,
                                -90.80369077171775,-79.72672841857013,
                                13.119931863401371,92.51046662013187,
                                77.01998642954916,-17.46409038476545,
                                -94.0365656108813,-74.16282114645263,
                                21.77414079827998,95.37900720803106,
                                71.16081273403421,-26.041665393024896,
                                -96.53516956631069,-68.01982424181102,
                                30.258329512804472,97.50279465168919,
                                64.74599015327948,-34.41589783408374,
                                -98.27999265141129,-61.34570440500967,
                                38.506250449923876,98.86524566488146,
                                57.825607899016234,-42.521398728504494,
                                -99.25741066818819,-54.19257553279584,
                                46.45350091525942,99.45572174647798,
                                50.45370277236124,-50.29487744815559,
                                -99.45979158981964,-46.616291794495616,
                                54.03802595620268,99.26961224963685,
                                42.68783722529247,-57.67563591190137,
                                -98.88555515423205,-38.67601150283338,
                                61.20060290901283,98.30837038337134,
                                34.58864989259169,-64.60604253776518,
                                -97.53918520334676,-30.43373518482736,
                                67.88530383039686,96.57950186537784,
                                26.219382103859576,-71.03198225077851,
                                -95.43119467165099,-21.9538214596667,
                                74.0399322027431,94.09650631472823,
                                17.64538407276588,-76.90327903269532,
                                -92.57804349747366,-13.302484503767923,
                                79.61643050305877,90.87877184205206,
                                8.933604619384223,-82.17408771415279,
                                -89.00201009794394,-4.547277026981971,
                                84.57125545316768,86.95142366028665,
                                0.1520684100407383,-86.80325195002712,
                                -84.73101741120291,4.243437202942997,
                                88.86571802108293,82.34512789809618,
                                -8.630655203426915,-90.75462558278552,
                                -79.7984148641903,13.001017168926758,
                                92.46628551870148,77.09585214785358,
                                -17.34598759750817,-93.99735488451432,
                                -74.24271796848174,21.657080577983344,
                                95.34484343693592,71.24458461791224,
                                -25.925876363254797,-96.50611947377863,
                                -68.10730757750228,30.144037814437866,
                                97.4789149737808,64.8370140821259,
                                -34.30332668364596,-98.26133002614844,
                                -61.44009115342476,38.39561970363741,
                                98.8518365411613,57.92317312568456,
                                -42.412924452900256,-99.24928123459708,
                                -54.2931286886994,46.347394965189835,
                                99.45288788014388,50.557047472929604,
                                -50.19134705303304,-99.46225882540341,
                                -46.72222620315365,53.93727331527176,
                                99.277375768523,42.796154447655425,
                                -57.577857799335035,-98.8985997939395,
                                -38.78649999078044,61.10599028960435,
                                98.32667066716644,34.70109385742846,
                                -64.51478019396556,-97.56270539000067,
                                -30.547915018730272,67.79757000142706,
                                96.60819601897029,26.335074808779325,
                                -70.94794828452069,-95.46500675128519,
                                -22.07080108285281,73.95976222109087,
                                94.13537028398132,17.763422148062034,
                                -76.82712961101674,-92.6218834533683,
                                -13.421350497816029,79.54445036439806,
                                90.9275021632986,9.053066381865811,
                                -82.10641743876803,-89.05553561216199,
                                -4.667101244017586,84.50802720397148,
                                87.00963982988095,0.2720210598618543,
                                -86.74458921446686,-84.79381053752968,
                                4.123590392939578,88.8117353699646,
                                82.41237534352148,-8.51114829913475,
                                -90.70542844651203,-79.86998529160635,
                                12.88208357238915,92.42196998136278,
                                77.1716057771857,-17.22785959107413,
                                -93.95800749623078,-74.32250684968488,
                                21.539988870650276,95.31054104482266,
                                71.32825291992366,-25.81004964008411,
                                -96.47692907185966,-68.19469189258614,
                                30.029702289923332,97.45489357214673,
                                64.9279437450154,-34.19070565990728,
                                -98.24252453961364,-61.53438857463889,
                                38.284933134301646,98.83828369763705,
                                58.020654138367696,-42.30438851352315,
                                -99.24100750336054,-54.39360290830699,
                                46.241221631055765,99.44990942014246,
                                50.66031866905726,-50.08774368515837,
                                -99.46458145369559,-46.82809268278345,
                                53.83644225541988,99.2849949489173,
                                42.90440944907084,-57.47999597483445,
                                -98.91150064585416,-38.89693208738084,
                                61.01128882874121,98.34482799469151,
                                34.81348737065413,-64.42342405270202,
                                -97.58608373110678,-30.6620504392918,
                                67.7097376021745,96.63674971476749,
                                26.45072922536852,-70.86381116767318,
                                -95.49868003519694,-22.18774861749794,
                                73.87948470999949
                              };

     // test

     for(int i = 0; i != ELEMS; ++i) {
          EXPECT_TRUE((std::real(matB(i))-L1[i])< pow(10.0, -6.0));
     }
}

//============//
// test # 192 //
//============//

TEST(MatrixTest192, DotComplex)
{
     MatrixDense<complex<double>> mat;
     VectorDense<complex<double>> vec;

     // # 1

     const int ELEMS = 200;

     mat.Allocate(ELEMS, 1);
     vec.Allocate(ELEMS);

     // # 2

     for(int i = 0; i != ELEMS; ++i) {
          mat.SetElement(i, sin(static_cast<double>(i+1)));
     }

     // # 3

     for(int i = 0; i != ELEMS; ++i) {
          vec.SetElement(i, cos(static_cast<double>(i+1)));
     }

     // # 4

     complex<double> tmp_val;

     tmp_val = mat.DotBLASC(vec, pgg::WITH_COLS);

     const double L1[1] = { 0.032115539128716786 };

     // test

     EXPECT_TRUE((std::real(tmp_val)-L1[0])< pow(10.0, -6.0));
}

//============//
// test # 193 //
//============//

TEST(MatrixTest193, DotComplex)
{
     MatrixDense<complex<double>> mat;
     VectorDense<complex<double>> vec;

     // # 1

     const int ELEMS = 200;

     mat.Allocate(ELEMS, 1);
     vec.Allocate(ELEMS);

     // # 2

     for(int i = 0; i != ELEMS; ++i) {
          mat.SetElement(i, sin(static_cast<double>(i+1)));
     }

     // # 3

     for(int i = 0; i != ELEMS; ++i) {
          vec.SetElement(i, cos(static_cast<double>(i+1)));
     }

     // # 4

     complex<double> tmp_val;

     tmp_val = mat.DotC(vec, pgg::WITH_COLS);

     const double L1[1] = { 0.032115539128716786 };

     // test

     EXPECT_TRUE((std::real(tmp_val)-L1[0])< pow(10.0, -6.0));
}

//============//
// test # 194 //
//============//

TEST(MatrixTest194, DotComplex)
{
     MatrixDense<complex<double>> matRes;
     MatrixDense<complex<double>> mat;
     VectorDense<complex<double>> vec;

     // # 1

     const int ELEMS = 200;

     matRes.Allocate(1,1);
     mat.Allocate(ELEMS, 1);
     vec.Allocate(ELEMS);

     // # 2

     for(int i = 0; i != ELEMS; ++i) {
          mat.SetElement(i, sin(static_cast<double>(i+1)));
     }

     // # 3

     for(int i = 0; i != ELEMS; ++i) {
          vec.SetElement(i, cos(static_cast<double>(i+1)));
     }

     // # 4

     matRes.Dot(mat, vec, pgg::WITH_COLS);

     const double L1[1] = { 0.032115539128716786 };

     // test

     EXPECT_TRUE((std::real(matRes(0,0))-L1[0]) < pow(10.0, -6.0));
}

//============//
// test # 195 //
//============//

TEST(MatrixTest44, DotComplex)
{
     MatrixDense<complex<double>> matRes;
     MatrixDense<complex<double>> mat;
     VectorDense<complex<double>> vec;

     // # 1

     const int ELEMS = 200;

     matRes.Allocate(1,1);
     mat.Allocate(1, ELEMS);
     vec.Allocate(ELEMS);

     // # 2

     for(int i = 0; i != ELEMS; ++i) {
          mat.SetElement(i, sin(static_cast<double>(i+1)));
     }

     // # 3

     for(int i = 0; i != ELEMS; ++i) {
          vec.SetElement(i, cos(static_cast<double>(i+1)));
     }

     // # 4

     matRes.Dot(mat, vec, pgg::WITH_ROWS);

     const double L1[1] = { 0.032115539128716786 };

     // test

     EXPECT_TRUE((std::real(matRes(0,0))-L1[0]) < pow(10.0, -6.0));
}

//============//
// test # 196 //
//============//

TEST(MatrixTest196, DotComplex)
{
     MatrixDense<complex<double>> matRes;
     MatrixDense<complex<double>> mat;
     VectorDense<complex<double>> vec;

     // # 1

     const int ELEMS = 200;

     matRes.Allocate(1,1);
     mat.Allocate(1, ELEMS);
     vec.Allocate(ELEMS);

     // # 2

     for(int i = 0; i != ELEMS; ++i) {
          mat.SetElement(i, sin(static_cast<double>(i+1)));
     }

     // # 3

     for(int i = 0; i != ELEMS; ++i) {
          vec.SetElement(i, cos(static_cast<double>(i+1)));
     }

     // # 4

     matRes.Dot(mat, vec);

     const double L1[1] = { 0.032115539128716786 };

     // test

     EXPECT_TRUE((std::real(matRes(0,0))-L1[0]) < pow(10.0, -6.0));
}

// END
