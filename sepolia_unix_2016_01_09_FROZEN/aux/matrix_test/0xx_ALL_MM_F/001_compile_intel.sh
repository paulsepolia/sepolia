#!/bin/bash

  # 1. compile

  icpc  -c                     \
        -O3                    \
        -Wall                  \
        -std=c++11             \
        -openmp                \
        -isystem               \
        /opt/gtest/170/include \
        unit_test_all.cpp      \
        driver_program.cpp

  # 2. link

  icpc  unit_test_all.o                                            \
        driver_program.o                                           \
        /opt/gsl/116/gnu_492/lib/libgsl.a                          \
        /opt/mkl/libs_pgg/libmkl_core_thread_lp64_intel_2015_pgg.a \
        /opt/gtest/170/libgtest.a                                  \
        -static                                                    \
        -openmp                                                    \
        -o x_intel

  # 3. exit

  rm *.o
