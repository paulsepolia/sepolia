//=============//
// test driver //
//=============//

// sepolia

#include "../../../head/sepolia.h"

using pgg::sep::MatrixDense;
using pgg::sep::VectorDense;

// C++

#include <cmath>

using std::cos;
using std::sin;

// Google Test

#include "gtest/gtest.h"

//==========//
// test # 1 //
//==========//

TEST(MatrixTest1, Get1)
{
     // This test is named "Get",
     // and belongs to the "MatrixTest1"

     MatrixDense<double> matA;

     // test
     // AllocatedQ()
     // DeallocatedQ()

     EXPECT_EQ(matA.AllocatedQ(), false);
     EXPECT_EQ(matA.DeallocatedQ(), true);

     // test
     // AllocatedQ()
     // DeallocatedQ()

     const int NUM_ELEMS = 1000;

     matA.Allocate(NUM_ELEMS);

     EXPECT_EQ(matA.AllocatedQ(), true);
     EXPECT_EQ(matA.DeallocatedQ(), false);

     // test
     // Set
     // (i) operator

     const double ELEM_A = 12.34567;

     matA.Set(ELEM_A);

     EXPECT_EQ(matA, ELEM_A);
     EXPECT_EQ(matA(0), ELEM_A);

     for (int i = 0; i != NUM_ELEMS; ++i) {
          EXPECT_EQ(matA(i), ELEM_A);
     }

     // test
     // SetElement
     // GetElement

     const double ELEM_B = 11.2233445566;

     for (int i = 0; i != NUM_ELEMS; ++i) {
          matA.SetElement(i, i, ELEM_B);
     }

     for (int i = 0; i != NUM_ELEMS; ++i) {
          EXPECT_EQ(matA.GetElement(i, i), ELEM_B);
     }

     // test
     // SetElement
     // GetElement

     for (int i = 0; i != NUM_ELEMS; ++i) {
          matA.SetElement(i, i, cos(i));
     }

     const double THRES_A = pow(10.0, -10.0);

     for (int i = 0; i != NUM_ELEMS; ++i) {
          EXPECT_TRUE((matA.GetElement(i, i) - cos(i)) < THRES_A);
     }

     const double THRES_B = pow(10.0, -15.0);

     for (int i = 0; i != NUM_ELEMS; ++i) {
          EXPECT_TRUE((matA.GetElement(i, i) - cos(i)) < THRES_B);
     }

     // test
     // SetElement
     // GetElement

     MatrixDense<long double> matB;

     matB.Allocate(NUM_ELEMS);

     for (int i = 0; i != NUM_ELEMS; ++i) {
          matB.SetElement(i, i, pgg::functions::Cos(static_cast<long double>(i)));
     }

     for (int i = 0; i != NUM_ELEMS; ++i) {
          EXPECT_TRUE((matB.GetElement(i, i) -
                       pgg::functions::Cos(static_cast<long double>(i))) < THRES_A);
     }

     const double THRES_C = pow(10.0, -16.0);

     for (int i = 0; i != NUM_ELEMS; ++i) {
          EXPECT_TRUE((matB.GetElement(i, i) -
                       pgg::functions::Cos(static_cast<long double>(i))) < THRES_C);
     }

     const double THRES_D = pow(10.0, -20.0);

     for (int i = 0; i != NUM_ELEMS; ++i) {
          EXPECT_TRUE((matB.GetElement(i, i) -
                       pgg::functions::Cos(static_cast<long double>(i))) < THRES_D);
     }

     // end test
}

//==========//
// test # 2 //
//==========//

TEST(MatrixTest2, Get2)
{
     // This test is named "Get",
     // and belongs to the "VectorTest1"

     MatrixDense<long double> matA;

     // test
     // AllocatedQ()
     // DeallocatedQ()

     EXPECT_EQ(matA.AllocatedQ(), false);
     EXPECT_EQ(matA.DeallocatedQ(), true);

     // test
     // AllocatedQ()
     // DeallocatedQ()

     const int NUM_ELEMS = 1000;

     matA.Allocate(NUM_ELEMS);

     EXPECT_EQ(matA.AllocatedQ(), true);
     EXPECT_EQ(matA.DeallocatedQ(), false);

     // test
     // Set
     // (i) operator

     const long double ELEM_A = 12.34567L;

     matA.Set(pgg::functions::Cos(ELEM_A));

     EXPECT_EQ(matA, pgg::functions::Cos(ELEM_A));
     EXPECT_EQ(matA(0), pgg::functions::Cos(ELEM_A));

     for (int i = 0; i != NUM_ELEMS; ++i) {
          EXPECT_EQ(matA(i), pgg::functions::Cos(ELEM_A));
     }

     // test
     // SetElement
     // GetElement

     const long double ELEM_B = pgg::functions::Sin(11.2233445566L);

     for (int i = 0; i != NUM_ELEMS; ++i) {
          matA.SetElement(i, i, pgg::functions::Sin(ELEM_B));
     }

     for (int i = 0; i != NUM_ELEMS; ++i) {
          EXPECT_EQ(matA.GetElement(i, i), pgg::functions::Sin(ELEM_B));
     }

     // test
     // SetElement
     // GetElement

     for (int i = 0; i != NUM_ELEMS; ++i) {
          matA.SetElement(i, i, pgg::functions::Sin(static_cast<long double>(i)));
     }

     const double THRES_A = pow(10.0, -10.0);

     for (int i = 0; i != NUM_ELEMS; ++i) {
          EXPECT_TRUE((matA.GetElement(i, i) - sin(i)) < THRES_A);
     }

     const double THRES_B = pow(10.0, -14.0);

     for (int i = 0; i != NUM_ELEMS; ++i) {
          EXPECT_TRUE((matA.GetElement(i, i) - sin(i)) < THRES_B);
     }

     const double THRES_C = pow(10.0, -16.0);

     for (int i = 0; i != NUM_ELEMS; ++i) {
          EXPECT_TRUE((matA.GetElement(i,i) -
                       pgg::functions::Sin(static_cast<long double>(i))) < THRES_C);
     }

     const double THRES_D = pow(10.0, -20.0);

     for (int i = 0; i != NUM_ELEMS; ++i) {
          EXPECT_TRUE((matA.GetElement(i,i) -
                       pgg::functions::Sin(static_cast<long double>(i))) < THRES_D);
     }

     const double THRES_E = pow(10.0, -14.0);

     for (int i = 0; i != NUM_ELEMS; ++i) {
          EXPECT_TRUE((matA.GetElement(i,i) -
                       sin(static_cast<long double>(i))) < THRES_E);
     }

     // end test
}

//==========//
// test # 3 //
//==========//

TEST(MatrixTest3, Get3)
{
     MatrixDense<double> matA;

     const int ELEMS = 1000;

     matA.Allocate(ELEMS);

     const double ELEM_A = 12.34;

     matA = pgg::functions::Sin(ELEM_A);

     EXPECT_TRUE(matA.Equal(pgg::functions::Sin(ELEM_A)));

     EXPECT_TRUE(matA.Equal(sin(ELEM_A)));

     EXPECT_TRUE(matA == sin(ELEM_A));

     matA.Map(pgg::functions::Cos<double>);

     EXPECT_TRUE(matA == cos(sin(ELEM_A)));

     matA.Map(pgg::functions::Sin<double>);

     EXPECT_TRUE(matA == sin(cos(sin(ELEM_A))));
}

//==========//
// test # 4 //
//==========//

TEST(MatrixTest4, GetSet)
{
     MatrixDense<double> matA;
     MatrixDense<double> matB;

     const int ELEMS = 100;

     matA.Allocate(ELEMS);
     matB.Allocate(ELEMS);

     const double ELEM_A = 12.34;

     // # 1

     matA = pgg::functions::Sin(ELEM_A);
     matB = matA;

     EXPECT_TRUE(matA == matB);

     // # 2

     matA.Map(sin);
     matB.Map(pgg::functions::Sin<double>);

     EXPECT_TRUE(matA == matB);

     // # 3

     matA.Map(asin);
     matB.Map(pgg::functions::ArcSin<double>);

     EXPECT_TRUE(matA == matB);

     // # 4

     const int NEST_NUM = 50;

     matA.Nest(asin, NEST_NUM);
     matB.Nest(pgg::functions::ArcSin<double>, NEST_NUM);

     EXPECT_TRUE(matA == matB);

     // # 5

     const int NEST_NUM_B = 200;

     matA.Nest(sin, NEST_NUM_B);
     matB.Nest(pgg::functions::Sin<double>, NEST_NUM_B);

     EXPECT_TRUE(matA == matB);

     // # 6

     const int NEST_NUM_C = 1;

     matA.Nest(acos, NEST_NUM_C);
     matB.Nest(pgg::functions::ArcCos<double>, NEST_NUM_C);

     EXPECT_TRUE(matA == matB);

     // # 7

     const int NEST_NUM_D = 10;

     matA.Nest(cos, NEST_NUM_D);
     matB.Nest(pgg::functions::Cos<double>, NEST_NUM_D);

     EXPECT_TRUE(matA == matB);

     // # 8

     const int NEST_NUM_E = 10;

     matA.Nest(cos, NEST_NUM_E);
     matB.Nest(pgg::functions::Cos<double>, NEST_NUM_E);
     const double THRES_E = pow(10.0, -15.0);

     for (int i = 0; i != ELEMS; ++i) {
          EXPECT_TRUE((matA(i) - matB(i)) < THRES_E);
     }

}

// END
