//=============//
// test driver //
//=============//

// sepolia

#include "../../../head/sepolia.hpp"

using pgg::sep::MatrixDense;
using pgg::sep::VectorDense;

// C++

#include <cmath>
#include <complex>

using std::cos;
using std::sin;
using std::abs;
using std::complex;

// Google Test

#include "gtest/gtest.h"

// common parameters

const auto ROWS = static_cast<int>(pow(10.0, 2.0));
const auto COLS = static_cast<int>(pow(10.0, 2.0));
const auto K_MAX = 200;
const auto K_MAX_LOC = 200;

const double THRES_1 = std::pow(10.0, -1.0);
const double THRES_2 = std::pow(10.0, -2.0);
const double THRES_3 = std::pow(10.0, -3.0);
const double THRES_4 = std::pow(10.0, -4.0);
const double THRES_5 = std::pow(10.0, -5.0);
const double THRES_6 = std::pow(10.0, -6.0);
const double THRES_7 = std::pow(10.0, -7.0);
const double THRES_8 = std::pow(10.0, -8.0);
const double THRES_9 = std::pow(10.0, -9.0);
const double THRES_10 = std::pow(10.0, -10.0);
const double THRES_11 = std::pow(10.0, -11.0);
const double THRES_12 = std::pow(10.0, -12.0);
const double THRES_13 = std::pow(10.0, -13.0);
const double THRES_14 = std::pow(10.0, -14.0);
const double THRES_15 = std::pow(10.0, -15.0);
const double THRES_16 = std::pow(10.0, -16.0);
const double THRES_17 = std::pow(10.0, -17.0);
const double THRES_18 = std::pow(10.0, -18.0);
const double THRES_19 = std::pow(10.0, -19.0);
const double THRES_20 = std::pow(10.0, -20.0);

//==========//
// test # 1 //
//==========//

TEST(MatrixTest1, Heavy1)
{
     // main test loop

     for (int k = 0; k != K_MAX; ++k) {

          // 1

          MatrixDense<double> matA;
          MatrixDense<double> matB;

          matA.Allocate(ROWS, COLS);
          matB.Allocate(ROWS, COLS);

          EXPECT_EQ(matA.AllocatedQ(), true);
          EXPECT_EQ(matB.AllocatedQ(), true);

          // 2

          const double valA = 11.11111;

          matA.Set(valA);

          EXPECT_EQ(matA, valA);
          EXPECT_EQ(matA(1), valA);
          EXPECT_EQ(matA(2), valA);
          EXPECT_EQ(matA[10], valA);
          EXPECT_EQ(matA[20], valA);

          // 3

          matB.Set(matA);

          EXPECT_EQ(matB, valA);
          EXPECT_EQ(matB(1), valA);
          EXPECT_EQ(matB(2), valA);
          EXPECT_EQ(matB[10], valA);
          EXPECT_EQ(matB[20], valA);
          EXPECT_EQ(matB, matA);
          EXPECT_EQ(matB[10], matA(2));

          // 4

          const double valB = 12.3456789;

          matB = valB;
          matA = matB;

          EXPECT_EQ(matA, valB);
          EXPECT_EQ(matA(1), valB);
          EXPECT_EQ(matA(2), valB);
          EXPECT_EQ(matA[10], valB);
          EXPECT_EQ(matA[20], valB);

          EXPECT_EQ(matB, valB);
          EXPECT_EQ(matB(1), valB);
          EXPECT_EQ(matB(2), valB);
          EXPECT_EQ(matB[10], valB);
          EXPECT_EQ(matB[20], valB);
          EXPECT_EQ(matB, matB);
          EXPECT_EQ(matB[10], matA(2));

          // 5

          const double valC = 10.0;

          matA = matA + valC;

          EXPECT_EQ(matA, valB+valC);
          EXPECT_EQ(matA(1), valB+valC);
          EXPECT_EQ(matA(2), valB+valC);
          EXPECT_EQ(matA[10], valB+valC);
          EXPECT_EQ(matA[20], valB+valC);

          matA = matA - matA;

          EXPECT_EQ(matA, pgg::ZERO_DBL);
          EXPECT_EQ(matA(1), pgg::ZERO_DBL);
          EXPECT_EQ(matA(2), pgg::ZERO_DBL);
          EXPECT_EQ(matA[10], pgg::ZERO_DBL);
          EXPECT_EQ(matA[20], pgg::ZERO_DBL);

          // 6

          const double val4 = 2.2222;
          const double val5 = 3.4567;

          matA = val4;
          matA = matA * val5;

          const double THRES_A = std::pow(10.0, -14.0);
          const double THRES_B = std::pow(10.0, -15.0);
          const double THRES_C = std::pow(10.0, -16.0);
          const double THRES_D = std::pow(10.0, -17.0);
          const double THRES_E = std::pow(10.0, -18.0);
          const double THRES_F = std::pow(10.0, -19.0);
          const double THRES_G = std::pow(10.0, -20.0);

          EXPECT_TRUE(matA.Equal(val4*val5, THRES_A));
          EXPECT_TRUE(matA.Equal(val4*val5, THRES_B));
          EXPECT_TRUE(matA.Equal(val4*val5, THRES_C));
          EXPECT_TRUE(matA.Equal(val4*val5, THRES_D));
          EXPECT_TRUE(matA.Equal(val4*val5, THRES_E));
          EXPECT_TRUE(matA.Equal(val4*val5, THRES_F));
          EXPECT_TRUE(matA.Equal(val4*val5, THRES_G));

          // 7

          const double val6 = 2.012012;

          matB = val6;

          matA = matA * matB * matA;

          EXPECT_TRUE(matA.Equal(val4*val5*val6*val4*val5, THRES_A));
          EXPECT_TRUE(matA.Equal(val4*val5*val6*val4*val5, THRES_B));
          EXPECT_TRUE(matA.Equal(val4*val5*val6*val4*val5, THRES_C));
          EXPECT_TRUE(matA.Equal(val4*val5*val6*val4*val5, THRES_D));
          EXPECT_TRUE(matA.Equal(val4*val5*val6*val4*val5, THRES_E));
          EXPECT_TRUE(matA.Equal(val4*val5*val6*val4*val5, THRES_F));
          EXPECT_TRUE(matA.Equal(val4*val5*val6*val4*val5, THRES_G));

          // 8

          matA = val5;
          matB = val6;
          matA = matA/matB;

          EXPECT_TRUE(matA.Equal(val5/val6, THRES_A));
          EXPECT_TRUE(matA.Equal(val5/val6, THRES_B));
          EXPECT_TRUE(matA.Equal(val5/val6, THRES_C));
          EXPECT_TRUE(matA.Equal(val5/val6, THRES_D));
          EXPECT_TRUE(matA.Equal(val5/val6, THRES_E));
          EXPECT_TRUE(matA.Equal(val5/val6, THRES_F));
          EXPECT_TRUE(matA.Equal(val5/val6, THRES_G));

          // 9

          matA = val5;
          matB = val6;
          matA = matA*matB+matA-matB/matA;

          EXPECT_TRUE(matA.Equal(val5*val6+val5-val6/val5, THRES_A));
          EXPECT_TRUE(matA.Equal(val5*val6+val5-val6/val5, THRES_B));
          EXPECT_TRUE(matA.Equal(val5*val6+val5-val6/val5, THRES_C));
          EXPECT_TRUE(matA.Equal(val5*val6+val5-val6/val5, THRES_D));
          EXPECT_TRUE(matA.Equal(val5*val6+val5-val6/val5, THRES_E));
          EXPECT_TRUE(matA.Equal(val5*val6+val5-val6/val5, THRES_F));
          EXPECT_TRUE(matA.Equal(val5*val6+val5-val6/val5, THRES_G));

          // 10

          matA--;

          EXPECT_TRUE(matA.Equal(val5*val6+val5-val6/val5-1.0, THRES_A));
          EXPECT_TRUE(matA.Equal(val5*val6+val5-val6/val5-1.0, THRES_B));
          EXPECT_TRUE(matA.Equal(val5*val6+val5-val6/val5-1.0, THRES_C));
          EXPECT_TRUE(matA.Equal(val5*val6+val5-val6/val5-1.0, THRES_D));
          EXPECT_TRUE(matA.Equal(val5*val6+val5-val6/val5-1.0, THRES_E));
          EXPECT_TRUE(matA.Equal(val5*val6+val5-val6/val5-1.0, THRES_F));
          EXPECT_TRUE(matA.Equal(val5*val6+val5-val6/val5-1.0, THRES_G));

          // 11

          --matB;

          EXPECT_TRUE(matB.Equal(val6-1.0, THRES_A));
          EXPECT_TRUE(matB.Equal(val6-1.0, THRES_B));
          EXPECT_TRUE(matB.Equal(val6-1.0, THRES_C));
          EXPECT_TRUE(matB.Equal(val6-1.0, THRES_D));
          EXPECT_TRUE(matB.Equal(val6-1.0, THRES_E));
          EXPECT_TRUE(matB.Equal(val6-1.0, THRES_F));
          EXPECT_TRUE(matB.Equal(val6-1.0, THRES_G));

          // 12

          matA = 0;
          matB = 0;

          matA += 10;
          matB -= 11;


          EXPECT_EQ(matA(10,11), 10);
          EXPECT_EQ(matB(10,12), -11);

          // 13

          matA *= 10;
          matB /= 11;

          EXPECT_EQ(matA(10,11),10*10);
          EXPECT_EQ(matB(1,2), -1);

          // 14

          matA = val4;
          matB = val5;

          matA.Plus(matA, matA);
          matB.Plus(matB, matB);

          EXPECT_EQ(matA, 2*val4);
          EXPECT_EQ(matB, 2*val5);

          EXPECT_TRUE(matA.Equal(2*val4, THRES_A));
          EXPECT_TRUE(matB.Equal(2*val5, THRES_A));

          EXPECT_TRUE(matA.Equal(2*val4, THRES_E));
          EXPECT_TRUE(matB.Equal(2*val5, THRES_E));

          EXPECT_TRUE(matA.Equal(2*val4, THRES_F));
          EXPECT_TRUE(matB.Equal(2*val5, THRES_F));

          EXPECT_TRUE(matA.Equal(2*val4, THRES_G));
          EXPECT_TRUE(matB.Equal(2*val5, THRES_G));

          // 15

          matA.Subtract(matA, matA);
          matB.Subtract(matB, matB);

          EXPECT_EQ(matA, 0.0);
          EXPECT_EQ(matB, 0.0);

          EXPECT_TRUE(matA.Equal(0.0, THRES_G));
          EXPECT_TRUE(matB.Equal(0.0, THRES_G));

          // 16

          matA = valB;
          matB = valA;

          matA.Divide(matA, matA);
          matB.Divide(matB, matB);

          EXPECT_EQ(matA, 1.0);
          EXPECT_EQ(matB, 1.0);

          EXPECT_TRUE(matA.Equal(1.0, THRES_G));
          EXPECT_TRUE(matB.Equal(1.0, THRES_G));

          // 17

          matA = val4;
          matB = val5;

          matA.Times(matA, matA);
          matB.Times(matB, matB);

          EXPECT_EQ(matA, val4*val4);
          EXPECT_EQ(matB, val5*val5);

          EXPECT_TRUE(matA.Equal(val4*val4, THRES_G));
          EXPECT_TRUE(matB.Equal(val5*val5, THRES_G));

          // 18

          matA.Deallocate();
          matB.Deallocate();

          EXPECT_TRUE(matA.DeallocatedQ());
          EXPECT_TRUE(matB.DeallocatedQ());
     }
}

//==========//
// test # 2 //
//==========//

TEST(MatrixTest2, Heavy2)
{
     // main test loop

     for (int k = 0; k != K_MAX; ++k) {

          MatrixDense<double> matA;
          MatrixDense<double> matB;

          const int the_seed = 10;
          const double s1 = 0.0;
          const double s2 = 1.0;

          matA.Allocate(ROWS, COLS);
          matB.Allocate(ROWS, COLS);

          matA.Random(s1, s2, the_seed);
          matB.Random(s1, s2, the_seed);

          // 1

          EXPECT_EQ(matA, matB);
          EXPECT_TRUE(matA == matB);

          // 2

          matA.Sort();
          matB.Sort();

          EXPECT_TRUE(matA.SortedQ());
          EXPECT_TRUE(matB.SortedQ());

          // 3

          EXPECT_TRUE(matA==matB);
     }
}

//==========//
// test # 3 //
//==========//

TEST(MatrixTest3, Heavy3)
{
     // main test loop

     for (int k = 0; k != K_MAX; ++k) {

          MatrixDense<double> matA;
          MatrixDense<double> matB;

          matA.Allocate(ROWS, COLS);
          matB.Allocate(ROWS, COLS);

          // 1

          matA = 0.1;
          matB = 0.2;

          matA.Map(sin);
          matB.Map(cos);

          EXPECT_EQ(matA, sin(0.1));
          EXPECT_EQ(matB, cos(0.2));

          // 2

          matA = 0.1;
          matB = 0.2;

          matA.Nest(sin, 50);
          matB.Nest(cos, 50);

          double valA = 0.1;
          double valB = 0.2;

          for (int ik = 0; ik != 50; ++ik) {
               valA = sin(valA);
               valB = cos(valB);
          }

          EXPECT_TRUE(matA.Equal(valA, THRES_20));
          EXPECT_TRUE(matA.Equal(valA, THRES_20));
          EXPECT_TRUE(matA.Equal(valA, THRES_19));
          EXPECT_TRUE(matA.Equal(valA, THRES_19));
          EXPECT_TRUE(matA.Equal(valA, THRES_18));
          EXPECT_TRUE(matB.Equal(valB, THRES_18));
          EXPECT_TRUE(matA.Equal(valA, THRES_17));
          EXPECT_TRUE(matB.Equal(valB, THRES_17));
          EXPECT_TRUE(matA.Equal(valA, THRES_16));
          EXPECT_TRUE(matB.Equal(valB, THRES_16));
          EXPECT_TRUE(matA.Equal(valA, THRES_15));
          EXPECT_TRUE(matB.Equal(valB, THRES_15));
          EXPECT_TRUE(matA.Equal(valA, THRES_10));
          EXPECT_TRUE(matB.Equal(valB, THRES_10));

          // 3

          matA.Nest(asin, 50);
          matB.Nest(acos, 50);

          for (int ik = 0; ik != 50; ++ik) {
               valA = asin(valA);
               valB = acos(valB);
          }

          EXPECT_TRUE(matA.Equal(valA, THRES_20));
          EXPECT_TRUE(matA.Equal(valA, THRES_20));
          EXPECT_TRUE(matA.Equal(valA, THRES_19));
          EXPECT_TRUE(matA.Equal(valA, THRES_19));
          EXPECT_TRUE(matA.Equal(valA, THRES_18));
          EXPECT_TRUE(matB.Equal(valB, THRES_18));
          EXPECT_TRUE(matA.Equal(valA, THRES_17));
          EXPECT_TRUE(matB.Equal(valB, THRES_17));
          EXPECT_TRUE(matA.Equal(valA, THRES_16));
          EXPECT_TRUE(matB.Equal(valB, THRES_16));
          EXPECT_TRUE(matA.Equal(valA, THRES_15));
          EXPECT_TRUE(matB.Equal(valB, THRES_15));
          EXPECT_TRUE(matA.Equal(valA, THRES_10));
          EXPECT_TRUE(matB.Equal(valB, THRES_10));
     }
}

//==========//
// test # 4 //
//==========//

TEST(MatrixTest4, Heavy4)
{
     // main test loop

     for (int k = 0; k != K_MAX; ++k) {

          MatrixDense<double> matA;
          MatrixDense<double> matB;

          matA.Allocate(ROWS, COLS);
          matB.Allocate(ROWS, COLS);

          // 1

          matA = 0.1;
          matB = 0.2;

          matA.Map(sin);
          matB.Map(cos);

          EXPECT_EQ(matA, pgg::functions::Sin<double>(0.1));
          EXPECT_EQ(matB, pgg::functions::Cos<double>(0.2));

          // 2

          matA = 0.1;
          matB = 0.2;

          matA.Nest(pgg::functions::Sin<double>, 50);
          matB.Nest(pgg::functions::Cos<double>, 50);

          double valA = 0.1;
          double valB = 0.2;

          for (int ik = 0; ik != 50; ++ik) {
               valA = pgg::functions::Sin<double>(valA);
               valB = pgg::functions::Cos<double>(valB);
          }

          EXPECT_TRUE(matA.Equal(valA, THRES_20));
          EXPECT_TRUE(matA.Equal(valA, THRES_20));
          EXPECT_TRUE(matA.Equal(valA, THRES_19));
          EXPECT_TRUE(matA.Equal(valA, THRES_19));
          EXPECT_TRUE(matA.Equal(valA, THRES_18));
          EXPECT_TRUE(matB.Equal(valB, THRES_18));
          EXPECT_TRUE(matA.Equal(valA, THRES_17));
          EXPECT_TRUE(matB.Equal(valB, THRES_17));
          EXPECT_TRUE(matA.Equal(valA, THRES_16));
          EXPECT_TRUE(matB.Equal(valB, THRES_16));
          EXPECT_TRUE(matA.Equal(valA, THRES_15));
          EXPECT_TRUE(matB.Equal(valB, THRES_15));
          EXPECT_TRUE(matA.Equal(valA, THRES_10));
          EXPECT_TRUE(matB.Equal(valB, THRES_10));

          // 3

          matA.Nest(pgg::functions::ArcSin<double>, 50);
          matB.Nest(pgg::functions::ArcCos<double>, 50);

          for (int ik = 0; ik != 50; ++ik) {
               valA = pgg::functions::ArcSin<double>(valA);
               valB = pgg::functions::ArcCos<double>(valB);
          }

          EXPECT_TRUE(matA.Equal(valA, THRES_20));
          EXPECT_TRUE(matA.Equal(valA, THRES_20));
          EXPECT_TRUE(matA.Equal(valA, THRES_19));
          EXPECT_TRUE(matA.Equal(valA, THRES_19));
          EXPECT_TRUE(matA.Equal(valA, THRES_18));
          EXPECT_TRUE(matB.Equal(valB, THRES_18));
          EXPECT_TRUE(matA.Equal(valA, THRES_17));
          EXPECT_TRUE(matB.Equal(valB, THRES_17));
          EXPECT_TRUE(matA.Equal(valA, THRES_16));
          EXPECT_TRUE(matB.Equal(valB, THRES_16));
          EXPECT_TRUE(matA.Equal(valA, THRES_15));
          EXPECT_TRUE(matB.Equal(valB, THRES_15));
          EXPECT_TRUE(matA.Equal(valA, THRES_10));
          EXPECT_TRUE(matB.Equal(valB, THRES_10));
     }
}

//==========//
// test # 5 //
//==========//

TEST(MatrixTest5, Heavy5)
{
     // main test loop

     for (int k = 0; k != K_MAX; ++k) {

          MatrixDense<double> matA;
          MatrixDense<double> matB;

          matA.Allocate(ROWS, COLS);
          matB.Allocate(ROWS, COLS);

          // 1

          matA = 0.1;
          matB = 0.2;

          pgg::functors::Sin<double> sin_d;
          pgg::functors::Cos<double> cos_d;

          matA.Map(sin_d);
          matB.Map(cos_d);

          EXPECT_EQ(matA, sin_d(0.1));
          EXPECT_TRUE(matB.Equal(cos_d(0.2), THRES_1));
          EXPECT_TRUE(matB.Equal(cos_d(0.2), THRES_2));
          EXPECT_TRUE(matB.Equal(cos_d(0.2), THRES_3));
          EXPECT_TRUE(matB.Equal(cos_d(0.2), THRES_4));
          EXPECT_TRUE(matB.Equal(cos_d(0.2), THRES_5));
          EXPECT_TRUE(matB.Equal(cos_d(0.2), THRES_6));
          EXPECT_TRUE(matB.Equal(cos_d(0.2), THRES_7));
          EXPECT_TRUE(matB.Equal(cos_d(0.2), THRES_8));
          EXPECT_TRUE(matB.Equal(cos_d(0.2), THRES_9));
          EXPECT_TRUE(matB.Equal(cos_d(0.2), THRES_10));
          EXPECT_TRUE(matB.Equal(cos_d(0.2), THRES_11));
          EXPECT_TRUE(matB.Equal(cos_d(0.2), THRES_12));
          EXPECT_TRUE(matB.Equal(cos_d(0.2), THRES_13));
          EXPECT_TRUE(matB.Equal(cos_d(0.2), THRES_14));
          EXPECT_TRUE(matB.Equal(cos_d(0.2), THRES_15));

          // 2

          matA = 0.1;
          matB = 0.2;

          matA.Nest(sin_d, 50);
          matB.Nest(cos_d, 50);

          double valA = 0.1;
          double valB = 0.2;

          for (int ik = 0; ik != 50; ++ik) {
               valA = sin_d(valA);
               valB = cos_d(valB);
          }

          EXPECT_TRUE(matA.Equal(valA, THRES_20));
          EXPECT_TRUE(matB.Equal(valB, THRES_20));
          EXPECT_TRUE(matA.Equal(valA, THRES_19));
          EXPECT_TRUE(matB.Equal(valB, THRES_19));
          EXPECT_TRUE(matA.Equal(valA, THRES_18));
          EXPECT_TRUE(matB.Equal(valB, THRES_18));
          EXPECT_TRUE(matA.Equal(valA, THRES_17));
          EXPECT_TRUE(matB.Equal(valB, THRES_17));
          EXPECT_TRUE(matA.Equal(valA, THRES_16));
          EXPECT_TRUE(matB.Equal(valB, THRES_16));
          EXPECT_TRUE(matA.Equal(valA, THRES_15));
          EXPECT_TRUE(matB.Equal(valB, THRES_15));
          EXPECT_TRUE(matA.Equal(valA, THRES_10));
          EXPECT_TRUE(matB.Equal(valB, THRES_10));

          // 3

          pgg::functors::ArcSin<double> asin_d;
          pgg::functors::ArcCos<double> acos_d;

          matA.Nest(asin_d, 50);
          matB.Nest(acos_d, 50);

          for (int ik = 0; ik != 50; ++ik) {
               valA = asin_d(valA);
               valB = acos_d(valB);
          }

          EXPECT_TRUE(matA.Equal(valA, THRES_20));
          EXPECT_TRUE(matB.Equal(valB, THRES_20));
          EXPECT_TRUE(matA.Equal(valA, THRES_19));
          EXPECT_TRUE(matB.Equal(valB, THRES_19));
          EXPECT_TRUE(matA.Equal(valA, THRES_18));
          EXPECT_TRUE(matB.Equal(valB, THRES_18));
          EXPECT_TRUE(matA.Equal(valA, THRES_17));
          EXPECT_TRUE(matB.Equal(valB, THRES_17));
          EXPECT_TRUE(matA.Equal(valA, THRES_16));
          EXPECT_TRUE(matB.Equal(valB, THRES_16));
          EXPECT_TRUE(matA.Equal(valA, THRES_15));
          EXPECT_TRUE(matB.Equal(valB, THRES_15));
          EXPECT_TRUE(matA.Equal(valA, THRES_10));
          EXPECT_TRUE(matB.Equal(valB, THRES_10));
     }
}

//==========//
// test # 6 //
//==========//

TEST(MatrixTest6, Heavy6)
{
     // main test loop

     for (int k = 0; k != K_MAX; ++k) {

          MatrixDense<double> matA;
          MatrixDense<double> matB;

          matA.Allocate(ROWS, COLS);
          matB.Allocate(ROWS, COLS);

          // 1

          matA.Random(0.0, 8.0, 10);
          matB.Random(0.0, 8.0, 10);

          EXPECT_EQ(matA.Min(), matB.Min());
          EXPECT_EQ(matA.Max(), matB.Max());

          // 2

          matA = matA + matB;

          EXPECT_TRUE(matA.Equal(2.0*matB, THRES_1));

          pgg::functions::SeedRandomGenerator(10);

          matA.SetElement(1, 1, pgg::functions::RandomNumber());

          pgg::functions::SeedRandomGenerator(10);

          matA.SetElement(1, 2, pgg::functions::RandomNumber());

          EXPECT_EQ(matA(1,1), matA(1,2));

          // 3

          matA = 10.0;

          EXPECT_EQ(matA.Count(10.0), ROWS*COLS);

          matA = 11.1;

          EXPECT_EQ(matA.Count(11.1), ROWS*COLS);

          matA = 12.3456;

          EXPECT_EQ(matA.Count(12.3456), ROWS*COLS);

          // 4

          matA.Range(10.0);
          matB.Range(10.0);

          matA.RotateRight(1);

          EXPECT_EQ(matA(0), matB(ROWS*COLS-1));
          EXPECT_EQ(matA(1), matB(0));
          EXPECT_EQ(matA(2), matB(1));
          EXPECT_EQ(matA(ROWS*COLS-1), matB(ROWS*COLS-2));

          // 5

          matA.RotateLeft(1);

          EXPECT_EQ(matA, matB);
          EXPECT_TRUE(matA==matB);
     }
}

//==========//
// test # 7 //
//==========//

TEST(MatrixTest7, Heavy7)
{
     pgg::functors::Cos<double> dCos;
     pgg::functors::Cos<long double> dlCos;

     // main test loop

     for (int k = 0; k != K_MAX; ++k) {

          MatrixDense<double> matA;

          matA.Allocate(ROWS, COLS);

          // 1

          matA.Set(10.0);
          matA.Cos();

          EXPECT_TRUE(matA.Equal(dlCos(10.0), THRES_15));
          EXPECT_TRUE(matA.Equal(dCos(10.0), THRES_15));
          EXPECT_TRUE(matA.Equal(cos(10.0), THRES_15));
          EXPECT_TRUE(matA.Equal(cos(10.0L), THRES_15));

          EXPECT_TRUE(matA.Equal(dlCos(10.0), THRES_16));
          EXPECT_TRUE(matA.Equal(dCos(10.0), THRES_16));
          EXPECT_TRUE(matA.Equal(cos(10.0), THRES_16));
          EXPECT_TRUE(matA.Equal(cos(10.0L), THRES_16));

          EXPECT_TRUE(matA.Equal(dlCos(10.0), THRES_17));
          EXPECT_TRUE(matA.Equal(dCos(10.0), THRES_17));
          EXPECT_TRUE(matA.Equal(cos(10.0), THRES_17));
          EXPECT_TRUE(matA.Equal(cos(10.0L), THRES_17));

          matA.Deallocate();
     }
}

//==========//
// test # 8 //
//==========//

TEST(MatrixTest8, Heavy8)
{
     // main test loop

     for (int k = 0; k != K_MAX; ++k) {

          MatrixDense<double> matA;
          MatrixDense<double> matB;
          MatrixDense<double> matC;
          MatrixDense<double> matD;

          matA.Allocate(ROWS, COLS);
          matB.Allocate(ROWS, COLS);
          matC.Allocate(ROWS, COLS);
          matD.Allocate(ROWS, COLS);

          matA = 10.0;
          matB = 20.0;
          matC = 10.0L;
          matD = 20.0L;

          for (int i = 1; i != 10; ++i) {
               matA.FastMA(matB, matB, matB);
               matC.FastMA(matD, matD, matD);
          }

          // test

          EXPECT_TRUE(matA.Equal(matC, THRES_1));
          EXPECT_TRUE(matA.Equal(matC, THRES_2));
          EXPECT_TRUE(matA.Equal(matC, THRES_3));
          EXPECT_TRUE(matA.Equal(matC, THRES_4));
          EXPECT_TRUE(matA.Equal(matC, THRES_5));
          EXPECT_TRUE(matA.Equal(matC, THRES_6));
          EXPECT_TRUE(matA.Equal(matC, THRES_7));
          EXPECT_TRUE(matA.Equal(matC, THRES_8));
          EXPECT_TRUE(matA.Equal(matC, THRES_9));
          EXPECT_TRUE(matA.Equal(matC, THRES_10));
          EXPECT_TRUE(matA.Equal(matC, THRES_11));
          EXPECT_TRUE(matA.Equal(matC, THRES_12));
          EXPECT_TRUE(matA.Equal(matC, THRES_13));
          EXPECT_TRUE(matA.Equal(matC, THRES_14));
          EXPECT_TRUE(matA.Equal(matC, THRES_15));
          EXPECT_TRUE(matA.Equal(matC, THRES_16));
          EXPECT_TRUE(matA.Equal(matC, THRES_17));
          EXPECT_TRUE(matA.Equal(matC, THRES_18));
          EXPECT_TRUE(matA.Equal(matC, THRES_19));

          // deallocations

          matA.Deallocate();
          matB.Deallocate();
          matC.Deallocate();
          matD.Deallocate();
     }
}

//==========//
// test # 9 //
//==========//

TEST(MatrixTest9, Heavy9)
{
     // main test loop

     for (int k = 0; k != K_MAX; ++k) {

          MatrixDense<double> matA;
          MatrixDense<double> matB;
          VectorDense<double> vecA;
          VectorDense<double> vecB;

          // 1

          matA.Allocate(ROWS);
          matB.Allocate(ROWS);
          vecA.Allocate(ROWS);
          vecB.Allocate(ROWS);

          // 2

          matA.Random(0.0, 1.0, 10);
          matB.Random(0.0, 1.0, 10);

          // 3

          for (int ik = 0; ik != 10; ++ik) {
               matA.GetRow(10, vecA);
          }

          // 4

          for (int ik = 0; ik != 10; ++ik) {
               matB.GetRow(10, vecB);
          }

          // 5

          EXPECT_EQ(matA, matB);
          EXPECT_EQ(vecA, vecB);

          // 6

          for (int ik = 0; ik != 10; ++ik) {
               matA.GetColumn(10, vecA);
          }

          // 7

          for (int ik = 0; ik != 10; ++ik) {
               matB.GetColumn(10, vecB);
          }

          // 8

          EXPECT_EQ(matA, matB);
          EXPECT_EQ(vecA, vecB);

          // 9

          for (int ik = 0; ik != 10; ++ik) {
               vecA.GetRow(matA, 10);
          }

          for (int ik = 0; ik != 10; ++ik) {
               vecB.GetRow(matB, 10);
          }

          EXPECT_EQ(vecA, vecB);

          // 10

          for (int ik = 0; ik != 10; ++ik) {
               vecA.GetColumn(matA, 10);
          }

          for (int ik = 0; ik != 10; ++ik) {
               vecB.GetColumn(matB, 10);
          }

          EXPECT_EQ(vecA, vecB);

          // 11

          vecA = vecB = 10.0;

          for (int ik = 0; ik != ROWS; ++ik) {
               matA.SetRow(ik, vecA);
               matB.SetRow(ik, vecB);
          }

          EXPECT_EQ(vecA, vecB);
          EXPECT_EQ(matA, matB);

          // 12

          vecA = vecB = 11.0;

          for (int ik = 0; ik != ROWS; ++ik) {
               matA.SetColumn(ik, vecA);
               matB.SetColumn(ik, vecB);
          }

          EXPECT_EQ(vecA, vecB);
          EXPECT_EQ(matA, matB);

          // 13

          matA.Deallocate();
          matB.Deallocate();
          vecA.Deallocate();
          vecB.Deallocate();
     }
}

//===========//
// test # 10 //
//===========//

TEST(MatrixTest10, Heavy10)
{
     // main test loop

     for (int k = 0; k != K_MAX; ++k) {

          MatrixDense<double> matA;
          VectorDense<double> vecA;
          VectorDense<double> vecB;

          matA.Allocate(ROWS);
          vecA.Allocate(ROWS);
          vecB.Allocate(ROWS);

          // 1

          matA = 1.0;
          vecA = 0.0;

          for (int ik = 0; ik != 10; ++ik) {
               vecB.DotBLAS(matA, vecA);
          }

          EXPECT_EQ(vecB, 0.0);

          // 2

          matA = 1.0;
          vecA = 1.0;

          for (int ik = 0; ik != 10; ++ik) {
               vecB.DotBLAS(matA, vecA);
          }

          EXPECT_EQ(vecB, 1.0*ROWS);

          // 3

          matA = 1.0;
          vecA = 2.0;

          for (int ik = 0; ik != 10; ++ik) {
               vecB.Dot(matA, vecA);
          }

          EXPECT_EQ(vecB, 2.0*ROWS);

          // 4

          matA = 2.0;
          vecA = 2.0;

          for (int ik = 0; ik != 10; ++ik) {
               vecB.Dot(matA, vecA);
          }

          EXPECT_EQ(vecB, 2.0*2.0*ROWS);

          // 5

          matA = 2.0;
          vecA = 2.0;

          for (int ik = 0; ik != 10; ++ik) {
               vecB.DotGSL(matA, vecA);
          }

          EXPECT_EQ(vecB, 2.0*2.0*ROWS);
     }
}

//===========//
// test # 11 //
//===========//

TEST(MatrixTest11, Heavy11)
{
     // main test loop

     for (int k = 0; k != K_MAX; ++k) {

          MatrixDense<double> matA;
          VectorDense<double> vecA;
          VectorDense<double> vecB;

          matA.Allocate(ROWS);
          vecA.Allocate(ROWS);
          vecB.Allocate(ROWS);

          // 1

          matA = 1.0;
          vecA = 0.0;

          for (int ik = 0; ik != 10; ++ik) {
               vecB.DotBLAS(matA, vecA, "WithColumns");
          }

          EXPECT_EQ(vecB, 0.0);

          // 2

          matA = 1.0;
          vecA = 0.0;

          for (int ik = 0; ik != 10; ++ik) {
               vecB.DotBLAS(matA, vecA, "WithRows");
          }

          EXPECT_EQ(vecB, 0.0);

          // 3

          matA = 0.0;
          vecA = 0.0;

          for (int ik = 0; ik != ROWS; ++ik) {
               vecA = static_cast<double>(ik);
               matA.SetRow(ik, vecA);
          }

          for (int ik = 0; ik != 10; ++ik) {

               vecA = 1.0;
               vecB.DotBLAS(matA, vecA);
          }

          EXPECT_EQ(vecB(0), 0.0);
          EXPECT_EQ(vecB(1), 1.0*1.0*ROWS);
     }
}

//===========//
// test # 12 //
//===========//

TEST(MatrixTest12, Heavy12)
{
     // main test loop

     for (int k = 0; k != K_MAX; ++k) {

          MatrixDense<double> matA;
          MatrixDense<double> matB;
          MatrixDense<double> matC;
          MatrixDense<double> matD;

          matA.Allocate(ROWS);
          matB.Allocate(ROWS);
          matC.Allocate(ROWS);
          matD.Allocate(ROWS);

          matA.Random(0.0, 1.0, 10);
          matB.Random(0.0, 1.0, 20);
          matC.Random(0.0, 1.0, 10);
          matD.Random(0.0, 1.0, 20);

          // 1

          for (int ik = 0; ik != 10; ++ik) {
               matA.Swap(matB);
          }

          EXPECT_EQ(matA, matC);
          EXPECT_EQ(matB, matD);

          // 2

          for (int ik = 0; ik != 11; ++ik) {
               matA.Swap(matB);
          }

          EXPECT_EQ(matA, matD);
          EXPECT_EQ(matB, matC);
     }
}

//===========//
// test # 13 //
//===========//

TEST(MatrixTest13, Heavy13)
{
     // main test loop

     for (int k = 0; k != K_MAX; ++k) {

          VectorDense<double> vecA;
          VectorDense<double> vecB;
          VectorDense<double> vecC;
          VectorDense<double> vecD;

          vecA.Allocate(ROWS);
          vecB.Allocate(ROWS);
          vecC.Allocate(ROWS);
          vecD.Allocate(ROWS);

          vecA.Random(0.0, 1.0, 10);
          vecB.Random(0.0, 1.0, 20);
          vecC.Random(0.0, 1.0, 10);
          vecD.Random(0.0, 1.0, 20);

          // 1

          for (int ik = 0; ik != 10; ++ik) {
               vecA.Swap(vecB);
          }

          EXPECT_EQ(vecA, vecC);
          EXPECT_EQ(vecB, vecD);

          // 2

          for (int ik = 0; ik != 11; ++ik) {
               vecA.Swap(vecB);
          }

          EXPECT_EQ(vecA, vecD);
          EXPECT_EQ(vecB, vecC);
     }
}

//===========//
// test # 14 //
//===========//

TEST(MatrixTest14, Heavy14)
{
     // main test loop

     for (int k = 0; k != K_MAX; ++k) {

          MatrixDense<double> matA;
          MatrixDense<double> matB;

          matA.Allocate(ROWS);
          matB.Allocate(ROWS);

          // 1

          matA.Range(0.0);
          matB.Range(0.0);

          matA.Transpose();

          for (int ik = 0; ik != ROWS*ROWS; ++ik) {
               EXPECT_EQ(matA(ik), matB[ik]);
          }

          // 2

          matA.Range(0.0);
          matB.Range(0.0);

          matA.TransposePGG();

          for (int ik = 0; ik != ROWS*ROWS; ++ik) {
               EXPECT_EQ(matA(ik), matB[ik]);
          }

          // 3

          matA.Range(0.0);
          matB.Range(0.0);

          matA.TransposeMKL();

          for (int ik = 0; ik != ROWS*ROWS; ++ik) {
               EXPECT_EQ(matA(ik), matB[ik]);
          }

          // 4

          matA.Range(0.0);
          matB.Range(0.0);

          matA.TransposeGSL();

          for (int ik = 0; ik != ROWS*ROWS; ++ik) {
               EXPECT_EQ(matA(ik), matB[ik]);
          }
     }
}

//===========//
// test # 15 //
//===========//

TEST(MatrixTest15, Heavy15)
{
     // main test loop

     for (int k = 0; k != K_MAX; ++k) {

          MatrixDense<double> matA;
          MatrixDense<double> matB;

          matA.Allocate(ROWS);
          matB.Allocate(ROWS);

          // 1

          matA.Range(0.0);
          matB.Range(0.0);

          for (int ik = 0; ik != 11; ++ik) {
               matA.Transpose(matA);
          }

          for (int ik = 0; ik != ROWS*ROWS; ++ik) {
               EXPECT_EQ(matA(ik), matB[ik]);
          }

          // 2

          matA.Range(0.0);
          matB.Range(0.0);

          for (int ik = 0; ik != 11; ++ik) {
               matA.TransposePGG(matA);
          }

          for (int ik = 0; ik != ROWS*ROWS; ++ik) {
               EXPECT_EQ(matA(ik), matB[ik]);
          }

          // 3

          matA.Range(0.0);
          matB.Range(0.0);

          for (int ik = 0; ik != 11; ++ik) {
               matA.TransposeMKL(matA);
          }

          for (int ik = 0; ik != ROWS*ROWS; ++ik) {
               EXPECT_EQ(matA(ik), matB[ik]);
          }

          // 4

          matA.Range(0.0);
          matB.Range(0.0);

          for (int ik = 0; ik != 11; ++ik) {
               matA.TransposeGSL(matA);
          }

          for (int ik = 0; ik != ROWS*ROWS; ++ik) {
               EXPECT_EQ(matA(ik), matB[ik]);
          }
     }
}

//===========//
// test # 16 //
//===========//

TEST(MatrixTest16, Heavy16)
{
     // main test loop

     for (int k = 0; k != K_MAX; ++k) {

          MatrixDense<double> matA;
          MatrixDense<double> matB;

          matA.Allocate(ROWS);
          matB.Allocate(ROWS);

          // 1

          matA.Range(0.0);
          matB.Range(0.0);

          for (int ik = 0; ik != 11; ++ik) {
               matA.Transpose(matA);
          }

          for (int ik = 0; ik != ROWS*ROWS; ++ik) {
               EXPECT_EQ(matA(ik), matB[ik]);
          }

          // 2

          matA.Range(0.0);
          matB.Range(0.0);

          for (int ik = 0; ik != 11; ++ik) {
               matA.Transpose(matA, "PGG");
          }

          for (int ik = 0; ik != ROWS*ROWS; ++ik) {
               EXPECT_EQ(matA(ik), matB[ik]);
          }

          // 3

          matA.Range(0.0);
          matB.Range(0.0);

          for (int ik = 0; ik != 11; ++ik) {
               matA.Transpose(matA, "MKL");
          }

          for (int ik = 0; ik != ROWS*ROWS; ++ik) {
               EXPECT_EQ(matA(ik), matB[ik]);
          }

          // 4

          matA.Range(0.0);
          matB.Range(0.0);

          for (int ik = 0; ik != 11; ++ik) {
               matA.Transpose(matA, "GSL");
          }

          for (int ik = 0; ik != ROWS*ROWS; ++ik) {
               EXPECT_EQ(matA(ik), matB[ik]);
          }
     }
}

//===========//
// test # 17 //
//===========//

TEST(MatrixTest17, Heavy17)
{
     // main test loop

     for (int k = 0; k != K_MAX; ++k) {

          MatrixDense<std::complex<double>> matA;
          MatrixDense<std::complex<double>> matB;
          MatrixDense<std::complex<double>> matC;

          matA.Allocate(ROWS);
          matB.Allocate(ROWS);
          matC.Allocate(ROWS);

          matA = {1.0, 2.0};
          matB = {2.0, 3.0};
          matC = {0.0, 0.0};

          // 1

          matC = matA + matB;

          EXPECT_TRUE(matC == matA+matB);

          // 2

          matC = matA - matB;

          EXPECT_TRUE(matC == matA - matB);

          // 3

          matC = matA * matB;

          EXPECT_TRUE(matC == matA * matB);

          // 4

          matC = matA / matB;

          EXPECT_TRUE(matC == matA / matB);

          // 5

          matC = matA + 10.0;

          EXPECT_EQ(matC, std::complex<double>( {11.0, 2.0}));

          // 6

          matC = matA + std::complex<double> {10.0, 11.0};

          EXPECT_EQ(matC, std::complex<double>( {11.0, 13.0}));

          // 7

          matC = matA - 10.0;

          EXPECT_TRUE(matC == std::complex<double>( {-9.0, 2.0}));

          // 8

          matC = matA * std::complex<double> {10.0, 11.0};

          EXPECT_TRUE(matC == std::complex<double>( {-12.0, 31.0}));

          // 9

          matA.Deallocate();
          matB.Deallocate();
          matC.Deallocate();
     }
}

//===========//
// test # 18 //
//===========//

TEST(MatrixTest18, Heavy18)
{
     const std::complex<double> ONE_ONE_CMPLX = {1.0, 1.0};
     const std::complex<double> TWO_TWO_CMPLX = {2.0, 2.0};
     const std::complex<double> THREE_THREE_CMPLX = {3.0, 3.0};
     const double ZERO_DBL = 0.0;
     const double ONE_DBL = 1.0;
     const double TWO_DBL = 2.0;
     const double THREE_DBL = 3.0;
     const std::complex<double> ZERO_ONE_CMPLX = {0.0, 1.0};
     const std::complex<double> ZERO_TWO_CMPLX = {0.0, 2.0};
     const std::complex<double> ZERO_THREE_CMPLX = {0.0, 3.0};
     const std::complex<double> ONE_ZERO_CMPLX = {1.0, 0.0};
     const std::complex<double> TWO_ZERO_CMPLX = {2.0, 0.0};
     const std::complex<double> THREE_ZERO_CMPLX = {3.0, 0.0};

     const auto K_MAX_LOC = 50;

     // main test loop

     for (int k = 0; k != K_MAX; ++k) {

          MatrixDense<std::complex<double>> matA;
          MatrixDense<std::complex<double>> matB;
          MatrixDense<std::complex<double>> matC;

          // allocate heap space

          matA.Allocate(ROWS);
          matB.Allocate(ROWS);
          matC.Allocate(ROWS);

          // 1

          for (int i = 0; i != 10; ++i) {
               matA.Set(ONE_ONE_CMPLX);
               matB.Set(TWO_TWO_CMPLX);
               matC.Set(THREE_THREE_CMPLX);
          }

          EXPECT_TRUE(matA.Equal(ONE_ONE_CMPLX, ZERO_DBL));
          EXPECT_TRUE(matB.Equal(TWO_TWO_CMPLX, ZERO_DBL));
          EXPECT_TRUE(matC.Equal(THREE_THREE_CMPLX, ZERO_DBL));

          EXPECT_TRUE(matA.Equal(ONE_ONE_CMPLX));
          EXPECT_TRUE(matB.Equal(TWO_TWO_CMPLX));
          EXPECT_TRUE(matC.Equal(THREE_THREE_CMPLX));

          EXPECT_TRUE(matA.GreaterEqual(ONE_ONE_CMPLX));
          EXPECT_TRUE(matB.GreaterEqual(TWO_TWO_CMPLX));
          EXPECT_TRUE(matC.GreaterEqual(THREE_THREE_CMPLX));

          EXPECT_TRUE(matA.LessEqual(ONE_ONE_CMPLX));
          EXPECT_TRUE(matB.LessEqual(TWO_TWO_CMPLX));
          EXPECT_TRUE(matC.LessEqual(THREE_THREE_CMPLX));

          EXPECT_TRUE(matA.Greater(ZERO_DBL)) ;
          EXPECT_TRUE(matB.Greater(ONE_ONE_CMPLX)) ;
          EXPECT_TRUE(matC.Greater(TWO_TWO_CMPLX)) ;

          EXPECT_TRUE(matA.Less(TWO_TWO_CMPLX));
          EXPECT_TRUE(matB.Less(THREE_THREE_CMPLX));
          EXPECT_TRUE(matC.Less( {4.0,5.0}));

          // 2

          for (int i = 0; i != K_MAX_LOC; ++i) {
               matA.Set(matA);
               matB.Set(matA);
               matC.Set(matA);
          }

          EXPECT_TRUE(matA.Equal(ONE_ONE_CMPLX, ZERO_DBL));
          EXPECT_TRUE(matB.Equal(ONE_ONE_CMPLX, ZERO_DBL));
          EXPECT_TRUE(matC.Equal(ONE_ONE_CMPLX, ZERO_DBL));

          EXPECT_TRUE(matA.Equal(ONE_ONE_CMPLX));
          EXPECT_TRUE(matB.Equal(ONE_ONE_CMPLX));
          EXPECT_TRUE(matC.Equal(ONE_ONE_CMPLX));

          EXPECT_TRUE(matA.GreaterEqual(matA));
          EXPECT_TRUE(matB.GreaterEqual(matA));
          EXPECT_TRUE(matC.GreaterEqual(matA));

          EXPECT_TRUE(matA.LessEqual(matA));
          EXPECT_TRUE(matB.LessEqual(matA));
          EXPECT_TRUE(matC.LessEqual(matA));

          EXPECT_TRUE(matA.Greater(ZERO_DBL));
          EXPECT_TRUE(matB.Greater(ZERO_DBL));
          EXPECT_TRUE(matC.Greater(ZERO_DBL));

          EXPECT_TRUE(matA.Less(TWO_TWO_CMPLX));
          EXPECT_TRUE(matB.Less(THREE_THREE_CMPLX));
          EXPECT_TRUE(matC.Less( {4.0,5.0}));

          // 3

          for (int i = 0; i != K_MAX_LOC; ++i) {
               matA.Set(ZERO_DBL);
               matB.Set(ONE_DBL);
               matC.Set(TWO_DBL);
          }

          EXPECT_TRUE(matA.Equal(ZERO_DBL, ZERO_DBL));
          EXPECT_TRUE(matB.Equal(ONE_DBL, ZERO_DBL));
          EXPECT_TRUE(matC.Equal(TWO_DBL, ZERO_DBL));

          EXPECT_TRUE(matA.Equal(ZERO_DBL));
          EXPECT_TRUE(matB.Equal(ONE_DBL));
          EXPECT_TRUE(matC.Equal(TWO_DBL));

          EXPECT_TRUE(matA.GreaterEqual(ZERO_DBL));
          EXPECT_TRUE(matB.GreaterEqual(ONE_DBL));
          EXPECT_TRUE(matC.GreaterEqual(TWO_DBL));

          EXPECT_TRUE(matA.LessEqual(ZERO_DBL));
          EXPECT_TRUE(matB.LessEqual(ONE_DBL));
          EXPECT_TRUE(matC.LessEqual(TWO_DBL));

          EXPECT_TRUE(!matA.Greater(ZERO_DBL));
          EXPECT_TRUE(matB.GreaterEqual(ZERO_DBL));
          EXPECT_TRUE(matC.GreaterEqual(ONE_DBL));

          EXPECT_TRUE(matA.Less(TWO_TWO_CMPLX));
          EXPECT_TRUE(matB.Less(THREE_THREE_CMPLX));
          EXPECT_TRUE(matC.Less( {4.0,5.0}));

          // 4

          for (int i = 0; i != K_MAX_LOC; ++i) {
               matA.Set(ZERO_ONE_CMPLX);
               matB.Set(ZERO_TWO_CMPLX);
               matC.Set(ZERO_THREE_CMPLX);
          }

          EXPECT_TRUE(matA.Equal(ZERO_ONE_CMPLX, ZERO_DBL));
          EXPECT_TRUE(matB.Equal(ZERO_TWO_CMPLX, ZERO_DBL));
          EXPECT_TRUE(matC.Equal(ZERO_THREE_CMPLX, ZERO_DBL));

          EXPECT_TRUE(matA.Equal(ZERO_ONE_CMPLX));
          EXPECT_TRUE(matB.Equal(ZERO_TWO_CMPLX));
          EXPECT_TRUE(matC.Equal(ZERO_THREE_CMPLX));

          // 5

          for (int i = 0; i != K_MAX_LOC; ++i) {
               matA.Set(ONE_ZERO_CMPLX);
               matB.Set(TWO_ZERO_CMPLX);
               matC.Set(THREE_ZERO_CMPLX);
          }

          EXPECT_TRUE(matA.Equal(ONE_DBL, ZERO_DBL));
          EXPECT_TRUE(matB.Equal(TWO_DBL, ZERO_DBL));
          EXPECT_TRUE(matC.Equal(THREE_DBL, ZERO_DBL));

          EXPECT_TRUE(matA.Equal(ONE_DBL));
          EXPECT_TRUE(matB.Equal(TWO_DBL));
          EXPECT_TRUE(matC.Equal(THREE_DBL));

          matA.SetElement(1, 1, {10.0, 10.0});
          matB.SetElement(1, 1, {10.0, 10.0});
          matC.SetElement(1, 1, {10.0, 10.0});

          EXPECT_TRUE(!matA.Equal(ONE_DBL));
          EXPECT_TRUE(!matB.Equal(TWO_DBL));
          EXPECT_TRUE(!matC.Equal(THREE_DBL));

          EXPECT_TRUE(matA.NotEqual(ONE_DBL));
          EXPECT_TRUE(matB.NotEqual(TWO_DBL));
          EXPECT_TRUE(matC.NotEqual(THREE_DBL));

          // deallocate

          matA.Deallocate();
          matB.Deallocate();
          matC.Deallocate();
     }
}

//===========//
// test # 19 //
//===========//

TEST(MatrixTest19, Heavy19)
{
     MatrixDense<double> matA;
     MatrixDense<double> eigenvectorsA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 5) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 5) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigensystemDSYEVD(eigenvectorsA, eigenvaluesA);

     // test

     const double arr_values[100] = {-323.38478082263765, -318.95649570052416, -259.5812044929057,
                                     -255.47902023149175, -210.8574832531544, -207.046095983574,
                                     -172.17668982103106, -169.63822144706373, -167.11362728657673,
                                     -164.65335127248554, -137.93078894722365, -135.63554168942136,
                                     -133.27655590262873, -130.90313825750582, -111.16491214977447,
                                     -108.69201485977163, -104.86333072392988, -102.09654131372476,
                                     -89.52930445237439, -87.11011352113846, -79.24098389717554,
                                     -76.62676551546362, -71.01471245774239, -68.78074078937429,
                                     -57.39730933358366, -55.597048251741185, -53.807888693814036,
                                     -52.082337027364105, -42.001162570408376, -40.10702874995469,
                                     -36.13941170041208, -34.10866137118017, -29.537625276581757,
                                     -27.807512581350473, -20.493485216120316, -19.180726411630474,
                                     -17.86587524596272, -16.594313970872253, -10.527129255160773,
                                     -9.235915164234662, -6.274431175607891, -4.736234460618425,
                                     -3.113902006580256, -2.3717281394985243, -0.07129875210819334,
                                     2.4178759119876623, 3.3310614360036803, 4.154955748257782,
                                     8.633411865685325, 10.366556352869488, 12.095377640289747,
                                     14.018873556316715, 17.48114915683801, 21.917608573061386,
                                     23.316848960689295, 27.074595655585338, 31.322004721507344,
                                     34.9238763246869, 36.70168716147582, 39.12924266179792,
                                     48.291435480841905, 50.073858853444456, 52.12562986320187,
                                     57.27293729356433, 64.70819943681298, 66.93714122637961,
                                     68.82639047739008, 80.9665198923797, 83.5742202822415,
                                     86.58950836310686, 89.44951473838972, 102.06705367044331,
                                     107.13994334023343, 109.44821746324266, 117.55286230732882,
                                     126.41194103599305, 133.5454916018076, 135.893724579225,
                                     151.52771532059967, 157.8972863072518, 165.22044899155756,
                                     167.77180305861472, 190.84742611813815, 205.77684982675112,
                                     208.5174101551291, 234.35823130936174, 279.92197126989515,
                                     330.06056030550724, 384.6942438609073, 444.0868835363212,
                                     508.62422446622827, 578.8097468653175, 655.2951900145777,
                                     738.9379634908137, 830.8998363959786, 932.8239020042909,
                                     1047.1805607335104, 1178.0384559097015, 1333.1755507750008,
                                     1532.5774637949444
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -10.0));
     }

     // test #2

     EXPECT_TRUE(res == 0);
}

//===========//
// test # 20 //
//===========//

TEST(MatrixTest20, Heavy20)
{
     MatrixDense<double> matA;
     MatrixDense<double> eigenvectorsA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 20) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 20) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigensystemDSYEVD(eigenvectorsA, eigenvaluesA);

     // test

     const double arr_values[100] = {-890.0679762687605,-833.96613152625,-387.9183497645402,
                                     -374.28858505936836,-325.97417429278767,-290.39317003462327,
                                     -243.09349980476634,-237.9671765884564,-184.65425879239723,
                                     -181.6433889406905,-152.42678092373745,-150.01440312367828,
                                     -140.20043836510496,-134.26239413119825,-130.50508513840435,
                                     -128.1365612192653,-119.6243787096752,-117.25234191992651,
                                     -112.47792700587912,-108.89915545206071,-104.74759545668331,
                                     -100.22968339474058,-94.96995783385485,-89.69665862307328,
                                     -86.4677796222291,-68.14673474241698,-66.07309844841615,
                                     -56.302362373674626,-54.63592515665142,-49.54128744936952,
                                     -47.77783428990662,-45.159157747842364,-43.112766536500985,
                                     -40.904881657341534,-38.624975888999664,-36.21821364136139,
                                     -33.61319247902676,-30.618705849873916,-26.992630355951256,
                                     -19.965626399359778,-9.761220034345175,-0.009675776079236078,
                                     -8.793180471674402e-7,-4.675655622579791e-14,-3.291874702847949e-14,
                                     -3.0408119421278934e-14,-2.2328449483329286e-14,
                                     -1.0693016765542536e-14,-7.240806196368183e-15,
                                     -5.761950647030487e-15,-5.957409950992936e-16,3.2491920424847875e-15,
                                     9.219910025371442e-15,1.2799168101873962e-14,1.7108898977066272e-14,
                                     2.855032832850196e-14,4.1048723593164446e-14,9.99499970701814,
                                     18.470571911228987,30.51830887197781,33.54443547978839,
                                     36.1607309907248,38.57320314001222,40.85924266944485,
                                     43.035391586290125,45.287513370945284,47.151147140980484,
                                     50.73276704593593,52.39952223577173,59.50068014643575,
                                     61.31379159039717,74.51253995296709,77.13729578952395,
                                     94.37203226002364,99.43109411760354,102.6487394625419,
                                     105.06218043709777,107.8179491492106,110.12126119755328,
                                     113.57945167126782,116.37801423876415,122.99832759829658,
                                     125.12209476580418,138.05692785144052,140.21814403606146,
                                     161.31660187081715,164.54676840029904,175.43647569692263,
                                     192.3282320950376,207.53405143733352,212.25537253729775,
                                     284.3628999051823,294.6884623476941,341.98290548531804,
                                     507.44043882416463,534.3382764375148,967.6392677402239,
                                     1875.507871717115,3171.7801027349824,5101.18005605357
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -10.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);

     // test data # 1

     const double arr_values99[100] = {-0.00002379437463800071,-0.0000321126535355192,
                                       -0.00004261209009056319,-0.00005574228105018768,
                                       -0.0000720258070448845,-0.00009206699311457821,
                                       -0.00011656127426176481,-0.00014630515846035676,
                                       -0.0001822067730093074,-0.00022529697290878234,
                                       -0.00027674098203948964,-0.0003378505293342626,
                                       -0.00041009643284298355,-0.0004951215746125915,
                                       -0.0005947541986465926,-0.0007110214528956958,-0.0008461630842956044,
                                       -0.001002645183353377,-0.0011831738617460526,-0.0013907087329034266,
                                       -0.0016283827619966063,-0.0018997449209395513,-0.002208573606467658,
                                       -0.002558944309118129,-0.0029552087127267114,-0.0034019962501085446,
                                       -0.003904213875493434,-0.00446704386484559,-0.00509593944935361,
                                       -0.005796618082542783,-0.006575052137809326,-0.007437456830885415,
                                       -0.00839027516100404,-0.009440159665541611,-0.010593950785877937,
                                       -0.01185865164733851,-0.013241399063586366,-0.014749430585927434,
                                       -0.0163900474309,-0.018170573135449803,-0.0200983089054282,
                                       -0.022180481856340313,-0.024424190854787288,-0.026834080780647522,
                                       -0.02941440937535356,-0.03216899598488147,-0.035101168178730414,
                                       -0.03821370638490854,-0.04150878671030463,-0.04498792214706611,
                                       -0.04865190239866414,-0.05250073259412269,-0.056533571195309565,
                                       -0.06074866744009344,-0.0651432987033876,-0.06971370819842077,
                                       -0.07445504348175375,-0.07936129626731185,-0.08442524409670342,
                                       -0.08963839445497533,-0.09499093194079421,-0.10047166921101545,
                                       -0.10595789789842808,-0.11143628534678302,-0.11689273862688677,
                                       -0.1223124096438104,-0.12767970465965733,-0.13297829857254914,
                                       -0.13819115429332646,-0.1433005475596976,-0.14828809752292466,
                                       -0.15313480343431787,-0.15782108774752324,-0.16232684593753477,
                                       -0.16663150331824728,-0.17071407911689127,-0.17455325803557586,
                                       -0.17812746949713273,-0.18141497473424503,-0.18439396183722762,
                                       -0.18704264882716626,-0.18541168339207653,-0.18356676120058393,
                                       -0.18150272926411598,-0.17921485423076686,-0.17669886945503793,
                                       -0.17395102341224067,-0.17096812928970723,-0.1677476155625255,
                                       -0.1642875773360043,-0.16058682821057293,-0.15664495239745393,
                                       -0.15246235678535205,-0.14804032262974395,-0.14338105650732136,
                                       -0.13848774014895246,-0.1333645787354236,-0.128016847211483,
                                       -0.1224509341456258,-0.11667438263597216
                                      };

     // test data # 2

     const double arr_values98[100] = {0.0005490815006598427,0.0007217004212979288,0.0009319727820738912,
                                       0.0011856764796110383,0.0014891369301256077,0.0018492353084341568,
                                       0.002273410759398609,0.002769655716932461,0.003346503443557062,
                                       0.00401300689217423,0.0047787079930774546,0.00565359648519078,
                                       0.006648057443072511,0.007772806702325777,0.009038813457661714,
                                       0.010457209401826704,0.012039183891666605,0.013795864771304657,
                                       0.01573818465305239,0.01787673265523282,0.020218129530799774,
                                       0.022773522732384158,0.02555088599547039,0.02855699048006569,
                                       0.03179740706616532,0.03527631554658473,0.038996305859039175,
                                       0.04295817276333242,0.047160705650423344,0.05160047546600458,
                                       0.056271621039461366,0.061165637426456754,0.06627116919601259,
                                       0.07157381191620707,0.0770559254111312,0.08269646266941802,
                                       0.08847081857459133,0.09435070289203276,0.10030404217816305,
                                       0.10629491546542182,0.11228359420772163,0.11822646848984367,
                                       0.12407615562938958,0.12979275139874066,0.13533385216700752,
                                       0.14065467360130016,0.14570820649639804,0.1504454123825699,
                                       0.15481546139101132,0.15876601462676446,0.16224355300490953,
                                       0.1651937541424425,0.16756191846119972,0.16929344514268863,
                                       0.17033435798074814,0.17063188050057534,0.17013505895196374,
                                       0.16879543094112387,0.16656773654130375,0.16341066772151153,
                                       0.1592876487959431,0.15416764632985688,0.14836208495033895,
                                       0.14186022973422263,0.1346552528267162,0.12674464039743252,
                                       0.118130599994188,0.10882046380600134,0.09882708283296263,
                                       0.08816920644738636,0.07687184132594177,0.06496658324539006,
                                       0.052491914775380344,0.039493461481783246,0.026024198885649517,
                                       0.012144602119448178,-0.002077270001960313,-0.0165657649080691,
                                       -0.0312377086295896,-0.04600256891175273,-0.06076271707085829,
                                       -0.06795927022946352,-0.07503806683258157,-0.0819579039270416,
                                       -0.08867572326732134,-0.09514679596043041,-0.1013249415936529,
                                       -0.10716278331612594,-0.1126120400798519,-0.11762385692798413,
                                       -0.12214917384544385,-0.12613913325700105,-0.12954552576940606,
                                       -0.1323212732062947,-0.13442094737761986,-0.13580132236057105,
                                       -0.13642195734879647,-0.13624580635507777,-0.1352398502347542,
                                       -0.13337574564008783
                                      };

     // test data # 3

     const double arr_values97[100] = {-0.0048844047935863765,-0.006236486880389232,-0.007815274325331554,
                                       -0.009640183890712983,-0.011730050533458039,-0.014102771342375649,
                                       -0.016774922205544347,-0.019761352037216788,-0.023074760743478643,
                                       -0.026725268499719908,-0.03071998531275152,-0.03506259120033407,
                                       -0.03975293858755401,-0.04478668963230909,-0.050155002084304526,
                                       -0.05584427788164485,-0.06183598892150303,-0.06810659423095557,
                                       -0.07462756203769172,-0.08136550893055443,-0.08823038013976417,
                                       -0.09520849071940404,-0.1022476308045662,-0.10929156383600795,
                                       -0.11627847844623534,-0.12314131678294159,-0.1298082479646105,
                                       -0.13620329680129123,-0.14224713548726084,-0.14785804289917964,
                                       -0.15295303238959757,-0.1574491445459654,-0.1612648963069822,
                                       -0.16432187213530286,-0.16654643671328404,-0.16787154196597193,
                                       -0.16823859426980445,-0.16759934066305365,-0.16591772596198778,
                                       -0.1631716661722737,-0.15935634408053131,-0.15448321146294608,
                                       -0.14858218139776455,-0.1416381780493364,-0.1336490652093021,
                                       -0.12462715724623677,-0.11460064260169803,-0.10361487512764765,
                                       -0.09173348472751607,-0.07903925562502323,-0.06563471836746396,
                                       -0.051642400613860905,-0.03720468211196078,-0.022483201281379775,
                                       -0.007657764735299888,0.007075282886553215,0.021505263042677962,
                                       0.03540996481701064,0.04855905391412624,0.06071808324036558,
                                       0.07165316959606502,0.08113608035225883,0.09009849834439126,
                                       0.09838663571146652,0.10584187751947657,0.11230295059241002,
                                       0.11760842867321217,0.12159956720711124,0.12412345180969263,
                                       0.1250364341022301,0.12420781716730643,0.12152374055295379,
                                       0.11689120174753254,0.11024213763900717,0.10153747600764697,
                                       0.09077105399613189,0.07797328823931185,0.06321447046527728,
                                       0.04660755350441126,0.028310286413503014,0.008526547885299433,
                                       0.0025719295116293645,-0.004200598749307477,-0.011762440439929252,
                                       -0.020069965854842452,-0.029063538293552106,-0.038666727752430484,
                                       -0.048785756486008615,-0.05930922217827011,-0.07010814475633873,
                                       -0.08103638195706307,-0.09193145642839495,-0.10261583322770457,
                                       -0.11289868090088843,-0.12257814174844638,-0.1314441273038225,
                                       -0.139281643409791,-0.14587463558538188,-0.15101032970590494,
                                       -0.15448402553323257
                                      };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,99)) - abs(arr_values99[i])) < pow(10.0, -10.0));
     }

     // test # 2

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,98)) - abs(arr_values98[i])) < pow(10.0, -10.0));
     }

     // test # 3

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,97)) - abs(arr_values97[i])) < pow(10.0, -10.0));
     }
}

//===========//
// test # 21 //
//===========//

TEST(MatrixTest21, Heavy21)
{
     MatrixDense<double> matA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 5) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 5) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvaluesDSYEVD(eigenvaluesA);

     // test

     const double arr_values[100] = {-323.38478082263765, -318.95649570052416, -259.5812044929057,
                                     -255.47902023149175, -210.8574832531544, -207.046095983574,
                                     -172.17668982103106, -169.63822144706373, -167.11362728657673,
                                     -164.65335127248554, -137.93078894722365, -135.63554168942136,
                                     -133.27655590262873, -130.90313825750582, -111.16491214977447,
                                     -108.69201485977163, -104.86333072392988, -102.09654131372476,
                                     -89.52930445237439, -87.11011352113846, -79.24098389717554,
                                     -76.62676551546362, -71.01471245774239, -68.78074078937429,
                                     -57.39730933358366, -55.597048251741185, -53.807888693814036,
                                     -52.082337027364105, -42.001162570408376, -40.10702874995469,
                                     -36.13941170041208, -34.10866137118017, -29.537625276581757,
                                     -27.807512581350473, -20.493485216120316, -19.180726411630474,
                                     -17.86587524596272, -16.594313970872253, -10.527129255160773,
                                     -9.235915164234662, -6.274431175607891, -4.736234460618425,
                                     -3.113902006580256, -2.3717281394985243, -0.07129875210819334,
                                     2.4178759119876623, 3.3310614360036803, 4.154955748257782,
                                     8.633411865685325, 10.366556352869488, 12.095377640289747,
                                     14.018873556316715, 17.48114915683801, 21.917608573061386,
                                     23.316848960689295, 27.074595655585338, 31.322004721507344,
                                     34.9238763246869, 36.70168716147582, 39.12924266179792,
                                     48.291435480841905, 50.073858853444456, 52.12562986320187,
                                     57.27293729356433, 64.70819943681298, 66.93714122637961,
                                     68.82639047739008, 80.9665198923797, 83.5742202822415,
                                     86.58950836310686, 89.44951473838972, 102.06705367044331,
                                     107.13994334023343, 109.44821746324266, 117.55286230732882,
                                     126.41194103599305, 133.5454916018076, 135.893724579225,
                                     151.52771532059967, 157.8972863072518, 165.22044899155756,
                                     167.77180305861472, 190.84742611813815, 205.77684982675112,
                                     208.5174101551291, 234.35823130936174, 279.92197126989515,
                                     330.06056030550724, 384.6942438609073, 444.0868835363212,
                                     508.62422446622827, 578.8097468653175, 655.2951900145777,
                                     738.9379634908137, 830.8998363959786, 932.8239020042909,
                                     1047.1805607335104, 1178.0384559097015, 1333.1755507750008,
                                     1532.5774637949444
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -10.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//===========//
// test # 22 //
//===========//

TEST(MatrixTest22, Heavy22)
{
     MatrixDense<double> matA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 20) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 20) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvaluesDSYEVD(eigenvaluesA);

     // test

     const double arr_values[100] = {-890.0679762687605,-833.96613152625,-387.9183497645402,
                                     -374.28858505936836,-325.97417429278767,-290.39317003462327,
                                     -243.09349980476634,-237.9671765884564,-184.65425879239723,
                                     -181.6433889406905,-152.42678092373745,-150.01440312367828,
                                     -140.20043836510496,-134.26239413119825,-130.50508513840435,
                                     -128.1365612192653,-119.6243787096752,-117.25234191992651,
                                     -112.47792700587912,-108.89915545206071,-104.74759545668331,
                                     -100.22968339474058,-94.96995783385485,-89.69665862307328,
                                     -86.4677796222291,-68.14673474241698,-66.07309844841615,
                                     -56.302362373674626,-54.63592515665142,-49.54128744936952,
                                     -47.77783428990662,-45.159157747842364,-43.112766536500985,
                                     -40.904881657341534,-38.624975888999664,-36.21821364136139,
                                     -33.61319247902676,-30.618705849873916,-26.992630355951256,
                                     -19.965626399359778,-9.761220034345175,-0.009675776079236078,
                                     -8.793180471674402e-7,-4.675655622579791e-14,-3.291874702847949e-14,
                                     -3.0408119421278934e-14,-2.2328449483329286e-14,
                                     -1.0693016765542536e-14,-7.240806196368183e-15,
                                     -5.761950647030487e-15,-5.957409950992936e-16,3.2491920424847875e-15,
                                     9.219910025371442e-15,1.2799168101873962e-14,1.7108898977066272e-14,
                                     2.855032832850196e-14,4.1048723593164446e-14,9.99499970701814,
                                     18.470571911228987,30.51830887197781,33.54443547978839,
                                     36.1607309907248,38.57320314001222,40.85924266944485,
                                     43.035391586290125,45.287513370945284,47.151147140980484,
                                     50.73276704593593,52.39952223577173,59.50068014643575,
                                     61.31379159039717,74.51253995296709,77.13729578952395,
                                     94.37203226002364,99.43109411760354,102.6487394625419,
                                     105.06218043709777,107.8179491492106,110.12126119755328,
                                     113.57945167126782,116.37801423876415,122.99832759829658,
                                     125.12209476580418,138.05692785144052,140.21814403606146,
                                     161.31660187081715,164.54676840029904,175.43647569692263,
                                     192.3282320950376,207.53405143733352,212.25537253729775,
                                     284.3628999051823,294.6884623476941,341.98290548531804,
                                     507.44043882416463,534.3382764375148,967.6392677402239,
                                     1875.507871717115,3171.7801027349824,5101.18005605357
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -10.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//===========//
// test # 23 //
//===========//

TEST(MatrixTest23, Heavy23)
{
     MatrixDense<double> matA;
     MatrixDense<double> eigenvectorsA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 5) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 5) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvectorsDSYEVD(eigenvectorsA);

     // test

     const double arr_values99[100] = {3.6942314255214093e-22,
                                       1.9280574390556084e-21,
                                       8.336207484683114e-21,
                                       3.201738809930229e-20,
                                       1.128792365433472e-19,
                                       3.7219958073502635e-19,
                                       1.1616570563876596e-18,
                                       3.4603856776191363e-18,
                                       9.898106164891213e-18,
                                       2.7313034552506115e-17,
                                       7.297235551812733e-17,
                                       1.8931798526470812e-16,
                                       4.781008453916819e-16,
                                       1.1776583982769647e-15,
                                       2.8342405979299284e-15,
                                       6.674396729414062e-15,
                                       1.5399303294661802e-14,
                                       3.4849026635393604e-14,
                                       7.743020050094637e-14,
                                       1.6906046576799098e-13,
                                       3.630170569572799e-13,
                                       7.671355827326599e-13,
                                       1.5964469231753074e-12,
                                       3.2735991022182145e-12,
                                       6.617806646334326e-12,
                                       1.3195608156949319e-11,
                                       2.596350061894761e-11,
                                       5.0430333328453936e-11,
                                       9.67336578559975e-11,
                                       1.8330342627363303e-10,
                                       3.4324925188064113e-10,
                                       6.353651498186134e-10,
                                       1.1628708815038037e-9,
                                       2.104965971919157e-9,
                                       3.7693601315726985e-9,
                                       6.6787563519161844e-9,
                                       1.1711672807554324e-8,
                                       2.03292597117336e-8,
                                       3.4936698501777865e-8,
                                       5.9452893015691615e-8,
                                       1.0019897315247485e-7,
                                       1.672693861786821e-7,
                                       2.7662540139461026e-7,
                                       4.5325802559019753e-7,
                                       7.359163073250683e-7,
                                       1.1841012055800893e-6,
                                       1.8883009979218705e-6,
                                       2.984811021145573e-6,
                                       4.676960526639804e-6,
                                       7.265186654807506e-6,
                                       0.000011189174362740128,
                                       0.00001708624363917233,
                                       0.000025871327250800013,
                                       0.00003884524735147213,
                                       0.000057839551835632114,
                                       0.0000854078666854305,
                                       0.00012507547298611522,
                                       0.0001816604867751283,
                                       0.0002616813987570242,
                                       0.00037386653150919884,
                                       0.0005297808051872514,
                                       0.0007445835839285321,
                                       0.0010379277320564163,
                                       0.0014350037062010874,
                                       0.001967722735545796,
                                       0.0026760192519511626,
                                       0.0036092342363378766,
                                       0.00482751783582054,
                                       0.00640315989292051,
                                       0.008421723355192411,
                                       0.010982819682174948,
                                       0.014200329986124536,
                                       0.018201824732492763,
                                       0.023126912983358,
                                       0.029124247016656085,
                                       0.03634692535629019,
                                       0.04494587964943125,
                                       0.055061139884829774,
                                       0.0668110106541383,
                                       0.08027936243852593,
                                       0.09549945152005936,
                                       0.11243731017783884,
                                       0.13097574227719463,
                                       0.15090013051721174,
                                       0.17186879268938443,
                                       0.19341147439162046,
                                       0.21493488140894385,
                                       0.23573601738701488,
                                       0.2548580930562397,
                                       0.27133687410309687,
                                       0.28424352285526244,
                                       0.29273026481733555,
                                       0.29466646943807095,
                                       0.28996990773813897,
                                       0.27878021480447934,
                                       0.26146670556930435,
                                       0.22712948196564564,
                                       0.19198046679049208,
                                       0.15683996268683814,
                                       0.1225409827758915
                                      };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,99)) - abs(arr_values99[i])) < pow(10.0, -10.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//===========//
// test # 24 //
//===========//

TEST(MatrixTest24, Heavy24)
{
     MatrixDense<double> matA;
     MatrixDense<double> eigenvectorsA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 20) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 20) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvectorsDSYEVD(eigenvectorsA);

     // test data # 1

     const double arr_values99[100] = {-0.00002379437463800071,-0.0000321126535355192,
                                       -0.00004261209009056319,-0.00005574228105018768,
                                       -0.0000720258070448845,-0.00009206699311457821,
                                       -0.00011656127426176481,-0.00014630515846035676,
                                       -0.0001822067730093074,-0.00022529697290878234,
                                       -0.00027674098203948964,-0.0003378505293342626,
                                       -0.00041009643284298355,-0.0004951215746125915,
                                       -0.0005947541986465926,-0.0007110214528956958,-0.0008461630842956044,
                                       -0.001002645183353377,-0.0011831738617460526,-0.0013907087329034266,
                                       -0.0016283827619966063,-0.0018997449209395513,-0.002208573606467658,
                                       -0.002558944309118129,-0.0029552087127267114,-0.0034019962501085446,
                                       -0.003904213875493434,-0.00446704386484559,-0.00509593944935361,
                                       -0.005796618082542783,-0.006575052137809326,-0.007437456830885415,
                                       -0.00839027516100404,-0.009440159665541611,-0.010593950785877937,
                                       -0.01185865164733851,-0.013241399063586366,-0.014749430585927434,
                                       -0.0163900474309,-0.018170573135449803,-0.0200983089054282,
                                       -0.022180481856340313,-0.024424190854787288,-0.026834080780647522,
                                       -0.02941440937535356,-0.03216899598488147,-0.035101168178730414,
                                       -0.03821370638490854,-0.04150878671030463,-0.04498792214706611,
                                       -0.04865190239866414,-0.05250073259412269,-0.056533571195309565,
                                       -0.06074866744009344,-0.0651432987033876,-0.06971370819842077,
                                       -0.07445504348175375,-0.07936129626731185,-0.08442524409670342,
                                       -0.08963839445497533,-0.09499093194079421,-0.10047166921101545,
                                       -0.10595789789842808,-0.11143628534678302,-0.11689273862688677,
                                       -0.1223124096438104,-0.12767970465965733,-0.13297829857254914,
                                       -0.13819115429332646,-0.1433005475596976,-0.14828809752292466,
                                       -0.15313480343431787,-0.15782108774752324,-0.16232684593753477,
                                       -0.16663150331824728,-0.17071407911689127,-0.17455325803557586,
                                       -0.17812746949713273,-0.18141497473424503,-0.18439396183722762,
                                       -0.18704264882716626,-0.18541168339207653,-0.18356676120058393,
                                       -0.18150272926411598,-0.17921485423076686,-0.17669886945503793,
                                       -0.17395102341224067,-0.17096812928970723,-0.1677476155625255,
                                       -0.1642875773360043,-0.16058682821057293,-0.15664495239745393,
                                       -0.15246235678535205,-0.14804032262974395,-0.14338105650732136,
                                       -0.13848774014895246,-0.1333645787354236,-0.128016847211483,
                                       -0.1224509341456258,-0.11667438263597216
                                      };

     // test data # 2

     const double arr_values98[100] = {0.0005490815006598427,0.0007217004212979288,0.0009319727820738912,
                                       0.0011856764796110383,0.0014891369301256077,0.0018492353084341568,
                                       0.002273410759398609,0.002769655716932461,0.003346503443557062,
                                       0.00401300689217423,0.0047787079930774546,0.00565359648519078,
                                       0.006648057443072511,0.007772806702325777,0.009038813457661714,
                                       0.010457209401826704,0.012039183891666605,0.013795864771304657,
                                       0.01573818465305239,0.01787673265523282,0.020218129530799774,
                                       0.022773522732384158,0.02555088599547039,0.02855699048006569,
                                       0.03179740706616532,0.03527631554658473,0.038996305859039175,
                                       0.04295817276333242,0.047160705650423344,0.05160047546600458,
                                       0.056271621039461366,0.061165637426456754,0.06627116919601259,
                                       0.07157381191620707,0.0770559254111312,0.08269646266941802,
                                       0.08847081857459133,0.09435070289203276,0.10030404217816305,
                                       0.10629491546542182,0.11228359420772163,0.11822646848984367,
                                       0.12407615562938958,0.12979275139874066,0.13533385216700752,
                                       0.14065467360130016,0.14570820649639804,0.1504454123825699,
                                       0.15481546139101132,0.15876601462676446,0.16224355300490953,
                                       0.1651937541424425,0.16756191846119972,0.16929344514268863,
                                       0.17033435798074814,0.17063188050057534,0.17013505895196374,
                                       0.16879543094112387,0.16656773654130375,0.16341066772151153,
                                       0.1592876487959431,0.15416764632985688,0.14836208495033895,
                                       0.14186022973422263,0.1346552528267162,0.12674464039743252,
                                       0.118130599994188,0.10882046380600134,0.09882708283296263,
                                       0.08816920644738636,0.07687184132594177,0.06496658324539006,
                                       0.052491914775380344,0.039493461481783246,0.026024198885649517,
                                       0.012144602119448178,-0.002077270001960313,-0.0165657649080691,
                                       -0.0312377086295896,-0.04600256891175273,-0.06076271707085829,
                                       -0.06795927022946352,-0.07503806683258157,-0.0819579039270416,
                                       -0.08867572326732134,-0.09514679596043041,-0.1013249415936529,
                                       -0.10716278331612594,-0.1126120400798519,-0.11762385692798413,
                                       -0.12214917384544385,-0.12613913325700105,-0.12954552576940606,
                                       -0.1323212732062947,-0.13442094737761986,-0.13580132236057105,
                                       -0.13642195734879647,-0.13624580635507777,-0.1352398502347542,
                                       -0.13337574564008783
                                      };

     // test data # 3

     const double arr_values97[100] = {-0.0048844047935863765,-0.006236486880389232,-0.007815274325331554,
                                       -0.009640183890712983,-0.011730050533458039,-0.014102771342375649,
                                       -0.016774922205544347,-0.019761352037216788,-0.023074760743478643,
                                       -0.026725268499719908,-0.03071998531275152,-0.03506259120033407,
                                       -0.03975293858755401,-0.04478668963230909,-0.050155002084304526,
                                       -0.05584427788164485,-0.06183598892150303,-0.06810659423095557,
                                       -0.07462756203769172,-0.08136550893055443,-0.08823038013976417,
                                       -0.09520849071940404,-0.1022476308045662,-0.10929156383600795,
                                       -0.11627847844623534,-0.12314131678294159,-0.1298082479646105,
                                       -0.13620329680129123,-0.14224713548726084,-0.14785804289917964,
                                       -0.15295303238959757,-0.1574491445459654,-0.1612648963069822,
                                       -0.16432187213530286,-0.16654643671328404,-0.16787154196597193,
                                       -0.16823859426980445,-0.16759934066305365,-0.16591772596198778,
                                       -0.1631716661722737,-0.15935634408053131,-0.15448321146294608,
                                       -0.14858218139776455,-0.1416381780493364,-0.1336490652093021,
                                       -0.12462715724623677,-0.11460064260169803,-0.10361487512764765,
                                       -0.09173348472751607,-0.07903925562502323,-0.06563471836746396,
                                       -0.051642400613860905,-0.03720468211196078,-0.022483201281379775,
                                       -0.007657764735299888,0.007075282886553215,0.021505263042677962,
                                       0.03540996481701064,0.04855905391412624,0.06071808324036558,
                                       0.07165316959606502,0.08113608035225883,0.09009849834439126,
                                       0.09838663571146652,0.10584187751947657,0.11230295059241002,
                                       0.11760842867321217,0.12159956720711124,0.12412345180969263,
                                       0.1250364341022301,0.12420781716730643,0.12152374055295379,
                                       0.11689120174753254,0.11024213763900717,0.10153747600764697,
                                       0.09077105399613189,0.07797328823931185,0.06321447046527728,
                                       0.04660755350441126,0.028310286413503014,0.008526547885299433,
                                       0.0025719295116293645,-0.004200598749307477,-0.011762440439929252,
                                       -0.020069965854842452,-0.029063538293552106,-0.038666727752430484,
                                       -0.048785756486008615,-0.05930922217827011,-0.07010814475633873,
                                       -0.08103638195706307,-0.09193145642839495,-0.10261583322770457,
                                       -0.11289868090088843,-0.12257814174844638,-0.1314441273038225,
                                       -0.139281643409791,-0.14587463558538188,-0.15101032970590494,
                                       -0.15448402553323257
                                      };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,99)) - abs(arr_values99[i])) < pow(10.0, -10.0));
     }

     // test # 2

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,98)) - abs(arr_values98[i])) < pow(10.0, -10.0));
     }

     // test # 3

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,97)) - abs(arr_values97[i])) < pow(10.0, -10.0));
     }

     // test # 4

     EXPECT_TRUE(res == 0);
}

//===========//
// test # 25 //
//===========//

TEST(MatrixTest25, Heavy25)
{
     MatrixDense<double> matA;
     MatrixDense<double> eigenvectorsA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 5) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 5) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigensystemDSYEVR(eigenvectorsA, eigenvaluesA);

     // test

     const double arr_values[100] = {-323.38478082263765, -318.95649570052416, -259.5812044929057,
                                     -255.47902023149175, -210.8574832531544, -207.046095983574,
                                     -172.17668982103106, -169.63822144706373, -167.11362728657673,
                                     -164.65335127248554, -137.93078894722365, -135.63554168942136,
                                     -133.27655590262873, -130.90313825750582, -111.16491214977447,
                                     -108.69201485977163, -104.86333072392988, -102.09654131372476,
                                     -89.52930445237439, -87.11011352113846, -79.24098389717554,
                                     -76.62676551546362, -71.01471245774239, -68.78074078937429,
                                     -57.39730933358366, -55.597048251741185, -53.807888693814036,
                                     -52.082337027364105, -42.001162570408376, -40.10702874995469,
                                     -36.13941170041208, -34.10866137118017, -29.537625276581757,
                                     -27.807512581350473, -20.493485216120316, -19.180726411630474,
                                     -17.86587524596272, -16.594313970872253, -10.527129255160773,
                                     -9.235915164234662, -6.274431175607891, -4.736234460618425,
                                     -3.113902006580256, -2.3717281394985243, -0.07129875210819334,
                                     2.4178759119876623, 3.3310614360036803, 4.154955748257782,
                                     8.633411865685325, 10.366556352869488, 12.095377640289747,
                                     14.018873556316715, 17.48114915683801, 21.917608573061386,
                                     23.316848960689295, 27.074595655585338, 31.322004721507344,
                                     34.9238763246869, 36.70168716147582, 39.12924266179792,
                                     48.291435480841905, 50.073858853444456, 52.12562986320187,
                                     57.27293729356433, 64.70819943681298, 66.93714122637961,
                                     68.82639047739008, 80.9665198923797, 83.5742202822415,
                                     86.58950836310686, 89.44951473838972, 102.06705367044331,
                                     107.13994334023343, 109.44821746324266, 117.55286230732882,
                                     126.41194103599305, 133.5454916018076, 135.893724579225,
                                     151.52771532059967, 157.8972863072518, 165.22044899155756,
                                     167.77180305861472, 190.84742611813815, 205.77684982675112,
                                     208.5174101551291, 234.35823130936174, 279.92197126989515,
                                     330.06056030550724, 384.6942438609073, 444.0868835363212,
                                     508.62422446622827, 578.8097468653175, 655.2951900145777,
                                     738.9379634908137, 830.8998363959786, 932.8239020042909,
                                     1047.1805607335104, 1178.0384559097015, 1333.1755507750008,
                                     1532.5774637949444
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -10.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//===========//
// test # 26 //
//===========//

TEST(MatrixTest26, Heavy26)
{
     MatrixDense<double> matA;
     MatrixDense<double> eigenvectorsA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 20) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 20) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigensystemDSYEVR(eigenvectorsA, eigenvaluesA);

     // test

     const double arr_values[100] = {-890.0679762687605,-833.96613152625,-387.9183497645402,
                                     -374.28858505936836,-325.97417429278767,-290.39317003462327,
                                     -243.09349980476634,-237.9671765884564,-184.65425879239723,
                                     -181.6433889406905,-152.42678092373745,-150.01440312367828,
                                     -140.20043836510496,-134.26239413119825,-130.50508513840435,
                                     -128.1365612192653,-119.6243787096752,-117.25234191992651,
                                     -112.47792700587912,-108.89915545206071,-104.74759545668331,
                                     -100.22968339474058,-94.96995783385485,-89.69665862307328,
                                     -86.4677796222291,-68.14673474241698,-66.07309844841615,
                                     -56.302362373674626,-54.63592515665142,-49.54128744936952,
                                     -47.77783428990662,-45.159157747842364,-43.112766536500985,
                                     -40.904881657341534,-38.624975888999664,-36.21821364136139,
                                     -33.61319247902676,-30.618705849873916,-26.992630355951256,
                                     -19.965626399359778,-9.761220034345175,-0.009675776079236078,
                                     -8.793180471674402e-7,-4.675655622579791e-14,-3.291874702847949e-14,
                                     -3.0408119421278934e-14,-2.2328449483329286e-14,
                                     -1.0693016765542536e-14,-7.240806196368183e-15,
                                     -5.761950647030487e-15,-5.957409950992936e-16,3.2491920424847875e-15,
                                     9.219910025371442e-15,1.2799168101873962e-14,1.7108898977066272e-14,
                                     2.855032832850196e-14,4.1048723593164446e-14,9.99499970701814,
                                     18.470571911228987,30.51830887197781,33.54443547978839,
                                     36.1607309907248,38.57320314001222,40.85924266944485,
                                     43.035391586290125,45.287513370945284,47.151147140980484,
                                     50.73276704593593,52.39952223577173,59.50068014643575,
                                     61.31379159039717,74.51253995296709,77.13729578952395,
                                     94.37203226002364,99.43109411760354,102.6487394625419,
                                     105.06218043709777,107.8179491492106,110.12126119755328,
                                     113.57945167126782,116.37801423876415,122.99832759829658,
                                     125.12209476580418,138.05692785144052,140.21814403606146,
                                     161.31660187081715,164.54676840029904,175.43647569692263,
                                     192.3282320950376,207.53405143733352,212.25537253729775,
                                     284.3628999051823,294.6884623476941,341.98290548531804,
                                     507.44043882416463,534.3382764375148,967.6392677402239,
                                     1875.507871717115,3171.7801027349824,5101.18005605357
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -10.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);

     // test data # 1

     const double arr_values99[100] = {-0.00002379437463800071,-0.0000321126535355192,
                                       -0.00004261209009056319,-0.00005574228105018768,
                                       -0.0000720258070448845,-0.00009206699311457821,
                                       -0.00011656127426176481,-0.00014630515846035676,
                                       -0.0001822067730093074,-0.00022529697290878234,
                                       -0.00027674098203948964,-0.0003378505293342626,
                                       -0.00041009643284298355,-0.0004951215746125915,
                                       -0.0005947541986465926,-0.0007110214528956958,-0.0008461630842956044,
                                       -0.001002645183353377,-0.0011831738617460526,-0.0013907087329034266,
                                       -0.0016283827619966063,-0.0018997449209395513,-0.002208573606467658,
                                       -0.002558944309118129,-0.0029552087127267114,-0.0034019962501085446,
                                       -0.003904213875493434,-0.00446704386484559,-0.00509593944935361,
                                       -0.005796618082542783,-0.006575052137809326,-0.007437456830885415,
                                       -0.00839027516100404,-0.009440159665541611,-0.010593950785877937,
                                       -0.01185865164733851,-0.013241399063586366,-0.014749430585927434,
                                       -0.0163900474309,-0.018170573135449803,-0.0200983089054282,
                                       -0.022180481856340313,-0.024424190854787288,-0.026834080780647522,
                                       -0.02941440937535356,-0.03216899598488147,-0.035101168178730414,
                                       -0.03821370638490854,-0.04150878671030463,-0.04498792214706611,
                                       -0.04865190239866414,-0.05250073259412269,-0.056533571195309565,
                                       -0.06074866744009344,-0.0651432987033876,-0.06971370819842077,
                                       -0.07445504348175375,-0.07936129626731185,-0.08442524409670342,
                                       -0.08963839445497533,-0.09499093194079421,-0.10047166921101545,
                                       -0.10595789789842808,-0.11143628534678302,-0.11689273862688677,
                                       -0.1223124096438104,-0.12767970465965733,-0.13297829857254914,
                                       -0.13819115429332646,-0.1433005475596976,-0.14828809752292466,
                                       -0.15313480343431787,-0.15782108774752324,-0.16232684593753477,
                                       -0.16663150331824728,-0.17071407911689127,-0.17455325803557586,
                                       -0.17812746949713273,-0.18141497473424503,-0.18439396183722762,
                                       -0.18704264882716626,-0.18541168339207653,-0.18356676120058393,
                                       -0.18150272926411598,-0.17921485423076686,-0.17669886945503793,
                                       -0.17395102341224067,-0.17096812928970723,-0.1677476155625255,
                                       -0.1642875773360043,-0.16058682821057293,-0.15664495239745393,
                                       -0.15246235678535205,-0.14804032262974395,-0.14338105650732136,
                                       -0.13848774014895246,-0.1333645787354236,-0.128016847211483,
                                       -0.1224509341456258,-0.11667438263597216
                                      };

     // test data # 2

     const double arr_values98[100] = {0.0005490815006598427,0.0007217004212979288,0.0009319727820738912,
                                       0.0011856764796110383,0.0014891369301256077,0.0018492353084341568,
                                       0.002273410759398609,0.002769655716932461,0.003346503443557062,
                                       0.00401300689217423,0.0047787079930774546,0.00565359648519078,
                                       0.006648057443072511,0.007772806702325777,0.009038813457661714,
                                       0.010457209401826704,0.012039183891666605,0.013795864771304657,
                                       0.01573818465305239,0.01787673265523282,0.020218129530799774,
                                       0.022773522732384158,0.02555088599547039,0.02855699048006569,
                                       0.03179740706616532,0.03527631554658473,0.038996305859039175,
                                       0.04295817276333242,0.047160705650423344,0.05160047546600458,
                                       0.056271621039461366,0.061165637426456754,0.06627116919601259,
                                       0.07157381191620707,0.0770559254111312,0.08269646266941802,
                                       0.08847081857459133,0.09435070289203276,0.10030404217816305,
                                       0.10629491546542182,0.11228359420772163,0.11822646848984367,
                                       0.12407615562938958,0.12979275139874066,0.13533385216700752,
                                       0.14065467360130016,0.14570820649639804,0.1504454123825699,
                                       0.15481546139101132,0.15876601462676446,0.16224355300490953,
                                       0.1651937541424425,0.16756191846119972,0.16929344514268863,
                                       0.17033435798074814,0.17063188050057534,0.17013505895196374,
                                       0.16879543094112387,0.16656773654130375,0.16341066772151153,
                                       0.1592876487959431,0.15416764632985688,0.14836208495033895,
                                       0.14186022973422263,0.1346552528267162,0.12674464039743252,
                                       0.118130599994188,0.10882046380600134,0.09882708283296263,
                                       0.08816920644738636,0.07687184132594177,0.06496658324539006,
                                       0.052491914775380344,0.039493461481783246,0.026024198885649517,
                                       0.012144602119448178,-0.002077270001960313,-0.0165657649080691,
                                       -0.0312377086295896,-0.04600256891175273,-0.06076271707085829,
                                       -0.06795927022946352,-0.07503806683258157,-0.0819579039270416,
                                       -0.08867572326732134,-0.09514679596043041,-0.1013249415936529,
                                       -0.10716278331612594,-0.1126120400798519,-0.11762385692798413,
                                       -0.12214917384544385,-0.12613913325700105,-0.12954552576940606,
                                       -0.1323212732062947,-0.13442094737761986,-0.13580132236057105,
                                       -0.13642195734879647,-0.13624580635507777,-0.1352398502347542,
                                       -0.13337574564008783
                                      };

     // test data # 3

     const double arr_values97[100] = {-0.0048844047935863765,-0.006236486880389232,-0.007815274325331554,
                                       -0.009640183890712983,-0.011730050533458039,-0.014102771342375649,
                                       -0.016774922205544347,-0.019761352037216788,-0.023074760743478643,
                                       -0.026725268499719908,-0.03071998531275152,-0.03506259120033407,
                                       -0.03975293858755401,-0.04478668963230909,-0.050155002084304526,
                                       -0.05584427788164485,-0.06183598892150303,-0.06810659423095557,
                                       -0.07462756203769172,-0.08136550893055443,-0.08823038013976417,
                                       -0.09520849071940404,-0.1022476308045662,-0.10929156383600795,
                                       -0.11627847844623534,-0.12314131678294159,-0.1298082479646105,
                                       -0.13620329680129123,-0.14224713548726084,-0.14785804289917964,
                                       -0.15295303238959757,-0.1574491445459654,-0.1612648963069822,
                                       -0.16432187213530286,-0.16654643671328404,-0.16787154196597193,
                                       -0.16823859426980445,-0.16759934066305365,-0.16591772596198778,
                                       -0.1631716661722737,-0.15935634408053131,-0.15448321146294608,
                                       -0.14858218139776455,-0.1416381780493364,-0.1336490652093021,
                                       -0.12462715724623677,-0.11460064260169803,-0.10361487512764765,
                                       -0.09173348472751607,-0.07903925562502323,-0.06563471836746396,
                                       -0.051642400613860905,-0.03720468211196078,-0.022483201281379775,
                                       -0.007657764735299888,0.007075282886553215,0.021505263042677962,
                                       0.03540996481701064,0.04855905391412624,0.06071808324036558,
                                       0.07165316959606502,0.08113608035225883,0.09009849834439126,
                                       0.09838663571146652,0.10584187751947657,0.11230295059241002,
                                       0.11760842867321217,0.12159956720711124,0.12412345180969263,
                                       0.1250364341022301,0.12420781716730643,0.12152374055295379,
                                       0.11689120174753254,0.11024213763900717,0.10153747600764697,
                                       0.09077105399613189,0.07797328823931185,0.06321447046527728,
                                       0.04660755350441126,0.028310286413503014,0.008526547885299433,
                                       0.0025719295116293645,-0.004200598749307477,-0.011762440439929252,
                                       -0.020069965854842452,-0.029063538293552106,-0.038666727752430484,
                                       -0.048785756486008615,-0.05930922217827011,-0.07010814475633873,
                                       -0.08103638195706307,-0.09193145642839495,-0.10261583322770457,
                                       -0.11289868090088843,-0.12257814174844638,-0.1314441273038225,
                                       -0.139281643409791,-0.14587463558538188,-0.15101032970590494,
                                       -0.15448402553323257
                                      };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,99)) - abs(arr_values99[i])) < pow(10.0, -10.0));
     }

     // test # 2

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,98)) - abs(arr_values98[i])) < pow(10.0, -10.0));
     }

     // test # 3

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,97)) - abs(arr_values97[i])) < pow(10.0, -10.0));
     }

     // test # 4

     EXPECT_TRUE(res == 0);
}

//===========//
// test # 27 //
//===========//

TEST(MatrixTest27, Heavy27)
{
     MatrixDense<double> matA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 5) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 5) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvaluesDSYEVR(eigenvaluesA);

     // test

     const double arr_values[100] = {-323.38478082263765, -318.95649570052416, -259.5812044929057,
                                     -255.47902023149175, -210.8574832531544, -207.046095983574,
                                     -172.17668982103106, -169.63822144706373, -167.11362728657673,
                                     -164.65335127248554, -137.93078894722365, -135.63554168942136,
                                     -133.27655590262873, -130.90313825750582, -111.16491214977447,
                                     -108.69201485977163, -104.86333072392988, -102.09654131372476,
                                     -89.52930445237439, -87.11011352113846, -79.24098389717554,
                                     -76.62676551546362, -71.01471245774239, -68.78074078937429,
                                     -57.39730933358366, -55.597048251741185, -53.807888693814036,
                                     -52.082337027364105, -42.001162570408376, -40.10702874995469,
                                     -36.13941170041208, -34.10866137118017, -29.537625276581757,
                                     -27.807512581350473, -20.493485216120316, -19.180726411630474,
                                     -17.86587524596272, -16.594313970872253, -10.527129255160773,
                                     -9.235915164234662, -6.274431175607891, -4.736234460618425,
                                     -3.113902006580256, -2.3717281394985243, -0.07129875210819334,
                                     2.4178759119876623, 3.3310614360036803, 4.154955748257782,
                                     8.633411865685325, 10.366556352869488, 12.095377640289747,
                                     14.018873556316715, 17.48114915683801, 21.917608573061386,
                                     23.316848960689295, 27.074595655585338, 31.322004721507344,
                                     34.9238763246869, 36.70168716147582, 39.12924266179792,
                                     48.291435480841905, 50.073858853444456, 52.12562986320187,
                                     57.27293729356433, 64.70819943681298, 66.93714122637961,
                                     68.82639047739008, 80.9665198923797, 83.5742202822415,
                                     86.58950836310686, 89.44951473838972, 102.06705367044331,
                                     107.13994334023343, 109.44821746324266, 117.55286230732882,
                                     126.41194103599305, 133.5454916018076, 135.893724579225,
                                     151.52771532059967, 157.8972863072518, 165.22044899155756,
                                     167.77180305861472, 190.84742611813815, 205.77684982675112,
                                     208.5174101551291, 234.35823130936174, 279.92197126989515,
                                     330.06056030550724, 384.6942438609073, 444.0868835363212,
                                     508.62422446622827, 578.8097468653175, 655.2951900145777,
                                     738.9379634908137, 830.8998363959786, 932.8239020042909,
                                     1047.1805607335104, 1178.0384559097015, 1333.1755507750008,
                                     1532.5774637949444
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -5.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//===========//
// test # 28 //
//===========//

TEST(MatrixTest28, Heavy28)
{
     MatrixDense<double> matA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 20) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 20) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvaluesDSYEVR(eigenvaluesA);

     // test

     const double arr_values[100] = {-890.0679762687605,-833.96613152625,-387.9183497645402,
                                     -374.28858505936836,-325.97417429278767,-290.39317003462327,
                                     -243.09349980476634,-237.9671765884564,-184.65425879239723,
                                     -181.6433889406905,-152.42678092373745,-150.01440312367828,
                                     -140.20043836510496,-134.26239413119825,-130.50508513840435,
                                     -128.1365612192653,-119.6243787096752,-117.25234191992651,
                                     -112.47792700587912,-108.89915545206071,-104.74759545668331,
                                     -100.22968339474058,-94.96995783385485,-89.69665862307328,
                                     -86.4677796222291,-68.14673474241698,-66.07309844841615,
                                     -56.302362373674626,-54.63592515665142,-49.54128744936952,
                                     -47.77783428990662,-45.159157747842364,-43.112766536500985,
                                     -40.904881657341534,-38.624975888999664,-36.21821364136139,
                                     -33.61319247902676,-30.618705849873916,-26.992630355951256,
                                     -19.965626399359778,-9.761220034345175,-0.009675776079236078,
                                     -8.793180471674402e-7,-4.675655622579791e-14,-3.291874702847949e-14,
                                     -3.0408119421278934e-14,-2.2328449483329286e-14,
                                     -1.0693016765542536e-14,-7.240806196368183e-15,
                                     -5.761950647030487e-15,-5.957409950992936e-16,3.2491920424847875e-15,
                                     9.219910025371442e-15,1.2799168101873962e-14,1.7108898977066272e-14,
                                     2.855032832850196e-14,4.1048723593164446e-14,9.99499970701814,
                                     18.470571911228987,30.51830887197781,33.54443547978839,
                                     36.1607309907248,38.57320314001222,40.85924266944485,
                                     43.035391586290125,45.287513370945284,47.151147140980484,
                                     50.73276704593593,52.39952223577173,59.50068014643575,
                                     61.31379159039717,74.51253995296709,77.13729578952395,
                                     94.37203226002364,99.43109411760354,102.6487394625419,
                                     105.06218043709777,107.8179491492106,110.12126119755328,
                                     113.57945167126782,116.37801423876415,122.99832759829658,
                                     125.12209476580418,138.05692785144052,140.21814403606146,
                                     161.31660187081715,164.54676840029904,175.43647569692263,
                                     192.3282320950376,207.53405143733352,212.25537253729775,
                                     284.3628999051823,294.6884623476941,341.98290548531804,
                                     507.44043882416463,534.3382764375148,967.6392677402239,
                                     1875.507871717115,3171.7801027349824,5101.18005605357
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -5.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//===========//
// test # 29 //
//===========//

TEST(MatrixTest29, Heavy29)
{
     MatrixDense<double> matA;
     MatrixDense<double> eigenvectorsA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 5) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 5) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvectorsDSYEVR(eigenvectorsA);

     // test

     const double arr_values99[100] = {3.6942314255214093e-22,
                                       1.9280574390556084e-21,
                                       8.336207484683114e-21,
                                       3.201738809930229e-20,
                                       1.128792365433472e-19,
                                       3.7219958073502635e-19,
                                       1.1616570563876596e-18,
                                       3.4603856776191363e-18,
                                       9.898106164891213e-18,
                                       2.7313034552506115e-17,
                                       7.297235551812733e-17,
                                       1.8931798526470812e-16,
                                       4.781008453916819e-16,
                                       1.1776583982769647e-15,
                                       2.8342405979299284e-15,
                                       6.674396729414062e-15,
                                       1.5399303294661802e-14,
                                       3.4849026635393604e-14,
                                       7.743020050094637e-14,
                                       1.6906046576799098e-13,
                                       3.630170569572799e-13,
                                       7.671355827326599e-13,
                                       1.5964469231753074e-12,
                                       3.2735991022182145e-12,
                                       6.617806646334326e-12,
                                       1.3195608156949319e-11,
                                       2.596350061894761e-11,
                                       5.0430333328453936e-11,
                                       9.67336578559975e-11,
                                       1.8330342627363303e-10,
                                       3.4324925188064113e-10,
                                       6.353651498186134e-10,
                                       1.1628708815038037e-9,
                                       2.104965971919157e-9,
                                       3.7693601315726985e-9,
                                       6.6787563519161844e-9,
                                       1.1711672807554324e-8,
                                       2.03292597117336e-8,
                                       3.4936698501777865e-8,
                                       5.9452893015691615e-8,
                                       1.0019897315247485e-7,
                                       1.672693861786821e-7,
                                       2.7662540139461026e-7,
                                       4.5325802559019753e-7,
                                       7.359163073250683e-7,
                                       1.1841012055800893e-6,
                                       1.8883009979218705e-6,
                                       2.984811021145573e-6,
                                       4.676960526639804e-6,
                                       7.265186654807506e-6,
                                       0.000011189174362740128,
                                       0.00001708624363917233,
                                       0.000025871327250800013,
                                       0.00003884524735147213,
                                       0.000057839551835632114,
                                       0.0000854078666854305,
                                       0.00012507547298611522,
                                       0.0001816604867751283,
                                       0.0002616813987570242,
                                       0.00037386653150919884,
                                       0.0005297808051872514,
                                       0.0007445835839285321,
                                       0.0010379277320564163,
                                       0.0014350037062010874,
                                       0.001967722735545796,
                                       0.0026760192519511626,
                                       0.0036092342363378766,
                                       0.00482751783582054,
                                       0.00640315989292051,
                                       0.008421723355192411,
                                       0.010982819682174948,
                                       0.014200329986124536,
                                       0.018201824732492763,
                                       0.023126912983358,
                                       0.029124247016656085,
                                       0.03634692535629019,
                                       0.04494587964943125,
                                       0.055061139884829774,
                                       0.0668110106541383,
                                       0.08027936243852593,
                                       0.09549945152005936,
                                       0.11243731017783884,
                                       0.13097574227719463,
                                       0.15090013051721174,
                                       0.17186879268938443,
                                       0.19341147439162046,
                                       0.21493488140894385,
                                       0.23573601738701488,
                                       0.2548580930562397,
                                       0.27133687410309687,
                                       0.28424352285526244,
                                       0.29273026481733555,
                                       0.29466646943807095,
                                       0.28996990773813897,
                                       0.27878021480447934,
                                       0.26146670556930435,
                                       0.22712948196564564,
                                       0.19198046679049208,
                                       0.15683996268683814,
                                       0.1225409827758915
                                      };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,99)) - abs(arr_values99[i])) < pow(10.0, -10.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//===========//
// test # 30 //
//===========//

TEST(MatrixTest30, Heavy30)
{
     MatrixDense<double> matA;
     MatrixDense<double> eigenvectorsA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 20) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 20) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvectorsDSYEVR(eigenvectorsA);

     // test data # 1

     const double arr_values99[100] = {-0.00002379437463800071,-0.0000321126535355192,
                                       -0.00004261209009056319,-0.00005574228105018768,
                                       -0.0000720258070448845,-0.00009206699311457821,
                                       -0.00011656127426176481,-0.00014630515846035676,
                                       -0.0001822067730093074,-0.00022529697290878234,
                                       -0.00027674098203948964,-0.0003378505293342626,
                                       -0.00041009643284298355,-0.0004951215746125915,
                                       -0.0005947541986465926,-0.0007110214528956958,-0.0008461630842956044,
                                       -0.001002645183353377,-0.0011831738617460526,-0.0013907087329034266,
                                       -0.0016283827619966063,-0.0018997449209395513,-0.002208573606467658,
                                       -0.002558944309118129,-0.0029552087127267114,-0.0034019962501085446,
                                       -0.003904213875493434,-0.00446704386484559,-0.00509593944935361,
                                       -0.005796618082542783,-0.006575052137809326,-0.007437456830885415,
                                       -0.00839027516100404,-0.009440159665541611,-0.010593950785877937,
                                       -0.01185865164733851,-0.013241399063586366,-0.014749430585927434,
                                       -0.0163900474309,-0.018170573135449803,-0.0200983089054282,
                                       -0.022180481856340313,-0.024424190854787288,-0.026834080780647522,
                                       -0.02941440937535356,-0.03216899598488147,-0.035101168178730414,
                                       -0.03821370638490854,-0.04150878671030463,-0.04498792214706611,
                                       -0.04865190239866414,-0.05250073259412269,-0.056533571195309565,
                                       -0.06074866744009344,-0.0651432987033876,-0.06971370819842077,
                                       -0.07445504348175375,-0.07936129626731185,-0.08442524409670342,
                                       -0.08963839445497533,-0.09499093194079421,-0.10047166921101545,
                                       -0.10595789789842808,-0.11143628534678302,-0.11689273862688677,
                                       -0.1223124096438104,-0.12767970465965733,-0.13297829857254914,
                                       -0.13819115429332646,-0.1433005475596976,-0.14828809752292466,
                                       -0.15313480343431787,-0.15782108774752324,-0.16232684593753477,
                                       -0.16663150331824728,-0.17071407911689127,-0.17455325803557586,
                                       -0.17812746949713273,-0.18141497473424503,-0.18439396183722762,
                                       -0.18704264882716626,-0.18541168339207653,-0.18356676120058393,
                                       -0.18150272926411598,-0.17921485423076686,-0.17669886945503793,
                                       -0.17395102341224067,-0.17096812928970723,-0.1677476155625255,
                                       -0.1642875773360043,-0.16058682821057293,-0.15664495239745393,
                                       -0.15246235678535205,-0.14804032262974395,-0.14338105650732136,
                                       -0.13848774014895246,-0.1333645787354236,-0.128016847211483,
                                       -0.1224509341456258,-0.11667438263597216
                                      };

     // test data # 2

     const double arr_values98[100] = {0.0005490815006598427,0.0007217004212979288,0.0009319727820738912,
                                       0.0011856764796110383,0.0014891369301256077,0.0018492353084341568,
                                       0.002273410759398609,0.002769655716932461,0.003346503443557062,
                                       0.00401300689217423,0.0047787079930774546,0.00565359648519078,
                                       0.006648057443072511,0.007772806702325777,0.009038813457661714,
                                       0.010457209401826704,0.012039183891666605,0.013795864771304657,
                                       0.01573818465305239,0.01787673265523282,0.020218129530799774,
                                       0.022773522732384158,0.02555088599547039,0.02855699048006569,
                                       0.03179740706616532,0.03527631554658473,0.038996305859039175,
                                       0.04295817276333242,0.047160705650423344,0.05160047546600458,
                                       0.056271621039461366,0.061165637426456754,0.06627116919601259,
                                       0.07157381191620707,0.0770559254111312,0.08269646266941802,
                                       0.08847081857459133,0.09435070289203276,0.10030404217816305,
                                       0.10629491546542182,0.11228359420772163,0.11822646848984367,
                                       0.12407615562938958,0.12979275139874066,0.13533385216700752,
                                       0.14065467360130016,0.14570820649639804,0.1504454123825699,
                                       0.15481546139101132,0.15876601462676446,0.16224355300490953,
                                       0.1651937541424425,0.16756191846119972,0.16929344514268863,
                                       0.17033435798074814,0.17063188050057534,0.17013505895196374,
                                       0.16879543094112387,0.16656773654130375,0.16341066772151153,
                                       0.1592876487959431,0.15416764632985688,0.14836208495033895,
                                       0.14186022973422263,0.1346552528267162,0.12674464039743252,
                                       0.118130599994188,0.10882046380600134,0.09882708283296263,
                                       0.08816920644738636,0.07687184132594177,0.06496658324539006,
                                       0.052491914775380344,0.039493461481783246,0.026024198885649517,
                                       0.012144602119448178,-0.002077270001960313,-0.0165657649080691,
                                       -0.0312377086295896,-0.04600256891175273,-0.06076271707085829,
                                       -0.06795927022946352,-0.07503806683258157,-0.0819579039270416,
                                       -0.08867572326732134,-0.09514679596043041,-0.1013249415936529,
                                       -0.10716278331612594,-0.1126120400798519,-0.11762385692798413,
                                       -0.12214917384544385,-0.12613913325700105,-0.12954552576940606,
                                       -0.1323212732062947,-0.13442094737761986,-0.13580132236057105,
                                       -0.13642195734879647,-0.13624580635507777,-0.1352398502347542,
                                       -0.13337574564008783
                                      };

     // test data # 3

     const double arr_values97[100] = {-0.0048844047935863765,-0.006236486880389232,-0.007815274325331554,
                                       -0.009640183890712983,-0.011730050533458039,-0.014102771342375649,
                                       -0.016774922205544347,-0.019761352037216788,-0.023074760743478643,
                                       -0.026725268499719908,-0.03071998531275152,-0.03506259120033407,
                                       -0.03975293858755401,-0.04478668963230909,-0.050155002084304526,
                                       -0.05584427788164485,-0.06183598892150303,-0.06810659423095557,
                                       -0.07462756203769172,-0.08136550893055443,-0.08823038013976417,
                                       -0.09520849071940404,-0.1022476308045662,-0.10929156383600795,
                                       -0.11627847844623534,-0.12314131678294159,-0.1298082479646105,
                                       -0.13620329680129123,-0.14224713548726084,-0.14785804289917964,
                                       -0.15295303238959757,-0.1574491445459654,-0.1612648963069822,
                                       -0.16432187213530286,-0.16654643671328404,-0.16787154196597193,
                                       -0.16823859426980445,-0.16759934066305365,-0.16591772596198778,
                                       -0.1631716661722737,-0.15935634408053131,-0.15448321146294608,
                                       -0.14858218139776455,-0.1416381780493364,-0.1336490652093021,
                                       -0.12462715724623677,-0.11460064260169803,-0.10361487512764765,
                                       -0.09173348472751607,-0.07903925562502323,-0.06563471836746396,
                                       -0.051642400613860905,-0.03720468211196078,-0.022483201281379775,
                                       -0.007657764735299888,0.007075282886553215,0.021505263042677962,
                                       0.03540996481701064,0.04855905391412624,0.06071808324036558,
                                       0.07165316959606502,0.08113608035225883,0.09009849834439126,
                                       0.09838663571146652,0.10584187751947657,0.11230295059241002,
                                       0.11760842867321217,0.12159956720711124,0.12412345180969263,
                                       0.1250364341022301,0.12420781716730643,0.12152374055295379,
                                       0.11689120174753254,0.11024213763900717,0.10153747600764697,
                                       0.09077105399613189,0.07797328823931185,0.06321447046527728,
                                       0.04660755350441126,0.028310286413503014,0.008526547885299433,
                                       0.0025719295116293645,-0.004200598749307477,-0.011762440439929252,
                                       -0.020069965854842452,-0.029063538293552106,-0.038666727752430484,
                                       -0.048785756486008615,-0.05930922217827011,-0.07010814475633873,
                                       -0.08103638195706307,-0.09193145642839495,-0.10261583322770457,
                                       -0.11289868090088843,-0.12257814174844638,-0.1314441273038225,
                                       -0.139281643409791,-0.14587463558538188,-0.15101032970590494,
                                       -0.15448402553323257
                                      };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,99)) - abs(arr_values99[i])) < pow(10.0, -10.0));
     }

     // test # 2

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,98)) - abs(arr_values98[i])) < pow(10.0, -10.0));
     }

     // test # 3

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,97)) - abs(arr_values97[i])) < pow(10.0, -10.0));
     }

     // test # 4

     EXPECT_TRUE(res == 0);
}

//===========//
// test # 31 //
//===========//

TEST(MatrixTest31, Heavy31)
{
     MatrixDense<double> matA;
     MatrixDense<double> eigenvectorsA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 5) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 5) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigensystemDSYEVX(eigenvectorsA, eigenvaluesA);

     // test

     const double arr_values[100] = {-323.38478082263765, -318.95649570052416, -259.5812044929057,
                                     -255.47902023149175, -210.8574832531544, -207.046095983574,
                                     -172.17668982103106, -169.63822144706373, -167.11362728657673,
                                     -164.65335127248554, -137.93078894722365, -135.63554168942136,
                                     -133.27655590262873, -130.90313825750582, -111.16491214977447,
                                     -108.69201485977163, -104.86333072392988, -102.09654131372476,
                                     -89.52930445237439, -87.11011352113846, -79.24098389717554,
                                     -76.62676551546362, -71.01471245774239, -68.78074078937429,
                                     -57.39730933358366, -55.597048251741185, -53.807888693814036,
                                     -52.082337027364105, -42.001162570408376, -40.10702874995469,
                                     -36.13941170041208, -34.10866137118017, -29.537625276581757,
                                     -27.807512581350473, -20.493485216120316, -19.180726411630474,
                                     -17.86587524596272, -16.594313970872253, -10.527129255160773,
                                     -9.235915164234662, -6.274431175607891, -4.736234460618425,
                                     -3.113902006580256, -2.3717281394985243, -0.07129875210819334,
                                     2.4178759119876623, 3.3310614360036803, 4.154955748257782,
                                     8.633411865685325, 10.366556352869488, 12.095377640289747,
                                     14.018873556316715, 17.48114915683801, 21.917608573061386,
                                     23.316848960689295, 27.074595655585338, 31.322004721507344,
                                     34.9238763246869, 36.70168716147582, 39.12924266179792,
                                     48.291435480841905, 50.073858853444456, 52.12562986320187,
                                     57.27293729356433, 64.70819943681298, 66.93714122637961,
                                     68.82639047739008, 80.9665198923797, 83.5742202822415,
                                     86.58950836310686, 89.44951473838972, 102.06705367044331,
                                     107.13994334023343, 109.44821746324266, 117.55286230732882,
                                     126.41194103599305, 133.5454916018076, 135.893724579225,
                                     151.52771532059967, 157.8972863072518, 165.22044899155756,
                                     167.77180305861472, 190.84742611813815, 205.77684982675112,
                                     208.5174101551291, 234.35823130936174, 279.92197126989515,
                                     330.06056030550724, 384.6942438609073, 444.0868835363212,
                                     508.62422446622827, 578.8097468653175, 655.2951900145777,
                                     738.9379634908137, 830.8998363959786, 932.8239020042909,
                                     1047.1805607335104, 1178.0384559097015, 1333.1755507750008,
                                     1532.5774637949444
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -10.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//===========//
// test # 32 //
//===========//

TEST(MatrixTest32, Heavy32)
{
     MatrixDense<double> matA;
     MatrixDense<double> eigenvectorsA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 20) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 20) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigensystemDSYEVX(eigenvectorsA, eigenvaluesA);

     // test

     const double arr_values[100] = {-890.0679762687605,-833.96613152625,-387.9183497645402,
                                     -374.28858505936836,-325.97417429278767,-290.39317003462327,
                                     -243.09349980476634,-237.9671765884564,-184.65425879239723,
                                     -181.6433889406905,-152.42678092373745,-150.01440312367828,
                                     -140.20043836510496,-134.26239413119825,-130.50508513840435,
                                     -128.1365612192653,-119.6243787096752,-117.25234191992651,
                                     -112.47792700587912,-108.89915545206071,-104.74759545668331,
                                     -100.22968339474058,-94.96995783385485,-89.69665862307328,
                                     -86.4677796222291,-68.14673474241698,-66.07309844841615,
                                     -56.302362373674626,-54.63592515665142,-49.54128744936952,
                                     -47.77783428990662,-45.159157747842364,-43.112766536500985,
                                     -40.904881657341534,-38.624975888999664,-36.21821364136139,
                                     -33.61319247902676,-30.618705849873916,-26.992630355951256,
                                     -19.965626399359778,-9.761220034345175,-0.009675776079236078,
                                     -8.793180471674402e-7,-4.675655622579791e-14,-3.291874702847949e-14,
                                     -3.0408119421278934e-14,-2.2328449483329286e-14,
                                     -1.0693016765542536e-14,-7.240806196368183e-15,
                                     -5.761950647030487e-15,-5.957409950992936e-16,3.2491920424847875e-15,
                                     9.219910025371442e-15,1.2799168101873962e-14,1.7108898977066272e-14,
                                     2.855032832850196e-14,4.1048723593164446e-14,9.99499970701814,
                                     18.470571911228987,30.51830887197781,33.54443547978839,
                                     36.1607309907248,38.57320314001222,40.85924266944485,
                                     43.035391586290125,45.287513370945284,47.151147140980484,
                                     50.73276704593593,52.39952223577173,59.50068014643575,
                                     61.31379159039717,74.51253995296709,77.13729578952395,
                                     94.37203226002364,99.43109411760354,102.6487394625419,
                                     105.06218043709777,107.8179491492106,110.12126119755328,
                                     113.57945167126782,116.37801423876415,122.99832759829658,
                                     125.12209476580418,138.05692785144052,140.21814403606146,
                                     161.31660187081715,164.54676840029904,175.43647569692263,
                                     192.3282320950376,207.53405143733352,212.25537253729775,
                                     284.3628999051823,294.6884623476941,341.98290548531804,
                                     507.44043882416463,534.3382764375148,967.6392677402239,
                                     1875.507871717115,3171.7801027349824,5101.18005605357
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -10.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);

     // test data # 1

     const double arr_values99[100] = {-0.00002379437463800071,-0.0000321126535355192,
                                       -0.00004261209009056319,-0.00005574228105018768,
                                       -0.0000720258070448845,-0.00009206699311457821,
                                       -0.00011656127426176481,-0.00014630515846035676,
                                       -0.0001822067730093074,-0.00022529697290878234,
                                       -0.00027674098203948964,-0.0003378505293342626,
                                       -0.00041009643284298355,-0.0004951215746125915,
                                       -0.0005947541986465926,-0.0007110214528956958,-0.0008461630842956044,
                                       -0.001002645183353377,-0.0011831738617460526,-0.0013907087329034266,
                                       -0.0016283827619966063,-0.0018997449209395513,-0.002208573606467658,
                                       -0.002558944309118129,-0.0029552087127267114,-0.0034019962501085446,
                                       -0.003904213875493434,-0.00446704386484559,-0.00509593944935361,
                                       -0.005796618082542783,-0.006575052137809326,-0.007437456830885415,
                                       -0.00839027516100404,-0.009440159665541611,-0.010593950785877937,
                                       -0.01185865164733851,-0.013241399063586366,-0.014749430585927434,
                                       -0.0163900474309,-0.018170573135449803,-0.0200983089054282,
                                       -0.022180481856340313,-0.024424190854787288,-0.026834080780647522,
                                       -0.02941440937535356,-0.03216899598488147,-0.035101168178730414,
                                       -0.03821370638490854,-0.04150878671030463,-0.04498792214706611,
                                       -0.04865190239866414,-0.05250073259412269,-0.056533571195309565,
                                       -0.06074866744009344,-0.0651432987033876,-0.06971370819842077,
                                       -0.07445504348175375,-0.07936129626731185,-0.08442524409670342,
                                       -0.08963839445497533,-0.09499093194079421,-0.10047166921101545,
                                       -0.10595789789842808,-0.11143628534678302,-0.11689273862688677,
                                       -0.1223124096438104,-0.12767970465965733,-0.13297829857254914,
                                       -0.13819115429332646,-0.1433005475596976,-0.14828809752292466,
                                       -0.15313480343431787,-0.15782108774752324,-0.16232684593753477,
                                       -0.16663150331824728,-0.17071407911689127,-0.17455325803557586,
                                       -0.17812746949713273,-0.18141497473424503,-0.18439396183722762,
                                       -0.18704264882716626,-0.18541168339207653,-0.18356676120058393,
                                       -0.18150272926411598,-0.17921485423076686,-0.17669886945503793,
                                       -0.17395102341224067,-0.17096812928970723,-0.1677476155625255,
                                       -0.1642875773360043,-0.16058682821057293,-0.15664495239745393,
                                       -0.15246235678535205,-0.14804032262974395,-0.14338105650732136,
                                       -0.13848774014895246,-0.1333645787354236,-0.128016847211483,
                                       -0.1224509341456258,-0.11667438263597216
                                      };

     // test data # 2

     const double arr_values98[100] = {0.0005490815006598427,0.0007217004212979288,0.0009319727820738912,
                                       0.0011856764796110383,0.0014891369301256077,0.0018492353084341568,
                                       0.002273410759398609,0.002769655716932461,0.003346503443557062,
                                       0.00401300689217423,0.0047787079930774546,0.00565359648519078,
                                       0.006648057443072511,0.007772806702325777,0.009038813457661714,
                                       0.010457209401826704,0.012039183891666605,0.013795864771304657,
                                       0.01573818465305239,0.01787673265523282,0.020218129530799774,
                                       0.022773522732384158,0.02555088599547039,0.02855699048006569,
                                       0.03179740706616532,0.03527631554658473,0.038996305859039175,
                                       0.04295817276333242,0.047160705650423344,0.05160047546600458,
                                       0.056271621039461366,0.061165637426456754,0.06627116919601259,
                                       0.07157381191620707,0.0770559254111312,0.08269646266941802,
                                       0.08847081857459133,0.09435070289203276,0.10030404217816305,
                                       0.10629491546542182,0.11228359420772163,0.11822646848984367,
                                       0.12407615562938958,0.12979275139874066,0.13533385216700752,
                                       0.14065467360130016,0.14570820649639804,0.1504454123825699,
                                       0.15481546139101132,0.15876601462676446,0.16224355300490953,
                                       0.1651937541424425,0.16756191846119972,0.16929344514268863,
                                       0.17033435798074814,0.17063188050057534,0.17013505895196374,
                                       0.16879543094112387,0.16656773654130375,0.16341066772151153,
                                       0.1592876487959431,0.15416764632985688,0.14836208495033895,
                                       0.14186022973422263,0.1346552528267162,0.12674464039743252,
                                       0.118130599994188,0.10882046380600134,0.09882708283296263,
                                       0.08816920644738636,0.07687184132594177,0.06496658324539006,
                                       0.052491914775380344,0.039493461481783246,0.026024198885649517,
                                       0.012144602119448178,-0.002077270001960313,-0.0165657649080691,
                                       -0.0312377086295896,-0.04600256891175273,-0.06076271707085829,
                                       -0.06795927022946352,-0.07503806683258157,-0.0819579039270416,
                                       -0.08867572326732134,-0.09514679596043041,-0.1013249415936529,
                                       -0.10716278331612594,-0.1126120400798519,-0.11762385692798413,
                                       -0.12214917384544385,-0.12613913325700105,-0.12954552576940606,
                                       -0.1323212732062947,-0.13442094737761986,-0.13580132236057105,
                                       -0.13642195734879647,-0.13624580635507777,-0.1352398502347542,
                                       -0.13337574564008783
                                      };

     // test data # 3

     const double arr_values97[100] = {-0.0048844047935863765,-0.006236486880389232,-0.007815274325331554,
                                       -0.009640183890712983,-0.011730050533458039,-0.014102771342375649,
                                       -0.016774922205544347,-0.019761352037216788,-0.023074760743478643,
                                       -0.026725268499719908,-0.03071998531275152,-0.03506259120033407,
                                       -0.03975293858755401,-0.04478668963230909,-0.050155002084304526,
                                       -0.05584427788164485,-0.06183598892150303,-0.06810659423095557,
                                       -0.07462756203769172,-0.08136550893055443,-0.08823038013976417,
                                       -0.09520849071940404,-0.1022476308045662,-0.10929156383600795,
                                       -0.11627847844623534,-0.12314131678294159,-0.1298082479646105,
                                       -0.13620329680129123,-0.14224713548726084,-0.14785804289917964,
                                       -0.15295303238959757,-0.1574491445459654,-0.1612648963069822,
                                       -0.16432187213530286,-0.16654643671328404,-0.16787154196597193,
                                       -0.16823859426980445,-0.16759934066305365,-0.16591772596198778,
                                       -0.1631716661722737,-0.15935634408053131,-0.15448321146294608,
                                       -0.14858218139776455,-0.1416381780493364,-0.1336490652093021,
                                       -0.12462715724623677,-0.11460064260169803,-0.10361487512764765,
                                       -0.09173348472751607,-0.07903925562502323,-0.06563471836746396,
                                       -0.051642400613860905,-0.03720468211196078,-0.022483201281379775,
                                       -0.007657764735299888,0.007075282886553215,0.021505263042677962,
                                       0.03540996481701064,0.04855905391412624,0.06071808324036558,
                                       0.07165316959606502,0.08113608035225883,0.09009849834439126,
                                       0.09838663571146652,0.10584187751947657,0.11230295059241002,
                                       0.11760842867321217,0.12159956720711124,0.12412345180969263,
                                       0.1250364341022301,0.12420781716730643,0.12152374055295379,
                                       0.11689120174753254,0.11024213763900717,0.10153747600764697,
                                       0.09077105399613189,0.07797328823931185,0.06321447046527728,
                                       0.04660755350441126,0.028310286413503014,0.008526547885299433,
                                       0.0025719295116293645,-0.004200598749307477,-0.011762440439929252,
                                       -0.020069965854842452,-0.029063538293552106,-0.038666727752430484,
                                       -0.048785756486008615,-0.05930922217827011,-0.07010814475633873,
                                       -0.08103638195706307,-0.09193145642839495,-0.10261583322770457,
                                       -0.11289868090088843,-0.12257814174844638,-0.1314441273038225,
                                       -0.139281643409791,-0.14587463558538188,-0.15101032970590494,
                                       -0.15448402553323257
                                      };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,99)) - abs(arr_values99[i])) < pow(10.0, -10.0));
     }

     // test # 2

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,98)) - abs(arr_values98[i])) < pow(10.0, -10.0));
     }

     // test # 3

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,97)) - abs(arr_values97[i])) < pow(10.0, -10.0));
     }
}

//===========//
// test # 33 //
//===========//

TEST(MatrixTest33, Heavy33)
{
     MatrixDense<double> matA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 5) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 5) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvaluesDSYEVX(eigenvaluesA);

     // test

     const double arr_values[100] = {-323.38478082263765, -318.95649570052416, -259.5812044929057,
                                     -255.47902023149175, -210.8574832531544, -207.046095983574,
                                     -172.17668982103106, -169.63822144706373, -167.11362728657673,
                                     -164.65335127248554, -137.93078894722365, -135.63554168942136,
                                     -133.27655590262873, -130.90313825750582, -111.16491214977447,
                                     -108.69201485977163, -104.86333072392988, -102.09654131372476,
                                     -89.52930445237439, -87.11011352113846, -79.24098389717554,
                                     -76.62676551546362, -71.01471245774239, -68.78074078937429,
                                     -57.39730933358366, -55.597048251741185, -53.807888693814036,
                                     -52.082337027364105, -42.001162570408376, -40.10702874995469,
                                     -36.13941170041208, -34.10866137118017, -29.537625276581757,
                                     -27.807512581350473, -20.493485216120316, -19.180726411630474,
                                     -17.86587524596272, -16.594313970872253, -10.527129255160773,
                                     -9.235915164234662, -6.274431175607891, -4.736234460618425,
                                     -3.113902006580256, -2.3717281394985243, -0.07129875210819334,
                                     2.4178759119876623, 3.3310614360036803, 4.154955748257782,
                                     8.633411865685325, 10.366556352869488, 12.095377640289747,
                                     14.018873556316715, 17.48114915683801, 21.917608573061386,
                                     23.316848960689295, 27.074595655585338, 31.322004721507344,
                                     34.9238763246869, 36.70168716147582, 39.12924266179792,
                                     48.291435480841905, 50.073858853444456, 52.12562986320187,
                                     57.27293729356433, 64.70819943681298, 66.93714122637961,
                                     68.82639047739008, 80.9665198923797, 83.5742202822415,
                                     86.58950836310686, 89.44951473838972, 102.06705367044331,
                                     107.13994334023343, 109.44821746324266, 117.55286230732882,
                                     126.41194103599305, 133.5454916018076, 135.893724579225,
                                     151.52771532059967, 157.8972863072518, 165.22044899155756,
                                     167.77180305861472, 190.84742611813815, 205.77684982675112,
                                     208.5174101551291, 234.35823130936174, 279.92197126989515,
                                     330.06056030550724, 384.6942438609073, 444.0868835363212,
                                     508.62422446622827, 578.8097468653175, 655.2951900145777,
                                     738.9379634908137, 830.8998363959786, 932.8239020042909,
                                     1047.1805607335104, 1178.0384559097015, 1333.1755507750008,
                                     1532.5774637949444
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -5.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//===========//
// test # 34 //
//===========//

TEST(MatrixTest34, Heavy34)
{
     MatrixDense<double> matA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 20) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 20) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvaluesDSYEVX(eigenvaluesA);

     // test

     const double arr_values[100] = {-890.0679762687605,-833.96613152625,-387.9183497645402,
                                     -374.28858505936836,-325.97417429278767,-290.39317003462327,
                                     -243.09349980476634,-237.9671765884564,-184.65425879239723,
                                     -181.6433889406905,-152.42678092373745,-150.01440312367828,
                                     -140.20043836510496,-134.26239413119825,-130.50508513840435,
                                     -128.1365612192653,-119.6243787096752,-117.25234191992651,
                                     -112.47792700587912,-108.89915545206071,-104.74759545668331,
                                     -100.22968339474058,-94.96995783385485,-89.69665862307328,
                                     -86.4677796222291,-68.14673474241698,-66.07309844841615,
                                     -56.302362373674626,-54.63592515665142,-49.54128744936952,
                                     -47.77783428990662,-45.159157747842364,-43.112766536500985,
                                     -40.904881657341534,-38.624975888999664,-36.21821364136139,
                                     -33.61319247902676,-30.618705849873916,-26.992630355951256,
                                     -19.965626399359778,-9.761220034345175,-0.009675776079236078,
                                     -8.793180471674402e-7,-4.675655622579791e-14,-3.291874702847949e-14,
                                     -3.0408119421278934e-14,-2.2328449483329286e-14,
                                     -1.0693016765542536e-14,-7.240806196368183e-15,
                                     -5.761950647030487e-15,-5.957409950992936e-16,3.2491920424847875e-15,
                                     9.219910025371442e-15,1.2799168101873962e-14,1.7108898977066272e-14,
                                     2.855032832850196e-14,4.1048723593164446e-14,9.99499970701814,
                                     18.470571911228987,30.51830887197781,33.54443547978839,
                                     36.1607309907248,38.57320314001222,40.85924266944485,
                                     43.035391586290125,45.287513370945284,47.151147140980484,
                                     50.73276704593593,52.39952223577173,59.50068014643575,
                                     61.31379159039717,74.51253995296709,77.13729578952395,
                                     94.37203226002364,99.43109411760354,102.6487394625419,
                                     105.06218043709777,107.8179491492106,110.12126119755328,
                                     113.57945167126782,116.37801423876415,122.99832759829658,
                                     125.12209476580418,138.05692785144052,140.21814403606146,
                                     161.31660187081715,164.54676840029904,175.43647569692263,
                                     192.3282320950376,207.53405143733352,212.25537253729775,
                                     284.3628999051823,294.6884623476941,341.98290548531804,
                                     507.44043882416463,534.3382764375148,967.6392677402239,
                                     1875.507871717115,3171.7801027349824,5101.18005605357
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -5.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//===========//
// test # 35 //
//===========//

TEST(MatrixTest35, Heavy35)
{
     MatrixDense<double> matA;
     MatrixDense<double> eigenvectorsA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 5) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 5) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvectorsDSYEVX(eigenvectorsA);

     // test

     const double arr_values99[100] = {3.6942314255214093e-22,
                                       1.9280574390556084e-21,
                                       8.336207484683114e-21,
                                       3.201738809930229e-20,
                                       1.128792365433472e-19,
                                       3.7219958073502635e-19,
                                       1.1616570563876596e-18,
                                       3.4603856776191363e-18,
                                       9.898106164891213e-18,
                                       2.7313034552506115e-17,
                                       7.297235551812733e-17,
                                       1.8931798526470812e-16,
                                       4.781008453916819e-16,
                                       1.1776583982769647e-15,
                                       2.8342405979299284e-15,
                                       6.674396729414062e-15,
                                       1.5399303294661802e-14,
                                       3.4849026635393604e-14,
                                       7.743020050094637e-14,
                                       1.6906046576799098e-13,
                                       3.630170569572799e-13,
                                       7.671355827326599e-13,
                                       1.5964469231753074e-12,
                                       3.2735991022182145e-12,
                                       6.617806646334326e-12,
                                       1.3195608156949319e-11,
                                       2.596350061894761e-11,
                                       5.0430333328453936e-11,
                                       9.67336578559975e-11,
                                       1.8330342627363303e-10,
                                       3.4324925188064113e-10,
                                       6.353651498186134e-10,
                                       1.1628708815038037e-9,
                                       2.104965971919157e-9,
                                       3.7693601315726985e-9,
                                       6.6787563519161844e-9,
                                       1.1711672807554324e-8,
                                       2.03292597117336e-8,
                                       3.4936698501777865e-8,
                                       5.9452893015691615e-8,
                                       1.0019897315247485e-7,
                                       1.672693861786821e-7,
                                       2.7662540139461026e-7,
                                       4.5325802559019753e-7,
                                       7.359163073250683e-7,
                                       1.1841012055800893e-6,
                                       1.8883009979218705e-6,
                                       2.984811021145573e-6,
                                       4.676960526639804e-6,
                                       7.265186654807506e-6,
                                       0.000011189174362740128,
                                       0.00001708624363917233,
                                       0.000025871327250800013,
                                       0.00003884524735147213,
                                       0.000057839551835632114,
                                       0.0000854078666854305,
                                       0.00012507547298611522,
                                       0.0001816604867751283,
                                       0.0002616813987570242,
                                       0.00037386653150919884,
                                       0.0005297808051872514,
                                       0.0007445835839285321,
                                       0.0010379277320564163,
                                       0.0014350037062010874,
                                       0.001967722735545796,
                                       0.0026760192519511626,
                                       0.0036092342363378766,
                                       0.00482751783582054,
                                       0.00640315989292051,
                                       0.008421723355192411,
                                       0.010982819682174948,
                                       0.014200329986124536,
                                       0.018201824732492763,
                                       0.023126912983358,
                                       0.029124247016656085,
                                       0.03634692535629019,
                                       0.04494587964943125,
                                       0.055061139884829774,
                                       0.0668110106541383,
                                       0.08027936243852593,
                                       0.09549945152005936,
                                       0.11243731017783884,
                                       0.13097574227719463,
                                       0.15090013051721174,
                                       0.17186879268938443,
                                       0.19341147439162046,
                                       0.21493488140894385,
                                       0.23573601738701488,
                                       0.2548580930562397,
                                       0.27133687410309687,
                                       0.28424352285526244,
                                       0.29273026481733555,
                                       0.29466646943807095,
                                       0.28996990773813897,
                                       0.27878021480447934,
                                       0.26146670556930435,
                                       0.22712948196564564,
                                       0.19198046679049208,
                                       0.15683996268683814,
                                       0.1225409827758915
                                      };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,99)) - abs(arr_values99[i])) < pow(10.0, -10.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//===========//
// test # 36 //
//===========//

TEST(MatrixTest36, Heavy36)
{
     MatrixDense<double> matA;
     MatrixDense<double> eigenvectorsA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 20) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 20) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvectorsDSYEVX(eigenvectorsA);

     // test data # 1

     const double arr_values99[100] = {-0.00002379437463800071,-0.0000321126535355192,
                                       -0.00004261209009056319,-0.00005574228105018768,
                                       -0.0000720258070448845,-0.00009206699311457821,
                                       -0.00011656127426176481,-0.00014630515846035676,
                                       -0.0001822067730093074,-0.00022529697290878234,
                                       -0.00027674098203948964,-0.0003378505293342626,
                                       -0.00041009643284298355,-0.0004951215746125915,
                                       -0.0005947541986465926,-0.0007110214528956958,-0.0008461630842956044,
                                       -0.001002645183353377,-0.0011831738617460526,-0.0013907087329034266,
                                       -0.0016283827619966063,-0.0018997449209395513,-0.002208573606467658,
                                       -0.002558944309118129,-0.0029552087127267114,-0.0034019962501085446,
                                       -0.003904213875493434,-0.00446704386484559,-0.00509593944935361,
                                       -0.005796618082542783,-0.006575052137809326,-0.007437456830885415,
                                       -0.00839027516100404,-0.009440159665541611,-0.010593950785877937,
                                       -0.01185865164733851,-0.013241399063586366,-0.014749430585927434,
                                       -0.0163900474309,-0.018170573135449803,-0.0200983089054282,
                                       -0.022180481856340313,-0.024424190854787288,-0.026834080780647522,
                                       -0.02941440937535356,-0.03216899598488147,-0.035101168178730414,
                                       -0.03821370638490854,-0.04150878671030463,-0.04498792214706611,
                                       -0.04865190239866414,-0.05250073259412269,-0.056533571195309565,
                                       -0.06074866744009344,-0.0651432987033876,-0.06971370819842077,
                                       -0.07445504348175375,-0.07936129626731185,-0.08442524409670342,
                                       -0.08963839445497533,-0.09499093194079421,-0.10047166921101545,
                                       -0.10595789789842808,-0.11143628534678302,-0.11689273862688677,
                                       -0.1223124096438104,-0.12767970465965733,-0.13297829857254914,
                                       -0.13819115429332646,-0.1433005475596976,-0.14828809752292466,
                                       -0.15313480343431787,-0.15782108774752324,-0.16232684593753477,
                                       -0.16663150331824728,-0.17071407911689127,-0.17455325803557586,
                                       -0.17812746949713273,-0.18141497473424503,-0.18439396183722762,
                                       -0.18704264882716626,-0.18541168339207653,-0.18356676120058393,
                                       -0.18150272926411598,-0.17921485423076686,-0.17669886945503793,
                                       -0.17395102341224067,-0.17096812928970723,-0.1677476155625255,
                                       -0.1642875773360043,-0.16058682821057293,-0.15664495239745393,
                                       -0.15246235678535205,-0.14804032262974395,-0.14338105650732136,
                                       -0.13848774014895246,-0.1333645787354236,-0.128016847211483,
                                       -0.1224509341456258,-0.11667438263597216
                                      };

     // test data # 2

     const double arr_values98[100] = {0.0005490815006598427,0.0007217004212979288,0.0009319727820738912,
                                       0.0011856764796110383,0.0014891369301256077,0.0018492353084341568,
                                       0.002273410759398609,0.002769655716932461,0.003346503443557062,
                                       0.00401300689217423,0.0047787079930774546,0.00565359648519078,
                                       0.006648057443072511,0.007772806702325777,0.009038813457661714,
                                       0.010457209401826704,0.012039183891666605,0.013795864771304657,
                                       0.01573818465305239,0.01787673265523282,0.020218129530799774,
                                       0.022773522732384158,0.02555088599547039,0.02855699048006569,
                                       0.03179740706616532,0.03527631554658473,0.038996305859039175,
                                       0.04295817276333242,0.047160705650423344,0.05160047546600458,
                                       0.056271621039461366,0.061165637426456754,0.06627116919601259,
                                       0.07157381191620707,0.0770559254111312,0.08269646266941802,
                                       0.08847081857459133,0.09435070289203276,0.10030404217816305,
                                       0.10629491546542182,0.11228359420772163,0.11822646848984367,
                                       0.12407615562938958,0.12979275139874066,0.13533385216700752,
                                       0.14065467360130016,0.14570820649639804,0.1504454123825699,
                                       0.15481546139101132,0.15876601462676446,0.16224355300490953,
                                       0.1651937541424425,0.16756191846119972,0.16929344514268863,
                                       0.17033435798074814,0.17063188050057534,0.17013505895196374,
                                       0.16879543094112387,0.16656773654130375,0.16341066772151153,
                                       0.1592876487959431,0.15416764632985688,0.14836208495033895,
                                       0.14186022973422263,0.1346552528267162,0.12674464039743252,
                                       0.118130599994188,0.10882046380600134,0.09882708283296263,
                                       0.08816920644738636,0.07687184132594177,0.06496658324539006,
                                       0.052491914775380344,0.039493461481783246,0.026024198885649517,
                                       0.012144602119448178,-0.002077270001960313,-0.0165657649080691,
                                       -0.0312377086295896,-0.04600256891175273,-0.06076271707085829,
                                       -0.06795927022946352,-0.07503806683258157,-0.0819579039270416,
                                       -0.08867572326732134,-0.09514679596043041,-0.1013249415936529,
                                       -0.10716278331612594,-0.1126120400798519,-0.11762385692798413,
                                       -0.12214917384544385,-0.12613913325700105,-0.12954552576940606,
                                       -0.1323212732062947,-0.13442094737761986,-0.13580132236057105,
                                       -0.13642195734879647,-0.13624580635507777,-0.1352398502347542,
                                       -0.13337574564008783
                                      };

     // test data # 3

     const double arr_values97[100] = {-0.0048844047935863765,-0.006236486880389232,-0.007815274325331554,
                                       -0.009640183890712983,-0.011730050533458039,-0.014102771342375649,
                                       -0.016774922205544347,-0.019761352037216788,-0.023074760743478643,
                                       -0.026725268499719908,-0.03071998531275152,-0.03506259120033407,
                                       -0.03975293858755401,-0.04478668963230909,-0.050155002084304526,
                                       -0.05584427788164485,-0.06183598892150303,-0.06810659423095557,
                                       -0.07462756203769172,-0.08136550893055443,-0.08823038013976417,
                                       -0.09520849071940404,-0.1022476308045662,-0.10929156383600795,
                                       -0.11627847844623534,-0.12314131678294159,-0.1298082479646105,
                                       -0.13620329680129123,-0.14224713548726084,-0.14785804289917964,
                                       -0.15295303238959757,-0.1574491445459654,-0.1612648963069822,
                                       -0.16432187213530286,-0.16654643671328404,-0.16787154196597193,
                                       -0.16823859426980445,-0.16759934066305365,-0.16591772596198778,
                                       -0.1631716661722737,-0.15935634408053131,-0.15448321146294608,
                                       -0.14858218139776455,-0.1416381780493364,-0.1336490652093021,
                                       -0.12462715724623677,-0.11460064260169803,-0.10361487512764765,
                                       -0.09173348472751607,-0.07903925562502323,-0.06563471836746396,
                                       -0.051642400613860905,-0.03720468211196078,-0.022483201281379775,
                                       -0.007657764735299888,0.007075282886553215,0.021505263042677962,
                                       0.03540996481701064,0.04855905391412624,0.06071808324036558,
                                       0.07165316959606502,0.08113608035225883,0.09009849834439126,
                                       0.09838663571146652,0.10584187751947657,0.11230295059241002,
                                       0.11760842867321217,0.12159956720711124,0.12412345180969263,
                                       0.1250364341022301,0.12420781716730643,0.12152374055295379,
                                       0.11689120174753254,0.11024213763900717,0.10153747600764697,
                                       0.09077105399613189,0.07797328823931185,0.06321447046527728,
                                       0.04660755350441126,0.028310286413503014,0.008526547885299433,
                                       0.0025719295116293645,-0.004200598749307477,-0.011762440439929252,
                                       -0.020069965854842452,-0.029063538293552106,-0.038666727752430484,
                                       -0.048785756486008615,-0.05930922217827011,-0.07010814475633873,
                                       -0.08103638195706307,-0.09193145642839495,-0.10261583322770457,
                                       -0.11289868090088843,-0.12257814174844638,-0.1314441273038225,
                                       -0.139281643409791,-0.14587463558538188,-0.15101032970590494,
                                       -0.15448402553323257
                                      };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,99)) - abs(arr_values99[i])) < pow(10.0, -10.0));
     }

     // test # 2

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,98)) - abs(arr_values98[i])) < pow(10.0, -10.0));
     }

     // test # 3

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,97)) - abs(arr_values97[i])) < pow(10.0, -10.0));
     }

     // test # 4

     EXPECT_TRUE(res == 0);
}

//===========//
// test # 37 //
//===========//

TEST(MatrixTest37, Heavy37)
{
     MatrixDense<double> matA;
     MatrixDense<double> eigenvectorsA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 5) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 5) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigensystemDSYEV(eigenvectorsA, eigenvaluesA);

     // test

     const double arr_values[100] = {-323.38478082263765, -318.95649570052416, -259.5812044929057,
                                     -255.47902023149175, -210.8574832531544, -207.046095983574,
                                     -172.17668982103106, -169.63822144706373, -167.11362728657673,
                                     -164.65335127248554, -137.93078894722365, -135.63554168942136,
                                     -133.27655590262873, -130.90313825750582, -111.16491214977447,
                                     -108.69201485977163, -104.86333072392988, -102.09654131372476,
                                     -89.52930445237439, -87.11011352113846, -79.24098389717554,
                                     -76.62676551546362, -71.01471245774239, -68.78074078937429,
                                     -57.39730933358366, -55.597048251741185, -53.807888693814036,
                                     -52.082337027364105, -42.001162570408376, -40.10702874995469,
                                     -36.13941170041208, -34.10866137118017, -29.537625276581757,
                                     -27.807512581350473, -20.493485216120316, -19.180726411630474,
                                     -17.86587524596272, -16.594313970872253, -10.527129255160773,
                                     -9.235915164234662, -6.274431175607891, -4.736234460618425,
                                     -3.113902006580256, -2.3717281394985243, -0.07129875210819334,
                                     2.4178759119876623, 3.3310614360036803, 4.154955748257782,
                                     8.633411865685325, 10.366556352869488, 12.095377640289747,
                                     14.018873556316715, 17.48114915683801, 21.917608573061386,
                                     23.316848960689295, 27.074595655585338, 31.322004721507344,
                                     34.9238763246869, 36.70168716147582, 39.12924266179792,
                                     48.291435480841905, 50.073858853444456, 52.12562986320187,
                                     57.27293729356433, 64.70819943681298, 66.93714122637961,
                                     68.82639047739008, 80.9665198923797, 83.5742202822415,
                                     86.58950836310686, 89.44951473838972, 102.06705367044331,
                                     107.13994334023343, 109.44821746324266, 117.55286230732882,
                                     126.41194103599305, 133.5454916018076, 135.893724579225,
                                     151.52771532059967, 157.8972863072518, 165.22044899155756,
                                     167.77180305861472, 190.84742611813815, 205.77684982675112,
                                     208.5174101551291, 234.35823130936174, 279.92197126989515,
                                     330.06056030550724, 384.6942438609073, 444.0868835363212,
                                     508.62422446622827, 578.8097468653175, 655.2951900145777,
                                     738.9379634908137, 830.8998363959786, 932.8239020042909,
                                     1047.1805607335104, 1178.0384559097015, 1333.1755507750008,
                                     1532.5774637949444
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -10.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//===========//
// test # 38 //
//===========//

TEST(MatrixTest38, Heavy38)
{
     MatrixDense<double> matA;
     MatrixDense<double> eigenvectorsA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 20) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 20) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigensystemDSYEV(eigenvectorsA, eigenvaluesA);

     // test

     const double arr_values[100] = {-890.0679762687605,-833.96613152625,-387.9183497645402,
                                     -374.28858505936836,-325.97417429278767,-290.39317003462327,
                                     -243.09349980476634,-237.9671765884564,-184.65425879239723,
                                     -181.6433889406905,-152.42678092373745,-150.01440312367828,
                                     -140.20043836510496,-134.26239413119825,-130.50508513840435,
                                     -128.1365612192653,-119.6243787096752,-117.25234191992651,
                                     -112.47792700587912,-108.89915545206071,-104.74759545668331,
                                     -100.22968339474058,-94.96995783385485,-89.69665862307328,
                                     -86.4677796222291,-68.14673474241698,-66.07309844841615,
                                     -56.302362373674626,-54.63592515665142,-49.54128744936952,
                                     -47.77783428990662,-45.159157747842364,-43.112766536500985,
                                     -40.904881657341534,-38.624975888999664,-36.21821364136139,
                                     -33.61319247902676,-30.618705849873916,-26.992630355951256,
                                     -19.965626399359778,-9.761220034345175,-0.009675776079236078,
                                     -8.793180471674402e-7,-4.675655622579791e-14,-3.291874702847949e-14,
                                     -3.0408119421278934e-14,-2.2328449483329286e-14,
                                     -1.0693016765542536e-14,-7.240806196368183e-15,
                                     -5.761950647030487e-15,-5.957409950992936e-16,3.2491920424847875e-15,
                                     9.219910025371442e-15,1.2799168101873962e-14,1.7108898977066272e-14,
                                     2.855032832850196e-14,4.1048723593164446e-14,9.99499970701814,
                                     18.470571911228987,30.51830887197781,33.54443547978839,
                                     36.1607309907248,38.57320314001222,40.85924266944485,
                                     43.035391586290125,45.287513370945284,47.151147140980484,
                                     50.73276704593593,52.39952223577173,59.50068014643575,
                                     61.31379159039717,74.51253995296709,77.13729578952395,
                                     94.37203226002364,99.43109411760354,102.6487394625419,
                                     105.06218043709777,107.8179491492106,110.12126119755328,
                                     113.57945167126782,116.37801423876415,122.99832759829658,
                                     125.12209476580418,138.05692785144052,140.21814403606146,
                                     161.31660187081715,164.54676840029904,175.43647569692263,
                                     192.3282320950376,207.53405143733352,212.25537253729775,
                                     284.3628999051823,294.6884623476941,341.98290548531804,
                                     507.44043882416463,534.3382764375148,967.6392677402239,
                                     1875.507871717115,3171.7801027349824,5101.18005605357
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -10.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//===========//
// test # 39 //
//===========//

TEST(MatrixTest39, Heavy39)
{
     MatrixDense<double> matA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 5) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 5) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvaluesDSYEV(eigenvaluesA);

     // test

     const double arr_values[100] = {-323.38478082263765, -318.95649570052416, -259.5812044929057,
                                     -255.47902023149175, -210.8574832531544, -207.046095983574,
                                     -172.17668982103106, -169.63822144706373, -167.11362728657673,
                                     -164.65335127248554, -137.93078894722365, -135.63554168942136,
                                     -133.27655590262873, -130.90313825750582, -111.16491214977447,
                                     -108.69201485977163, -104.86333072392988, -102.09654131372476,
                                     -89.52930445237439, -87.11011352113846, -79.24098389717554,
                                     -76.62676551546362, -71.01471245774239, -68.78074078937429,
                                     -57.39730933358366, -55.597048251741185, -53.807888693814036,
                                     -52.082337027364105, -42.001162570408376, -40.10702874995469,
                                     -36.13941170041208, -34.10866137118017, -29.537625276581757,
                                     -27.807512581350473, -20.493485216120316, -19.180726411630474,
                                     -17.86587524596272, -16.594313970872253, -10.527129255160773,
                                     -9.235915164234662, -6.274431175607891, -4.736234460618425,
                                     -3.113902006580256, -2.3717281394985243, -0.07129875210819334,
                                     2.4178759119876623, 3.3310614360036803, 4.154955748257782,
                                     8.633411865685325, 10.366556352869488, 12.095377640289747,
                                     14.018873556316715, 17.48114915683801, 21.917608573061386,
                                     23.316848960689295, 27.074595655585338, 31.322004721507344,
                                     34.9238763246869, 36.70168716147582, 39.12924266179792,
                                     48.291435480841905, 50.073858853444456, 52.12562986320187,
                                     57.27293729356433, 64.70819943681298, 66.93714122637961,
                                     68.82639047739008, 80.9665198923797, 83.5742202822415,
                                     86.58950836310686, 89.44951473838972, 102.06705367044331,
                                     107.13994334023343, 109.44821746324266, 117.55286230732882,
                                     126.41194103599305, 133.5454916018076, 135.893724579225,
                                     151.52771532059967, 157.8972863072518, 165.22044899155756,
                                     167.77180305861472, 190.84742611813815, 205.77684982675112,
                                     208.5174101551291, 234.35823130936174, 279.92197126989515,
                                     330.06056030550724, 384.6942438609073, 444.0868835363212,
                                     508.62422446622827, 578.8097468653175, 655.2951900145777,
                                     738.9379634908137, 830.8998363959786, 932.8239020042909,
                                     1047.1805607335104, 1178.0384559097015, 1333.1755507750008,
                                     1532.5774637949444
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -5.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//===========//
// test # 40 //
//===========//

TEST(MatrixTest40, Heavy40)
{
     MatrixDense<double> matA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 20) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 20) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvaluesDSYEV(eigenvaluesA);

     // test

     const double arr_values[100] = {-890.0679762687605,-833.96613152625,-387.9183497645402,
                                     -374.28858505936836,-325.97417429278767,-290.39317003462327,
                                     -243.09349980476634,-237.9671765884564,-184.65425879239723,
                                     -181.6433889406905,-152.42678092373745,-150.01440312367828,
                                     -140.20043836510496,-134.26239413119825,-130.50508513840435,
                                     -128.1365612192653,-119.6243787096752,-117.25234191992651,
                                     -112.47792700587912,-108.89915545206071,-104.74759545668331,
                                     -100.22968339474058,-94.96995783385485,-89.69665862307328,
                                     -86.4677796222291,-68.14673474241698,-66.07309844841615,
                                     -56.302362373674626,-54.63592515665142,-49.54128744936952,
                                     -47.77783428990662,-45.159157747842364,-43.112766536500985,
                                     -40.904881657341534,-38.624975888999664,-36.21821364136139,
                                     -33.61319247902676,-30.618705849873916,-26.992630355951256,
                                     -19.965626399359778,-9.761220034345175,-0.009675776079236078,
                                     -8.793180471674402e-7,-4.675655622579791e-14,-3.291874702847949e-14,
                                     -3.0408119421278934e-14,-2.2328449483329286e-14,
                                     -1.0693016765542536e-14,-7.240806196368183e-15,
                                     -5.761950647030487e-15,-5.957409950992936e-16,3.2491920424847875e-15,
                                     9.219910025371442e-15,1.2799168101873962e-14,1.7108898977066272e-14,
                                     2.855032832850196e-14,4.1048723593164446e-14,9.99499970701814,
                                     18.470571911228987,30.51830887197781,33.54443547978839,
                                     36.1607309907248,38.57320314001222,40.85924266944485,
                                     43.035391586290125,45.287513370945284,47.151147140980484,
                                     50.73276704593593,52.39952223577173,59.50068014643575,
                                     61.31379159039717,74.51253995296709,77.13729578952395,
                                     94.37203226002364,99.43109411760354,102.6487394625419,
                                     105.06218043709777,107.8179491492106,110.12126119755328,
                                     113.57945167126782,116.37801423876415,122.99832759829658,
                                     125.12209476580418,138.05692785144052,140.21814403606146,
                                     161.31660187081715,164.54676840029904,175.43647569692263,
                                     192.3282320950376,207.53405143733352,212.25537253729775,
                                     284.3628999051823,294.6884623476941,341.98290548531804,
                                     507.44043882416463,534.3382764375148,967.6392677402239,
                                     1875.507871717115,3171.7801027349824,5101.18005605357
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -5.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//===========//
// test # 41 //
//===========//

TEST(MatrixTest41, Heavy41)
{
     MatrixDense<double> matA;
     MatrixDense<double> eigenvectorsA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 5) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 5) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvectorsDSYEV(eigenvectorsA);

     // test

     const double arr_values99[100] = {3.6942314255214093e-22,
                                       1.9280574390556084e-21,
                                       8.336207484683114e-21,
                                       3.201738809930229e-20,
                                       1.128792365433472e-19,
                                       3.7219958073502635e-19,
                                       1.1616570563876596e-18,
                                       3.4603856776191363e-18,
                                       9.898106164891213e-18,
                                       2.7313034552506115e-17,
                                       7.297235551812733e-17,
                                       1.8931798526470812e-16,
                                       4.781008453916819e-16,
                                       1.1776583982769647e-15,
                                       2.8342405979299284e-15,
                                       6.674396729414062e-15,
                                       1.5399303294661802e-14,
                                       3.4849026635393604e-14,
                                       7.743020050094637e-14,
                                       1.6906046576799098e-13,
                                       3.630170569572799e-13,
                                       7.671355827326599e-13,
                                       1.5964469231753074e-12,
                                       3.2735991022182145e-12,
                                       6.617806646334326e-12,
                                       1.3195608156949319e-11,
                                       2.596350061894761e-11,
                                       5.0430333328453936e-11,
                                       9.67336578559975e-11,
                                       1.8330342627363303e-10,
                                       3.4324925188064113e-10,
                                       6.353651498186134e-10,
                                       1.1628708815038037e-9,
                                       2.104965971919157e-9,
                                       3.7693601315726985e-9,
                                       6.6787563519161844e-9,
                                       1.1711672807554324e-8,
                                       2.03292597117336e-8,
                                       3.4936698501777865e-8,
                                       5.9452893015691615e-8,
                                       1.0019897315247485e-7,
                                       1.672693861786821e-7,
                                       2.7662540139461026e-7,
                                       4.5325802559019753e-7,
                                       7.359163073250683e-7,
                                       1.1841012055800893e-6,
                                       1.8883009979218705e-6,
                                       2.984811021145573e-6,
                                       4.676960526639804e-6,
                                       7.265186654807506e-6,
                                       0.000011189174362740128,
                                       0.00001708624363917233,
                                       0.000025871327250800013,
                                       0.00003884524735147213,
                                       0.000057839551835632114,
                                       0.0000854078666854305,
                                       0.00012507547298611522,
                                       0.0001816604867751283,
                                       0.0002616813987570242,
                                       0.00037386653150919884,
                                       0.0005297808051872514,
                                       0.0007445835839285321,
                                       0.0010379277320564163,
                                       0.0014350037062010874,
                                       0.001967722735545796,
                                       0.0026760192519511626,
                                       0.0036092342363378766,
                                       0.00482751783582054,
                                       0.00640315989292051,
                                       0.008421723355192411,
                                       0.010982819682174948,
                                       0.014200329986124536,
                                       0.018201824732492763,
                                       0.023126912983358,
                                       0.029124247016656085,
                                       0.03634692535629019,
                                       0.04494587964943125,
                                       0.055061139884829774,
                                       0.0668110106541383,
                                       0.08027936243852593,
                                       0.09549945152005936,
                                       0.11243731017783884,
                                       0.13097574227719463,
                                       0.15090013051721174,
                                       0.17186879268938443,
                                       0.19341147439162046,
                                       0.21493488140894385,
                                       0.23573601738701488,
                                       0.2548580930562397,
                                       0.27133687410309687,
                                       0.28424352285526244,
                                       0.29273026481733555,
                                       0.29466646943807095,
                                       0.28996990773813897,
                                       0.27878021480447934,
                                       0.26146670556930435,
                                       0.22712948196564564,
                                       0.19198046679049208,
                                       0.15683996268683814,
                                       0.1225409827758915
                                      };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,99)) - abs(arr_values99[i])) < pow(10.0, -10.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//===========//
// test # 42 //
//===========//

TEST(MatrixTest42, Heavy42)
{
     MatrixDense<double> matA;
     MatrixDense<double> eigenvectorsA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 20) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 20) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvectorsDSYEV(eigenvectorsA);

     // test data # 1

     const double arr_values99[100] = {-0.00002379437463800071,-0.0000321126535355192,
                                       -0.00004261209009056319,-0.00005574228105018768,
                                       -0.0000720258070448845,-0.00009206699311457821,
                                       -0.00011656127426176481,-0.00014630515846035676,
                                       -0.0001822067730093074,-0.00022529697290878234,
                                       -0.00027674098203948964,-0.0003378505293342626,
                                       -0.00041009643284298355,-0.0004951215746125915,
                                       -0.0005947541986465926,-0.0007110214528956958,-0.0008461630842956044,
                                       -0.001002645183353377,-0.0011831738617460526,-0.0013907087329034266,
                                       -0.0016283827619966063,-0.0018997449209395513,-0.002208573606467658,
                                       -0.002558944309118129,-0.0029552087127267114,-0.0034019962501085446,
                                       -0.003904213875493434,-0.00446704386484559,-0.00509593944935361,
                                       -0.005796618082542783,-0.006575052137809326,-0.007437456830885415,
                                       -0.00839027516100404,-0.009440159665541611,-0.010593950785877937,
                                       -0.01185865164733851,-0.013241399063586366,-0.014749430585927434,
                                       -0.0163900474309,-0.018170573135449803,-0.0200983089054282,
                                       -0.022180481856340313,-0.024424190854787288,-0.026834080780647522,
                                       -0.02941440937535356,-0.03216899598488147,-0.035101168178730414,
                                       -0.03821370638490854,-0.04150878671030463,-0.04498792214706611,
                                       -0.04865190239866414,-0.05250073259412269,-0.056533571195309565,
                                       -0.06074866744009344,-0.0651432987033876,-0.06971370819842077,
                                       -0.07445504348175375,-0.07936129626731185,-0.08442524409670342,
                                       -0.08963839445497533,-0.09499093194079421,-0.10047166921101545,
                                       -0.10595789789842808,-0.11143628534678302,-0.11689273862688677,
                                       -0.1223124096438104,-0.12767970465965733,-0.13297829857254914,
                                       -0.13819115429332646,-0.1433005475596976,-0.14828809752292466,
                                       -0.15313480343431787,-0.15782108774752324,-0.16232684593753477,
                                       -0.16663150331824728,-0.17071407911689127,-0.17455325803557586,
                                       -0.17812746949713273,-0.18141497473424503,-0.18439396183722762,
                                       -0.18704264882716626,-0.18541168339207653,-0.18356676120058393,
                                       -0.18150272926411598,-0.17921485423076686,-0.17669886945503793,
                                       -0.17395102341224067,-0.17096812928970723,-0.1677476155625255,
                                       -0.1642875773360043,-0.16058682821057293,-0.15664495239745393,
                                       -0.15246235678535205,-0.14804032262974395,-0.14338105650732136,
                                       -0.13848774014895246,-0.1333645787354236,-0.128016847211483,
                                       -0.1224509341456258,-0.11667438263597216
                                      };

     // test data # 2

     const double arr_values98[100] = {0.0005490815006598427,0.0007217004212979288,0.0009319727820738912,
                                       0.0011856764796110383,0.0014891369301256077,0.0018492353084341568,
                                       0.002273410759398609,0.002769655716932461,0.003346503443557062,
                                       0.00401300689217423,0.0047787079930774546,0.00565359648519078,
                                       0.006648057443072511,0.007772806702325777,0.009038813457661714,
                                       0.010457209401826704,0.012039183891666605,0.013795864771304657,
                                       0.01573818465305239,0.01787673265523282,0.020218129530799774,
                                       0.022773522732384158,0.02555088599547039,0.02855699048006569,
                                       0.03179740706616532,0.03527631554658473,0.038996305859039175,
                                       0.04295817276333242,0.047160705650423344,0.05160047546600458,
                                       0.056271621039461366,0.061165637426456754,0.06627116919601259,
                                       0.07157381191620707,0.0770559254111312,0.08269646266941802,
                                       0.08847081857459133,0.09435070289203276,0.10030404217816305,
                                       0.10629491546542182,0.11228359420772163,0.11822646848984367,
                                       0.12407615562938958,0.12979275139874066,0.13533385216700752,
                                       0.14065467360130016,0.14570820649639804,0.1504454123825699,
                                       0.15481546139101132,0.15876601462676446,0.16224355300490953,
                                       0.1651937541424425,0.16756191846119972,0.16929344514268863,
                                       0.17033435798074814,0.17063188050057534,0.17013505895196374,
                                       0.16879543094112387,0.16656773654130375,0.16341066772151153,
                                       0.1592876487959431,0.15416764632985688,0.14836208495033895,
                                       0.14186022973422263,0.1346552528267162,0.12674464039743252,
                                       0.118130599994188,0.10882046380600134,0.09882708283296263,
                                       0.08816920644738636,0.07687184132594177,0.06496658324539006,
                                       0.052491914775380344,0.039493461481783246,0.026024198885649517,
                                       0.012144602119448178,-0.002077270001960313,-0.0165657649080691,
                                       -0.0312377086295896,-0.04600256891175273,-0.06076271707085829,
                                       -0.06795927022946352,-0.07503806683258157,-0.0819579039270416,
                                       -0.08867572326732134,-0.09514679596043041,-0.1013249415936529,
                                       -0.10716278331612594,-0.1126120400798519,-0.11762385692798413,
                                       -0.12214917384544385,-0.12613913325700105,-0.12954552576940606,
                                       -0.1323212732062947,-0.13442094737761986,-0.13580132236057105,
                                       -0.13642195734879647,-0.13624580635507777,-0.1352398502347542,
                                       -0.13337574564008783
                                      };

     // test data # 3

     const double arr_values97[100] = {-0.0048844047935863765,-0.006236486880389232,-0.007815274325331554,
                                       -0.009640183890712983,-0.011730050533458039,-0.014102771342375649,
                                       -0.016774922205544347,-0.019761352037216788,-0.023074760743478643,
                                       -0.026725268499719908,-0.03071998531275152,-0.03506259120033407,
                                       -0.03975293858755401,-0.04478668963230909,-0.050155002084304526,
                                       -0.05584427788164485,-0.06183598892150303,-0.06810659423095557,
                                       -0.07462756203769172,-0.08136550893055443,-0.08823038013976417,
                                       -0.09520849071940404,-0.1022476308045662,-0.10929156383600795,
                                       -0.11627847844623534,-0.12314131678294159,-0.1298082479646105,
                                       -0.13620329680129123,-0.14224713548726084,-0.14785804289917964,
                                       -0.15295303238959757,-0.1574491445459654,-0.1612648963069822,
                                       -0.16432187213530286,-0.16654643671328404,-0.16787154196597193,
                                       -0.16823859426980445,-0.16759934066305365,-0.16591772596198778,
                                       -0.1631716661722737,-0.15935634408053131,-0.15448321146294608,
                                       -0.14858218139776455,-0.1416381780493364,-0.1336490652093021,
                                       -0.12462715724623677,-0.11460064260169803,-0.10361487512764765,
                                       -0.09173348472751607,-0.07903925562502323,-0.06563471836746396,
                                       -0.051642400613860905,-0.03720468211196078,-0.022483201281379775,
                                       -0.007657764735299888,0.007075282886553215,0.021505263042677962,
                                       0.03540996481701064,0.04855905391412624,0.06071808324036558,
                                       0.07165316959606502,0.08113608035225883,0.09009849834439126,
                                       0.09838663571146652,0.10584187751947657,0.11230295059241002,
                                       0.11760842867321217,0.12159956720711124,0.12412345180969263,
                                       0.1250364341022301,0.12420781716730643,0.12152374055295379,
                                       0.11689120174753254,0.11024213763900717,0.10153747600764697,
                                       0.09077105399613189,0.07797328823931185,0.06321447046527728,
                                       0.04660755350441126,0.028310286413503014,0.008526547885299433,
                                       0.0025719295116293645,-0.004200598749307477,-0.011762440439929252,
                                       -0.020069965854842452,-0.029063538293552106,-0.038666727752430484,
                                       -0.048785756486008615,-0.05930922217827011,-0.07010814475633873,
                                       -0.08103638195706307,-0.09193145642839495,-0.10261583322770457,
                                       -0.11289868090088843,-0.12257814174844638,-0.1314441273038225,
                                       -0.139281643409791,-0.14587463558538188,-0.15101032970590494,
                                       -0.15448402553323257
                                      };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,99)) - abs(arr_values99[i])) < pow(10.0, -10.0));
     }

     // test # 2

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,98)) - abs(arr_values98[i])) < pow(10.0, -10.0));
     }

     // test # 3

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,97)) - abs(arr_values97[i])) < pow(10.0, -10.0));
     }

     // test # 4

     EXPECT_TRUE(res == 0);
}

//===========//
// test # 43 //
//===========//

TEST(MatrixTest43, Heavy43)
{
     MatrixDense<double> matA;
     MatrixDense<double> eigenvectorsA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 5) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 5) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigensystemRealSymmetric(eigenvectorsA, eigenvaluesA);

     // test

     const double arr_values[100] = {-323.38478082263765, -318.95649570052416, -259.5812044929057,
                                     -255.47902023149175, -210.8574832531544, -207.046095983574,
                                     -172.17668982103106, -169.63822144706373, -167.11362728657673,
                                     -164.65335127248554, -137.93078894722365, -135.63554168942136,
                                     -133.27655590262873, -130.90313825750582, -111.16491214977447,
                                     -108.69201485977163, -104.86333072392988, -102.09654131372476,
                                     -89.52930445237439, -87.11011352113846, -79.24098389717554,
                                     -76.62676551546362, -71.01471245774239, -68.78074078937429,
                                     -57.39730933358366, -55.597048251741185, -53.807888693814036,
                                     -52.082337027364105, -42.001162570408376, -40.10702874995469,
                                     -36.13941170041208, -34.10866137118017, -29.537625276581757,
                                     -27.807512581350473, -20.493485216120316, -19.180726411630474,
                                     -17.86587524596272, -16.594313970872253, -10.527129255160773,
                                     -9.235915164234662, -6.274431175607891, -4.736234460618425,
                                     -3.113902006580256, -2.3717281394985243, -0.07129875210819334,
                                     2.4178759119876623, 3.3310614360036803, 4.154955748257782,
                                     8.633411865685325, 10.366556352869488, 12.095377640289747,
                                     14.018873556316715, 17.48114915683801, 21.917608573061386,
                                     23.316848960689295, 27.074595655585338, 31.322004721507344,
                                     34.9238763246869, 36.70168716147582, 39.12924266179792,
                                     48.291435480841905, 50.073858853444456, 52.12562986320187,
                                     57.27293729356433, 64.70819943681298, 66.93714122637961,
                                     68.82639047739008, 80.9665198923797, 83.5742202822415,
                                     86.58950836310686, 89.44951473838972, 102.06705367044331,
                                     107.13994334023343, 109.44821746324266, 117.55286230732882,
                                     126.41194103599305, 133.5454916018076, 135.893724579225,
                                     151.52771532059967, 157.8972863072518, 165.22044899155756,
                                     167.77180305861472, 190.84742611813815, 205.77684982675112,
                                     208.5174101551291, 234.35823130936174, 279.92197126989515,
                                     330.06056030550724, 384.6942438609073, 444.0868835363212,
                                     508.62422446622827, 578.8097468653175, 655.2951900145777,
                                     738.9379634908137, 830.8998363959786, 932.8239020042909,
                                     1047.1805607335104, 1178.0384559097015, 1333.1755507750008,
                                     1532.5774637949444
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -10.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//===========//
// test # 44 //
//===========//

TEST(MatrixTest44, Heavy44)
{
     MatrixDense<double> matA;
     MatrixDense<double> eigenvectorsA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 20) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 20) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigensystemRealSymmetric(eigenvectorsA, eigenvaluesA);

     // test

     const double arr_values[100] = {-890.0679762687605,-833.96613152625,-387.9183497645402,
                                     -374.28858505936836,-325.97417429278767,-290.39317003462327,
                                     -243.09349980476634,-237.9671765884564,-184.65425879239723,
                                     -181.6433889406905,-152.42678092373745,-150.01440312367828,
                                     -140.20043836510496,-134.26239413119825,-130.50508513840435,
                                     -128.1365612192653,-119.6243787096752,-117.25234191992651,
                                     -112.47792700587912,-108.89915545206071,-104.74759545668331,
                                     -100.22968339474058,-94.96995783385485,-89.69665862307328,
                                     -86.4677796222291,-68.14673474241698,-66.07309844841615,
                                     -56.302362373674626,-54.63592515665142,-49.54128744936952,
                                     -47.77783428990662,-45.159157747842364,-43.112766536500985,
                                     -40.904881657341534,-38.624975888999664,-36.21821364136139,
                                     -33.61319247902676,-30.618705849873916,-26.992630355951256,
                                     -19.965626399359778,-9.761220034345175,-0.009675776079236078,
                                     -8.793180471674402e-7,-4.675655622579791e-14,-3.291874702847949e-14,
                                     -3.0408119421278934e-14,-2.2328449483329286e-14,
                                     -1.0693016765542536e-14,-7.240806196368183e-15,
                                     -5.761950647030487e-15,-5.957409950992936e-16,3.2491920424847875e-15,
                                     9.219910025371442e-15,1.2799168101873962e-14,1.7108898977066272e-14,
                                     2.855032832850196e-14,4.1048723593164446e-14,9.99499970701814,
                                     18.470571911228987,30.51830887197781,33.54443547978839,
                                     36.1607309907248,38.57320314001222,40.85924266944485,
                                     43.035391586290125,45.287513370945284,47.151147140980484,
                                     50.73276704593593,52.39952223577173,59.50068014643575,
                                     61.31379159039717,74.51253995296709,77.13729578952395,
                                     94.37203226002364,99.43109411760354,102.6487394625419,
                                     105.06218043709777,107.8179491492106,110.12126119755328,
                                     113.57945167126782,116.37801423876415,122.99832759829658,
                                     125.12209476580418,138.05692785144052,140.21814403606146,
                                     161.31660187081715,164.54676840029904,175.43647569692263,
                                     192.3282320950376,207.53405143733352,212.25537253729775,
                                     284.3628999051823,294.6884623476941,341.98290548531804,
                                     507.44043882416463,534.3382764375148,967.6392677402239,
                                     1875.507871717115,3171.7801027349824,5101.18005605357
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -10.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//===========//
// test # 45 //
//===========//

TEST(MatrixTest45, Heavy45)
{
     MatrixDense<double> matA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 5) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 5) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvaluesRealSymmetric(eigenvaluesA);

     // test

     const double arr_values[100] = {-323.38478082263765, -318.95649570052416, -259.5812044929057,
                                     -255.47902023149175, -210.8574832531544, -207.046095983574,
                                     -172.17668982103106, -169.63822144706373, -167.11362728657673,
                                     -164.65335127248554, -137.93078894722365, -135.63554168942136,
                                     -133.27655590262873, -130.90313825750582, -111.16491214977447,
                                     -108.69201485977163, -104.86333072392988, -102.09654131372476,
                                     -89.52930445237439, -87.11011352113846, -79.24098389717554,
                                     -76.62676551546362, -71.01471245774239, -68.78074078937429,
                                     -57.39730933358366, -55.597048251741185, -53.807888693814036,
                                     -52.082337027364105, -42.001162570408376, -40.10702874995469,
                                     -36.13941170041208, -34.10866137118017, -29.537625276581757,
                                     -27.807512581350473, -20.493485216120316, -19.180726411630474,
                                     -17.86587524596272, -16.594313970872253, -10.527129255160773,
                                     -9.235915164234662, -6.274431175607891, -4.736234460618425,
                                     -3.113902006580256, -2.3717281394985243, -0.07129875210819334,
                                     2.4178759119876623, 3.3310614360036803, 4.154955748257782,
                                     8.633411865685325, 10.366556352869488, 12.095377640289747,
                                     14.018873556316715, 17.48114915683801, 21.917608573061386,
                                     23.316848960689295, 27.074595655585338, 31.322004721507344,
                                     34.9238763246869, 36.70168716147582, 39.12924266179792,
                                     48.291435480841905, 50.073858853444456, 52.12562986320187,
                                     57.27293729356433, 64.70819943681298, 66.93714122637961,
                                     68.82639047739008, 80.9665198923797, 83.5742202822415,
                                     86.58950836310686, 89.44951473838972, 102.06705367044331,
                                     107.13994334023343, 109.44821746324266, 117.55286230732882,
                                     126.41194103599305, 133.5454916018076, 135.893724579225,
                                     151.52771532059967, 157.8972863072518, 165.22044899155756,
                                     167.77180305861472, 190.84742611813815, 205.77684982675112,
                                     208.5174101551291, 234.35823130936174, 279.92197126989515,
                                     330.06056030550724, 384.6942438609073, 444.0868835363212,
                                     508.62422446622827, 578.8097468653175, 655.2951900145777,
                                     738.9379634908137, 830.8998363959786, 932.8239020042909,
                                     1047.1805607335104, 1178.0384559097015, 1333.1755507750008,
                                     1532.5774637949444
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -5.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//===========//
// test # 46 //
//===========//

TEST(MatrixTest46, Heavy46)
{
     MatrixDense<double> matA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 20) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 20) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvaluesRealSymmetric(eigenvaluesA);

     // test

     const double arr_values[100] = {-890.0679762687605,-833.96613152625,-387.9183497645402,
                                     -374.28858505936836,-325.97417429278767,-290.39317003462327,
                                     -243.09349980476634,-237.9671765884564,-184.65425879239723,
                                     -181.6433889406905,-152.42678092373745,-150.01440312367828,
                                     -140.20043836510496,-134.26239413119825,-130.50508513840435,
                                     -128.1365612192653,-119.6243787096752,-117.25234191992651,
                                     -112.47792700587912,-108.89915545206071,-104.74759545668331,
                                     -100.22968339474058,-94.96995783385485,-89.69665862307328,
                                     -86.4677796222291,-68.14673474241698,-66.07309844841615,
                                     -56.302362373674626,-54.63592515665142,-49.54128744936952,
                                     -47.77783428990662,-45.159157747842364,-43.112766536500985,
                                     -40.904881657341534,-38.624975888999664,-36.21821364136139,
                                     -33.61319247902676,-30.618705849873916,-26.992630355951256,
                                     -19.965626399359778,-9.761220034345175,-0.009675776079236078,
                                     -8.793180471674402e-7,-4.675655622579791e-14,-3.291874702847949e-14,
                                     -3.0408119421278934e-14,-2.2328449483329286e-14,
                                     -1.0693016765542536e-14,-7.240806196368183e-15,
                                     -5.761950647030487e-15,-5.957409950992936e-16,3.2491920424847875e-15,
                                     9.219910025371442e-15,1.2799168101873962e-14,1.7108898977066272e-14,
                                     2.855032832850196e-14,4.1048723593164446e-14,9.99499970701814,
                                     18.470571911228987,30.51830887197781,33.54443547978839,
                                     36.1607309907248,38.57320314001222,40.85924266944485,
                                     43.035391586290125,45.287513370945284,47.151147140980484,
                                     50.73276704593593,52.39952223577173,59.50068014643575,
                                     61.31379159039717,74.51253995296709,77.13729578952395,
                                     94.37203226002364,99.43109411760354,102.6487394625419,
                                     105.06218043709777,107.8179491492106,110.12126119755328,
                                     113.57945167126782,116.37801423876415,122.99832759829658,
                                     125.12209476580418,138.05692785144052,140.21814403606146,
                                     161.31660187081715,164.54676840029904,175.43647569692263,
                                     192.3282320950376,207.53405143733352,212.25537253729775,
                                     284.3628999051823,294.6884623476941,341.98290548531804,
                                     507.44043882416463,534.3382764375148,967.6392677402239,
                                     1875.507871717115,3171.7801027349824,5101.18005605357
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -5.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//===========//
// test # 47 //
//===========//

TEST(MatrixTest47, Heavy47)
{
     MatrixDense<double> matA;
     MatrixDense<double> eigenvectorsA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 5) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 5) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvectorsRealSymmetric(eigenvectorsA);

     // test

     const double arr_values99[100] = {3.6942314255214093e-22,
                                       1.9280574390556084e-21,
                                       8.336207484683114e-21,
                                       3.201738809930229e-20,
                                       1.128792365433472e-19,
                                       3.7219958073502635e-19,
                                       1.1616570563876596e-18,
                                       3.4603856776191363e-18,
                                       9.898106164891213e-18,
                                       2.7313034552506115e-17,
                                       7.297235551812733e-17,
                                       1.8931798526470812e-16,
                                       4.781008453916819e-16,
                                       1.1776583982769647e-15,
                                       2.8342405979299284e-15,
                                       6.674396729414062e-15,
                                       1.5399303294661802e-14,
                                       3.4849026635393604e-14,
                                       7.743020050094637e-14,
                                       1.6906046576799098e-13,
                                       3.630170569572799e-13,
                                       7.671355827326599e-13,
                                       1.5964469231753074e-12,
                                       3.2735991022182145e-12,
                                       6.617806646334326e-12,
                                       1.3195608156949319e-11,
                                       2.596350061894761e-11,
                                       5.0430333328453936e-11,
                                       9.67336578559975e-11,
                                       1.8330342627363303e-10,
                                       3.4324925188064113e-10,
                                       6.353651498186134e-10,
                                       1.1628708815038037e-9,
                                       2.104965971919157e-9,
                                       3.7693601315726985e-9,
                                       6.6787563519161844e-9,
                                       1.1711672807554324e-8,
                                       2.03292597117336e-8,
                                       3.4936698501777865e-8,
                                       5.9452893015691615e-8,
                                       1.0019897315247485e-7,
                                       1.672693861786821e-7,
                                       2.7662540139461026e-7,
                                       4.5325802559019753e-7,
                                       7.359163073250683e-7,
                                       1.1841012055800893e-6,
                                       1.8883009979218705e-6,
                                       2.984811021145573e-6,
                                       4.676960526639804e-6,
                                       7.265186654807506e-6,
                                       0.000011189174362740128,
                                       0.00001708624363917233,
                                       0.000025871327250800013,
                                       0.00003884524735147213,
                                       0.000057839551835632114,
                                       0.0000854078666854305,
                                       0.00012507547298611522,
                                       0.0001816604867751283,
                                       0.0002616813987570242,
                                       0.00037386653150919884,
                                       0.0005297808051872514,
                                       0.0007445835839285321,
                                       0.0010379277320564163,
                                       0.0014350037062010874,
                                       0.001967722735545796,
                                       0.0026760192519511626,
                                       0.0036092342363378766,
                                       0.00482751783582054,
                                       0.00640315989292051,
                                       0.008421723355192411,
                                       0.010982819682174948,
                                       0.014200329986124536,
                                       0.018201824732492763,
                                       0.023126912983358,
                                       0.029124247016656085,
                                       0.03634692535629019,
                                       0.04494587964943125,
                                       0.055061139884829774,
                                       0.0668110106541383,
                                       0.08027936243852593,
                                       0.09549945152005936,
                                       0.11243731017783884,
                                       0.13097574227719463,
                                       0.15090013051721174,
                                       0.17186879268938443,
                                       0.19341147439162046,
                                       0.21493488140894385,
                                       0.23573601738701488,
                                       0.2548580930562397,
                                       0.27133687410309687,
                                       0.28424352285526244,
                                       0.29273026481733555,
                                       0.29466646943807095,
                                       0.28996990773813897,
                                       0.27878021480447934,
                                       0.26146670556930435,
                                       0.22712948196564564,
                                       0.19198046679049208,
                                       0.15683996268683814,
                                       0.1225409827758915
                                      };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,99)) - abs(arr_values99[i])) < pow(10.0, -10.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//===========//
// test # 48 //
//===========//

TEST(MatrixTest48, Heavy48)
{
     MatrixDense<double> matA;
     MatrixDense<double> eigenvectorsA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 20) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 20) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvectorsRealSymmetric(eigenvectorsA);

     // test data # 1

     const double arr_values99[100] = {-0.00002379437463800071,-0.0000321126535355192,
                                       -0.00004261209009056319,-0.00005574228105018768,
                                       -0.0000720258070448845,-0.00009206699311457821,
                                       -0.00011656127426176481,-0.00014630515846035676,
                                       -0.0001822067730093074,-0.00022529697290878234,
                                       -0.00027674098203948964,-0.0003378505293342626,
                                       -0.00041009643284298355,-0.0004951215746125915,
                                       -0.0005947541986465926,-0.0007110214528956958,-0.0008461630842956044,
                                       -0.001002645183353377,-0.0011831738617460526,-0.0013907087329034266,
                                       -0.0016283827619966063,-0.0018997449209395513,-0.002208573606467658,
                                       -0.002558944309118129,-0.0029552087127267114,-0.0034019962501085446,
                                       -0.003904213875493434,-0.00446704386484559,-0.00509593944935361,
                                       -0.005796618082542783,-0.006575052137809326,-0.007437456830885415,
                                       -0.00839027516100404,-0.009440159665541611,-0.010593950785877937,
                                       -0.01185865164733851,-0.013241399063586366,-0.014749430585927434,
                                       -0.0163900474309,-0.018170573135449803,-0.0200983089054282,
                                       -0.022180481856340313,-0.024424190854787288,-0.026834080780647522,
                                       -0.02941440937535356,-0.03216899598488147,-0.035101168178730414,
                                       -0.03821370638490854,-0.04150878671030463,-0.04498792214706611,
                                       -0.04865190239866414,-0.05250073259412269,-0.056533571195309565,
                                       -0.06074866744009344,-0.0651432987033876,-0.06971370819842077,
                                       -0.07445504348175375,-0.07936129626731185,-0.08442524409670342,
                                       -0.08963839445497533,-0.09499093194079421,-0.10047166921101545,
                                       -0.10595789789842808,-0.11143628534678302,-0.11689273862688677,
                                       -0.1223124096438104,-0.12767970465965733,-0.13297829857254914,
                                       -0.13819115429332646,-0.1433005475596976,-0.14828809752292466,
                                       -0.15313480343431787,-0.15782108774752324,-0.16232684593753477,
                                       -0.16663150331824728,-0.17071407911689127,-0.17455325803557586,
                                       -0.17812746949713273,-0.18141497473424503,-0.18439396183722762,
                                       -0.18704264882716626,-0.18541168339207653,-0.18356676120058393,
                                       -0.18150272926411598,-0.17921485423076686,-0.17669886945503793,
                                       -0.17395102341224067,-0.17096812928970723,-0.1677476155625255,
                                       -0.1642875773360043,-0.16058682821057293,-0.15664495239745393,
                                       -0.15246235678535205,-0.14804032262974395,-0.14338105650732136,
                                       -0.13848774014895246,-0.1333645787354236,-0.128016847211483,
                                       -0.1224509341456258,-0.11667438263597216
                                      };

     // test data # 2

     const double arr_values98[100] = {0.0005490815006598427,0.0007217004212979288,0.0009319727820738912,
                                       0.0011856764796110383,0.0014891369301256077,0.0018492353084341568,
                                       0.002273410759398609,0.002769655716932461,0.003346503443557062,
                                       0.00401300689217423,0.0047787079930774546,0.00565359648519078,
                                       0.006648057443072511,0.007772806702325777,0.009038813457661714,
                                       0.010457209401826704,0.012039183891666605,0.013795864771304657,
                                       0.01573818465305239,0.01787673265523282,0.020218129530799774,
                                       0.022773522732384158,0.02555088599547039,0.02855699048006569,
                                       0.03179740706616532,0.03527631554658473,0.038996305859039175,
                                       0.04295817276333242,0.047160705650423344,0.05160047546600458,
                                       0.056271621039461366,0.061165637426456754,0.06627116919601259,
                                       0.07157381191620707,0.0770559254111312,0.08269646266941802,
                                       0.08847081857459133,0.09435070289203276,0.10030404217816305,
                                       0.10629491546542182,0.11228359420772163,0.11822646848984367,
                                       0.12407615562938958,0.12979275139874066,0.13533385216700752,
                                       0.14065467360130016,0.14570820649639804,0.1504454123825699,
                                       0.15481546139101132,0.15876601462676446,0.16224355300490953,
                                       0.1651937541424425,0.16756191846119972,0.16929344514268863,
                                       0.17033435798074814,0.17063188050057534,0.17013505895196374,
                                       0.16879543094112387,0.16656773654130375,0.16341066772151153,
                                       0.1592876487959431,0.15416764632985688,0.14836208495033895,
                                       0.14186022973422263,0.1346552528267162,0.12674464039743252,
                                       0.118130599994188,0.10882046380600134,0.09882708283296263,
                                       0.08816920644738636,0.07687184132594177,0.06496658324539006,
                                       0.052491914775380344,0.039493461481783246,0.026024198885649517,
                                       0.012144602119448178,-0.002077270001960313,-0.0165657649080691,
                                       -0.0312377086295896,-0.04600256891175273,-0.06076271707085829,
                                       -0.06795927022946352,-0.07503806683258157,-0.0819579039270416,
                                       -0.08867572326732134,-0.09514679596043041,-0.1013249415936529,
                                       -0.10716278331612594,-0.1126120400798519,-0.11762385692798413,
                                       -0.12214917384544385,-0.12613913325700105,-0.12954552576940606,
                                       -0.1323212732062947,-0.13442094737761986,-0.13580132236057105,
                                       -0.13642195734879647,-0.13624580635507777,-0.1352398502347542,
                                       -0.13337574564008783
                                      };

     // test data # 3

     const double arr_values97[100] = {-0.0048844047935863765,-0.006236486880389232,-0.007815274325331554,
                                       -0.009640183890712983,-0.011730050533458039,-0.014102771342375649,
                                       -0.016774922205544347,-0.019761352037216788,-0.023074760743478643,
                                       -0.026725268499719908,-0.03071998531275152,-0.03506259120033407,
                                       -0.03975293858755401,-0.04478668963230909,-0.050155002084304526,
                                       -0.05584427788164485,-0.06183598892150303,-0.06810659423095557,
                                       -0.07462756203769172,-0.08136550893055443,-0.08823038013976417,
                                       -0.09520849071940404,-0.1022476308045662,-0.10929156383600795,
                                       -0.11627847844623534,-0.12314131678294159,-0.1298082479646105,
                                       -0.13620329680129123,-0.14224713548726084,-0.14785804289917964,
                                       -0.15295303238959757,-0.1574491445459654,-0.1612648963069822,
                                       -0.16432187213530286,-0.16654643671328404,-0.16787154196597193,
                                       -0.16823859426980445,-0.16759934066305365,-0.16591772596198778,
                                       -0.1631716661722737,-0.15935634408053131,-0.15448321146294608,
                                       -0.14858218139776455,-0.1416381780493364,-0.1336490652093021,
                                       -0.12462715724623677,-0.11460064260169803,-0.10361487512764765,
                                       -0.09173348472751607,-0.07903925562502323,-0.06563471836746396,
                                       -0.051642400613860905,-0.03720468211196078,-0.022483201281379775,
                                       -0.007657764735299888,0.007075282886553215,0.021505263042677962,
                                       0.03540996481701064,0.04855905391412624,0.06071808324036558,
                                       0.07165316959606502,0.08113608035225883,0.09009849834439126,
                                       0.09838663571146652,0.10584187751947657,0.11230295059241002,
                                       0.11760842867321217,0.12159956720711124,0.12412345180969263,
                                       0.1250364341022301,0.12420781716730643,0.12152374055295379,
                                       0.11689120174753254,0.11024213763900717,0.10153747600764697,
                                       0.09077105399613189,0.07797328823931185,0.06321447046527728,
                                       0.04660755350441126,0.028310286413503014,0.008526547885299433,
                                       0.0025719295116293645,-0.004200598749307477,-0.011762440439929252,
                                       -0.020069965854842452,-0.029063538293552106,-0.038666727752430484,
                                       -0.048785756486008615,-0.05930922217827011,-0.07010814475633873,
                                       -0.08103638195706307,-0.09193145642839495,-0.10261583322770457,
                                       -0.11289868090088843,-0.12257814174844638,-0.1314441273038225,
                                       -0.139281643409791,-0.14587463558538188,-0.15101032970590494,
                                       -0.15448402553323257
                                      };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,99)) - abs(arr_values99[i])) < pow(10.0, -10.0));
     }

     // test # 2

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,98)) - abs(arr_values98[i])) < pow(10.0, -10.0));
     }

     // test # 3

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,97)) - abs(arr_values97[i])) < pow(10.0, -10.0));
     }

     // test # 4

     EXPECT_TRUE(res == 0);
}

//===========//
// test # 49 //
//===========//

TEST(MatrixTest49, Heavy49)
{
     MatrixDense<double> matA;
     MatrixDense<double> eigenvectorsA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 5) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 5) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigensystemRealSymmetric(eigenvectorsA, eigenvaluesA, "DSYEVR");

     // test

     const double arr_values[100] = {-323.38478082263765, -318.95649570052416, -259.5812044929057,
                                     -255.47902023149175, -210.8574832531544, -207.046095983574,
                                     -172.17668982103106, -169.63822144706373, -167.11362728657673,
                                     -164.65335127248554, -137.93078894722365, -135.63554168942136,
                                     -133.27655590262873, -130.90313825750582, -111.16491214977447,
                                     -108.69201485977163, -104.86333072392988, -102.09654131372476,
                                     -89.52930445237439, -87.11011352113846, -79.24098389717554,
                                     -76.62676551546362, -71.01471245774239, -68.78074078937429,
                                     -57.39730933358366, -55.597048251741185, -53.807888693814036,
                                     -52.082337027364105, -42.001162570408376, -40.10702874995469,
                                     -36.13941170041208, -34.10866137118017, -29.537625276581757,
                                     -27.807512581350473, -20.493485216120316, -19.180726411630474,
                                     -17.86587524596272, -16.594313970872253, -10.527129255160773,
                                     -9.235915164234662, -6.274431175607891, -4.736234460618425,
                                     -3.113902006580256, -2.3717281394985243, -0.07129875210819334,
                                     2.4178759119876623, 3.3310614360036803, 4.154955748257782,
                                     8.633411865685325, 10.366556352869488, 12.095377640289747,
                                     14.018873556316715, 17.48114915683801, 21.917608573061386,
                                     23.316848960689295, 27.074595655585338, 31.322004721507344,
                                     34.9238763246869, 36.70168716147582, 39.12924266179792,
                                     48.291435480841905, 50.073858853444456, 52.12562986320187,
                                     57.27293729356433, 64.70819943681298, 66.93714122637961,
                                     68.82639047739008, 80.9665198923797, 83.5742202822415,
                                     86.58950836310686, 89.44951473838972, 102.06705367044331,
                                     107.13994334023343, 109.44821746324266, 117.55286230732882,
                                     126.41194103599305, 133.5454916018076, 135.893724579225,
                                     151.52771532059967, 157.8972863072518, 165.22044899155756,
                                     167.77180305861472, 190.84742611813815, 205.77684982675112,
                                     208.5174101551291, 234.35823130936174, 279.92197126989515,
                                     330.06056030550724, 384.6942438609073, 444.0868835363212,
                                     508.62422446622827, 578.8097468653175, 655.2951900145777,
                                     738.9379634908137, 830.8998363959786, 932.8239020042909,
                                     1047.1805607335104, 1178.0384559097015, 1333.1755507750008,
                                     1532.5774637949444
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -10.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//===========//
// test # 50 //
//===========//

TEST(MatrixTest50, Heavy50)
{
     MatrixDense<double> matA;
     MatrixDense<double> eigenvectorsA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 20) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 20) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigensystemRealSymmetric(eigenvectorsA, eigenvaluesA, "DSYEVR");

     // test

     const double arr_values[100] = {-890.0679762687605,-833.96613152625,-387.9183497645402,
                                     -374.28858505936836,-325.97417429278767,-290.39317003462327,
                                     -243.09349980476634,-237.9671765884564,-184.65425879239723,
                                     -181.6433889406905,-152.42678092373745,-150.01440312367828,
                                     -140.20043836510496,-134.26239413119825,-130.50508513840435,
                                     -128.1365612192653,-119.6243787096752,-117.25234191992651,
                                     -112.47792700587912,-108.89915545206071,-104.74759545668331,
                                     -100.22968339474058,-94.96995783385485,-89.69665862307328,
                                     -86.4677796222291,-68.14673474241698,-66.07309844841615,
                                     -56.302362373674626,-54.63592515665142,-49.54128744936952,
                                     -47.77783428990662,-45.159157747842364,-43.112766536500985,
                                     -40.904881657341534,-38.624975888999664,-36.21821364136139,
                                     -33.61319247902676,-30.618705849873916,-26.992630355951256,
                                     -19.965626399359778,-9.761220034345175,-0.009675776079236078,
                                     -8.793180471674402e-7,-4.675655622579791e-14,-3.291874702847949e-14,
                                     -3.0408119421278934e-14,-2.2328449483329286e-14,
                                     -1.0693016765542536e-14,-7.240806196368183e-15,
                                     -5.761950647030487e-15,-5.957409950992936e-16,3.2491920424847875e-15,
                                     9.219910025371442e-15,1.2799168101873962e-14,1.7108898977066272e-14,
                                     2.855032832850196e-14,4.1048723593164446e-14,9.99499970701814,
                                     18.470571911228987,30.51830887197781,33.54443547978839,
                                     36.1607309907248,38.57320314001222,40.85924266944485,
                                     43.035391586290125,45.287513370945284,47.151147140980484,
                                     50.73276704593593,52.39952223577173,59.50068014643575,
                                     61.31379159039717,74.51253995296709,77.13729578952395,
                                     94.37203226002364,99.43109411760354,102.6487394625419,
                                     105.06218043709777,107.8179491492106,110.12126119755328,
                                     113.57945167126782,116.37801423876415,122.99832759829658,
                                     125.12209476580418,138.05692785144052,140.21814403606146,
                                     161.31660187081715,164.54676840029904,175.43647569692263,
                                     192.3282320950376,207.53405143733352,212.25537253729775,
                                     284.3628999051823,294.6884623476941,341.98290548531804,
                                     507.44043882416463,534.3382764375148,967.6392677402239,
                                     1875.507871717115,3171.7801027349824,5101.18005605357
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -10.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//===========//
// test # 51 //
//===========//

TEST(MatrixTest51, Heavy51)
{
     MatrixDense<double> matA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 5) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 5) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvaluesRealSymmetric(eigenvaluesA, "DSYEVR");

     // test

     const double arr_values[100] = {-323.38478082263765, -318.95649570052416, -259.5812044929057,
                                     -255.47902023149175, -210.8574832531544, -207.046095983574,
                                     -172.17668982103106, -169.63822144706373, -167.11362728657673,
                                     -164.65335127248554, -137.93078894722365, -135.63554168942136,
                                     -133.27655590262873, -130.90313825750582, -111.16491214977447,
                                     -108.69201485977163, -104.86333072392988, -102.09654131372476,
                                     -89.52930445237439, -87.11011352113846, -79.24098389717554,
                                     -76.62676551546362, -71.01471245774239, -68.78074078937429,
                                     -57.39730933358366, -55.597048251741185, -53.807888693814036,
                                     -52.082337027364105, -42.001162570408376, -40.10702874995469,
                                     -36.13941170041208, -34.10866137118017, -29.537625276581757,
                                     -27.807512581350473, -20.493485216120316, -19.180726411630474,
                                     -17.86587524596272, -16.594313970872253, -10.527129255160773,
                                     -9.235915164234662, -6.274431175607891, -4.736234460618425,
                                     -3.113902006580256, -2.3717281394985243, -0.07129875210819334,
                                     2.4178759119876623, 3.3310614360036803, 4.154955748257782,
                                     8.633411865685325, 10.366556352869488, 12.095377640289747,
                                     14.018873556316715, 17.48114915683801, 21.917608573061386,
                                     23.316848960689295, 27.074595655585338, 31.322004721507344,
                                     34.9238763246869, 36.70168716147582, 39.12924266179792,
                                     48.291435480841905, 50.073858853444456, 52.12562986320187,
                                     57.27293729356433, 64.70819943681298, 66.93714122637961,
                                     68.82639047739008, 80.9665198923797, 83.5742202822415,
                                     86.58950836310686, 89.44951473838972, 102.06705367044331,
                                     107.13994334023343, 109.44821746324266, 117.55286230732882,
                                     126.41194103599305, 133.5454916018076, 135.893724579225,
                                     151.52771532059967, 157.8972863072518, 165.22044899155756,
                                     167.77180305861472, 190.84742611813815, 205.77684982675112,
                                     208.5174101551291, 234.35823130936174, 279.92197126989515,
                                     330.06056030550724, 384.6942438609073, 444.0868835363212,
                                     508.62422446622827, 578.8097468653175, 655.2951900145777,
                                     738.9379634908137, 830.8998363959786, 932.8239020042909,
                                     1047.1805607335104, 1178.0384559097015, 1333.1755507750008,
                                     1532.5774637949444
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -5.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//===========//
// test # 52 //
//===========//

TEST(MatrixTest52, Heavy52)
{
     MatrixDense<double> matA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 20) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 20) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvaluesRealSymmetric(eigenvaluesA, "DSYEVR");

     // test

     const double arr_values[100] = {-890.0679762687605,-833.96613152625,-387.9183497645402,
                                     -374.28858505936836,-325.97417429278767,-290.39317003462327,
                                     -243.09349980476634,-237.9671765884564,-184.65425879239723,
                                     -181.6433889406905,-152.42678092373745,-150.01440312367828,
                                     -140.20043836510496,-134.26239413119825,-130.50508513840435,
                                     -128.1365612192653,-119.6243787096752,-117.25234191992651,
                                     -112.47792700587912,-108.89915545206071,-104.74759545668331,
                                     -100.22968339474058,-94.96995783385485,-89.69665862307328,
                                     -86.4677796222291,-68.14673474241698,-66.07309844841615,
                                     -56.302362373674626,-54.63592515665142,-49.54128744936952,
                                     -47.77783428990662,-45.159157747842364,-43.112766536500985,
                                     -40.904881657341534,-38.624975888999664,-36.21821364136139,
                                     -33.61319247902676,-30.618705849873916,-26.992630355951256,
                                     -19.965626399359778,-9.761220034345175,-0.009675776079236078,
                                     -8.793180471674402e-7,-4.675655622579791e-14,-3.291874702847949e-14,
                                     -3.0408119421278934e-14,-2.2328449483329286e-14,
                                     -1.0693016765542536e-14,-7.240806196368183e-15,
                                     -5.761950647030487e-15,-5.957409950992936e-16,3.2491920424847875e-15,
                                     9.219910025371442e-15,1.2799168101873962e-14,1.7108898977066272e-14,
                                     2.855032832850196e-14,4.1048723593164446e-14,9.99499970701814,
                                     18.470571911228987,30.51830887197781,33.54443547978839,
                                     36.1607309907248,38.57320314001222,40.85924266944485,
                                     43.035391586290125,45.287513370945284,47.151147140980484,
                                     50.73276704593593,52.39952223577173,59.50068014643575,
                                     61.31379159039717,74.51253995296709,77.13729578952395,
                                     94.37203226002364,99.43109411760354,102.6487394625419,
                                     105.06218043709777,107.8179491492106,110.12126119755328,
                                     113.57945167126782,116.37801423876415,122.99832759829658,
                                     125.12209476580418,138.05692785144052,140.21814403606146,
                                     161.31660187081715,164.54676840029904,175.43647569692263,
                                     192.3282320950376,207.53405143733352,212.25537253729775,
                                     284.3628999051823,294.6884623476941,341.98290548531804,
                                     507.44043882416463,534.3382764375148,967.6392677402239,
                                     1875.507871717115,3171.7801027349824,5101.18005605357
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -5.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//===========//
// test # 53 //
//===========//

TEST(MatrixTest53, Heavy53)
{
     MatrixDense<double> matA;
     MatrixDense<double> eigenvectorsA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 5) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 5) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvectorsRealSymmetric(eigenvectorsA, "DSYEVR");

     // test

     const double arr_values99[100] = {3.6942314255214093e-22,
                                       1.9280574390556084e-21,
                                       8.336207484683114e-21,
                                       3.201738809930229e-20,
                                       1.128792365433472e-19,
                                       3.7219958073502635e-19,
                                       1.1616570563876596e-18,
                                       3.4603856776191363e-18,
                                       9.898106164891213e-18,
                                       2.7313034552506115e-17,
                                       7.297235551812733e-17,
                                       1.8931798526470812e-16,
                                       4.781008453916819e-16,
                                       1.1776583982769647e-15,
                                       2.8342405979299284e-15,
                                       6.674396729414062e-15,
                                       1.5399303294661802e-14,
                                       3.4849026635393604e-14,
                                       7.743020050094637e-14,
                                       1.6906046576799098e-13,
                                       3.630170569572799e-13,
                                       7.671355827326599e-13,
                                       1.5964469231753074e-12,
                                       3.2735991022182145e-12,
                                       6.617806646334326e-12,
                                       1.3195608156949319e-11,
                                       2.596350061894761e-11,
                                       5.0430333328453936e-11,
                                       9.67336578559975e-11,
                                       1.8330342627363303e-10,
                                       3.4324925188064113e-10,
                                       6.353651498186134e-10,
                                       1.1628708815038037e-9,
                                       2.104965971919157e-9,
                                       3.7693601315726985e-9,
                                       6.6787563519161844e-9,
                                       1.1711672807554324e-8,
                                       2.03292597117336e-8,
                                       3.4936698501777865e-8,
                                       5.9452893015691615e-8,
                                       1.0019897315247485e-7,
                                       1.672693861786821e-7,
                                       2.7662540139461026e-7,
                                       4.5325802559019753e-7,
                                       7.359163073250683e-7,
                                       1.1841012055800893e-6,
                                       1.8883009979218705e-6,
                                       2.984811021145573e-6,
                                       4.676960526639804e-6,
                                       7.265186654807506e-6,
                                       0.000011189174362740128,
                                       0.00001708624363917233,
                                       0.000025871327250800013,
                                       0.00003884524735147213,
                                       0.000057839551835632114,
                                       0.0000854078666854305,
                                       0.00012507547298611522,
                                       0.0001816604867751283,
                                       0.0002616813987570242,
                                       0.00037386653150919884,
                                       0.0005297808051872514,
                                       0.0007445835839285321,
                                       0.0010379277320564163,
                                       0.0014350037062010874,
                                       0.001967722735545796,
                                       0.0026760192519511626,
                                       0.0036092342363378766,
                                       0.00482751783582054,
                                       0.00640315989292051,
                                       0.008421723355192411,
                                       0.010982819682174948,
                                       0.014200329986124536,
                                       0.018201824732492763,
                                       0.023126912983358,
                                       0.029124247016656085,
                                       0.03634692535629019,
                                       0.04494587964943125,
                                       0.055061139884829774,
                                       0.0668110106541383,
                                       0.08027936243852593,
                                       0.09549945152005936,
                                       0.11243731017783884,
                                       0.13097574227719463,
                                       0.15090013051721174,
                                       0.17186879268938443,
                                       0.19341147439162046,
                                       0.21493488140894385,
                                       0.23573601738701488,
                                       0.2548580930562397,
                                       0.27133687410309687,
                                       0.28424352285526244,
                                       0.29273026481733555,
                                       0.29466646943807095,
                                       0.28996990773813897,
                                       0.27878021480447934,
                                       0.26146670556930435,
                                       0.22712948196564564,
                                       0.19198046679049208,
                                       0.15683996268683814,
                                       0.1225409827758915
                                      };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,99)) - abs(arr_values99[i])) < pow(10.0, -10.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//===========//
// test # 54 //
//===========//

TEST(MatrixTest54, Heavy54)
{
     MatrixDense<double> matA;
     MatrixDense<double> eigenvectorsA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 20) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 20) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvectorsRealSymmetric(eigenvectorsA, "DSYEVR");

     // test data # 1

     const double arr_values99[100] = {-0.00002379437463800071,-0.0000321126535355192,
                                       -0.00004261209009056319,-0.00005574228105018768,
                                       -0.0000720258070448845,-0.00009206699311457821,
                                       -0.00011656127426176481,-0.00014630515846035676,
                                       -0.0001822067730093074,-0.00022529697290878234,
                                       -0.00027674098203948964,-0.0003378505293342626,
                                       -0.00041009643284298355,-0.0004951215746125915,
                                       -0.0005947541986465926,-0.0007110214528956958,-0.0008461630842956044,
                                       -0.001002645183353377,-0.0011831738617460526,-0.0013907087329034266,
                                       -0.0016283827619966063,-0.0018997449209395513,-0.002208573606467658,
                                       -0.002558944309118129,-0.0029552087127267114,-0.0034019962501085446,
                                       -0.003904213875493434,-0.00446704386484559,-0.00509593944935361,
                                       -0.005796618082542783,-0.006575052137809326,-0.007437456830885415,
                                       -0.00839027516100404,-0.009440159665541611,-0.010593950785877937,
                                       -0.01185865164733851,-0.013241399063586366,-0.014749430585927434,
                                       -0.0163900474309,-0.018170573135449803,-0.0200983089054282,
                                       -0.022180481856340313,-0.024424190854787288,-0.026834080780647522,
                                       -0.02941440937535356,-0.03216899598488147,-0.035101168178730414,
                                       -0.03821370638490854,-0.04150878671030463,-0.04498792214706611,
                                       -0.04865190239866414,-0.05250073259412269,-0.056533571195309565,
                                       -0.06074866744009344,-0.0651432987033876,-0.06971370819842077,
                                       -0.07445504348175375,-0.07936129626731185,-0.08442524409670342,
                                       -0.08963839445497533,-0.09499093194079421,-0.10047166921101545,
                                       -0.10595789789842808,-0.11143628534678302,-0.11689273862688677,
                                       -0.1223124096438104,-0.12767970465965733,-0.13297829857254914,
                                       -0.13819115429332646,-0.1433005475596976,-0.14828809752292466,
                                       -0.15313480343431787,-0.15782108774752324,-0.16232684593753477,
                                       -0.16663150331824728,-0.17071407911689127,-0.17455325803557586,
                                       -0.17812746949713273,-0.18141497473424503,-0.18439396183722762,
                                       -0.18704264882716626,-0.18541168339207653,-0.18356676120058393,
                                       -0.18150272926411598,-0.17921485423076686,-0.17669886945503793,
                                       -0.17395102341224067,-0.17096812928970723,-0.1677476155625255,
                                       -0.1642875773360043,-0.16058682821057293,-0.15664495239745393,
                                       -0.15246235678535205,-0.14804032262974395,-0.14338105650732136,
                                       -0.13848774014895246,-0.1333645787354236,-0.128016847211483,
                                       -0.1224509341456258,-0.11667438263597216
                                      };

     // test data # 2

     const double arr_values98[100] = {0.0005490815006598427,0.0007217004212979288,0.0009319727820738912,
                                       0.0011856764796110383,0.0014891369301256077,0.0018492353084341568,
                                       0.002273410759398609,0.002769655716932461,0.003346503443557062,
                                       0.00401300689217423,0.0047787079930774546,0.00565359648519078,
                                       0.006648057443072511,0.007772806702325777,0.009038813457661714,
                                       0.010457209401826704,0.012039183891666605,0.013795864771304657,
                                       0.01573818465305239,0.01787673265523282,0.020218129530799774,
                                       0.022773522732384158,0.02555088599547039,0.02855699048006569,
                                       0.03179740706616532,0.03527631554658473,0.038996305859039175,
                                       0.04295817276333242,0.047160705650423344,0.05160047546600458,
                                       0.056271621039461366,0.061165637426456754,0.06627116919601259,
                                       0.07157381191620707,0.0770559254111312,0.08269646266941802,
                                       0.08847081857459133,0.09435070289203276,0.10030404217816305,
                                       0.10629491546542182,0.11228359420772163,0.11822646848984367,
                                       0.12407615562938958,0.12979275139874066,0.13533385216700752,
                                       0.14065467360130016,0.14570820649639804,0.1504454123825699,
                                       0.15481546139101132,0.15876601462676446,0.16224355300490953,
                                       0.1651937541424425,0.16756191846119972,0.16929344514268863,
                                       0.17033435798074814,0.17063188050057534,0.17013505895196374,
                                       0.16879543094112387,0.16656773654130375,0.16341066772151153,
                                       0.1592876487959431,0.15416764632985688,0.14836208495033895,
                                       0.14186022973422263,0.1346552528267162,0.12674464039743252,
                                       0.118130599994188,0.10882046380600134,0.09882708283296263,
                                       0.08816920644738636,0.07687184132594177,0.06496658324539006,
                                       0.052491914775380344,0.039493461481783246,0.026024198885649517,
                                       0.012144602119448178,-0.002077270001960313,-0.0165657649080691,
                                       -0.0312377086295896,-0.04600256891175273,-0.06076271707085829,
                                       -0.06795927022946352,-0.07503806683258157,-0.0819579039270416,
                                       -0.08867572326732134,-0.09514679596043041,-0.1013249415936529,
                                       -0.10716278331612594,-0.1126120400798519,-0.11762385692798413,
                                       -0.12214917384544385,-0.12613913325700105,-0.12954552576940606,
                                       -0.1323212732062947,-0.13442094737761986,-0.13580132236057105,
                                       -0.13642195734879647,-0.13624580635507777,-0.1352398502347542,
                                       -0.13337574564008783
                                      };

     // test data # 3

     const double arr_values97[100] = {-0.0048844047935863765,-0.006236486880389232,-0.007815274325331554,
                                       -0.009640183890712983,-0.011730050533458039,-0.014102771342375649,
                                       -0.016774922205544347,-0.019761352037216788,-0.023074760743478643,
                                       -0.026725268499719908,-0.03071998531275152,-0.03506259120033407,
                                       -0.03975293858755401,-0.04478668963230909,-0.050155002084304526,
                                       -0.05584427788164485,-0.06183598892150303,-0.06810659423095557,
                                       -0.07462756203769172,-0.08136550893055443,-0.08823038013976417,
                                       -0.09520849071940404,-0.1022476308045662,-0.10929156383600795,
                                       -0.11627847844623534,-0.12314131678294159,-0.1298082479646105,
                                       -0.13620329680129123,-0.14224713548726084,-0.14785804289917964,
                                       -0.15295303238959757,-0.1574491445459654,-0.1612648963069822,
                                       -0.16432187213530286,-0.16654643671328404,-0.16787154196597193,
                                       -0.16823859426980445,-0.16759934066305365,-0.16591772596198778,
                                       -0.1631716661722737,-0.15935634408053131,-0.15448321146294608,
                                       -0.14858218139776455,-0.1416381780493364,-0.1336490652093021,
                                       -0.12462715724623677,-0.11460064260169803,-0.10361487512764765,
                                       -0.09173348472751607,-0.07903925562502323,-0.06563471836746396,
                                       -0.051642400613860905,-0.03720468211196078,-0.022483201281379775,
                                       -0.007657764735299888,0.007075282886553215,0.021505263042677962,
                                       0.03540996481701064,0.04855905391412624,0.06071808324036558,
                                       0.07165316959606502,0.08113608035225883,0.09009849834439126,
                                       0.09838663571146652,0.10584187751947657,0.11230295059241002,
                                       0.11760842867321217,0.12159956720711124,0.12412345180969263,
                                       0.1250364341022301,0.12420781716730643,0.12152374055295379,
                                       0.11689120174753254,0.11024213763900717,0.10153747600764697,
                                       0.09077105399613189,0.07797328823931185,0.06321447046527728,
                                       0.04660755350441126,0.028310286413503014,0.008526547885299433,
                                       0.0025719295116293645,-0.004200598749307477,-0.011762440439929252,
                                       -0.020069965854842452,-0.029063538293552106,-0.038666727752430484,
                                       -0.048785756486008615,-0.05930922217827011,-0.07010814475633873,
                                       -0.08103638195706307,-0.09193145642839495,-0.10261583322770457,
                                       -0.11289868090088843,-0.12257814174844638,-0.1314441273038225,
                                       -0.139281643409791,-0.14587463558538188,-0.15101032970590494,
                                       -0.15448402553323257
                                      };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,99)) - abs(arr_values99[i])) < pow(10.0, -10.0));
     }

     // test # 2

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,98)) - abs(arr_values98[i])) < pow(10.0, -10.0));
     }

     // test # 3

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,97)) - abs(arr_values97[i])) < pow(10.0, -10.0));
     }

     // test # 4

     EXPECT_TRUE(res == 0);
}

//===========//
// test # 55 //
//===========//

TEST(MatrixTest55, Heavy55)
{
     MatrixDense<double> matA;
     MatrixDense<double> eigenvectorsA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 5) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 5) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigensystemRealSymmetric(eigenvectorsA, eigenvaluesA, "DSYEVX");

     // test

     const double arr_values[100] = {-323.38478082263765, -318.95649570052416, -259.5812044929057,
                                     -255.47902023149175, -210.8574832531544, -207.046095983574,
                                     -172.17668982103106, -169.63822144706373, -167.11362728657673,
                                     -164.65335127248554, -137.93078894722365, -135.63554168942136,
                                     -133.27655590262873, -130.90313825750582, -111.16491214977447,
                                     -108.69201485977163, -104.86333072392988, -102.09654131372476,
                                     -89.52930445237439, -87.11011352113846, -79.24098389717554,
                                     -76.62676551546362, -71.01471245774239, -68.78074078937429,
                                     -57.39730933358366, -55.597048251741185, -53.807888693814036,
                                     -52.082337027364105, -42.001162570408376, -40.10702874995469,
                                     -36.13941170041208, -34.10866137118017, -29.537625276581757,
                                     -27.807512581350473, -20.493485216120316, -19.180726411630474,
                                     -17.86587524596272, -16.594313970872253, -10.527129255160773,
                                     -9.235915164234662, -6.274431175607891, -4.736234460618425,
                                     -3.113902006580256, -2.3717281394985243, -0.07129875210819334,
                                     2.4178759119876623, 3.3310614360036803, 4.154955748257782,
                                     8.633411865685325, 10.366556352869488, 12.095377640289747,
                                     14.018873556316715, 17.48114915683801, 21.917608573061386,
                                     23.316848960689295, 27.074595655585338, 31.322004721507344,
                                     34.9238763246869, 36.70168716147582, 39.12924266179792,
                                     48.291435480841905, 50.073858853444456, 52.12562986320187,
                                     57.27293729356433, 64.70819943681298, 66.93714122637961,
                                     68.82639047739008, 80.9665198923797, 83.5742202822415,
                                     86.58950836310686, 89.44951473838972, 102.06705367044331,
                                     107.13994334023343, 109.44821746324266, 117.55286230732882,
                                     126.41194103599305, 133.5454916018076, 135.893724579225,
                                     151.52771532059967, 157.8972863072518, 165.22044899155756,
                                     167.77180305861472, 190.84742611813815, 205.77684982675112,
                                     208.5174101551291, 234.35823130936174, 279.92197126989515,
                                     330.06056030550724, 384.6942438609073, 444.0868835363212,
                                     508.62422446622827, 578.8097468653175, 655.2951900145777,
                                     738.9379634908137, 830.8998363959786, 932.8239020042909,
                                     1047.1805607335104, 1178.0384559097015, 1333.1755507750008,
                                     1532.5774637949444
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -10.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//===========//
// test # 56 //
//===========//

TEST(MatrixTest56, Heavy56)
{
     MatrixDense<double> matA;
     MatrixDense<double> eigenvectorsA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 20) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 20) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigensystemRealSymmetric(eigenvectorsA, eigenvaluesA, "DSYEVX");

     // test

     const double arr_values[100] = {-890.0679762687605,-833.96613152625,-387.9183497645402,
                                     -374.28858505936836,-325.97417429278767,-290.39317003462327,
                                     -243.09349980476634,-237.9671765884564,-184.65425879239723,
                                     -181.6433889406905,-152.42678092373745,-150.01440312367828,
                                     -140.20043836510496,-134.26239413119825,-130.50508513840435,
                                     -128.1365612192653,-119.6243787096752,-117.25234191992651,
                                     -112.47792700587912,-108.89915545206071,-104.74759545668331,
                                     -100.22968339474058,-94.96995783385485,-89.69665862307328,
                                     -86.4677796222291,-68.14673474241698,-66.07309844841615,
                                     -56.302362373674626,-54.63592515665142,-49.54128744936952,
                                     -47.77783428990662,-45.159157747842364,-43.112766536500985,
                                     -40.904881657341534,-38.624975888999664,-36.21821364136139,
                                     -33.61319247902676,-30.618705849873916,-26.992630355951256,
                                     -19.965626399359778,-9.761220034345175,-0.009675776079236078,
                                     -8.793180471674402e-7,-4.675655622579791e-14,-3.291874702847949e-14,
                                     -3.0408119421278934e-14,-2.2328449483329286e-14,
                                     -1.0693016765542536e-14,-7.240806196368183e-15,
                                     -5.761950647030487e-15,-5.957409950992936e-16,3.2491920424847875e-15,
                                     9.219910025371442e-15,1.2799168101873962e-14,1.7108898977066272e-14,
                                     2.855032832850196e-14,4.1048723593164446e-14,9.99499970701814,
                                     18.470571911228987,30.51830887197781,33.54443547978839,
                                     36.1607309907248,38.57320314001222,40.85924266944485,
                                     43.035391586290125,45.287513370945284,47.151147140980484,
                                     50.73276704593593,52.39952223577173,59.50068014643575,
                                     61.31379159039717,74.51253995296709,77.13729578952395,
                                     94.37203226002364,99.43109411760354,102.6487394625419,
                                     105.06218043709777,107.8179491492106,110.12126119755328,
                                     113.57945167126782,116.37801423876415,122.99832759829658,
                                     125.12209476580418,138.05692785144052,140.21814403606146,
                                     161.31660187081715,164.54676840029904,175.43647569692263,
                                     192.3282320950376,207.53405143733352,212.25537253729775,
                                     284.3628999051823,294.6884623476941,341.98290548531804,
                                     507.44043882416463,534.3382764375148,967.6392677402239,
                                     1875.507871717115,3171.7801027349824,5101.18005605357
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -10.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//===========//
// test # 57 //
//===========//

TEST(MatrixTest57, Heavy57)
{
     MatrixDense<double> matA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 5) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 5) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvaluesRealSymmetric(eigenvaluesA, "DSYEVX");

     // test

     const double arr_values[100] = {-323.38478082263765, -318.95649570052416, -259.5812044929057,
                                     -255.47902023149175, -210.8574832531544, -207.046095983574,
                                     -172.17668982103106, -169.63822144706373, -167.11362728657673,
                                     -164.65335127248554, -137.93078894722365, -135.63554168942136,
                                     -133.27655590262873, -130.90313825750582, -111.16491214977447,
                                     -108.69201485977163, -104.86333072392988, -102.09654131372476,
                                     -89.52930445237439, -87.11011352113846, -79.24098389717554,
                                     -76.62676551546362, -71.01471245774239, -68.78074078937429,
                                     -57.39730933358366, -55.597048251741185, -53.807888693814036,
                                     -52.082337027364105, -42.001162570408376, -40.10702874995469,
                                     -36.13941170041208, -34.10866137118017, -29.537625276581757,
                                     -27.807512581350473, -20.493485216120316, -19.180726411630474,
                                     -17.86587524596272, -16.594313970872253, -10.527129255160773,
                                     -9.235915164234662, -6.274431175607891, -4.736234460618425,
                                     -3.113902006580256, -2.3717281394985243, -0.07129875210819334,
                                     2.4178759119876623, 3.3310614360036803, 4.154955748257782,
                                     8.633411865685325, 10.366556352869488, 12.095377640289747,
                                     14.018873556316715, 17.48114915683801, 21.917608573061386,
                                     23.316848960689295, 27.074595655585338, 31.322004721507344,
                                     34.9238763246869, 36.70168716147582, 39.12924266179792,
                                     48.291435480841905, 50.073858853444456, 52.12562986320187,
                                     57.27293729356433, 64.70819943681298, 66.93714122637961,
                                     68.82639047739008, 80.9665198923797, 83.5742202822415,
                                     86.58950836310686, 89.44951473838972, 102.06705367044331,
                                     107.13994334023343, 109.44821746324266, 117.55286230732882,
                                     126.41194103599305, 133.5454916018076, 135.893724579225,
                                     151.52771532059967, 157.8972863072518, 165.22044899155756,
                                     167.77180305861472, 190.84742611813815, 205.77684982675112,
                                     208.5174101551291, 234.35823130936174, 279.92197126989515,
                                     330.06056030550724, 384.6942438609073, 444.0868835363212,
                                     508.62422446622827, 578.8097468653175, 655.2951900145777,
                                     738.9379634908137, 830.8998363959786, 932.8239020042909,
                                     1047.1805607335104, 1178.0384559097015, 1333.1755507750008,
                                     1532.5774637949444
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -5.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//===========//
// test # 58 //
//===========//

TEST(MatrixTest58, Heavy58)
{
     MatrixDense<double> matA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 20) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 20) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvaluesRealSymmetric(eigenvaluesA, "DSYEVX");

     // test

     const double arr_values[100] = {-890.0679762687605,-833.96613152625,-387.9183497645402,
                                     -374.28858505936836,-325.97417429278767,-290.39317003462327,
                                     -243.09349980476634,-237.9671765884564,-184.65425879239723,
                                     -181.6433889406905,-152.42678092373745,-150.01440312367828,
                                     -140.20043836510496,-134.26239413119825,-130.50508513840435,
                                     -128.1365612192653,-119.6243787096752,-117.25234191992651,
                                     -112.47792700587912,-108.89915545206071,-104.74759545668331,
                                     -100.22968339474058,-94.96995783385485,-89.69665862307328,
                                     -86.4677796222291,-68.14673474241698,-66.07309844841615,
                                     -56.302362373674626,-54.63592515665142,-49.54128744936952,
                                     -47.77783428990662,-45.159157747842364,-43.112766536500985,
                                     -40.904881657341534,-38.624975888999664,-36.21821364136139,
                                     -33.61319247902676,-30.618705849873916,-26.992630355951256,
                                     -19.965626399359778,-9.761220034345175,-0.009675776079236078,
                                     -8.793180471674402e-7,-4.675655622579791e-14,-3.291874702847949e-14,
                                     -3.0408119421278934e-14,-2.2328449483329286e-14,
                                     -1.0693016765542536e-14,-7.240806196368183e-15,
                                     -5.761950647030487e-15,-5.957409950992936e-16,3.2491920424847875e-15,
                                     9.219910025371442e-15,1.2799168101873962e-14,1.7108898977066272e-14,
                                     2.855032832850196e-14,4.1048723593164446e-14,9.99499970701814,
                                     18.470571911228987,30.51830887197781,33.54443547978839,
                                     36.1607309907248,38.57320314001222,40.85924266944485,
                                     43.035391586290125,45.287513370945284,47.151147140980484,
                                     50.73276704593593,52.39952223577173,59.50068014643575,
                                     61.31379159039717,74.51253995296709,77.13729578952395,
                                     94.37203226002364,99.43109411760354,102.6487394625419,
                                     105.06218043709777,107.8179491492106,110.12126119755328,
                                     113.57945167126782,116.37801423876415,122.99832759829658,
                                     125.12209476580418,138.05692785144052,140.21814403606146,
                                     161.31660187081715,164.54676840029904,175.43647569692263,
                                     192.3282320950376,207.53405143733352,212.25537253729775,
                                     284.3628999051823,294.6884623476941,341.98290548531804,
                                     507.44043882416463,534.3382764375148,967.6392677402239,
                                     1875.507871717115,3171.7801027349824,5101.18005605357
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -5.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//===========//
// test # 59 //
//===========//

TEST(MatrixTest59, Heavy59)
{
     MatrixDense<double> matA;
     MatrixDense<double> eigenvectorsA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 5) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 5) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvectorsRealSymmetric(eigenvectorsA, "DSYEVX");

     // test

     const double arr_values99[100] = {3.6942314255214093e-22,
                                       1.9280574390556084e-21,
                                       8.336207484683114e-21,
                                       3.201738809930229e-20,
                                       1.128792365433472e-19,
                                       3.7219958073502635e-19,
                                       1.1616570563876596e-18,
                                       3.4603856776191363e-18,
                                       9.898106164891213e-18,
                                       2.7313034552506115e-17,
                                       7.297235551812733e-17,
                                       1.8931798526470812e-16,
                                       4.781008453916819e-16,
                                       1.1776583982769647e-15,
                                       2.8342405979299284e-15,
                                       6.674396729414062e-15,
                                       1.5399303294661802e-14,
                                       3.4849026635393604e-14,
                                       7.743020050094637e-14,
                                       1.6906046576799098e-13,
                                       3.630170569572799e-13,
                                       7.671355827326599e-13,
                                       1.5964469231753074e-12,
                                       3.2735991022182145e-12,
                                       6.617806646334326e-12,
                                       1.3195608156949319e-11,
                                       2.596350061894761e-11,
                                       5.0430333328453936e-11,
                                       9.67336578559975e-11,
                                       1.8330342627363303e-10,
                                       3.4324925188064113e-10,
                                       6.353651498186134e-10,
                                       1.1628708815038037e-9,
                                       2.104965971919157e-9,
                                       3.7693601315726985e-9,
                                       6.6787563519161844e-9,
                                       1.1711672807554324e-8,
                                       2.03292597117336e-8,
                                       3.4936698501777865e-8,
                                       5.9452893015691615e-8,
                                       1.0019897315247485e-7,
                                       1.672693861786821e-7,
                                       2.7662540139461026e-7,
                                       4.5325802559019753e-7,
                                       7.359163073250683e-7,
                                       1.1841012055800893e-6,
                                       1.8883009979218705e-6,
                                       2.984811021145573e-6,
                                       4.676960526639804e-6,
                                       7.265186654807506e-6,
                                       0.000011189174362740128,
                                       0.00001708624363917233,
                                       0.000025871327250800013,
                                       0.00003884524735147213,
                                       0.000057839551835632114,
                                       0.0000854078666854305,
                                       0.00012507547298611522,
                                       0.0001816604867751283,
                                       0.0002616813987570242,
                                       0.00037386653150919884,
                                       0.0005297808051872514,
                                       0.0007445835839285321,
                                       0.0010379277320564163,
                                       0.0014350037062010874,
                                       0.001967722735545796,
                                       0.0026760192519511626,
                                       0.0036092342363378766,
                                       0.00482751783582054,
                                       0.00640315989292051,
                                       0.008421723355192411,
                                       0.010982819682174948,
                                       0.014200329986124536,
                                       0.018201824732492763,
                                       0.023126912983358,
                                       0.029124247016656085,
                                       0.03634692535629019,
                                       0.04494587964943125,
                                       0.055061139884829774,
                                       0.0668110106541383,
                                       0.08027936243852593,
                                       0.09549945152005936,
                                       0.11243731017783884,
                                       0.13097574227719463,
                                       0.15090013051721174,
                                       0.17186879268938443,
                                       0.19341147439162046,
                                       0.21493488140894385,
                                       0.23573601738701488,
                                       0.2548580930562397,
                                       0.27133687410309687,
                                       0.28424352285526244,
                                       0.29273026481733555,
                                       0.29466646943807095,
                                       0.28996990773813897,
                                       0.27878021480447934,
                                       0.26146670556930435,
                                       0.22712948196564564,
                                       0.19198046679049208,
                                       0.15683996268683814,
                                       0.1225409827758915
                                      };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,99)) - abs(arr_values99[i])) < pow(10.0, -10.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//===========//
// test # 60 //
//===========//

TEST(MatrixTest60, Heavy60)
{
     MatrixDense<double> matA;
     MatrixDense<double> eigenvectorsA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 20) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 20) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvectorsRealSymmetric(eigenvectorsA, "DSYEVX");

     // test data # 1

     const double arr_values99[100] = {-0.00002379437463800071,-0.0000321126535355192,
                                       -0.00004261209009056319,-0.00005574228105018768,
                                       -0.0000720258070448845,-0.00009206699311457821,
                                       -0.00011656127426176481,-0.00014630515846035676,
                                       -0.0001822067730093074,-0.00022529697290878234,
                                       -0.00027674098203948964,-0.0003378505293342626,
                                       -0.00041009643284298355,-0.0004951215746125915,
                                       -0.0005947541986465926,-0.0007110214528956958,-0.0008461630842956044,
                                       -0.001002645183353377,-0.0011831738617460526,-0.0013907087329034266,
                                       -0.0016283827619966063,-0.0018997449209395513,-0.002208573606467658,
                                       -0.002558944309118129,-0.0029552087127267114,-0.0034019962501085446,
                                       -0.003904213875493434,-0.00446704386484559,-0.00509593944935361,
                                       -0.005796618082542783,-0.006575052137809326,-0.007437456830885415,
                                       -0.00839027516100404,-0.009440159665541611,-0.010593950785877937,
                                       -0.01185865164733851,-0.013241399063586366,-0.014749430585927434,
                                       -0.0163900474309,-0.018170573135449803,-0.0200983089054282,
                                       -0.022180481856340313,-0.024424190854787288,-0.026834080780647522,
                                       -0.02941440937535356,-0.03216899598488147,-0.035101168178730414,
                                       -0.03821370638490854,-0.04150878671030463,-0.04498792214706611,
                                       -0.04865190239866414,-0.05250073259412269,-0.056533571195309565,
                                       -0.06074866744009344,-0.0651432987033876,-0.06971370819842077,
                                       -0.07445504348175375,-0.07936129626731185,-0.08442524409670342,
                                       -0.08963839445497533,-0.09499093194079421,-0.10047166921101545,
                                       -0.10595789789842808,-0.11143628534678302,-0.11689273862688677,
                                       -0.1223124096438104,-0.12767970465965733,-0.13297829857254914,
                                       -0.13819115429332646,-0.1433005475596976,-0.14828809752292466,
                                       -0.15313480343431787,-0.15782108774752324,-0.16232684593753477,
                                       -0.16663150331824728,-0.17071407911689127,-0.17455325803557586,
                                       -0.17812746949713273,-0.18141497473424503,-0.18439396183722762,
                                       -0.18704264882716626,-0.18541168339207653,-0.18356676120058393,
                                       -0.18150272926411598,-0.17921485423076686,-0.17669886945503793,
                                       -0.17395102341224067,-0.17096812928970723,-0.1677476155625255,
                                       -0.1642875773360043,-0.16058682821057293,-0.15664495239745393,
                                       -0.15246235678535205,-0.14804032262974395,-0.14338105650732136,
                                       -0.13848774014895246,-0.1333645787354236,-0.128016847211483,
                                       -0.1224509341456258,-0.11667438263597216
                                      };

     // test data # 2

     const double arr_values98[100] = {0.0005490815006598427,0.0007217004212979288,0.0009319727820738912,
                                       0.0011856764796110383,0.0014891369301256077,0.0018492353084341568,
                                       0.002273410759398609,0.002769655716932461,0.003346503443557062,
                                       0.00401300689217423,0.0047787079930774546,0.00565359648519078,
                                       0.006648057443072511,0.007772806702325777,0.009038813457661714,
                                       0.010457209401826704,0.012039183891666605,0.013795864771304657,
                                       0.01573818465305239,0.01787673265523282,0.020218129530799774,
                                       0.022773522732384158,0.02555088599547039,0.02855699048006569,
                                       0.03179740706616532,0.03527631554658473,0.038996305859039175,
                                       0.04295817276333242,0.047160705650423344,0.05160047546600458,
                                       0.056271621039461366,0.061165637426456754,0.06627116919601259,
                                       0.07157381191620707,0.0770559254111312,0.08269646266941802,
                                       0.08847081857459133,0.09435070289203276,0.10030404217816305,
                                       0.10629491546542182,0.11228359420772163,0.11822646848984367,
                                       0.12407615562938958,0.12979275139874066,0.13533385216700752,
                                       0.14065467360130016,0.14570820649639804,0.1504454123825699,
                                       0.15481546139101132,0.15876601462676446,0.16224355300490953,
                                       0.1651937541424425,0.16756191846119972,0.16929344514268863,
                                       0.17033435798074814,0.17063188050057534,0.17013505895196374,
                                       0.16879543094112387,0.16656773654130375,0.16341066772151153,
                                       0.1592876487959431,0.15416764632985688,0.14836208495033895,
                                       0.14186022973422263,0.1346552528267162,0.12674464039743252,
                                       0.118130599994188,0.10882046380600134,0.09882708283296263,
                                       0.08816920644738636,0.07687184132594177,0.06496658324539006,
                                       0.052491914775380344,0.039493461481783246,0.026024198885649517,
                                       0.012144602119448178,-0.002077270001960313,-0.0165657649080691,
                                       -0.0312377086295896,-0.04600256891175273,-0.06076271707085829,
                                       -0.06795927022946352,-0.07503806683258157,-0.0819579039270416,
                                       -0.08867572326732134,-0.09514679596043041,-0.1013249415936529,
                                       -0.10716278331612594,-0.1126120400798519,-0.11762385692798413,
                                       -0.12214917384544385,-0.12613913325700105,-0.12954552576940606,
                                       -0.1323212732062947,-0.13442094737761986,-0.13580132236057105,
                                       -0.13642195734879647,-0.13624580635507777,-0.1352398502347542,
                                       -0.13337574564008783
                                      };

     // test data # 3

     const double arr_values97[100] = {-0.0048844047935863765,-0.006236486880389232,-0.007815274325331554,
                                       -0.009640183890712983,-0.011730050533458039,-0.014102771342375649,
                                       -0.016774922205544347,-0.019761352037216788,-0.023074760743478643,
                                       -0.026725268499719908,-0.03071998531275152,-0.03506259120033407,
                                       -0.03975293858755401,-0.04478668963230909,-0.050155002084304526,
                                       -0.05584427788164485,-0.06183598892150303,-0.06810659423095557,
                                       -0.07462756203769172,-0.08136550893055443,-0.08823038013976417,
                                       -0.09520849071940404,-0.1022476308045662,-0.10929156383600795,
                                       -0.11627847844623534,-0.12314131678294159,-0.1298082479646105,
                                       -0.13620329680129123,-0.14224713548726084,-0.14785804289917964,
                                       -0.15295303238959757,-0.1574491445459654,-0.1612648963069822,
                                       -0.16432187213530286,-0.16654643671328404,-0.16787154196597193,
                                       -0.16823859426980445,-0.16759934066305365,-0.16591772596198778,
                                       -0.1631716661722737,-0.15935634408053131,-0.15448321146294608,
                                       -0.14858218139776455,-0.1416381780493364,-0.1336490652093021,
                                       -0.12462715724623677,-0.11460064260169803,-0.10361487512764765,
                                       -0.09173348472751607,-0.07903925562502323,-0.06563471836746396,
                                       -0.051642400613860905,-0.03720468211196078,-0.022483201281379775,
                                       -0.007657764735299888,0.007075282886553215,0.021505263042677962,
                                       0.03540996481701064,0.04855905391412624,0.06071808324036558,
                                       0.07165316959606502,0.08113608035225883,0.09009849834439126,
                                       0.09838663571146652,0.10584187751947657,0.11230295059241002,
                                       0.11760842867321217,0.12159956720711124,0.12412345180969263,
                                       0.1250364341022301,0.12420781716730643,0.12152374055295379,
                                       0.11689120174753254,0.11024213763900717,0.10153747600764697,
                                       0.09077105399613189,0.07797328823931185,0.06321447046527728,
                                       0.04660755350441126,0.028310286413503014,0.008526547885299433,
                                       0.0025719295116293645,-0.004200598749307477,-0.011762440439929252,
                                       -0.020069965854842452,-0.029063538293552106,-0.038666727752430484,
                                       -0.048785756486008615,-0.05930922217827011,-0.07010814475633873,
                                       -0.08103638195706307,-0.09193145642839495,-0.10261583322770457,
                                       -0.11289868090088843,-0.12257814174844638,-0.1314441273038225,
                                       -0.139281643409791,-0.14587463558538188,-0.15101032970590494,
                                       -0.15448402553323257
                                      };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,99)) - abs(arr_values99[i])) < pow(10.0, -10.0));
     }

     // test # 2

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,98)) - abs(arr_values98[i])) < pow(10.0, -10.0));
     }

     // test # 3

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,97)) - abs(arr_values97[i])) < pow(10.0, -10.0));
     }

     // test # 4

     EXPECT_TRUE(res == 0);
}

//===========//
// test # 61 //
//===========//

TEST(MatrixTest61, Heavy61)
{
     MatrixDense<double> matA;
     MatrixDense<double> eigenvectorsA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 5) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 5) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigensystemRealSymmetric(eigenvectorsA, eigenvaluesA, "DSYEV");

     // test

     const double arr_values[100] = {-323.38478082263765, -318.95649570052416, -259.5812044929057,
                                     -255.47902023149175, -210.8574832531544, -207.046095983574,
                                     -172.17668982103106, -169.63822144706373, -167.11362728657673,
                                     -164.65335127248554, -137.93078894722365, -135.63554168942136,
                                     -133.27655590262873, -130.90313825750582, -111.16491214977447,
                                     -108.69201485977163, -104.86333072392988, -102.09654131372476,
                                     -89.52930445237439, -87.11011352113846, -79.24098389717554,
                                     -76.62676551546362, -71.01471245774239, -68.78074078937429,
                                     -57.39730933358366, -55.597048251741185, -53.807888693814036,
                                     -52.082337027364105, -42.001162570408376, -40.10702874995469,
                                     -36.13941170041208, -34.10866137118017, -29.537625276581757,
                                     -27.807512581350473, -20.493485216120316, -19.180726411630474,
                                     -17.86587524596272, -16.594313970872253, -10.527129255160773,
                                     -9.235915164234662, -6.274431175607891, -4.736234460618425,
                                     -3.113902006580256, -2.3717281394985243, -0.07129875210819334,
                                     2.4178759119876623, 3.3310614360036803, 4.154955748257782,
                                     8.633411865685325, 10.366556352869488, 12.095377640289747,
                                     14.018873556316715, 17.48114915683801, 21.917608573061386,
                                     23.316848960689295, 27.074595655585338, 31.322004721507344,
                                     34.9238763246869, 36.70168716147582, 39.12924266179792,
                                     48.291435480841905, 50.073858853444456, 52.12562986320187,
                                     57.27293729356433, 64.70819943681298, 66.93714122637961,
                                     68.82639047739008, 80.9665198923797, 83.5742202822415,
                                     86.58950836310686, 89.44951473838972, 102.06705367044331,
                                     107.13994334023343, 109.44821746324266, 117.55286230732882,
                                     126.41194103599305, 133.5454916018076, 135.893724579225,
                                     151.52771532059967, 157.8972863072518, 165.22044899155756,
                                     167.77180305861472, 190.84742611813815, 205.77684982675112,
                                     208.5174101551291, 234.35823130936174, 279.92197126989515,
                                     330.06056030550724, 384.6942438609073, 444.0868835363212,
                                     508.62422446622827, 578.8097468653175, 655.2951900145777,
                                     738.9379634908137, 830.8998363959786, 932.8239020042909,
                                     1047.1805607335104, 1178.0384559097015, 1333.1755507750008,
                                     1532.5774637949444
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -10.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//===========//
// test # 62 //
//===========//

TEST(MatrixTest62, Heavy62)
{
     MatrixDense<double> matA;
     MatrixDense<double> eigenvectorsA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 20) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 20) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigensystemRealSymmetric(eigenvectorsA, eigenvaluesA, "DSYEV");

     // test

     const double arr_values[100] = {-890.0679762687605,-833.96613152625,-387.9183497645402,
                                     -374.28858505936836,-325.97417429278767,-290.39317003462327,
                                     -243.09349980476634,-237.9671765884564,-184.65425879239723,
                                     -181.6433889406905,-152.42678092373745,-150.01440312367828,
                                     -140.20043836510496,-134.26239413119825,-130.50508513840435,
                                     -128.1365612192653,-119.6243787096752,-117.25234191992651,
                                     -112.47792700587912,-108.89915545206071,-104.74759545668331,
                                     -100.22968339474058,-94.96995783385485,-89.69665862307328,
                                     -86.4677796222291,-68.14673474241698,-66.07309844841615,
                                     -56.302362373674626,-54.63592515665142,-49.54128744936952,
                                     -47.77783428990662,-45.159157747842364,-43.112766536500985,
                                     -40.904881657341534,-38.624975888999664,-36.21821364136139,
                                     -33.61319247902676,-30.618705849873916,-26.992630355951256,
                                     -19.965626399359778,-9.761220034345175,-0.009675776079236078,
                                     -8.793180471674402e-7,-4.675655622579791e-14,-3.291874702847949e-14,
                                     -3.0408119421278934e-14,-2.2328449483329286e-14,
                                     -1.0693016765542536e-14,-7.240806196368183e-15,
                                     -5.761950647030487e-15,-5.957409950992936e-16,3.2491920424847875e-15,
                                     9.219910025371442e-15,1.2799168101873962e-14,1.7108898977066272e-14,
                                     2.855032832850196e-14,4.1048723593164446e-14,9.99499970701814,
                                     18.470571911228987,30.51830887197781,33.54443547978839,
                                     36.1607309907248,38.57320314001222,40.85924266944485,
                                     43.035391586290125,45.287513370945284,47.151147140980484,
                                     50.73276704593593,52.39952223577173,59.50068014643575,
                                     61.31379159039717,74.51253995296709,77.13729578952395,
                                     94.37203226002364,99.43109411760354,102.6487394625419,
                                     105.06218043709777,107.8179491492106,110.12126119755328,
                                     113.57945167126782,116.37801423876415,122.99832759829658,
                                     125.12209476580418,138.05692785144052,140.21814403606146,
                                     161.31660187081715,164.54676840029904,175.43647569692263,
                                     192.3282320950376,207.53405143733352,212.25537253729775,
                                     284.3628999051823,294.6884623476941,341.98290548531804,
                                     507.44043882416463,534.3382764375148,967.6392677402239,
                                     1875.507871717115,3171.7801027349824,5101.18005605357
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -10.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//===========//
// test # 63 //
//===========//

TEST(MatrixTest63, Heavy63)
{
     MatrixDense<double> matA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 5) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 5) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvaluesRealSymmetric(eigenvaluesA, "DSYEV");

     // test

     const double arr_values[100] = {-323.38478082263765, -318.95649570052416, -259.5812044929057,
                                     -255.47902023149175, -210.8574832531544, -207.046095983574,
                                     -172.17668982103106, -169.63822144706373, -167.11362728657673,
                                     -164.65335127248554, -137.93078894722365, -135.63554168942136,
                                     -133.27655590262873, -130.90313825750582, -111.16491214977447,
                                     -108.69201485977163, -104.86333072392988, -102.09654131372476,
                                     -89.52930445237439, -87.11011352113846, -79.24098389717554,
                                     -76.62676551546362, -71.01471245774239, -68.78074078937429,
                                     -57.39730933358366, -55.597048251741185, -53.807888693814036,
                                     -52.082337027364105, -42.001162570408376, -40.10702874995469,
                                     -36.13941170041208, -34.10866137118017, -29.537625276581757,
                                     -27.807512581350473, -20.493485216120316, -19.180726411630474,
                                     -17.86587524596272, -16.594313970872253, -10.527129255160773,
                                     -9.235915164234662, -6.274431175607891, -4.736234460618425,
                                     -3.113902006580256, -2.3717281394985243, -0.07129875210819334,
                                     2.4178759119876623, 3.3310614360036803, 4.154955748257782,
                                     8.633411865685325, 10.366556352869488, 12.095377640289747,
                                     14.018873556316715, 17.48114915683801, 21.917608573061386,
                                     23.316848960689295, 27.074595655585338, 31.322004721507344,
                                     34.9238763246869, 36.70168716147582, 39.12924266179792,
                                     48.291435480841905, 50.073858853444456, 52.12562986320187,
                                     57.27293729356433, 64.70819943681298, 66.93714122637961,
                                     68.82639047739008, 80.9665198923797, 83.5742202822415,
                                     86.58950836310686, 89.44951473838972, 102.06705367044331,
                                     107.13994334023343, 109.44821746324266, 117.55286230732882,
                                     126.41194103599305, 133.5454916018076, 135.893724579225,
                                     151.52771532059967, 157.8972863072518, 165.22044899155756,
                                     167.77180305861472, 190.84742611813815, 205.77684982675112,
                                     208.5174101551291, 234.35823130936174, 279.92197126989515,
                                     330.06056030550724, 384.6942438609073, 444.0868835363212,
                                     508.62422446622827, 578.8097468653175, 655.2951900145777,
                                     738.9379634908137, 830.8998363959786, 932.8239020042909,
                                     1047.1805607335104, 1178.0384559097015, 1333.1755507750008,
                                     1532.5774637949444
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -5.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//===========//
// test # 64 //
//===========//

TEST(MatrixTest64, Heavy64)
{
     MatrixDense<double> matA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 20) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 20) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvaluesRealSymmetric(eigenvaluesA, "DSYEV");

     // test

     const double arr_values[100] = {-890.0679762687605,-833.96613152625,-387.9183497645402,
                                     -374.28858505936836,-325.97417429278767,-290.39317003462327,
                                     -243.09349980476634,-237.9671765884564,-184.65425879239723,
                                     -181.6433889406905,-152.42678092373745,-150.01440312367828,
                                     -140.20043836510496,-134.26239413119825,-130.50508513840435,
                                     -128.1365612192653,-119.6243787096752,-117.25234191992651,
                                     -112.47792700587912,-108.89915545206071,-104.74759545668331,
                                     -100.22968339474058,-94.96995783385485,-89.69665862307328,
                                     -86.4677796222291,-68.14673474241698,-66.07309844841615,
                                     -56.302362373674626,-54.63592515665142,-49.54128744936952,
                                     -47.77783428990662,-45.159157747842364,-43.112766536500985,
                                     -40.904881657341534,-38.624975888999664,-36.21821364136139,
                                     -33.61319247902676,-30.618705849873916,-26.992630355951256,
                                     -19.965626399359778,-9.761220034345175,-0.009675776079236078,
                                     -8.793180471674402e-7,-4.675655622579791e-14,-3.291874702847949e-14,
                                     -3.0408119421278934e-14,-2.2328449483329286e-14,
                                     -1.0693016765542536e-14,-7.240806196368183e-15,
                                     -5.761950647030487e-15,-5.957409950992936e-16,3.2491920424847875e-15,
                                     9.219910025371442e-15,1.2799168101873962e-14,1.7108898977066272e-14,
                                     2.855032832850196e-14,4.1048723593164446e-14,9.99499970701814,
                                     18.470571911228987,30.51830887197781,33.54443547978839,
                                     36.1607309907248,38.57320314001222,40.85924266944485,
                                     43.035391586290125,45.287513370945284,47.151147140980484,
                                     50.73276704593593,52.39952223577173,59.50068014643575,
                                     61.31379159039717,74.51253995296709,77.13729578952395,
                                     94.37203226002364,99.43109411760354,102.6487394625419,
                                     105.06218043709777,107.8179491492106,110.12126119755328,
                                     113.57945167126782,116.37801423876415,122.99832759829658,
                                     125.12209476580418,138.05692785144052,140.21814403606146,
                                     161.31660187081715,164.54676840029904,175.43647569692263,
                                     192.3282320950376,207.53405143733352,212.25537253729775,
                                     284.3628999051823,294.6884623476941,341.98290548531804,
                                     507.44043882416463,534.3382764375148,967.6392677402239,
                                     1875.507871717115,3171.7801027349824,5101.18005605357
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -5.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//===========//
// test # 65 //
//===========//

TEST(MatrixTest65, Heavy65)
{
     MatrixDense<double> matA;
     MatrixDense<double> eigenvectorsA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 5) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 5) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvectorsRealSymmetric(eigenvectorsA, "DSYEV");

     // test

     const double arr_values99[100] = {3.6942314255214093e-22,
                                       1.9280574390556084e-21,
                                       8.336207484683114e-21,
                                       3.201738809930229e-20,
                                       1.128792365433472e-19,
                                       3.7219958073502635e-19,
                                       1.1616570563876596e-18,
                                       3.4603856776191363e-18,
                                       9.898106164891213e-18,
                                       2.7313034552506115e-17,
                                       7.297235551812733e-17,
                                       1.8931798526470812e-16,
                                       4.781008453916819e-16,
                                       1.1776583982769647e-15,
                                       2.8342405979299284e-15,
                                       6.674396729414062e-15,
                                       1.5399303294661802e-14,
                                       3.4849026635393604e-14,
                                       7.743020050094637e-14,
                                       1.6906046576799098e-13,
                                       3.630170569572799e-13,
                                       7.671355827326599e-13,
                                       1.5964469231753074e-12,
                                       3.2735991022182145e-12,
                                       6.617806646334326e-12,
                                       1.3195608156949319e-11,
                                       2.596350061894761e-11,
                                       5.0430333328453936e-11,
                                       9.67336578559975e-11,
                                       1.8330342627363303e-10,
                                       3.4324925188064113e-10,
                                       6.353651498186134e-10,
                                       1.1628708815038037e-9,
                                       2.104965971919157e-9,
                                       3.7693601315726985e-9,
                                       6.6787563519161844e-9,
                                       1.1711672807554324e-8,
                                       2.03292597117336e-8,
                                       3.4936698501777865e-8,
                                       5.9452893015691615e-8,
                                       1.0019897315247485e-7,
                                       1.672693861786821e-7,
                                       2.7662540139461026e-7,
                                       4.5325802559019753e-7,
                                       7.359163073250683e-7,
                                       1.1841012055800893e-6,
                                       1.8883009979218705e-6,
                                       2.984811021145573e-6,
                                       4.676960526639804e-6,
                                       7.265186654807506e-6,
                                       0.000011189174362740128,
                                       0.00001708624363917233,
                                       0.000025871327250800013,
                                       0.00003884524735147213,
                                       0.000057839551835632114,
                                       0.0000854078666854305,
                                       0.00012507547298611522,
                                       0.0001816604867751283,
                                       0.0002616813987570242,
                                       0.00037386653150919884,
                                       0.0005297808051872514,
                                       0.0007445835839285321,
                                       0.0010379277320564163,
                                       0.0014350037062010874,
                                       0.001967722735545796,
                                       0.0026760192519511626,
                                       0.0036092342363378766,
                                       0.00482751783582054,
                                       0.00640315989292051,
                                       0.008421723355192411,
                                       0.010982819682174948,
                                       0.014200329986124536,
                                       0.018201824732492763,
                                       0.023126912983358,
                                       0.029124247016656085,
                                       0.03634692535629019,
                                       0.04494587964943125,
                                       0.055061139884829774,
                                       0.0668110106541383,
                                       0.08027936243852593,
                                       0.09549945152005936,
                                       0.11243731017783884,
                                       0.13097574227719463,
                                       0.15090013051721174,
                                       0.17186879268938443,
                                       0.19341147439162046,
                                       0.21493488140894385,
                                       0.23573601738701488,
                                       0.2548580930562397,
                                       0.27133687410309687,
                                       0.28424352285526244,
                                       0.29273026481733555,
                                       0.29466646943807095,
                                       0.28996990773813897,
                                       0.27878021480447934,
                                       0.26146670556930435,
                                       0.22712948196564564,
                                       0.19198046679049208,
                                       0.15683996268683814,
                                       0.1225409827758915
                                      };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,99)) - abs(arr_values99[i])) < pow(10.0, -10.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//===========//
// test # 66 //
//===========//

TEST(MatrixTest66, Heavy66)
{
     MatrixDense<double> matA;
     MatrixDense<double> eigenvectorsA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 20) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 20) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvectorsRealSymmetric(eigenvectorsA, "DSYEV");

     // test data # 1

     const double arr_values99[100] = {-0.00002379437463800071,-0.0000321126535355192,
                                       -0.00004261209009056319,-0.00005574228105018768,
                                       -0.0000720258070448845,-0.00009206699311457821,
                                       -0.00011656127426176481,-0.00014630515846035676,
                                       -0.0001822067730093074,-0.00022529697290878234,
                                       -0.00027674098203948964,-0.0003378505293342626,
                                       -0.00041009643284298355,-0.0004951215746125915,
                                       -0.0005947541986465926,-0.0007110214528956958,-0.0008461630842956044,
                                       -0.001002645183353377,-0.0011831738617460526,-0.0013907087329034266,
                                       -0.0016283827619966063,-0.0018997449209395513,-0.002208573606467658,
                                       -0.002558944309118129,-0.0029552087127267114,-0.0034019962501085446,
                                       -0.003904213875493434,-0.00446704386484559,-0.00509593944935361,
                                       -0.005796618082542783,-0.006575052137809326,-0.007437456830885415,
                                       -0.00839027516100404,-0.009440159665541611,-0.010593950785877937,
                                       -0.01185865164733851,-0.013241399063586366,-0.014749430585927434,
                                       -0.0163900474309,-0.018170573135449803,-0.0200983089054282,
                                       -0.022180481856340313,-0.024424190854787288,-0.026834080780647522,
                                       -0.02941440937535356,-0.03216899598488147,-0.035101168178730414,
                                       -0.03821370638490854,-0.04150878671030463,-0.04498792214706611,
                                       -0.04865190239866414,-0.05250073259412269,-0.056533571195309565,
                                       -0.06074866744009344,-0.0651432987033876,-0.06971370819842077,
                                       -0.07445504348175375,-0.07936129626731185,-0.08442524409670342,
                                       -0.08963839445497533,-0.09499093194079421,-0.10047166921101545,
                                       -0.10595789789842808,-0.11143628534678302,-0.11689273862688677,
                                       -0.1223124096438104,-0.12767970465965733,-0.13297829857254914,
                                       -0.13819115429332646,-0.1433005475596976,-0.14828809752292466,
                                       -0.15313480343431787,-0.15782108774752324,-0.16232684593753477,
                                       -0.16663150331824728,-0.17071407911689127,-0.17455325803557586,
                                       -0.17812746949713273,-0.18141497473424503,-0.18439396183722762,
                                       -0.18704264882716626,-0.18541168339207653,-0.18356676120058393,
                                       -0.18150272926411598,-0.17921485423076686,-0.17669886945503793,
                                       -0.17395102341224067,-0.17096812928970723,-0.1677476155625255,
                                       -0.1642875773360043,-0.16058682821057293,-0.15664495239745393,
                                       -0.15246235678535205,-0.14804032262974395,-0.14338105650732136,
                                       -0.13848774014895246,-0.1333645787354236,-0.128016847211483,
                                       -0.1224509341456258,-0.11667438263597216
                                      };

     // test data # 2

     const double arr_values98[100] = {0.0005490815006598427,0.0007217004212979288,0.0009319727820738912,
                                       0.0011856764796110383,0.0014891369301256077,0.0018492353084341568,
                                       0.002273410759398609,0.002769655716932461,0.003346503443557062,
                                       0.00401300689217423,0.0047787079930774546,0.00565359648519078,
                                       0.006648057443072511,0.007772806702325777,0.009038813457661714,
                                       0.010457209401826704,0.012039183891666605,0.013795864771304657,
                                       0.01573818465305239,0.01787673265523282,0.020218129530799774,
                                       0.022773522732384158,0.02555088599547039,0.02855699048006569,
                                       0.03179740706616532,0.03527631554658473,0.038996305859039175,
                                       0.04295817276333242,0.047160705650423344,0.05160047546600458,
                                       0.056271621039461366,0.061165637426456754,0.06627116919601259,
                                       0.07157381191620707,0.0770559254111312,0.08269646266941802,
                                       0.08847081857459133,0.09435070289203276,0.10030404217816305,
                                       0.10629491546542182,0.11228359420772163,0.11822646848984367,
                                       0.12407615562938958,0.12979275139874066,0.13533385216700752,
                                       0.14065467360130016,0.14570820649639804,0.1504454123825699,
                                       0.15481546139101132,0.15876601462676446,0.16224355300490953,
                                       0.1651937541424425,0.16756191846119972,0.16929344514268863,
                                       0.17033435798074814,0.17063188050057534,0.17013505895196374,
                                       0.16879543094112387,0.16656773654130375,0.16341066772151153,
                                       0.1592876487959431,0.15416764632985688,0.14836208495033895,
                                       0.14186022973422263,0.1346552528267162,0.12674464039743252,
                                       0.118130599994188,0.10882046380600134,0.09882708283296263,
                                       0.08816920644738636,0.07687184132594177,0.06496658324539006,
                                       0.052491914775380344,0.039493461481783246,0.026024198885649517,
                                       0.012144602119448178,-0.002077270001960313,-0.0165657649080691,
                                       -0.0312377086295896,-0.04600256891175273,-0.06076271707085829,
                                       -0.06795927022946352,-0.07503806683258157,-0.0819579039270416,
                                       -0.08867572326732134,-0.09514679596043041,-0.1013249415936529,
                                       -0.10716278331612594,-0.1126120400798519,-0.11762385692798413,
                                       -0.12214917384544385,-0.12613913325700105,-0.12954552576940606,
                                       -0.1323212732062947,-0.13442094737761986,-0.13580132236057105,
                                       -0.13642195734879647,-0.13624580635507777,-0.1352398502347542,
                                       -0.13337574564008783
                                      };

     // test data # 3

     const double arr_values97[100] = {-0.0048844047935863765,-0.006236486880389232,-0.007815274325331554,
                                       -0.009640183890712983,-0.011730050533458039,-0.014102771342375649,
                                       -0.016774922205544347,-0.019761352037216788,-0.023074760743478643,
                                       -0.026725268499719908,-0.03071998531275152,-0.03506259120033407,
                                       -0.03975293858755401,-0.04478668963230909,-0.050155002084304526,
                                       -0.05584427788164485,-0.06183598892150303,-0.06810659423095557,
                                       -0.07462756203769172,-0.08136550893055443,-0.08823038013976417,
                                       -0.09520849071940404,-0.1022476308045662,-0.10929156383600795,
                                       -0.11627847844623534,-0.12314131678294159,-0.1298082479646105,
                                       -0.13620329680129123,-0.14224713548726084,-0.14785804289917964,
                                       -0.15295303238959757,-0.1574491445459654,-0.1612648963069822,
                                       -0.16432187213530286,-0.16654643671328404,-0.16787154196597193,
                                       -0.16823859426980445,-0.16759934066305365,-0.16591772596198778,
                                       -0.1631716661722737,-0.15935634408053131,-0.15448321146294608,
                                       -0.14858218139776455,-0.1416381780493364,-0.1336490652093021,
                                       -0.12462715724623677,-0.11460064260169803,-0.10361487512764765,
                                       -0.09173348472751607,-0.07903925562502323,-0.06563471836746396,
                                       -0.051642400613860905,-0.03720468211196078,-0.022483201281379775,
                                       -0.007657764735299888,0.007075282886553215,0.021505263042677962,
                                       0.03540996481701064,0.04855905391412624,0.06071808324036558,
                                       0.07165316959606502,0.08113608035225883,0.09009849834439126,
                                       0.09838663571146652,0.10584187751947657,0.11230295059241002,
                                       0.11760842867321217,0.12159956720711124,0.12412345180969263,
                                       0.1250364341022301,0.12420781716730643,0.12152374055295379,
                                       0.11689120174753254,0.11024213763900717,0.10153747600764697,
                                       0.09077105399613189,0.07797328823931185,0.06321447046527728,
                                       0.04660755350441126,0.028310286413503014,0.008526547885299433,
                                       0.0025719295116293645,-0.004200598749307477,-0.011762440439929252,
                                       -0.020069965854842452,-0.029063538293552106,-0.038666727752430484,
                                       -0.048785756486008615,-0.05930922217827011,-0.07010814475633873,
                                       -0.08103638195706307,-0.09193145642839495,-0.10261583322770457,
                                       -0.11289868090088843,-0.12257814174844638,-0.1314441273038225,
                                       -0.139281643409791,-0.14587463558538188,-0.15101032970590494,
                                       -0.15448402553323257
                                      };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,99)) - abs(arr_values99[i])) < pow(10.0, -10.0));
     }

     // test # 2

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,98)) - abs(arr_values98[i])) < pow(10.0, -10.0));
     }

     // test # 3

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,97)) - abs(arr_values97[i])) < pow(10.0, -10.0));
     }

     // test # 4

     EXPECT_TRUE(res == 0);
}

//===========//
// test # 67 //
//===========//

TEST(MatrixTest67, Heavy67)
{
     MatrixDense<complex<double>> matA;
     MatrixDense<double> matB;
     MatrixDense<double> matC;
     const int K_MAX = 20;

     // allocations

     const pgg::MAI DIM = 5*static_cast<pgg::MAI>(pow(10.0, 3.0));

     matA.Allocate(DIM);
     matB.Allocate(DIM);
     matC.Allocate(DIM);

     matA = {-1.23456, +3.45678};

     for (int k = 0; k != K_MAX; ++k) {
          matB.RealPart(matA);
          matC.ImaginaryPart(matA);
     }

     EXPECT_TRUE(matB == -1.23456);
     EXPECT_TRUE(matC == +3.45678);
}

//===========//
// test # 68 //
//===========//

TEST(MatrixTest68, Heavy68)
{
     MatrixDense<complex<double>> matA;
     MatrixDense<double> matB;
     MatrixDense<double> matC;
     const int K_MAX = 20;

     // allocations

     const pgg::MAI DIM = 5*static_cast<pgg::MAI>(pow(10.0, 3.0));

     matA.Allocate(DIM);
     matB.Allocate(DIM);
     matC.Allocate(DIM);

     // build matrix

     for (pgg::MAI_MAX i = 0; i != DIM; ++i) {
          for (pgg::MAI_MAX j = 0; j != DIM; ++j) {
               const complex<double> tmp_elem = { static_cast<double>(i),
                                                  static_cast<double>(j)
                                                };

               matA.SetElement(i,j, tmp_elem);
          }
     }

     // get real and imaginary parts

     for (int k = 0; k != K_MAX; ++k) {
          matB.RealPart(matA);
          matC.ImaginaryPart(matA);
     }

     // test

     for (pgg::MAI_MAX i = 0; i != DIM; ++i) {
          for (pgg::MAI_MAX j = 0; j != DIM; ++j) {
               EXPECT_TRUE(matB(i,j) == static_cast<double>(i));
               EXPECT_TRUE(matC(i,j) == static_cast<double>(j));
          }
     }
}

//===========//
// test # 69 //
//===========//

TEST(MatrixTest69, Heavy69)
{
     MatrixDense<complex<double>> matA;
     MatrixDense<complex<double>> eigenvectorsA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 5) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 5) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigensystemZHEEVD(eigenvectorsA, eigenvaluesA);

     // test

     const double arr_values[100] = {-323.38478082263765, -318.95649570052416, -259.5812044929057,
                                     -255.47902023149175, -210.8574832531544, -207.046095983574,
                                     -172.17668982103106, -169.63822144706373, -167.11362728657673,
                                     -164.65335127248554, -137.93078894722365, -135.63554168942136,
                                     -133.27655590262873, -130.90313825750582, -111.16491214977447,
                                     -108.69201485977163, -104.86333072392988, -102.09654131372476,
                                     -89.52930445237439, -87.11011352113846, -79.24098389717554,
                                     -76.62676551546362, -71.01471245774239, -68.78074078937429,
                                     -57.39730933358366, -55.597048251741185, -53.807888693814036,
                                     -52.082337027364105, -42.001162570408376, -40.10702874995469,
                                     -36.13941170041208, -34.10866137118017, -29.537625276581757,
                                     -27.807512581350473, -20.493485216120316, -19.180726411630474,
                                     -17.86587524596272, -16.594313970872253, -10.527129255160773,
                                     -9.235915164234662, -6.274431175607891, -4.736234460618425,
                                     -3.113902006580256, -2.3717281394985243, -0.07129875210819334,
                                     2.4178759119876623, 3.3310614360036803, 4.154955748257782,
                                     8.633411865685325, 10.366556352869488, 12.095377640289747,
                                     14.018873556316715, 17.48114915683801, 21.917608573061386,
                                     23.316848960689295, 27.074595655585338, 31.322004721507344,
                                     34.9238763246869, 36.70168716147582, 39.12924266179792,
                                     48.291435480841905, 50.073858853444456, 52.12562986320187,
                                     57.27293729356433, 64.70819943681298, 66.93714122637961,
                                     68.82639047739008, 80.9665198923797, 83.5742202822415,
                                     86.58950836310686, 89.44951473838972, 102.06705367044331,
                                     107.13994334023343, 109.44821746324266, 117.55286230732882,
                                     126.41194103599305, 133.5454916018076, 135.893724579225,
                                     151.52771532059967, 157.8972863072518, 165.22044899155756,
                                     167.77180305861472, 190.84742611813815, 205.77684982675112,
                                     208.5174101551291, 234.35823130936174, 279.92197126989515,
                                     330.06056030550724, 384.6942438609073, 444.0868835363212,
                                     508.62422446622827, 578.8097468653175, 655.2951900145777,
                                     738.9379634908137, 830.8998363959786, 932.8239020042909,
                                     1047.1805607335104, 1178.0384559097015, 1333.1755507750008,
                                     1532.5774637949444
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -10.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//===========//
// test # 70 //
//===========//

TEST(MatrixTest70, Heavy70)
{
     MatrixDense<complex<double>> matA;
     MatrixDense<complex<double>> eigenvectorsA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 20) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 20) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigensystemZHEEVD(eigenvectorsA, eigenvaluesA);

     // test

     const double arr_values[100] = {-890.0679762687605,-833.96613152625,-387.9183497645402,
                                     -374.28858505936836,-325.97417429278767,-290.39317003462327,
                                     -243.09349980476634,-237.9671765884564,-184.65425879239723,
                                     -181.6433889406905,-152.42678092373745,-150.01440312367828,
                                     -140.20043836510496,-134.26239413119825,-130.50508513840435,
                                     -128.1365612192653,-119.6243787096752,-117.25234191992651,
                                     -112.47792700587912,-108.89915545206071,-104.74759545668331,
                                     -100.22968339474058,-94.96995783385485,-89.69665862307328,
                                     -86.4677796222291,-68.14673474241698,-66.07309844841615,
                                     -56.302362373674626,-54.63592515665142,-49.54128744936952,
                                     -47.77783428990662,-45.159157747842364,-43.112766536500985,
                                     -40.904881657341534,-38.624975888999664,-36.21821364136139,
                                     -33.61319247902676,-30.618705849873916,-26.992630355951256,
                                     -19.965626399359778,-9.761220034345175,-0.009675776079236078,
                                     -8.793180471674402e-7,-4.675655622579791e-14,-3.291874702847949e-14,
                                     -3.0408119421278934e-14,-2.2328449483329286e-14,
                                     -1.0693016765542536e-14,-7.240806196368183e-15,
                                     -5.761950647030487e-15,-5.957409950992936e-16,3.2491920424847875e-15,
                                     9.219910025371442e-15,1.2799168101873962e-14,1.7108898977066272e-14,
                                     2.855032832850196e-14,4.1048723593164446e-14,9.99499970701814,
                                     18.470571911228987,30.51830887197781,33.54443547978839,
                                     36.1607309907248,38.57320314001222,40.85924266944485,
                                     43.035391586290125,45.287513370945284,47.151147140980484,
                                     50.73276704593593,52.39952223577173,59.50068014643575,
                                     61.31379159039717,74.51253995296709,77.13729578952395,
                                     94.37203226002364,99.43109411760354,102.6487394625419,
                                     105.06218043709777,107.8179491492106,110.12126119755328,
                                     113.57945167126782,116.37801423876415,122.99832759829658,
                                     125.12209476580418,138.05692785144052,140.21814403606146,
                                     161.31660187081715,164.54676840029904,175.43647569692263,
                                     192.3282320950376,207.53405143733352,212.25537253729775,
                                     284.3628999051823,294.6884623476941,341.98290548531804,
                                     507.44043882416463,534.3382764375148,967.6392677402239,
                                     1875.507871717115,3171.7801027349824,5101.18005605357
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -10.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//===========//
// test # 71 //
//===========//

TEST(MatrixTest71, Heavy71)
{
     MatrixDense<complex<double>> matA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 5) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 5) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvaluesZHEEVD(eigenvaluesA);

     // test

     const double arr_values[100] = {-323.38478082263765, -318.95649570052416, -259.5812044929057,
                                     -255.47902023149175, -210.8574832531544, -207.046095983574,
                                     -172.17668982103106, -169.63822144706373, -167.11362728657673,
                                     -164.65335127248554, -137.93078894722365, -135.63554168942136,
                                     -133.27655590262873, -130.90313825750582, -111.16491214977447,
                                     -108.69201485977163, -104.86333072392988, -102.09654131372476,
                                     -89.52930445237439, -87.11011352113846, -79.24098389717554,
                                     -76.62676551546362, -71.01471245774239, -68.78074078937429,
                                     -57.39730933358366, -55.597048251741185, -53.807888693814036,
                                     -52.082337027364105, -42.001162570408376, -40.10702874995469,
                                     -36.13941170041208, -34.10866137118017, -29.537625276581757,
                                     -27.807512581350473, -20.493485216120316, -19.180726411630474,
                                     -17.86587524596272, -16.594313970872253, -10.527129255160773,
                                     -9.235915164234662, -6.274431175607891, -4.736234460618425,
                                     -3.113902006580256, -2.3717281394985243, -0.07129875210819334,
                                     2.4178759119876623, 3.3310614360036803, 4.154955748257782,
                                     8.633411865685325, 10.366556352869488, 12.095377640289747,
                                     14.018873556316715, 17.48114915683801, 21.917608573061386,
                                     23.316848960689295, 27.074595655585338, 31.322004721507344,
                                     34.9238763246869, 36.70168716147582, 39.12924266179792,
                                     48.291435480841905, 50.073858853444456, 52.12562986320187,
                                     57.27293729356433, 64.70819943681298, 66.93714122637961,
                                     68.82639047739008, 80.9665198923797, 83.5742202822415,
                                     86.58950836310686, 89.44951473838972, 102.06705367044331,
                                     107.13994334023343, 109.44821746324266, 117.55286230732882,
                                     126.41194103599305, 133.5454916018076, 135.893724579225,
                                     151.52771532059967, 157.8972863072518, 165.22044899155756,
                                     167.77180305861472, 190.84742611813815, 205.77684982675112,
                                     208.5174101551291, 234.35823130936174, 279.92197126989515,
                                     330.06056030550724, 384.6942438609073, 444.0868835363212,
                                     508.62422446622827, 578.8097468653175, 655.2951900145777,
                                     738.9379634908137, 830.8998363959786, 932.8239020042909,
                                     1047.1805607335104, 1178.0384559097015, 1333.1755507750008,
                                     1532.5774637949444
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -5.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//===========//
// test # 72 //
//===========//

TEST(MatrixTest72, Heavy72)
{
     MatrixDense<complex<double>> matA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 20) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 20) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvaluesZHEEVD(eigenvaluesA);

     // test

     const double arr_values[100] = {-890.0679762687605,-833.96613152625,-387.9183497645402,
                                     -374.28858505936836,-325.97417429278767,-290.39317003462327,
                                     -243.09349980476634,-237.9671765884564,-184.65425879239723,
                                     -181.6433889406905,-152.42678092373745,-150.01440312367828,
                                     -140.20043836510496,-134.26239413119825,-130.50508513840435,
                                     -128.1365612192653,-119.6243787096752,-117.25234191992651,
                                     -112.47792700587912,-108.89915545206071,-104.74759545668331,
                                     -100.22968339474058,-94.96995783385485,-89.69665862307328,
                                     -86.4677796222291,-68.14673474241698,-66.07309844841615,
                                     -56.302362373674626,-54.63592515665142,-49.54128744936952,
                                     -47.77783428990662,-45.159157747842364,-43.112766536500985,
                                     -40.904881657341534,-38.624975888999664,-36.21821364136139,
                                     -33.61319247902676,-30.618705849873916,-26.992630355951256,
                                     -19.965626399359778,-9.761220034345175,-0.009675776079236078,
                                     -8.793180471674402e-7,-4.675655622579791e-14,-3.291874702847949e-14,
                                     -3.0408119421278934e-14,-2.2328449483329286e-14,
                                     -1.0693016765542536e-14,-7.240806196368183e-15,
                                     -5.761950647030487e-15,-5.957409950992936e-16,3.2491920424847875e-15,
                                     9.219910025371442e-15,1.2799168101873962e-14,1.7108898977066272e-14,
                                     2.855032832850196e-14,4.1048723593164446e-14,9.99499970701814,
                                     18.470571911228987,30.51830887197781,33.54443547978839,
                                     36.1607309907248,38.57320314001222,40.85924266944485,
                                     43.035391586290125,45.287513370945284,47.151147140980484,
                                     50.73276704593593,52.39952223577173,59.50068014643575,
                                     61.31379159039717,74.51253995296709,77.13729578952395,
                                     94.37203226002364,99.43109411760354,102.6487394625419,
                                     105.06218043709777,107.8179491492106,110.12126119755328,
                                     113.57945167126782,116.37801423876415,122.99832759829658,
                                     125.12209476580418,138.05692785144052,140.21814403606146,
                                     161.31660187081715,164.54676840029904,175.43647569692263,
                                     192.3282320950376,207.53405143733352,212.25537253729775,
                                     284.3628999051823,294.6884623476941,341.98290548531804,
                                     507.44043882416463,534.3382764375148,967.6392677402239,
                                     1875.507871717115,3171.7801027349824,5101.18005605357
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -5.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//===========//
// test # 73 //
//===========//

TEST(MatrixTest73, Heavy73)
{
     MatrixDense<complex<double>> matA;
     MatrixDense<complex<double>> eigenvectorsA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 5) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 5) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvectorsZHEEVD(eigenvectorsA);

     // test

     const double arr_values99[100] = {3.6942314255214093e-22,
                                       1.9280574390556084e-21,
                                       8.336207484683114e-21,
                                       3.201738809930229e-20,
                                       1.128792365433472e-19,
                                       3.7219958073502635e-19,
                                       1.1616570563876596e-18,
                                       3.4603856776191363e-18,
                                       9.898106164891213e-18,
                                       2.7313034552506115e-17,
                                       7.297235551812733e-17,
                                       1.8931798526470812e-16,
                                       4.781008453916819e-16,
                                       1.1776583982769647e-15,
                                       2.8342405979299284e-15,
                                       6.674396729414062e-15,
                                       1.5399303294661802e-14,
                                       3.4849026635393604e-14,
                                       7.743020050094637e-14,
                                       1.6906046576799098e-13,
                                       3.630170569572799e-13,
                                       7.671355827326599e-13,
                                       1.5964469231753074e-12,
                                       3.2735991022182145e-12,
                                       6.617806646334326e-12,
                                       1.3195608156949319e-11,
                                       2.596350061894761e-11,
                                       5.0430333328453936e-11,
                                       9.67336578559975e-11,
                                       1.8330342627363303e-10,
                                       3.4324925188064113e-10,
                                       6.353651498186134e-10,
                                       1.1628708815038037e-9,
                                       2.104965971919157e-9,
                                       3.7693601315726985e-9,
                                       6.6787563519161844e-9,
                                       1.1711672807554324e-8,
                                       2.03292597117336e-8,
                                       3.4936698501777865e-8,
                                       5.9452893015691615e-8,
                                       1.0019897315247485e-7,
                                       1.672693861786821e-7,
                                       2.7662540139461026e-7,
                                       4.5325802559019753e-7,
                                       7.359163073250683e-7,
                                       1.1841012055800893e-6,
                                       1.8883009979218705e-6,
                                       2.984811021145573e-6,
                                       4.676960526639804e-6,
                                       7.265186654807506e-6,
                                       0.000011189174362740128,
                                       0.00001708624363917233,
                                       0.000025871327250800013,
                                       0.00003884524735147213,
                                       0.000057839551835632114,
                                       0.0000854078666854305,
                                       0.00012507547298611522,
                                       0.0001816604867751283,
                                       0.0002616813987570242,
                                       0.00037386653150919884,
                                       0.0005297808051872514,
                                       0.0007445835839285321,
                                       0.0010379277320564163,
                                       0.0014350037062010874,
                                       0.001967722735545796,
                                       0.0026760192519511626,
                                       0.0036092342363378766,
                                       0.00482751783582054,
                                       0.00640315989292051,
                                       0.008421723355192411,
                                       0.010982819682174948,
                                       0.014200329986124536,
                                       0.018201824732492763,
                                       0.023126912983358,
                                       0.029124247016656085,
                                       0.03634692535629019,
                                       0.04494587964943125,
                                       0.055061139884829774,
                                       0.0668110106541383,
                                       0.08027936243852593,
                                       0.09549945152005936,
                                       0.11243731017783884,
                                       0.13097574227719463,
                                       0.15090013051721174,
                                       0.17186879268938443,
                                       0.19341147439162046,
                                       0.21493488140894385,
                                       0.23573601738701488,
                                       0.2548580930562397,
                                       0.27133687410309687,
                                       0.28424352285526244,
                                       0.29273026481733555,
                                       0.29466646943807095,
                                       0.28996990773813897,
                                       0.27878021480447934,
                                       0.26146670556930435,
                                       0.22712948196564564,
                                       0.19198046679049208,
                                       0.15683996268683814,
                                       0.1225409827758915
                                      };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,99)) - abs(arr_values99[i])) < pow(10.0, -10.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//===========//
// test # 74 //
//===========//

TEST(MatrixTest74, Heavy74)
{
     MatrixDense<complex<double>> matA;
     MatrixDense<complex<double>> eigenvectorsA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 20) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 20) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvectorsZHEEVD(eigenvectorsA);

     // test data # 1

     const double arr_values99[100] = {-0.00002379437463800071,-0.0000321126535355192,
                                       -0.00004261209009056319,-0.00005574228105018768,
                                       -0.0000720258070448845,-0.00009206699311457821,
                                       -0.00011656127426176481,-0.00014630515846035676,
                                       -0.0001822067730093074,-0.00022529697290878234,
                                       -0.00027674098203948964,-0.0003378505293342626,
                                       -0.00041009643284298355,-0.0004951215746125915,
                                       -0.0005947541986465926,-0.0007110214528956958,-0.0008461630842956044,
                                       -0.001002645183353377,-0.0011831738617460526,-0.0013907087329034266,
                                       -0.0016283827619966063,-0.0018997449209395513,-0.002208573606467658,
                                       -0.002558944309118129,-0.0029552087127267114,-0.0034019962501085446,
                                       -0.003904213875493434,-0.00446704386484559,-0.00509593944935361,
                                       -0.005796618082542783,-0.006575052137809326,-0.007437456830885415,
                                       -0.00839027516100404,-0.009440159665541611,-0.010593950785877937,
                                       -0.01185865164733851,-0.013241399063586366,-0.014749430585927434,
                                       -0.0163900474309,-0.018170573135449803,-0.0200983089054282,
                                       -0.022180481856340313,-0.024424190854787288,-0.026834080780647522,
                                       -0.02941440937535356,-0.03216899598488147,-0.035101168178730414,
                                       -0.03821370638490854,-0.04150878671030463,-0.04498792214706611,
                                       -0.04865190239866414,-0.05250073259412269,-0.056533571195309565,
                                       -0.06074866744009344,-0.0651432987033876,-0.06971370819842077,
                                       -0.07445504348175375,-0.07936129626731185,-0.08442524409670342,
                                       -0.08963839445497533,-0.09499093194079421,-0.10047166921101545,
                                       -0.10595789789842808,-0.11143628534678302,-0.11689273862688677,
                                       -0.1223124096438104,-0.12767970465965733,-0.13297829857254914,
                                       -0.13819115429332646,-0.1433005475596976,-0.14828809752292466,
                                       -0.15313480343431787,-0.15782108774752324,-0.16232684593753477,
                                       -0.16663150331824728,-0.17071407911689127,-0.17455325803557586,
                                       -0.17812746949713273,-0.18141497473424503,-0.18439396183722762,
                                       -0.18704264882716626,-0.18541168339207653,-0.18356676120058393,
                                       -0.18150272926411598,-0.17921485423076686,-0.17669886945503793,
                                       -0.17395102341224067,-0.17096812928970723,-0.1677476155625255,
                                       -0.1642875773360043,-0.16058682821057293,-0.15664495239745393,
                                       -0.15246235678535205,-0.14804032262974395,-0.14338105650732136,
                                       -0.13848774014895246,-0.1333645787354236,-0.128016847211483,
                                       -0.1224509341456258,-0.11667438263597216
                                      };

     // test data # 2

     const double arr_values98[100] = {0.0005490815006598427,0.0007217004212979288,0.0009319727820738912,
                                       0.0011856764796110383,0.0014891369301256077,0.0018492353084341568,
                                       0.002273410759398609,0.002769655716932461,0.003346503443557062,
                                       0.00401300689217423,0.0047787079930774546,0.00565359648519078,
                                       0.006648057443072511,0.007772806702325777,0.009038813457661714,
                                       0.010457209401826704,0.012039183891666605,0.013795864771304657,
                                       0.01573818465305239,0.01787673265523282,0.020218129530799774,
                                       0.022773522732384158,0.02555088599547039,0.02855699048006569,
                                       0.03179740706616532,0.03527631554658473,0.038996305859039175,
                                       0.04295817276333242,0.047160705650423344,0.05160047546600458,
                                       0.056271621039461366,0.061165637426456754,0.06627116919601259,
                                       0.07157381191620707,0.0770559254111312,0.08269646266941802,
                                       0.08847081857459133,0.09435070289203276,0.10030404217816305,
                                       0.10629491546542182,0.11228359420772163,0.11822646848984367,
                                       0.12407615562938958,0.12979275139874066,0.13533385216700752,
                                       0.14065467360130016,0.14570820649639804,0.1504454123825699,
                                       0.15481546139101132,0.15876601462676446,0.16224355300490953,
                                       0.1651937541424425,0.16756191846119972,0.16929344514268863,
                                       0.17033435798074814,0.17063188050057534,0.17013505895196374,
                                       0.16879543094112387,0.16656773654130375,0.16341066772151153,
                                       0.1592876487959431,0.15416764632985688,0.14836208495033895,
                                       0.14186022973422263,0.1346552528267162,0.12674464039743252,
                                       0.118130599994188,0.10882046380600134,0.09882708283296263,
                                       0.08816920644738636,0.07687184132594177,0.06496658324539006,
                                       0.052491914775380344,0.039493461481783246,0.026024198885649517,
                                       0.012144602119448178,-0.002077270001960313,-0.0165657649080691,
                                       -0.0312377086295896,-0.04600256891175273,-0.06076271707085829,
                                       -0.06795927022946352,-0.07503806683258157,-0.0819579039270416,
                                       -0.08867572326732134,-0.09514679596043041,-0.1013249415936529,
                                       -0.10716278331612594,-0.1126120400798519,-0.11762385692798413,
                                       -0.12214917384544385,-0.12613913325700105,-0.12954552576940606,
                                       -0.1323212732062947,-0.13442094737761986,-0.13580132236057105,
                                       -0.13642195734879647,-0.13624580635507777,-0.1352398502347542,
                                       -0.13337574564008783
                                      };

     // test data # 3

     const double arr_values97[100] = {-0.0048844047935863765,-0.006236486880389232,-0.007815274325331554,
                                       -0.009640183890712983,-0.011730050533458039,-0.014102771342375649,
                                       -0.016774922205544347,-0.019761352037216788,-0.023074760743478643,
                                       -0.026725268499719908,-0.03071998531275152,-0.03506259120033407,
                                       -0.03975293858755401,-0.04478668963230909,-0.050155002084304526,
                                       -0.05584427788164485,-0.06183598892150303,-0.06810659423095557,
                                       -0.07462756203769172,-0.08136550893055443,-0.08823038013976417,
                                       -0.09520849071940404,-0.1022476308045662,-0.10929156383600795,
                                       -0.11627847844623534,-0.12314131678294159,-0.1298082479646105,
                                       -0.13620329680129123,-0.14224713548726084,-0.14785804289917964,
                                       -0.15295303238959757,-0.1574491445459654,-0.1612648963069822,
                                       -0.16432187213530286,-0.16654643671328404,-0.16787154196597193,
                                       -0.16823859426980445,-0.16759934066305365,-0.16591772596198778,
                                       -0.1631716661722737,-0.15935634408053131,-0.15448321146294608,
                                       -0.14858218139776455,-0.1416381780493364,-0.1336490652093021,
                                       -0.12462715724623677,-0.11460064260169803,-0.10361487512764765,
                                       -0.09173348472751607,-0.07903925562502323,-0.06563471836746396,
                                       -0.051642400613860905,-0.03720468211196078,-0.022483201281379775,
                                       -0.007657764735299888,0.007075282886553215,0.021505263042677962,
                                       0.03540996481701064,0.04855905391412624,0.06071808324036558,
                                       0.07165316959606502,0.08113608035225883,0.09009849834439126,
                                       0.09838663571146652,0.10584187751947657,0.11230295059241002,
                                       0.11760842867321217,0.12159956720711124,0.12412345180969263,
                                       0.1250364341022301,0.12420781716730643,0.12152374055295379,
                                       0.11689120174753254,0.11024213763900717,0.10153747600764697,
                                       0.09077105399613189,0.07797328823931185,0.06321447046527728,
                                       0.04660755350441126,0.028310286413503014,0.008526547885299433,
                                       0.0025719295116293645,-0.004200598749307477,-0.011762440439929252,
                                       -0.020069965854842452,-0.029063538293552106,-0.038666727752430484,
                                       -0.048785756486008615,-0.05930922217827011,-0.07010814475633873,
                                       -0.08103638195706307,-0.09193145642839495,-0.10261583322770457,
                                       -0.11289868090088843,-0.12257814174844638,-0.1314441273038225,
                                       -0.139281643409791,-0.14587463558538188,-0.15101032970590494,
                                       -0.15448402553323257
                                      };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,99)) - abs(arr_values99[i])) < pow(10.0, -10.0));
     }

     // test # 2

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,98)) - abs(arr_values98[i])) < pow(10.0, -10.0));
     }

     // test # 3

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,97)) - abs(arr_values97[i])) < pow(10.0, -10.0));
     }

     // test # 4

     EXPECT_TRUE(res == 0);
}

//===========//
// test # 75 //
//===========//

TEST(MatrixTest75, Heavy75)
{
     MatrixDense<complex<double>> matA;
     MatrixDense<complex<double>> eigenvectorsA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 5) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 5) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigensystemZHEEVR(eigenvectorsA, eigenvaluesA);

     // test

     const double arr_values[100] = {-323.38478082263765, -318.95649570052416, -259.5812044929057,
                                     -255.47902023149175, -210.8574832531544, -207.046095983574,
                                     -172.17668982103106, -169.63822144706373, -167.11362728657673,
                                     -164.65335127248554, -137.93078894722365, -135.63554168942136,
                                     -133.27655590262873, -130.90313825750582, -111.16491214977447,
                                     -108.69201485977163, -104.86333072392988, -102.09654131372476,
                                     -89.52930445237439, -87.11011352113846, -79.24098389717554,
                                     -76.62676551546362, -71.01471245774239, -68.78074078937429,
                                     -57.39730933358366, -55.597048251741185, -53.807888693814036,
                                     -52.082337027364105, -42.001162570408376, -40.10702874995469,
                                     -36.13941170041208, -34.10866137118017, -29.537625276581757,
                                     -27.807512581350473, -20.493485216120316, -19.180726411630474,
                                     -17.86587524596272, -16.594313970872253, -10.527129255160773,
                                     -9.235915164234662, -6.274431175607891, -4.736234460618425,
                                     -3.113902006580256, -2.3717281394985243, -0.07129875210819334,
                                     2.4178759119876623, 3.3310614360036803, 4.154955748257782,
                                     8.633411865685325, 10.366556352869488, 12.095377640289747,
                                     14.018873556316715, 17.48114915683801, 21.917608573061386,
                                     23.316848960689295, 27.074595655585338, 31.322004721507344,
                                     34.9238763246869, 36.70168716147582, 39.12924266179792,
                                     48.291435480841905, 50.073858853444456, 52.12562986320187,
                                     57.27293729356433, 64.70819943681298, 66.93714122637961,
                                     68.82639047739008, 80.9665198923797, 83.5742202822415,
                                     86.58950836310686, 89.44951473838972, 102.06705367044331,
                                     107.13994334023343, 109.44821746324266, 117.55286230732882,
                                     126.41194103599305, 133.5454916018076, 135.893724579225,
                                     151.52771532059967, 157.8972863072518, 165.22044899155756,
                                     167.77180305861472, 190.84742611813815, 205.77684982675112,
                                     208.5174101551291, 234.35823130936174, 279.92197126989515,
                                     330.06056030550724, 384.6942438609073, 444.0868835363212,
                                     508.62422446622827, 578.8097468653175, 655.2951900145777,
                                     738.9379634908137, 830.8998363959786, 932.8239020042909,
                                     1047.1805607335104, 1178.0384559097015, 1333.1755507750008,
                                     1532.5774637949444
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -10.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//===========//
// test # 76 //
//===========//

TEST(MatrixTest76, Heavy76)
{
     MatrixDense<complex<double>> matA;
     MatrixDense<complex<double>> eigenvectorsA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 20) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 20) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigensystemZHEEVR(eigenvectorsA, eigenvaluesA);

     // test

     const double arr_values[100] = {-890.0679762687605,-833.96613152625,-387.9183497645402,
                                     -374.28858505936836,-325.97417429278767,-290.39317003462327,
                                     -243.09349980476634,-237.9671765884564,-184.65425879239723,
                                     -181.6433889406905,-152.42678092373745,-150.01440312367828,
                                     -140.20043836510496,-134.26239413119825,-130.50508513840435,
                                     -128.1365612192653,-119.6243787096752,-117.25234191992651,
                                     -112.47792700587912,-108.89915545206071,-104.74759545668331,
                                     -100.22968339474058,-94.96995783385485,-89.69665862307328,
                                     -86.4677796222291,-68.14673474241698,-66.07309844841615,
                                     -56.302362373674626,-54.63592515665142,-49.54128744936952,
                                     -47.77783428990662,-45.159157747842364,-43.112766536500985,
                                     -40.904881657341534,-38.624975888999664,-36.21821364136139,
                                     -33.61319247902676,-30.618705849873916,-26.992630355951256,
                                     -19.965626399359778,-9.761220034345175,-0.009675776079236078,
                                     -8.793180471674402e-7,-4.675655622579791e-14,-3.291874702847949e-14,
                                     -3.0408119421278934e-14,-2.2328449483329286e-14,
                                     -1.0693016765542536e-14,-7.240806196368183e-15,
                                     -5.761950647030487e-15,-5.957409950992936e-16,3.2491920424847875e-15,
                                     9.219910025371442e-15,1.2799168101873962e-14,1.7108898977066272e-14,
                                     2.855032832850196e-14,4.1048723593164446e-14,9.99499970701814,
                                     18.470571911228987,30.51830887197781,33.54443547978839,
                                     36.1607309907248,38.57320314001222,40.85924266944485,
                                     43.035391586290125,45.287513370945284,47.151147140980484,
                                     50.73276704593593,52.39952223577173,59.50068014643575,
                                     61.31379159039717,74.51253995296709,77.13729578952395,
                                     94.37203226002364,99.43109411760354,102.6487394625419,
                                     105.06218043709777,107.8179491492106,110.12126119755328,
                                     113.57945167126782,116.37801423876415,122.99832759829658,
                                     125.12209476580418,138.05692785144052,140.21814403606146,
                                     161.31660187081715,164.54676840029904,175.43647569692263,
                                     192.3282320950376,207.53405143733352,212.25537253729775,
                                     284.3628999051823,294.6884623476941,341.98290548531804,
                                     507.44043882416463,534.3382764375148,967.6392677402239,
                                     1875.507871717115,3171.7801027349824,5101.18005605357
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -10.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//===========//
// test # 77 //
//===========//

TEST(MatrixTest77, Heavy77)
{
     MatrixDense<complex<double>> matA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 5) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 5) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvaluesZHEEVR(eigenvaluesA);

     // test

     const double arr_values[100] = {-323.38478082263765, -318.95649570052416, -259.5812044929057,
                                     -255.47902023149175, -210.8574832531544, -207.046095983574,
                                     -172.17668982103106, -169.63822144706373, -167.11362728657673,
                                     -164.65335127248554, -137.93078894722365, -135.63554168942136,
                                     -133.27655590262873, -130.90313825750582, -111.16491214977447,
                                     -108.69201485977163, -104.86333072392988, -102.09654131372476,
                                     -89.52930445237439, -87.11011352113846, -79.24098389717554,
                                     -76.62676551546362, -71.01471245774239, -68.78074078937429,
                                     -57.39730933358366, -55.597048251741185, -53.807888693814036,
                                     -52.082337027364105, -42.001162570408376, -40.10702874995469,
                                     -36.13941170041208, -34.10866137118017, -29.537625276581757,
                                     -27.807512581350473, -20.493485216120316, -19.180726411630474,
                                     -17.86587524596272, -16.594313970872253, -10.527129255160773,
                                     -9.235915164234662, -6.274431175607891, -4.736234460618425,
                                     -3.113902006580256, -2.3717281394985243, -0.07129875210819334,
                                     2.4178759119876623, 3.3310614360036803, 4.154955748257782,
                                     8.633411865685325, 10.366556352869488, 12.095377640289747,
                                     14.018873556316715, 17.48114915683801, 21.917608573061386,
                                     23.316848960689295, 27.074595655585338, 31.322004721507344,
                                     34.9238763246869, 36.70168716147582, 39.12924266179792,
                                     48.291435480841905, 50.073858853444456, 52.12562986320187,
                                     57.27293729356433, 64.70819943681298, 66.93714122637961,
                                     68.82639047739008, 80.9665198923797, 83.5742202822415,
                                     86.58950836310686, 89.44951473838972, 102.06705367044331,
                                     107.13994334023343, 109.44821746324266, 117.55286230732882,
                                     126.41194103599305, 133.5454916018076, 135.893724579225,
                                     151.52771532059967, 157.8972863072518, 165.22044899155756,
                                     167.77180305861472, 190.84742611813815, 205.77684982675112,
                                     208.5174101551291, 234.35823130936174, 279.92197126989515,
                                     330.06056030550724, 384.6942438609073, 444.0868835363212,
                                     508.62422446622827, 578.8097468653175, 655.2951900145777,
                                     738.9379634908137, 830.8998363959786, 932.8239020042909,
                                     1047.1805607335104, 1178.0384559097015, 1333.1755507750008,
                                     1532.5774637949444
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -5.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//===========//
// test # 78 //
//===========//

TEST(MatrixTest78, Heavy78)
{
     MatrixDense<complex<double>> matA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 20) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 20) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvaluesZHEEVR(eigenvaluesA);

     // test

     const double arr_values[100] = {-890.0679762687605,-833.96613152625,-387.9183497645402,
                                     -374.28858505936836,-325.97417429278767,-290.39317003462327,
                                     -243.09349980476634,-237.9671765884564,-184.65425879239723,
                                     -181.6433889406905,-152.42678092373745,-150.01440312367828,
                                     -140.20043836510496,-134.26239413119825,-130.50508513840435,
                                     -128.1365612192653,-119.6243787096752,-117.25234191992651,
                                     -112.47792700587912,-108.89915545206071,-104.74759545668331,
                                     -100.22968339474058,-94.96995783385485,-89.69665862307328,
                                     -86.4677796222291,-68.14673474241698,-66.07309844841615,
                                     -56.302362373674626,-54.63592515665142,-49.54128744936952,
                                     -47.77783428990662,-45.159157747842364,-43.112766536500985,
                                     -40.904881657341534,-38.624975888999664,-36.21821364136139,
                                     -33.61319247902676,-30.618705849873916,-26.992630355951256,
                                     -19.965626399359778,-9.761220034345175,-0.009675776079236078,
                                     -8.793180471674402e-7,-4.675655622579791e-14,-3.291874702847949e-14,
                                     -3.0408119421278934e-14,-2.2328449483329286e-14,
                                     -1.0693016765542536e-14,-7.240806196368183e-15,
                                     -5.761950647030487e-15,-5.957409950992936e-16,3.2491920424847875e-15,
                                     9.219910025371442e-15,1.2799168101873962e-14,1.7108898977066272e-14,
                                     2.855032832850196e-14,4.1048723593164446e-14,9.99499970701814,
                                     18.470571911228987,30.51830887197781,33.54443547978839,
                                     36.1607309907248,38.57320314001222,40.85924266944485,
                                     43.035391586290125,45.287513370945284,47.151147140980484,
                                     50.73276704593593,52.39952223577173,59.50068014643575,
                                     61.31379159039717,74.51253995296709,77.13729578952395,
                                     94.37203226002364,99.43109411760354,102.6487394625419,
                                     105.06218043709777,107.8179491492106,110.12126119755328,
                                     113.57945167126782,116.37801423876415,122.99832759829658,
                                     125.12209476580418,138.05692785144052,140.21814403606146,
                                     161.31660187081715,164.54676840029904,175.43647569692263,
                                     192.3282320950376,207.53405143733352,212.25537253729775,
                                     284.3628999051823,294.6884623476941,341.98290548531804,
                                     507.44043882416463,534.3382764375148,967.6392677402239,
                                     1875.507871717115,3171.7801027349824,5101.18005605357
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -5.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//===========//
// test # 79 //
//===========//

TEST(MatrixTest79, Heavy79)
{
     MatrixDense<complex<double>> matA;
     MatrixDense<complex<double>> eigenvectorsA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 5) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 5) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvectorsZHEEVR(eigenvectorsA);

     // test

     const double arr_values99[100] = {3.6942314255214093e-22,
                                       1.9280574390556084e-21,
                                       8.336207484683114e-21,
                                       3.201738809930229e-20,
                                       1.128792365433472e-19,
                                       3.7219958073502635e-19,
                                       1.1616570563876596e-18,
                                       3.4603856776191363e-18,
                                       9.898106164891213e-18,
                                       2.7313034552506115e-17,
                                       7.297235551812733e-17,
                                       1.8931798526470812e-16,
                                       4.781008453916819e-16,
                                       1.1776583982769647e-15,
                                       2.8342405979299284e-15,
                                       6.674396729414062e-15,
                                       1.5399303294661802e-14,
                                       3.4849026635393604e-14,
                                       7.743020050094637e-14,
                                       1.6906046576799098e-13,
                                       3.630170569572799e-13,
                                       7.671355827326599e-13,
                                       1.5964469231753074e-12,
                                       3.2735991022182145e-12,
                                       6.617806646334326e-12,
                                       1.3195608156949319e-11,
                                       2.596350061894761e-11,
                                       5.0430333328453936e-11,
                                       9.67336578559975e-11,
                                       1.8330342627363303e-10,
                                       3.4324925188064113e-10,
                                       6.353651498186134e-10,
                                       1.1628708815038037e-9,
                                       2.104965971919157e-9,
                                       3.7693601315726985e-9,
                                       6.6787563519161844e-9,
                                       1.1711672807554324e-8,
                                       2.03292597117336e-8,
                                       3.4936698501777865e-8,
                                       5.9452893015691615e-8,
                                       1.0019897315247485e-7,
                                       1.672693861786821e-7,
                                       2.7662540139461026e-7,
                                       4.5325802559019753e-7,
                                       7.359163073250683e-7,
                                       1.1841012055800893e-6,
                                       1.8883009979218705e-6,
                                       2.984811021145573e-6,
                                       4.676960526639804e-6,
                                       7.265186654807506e-6,
                                       0.000011189174362740128,
                                       0.00001708624363917233,
                                       0.000025871327250800013,
                                       0.00003884524735147213,
                                       0.000057839551835632114,
                                       0.0000854078666854305,
                                       0.00012507547298611522,
                                       0.0001816604867751283,
                                       0.0002616813987570242,
                                       0.00037386653150919884,
                                       0.0005297808051872514,
                                       0.0007445835839285321,
                                       0.0010379277320564163,
                                       0.0014350037062010874,
                                       0.001967722735545796,
                                       0.0026760192519511626,
                                       0.0036092342363378766,
                                       0.00482751783582054,
                                       0.00640315989292051,
                                       0.008421723355192411,
                                       0.010982819682174948,
                                       0.014200329986124536,
                                       0.018201824732492763,
                                       0.023126912983358,
                                       0.029124247016656085,
                                       0.03634692535629019,
                                       0.04494587964943125,
                                       0.055061139884829774,
                                       0.0668110106541383,
                                       0.08027936243852593,
                                       0.09549945152005936,
                                       0.11243731017783884,
                                       0.13097574227719463,
                                       0.15090013051721174,
                                       0.17186879268938443,
                                       0.19341147439162046,
                                       0.21493488140894385,
                                       0.23573601738701488,
                                       0.2548580930562397,
                                       0.27133687410309687,
                                       0.28424352285526244,
                                       0.29273026481733555,
                                       0.29466646943807095,
                                       0.28996990773813897,
                                       0.27878021480447934,
                                       0.26146670556930435,
                                       0.22712948196564564,
                                       0.19198046679049208,
                                       0.15683996268683814,
                                       0.1225409827758915
                                      };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,99)) - abs(arr_values99[i])) < pow(10.0, -10.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//===========//
// test # 80 //
//===========//

TEST(MatrixTest80, Heavy80)
{
     MatrixDense<complex<double>> matA;
     MatrixDense<complex<double>> eigenvectorsA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 20) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 20) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvectorsZHEEVR(eigenvectorsA);

     // test data # 1

     const double arr_values99[100] = {-0.00002379437463800071,-0.0000321126535355192,
                                       -0.00004261209009056319,-0.00005574228105018768,
                                       -0.0000720258070448845,-0.00009206699311457821,
                                       -0.00011656127426176481,-0.00014630515846035676,
                                       -0.0001822067730093074,-0.00022529697290878234,
                                       -0.00027674098203948964,-0.0003378505293342626,
                                       -0.00041009643284298355,-0.0004951215746125915,
                                       -0.0005947541986465926,-0.0007110214528956958,-0.0008461630842956044,
                                       -0.001002645183353377,-0.0011831738617460526,-0.0013907087329034266,
                                       -0.0016283827619966063,-0.0018997449209395513,-0.002208573606467658,
                                       -0.002558944309118129,-0.0029552087127267114,-0.0034019962501085446,
                                       -0.003904213875493434,-0.00446704386484559,-0.00509593944935361,
                                       -0.005796618082542783,-0.006575052137809326,-0.007437456830885415,
                                       -0.00839027516100404,-0.009440159665541611,-0.010593950785877937,
                                       -0.01185865164733851,-0.013241399063586366,-0.014749430585927434,
                                       -0.0163900474309,-0.018170573135449803,-0.0200983089054282,
                                       -0.022180481856340313,-0.024424190854787288,-0.026834080780647522,
                                       -0.02941440937535356,-0.03216899598488147,-0.035101168178730414,
                                       -0.03821370638490854,-0.04150878671030463,-0.04498792214706611,
                                       -0.04865190239866414,-0.05250073259412269,-0.056533571195309565,
                                       -0.06074866744009344,-0.0651432987033876,-0.06971370819842077,
                                       -0.07445504348175375,-0.07936129626731185,-0.08442524409670342,
                                       -0.08963839445497533,-0.09499093194079421,-0.10047166921101545,
                                       -0.10595789789842808,-0.11143628534678302,-0.11689273862688677,
                                       -0.1223124096438104,-0.12767970465965733,-0.13297829857254914,
                                       -0.13819115429332646,-0.1433005475596976,-0.14828809752292466,
                                       -0.15313480343431787,-0.15782108774752324,-0.16232684593753477,
                                       -0.16663150331824728,-0.17071407911689127,-0.17455325803557586,
                                       -0.17812746949713273,-0.18141497473424503,-0.18439396183722762,
                                       -0.18704264882716626,-0.18541168339207653,-0.18356676120058393,
                                       -0.18150272926411598,-0.17921485423076686,-0.17669886945503793,
                                       -0.17395102341224067,-0.17096812928970723,-0.1677476155625255,
                                       -0.1642875773360043,-0.16058682821057293,-0.15664495239745393,
                                       -0.15246235678535205,-0.14804032262974395,-0.14338105650732136,
                                       -0.13848774014895246,-0.1333645787354236,-0.128016847211483,
                                       -0.1224509341456258,-0.11667438263597216
                                      };

     // test data # 2

     const double arr_values98[100] = {0.0005490815006598427,0.0007217004212979288,0.0009319727820738912,
                                       0.0011856764796110383,0.0014891369301256077,0.0018492353084341568,
                                       0.002273410759398609,0.002769655716932461,0.003346503443557062,
                                       0.00401300689217423,0.0047787079930774546,0.00565359648519078,
                                       0.006648057443072511,0.007772806702325777,0.009038813457661714,
                                       0.010457209401826704,0.012039183891666605,0.013795864771304657,
                                       0.01573818465305239,0.01787673265523282,0.020218129530799774,
                                       0.022773522732384158,0.02555088599547039,0.02855699048006569,
                                       0.03179740706616532,0.03527631554658473,0.038996305859039175,
                                       0.04295817276333242,0.047160705650423344,0.05160047546600458,
                                       0.056271621039461366,0.061165637426456754,0.06627116919601259,
                                       0.07157381191620707,0.0770559254111312,0.08269646266941802,
                                       0.08847081857459133,0.09435070289203276,0.10030404217816305,
                                       0.10629491546542182,0.11228359420772163,0.11822646848984367,
                                       0.12407615562938958,0.12979275139874066,0.13533385216700752,
                                       0.14065467360130016,0.14570820649639804,0.1504454123825699,
                                       0.15481546139101132,0.15876601462676446,0.16224355300490953,
                                       0.1651937541424425,0.16756191846119972,0.16929344514268863,
                                       0.17033435798074814,0.17063188050057534,0.17013505895196374,
                                       0.16879543094112387,0.16656773654130375,0.16341066772151153,
                                       0.1592876487959431,0.15416764632985688,0.14836208495033895,
                                       0.14186022973422263,0.1346552528267162,0.12674464039743252,
                                       0.118130599994188,0.10882046380600134,0.09882708283296263,
                                       0.08816920644738636,0.07687184132594177,0.06496658324539006,
                                       0.052491914775380344,0.039493461481783246,0.026024198885649517,
                                       0.012144602119448178,-0.002077270001960313,-0.0165657649080691,
                                       -0.0312377086295896,-0.04600256891175273,-0.06076271707085829,
                                       -0.06795927022946352,-0.07503806683258157,-0.0819579039270416,
                                       -0.08867572326732134,-0.09514679596043041,-0.1013249415936529,
                                       -0.10716278331612594,-0.1126120400798519,-0.11762385692798413,
                                       -0.12214917384544385,-0.12613913325700105,-0.12954552576940606,
                                       -0.1323212732062947,-0.13442094737761986,-0.13580132236057105,
                                       -0.13642195734879647,-0.13624580635507777,-0.1352398502347542,
                                       -0.13337574564008783
                                      };

     // test data # 3

     const double arr_values97[100] = {-0.0048844047935863765,-0.006236486880389232,-0.007815274325331554,
                                       -0.009640183890712983,-0.011730050533458039,-0.014102771342375649,
                                       -0.016774922205544347,-0.019761352037216788,-0.023074760743478643,
                                       -0.026725268499719908,-0.03071998531275152,-0.03506259120033407,
                                       -0.03975293858755401,-0.04478668963230909,-0.050155002084304526,
                                       -0.05584427788164485,-0.06183598892150303,-0.06810659423095557,
                                       -0.07462756203769172,-0.08136550893055443,-0.08823038013976417,
                                       -0.09520849071940404,-0.1022476308045662,-0.10929156383600795,
                                       -0.11627847844623534,-0.12314131678294159,-0.1298082479646105,
                                       -0.13620329680129123,-0.14224713548726084,-0.14785804289917964,
                                       -0.15295303238959757,-0.1574491445459654,-0.1612648963069822,
                                       -0.16432187213530286,-0.16654643671328404,-0.16787154196597193,
                                       -0.16823859426980445,-0.16759934066305365,-0.16591772596198778,
                                       -0.1631716661722737,-0.15935634408053131,-0.15448321146294608,
                                       -0.14858218139776455,-0.1416381780493364,-0.1336490652093021,
                                       -0.12462715724623677,-0.11460064260169803,-0.10361487512764765,
                                       -0.09173348472751607,-0.07903925562502323,-0.06563471836746396,
                                       -0.051642400613860905,-0.03720468211196078,-0.022483201281379775,
                                       -0.007657764735299888,0.007075282886553215,0.021505263042677962,
                                       0.03540996481701064,0.04855905391412624,0.06071808324036558,
                                       0.07165316959606502,0.08113608035225883,0.09009849834439126,
                                       0.09838663571146652,0.10584187751947657,0.11230295059241002,
                                       0.11760842867321217,0.12159956720711124,0.12412345180969263,
                                       0.1250364341022301,0.12420781716730643,0.12152374055295379,
                                       0.11689120174753254,0.11024213763900717,0.10153747600764697,
                                       0.09077105399613189,0.07797328823931185,0.06321447046527728,
                                       0.04660755350441126,0.028310286413503014,0.008526547885299433,
                                       0.0025719295116293645,-0.004200598749307477,-0.011762440439929252,
                                       -0.020069965854842452,-0.029063538293552106,-0.038666727752430484,
                                       -0.048785756486008615,-0.05930922217827011,-0.07010814475633873,
                                       -0.08103638195706307,-0.09193145642839495,-0.10261583322770457,
                                       -0.11289868090088843,-0.12257814174844638,-0.1314441273038225,
                                       -0.139281643409791,-0.14587463558538188,-0.15101032970590494,
                                       -0.15448402553323257
                                      };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,99)) - abs(arr_values99[i])) < pow(10.0, -10.0));
     }

     // test # 2

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,98)) - abs(arr_values98[i])) < pow(10.0, -10.0));
     }

     // test # 3

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,97)) - abs(arr_values97[i])) < pow(10.0, -10.0));
     }

     // test # 4

     EXPECT_TRUE(res == 0);
}

//===========//
// test # 81 //
//===========//

TEST(MatrixTest81, Heavy81)
{
     MatrixDense<complex<double>> matA;
     MatrixDense<complex<double>> eigenvectorsA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 5) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 5) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigensystemZHEEVX(eigenvectorsA, eigenvaluesA);

     // test

     const double arr_values[100] = {-323.38478082263765, -318.95649570052416, -259.5812044929057,
                                     -255.47902023149175, -210.8574832531544, -207.046095983574,
                                     -172.17668982103106, -169.63822144706373, -167.11362728657673,
                                     -164.65335127248554, -137.93078894722365, -135.63554168942136,
                                     -133.27655590262873, -130.90313825750582, -111.16491214977447,
                                     -108.69201485977163, -104.86333072392988, -102.09654131372476,
                                     -89.52930445237439, -87.11011352113846, -79.24098389717554,
                                     -76.62676551546362, -71.01471245774239, -68.78074078937429,
                                     -57.39730933358366, -55.597048251741185, -53.807888693814036,
                                     -52.082337027364105, -42.001162570408376, -40.10702874995469,
                                     -36.13941170041208, -34.10866137118017, -29.537625276581757,
                                     -27.807512581350473, -20.493485216120316, -19.180726411630474,
                                     -17.86587524596272, -16.594313970872253, -10.527129255160773,
                                     -9.235915164234662, -6.274431175607891, -4.736234460618425,
                                     -3.113902006580256, -2.3717281394985243, -0.07129875210819334,
                                     2.4178759119876623, 3.3310614360036803, 4.154955748257782,
                                     8.633411865685325, 10.366556352869488, 12.095377640289747,
                                     14.018873556316715, 17.48114915683801, 21.917608573061386,
                                     23.316848960689295, 27.074595655585338, 31.322004721507344,
                                     34.9238763246869, 36.70168716147582, 39.12924266179792,
                                     48.291435480841905, 50.073858853444456, 52.12562986320187,
                                     57.27293729356433, 64.70819943681298, 66.93714122637961,
                                     68.82639047739008, 80.9665198923797, 83.5742202822415,
                                     86.58950836310686, 89.44951473838972, 102.06705367044331,
                                     107.13994334023343, 109.44821746324266, 117.55286230732882,
                                     126.41194103599305, 133.5454916018076, 135.893724579225,
                                     151.52771532059967, 157.8972863072518, 165.22044899155756,
                                     167.77180305861472, 190.84742611813815, 205.77684982675112,
                                     208.5174101551291, 234.35823130936174, 279.92197126989515,
                                     330.06056030550724, 384.6942438609073, 444.0868835363212,
                                     508.62422446622827, 578.8097468653175, 655.2951900145777,
                                     738.9379634908137, 830.8998363959786, 932.8239020042909,
                                     1047.1805607335104, 1178.0384559097015, 1333.1755507750008,
                                     1532.5774637949444
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -10.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//===========//
// test # 82 //
//===========//

TEST(MatrixTest82, Heavy82)
{
     MatrixDense<complex<double>> matA;
     MatrixDense<complex<double>> eigenvectorsA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 20) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 20) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigensystemZHEEVX(eigenvectorsA, eigenvaluesA);

     // test

     const double arr_values[100] = {-890.0679762687605,-833.96613152625,-387.9183497645402,
                                     -374.28858505936836,-325.97417429278767,-290.39317003462327,
                                     -243.09349980476634,-237.9671765884564,-184.65425879239723,
                                     -181.6433889406905,-152.42678092373745,-150.01440312367828,
                                     -140.20043836510496,-134.26239413119825,-130.50508513840435,
                                     -128.1365612192653,-119.6243787096752,-117.25234191992651,
                                     -112.47792700587912,-108.89915545206071,-104.74759545668331,
                                     -100.22968339474058,-94.96995783385485,-89.69665862307328,
                                     -86.4677796222291,-68.14673474241698,-66.07309844841615,
                                     -56.302362373674626,-54.63592515665142,-49.54128744936952,
                                     -47.77783428990662,-45.159157747842364,-43.112766536500985,
                                     -40.904881657341534,-38.624975888999664,-36.21821364136139,
                                     -33.61319247902676,-30.618705849873916,-26.992630355951256,
                                     -19.965626399359778,-9.761220034345175,-0.009675776079236078,
                                     -8.793180471674402e-7,-4.675655622579791e-14,-3.291874702847949e-14,
                                     -3.0408119421278934e-14,-2.2328449483329286e-14,
                                     -1.0693016765542536e-14,-7.240806196368183e-15,
                                     -5.761950647030487e-15,-5.957409950992936e-16,3.2491920424847875e-15,
                                     9.219910025371442e-15,1.2799168101873962e-14,1.7108898977066272e-14,
                                     2.855032832850196e-14,4.1048723593164446e-14,9.99499970701814,
                                     18.470571911228987,30.51830887197781,33.54443547978839,
                                     36.1607309907248,38.57320314001222,40.85924266944485,
                                     43.035391586290125,45.287513370945284,47.151147140980484,
                                     50.73276704593593,52.39952223577173,59.50068014643575,
                                     61.31379159039717,74.51253995296709,77.13729578952395,
                                     94.37203226002364,99.43109411760354,102.6487394625419,
                                     105.06218043709777,107.8179491492106,110.12126119755328,
                                     113.57945167126782,116.37801423876415,122.99832759829658,
                                     125.12209476580418,138.05692785144052,140.21814403606146,
                                     161.31660187081715,164.54676840029904,175.43647569692263,
                                     192.3282320950376,207.53405143733352,212.25537253729775,
                                     284.3628999051823,294.6884623476941,341.98290548531804,
                                     507.44043882416463,534.3382764375148,967.6392677402239,
                                     1875.507871717115,3171.7801027349824,5101.18005605357
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -10.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//===========//
// test # 83 //
//===========//

TEST(MatrixTest83, Heavy83)
{
     MatrixDense<complex<double>> matA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 5) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 5) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvaluesZHEEVX(eigenvaluesA);

     // test

     const double arr_values[100] = {-323.38478082263765, -318.95649570052416, -259.5812044929057,
                                     -255.47902023149175, -210.8574832531544, -207.046095983574,
                                     -172.17668982103106, -169.63822144706373, -167.11362728657673,
                                     -164.65335127248554, -137.93078894722365, -135.63554168942136,
                                     -133.27655590262873, -130.90313825750582, -111.16491214977447,
                                     -108.69201485977163, -104.86333072392988, -102.09654131372476,
                                     -89.52930445237439, -87.11011352113846, -79.24098389717554,
                                     -76.62676551546362, -71.01471245774239, -68.78074078937429,
                                     -57.39730933358366, -55.597048251741185, -53.807888693814036,
                                     -52.082337027364105, -42.001162570408376, -40.10702874995469,
                                     -36.13941170041208, -34.10866137118017, -29.537625276581757,
                                     -27.807512581350473, -20.493485216120316, -19.180726411630474,
                                     -17.86587524596272, -16.594313970872253, -10.527129255160773,
                                     -9.235915164234662, -6.274431175607891, -4.736234460618425,
                                     -3.113902006580256, -2.3717281394985243, -0.07129875210819334,
                                     2.4178759119876623, 3.3310614360036803, 4.154955748257782,
                                     8.633411865685325, 10.366556352869488, 12.095377640289747,
                                     14.018873556316715, 17.48114915683801, 21.917608573061386,
                                     23.316848960689295, 27.074595655585338, 31.322004721507344,
                                     34.9238763246869, 36.70168716147582, 39.12924266179792,
                                     48.291435480841905, 50.073858853444456, 52.12562986320187,
                                     57.27293729356433, 64.70819943681298, 66.93714122637961,
                                     68.82639047739008, 80.9665198923797, 83.5742202822415,
                                     86.58950836310686, 89.44951473838972, 102.06705367044331,
                                     107.13994334023343, 109.44821746324266, 117.55286230732882,
                                     126.41194103599305, 133.5454916018076, 135.893724579225,
                                     151.52771532059967, 157.8972863072518, 165.22044899155756,
                                     167.77180305861472, 190.84742611813815, 205.77684982675112,
                                     208.5174101551291, 234.35823130936174, 279.92197126989515,
                                     330.06056030550724, 384.6942438609073, 444.0868835363212,
                                     508.62422446622827, 578.8097468653175, 655.2951900145777,
                                     738.9379634908137, 830.8998363959786, 932.8239020042909,
                                     1047.1805607335104, 1178.0384559097015, 1333.1755507750008,
                                     1532.5774637949444
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -5.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//===========//
// test # 84 //
//===========//

TEST(MatrixTest84, Heavy84)
{
     MatrixDense<complex<double>> matA;
     VectorDense<double> eigenvaluesA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvaluesA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 20) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 20) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvaluesZHEEVX(eigenvaluesA);

     // test

     const double arr_values[100] = {-890.0679762687605,-833.96613152625,-387.9183497645402,
                                     -374.28858505936836,-325.97417429278767,-290.39317003462327,
                                     -243.09349980476634,-237.9671765884564,-184.65425879239723,
                                     -181.6433889406905,-152.42678092373745,-150.01440312367828,
                                     -140.20043836510496,-134.26239413119825,-130.50508513840435,
                                     -128.1365612192653,-119.6243787096752,-117.25234191992651,
                                     -112.47792700587912,-108.89915545206071,-104.74759545668331,
                                     -100.22968339474058,-94.96995783385485,-89.69665862307328,
                                     -86.4677796222291,-68.14673474241698,-66.07309844841615,
                                     -56.302362373674626,-54.63592515665142,-49.54128744936952,
                                     -47.77783428990662,-45.159157747842364,-43.112766536500985,
                                     -40.904881657341534,-38.624975888999664,-36.21821364136139,
                                     -33.61319247902676,-30.618705849873916,-26.992630355951256,
                                     -19.965626399359778,-9.761220034345175,-0.009675776079236078,
                                     -8.793180471674402e-7,-4.675655622579791e-14,-3.291874702847949e-14,
                                     -3.0408119421278934e-14,-2.2328449483329286e-14,
                                     -1.0693016765542536e-14,-7.240806196368183e-15,
                                     -5.761950647030487e-15,-5.957409950992936e-16,3.2491920424847875e-15,
                                     9.219910025371442e-15,1.2799168101873962e-14,1.7108898977066272e-14,
                                     2.855032832850196e-14,4.1048723593164446e-14,9.99499970701814,
                                     18.470571911228987,30.51830887197781,33.54443547978839,
                                     36.1607309907248,38.57320314001222,40.85924266944485,
                                     43.035391586290125,45.287513370945284,47.151147140980484,
                                     50.73276704593593,52.39952223577173,59.50068014643575,
                                     61.31379159039717,74.51253995296709,77.13729578952395,
                                     94.37203226002364,99.43109411760354,102.6487394625419,
                                     105.06218043709777,107.8179491492106,110.12126119755328,
                                     113.57945167126782,116.37801423876415,122.99832759829658,
                                     125.12209476580418,138.05692785144052,140.21814403606146,
                                     161.31660187081715,164.54676840029904,175.43647569692263,
                                     192.3282320950376,207.53405143733352,212.25537253729775,
                                     284.3628999051823,294.6884623476941,341.98290548531804,
                                     507.44043882416463,534.3382764375148,967.6392677402239,
                                     1875.507871717115,3171.7801027349824,5101.18005605357
                                    };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(eigenvaluesA(i) - arr_values[i]) < pow(10.0, -5.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//===========//
// test # 85 //
//===========//

TEST(MatrixTest85, Heavy85)
{
     MatrixDense<complex<double>> matA;
     MatrixDense<complex<double>> eigenvectorsA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 5) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 5) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvectorsZHEEVX(eigenvectorsA);

     // test

     const double arr_values99[100] = {3.6942314255214093e-22,
                                       1.9280574390556084e-21,
                                       8.336207484683114e-21,
                                       3.201738809930229e-20,
                                       1.128792365433472e-19,
                                       3.7219958073502635e-19,
                                       1.1616570563876596e-18,
                                       3.4603856776191363e-18,
                                       9.898106164891213e-18,
                                       2.7313034552506115e-17,
                                       7.297235551812733e-17,
                                       1.8931798526470812e-16,
                                       4.781008453916819e-16,
                                       1.1776583982769647e-15,
                                       2.8342405979299284e-15,
                                       6.674396729414062e-15,
                                       1.5399303294661802e-14,
                                       3.4849026635393604e-14,
                                       7.743020050094637e-14,
                                       1.6906046576799098e-13,
                                       3.630170569572799e-13,
                                       7.671355827326599e-13,
                                       1.5964469231753074e-12,
                                       3.2735991022182145e-12,
                                       6.617806646334326e-12,
                                       1.3195608156949319e-11,
                                       2.596350061894761e-11,
                                       5.0430333328453936e-11,
                                       9.67336578559975e-11,
                                       1.8330342627363303e-10,
                                       3.4324925188064113e-10,
                                       6.353651498186134e-10,
                                       1.1628708815038037e-9,
                                       2.104965971919157e-9,
                                       3.7693601315726985e-9,
                                       6.6787563519161844e-9,
                                       1.1711672807554324e-8,
                                       2.03292597117336e-8,
                                       3.4936698501777865e-8,
                                       5.9452893015691615e-8,
                                       1.0019897315247485e-7,
                                       1.672693861786821e-7,
                                       2.7662540139461026e-7,
                                       4.5325802559019753e-7,
                                       7.359163073250683e-7,
                                       1.1841012055800893e-6,
                                       1.8883009979218705e-6,
                                       2.984811021145573e-6,
                                       4.676960526639804e-6,
                                       7.265186654807506e-6,
                                       0.000011189174362740128,
                                       0.00001708624363917233,
                                       0.000025871327250800013,
                                       0.00003884524735147213,
                                       0.000057839551835632114,
                                       0.0000854078666854305,
                                       0.00012507547298611522,
                                       0.0001816604867751283,
                                       0.0002616813987570242,
                                       0.00037386653150919884,
                                       0.0005297808051872514,
                                       0.0007445835839285321,
                                       0.0010379277320564163,
                                       0.0014350037062010874,
                                       0.001967722735545796,
                                       0.0026760192519511626,
                                       0.0036092342363378766,
                                       0.00482751783582054,
                                       0.00640315989292051,
                                       0.008421723355192411,
                                       0.010982819682174948,
                                       0.014200329986124536,
                                       0.018201824732492763,
                                       0.023126912983358,
                                       0.029124247016656085,
                                       0.03634692535629019,
                                       0.04494587964943125,
                                       0.055061139884829774,
                                       0.0668110106541383,
                                       0.08027936243852593,
                                       0.09549945152005936,
                                       0.11243731017783884,
                                       0.13097574227719463,
                                       0.15090013051721174,
                                       0.17186879268938443,
                                       0.19341147439162046,
                                       0.21493488140894385,
                                       0.23573601738701488,
                                       0.2548580930562397,
                                       0.27133687410309687,
                                       0.28424352285526244,
                                       0.29273026481733555,
                                       0.29466646943807095,
                                       0.28996990773813897,
                                       0.27878021480447934,
                                       0.26146670556930435,
                                       0.22712948196564564,
                                       0.19198046679049208,
                                       0.15683996268683814,
                                       0.1225409827758915
                                      };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,99)) - abs(arr_values99[i])) < pow(10.0, -10.0));
     }

     // test # 2

     EXPECT_TRUE(res == 0);
}

//===========//
// test # 86 //
//===========//

TEST(MatrixTest86, Heavy86)
{
     MatrixDense<complex<double>> matA;
     MatrixDense<complex<double>> eigenvectorsA;
     const pgg::MAI COLS = 100;
     const pgg::MAI ROWS = COLS;

     matA.Allocate(COLS);
     eigenvectorsA.Allocate(COLS);

     // build matrix

     pgg::MAI i;
     pgg::MAI j;

     for (i = 0; i != ROWS; ++i) {
          for (j = 0; j != COLS; ++j) {
               if (std::abs(static_cast<int>(i-j)) < 20) {
                    matA.SetElement(i, j, static_cast<double>(i+j));
               } else if (std::abs(static_cast<int>(i-j)) >= 20) {
                    matA.SetElement(i, j, static_cast<double>(0.0));
               }
          }
     }

     // diagonalize matrix

     int res;

     res = matA.EigenvectorsZHEEVX(eigenvectorsA);

     // test data # 1

     const double arr_values99[100] = {-0.00002379437463800071,-0.0000321126535355192,
                                       -0.00004261209009056319,-0.00005574228105018768,
                                       -0.0000720258070448845,-0.00009206699311457821,
                                       -0.00011656127426176481,-0.00014630515846035676,
                                       -0.0001822067730093074,-0.00022529697290878234,
                                       -0.00027674098203948964,-0.0003378505293342626,
                                       -0.00041009643284298355,-0.0004951215746125915,
                                       -0.0005947541986465926,-0.0007110214528956958,-0.0008461630842956044,
                                       -0.001002645183353377,-0.0011831738617460526,-0.0013907087329034266,
                                       -0.0016283827619966063,-0.0018997449209395513,-0.002208573606467658,
                                       -0.002558944309118129,-0.0029552087127267114,-0.0034019962501085446,
                                       -0.003904213875493434,-0.00446704386484559,-0.00509593944935361,
                                       -0.005796618082542783,-0.006575052137809326,-0.007437456830885415,
                                       -0.00839027516100404,-0.009440159665541611,-0.010593950785877937,
                                       -0.01185865164733851,-0.013241399063586366,-0.014749430585927434,
                                       -0.0163900474309,-0.018170573135449803,-0.0200983089054282,
                                       -0.022180481856340313,-0.024424190854787288,-0.026834080780647522,
                                       -0.02941440937535356,-0.03216899598488147,-0.035101168178730414,
                                       -0.03821370638490854,-0.04150878671030463,-0.04498792214706611,
                                       -0.04865190239866414,-0.05250073259412269,-0.056533571195309565,
                                       -0.06074866744009344,-0.0651432987033876,-0.06971370819842077,
                                       -0.07445504348175375,-0.07936129626731185,-0.08442524409670342,
                                       -0.08963839445497533,-0.09499093194079421,-0.10047166921101545,
                                       -0.10595789789842808,-0.11143628534678302,-0.11689273862688677,
                                       -0.1223124096438104,-0.12767970465965733,-0.13297829857254914,
                                       -0.13819115429332646,-0.1433005475596976,-0.14828809752292466,
                                       -0.15313480343431787,-0.15782108774752324,-0.16232684593753477,
                                       -0.16663150331824728,-0.17071407911689127,-0.17455325803557586,
                                       -0.17812746949713273,-0.18141497473424503,-0.18439396183722762,
                                       -0.18704264882716626,-0.18541168339207653,-0.18356676120058393,
                                       -0.18150272926411598,-0.17921485423076686,-0.17669886945503793,
                                       -0.17395102341224067,-0.17096812928970723,-0.1677476155625255,
                                       -0.1642875773360043,-0.16058682821057293,-0.15664495239745393,
                                       -0.15246235678535205,-0.14804032262974395,-0.14338105650732136,
                                       -0.13848774014895246,-0.1333645787354236,-0.128016847211483,
                                       -0.1224509341456258,-0.11667438263597216
                                      };

     // test data # 2

     const double arr_values98[100] = {0.0005490815006598427,0.0007217004212979288,0.0009319727820738912,
                                       0.0011856764796110383,0.0014891369301256077,0.0018492353084341568,
                                       0.002273410759398609,0.002769655716932461,0.003346503443557062,
                                       0.00401300689217423,0.0047787079930774546,0.00565359648519078,
                                       0.006648057443072511,0.007772806702325777,0.009038813457661714,
                                       0.010457209401826704,0.012039183891666605,0.013795864771304657,
                                       0.01573818465305239,0.01787673265523282,0.020218129530799774,
                                       0.022773522732384158,0.02555088599547039,0.02855699048006569,
                                       0.03179740706616532,0.03527631554658473,0.038996305859039175,
                                       0.04295817276333242,0.047160705650423344,0.05160047546600458,
                                       0.056271621039461366,0.061165637426456754,0.06627116919601259,
                                       0.07157381191620707,0.0770559254111312,0.08269646266941802,
                                       0.08847081857459133,0.09435070289203276,0.10030404217816305,
                                       0.10629491546542182,0.11228359420772163,0.11822646848984367,
                                       0.12407615562938958,0.12979275139874066,0.13533385216700752,
                                       0.14065467360130016,0.14570820649639804,0.1504454123825699,
                                       0.15481546139101132,0.15876601462676446,0.16224355300490953,
                                       0.1651937541424425,0.16756191846119972,0.16929344514268863,
                                       0.17033435798074814,0.17063188050057534,0.17013505895196374,
                                       0.16879543094112387,0.16656773654130375,0.16341066772151153,
                                       0.1592876487959431,0.15416764632985688,0.14836208495033895,
                                       0.14186022973422263,0.1346552528267162,0.12674464039743252,
                                       0.118130599994188,0.10882046380600134,0.09882708283296263,
                                       0.08816920644738636,0.07687184132594177,0.06496658324539006,
                                       0.052491914775380344,0.039493461481783246,0.026024198885649517,
                                       0.012144602119448178,-0.002077270001960313,-0.0165657649080691,
                                       -0.0312377086295896,-0.04600256891175273,-0.06076271707085829,
                                       -0.06795927022946352,-0.07503806683258157,-0.0819579039270416,
                                       -0.08867572326732134,-0.09514679596043041,-0.1013249415936529,
                                       -0.10716278331612594,-0.1126120400798519,-0.11762385692798413,
                                       -0.12214917384544385,-0.12613913325700105,-0.12954552576940606,
                                       -0.1323212732062947,-0.13442094737761986,-0.13580132236057105,
                                       -0.13642195734879647,-0.13624580635507777,-0.1352398502347542,
                                       -0.13337574564008783
                                      };

     // test data # 3

     const double arr_values97[100] = {-0.0048844047935863765,-0.006236486880389232,-0.007815274325331554,
                                       -0.009640183890712983,-0.011730050533458039,-0.014102771342375649,
                                       -0.016774922205544347,-0.019761352037216788,-0.023074760743478643,
                                       -0.026725268499719908,-0.03071998531275152,-0.03506259120033407,
                                       -0.03975293858755401,-0.04478668963230909,-0.050155002084304526,
                                       -0.05584427788164485,-0.06183598892150303,-0.06810659423095557,
                                       -0.07462756203769172,-0.08136550893055443,-0.08823038013976417,
                                       -0.09520849071940404,-0.1022476308045662,-0.10929156383600795,
                                       -0.11627847844623534,-0.12314131678294159,-0.1298082479646105,
                                       -0.13620329680129123,-0.14224713548726084,-0.14785804289917964,
                                       -0.15295303238959757,-0.1574491445459654,-0.1612648963069822,
                                       -0.16432187213530286,-0.16654643671328404,-0.16787154196597193,
                                       -0.16823859426980445,-0.16759934066305365,-0.16591772596198778,
                                       -0.1631716661722737,-0.15935634408053131,-0.15448321146294608,
                                       -0.14858218139776455,-0.1416381780493364,-0.1336490652093021,
                                       -0.12462715724623677,-0.11460064260169803,-0.10361487512764765,
                                       -0.09173348472751607,-0.07903925562502323,-0.06563471836746396,
                                       -0.051642400613860905,-0.03720468211196078,-0.022483201281379775,
                                       -0.007657764735299888,0.007075282886553215,0.021505263042677962,
                                       0.03540996481701064,0.04855905391412624,0.06071808324036558,
                                       0.07165316959606502,0.08113608035225883,0.09009849834439126,
                                       0.09838663571146652,0.10584187751947657,0.11230295059241002,
                                       0.11760842867321217,0.12159956720711124,0.12412345180969263,
                                       0.1250364341022301,0.12420781716730643,0.12152374055295379,
                                       0.11689120174753254,0.11024213763900717,0.10153747600764697,
                                       0.09077105399613189,0.07797328823931185,0.06321447046527728,
                                       0.04660755350441126,0.028310286413503014,0.008526547885299433,
                                       0.0025719295116293645,-0.004200598749307477,-0.011762440439929252,
                                       -0.020069965854842452,-0.029063538293552106,-0.038666727752430484,
                                       -0.048785756486008615,-0.05930922217827011,-0.07010814475633873,
                                       -0.08103638195706307,-0.09193145642839495,-0.10261583322770457,
                                       -0.11289868090088843,-0.12257814174844638,-0.1314441273038225,
                                       -0.139281643409791,-0.14587463558538188,-0.15101032970590494,
                                       -0.15448402553323257
                                      };

     // test # 1

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,99)) - abs(arr_values99[i])) < pow(10.0, -10.0));
     }

     // test # 2

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,98)) - abs(arr_values98[i])) < pow(10.0, -10.0));
     }

     // test # 3

     for (int i = 0; i != COLS; ++i) {
          EXPECT_TRUE(abs(abs(eigenvectorsA(i,97)) - abs(arr_values97[i])) < pow(10.0, -10.0));
     }

     // test # 4

     EXPECT_TRUE(res == 0);
}

// END
