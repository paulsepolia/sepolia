//=============//
// test driver //
//=============//

// sepolia

#include "../../../head/sepolia.hpp"

using pgg::sep::MatrixDense;
using pgg::sep::VectorDense;

// C++

#include <cmath>
#include <complex>

using std::abs;
using std::cos;
using std::sin;
using std::complex;

// Google Test

#include "gtest/gtest.h"

// common parameters

const auto ROWS = static_cast<int>(pow(10.0, 2.0));
const auto COLS = static_cast<int>(pow(10.0, 2.0));
const auto K_MAX = 200;
const auto K_MAX_LOC = 200;

const double THRES_1 = std::pow(10.0, -1.0);
const double THRES_2 = std::pow(10.0, -2.0);
const double THRES_3 = std::pow(10.0, -3.0);
const double THRES_4 = std::pow(10.0, -4.0);
const double THRES_5 = std::pow(10.0, -5.0);
const double THRES_6 = std::pow(10.0, -6.0);
const double THRES_7 = std::pow(10.0, -7.0);
const double THRES_8 = std::pow(10.0, -8.0);
const double THRES_9 = std::pow(10.0, -9.0);
const double THRES_10 = std::pow(10.0, -10.0);
const double THRES_11 = std::pow(10.0, -11.0);
const double THRES_12 = std::pow(10.0, -12.0);
const double THRES_13 = std::pow(10.0, -13.0);
const double THRES_14 = std::pow(10.0, -14.0);
const double THRES_15 = std::pow(10.0, -15.0);
const double THRES_16 = std::pow(10.0, -16.0);
const double THRES_17 = std::pow(10.0, -17.0);
const double THRES_18 = std::pow(10.0, -18.0);
const double THRES_19 = std::pow(10.0, -19.0);
const double THRES_20 = std::pow(10.0, -20.0);

//==========//
// test # 1 //
//==========//

TEST(VectorTest1, Heavy1)
{
     // main test loop

     for (int k = 0; k != K_MAX; ++k) {

          // 1

          VectorDense<double> vecA;
          VectorDense<double> vecB;

          vecA.Allocate(ROWS*COLS);
          vecB.Allocate(ROWS*COLS);

          EXPECT_EQ(vecA.AllocatedQ(), true);
          EXPECT_EQ(vecB.AllocatedQ(), true);

          // 2

          const double valA = 11.11111;

          vecA.Set(valA);

          EXPECT_EQ(vecA, valA);
          EXPECT_EQ(vecA(1), valA);
          EXPECT_EQ(vecA(2), valA);
          EXPECT_EQ(vecA[10], valA);
          EXPECT_EQ(vecA[20], valA);

          // 3

          vecB.Set(vecA);

          EXPECT_EQ(vecB, valA);
          EXPECT_EQ(vecB(1), valA);
          EXPECT_EQ(vecB(2), valA);
          EXPECT_EQ(vecB[10], valA);
          EXPECT_EQ(vecB[20], valA);
          EXPECT_EQ(vecB, vecA);
          EXPECT_EQ(vecB[10], vecA(2));

          // 4

          const double valB = 12.3456789;

          vecB = valB;
          vecA = vecB;

          EXPECT_EQ(vecA, valB);
          EXPECT_EQ(vecA(1), valB);
          EXPECT_EQ(vecA(2), valB);
          EXPECT_EQ(vecA[10], valB);
          EXPECT_EQ(vecA[20], valB);

          EXPECT_EQ(vecB, valB);
          EXPECT_EQ(vecB(1), valB);
          EXPECT_EQ(vecB(2), valB);
          EXPECT_EQ(vecB[10], valB);
          EXPECT_EQ(vecB[20], valB);
          EXPECT_EQ(vecB, vecB);
          EXPECT_EQ(vecB[10], vecA(2));

          // 5

          const double valC = 10.0;

          vecA = vecA + valC;

          EXPECT_EQ(vecA, valB+valC);
          EXPECT_EQ(vecA(1), valB+valC);
          EXPECT_EQ(vecA(2), valB+valC);
          EXPECT_EQ(vecA[10], valB+valC);
          EXPECT_EQ(vecA[20], valB+valC);

          vecA = vecA - vecA;

          EXPECT_EQ(vecA, pgg::ZERO_DBL);
          EXPECT_EQ(vecA(1), pgg::ZERO_DBL);
          EXPECT_EQ(vecA(2), pgg::ZERO_DBL);
          EXPECT_EQ(vecA[10], pgg::ZERO_DBL);
          EXPECT_EQ(vecA[20], pgg::ZERO_DBL);

          // 6

          const double val4 = 2.2222;
          const double val5 = 3.4567;

          vecA = val4;
          vecA = vecA * val5;

          const double THRES_A = std::pow(10.0, -14.0);
          const double THRES_B = std::pow(10.0, -15.0);
          const double THRES_C = std::pow(10.0, -16.0);
          const double THRES_D = std::pow(10.0, -17.0);
          const double THRES_E = std::pow(10.0, -18.0);
          const double THRES_F = std::pow(10.0, -19.0);
          const double THRES_G = std::pow(10.0, -20.0);

          EXPECT_TRUE(vecA.Equal(val4*val5, THRES_A));
          EXPECT_TRUE(vecA.Equal(val4*val5, THRES_B));
          EXPECT_TRUE(vecA.Equal(val4*val5, THRES_C));
          EXPECT_TRUE(vecA.Equal(val4*val5, THRES_D));
          EXPECT_TRUE(vecA.Equal(val4*val5, THRES_E));
          EXPECT_TRUE(vecA.Equal(val4*val5, THRES_F));
          EXPECT_TRUE(vecA.Equal(val4*val5, THRES_G));

          // 7

          const double val6 = 2.012012;

          vecB = val6;

          vecA = vecA * vecB * vecA;

          EXPECT_TRUE(vecA.Equal(val4*val5*val6*val4*val5, THRES_A));
          EXPECT_TRUE(vecA.Equal(val4*val5*val6*val4*val5, THRES_B));
          EXPECT_TRUE(vecA.Equal(val4*val5*val6*val4*val5, THRES_C));
          EXPECT_TRUE(vecA.Equal(val4*val5*val6*val4*val5, THRES_D));
          EXPECT_TRUE(vecA.Equal(val4*val5*val6*val4*val5, THRES_E));
          EXPECT_TRUE(vecA.Equal(val4*val5*val6*val4*val5, THRES_F));
          EXPECT_TRUE(vecA.Equal(val4*val5*val6*val4*val5, THRES_G));

          // 8

          vecA = val5;
          vecB = val6;
          vecA = vecA/vecB;

          EXPECT_TRUE(vecA.Equal(val5/val6, THRES_A));
          EXPECT_TRUE(vecA.Equal(val5/val6, THRES_B));
          EXPECT_TRUE(vecA.Equal(val5/val6, THRES_C));
          EXPECT_TRUE(vecA.Equal(val5/val6, THRES_D));
          EXPECT_TRUE(vecA.Equal(val5/val6, THRES_E));
          EXPECT_TRUE(vecA.Equal(val5/val6, THRES_F));
          EXPECT_TRUE(vecA.Equal(val5/val6, THRES_G));

          // 9

          vecA = val5;
          vecB = val6;
          vecA = vecA*vecB+vecA-vecB/vecA;

          EXPECT_TRUE(vecA.Equal(val5*val6+val5-val6/val5, THRES_A));
          EXPECT_TRUE(vecA.Equal(val5*val6+val5-val6/val5, THRES_B));
          EXPECT_TRUE(vecA.Equal(val5*val6+val5-val6/val5, THRES_C));
          EXPECT_TRUE(vecA.Equal(val5*val6+val5-val6/val5, THRES_D));
          EXPECT_TRUE(vecA.Equal(val5*val6+val5-val6/val5, THRES_E));
          EXPECT_TRUE(vecA.Equal(val5*val6+val5-val6/val5, THRES_F));
          EXPECT_TRUE(vecA.Equal(val5*val6+val5-val6/val5, THRES_G));

          // 10

          vecA--;

          EXPECT_TRUE(vecA.Equal(val5*val6+val5-val6/val5-1.0, THRES_A));
          EXPECT_TRUE(vecA.Equal(val5*val6+val5-val6/val5-1.0, THRES_B));
          EXPECT_TRUE(vecA.Equal(val5*val6+val5-val6/val5-1.0, THRES_C));
          EXPECT_TRUE(vecA.Equal(val5*val6+val5-val6/val5-1.0, THRES_D));
          EXPECT_TRUE(vecA.Equal(val5*val6+val5-val6/val5-1.0, THRES_E));
          EXPECT_TRUE(vecA.Equal(val5*val6+val5-val6/val5-1.0, THRES_F));
          EXPECT_TRUE(vecA.Equal(val5*val6+val5-val6/val5-1.0, THRES_G));

          // 11

          --vecB;

          EXPECT_TRUE(vecB.Equal(val6-1.0, THRES_A));
          EXPECT_TRUE(vecB.Equal(val6-1.0, THRES_B));
          EXPECT_TRUE(vecB.Equal(val6-1.0, THRES_C));
          EXPECT_TRUE(vecB.Equal(val6-1.0, THRES_D));
          EXPECT_TRUE(vecB.Equal(val6-1.0, THRES_E));
          EXPECT_TRUE(vecB.Equal(val6-1.0, THRES_F));
          EXPECT_TRUE(vecB.Equal(val6-1.0, THRES_G));

          // 12

          vecA = 0;
          vecB = 0;

          vecA += 10;
          vecB -= 11;


          EXPECT_EQ(vecA(10), 10);
          EXPECT_EQ(vecB(10), -11);

          // 13

          vecA *= 10;
          vecB /= 11;

          EXPECT_EQ(vecA(10),10*10);
          EXPECT_EQ(vecB(1), -1);

          // 14

          vecA = val4;
          vecB = val5;

          vecA.Plus(vecA, vecA);
          vecB.Plus(vecB, vecB);

          EXPECT_EQ(vecA, 2*val4);
          EXPECT_EQ(vecB, 2*val5);

          EXPECT_TRUE(vecA.Equal(2*val4, THRES_A));
          EXPECT_TRUE(vecB.Equal(2*val5, THRES_A));

          EXPECT_TRUE(vecA.Equal(2*val4, THRES_E));
          EXPECT_TRUE(vecB.Equal(2*val5, THRES_E));

          EXPECT_TRUE(vecA.Equal(2*val4, THRES_F));
          EXPECT_TRUE(vecB.Equal(2*val5, THRES_F));

          EXPECT_TRUE(vecA.Equal(2*val4, THRES_G));
          EXPECT_TRUE(vecB.Equal(2*val5, THRES_G));

          // 15

          vecA.Subtract(vecA, vecA);
          vecB.Subtract(vecB, vecB);

          EXPECT_EQ(vecA, 0.0);
          EXPECT_EQ(vecB, 0.0);

          EXPECT_TRUE(vecA.Equal(0.0, THRES_G));
          EXPECT_TRUE(vecB.Equal(0.0, THRES_G));

          // 16

          vecA = valB;
          vecB = valA;

          vecA.Divide(vecA, vecA);
          vecB.Divide(vecB, vecB);

          EXPECT_EQ(vecA, 1.0);
          EXPECT_EQ(vecB, 1.0);

          EXPECT_TRUE(vecA.Equal(1.0, THRES_G));
          EXPECT_TRUE(vecB.Equal(1.0, THRES_G));

          // 17

          vecA = val4;
          vecB = val5;

          vecA.Times(vecA, vecA);
          vecB.Times(vecB, vecB);

          EXPECT_EQ(vecA, val4*val4);
          EXPECT_EQ(vecB, val5*val5);

          EXPECT_TRUE(vecA.Equal(val4*val4, THRES_G));
          EXPECT_TRUE(vecB.Equal(val5*val5, THRES_G));

          // 18

          vecA.Deallocate();
          vecB.Deallocate();

          EXPECT_TRUE(vecA.DeallocatedQ());
          EXPECT_TRUE(vecB.DeallocatedQ());
     }
}

//==========//
// test # 2 //
//==========//

TEST(VectorTest2, Heavy2)
{
     // main test loop

     for (int k = 0; k != K_MAX; ++k) {

          VectorDense<double> vecA;
          VectorDense<double> vecB;

          const int the_seed = 10;
          const double s1 = 0.0;
          const double s2 = 1.0;

          vecA.Allocate(ROWS*COLS);
          vecB.Allocate(ROWS*COLS);

          vecA.Random(s1, s2, the_seed);
          vecB.Random(s1, s2, the_seed);

          // 1

          EXPECT_EQ(vecA, vecB);
          EXPECT_TRUE(vecA == vecB);

          // 2

          vecA.Sort();
          vecB.Sort();

          EXPECT_TRUE(vecA.SortedQ());
          EXPECT_TRUE(vecB.SortedQ());

          // 3

          EXPECT_TRUE(vecA==vecB);

     }
}

//==========//
// test # 3 //
//==========//

TEST(VectorTest3, Heavy3)
{
     // main test loop

     for (int k = 0; k != K_MAX; ++k) {

          VectorDense<double> vecA;
          VectorDense<double> vecB;

          vecA.Allocate(ROWS*COLS);
          vecB.Allocate(ROWS*COLS);

          // 1

          vecA = 0.1;
          vecB = 0.2;

          vecA.Map(sin);
          vecB.Map(cos);

          EXPECT_EQ(vecA, sin(0.1));
          EXPECT_EQ(vecB, cos(0.2));

          // 2

          vecA = 0.1;
          vecB = 0.2;

          vecA.Nest(sin, 50);
          vecB.Nest(cos, 50);

          double valA = 0.1;
          double valB = 0.2;

          for (int ik = 0; ik != 50; ++ik) {
               valA = sin(valA);
               valB = cos(valB);
          }

          EXPECT_TRUE(vecA.Equal(valA, THRES_20));
          EXPECT_TRUE(vecA.Equal(valA, THRES_20));
          EXPECT_TRUE(vecA.Equal(valA, THRES_19));
          EXPECT_TRUE(vecA.Equal(valA, THRES_19));
          EXPECT_TRUE(vecA.Equal(valA, THRES_18));
          EXPECT_TRUE(vecB.Equal(valB, THRES_18));
          EXPECT_TRUE(vecA.Equal(valA, THRES_17));
          EXPECT_TRUE(vecB.Equal(valB, THRES_17));
          EXPECT_TRUE(vecA.Equal(valA, THRES_16));
          EXPECT_TRUE(vecB.Equal(valB, THRES_16));
          EXPECT_TRUE(vecA.Equal(valA, THRES_15));
          EXPECT_TRUE(vecB.Equal(valB, THRES_15));
          EXPECT_TRUE(vecA.Equal(valA, THRES_10));
          EXPECT_TRUE(vecB.Equal(valB, THRES_10));

          // 3

          vecA.Nest(asin, 50);
          vecB.Nest(acos, 50);

          for (int ik = 0; ik != 50; ++ik) {
               valA = asin(valA);
               valB = acos(valB);
          }

          EXPECT_TRUE(vecA.Equal(valA, THRES_20));
          EXPECT_TRUE(vecA.Equal(valA, THRES_20));
          EXPECT_TRUE(vecA.Equal(valA, THRES_19));
          EXPECT_TRUE(vecA.Equal(valA, THRES_19));
          EXPECT_TRUE(vecA.Equal(valA, THRES_18));
          EXPECT_TRUE(vecB.Equal(valB, THRES_18));
          EXPECT_TRUE(vecA.Equal(valA, THRES_17));
          EXPECT_TRUE(vecB.Equal(valB, THRES_17));
          EXPECT_TRUE(vecA.Equal(valA, THRES_16));
          EXPECT_TRUE(vecB.Equal(valB, THRES_16));
          EXPECT_TRUE(vecA.Equal(valA, THRES_15));
          EXPECT_TRUE(vecB.Equal(valB, THRES_15));
          EXPECT_TRUE(vecA.Equal(valA, THRES_10));
          EXPECT_TRUE(vecB.Equal(valB, THRES_10));
     }
}

//==========//
// test # 4 //
//==========//

TEST(VectorTest4, Heavy4)
{
     // main test loop

     for (int k = 0; k != K_MAX; ++k) {

          VectorDense<double> vecA;
          VectorDense<double> vecB;

          vecA.Allocate(ROWS*COLS);
          vecB.Allocate(ROWS*COLS);

          // 1

          vecA = 0.1;
          vecB = 0.2;

          vecA.Map(sin);
          vecB.Map(cos);

          EXPECT_EQ(vecA, pgg::functions::Sin<double>(0.1));
          EXPECT_EQ(vecB, pgg::functions::Cos<double>(0.2));

          // 2

          vecA = 0.1;
          vecB = 0.2;

          vecA.Nest(pgg::functions::Sin<double>, 50);
          vecB.Nest(pgg::functions::Cos<double>, 50);

          double valA = 0.1;
          double valB = 0.2;

          for (int ik = 0; ik != 50; ++ik) {
               valA = pgg::functions::Sin<double>(valA);
               valB = pgg::functions::Cos<double>(valB);
          }

          EXPECT_TRUE(vecA.Equal(valA, THRES_20));
          EXPECT_TRUE(vecA.Equal(valA, THRES_20));
          EXPECT_TRUE(vecA.Equal(valA, THRES_19));
          EXPECT_TRUE(vecA.Equal(valA, THRES_19));
          EXPECT_TRUE(vecA.Equal(valA, THRES_18));
          EXPECT_TRUE(vecB.Equal(valB, THRES_18));
          EXPECT_TRUE(vecA.Equal(valA, THRES_17));
          EXPECT_TRUE(vecB.Equal(valB, THRES_17));
          EXPECT_TRUE(vecA.Equal(valA, THRES_16));
          EXPECT_TRUE(vecB.Equal(valB, THRES_16));
          EXPECT_TRUE(vecA.Equal(valA, THRES_15));
          EXPECT_TRUE(vecB.Equal(valB, THRES_15));
          EXPECT_TRUE(vecA.Equal(valA, THRES_10));
          EXPECT_TRUE(vecB.Equal(valB, THRES_10));

          // 3

          vecA.Nest(pgg::functions::ArcSin<double>, 50);
          vecB.Nest(pgg::functions::ArcCos<double>, 50);

          for (int ik = 0; ik != 50; ++ik) {
               valA = pgg::functions::ArcSin<double>(valA);
               valB = pgg::functions::ArcCos<double>(valB);
          }

          EXPECT_TRUE(vecA.Equal(valA, THRES_20));
          EXPECT_TRUE(vecA.Equal(valA, THRES_20));
          EXPECT_TRUE(vecA.Equal(valA, THRES_19));
          EXPECT_TRUE(vecA.Equal(valA, THRES_19));
          EXPECT_TRUE(vecA.Equal(valA, THRES_18));
          EXPECT_TRUE(vecB.Equal(valB, THRES_18));
          EXPECT_TRUE(vecA.Equal(valA, THRES_17));
          EXPECT_TRUE(vecB.Equal(valB, THRES_17));
          EXPECT_TRUE(vecA.Equal(valA, THRES_16));
          EXPECT_TRUE(vecB.Equal(valB, THRES_16));
          EXPECT_TRUE(vecA.Equal(valA, THRES_15));
          EXPECT_TRUE(vecB.Equal(valB, THRES_15));
          EXPECT_TRUE(vecA.Equal(valA, THRES_10));
          EXPECT_TRUE(vecB.Equal(valB, THRES_10));
     }
}

//==========//
// test # 5 //
//==========//

TEST(VectorTest5, Heavy5)
{
     // main test loop

     for (int k = 0; k != K_MAX; ++k) {

          VectorDense<double> vecA;
          VectorDense<double> vecB;

          vecA.Allocate(ROWS*COLS);
          vecB.Allocate(ROWS*COLS);

          // 1

          vecA = 0.1;
          vecB = 0.2;

          pgg::functors::Sin<double> sin_d;
          pgg::functors::Cos<double> cos_d;

          vecA.Map(sin_d);
          vecB.Map(cos_d);

          EXPECT_EQ(vecA, sin_d(0.1));
          EXPECT_TRUE(vecB.Equal(cos_d(0.2), THRES_1));
          EXPECT_TRUE(vecB.Equal(cos_d(0.2), THRES_2));
          EXPECT_TRUE(vecB.Equal(cos_d(0.2), THRES_3));
          EXPECT_TRUE(vecB.Equal(cos_d(0.2), THRES_4));
          EXPECT_TRUE(vecB.Equal(cos_d(0.2), THRES_5));
          EXPECT_TRUE(vecB.Equal(cos_d(0.2), THRES_6));
          EXPECT_TRUE(vecB.Equal(cos_d(0.2), THRES_7));
          EXPECT_TRUE(vecB.Equal(cos_d(0.2), THRES_8));
          EXPECT_TRUE(vecB.Equal(cos_d(0.2), THRES_9));
          EXPECT_TRUE(vecB.Equal(cos_d(0.2), THRES_10));
          EXPECT_TRUE(vecB.Equal(cos_d(0.2), THRES_11));
          EXPECT_TRUE(vecB.Equal(cos_d(0.2), THRES_12));
          EXPECT_TRUE(vecB.Equal(cos_d(0.2), THRES_13));
          EXPECT_TRUE(vecB.Equal(cos_d(0.2), THRES_14));
          EXPECT_TRUE(vecB.Equal(cos_d(0.2), THRES_15));

          // 2

          vecA = 0.1;
          vecB = 0.2;

          vecA.Nest(sin_d, 50);
          vecB.Nest(cos_d, 50);

          double valA = 0.1;
          double valB = 0.2;

          for (int ik = 0; ik != 50; ++ik) {
               valA = sin_d(valA);
               valB = cos_d(valB);
          }

          EXPECT_TRUE(vecA.Equal(valA, THRES_20));
          EXPECT_TRUE(vecB.Equal(valB, THRES_20));
          EXPECT_TRUE(vecA.Equal(valA, THRES_19));
          EXPECT_TRUE(vecB.Equal(valB, THRES_19));
          EXPECT_TRUE(vecA.Equal(valA, THRES_18));
          EXPECT_TRUE(vecB.Equal(valB, THRES_18));
          EXPECT_TRUE(vecA.Equal(valA, THRES_17));
          EXPECT_TRUE(vecB.Equal(valB, THRES_17));
          EXPECT_TRUE(vecA.Equal(valA, THRES_16));
          EXPECT_TRUE(vecB.Equal(valB, THRES_16));
          EXPECT_TRUE(vecA.Equal(valA, THRES_15));
          EXPECT_TRUE(vecB.Equal(valB, THRES_15));
          EXPECT_TRUE(vecA.Equal(valA, THRES_10));
          EXPECT_TRUE(vecB.Equal(valB, THRES_10));

          // 3

          pgg::functors::ArcSin<double> asin_d;
          pgg::functors::ArcCos<double> acos_d;

          vecA.Nest(asin_d, 50);
          vecB.Nest(acos_d, 50);

          for (int ik = 0; ik != 50; ++ik) {
               valA = asin_d(valA);
               valB = acos_d(valB);
          }

          EXPECT_TRUE(vecA.Equal(valA, THRES_20));
          EXPECT_TRUE(vecB.Equal(valB, THRES_20));
          EXPECT_TRUE(vecA.Equal(valA, THRES_19));
          EXPECT_TRUE(vecB.Equal(valB, THRES_19));
          EXPECT_TRUE(vecA.Equal(valA, THRES_18));
          EXPECT_TRUE(vecB.Equal(valB, THRES_18));
          EXPECT_TRUE(vecA.Equal(valA, THRES_17));
          EXPECT_TRUE(vecB.Equal(valB, THRES_17));
          EXPECT_TRUE(vecA.Equal(valA, THRES_16));
          EXPECT_TRUE(vecB.Equal(valB, THRES_16));
          EXPECT_TRUE(vecA.Equal(valA, THRES_15));
          EXPECT_TRUE(vecB.Equal(valB, THRES_15));
          EXPECT_TRUE(vecA.Equal(valA, THRES_10));
          EXPECT_TRUE(vecB.Equal(valB, THRES_10));
     }
}

//==========//
// test # 6 //
//==========//

TEST(VectorTest6, Heavy6)
{
     // main test loop

     for (int k = 0; k != K_MAX; ++k) {

          VectorDense<double> vecA;
          VectorDense<double> vecB;

          vecA.Allocate(ROWS*COLS);
          vecB.Allocate(ROWS*COLS);

          // 1

          vecA.Random(0.0, 8.0, 10);
          vecB.Random(0.0, 8.0, 10);

          EXPECT_EQ(vecA.Min(), vecB.Min());
          EXPECT_EQ(vecA.Max(), vecB.Max());

          // 2

          vecA = vecA + vecB;

          EXPECT_TRUE(vecA.Equal(2.0*vecB, THRES_1));

          pgg::functions::SeedRandomGenerator(10);

          vecA.SetElement(1, pgg::functions::RandomNumber());

          pgg::functions::SeedRandomGenerator(10);

          vecA.SetElement(2, pgg::functions::RandomNumber());

          EXPECT_EQ(vecA(1), vecA(2));

          // 3

          vecA = 10.0;

          EXPECT_EQ(vecA.Count(10.0), ROWS*COLS);

          vecA = 11.1;

          EXPECT_EQ(vecA.Count(11.1), ROWS*COLS);

          vecA = 12.3456;

          EXPECT_EQ(vecA.Count(12.3456), ROWS*COLS);

          // 4

          vecA.Range(10.0);
          vecB.Range(10.0);

          vecA.RotateRight(1);

          EXPECT_EQ(vecA(0), vecB(ROWS*COLS-1));
          EXPECT_EQ(vecA(1), vecB(0));
          EXPECT_EQ(vecA(2), vecB(1));
          EXPECT_EQ(vecA(ROWS*COLS-1), vecB(ROWS*COLS-2));

          // 5

          vecA.RotateLeft(1);

          EXPECT_EQ(vecA, vecB);
          EXPECT_TRUE(vecA==vecB);
     }
}

//==========//
// test # 7 //
//==========//

TEST(VectorTest7, Heavy7)
{
     pgg::functors::Cos<double> dCos;
     pgg::functors::Cos<long double> dlCos;

     // main test loop

     for (int k = 0; k != K_MAX; ++k) {

          VectorDense<double> vecA;

          vecA.Allocate(ROWS*COLS);

          // 1

          vecA.Set(10.0);
          vecA.Cos();

          EXPECT_TRUE(vecA.Equal(dlCos(10.0), THRES_15));
          EXPECT_TRUE(vecA.Equal(dCos(10.0), THRES_15));
          EXPECT_TRUE(vecA.Equal(cos(10.0), THRES_15));
          EXPECT_TRUE(vecA.Equal(cos(10.0L), THRES_15));

          EXPECT_TRUE(vecA.Equal(dlCos(10.0), THRES_16));
          EXPECT_TRUE(vecA.Equal(dCos(10.0), THRES_16));
          EXPECT_TRUE(vecA.Equal(cos(10.0), THRES_16));
          EXPECT_TRUE(vecA.Equal(cos(10.0L), THRES_16));

          EXPECT_TRUE(vecA.Equal(dlCos(10.0), THRES_17));
          EXPECT_TRUE(vecA.Equal(dCos(10.0), THRES_17));
          EXPECT_TRUE(vecA.Equal(cos(10.0), THRES_17));
          EXPECT_TRUE(vecA.Equal(cos(10.0L), THRES_17));

          vecA.Deallocate();
     }
}

//==========//
// test # 8 //
//==========//

TEST(VectorTest8, Heavy8)
{
     // main test loop

     for (int k = 0; k != K_MAX; ++k) {

          VectorDense<double> vecA;
          VectorDense<double> vecB;
          VectorDense<double> vecC;
          VectorDense<double> vecD;

          vecA.Allocate(ROWS*COLS);
          vecB.Allocate(ROWS*COLS);
          vecC.Allocate(ROWS*COLS);
          vecD.Allocate(ROWS*COLS);

          vecA = 10.0;
          vecB = 20.0;
          vecC = 10.0L;
          vecD = 20.0L;

          for (int i = 1; i != 10; ++i) {
               vecA.FastMA(vecB, vecB, vecB);
               vecC.FastMA(vecD, vecD, vecD);
          }

          // test

          EXPECT_TRUE(vecA.Equal(vecC, THRES_1));
          EXPECT_TRUE(vecA.Equal(vecC, THRES_2));
          EXPECT_TRUE(vecA.Equal(vecC, THRES_3));
          EXPECT_TRUE(vecA.Equal(vecC, THRES_4));
          EXPECT_TRUE(vecA.Equal(vecC, THRES_5));
          EXPECT_TRUE(vecA.Equal(vecC, THRES_6));
          EXPECT_TRUE(vecA.Equal(vecC, THRES_7));
          EXPECT_TRUE(vecA.Equal(vecC, THRES_8));
          EXPECT_TRUE(vecA.Equal(vecC, THRES_9));
          EXPECT_TRUE(vecA.Equal(vecC, THRES_10));
          EXPECT_TRUE(vecA.Equal(vecC, THRES_11));
          EXPECT_TRUE(vecA.Equal(vecC, THRES_12));
          EXPECT_TRUE(vecA.Equal(vecC, THRES_13));
          EXPECT_TRUE(vecA.Equal(vecC, THRES_14));
          EXPECT_TRUE(vecA.Equal(vecC, THRES_15));
          EXPECT_TRUE(vecA.Equal(vecC, THRES_16));
          EXPECT_TRUE(vecA.Equal(vecC, THRES_17));
          EXPECT_TRUE(vecA.Equal(vecC, THRES_18));
          EXPECT_TRUE(vecA.Equal(vecC, THRES_19));

          // deallocations

          vecA.Deallocate();
          vecB.Deallocate();
          vecC.Deallocate();
          vecD.Deallocate();
     }
}

//==========//
// test # 9 //
//==========//

TEST(VectorTest9, Heavy9)
{
     // main test loop

     for (int k = 0; k != K_MAX; ++k) {

          VectorDense<double> vecA;
          VectorDense<double> vecB;
          VectorDense<double> vecC;
          VectorDense<double> vecD;

          vecA.Allocate(ROWS);
          vecB.Allocate(ROWS);
          vecC.Allocate(ROWS);
          vecD.Allocate(ROWS);

          vecA.Random(0.0, 1.0, 10);
          vecB.Random(0.0, 1.0, 20);
          vecC.Random(0.0, 1.0, 10);
          vecD.Random(0.0, 1.0, 20);

          // 1

          for (int ik = 0; ik != 10; ++ik) {
               vecA.Swap(vecB);
          }

          EXPECT_EQ(vecA, vecC);
          EXPECT_EQ(vecB, vecD);

          // 2

          for (int ik = 0; ik != 11; ++ik) {
               vecA.Swap(vecB);
          }

          EXPECT_EQ(vecA, vecD);
          EXPECT_EQ(vecB, vecC);

     }
}

//===========//
// test # 10 //
//===========//

TEST(VectorTest10, Heavy10)
{
     // main test loop

     for (int k = 0; k != K_MAX; ++k) {

          VectorDense<double> vecA;
          VectorDense<double> vecB;
          VectorDense<double> vecC;
          VectorDense<double> vecD;

          vecA.Allocate(ROWS);
          vecB.Allocate(ROWS);
          vecC.Allocate(ROWS);
          vecD.Allocate(ROWS);

          vecA.Random(0.0, 1.0, 10);
          vecB.Random(0.0, 1.0, 20);
          vecC.Random(0.0, 1.0, 10);
          vecD.Random(0.0, 1.0, 20);

          // 1

          for (int ik = 0; ik != 10; ++ik) {
               vecA.Swap(vecB);
          }

          EXPECT_EQ(vecA, vecC);
          EXPECT_EQ(vecB, vecD);

          // 2

          for (int ik = 0; ik != 11; ++ik) {
               vecA.Swap(vecB);
          }

          EXPECT_EQ(vecA, vecD);
          EXPECT_EQ(vecB, vecC);
     }
}

//===========//
// test # 11 //
//===========//

TEST(VectorTest11, Heavy11)
{
     // main test loop

     for (int k = 0; k != K_MAX; ++k) {

          VectorDense<std::complex<double>> vecA;
          VectorDense<std::complex<double>> vecB;
          VectorDense<std::complex<double>> vecC;

          vecA.Allocate(ROWS);
          vecB.Allocate(ROWS);
          vecC.Allocate(ROWS);

          vecA = {1.0, 2.0};
          vecB = {2.0, 3.0};
          vecC = {0.0, 0.0};

          // 1

          vecC = vecA + vecB;

          EXPECT_TRUE(vecC == vecA+vecB);

          // 2

          vecC = vecA - vecB;

          EXPECT_TRUE(vecC == vecA - vecB);

          // 3

          vecC = vecA * vecB;

          EXPECT_TRUE(vecC == vecA * vecB);

          // 4

          vecC = vecA / vecB;

          EXPECT_TRUE(vecC == vecA / vecB);

          // 5

          vecC = vecA + 10.0;

          EXPECT_EQ(vecC, std::complex<double>( {11.0, 2.0}));

          // 6

          vecC = vecA + std::complex<double> {10.0, 11.0};

          EXPECT_EQ(vecC, std::complex<double>( {11.0, 13.0}));

          // 7

          vecC = vecA - 10.0;

          EXPECT_TRUE(vecC == std::complex<double>( {-9.0, 2.0}));

          // 8

          vecC = vecA * std::complex<double> {10.0, 11.0};

          EXPECT_TRUE(vecC == std::complex<double>( {-12.0, 31.0}));

          // 9

          vecA.Deallocate();
          vecB.Deallocate();
          vecC.Deallocate();
     }
}

//===========//
// test # 12 //
//===========//

TEST(VectorTest12, Heavy12)
{
     const std::complex<double> ONE_ONE_CMPLX = {1.0, 1.0};
     const std::complex<double> TWO_TWO_CMPLX = {2.0, 2.0};
     const std::complex<double> THREE_THREE_CMPLX = {3.0, 3.0};
     const double ZERO_DBL = 0.0;
     const double ONE_DBL = 1.0;
     const double TWO_DBL = 2.0;
     const double THREE_DBL = 3.0;
     const std::complex<double> ZERO_ONE_CMPLX = {0.0, 1.0};
     const std::complex<double> ZERO_TWO_CMPLX = {0.0, 2.0};
     const std::complex<double> ZERO_THREE_CMPLX = {0.0, 3.0};
     const std::complex<double> ONE_ZERO_CMPLX = {1.0, 0.0};
     const std::complex<double> TWO_ZERO_CMPLX = {2.0, 0.0};
     const std::complex<double> THREE_ZERO_CMPLX = {3.0, 0.0};

     // main test loop

     for (int k = 0; k != K_MAX; ++k) {

          VectorDense<std::complex<double>> vecA;
          VectorDense<std::complex<double>> vecB;
          VectorDense<std::complex<double>> vecC;

          // allocate heap space

          vecA.Allocate(ROWS);
          vecB.Allocate(ROWS);
          vecC.Allocate(ROWS);

          // 1

          for (int i = 0; i != 10; ++i) {
               vecA.Set(ONE_ONE_CMPLX);
               vecB.Set(TWO_TWO_CMPLX);
               vecC.Set(THREE_THREE_CMPLX);
          }

          EXPECT_TRUE(vecA.Equal(ONE_ONE_CMPLX, ZERO_DBL));
          EXPECT_TRUE(vecB.Equal(TWO_TWO_CMPLX, ZERO_DBL));
          EXPECT_TRUE(vecC.Equal(THREE_THREE_CMPLX, ZERO_DBL));

          EXPECT_TRUE(vecA.Equal(ONE_ONE_CMPLX));
          EXPECT_TRUE(vecB.Equal(TWO_TWO_CMPLX));
          EXPECT_TRUE(vecC.Equal(THREE_THREE_CMPLX));

          EXPECT_TRUE(vecA.GreaterEqual(ONE_ONE_CMPLX));
          EXPECT_TRUE(vecB.GreaterEqual(TWO_TWO_CMPLX));
          EXPECT_TRUE(vecC.GreaterEqual(THREE_THREE_CMPLX));

          EXPECT_TRUE(vecA.LessEqual(ONE_ONE_CMPLX));
          EXPECT_TRUE(vecB.LessEqual(TWO_TWO_CMPLX));
          EXPECT_TRUE(vecC.LessEqual(THREE_THREE_CMPLX));

          EXPECT_TRUE(vecA.Greater(ZERO_DBL)) ;
          EXPECT_TRUE(vecB.Greater(ONE_ONE_CMPLX)) ;
          EXPECT_TRUE(vecC.Greater(TWO_TWO_CMPLX)) ;

          EXPECT_TRUE(vecA.Less(TWO_TWO_CMPLX));
          EXPECT_TRUE(vecB.Less(THREE_THREE_CMPLX));
          EXPECT_TRUE(vecC.Less( {4.0,5.0}));

          // 2

          for (int i = 0; i != K_MAX_LOC; ++i) {
               vecA.Set(vecA);
               vecB.Set(vecA);
               vecC.Set(vecA);
          }

          EXPECT_TRUE(vecA.Equal(ONE_ONE_CMPLX, ZERO_DBL));
          EXPECT_TRUE(vecB.Equal(ONE_ONE_CMPLX, ZERO_DBL));
          EXPECT_TRUE(vecC.Equal(ONE_ONE_CMPLX, ZERO_DBL));

          EXPECT_TRUE(vecA.Equal(ONE_ONE_CMPLX));
          EXPECT_TRUE(vecB.Equal(ONE_ONE_CMPLX));
          EXPECT_TRUE(vecC.Equal(ONE_ONE_CMPLX));

          EXPECT_TRUE(vecA.GreaterEqual(vecA));
          EXPECT_TRUE(vecB.GreaterEqual(vecA));
          EXPECT_TRUE(vecC.GreaterEqual(vecA));

          EXPECT_TRUE(vecA.LessEqual(vecA));
          EXPECT_TRUE(vecB.LessEqual(vecA));
          EXPECT_TRUE(vecC.LessEqual(vecA));

          EXPECT_TRUE(vecA.Greater(ZERO_DBL));
          EXPECT_TRUE(vecB.Greater(ZERO_DBL));
          EXPECT_TRUE(vecC.Greater(ZERO_DBL));

          EXPECT_TRUE(vecA.Less(TWO_TWO_CMPLX));
          EXPECT_TRUE(vecB.Less(THREE_THREE_CMPLX));
          EXPECT_TRUE(vecC.Less( {4.0,5.0}));

          // 3

          for (int i = 0; i != K_MAX_LOC; ++i) {
               vecA.Set(ZERO_DBL);
               vecB.Set(ONE_DBL);
               vecC.Set(TWO_DBL);
          }

          EXPECT_TRUE(vecA.Equal(ZERO_DBL, ZERO_DBL));
          EXPECT_TRUE(vecB.Equal(ONE_DBL, ZERO_DBL));
          EXPECT_TRUE(vecC.Equal(TWO_DBL, ZERO_DBL));

          EXPECT_TRUE(vecA.Equal(ZERO_DBL));
          EXPECT_TRUE(vecB.Equal(ONE_DBL));
          EXPECT_TRUE(vecC.Equal(TWO_DBL));

          EXPECT_TRUE(vecA.GreaterEqual(ZERO_DBL));
          EXPECT_TRUE(vecB.GreaterEqual(ONE_DBL));
          EXPECT_TRUE(vecC.GreaterEqual(TWO_DBL));

          EXPECT_TRUE(vecA.LessEqual(ZERO_DBL));
          EXPECT_TRUE(vecB.LessEqual(ONE_DBL));
          EXPECT_TRUE(vecC.LessEqual(TWO_DBL));

          EXPECT_TRUE(!vecA.Greater(ZERO_DBL));
          EXPECT_TRUE(vecB.GreaterEqual(ZERO_DBL));
          EXPECT_TRUE(vecC.GreaterEqual(ONE_DBL));

          EXPECT_TRUE(vecA.Less(TWO_TWO_CMPLX));
          EXPECT_TRUE(vecB.Less(THREE_THREE_CMPLX));
          EXPECT_TRUE(vecC.Less( {4.0,5.0}));

          // 4

          for (int i = 0; i != K_MAX_LOC; ++i) {
               vecA.Set(ZERO_ONE_CMPLX);
               vecB.Set(ZERO_TWO_CMPLX);
               vecC.Set(ZERO_THREE_CMPLX);
          }

          EXPECT_TRUE(vecA.Equal(ZERO_ONE_CMPLX, ZERO_DBL));
          EXPECT_TRUE(vecB.Equal(ZERO_TWO_CMPLX, ZERO_DBL));
          EXPECT_TRUE(vecC.Equal(ZERO_THREE_CMPLX, ZERO_DBL));

          EXPECT_TRUE(vecA.Equal(ZERO_ONE_CMPLX));
          EXPECT_TRUE(vecB.Equal(ZERO_TWO_CMPLX));
          EXPECT_TRUE(vecC.Equal(ZERO_THREE_CMPLX));

          // 5

          for (int i = 0; i != K_MAX_LOC; ++i) {
               vecA.Set(ONE_ZERO_CMPLX);
               vecB.Set(TWO_ZERO_CMPLX);
               vecC.Set(THREE_ZERO_CMPLX);
          }

          EXPECT_TRUE(vecA.Equal(ONE_DBL, ZERO_DBL));
          EXPECT_TRUE(vecB.Equal(TWO_DBL, ZERO_DBL));
          EXPECT_TRUE(vecC.Equal(THREE_DBL, ZERO_DBL));

          EXPECT_TRUE(vecA.Equal(ONE_DBL));
          EXPECT_TRUE(vecB.Equal(TWO_DBL));
          EXPECT_TRUE(vecC.Equal(THREE_DBL));

          vecA.SetElement(1, {10.0, 10.0});
          vecB.SetElement(1, {10.0, 10.0});
          vecC.SetElement(1, {10.0, 10.0});

          EXPECT_TRUE(!vecA.Equal(ONE_DBL));
          EXPECT_TRUE(!vecB.Equal(TWO_DBL));
          EXPECT_TRUE(!vecC.Equal(THREE_DBL));

          EXPECT_TRUE(vecA.NotEqual(ONE_DBL));
          EXPECT_TRUE(vecB.NotEqual(TWO_DBL));
          EXPECT_TRUE(vecC.NotEqual(THREE_DBL));

          // deallocate

          vecA.Deallocate();
          vecB.Deallocate();
          vecC.Deallocate();
     }
}

//===========//
// test # 13 //
//===========//

TEST(VectorTest13, RealImaginaryPart1)
{
     VectorDense<complex<double>> vecA;
     VectorDense<double> vecB;
     VectorDense<double> vecC;
     const int K_MAX = 10;

     // allocations

     const pgg::VEI ELEMS = 5*static_cast<pgg::VEI>(pow(10.0, 7.0));

     vecA.Allocate(ELEMS);
     vecB.Allocate(ELEMS);
     vecC.Allocate(ELEMS);

     vecA = {-1.23456, +3.45678};

     for (int k = 0; k != K_MAX; ++k) {
          vecB.RealPart(vecA);
          vecC.ImaginaryPart(vecA);
     }

     EXPECT_TRUE(vecB == -1.23456);
     EXPECT_TRUE(vecC == +3.45678);
}

//===========//
// test # 14 //
//===========//

TEST(VectorTest14, RealImaginaryPart2)
{
     VectorDense<complex<double>> vecA;
     VectorDense<double> vecB;
     VectorDense<double> vecC;
     const int K_MAX = 10;

     // allocations

     const pgg::VEI ELEMS = 5*static_cast<pgg::VEI>(pow(10.0, 7.0));

     vecA.Allocate(ELEMS);
     vecB.Allocate(ELEMS);
     vecC.Allocate(ELEMS);

     // build vector

     for (pgg::VEI_MAX i = 0; i != ELEMS; ++i) {
          const complex<double> tmp_elem = { static_cast<double>(i),
                                             static_cast<double>(i)
                                           };

          vecA.SetElement(i, tmp_elem);
     }

     // get real and imaginary parts

     for (int k = 0; k != K_MAX; ++k) {
          vecB.RealPart(vecA);
          vecC.ImaginaryPart(vecA);
     }

     // test

     for (pgg::VEI_MAX i = 0; i != ELEMS; ++i) {
          EXPECT_TRUE(vecB(i) == static_cast<double>(i));
          EXPECT_TRUE(vecC(i) == static_cast<double>(i));
     }
}

// END
