//=============//
// test driver //
//=============//

// sepolia

#include "../../../head/sepolia.hpp"

using pgg::sep::MatrixDense;
using pgg::sep::VectorDense;
using std::cos;
using std::sin;

// Google Test

#include "gtest/gtest.h"

//==========//
// test # 1 //
//==========//

TEST(VectorTest1, Swap)
{
     // This test is named "Swap",
     // and belongs to the "VectorTest1"

     VectorDense<double> vecA;
     VectorDense<double> vecB;
     const int ELEMS = 1000;

     vecA.Allocate(ELEMS);
     vecB.Allocate(ELEMS);

     // # 1

     const double ELEM_A = 10.0;
     const double ELEM_B = 20.1;

     vecA = ELEM_A;
     vecB = ELEM_B;

     EXPECT_EQ(vecA, ELEM_A);
     EXPECT_EQ(vecB, ELEM_B);

     // # 2

     vecA.Swap(vecB);

     EXPECT_EQ(vecA, ELEM_B);
     EXPECT_EQ(vecB, ELEM_A);

     // # 3

     vecA.Swap(vecB);

     EXPECT_EQ(vecA, ELEM_A);
     EXPECT_EQ(vecB, ELEM_B);
}

//==========//
// test # 2 //
//==========//

TEST(VectorTest2, Swap)
{
     // This test is named "Swap",
     // and belongs to the "VectorTest2"

     VectorDense<double> vecA;
     VectorDense<double> vecB;
     const int ELEMS = static_cast<int>(pow(10.0, 3.0));

     vecA.Allocate(ELEMS);
     vecB.Allocate(ELEMS);

     // # 1

     const double ELEM_A = 10.0;
     const double ELEM_B = 20.1;

     vecA = ELEM_A;
     vecB = ELEM_B;

     EXPECT_EQ(vecA, ELEM_A);
     EXPECT_EQ(vecB, ELEM_B);

     // # 2

     for (int i = 0; i != 5; ++i) {
          vecA.Swap(vecB);
     }

     EXPECT_EQ(vecA, ELEM_B);
     EXPECT_EQ(vecB, ELEM_A);

     // # 3

     for (int i = 0; i != 5; ++i) {
          vecA.Swap(vecB);
     }

     EXPECT_EQ(vecA, ELEM_A);
     EXPECT_EQ(vecB, ELEM_B);
}

//==========//
// test # 3 //
//==========//

TEST(VectorTest3, CheckCompatibility)
{
     VectorDense<double> vecA;
     VectorDense<double> vecB;

     const int ELEM = 100;

     // # 1

     vecA.Allocate(ELEM);
     vecB.Allocate(ELEM);

     EXPECT_TRUE(vecA.AllocatedQ());
     EXPECT_TRUE(vecB.AllocatedQ());

     EXPECT_TRUE(vecA.CheckCompatibility(vecB));
     EXPECT_TRUE(vecB.CheckCompatibility(vecA));

     // # 2

     vecA.Deallocate();
     vecB.Deallocate();

     // # 3

     EXPECT_TRUE(vecA.DeallocatedQ());
     EXPECT_TRUE(vecB.DeallocatedQ());
}

//==========//
// test # 4 //
//==========//

TEST(VectorTest4, Equal)
{
     VectorDense<double> vecA;
     VectorDense<double> vecB;

     const int ELEM = 100;

     // # 1

     vecA.Allocate(ELEM);
     vecB.Allocate(ELEM);

     vecA = 11.1111;
     vecB = 12.3456;

     EXPECT_TRUE(vecA.Equal(11.1111));
     EXPECT_TRUE(vecB.Equal(12.3456));

     EXPECT_FALSE(vecA.Equal(11.11111));
     EXPECT_FALSE(vecB.Equal(12.34567));

     EXPECT_TRUE(vecA.Equal(11.11111, 0.01));
     EXPECT_TRUE(vecB.Equal(12.34567, 0.01));

     EXPECT_FALSE(vecA.Equal(11.11111, 0.000000001));
     EXPECT_FALSE(vecB.Equal(12.34567, 0.000000001));

     EXPECT_TRUE(vecA.Equal(vecA));
     EXPECT_TRUE(vecB.Equal(vecB));

     EXPECT_TRUE(vecA.Equal(vecA, 10.0));
     EXPECT_TRUE(vecB.Equal(vecB, 20.0));

     EXPECT_FALSE(vecA.Equal(vecA, -10.0));
     EXPECT_FALSE(vecB.Equal(vecB, -20.0));

     EXPECT_TRUE(vecA.Equal(vecB, 100.0));
     EXPECT_TRUE(vecB.Equal(vecA, 200.0));

     vecA = 10.00000000000000000;
     vecB = 10.00000000000000001;

     EXPECT_TRUE(vecA.Equal(vecB));
     EXPECT_TRUE(vecB.Equal(vecA));

     vecA = 10.0000000000000000;
     vecB = 10.0000000000000001;

     EXPECT_TRUE(vecA.Equal(vecB));
     EXPECT_TRUE(vecB.Equal(vecA));

     vecA = 10.000000000000000; // 15 digits accuracy for double
     vecB = 10.000000000000001;

     EXPECT_FALSE(vecA.Equal(vecB));
     EXPECT_FALSE(vecB.Equal(vecA));

     // # 2

     VectorDense<long double> vecC;
     VectorDense<long double> vecD;

     vecC.Allocate(1000);
     vecD.Allocate(1000);

     vecC = 10.00000000000000000L;
     vecD = 10.00000000000000001L;

     EXPECT_FALSE(vecC.Equal(vecD));
     EXPECT_FALSE(vecD.Equal(vecC));

     vecC = 10.0000000000000000000L;
     vecD = 10.0000000000000000001L; // 19 digits accuracy for long double

     EXPECT_TRUE(vecC.Equal(vecD));
     EXPECT_TRUE(vecD.Equal(vecC));
}

//==========//
// test # 5 //
//==========//

TEST(VectorTest5, Map)
{
     VectorDense<double> vecA;
     VectorDense<double> vecB;

     vecA.Allocate(100);
     vecB.Allocate(100);

     vecA = 10.0;
     vecB = 20.0;

     // # 1

     vecA.Map(cos);
     vecB.Map(sin);

     EXPECT_EQ(vecA, cos(10.0));
     EXPECT_EQ(vecB, sin(20.0));

     // # 2

     vecA.Map(cos);
     vecB.Map(sin);

     EXPECT_EQ(vecA, cos(cos(10.0)));
     EXPECT_EQ(vecB, sin(sin(20.0)));

     // # 3

     vecA.Map(cos);
     vecB.Map(sin);

     EXPECT_EQ(vecA, cos(cos(cos(10.0))));
     EXPECT_EQ(vecB, sin(sin(sin(20.0))));

     // # 4

     vecA.Map(sin);
     vecB.Map(cos);

     vecA.Map(sin);
     vecB.Map(cos);

     EXPECT_EQ(vecA, sin(sin(cos(cos(cos(10.0))))));
     EXPECT_EQ(vecB, cos(cos(sin(sin(sin(20.0))))));
}

//==========//
// test # 6 //
//==========//

TEST(VectorTest6, Nest)
{
     VectorDense<double> vecA;
     VectorDense<double> vecB;

     vecA.Allocate(100);
     vecB.Allocate(100);

     vecA = 10.0;
     vecB = 20.0;

     // # 1

     vecA.Map(cos);
     vecB.Map(sin);

     EXPECT_EQ(vecA, pgg::functions::Cos<double>(10.0));
     EXPECT_EQ(vecB, pgg::functions::Sin<double>(20.0));

     // # 2

     vecA.Nest(pgg::functions::Cos<double>, 4);
     vecB.Nest(pgg::functions::Sin<double>, 4);

     EXPECT_EQ(vecA, cos(cos(cos(cos(cos(10.0))))));
     EXPECT_EQ(vecB, sin(sin(sin(sin(sin(20.0))))));
}

//==========//
// test # 7 //
//==========//

TEST(VectorTest7, Mean)
{
     MatrixDense<double> vecA;
     MatrixDense<double> vecB;

     const int ELEM_A = 100;
     const int ELEM_B = 200;

     vecA.Allocate(ELEM_A);
     vecB.Allocate(ELEM_B);

     vecA.Random(0.0, 1.0, 20);
     vecB.Random(0.0, 1.0, 20);

     double tmpA = vecA.Mean();
     double tmpB = vecB.Mean();

     // # 1

     pgg::functions::SeedRandomGenerator(20);

     double tmp_val = 0.0;
     for (int i = 0; i != ELEM_A; ++i) {
          tmp_val = tmp_val + pgg::functions::RandomNumber(0.0, 1.0);
     }

     tmp_val = tmp_val/ELEM_A;

     EXPECT_TRUE((tmp_val - tmpA) < pow(10.0, -14.0));

     // # 2

     pgg::functions::SeedRandomGenerator(20);

     tmp_val = 0.0;
     for (int i = 0; i != ELEM_B; ++i) {
          tmp_val = tmp_val + pgg::functions::RandomNumber(0.0, 1.0);
     }

     tmp_val = tmp_val/ELEM_B;

     EXPECT_TRUE((tmp_val - tmpB) < pow(10.0, -14.0));
}

//==========//
// test # 8 //
//==========//

TEST(VectorTest8, Range)
{
     VectorDense<double> vecA;
     VectorDense<double> vecB;

     const int ELEM_A = 100;
     const int ELEM_B = 200;

     vecA.Allocate(ELEM_A);
     vecB.Allocate(ELEM_B);

     vecA.Range(1.0);
     vecB.Range(2.0);

     for(int i = 0; i != ELEM_A; ++i) {
          EXPECT_EQ(vecA(i), 1.0 + i);
     }

     for(int i = 0; i != ELEM_B; ++i) {
          EXPECT_EQ(vecB(i), 2.0 + i);
     }
}

//==========//
// test # 9 //
//==========//

TEST(VectorTest9, Range)
{
     VectorDense<double> vecA;
     VectorDense<double> vecB;

     const int ELEM_A = 100;
     const int ELEM_B = 200;

     vecA.Allocate(ELEM_A);
     vecB.Allocate(ELEM_B);

     vecA.Range(1.0, 2.0);
     vecB.Range(2.0, 4.0);

     const double STEP_A = (2.0-1.0)/ELEM_A;
     const double STEP_B = (4.0-2.0)/ELEM_B;

     for(int i = 0; i != ELEM_A; ++i) {
          EXPECT_EQ(vecA(i), STEP_A*i+1.0);
     }

     for(int i = 0; i != ELEM_B; ++i) {
          EXPECT_EQ(vecB(i), STEP_B*i+2.0);
     }
}

//===========//
// test # 10 //
//===========//

TEST(VectorTest10, GreaterAll)
{
     VectorDense<double> vecA;
     VectorDense<double> vecB;

     const int ELEM = 100;

     vecA.Allocate(ELEM);
     vecB.Allocate(ELEM);

     vecA.Range(1.0, 2.0);
     vecB.Range(3.0, 4.0);

     EXPECT_TRUE(vecB.Greater(vecA));
     EXPECT_TRUE(vecB > vecA);
     EXPECT_TRUE(vecB.GreaterEqual(vecA));
     EXPECT_TRUE(vecB >= vecA);
}

//===========//
// test # 11 //
//===========//

TEST(VectorTest11, LessAll)
{
     VectorDense<double> vecA;
     VectorDense<double> vecB;

     const int ELEM = 100;

     vecA.Allocate(ELEM);
     vecB.Allocate(ELEM);

     vecB.Range(1.0, 2.0);
     vecA.Range(3.0, 4.0);

     EXPECT_TRUE(vecB.Less(vecA));
     EXPECT_TRUE(vecB < vecA);
     EXPECT_TRUE(vecB.LessEqual(vecA));
     EXPECT_TRUE(vecB <= vecA);
}

//===========//
// test # 12 //
//===========//

TEST(VectorTest12, GreaterAll)
{
     VectorDense<double> vecA;

     const int ELEM = 100;

     vecA.Allocate(ELEM);

     vecA.Range(1.0, 2.0);

     EXPECT_TRUE(vecA.Greater(0.0));
     EXPECT_TRUE(vecA > -1.0);
     EXPECT_TRUE(vecA.GreaterEqual(1.0));
     EXPECT_TRUE(vecA >= 1.0);
}

//===========//
// test # 13 //
//===========//

TEST(VectorTest13, LessAll)
{
     VectorDense<double> vecA;

     const int ELEM = 100;

     vecA.Allocate(ELEM);

     vecA.Range(3.0, 4.0);

     EXPECT_TRUE(vecA.Less(10.0));
     EXPECT_TRUE(vecA < 4.001);
     EXPECT_TRUE(vecA.LessEqual(4.0));
     EXPECT_TRUE(vecA <= 4.0);
}

//===========//
// test # 14 //
//===========//

TEST(VectorTest14, CompareAllComplexA)
{
     VectorDense<std::complex<double>> vecA;
     VectorDense<std::complex<double>> vecB;

     const int ELEM = 100;

     vecA.Allocate(ELEM);
     vecB.Allocate(ELEM);

     vecA.Set( {1.0, 0.0});
     vecB.Set(1.0);

     EXPECT_TRUE(vecA.Equal(vecB));
     EXPECT_TRUE(vecA == vecB);
     EXPECT_TRUE(vecA.Less( {2.0, 1.0}));
     EXPECT_TRUE(vecA.LessEqual( {4.0,0.0}));

     vecA.Set( {2.0, 3.0});
     vecB.Set( {3.0, 2.0});

     EXPECT_FALSE(vecA.Equal(vecB));
     EXPECT_FALSE(vecA == vecB);
     EXPECT_TRUE(vecA.Less( {5.0,4.0}));
     EXPECT_TRUE(vecA.LessEqual( {5.0,3.0}));
     EXPECT_TRUE(vecB.Less( {5.0,15.0}));
     EXPECT_TRUE(vecB.LessEqual( {3.0,2.0}));

}

//===========//
// test # 15 //
//===========//

TEST(VectorTest15, CompareAllComplexB)
{
     VectorDense<std::complex<long double>> vecA;
     VectorDense<std::complex<long double>> vecB;

     const int ELEM = 100;

     vecA.Allocate(ELEM);
     vecB.Allocate(ELEM);

     vecA.Set( {1.0L, 0.0L});
     vecB.Set(1.0L);

     EXPECT_TRUE(vecA.Equal(vecB));
     EXPECT_TRUE(vecA == vecB);
     EXPECT_TRUE(vecA.Less( {2.0L,2.0L}));
     EXPECT_TRUE(vecA.LessEqual(4.0L));

     vecA.Set( {2.0L, 3.0L});
     vecB.Set( {3.0L, 2.0L});

     EXPECT_FALSE(vecA.Equal(vecB));
     EXPECT_FALSE(vecA == vecB);
     EXPECT_TRUE(vecA.Less( {5.0L, 5.0L}));
     EXPECT_TRUE(vecA.LessEqual( {5.0L,3.0L}));
     EXPECT_TRUE(vecB.Less( {5.0L,4.0L}));
     EXPECT_TRUE(vecB.LessEqual( {5.0L,2.0L}));

}

// END
