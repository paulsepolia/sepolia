//=============//
// test driver //
//=============//

// sepolia

#include "../../../head/sepolia.hpp"

using pgg::sep::MatrixDense;
using pgg::sep::VectorDense;
using std::sin;
using std::cos;

// Google Test

#include "gtest/gtest.h"

//==========//
// test # 1 //
//==========//

TEST(VectorTest1, Get)
{
     // This test is named "Get",
     // and belongs to the "VectorTest1"

     VectorDense<double> vecA;

     // test
     // AllocatedQ()
     // DeallocatedQ()

     EXPECT_EQ(vecA.AllocatedQ(), false);
     EXPECT_EQ(vecA.DeallocatedQ(), true);

     // test
     // AllocatedQ()
     // DeallocatedQ()

     const int NUM_ELEMS = 1000;

     vecA.Allocate(NUM_ELEMS);

     EXPECT_EQ(vecA.AllocatedQ(), true);
     EXPECT_EQ(vecA.DeallocatedQ(), false);

     // test
     // = operator
     // (i,j) operator

     const double ELEM_A = 12.34567;

     vecA = ELEM_A;

     EXPECT_EQ(vecA, ELEM_A);
     EXPECT_EQ(vecA(0), ELEM_A);

     for (int i = 0; i != NUM_ELEMS; ++i) {
          EXPECT_EQ(vecA(i), ELEM_A);
     }

     // test
     // Set
     // GetElement(i)

     const double ELEM_B = 11.2233445566;

     vecA.Set(ELEM_B);

     EXPECT_EQ(vecA, ELEM_B);
     EXPECT_EQ(vecA.GetElement(0), ELEM_B);

     for (int i = 0; i != NUM_ELEMS; ++i) {
          EXPECT_EQ(vecA.GetElement(i), ELEM_B);
     }

     // test
     // GetNumberOfElements()

     EXPECT_EQ(vecA.GetNumberOfElements(), NUM_ELEMS);

     // test
     // GetColumn

     MatrixDense<double> matA;

     EXPECT_FALSE(matA.AllocatedQ());
     EXPECT_TRUE(matA.DeallocatedQ());

     matA.Allocate(NUM_ELEMS);

     EXPECT_TRUE(matA.AllocatedQ());
     EXPECT_FALSE(matA.DeallocatedQ());

     const double ELEM_C = 4.12345;

     matA = ELEM_C;

     vecA.GetColumn(matA, 10);

     EXPECT_EQ(vecA, ELEM_C);

     // test
     // GetRow

     vecA.Deallocate();
     matA.Deallocate();

     EXPECT_FALSE(vecA.AllocatedQ());
     EXPECT_TRUE(vecA.DeallocatedQ());

     EXPECT_FALSE(matA.AllocatedQ());
     EXPECT_TRUE(matA.DeallocatedQ());

     vecA.Allocate(NUM_ELEMS);
     matA.Allocate(NUM_ELEMS);

     const double ELEM_D = 234.123;

     matA = ELEM_D;

     EXPECT_TRUE(vecA.AllocatedQ());
     EXPECT_FALSE(vecA.DeallocatedQ());

     EXPECT_TRUE(matA.AllocatedQ());
     EXPECT_FALSE(matA.DeallocatedQ());

     vecA.GetRow(matA, 3);

     EXPECT_EQ(vecA, ELEM_D);

     for (int i = 0; i != NUM_ELEMS; ++i) {
          EXPECT_EQ(vecA.GetElement(i), ELEM_D);
     }

     // end test
}

// END
