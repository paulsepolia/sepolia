//=============//
// test driver //
//=============//

// sepolia

#include "../../../head/sepolia.hpp"

using pgg::sep::MatrixDense;
using pgg::sep::VectorDense;
using std::sin;
using std::cos;

// Google Test

#include "gtest/gtest.h"

//==========//
// test # 1 //
//==========//

TEST(VectorTest1, Get)
{
     // This test is named "Get",
     // and belongs to the "VectorTest1"

     VectorDense<double> vecA;

     // test
     // AllocatedQ()
     // DeallocatedQ()

     EXPECT_EQ(vecA.AllocatedQ(), false);
     EXPECT_EQ(vecA.DeallocatedQ(), true);

     // test
     // AllocatedQ()
     // DeallocatedQ()

     const int NUM_ELEMS = 1000;

     vecA.Allocate(NUM_ELEMS);

     EXPECT_EQ(vecA.AllocatedQ(), true);
     EXPECT_EQ(vecA.DeallocatedQ(), false);

     // test
     // Set
     // (i) operator

     const double ELEM_A = 12.34567;

     vecA.Set(ELEM_A);

     EXPECT_EQ(vecA, ELEM_A);
     EXPECT_EQ(vecA(0), ELEM_A);

     for (int i = 0; i != NUM_ELEMS; ++i) {
          EXPECT_EQ(vecA(i), ELEM_A);
     }

     // test
     // SetElement
     // GetElement

     const double ELEM_B = 11.2233445566;

     for (int i = 0; i != NUM_ELEMS; ++i) {
          vecA.SetElement(i, ELEM_B);
     }

     for (int i = 0; i != NUM_ELEMS; ++i) {
          EXPECT_EQ(vecA.GetElement(i), ELEM_B);
     }

     // test
     // SetElement
     // GetElement

     for (int i = 0; i != NUM_ELEMS; ++i) {
          vecA.SetElement(i, cos(i));
     }

     const double THRES_A = pow(10.0, -10.0);

     for (int i = 0; i != NUM_ELEMS; ++i) {
          EXPECT_TRUE((vecA.GetElement(i) - cos(i)) < THRES_A);
     }

     const double THRES_B = pow(10.0, -15.0);

     for (int i = 0; i != NUM_ELEMS; ++i) {
          EXPECT_TRUE((vecA.GetElement(i) - cos(i)) < THRES_B);
     }

     // test
     // SetElement
     // GetElement

     VectorDense<long double> vecB;

     vecB.Allocate(NUM_ELEMS);

     for (int i = 0; i != NUM_ELEMS; ++i) {
          vecB.SetElement(i, pgg::functions::Cos(static_cast<long double>(i)));
     }

     for (int i = 0; i != NUM_ELEMS; ++i) {
          EXPECT_TRUE((vecB.GetElement(i) -
                       pgg::functions::Cos(static_cast<long double>(i))) < THRES_A);
     }

     const double THRES_C = pow(10.0, -16.0);

     for (int i = 0; i != NUM_ELEMS; ++i) {
          EXPECT_TRUE((vecB.GetElement(i) -
                       pgg::functions::Cos(static_cast<long double>(i))) < THRES_C);
     }

     const double THRES_D = pow(10.0, -20.0);

     for (int i = 0; i != NUM_ELEMS; ++i) {
          EXPECT_TRUE((vecB.GetElement(i) -
                       pgg::functions::Cos(static_cast<long double>(i))) < THRES_D);
     }

     // end test
}

//==========//
// test # 2 //
//==========//

TEST(VectorTest2, Get)
{
     // This test is named "Get",
     // and belongs to the "VectorTest1"

     VectorDense<long double> vecA;

     // test
     // AllocatedQ()
     // DeallocatedQ()

     EXPECT_EQ(vecA.AllocatedQ(), false);
     EXPECT_EQ(vecA.DeallocatedQ(), true);

     // test
     // AllocatedQ()
     // DeallocatedQ()

     const int NUM_ELEMS = 1000;

     vecA.Allocate(NUM_ELEMS);

     EXPECT_EQ(vecA.AllocatedQ(), true);
     EXPECT_EQ(vecA.DeallocatedQ(), false);

     // test
     // Set
     // (i) operator

     const long double ELEM_A = 12.34567L;

     vecA.Set(pgg::functions::Cos(ELEM_A));

     EXPECT_EQ(vecA, pgg::functions::Cos(ELEM_A));
     EXPECT_EQ(vecA(0), pgg::functions::Cos(ELEM_A));

     for (int i = 0; i != NUM_ELEMS; ++i) {
          EXPECT_EQ(vecA(i), pgg::functions::Cos(ELEM_A));
     }

     // test
     // SetElement
     // GetElement

     const long double ELEM_B = pgg::functions::Sin(11.2233445566L);

     for (int i = 0; i != NUM_ELEMS; ++i) {
          vecA.SetElement(i, pgg::functions::Sin(ELEM_B));
     }

     for (int i = 0; i != NUM_ELEMS; ++i) {
          EXPECT_EQ(vecA.GetElement(i), pgg::functions::Sin(ELEM_B));
     }

     // test
     // SetElement
     // GetElement

     for (int i = 0; i != NUM_ELEMS; ++i) {
          vecA.SetElement(i, pgg::functions::Sin(static_cast<long double>(i)));
     }

     const double THRES_A = pow(10.0, -10.0);

     for (int i = 0; i != NUM_ELEMS; ++i) {
          EXPECT_TRUE((vecA.GetElement(i) - sin(i)) < THRES_A);
     }

     const double THRES_B = pow(10.0, -14.0);

     for (int i = 0; i != NUM_ELEMS; ++i) {
          EXPECT_TRUE((vecA.GetElement(i) - sin(i)) < THRES_B);
     }

     const double THRES_C = pow(10.0, -16.0);

     for (int i = 0; i != NUM_ELEMS; ++i) {
          EXPECT_TRUE((vecA.GetElement(i) -
                       pgg::functions::Sin(static_cast<long double>(i))) < THRES_C);
     }

     const double THRES_D = pow(10.0, -20.0);

     for (int i = 0; i != NUM_ELEMS; ++i) {
          EXPECT_TRUE((vecA.GetElement(i) -
                       pgg::functions::Sin(static_cast<long double>(i))) < THRES_D);
     }

     const double THRES_E = pow(10.0, -14.0);

     for (int i = 0; i != NUM_ELEMS; ++i) {
          EXPECT_TRUE((vecA.GetElement(i) -
                       sin(static_cast<long double>(i))) < THRES_E);
     }

     // end test
}

//==========//
// test # 3 //
//==========//

TEST(VectorTest3, Get)
{
     VectorDense<double> vecA;

     const int ELEMS = 1000;

     vecA.Allocate(ELEMS);

     const double ELEM_A = 12.34;

     vecA = pgg::functions::Sin(ELEM_A);

     EXPECT_TRUE(vecA.Equal(pgg::functions::Sin(ELEM_A)));

     EXPECT_TRUE(vecA.Equal(sin(ELEM_A)));

     EXPECT_TRUE(vecA == sin(ELEM_A));

     vecA.Map(pgg::functions::Cos<double>);

     EXPECT_TRUE(vecA == cos(sin(ELEM_A)));

     vecA.Map(pgg::functions::Sin<double>);

     EXPECT_TRUE(vecA == sin(cos(sin(ELEM_A))));
}

//==========//
// test # 4 //
//==========//

TEST(VectorTest4, GetSet)
{
     VectorDense<double> vecA;
     VectorDense<double> vecB;

     const int ELEMS = 1000;

     vecA.Allocate(ELEMS);
     vecB.Allocate(ELEMS);

     const double ELEM_A = 12.34;

     // # 1

     vecA = pgg::functions::Sin(ELEM_A);
     vecB = vecA;

     EXPECT_TRUE(vecA == vecB);

     // # 2

     vecA.Map(sin);
     vecB.Map(pgg::functions::Sin<double>);

     EXPECT_TRUE(vecA == vecB);

     // # 3

     vecA.Map(asin);
     vecB.Map(pgg::functions::ArcSin<double>);

     EXPECT_TRUE(vecA == vecB);

     // # 4

     const int NEST_NUM = 50;

     vecA.Nest(asin, NEST_NUM);
     vecB.Nest(pgg::functions::ArcSin<double>, NEST_NUM);

     EXPECT_TRUE(vecA == vecB);

     // # 5

     const int NEST_NUM_B = 200;

     vecA.Nest(sin, NEST_NUM_B);
     vecB.Nest(pgg::functions::Sin<double>, NEST_NUM_B);

     EXPECT_TRUE(vecA == vecB);

     // # 6

     const int NEST_NUM_C = 1;

     vecA.Nest(acos, NEST_NUM_C);
     vecB.Nest(pgg::functions::ArcCos<double>, NEST_NUM_C);

     EXPECT_TRUE(vecA == vecB);

     // # 7

     const int NEST_NUM_D = 1;

     vecA.Nest(cos, NEST_NUM_D);
     vecB.Nest(pgg::functions::Cos<double>, NEST_NUM_D);

     EXPECT_TRUE(vecA == vecB);

     // # 8

     const int NEST_NUM_E = 500;

     vecA.Nest(cos, NEST_NUM_E);
     vecB.Nest(pgg::functions::Cos<double>, NEST_NUM_E);
     const double THRES_E = pow(10.0, -15.0);

     for (int i = 0; i != ELEMS; ++i) {
          EXPECT_TRUE((vecA(i) - vecB(i)) < THRES_E);
     }

}

// END
