//=============//
// test driver //
//=============//

// sepolia

#include "../../../head/sepolia.hpp"

using pgg::sep::MatrixDense;
using pgg::sep::VectorDense;

// C++

#include <complex>
#include <cmath>

using std::complex;
using std::pow;
using std::cos;
using std::sin;

// Google Test

#include "gtest/gtest.h"

//==========//
// test # 1 //
//==========//

TEST(VectorTestCOMPLEX1, RealImaginaryPart1)
{
     VectorDense<complex<double>> vecA;
     VectorDense<double> vecB;
     VectorDense<double> vecC;
     const int K_MAX = 10;

     // allocations

     const pgg::VEI ELEMS = 5*static_cast<pgg::VEI>(pow(10.0, 6.0));

     vecA.Allocate(ELEMS);
     vecB.Allocate(ELEMS);
     vecC.Allocate(ELEMS);

     vecA = {-1.23456, +3.45678};

     for (int k = 0; k != K_MAX; ++k) {
          vecB.RealPart(vecA);
          vecC.ImaginaryPart(vecA);
     }

     EXPECT_TRUE(vecB == -1.23456);
     EXPECT_TRUE(vecC == +3.45678);
}

//==========//
// test # 2 //
//==========//

TEST(VectorTestCOMPLEX2, RealImaginaryPart2)
{
     VectorDense<complex<double>> vecA;
     VectorDense<double> vecB;
     VectorDense<double> vecC;
     const int K_MAX = 10;

     // allocations

     const pgg::VEI ELEMS = 5*static_cast<pgg::VEI>(pow(10.0, 6.0));

     vecA.Allocate(ELEMS);
     vecB.Allocate(ELEMS);
     vecC.Allocate(ELEMS);

     // build vector

     for (pgg::VEI_MAX i = 0; i != ELEMS; ++i) {
          const complex<double> tmp_elem = { static_cast<double>(i),
                                             static_cast<double>(i)
                                           };

          vecA.SetElement(i, tmp_elem);
     }

     // get real and imaginary parts

     for (int k = 0; k != K_MAX; ++k) {
          vecB.RealPart(vecA);
          vecC.ImaginaryPart(vecA);
     }

     // test

     for (pgg::VEI_MAX i = 0; i != ELEMS; ++i) {
          EXPECT_TRUE(vecB(i) == static_cast<double>(i));
          EXPECT_TRUE(vecC(i) == static_cast<double>(i));
     }
}

// END
