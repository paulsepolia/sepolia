//=============//
// test driver //
//=============//

// sepolia

#include "../../../head/sepolia.hpp"

using pgg::sep::VectorDense;

// C++

#include <cmath>

using std::cos;
using std::sin;

// Google Test

#include "gtest/gtest.h"

//==========//
// test # 1 //
//==========//

TEST(VectorTest1, op_equal)
{
     VectorDense<double> vecA;
     VectorDense<double> vecB;
     VectorDense<double> vecC;

     const int ELEMS = 100;

     vecA.Allocate(ELEMS);
     vecB.Allocate(ELEMS);
     vecC.Allocate(ELEMS);

     vecA = 10.0;
     vecB = vecA;
     vecC = vecB;

     // # 1

     EXPECT_TRUE(vecA == vecB);
     EXPECT_TRUE(vecB == vecC);
}

//==========//
// test # 2 //
//==========//

TEST(VectorTest2, op_equal)
{
     VectorDense<double> vecA;
     VectorDense<double> vecB;
     VectorDense<double> vecC;

     const int ELEMS = 100;

     vecA.Allocate(ELEMS);
     vecB.Allocate(ELEMS);
     vecC.Allocate(ELEMS);

     vecA = 11.0;
     vecB = 12.0;
     vecC = 13.0;

     // # 1

     EXPECT_TRUE(vecA == 11.0);
     EXPECT_TRUE(vecB == 12.0);
     EXPECT_TRUE(vecC == 13.0);
}

//==========//
// test # 3 //
//==========//

TEST(VectorTest3, op_plus)
{
     VectorDense<double> vecA;
     VectorDense<double> vecB;
     VectorDense<double> vecC;

     const int ELEMS = 100;

     vecA.Allocate(ELEMS);
     vecB.Allocate(ELEMS);
     vecC.Allocate(ELEMS);

     vecA = 11.0;
     vecB = 12.0;
     vecC = 13.0;

     // # 1

     EXPECT_TRUE(vecA+vecB == 11.0+12.0);
     EXPECT_TRUE(vecB+vecA == 12.0+11.0);
     EXPECT_TRUE(vecC+vecA+vecB == 13.0+12.0+11.0);
}

//==========//
// test # 4 //
//==========//

TEST(VectorTest4, op_times)
{
     VectorDense<double> vecA;
     VectorDense<double> vecB;
     VectorDense<double> vecC;

     const int ELEMS = 100;

     vecA.Allocate(ELEMS);
     vecB.Allocate(ELEMS);
     vecC.Allocate(ELEMS);

     vecA = 11.0;
     vecB = 12.0;
     vecC = 13.0;

     // # 1

     EXPECT_TRUE(vecA*vecB == 11.0*12.0);
     EXPECT_TRUE(vecB*vecA == 12.0*11.0);
     EXPECT_TRUE(vecC*vecA*vecB == 13.0*12.0*11.0);
}

//==========//
// test # 5 //
//==========//

TEST(VectorTest5, op_divide)
{
     VectorDense<double> vecA;
     VectorDense<double> vecB;
     VectorDense<double> vecC;

     const int ELEMS = 100;

     vecA.Allocate(ELEMS);
     vecB.Allocate(ELEMS);
     vecC.Allocate(ELEMS);

     vecA = 11.0;
     vecB = 12.0;
     vecC = 13.0;

     // # 1

     EXPECT_TRUE(vecA/vecB == 11.0/12.0);
     EXPECT_TRUE(vecB/vecA == 12.0/11.0);
     EXPECT_TRUE(vecC/vecA/vecB == 13.0/11.0/12.0);
}

//==========//
// test # 6 //
//==========//

TEST(VectorTest6, op_subtract)
{
     VectorDense<double> vecA;
     VectorDense<double> vecB;
     VectorDense<double> vecC;

     const int ELEMS = 100;

     vecA.Allocate(ELEMS);
     vecB.Allocate(ELEMS);
     vecC.Allocate(ELEMS);

     vecA = 11.0;
     vecB = 12.0;
     vecC = 13.0;

     // # 1

     EXPECT_TRUE(vecA-vecB == 11.0-12.0);
     EXPECT_TRUE(vecB-vecA == 12.0-11.0);
     EXPECT_TRUE(vecC-vecA-vecB == 13.0-11.0-12.0);
}

//==========//
// test # 7 //
//==========//

TEST(VectorTest7, op_all)
{
     VectorDense<double> vecA;
     VectorDense<double> vecB;
     VectorDense<double> vecC;

     const int ELEMS = 100;

     vecA.Allocate(ELEMS);
     vecB.Allocate(ELEMS);
     vecC.Allocate(ELEMS);

     vecA = 11.0;
     vecB = 12.0;
     vecC = 13.0;

     // # 1

     EXPECT_TRUE(vecA-vecB+vecA/vecC*vecB == 11.0-12.0+11.0/13.0*12.0);
}

//==========//
// test # 8 //
//==========//

TEST(VectorTest8, op_all)
{
     VectorDense<double> vecA;
     VectorDense<double> vecB;
     VectorDense<double> vecC;

     const int ELEMS = 100;

     vecA.Allocate(ELEMS);
     vecB.Allocate(ELEMS);
     vecC.Allocate(ELEMS);

     vecA = 11.0;
     vecB = 12.0;
     vecC = 13.0;

     vecA = vecA/vecA/vecA/vecA/vecA;
     vecB = vecB/vecB/vecB/vecB/vecB;
     vecC = vecC/vecC/vecC/vecC/vecC;

     // # 1

     EXPECT_TRUE(vecA == 11.0/11.0/11.0/11.0/11.0);
     EXPECT_TRUE(vecB == 12.0/12.0/12.0/12.0/12.0);
     EXPECT_TRUE(vecC == 13.0/13.0/13.0/13.0/13.0);
}

//==========//
// test # 9 //
//==========//

TEST(VectorTest9, op_various)
{
     VectorDense<double> vecA;
     VectorDense<double> vecB;
     VectorDense<double> vecC;

     const int ELEMS = 100;

     vecA.Allocate(ELEMS);
     vecB.Allocate(ELEMS);
     vecC.Allocate(ELEMS);

     vecA = 11.0;
     vecB = 12.0;
     vecC = 13.0;

     vecA = vecA/vecA/vecA/vecA/vecA;
     vecB = vecB/vecB/vecB/vecB/vecB;
     vecC = vecC/vecC/vecC/vecC/vecC;

     // # 1

     EXPECT_FALSE(vecA != 11.0/11.0/11.0/11.0/11.0);
     EXPECT_FALSE(vecB != 12.0/12.0/12.0/12.0/12.0);
     EXPECT_FALSE(vecC != 13.0/13.0/13.0/13.0/13.0);
}

//===========//
// test # 10 //
//===========//

TEST(VectorTest10, op_various)
{
     VectorDense<double> vecA;
     VectorDense<double> vecB;
     VectorDense<double> vecC;

     const int ELEMS = 100;

     vecA.Allocate(ELEMS);
     vecB.Allocate(ELEMS);
     vecC.Allocate(ELEMS);

     vecA = 11.0;
     vecB = 12.0;
     vecC = 13.0;

     vecA += vecA;
     vecB += vecB;
     vecC += vecC;

     // # 1

     EXPECT_TRUE(vecA == 11.0+11.0);
     EXPECT_TRUE(vecB == 12.0+12.0);
     EXPECT_TRUE(vecC == 13.0+13.0);

     EXPECT_FALSE(vecA != 11.0+11.0);
     EXPECT_FALSE(vecB != 12.0+12.0);
     EXPECT_FALSE(vecC != 13.0+13.0);
}

//===========//
// test # 11 //
//===========//

TEST(VectorTest11, op_various)
{
     VectorDense<double> vecA;
     VectorDense<double> vecB;
     VectorDense<double> vecC;

     const int ELEMS = 100;

     vecA.Allocate(ELEMS);
     vecB.Allocate(ELEMS);
     vecC.Allocate(ELEMS);

     vecA = 11.0;
     vecB = 12.0;
     vecC = 13.0;

     vecA /= vecA;
     vecB /= vecB;
     vecC /= vecC;

     // # 1

     EXPECT_TRUE(vecA == 11.0/11.0);
     EXPECT_TRUE(vecB == 12.0/12.0);
     EXPECT_TRUE(vecC == 13.0/13.0);

     EXPECT_FALSE(vecA != 11.0/11.0);
     EXPECT_FALSE(vecB != 12.0/12.0);
     EXPECT_FALSE(vecC != 13.0/13.0);
}

//===========//
// test # 12 //
//===========//

TEST(VectorTest12, op_various)
{
     VectorDense<double> vecA;
     VectorDense<double> vecB;
     VectorDense<double> vecC;

     const int ELEMS = 100;

     vecA.Allocate(ELEMS);
     vecB.Allocate(ELEMS);
     vecC.Allocate(ELEMS);

     vecA = 11.0;
     vecB = 12.0;
     vecC = 13.0;

     vecA *= vecA;
     vecB *= vecB;
     vecC *= vecC;

     // # 1

     EXPECT_TRUE(vecA == 11.0*11.0);
     EXPECT_TRUE(vecB == 12.0*12.0);
     EXPECT_TRUE(vecC == 13.0*13.0);

     EXPECT_FALSE(vecA != 11.0*11.0);
     EXPECT_FALSE(vecB != 12.0*12.0);
     EXPECT_FALSE(vecC != 13.0*13.0);
}

//===========//
// test # 13 //
//===========//

TEST(VectorTest13, op_various)
{
     VectorDense<double> vecA;
     VectorDense<double> vecB;
     VectorDense<double> vecC;

     const int ELEMS = 100;

     vecA.Allocate(ELEMS);
     vecB.Allocate(ELEMS);
     vecC.Allocate(ELEMS);

     vecA = 11.0;
     vecB = 12.0;
     vecC = 13.0;

     vecA -= vecA;
     vecB -= vecB;
     vecC -= vecC;

     // # 1

     EXPECT_TRUE(vecA == 11.0-11.0);
     EXPECT_TRUE(vecB == 12.0-12.0);
     EXPECT_TRUE(vecC == 13.0-13.0);

     EXPECT_FALSE(vecA != 11.0-11.0);
     EXPECT_FALSE(vecB != 12.0-12.0);
     EXPECT_FALSE(vecC != 13.0-13.0);
}

//===========//
// test # 14 //
//===========//

TEST(VectorTest14, SlowWayA)
{
     // local parameters

     const auto DIM = 1 *static_cast<pgg::MAI_MAX>(pow(10.0, 2.0));
     const auto K_MAX_LOC = 10;
     const auto ONE_DOUBLE = 1.0;
     const auto TWO_DOUBLE = 2.0;
     const auto THREE_DOUBLE = 3.0;

     VectorDense<double> vecA;
     VectorDense<double> vecB;
     VectorDense<double> vecC;

     // allocate heap space

     vecA.Allocate(DIM);
     vecB.Allocate(DIM);
     vecC.Allocate(DIM);

     // build vectors

     vecA = ONE_DOUBLE;
     vecB = TWO_DOUBLE;
     vecC = THREE_DOUBLE;

     // test equality

     EXPECT_TRUE(vecA == ONE_DOUBLE);
     EXPECT_TRUE(vecB == TWO_DOUBLE);
     EXPECT_TRUE(vecC == THREE_DOUBLE);

     // add vectors

     for (int i = 0; i != K_MAX_LOC; ++i) {
          vecC = vecA + vecB;
     }

     EXPECT_TRUE(vecA == ONE_DOUBLE);
     EXPECT_TRUE(vecB == TWO_DOUBLE);
     EXPECT_TRUE(vecC == THREE_DOUBLE);

     // subtract vectors

     for (int i = 0; i != K_MAX_LOC; ++i) {
          vecC = vecA - vecB;
     }

     EXPECT_TRUE(vecA == ONE_DOUBLE);
     EXPECT_TRUE(vecB == TWO_DOUBLE);
     EXPECT_TRUE(vecC == -ONE_DOUBLE);

     // multiply vectors

     for (int i = 0; i != K_MAX_LOC; ++i) {
          vecC = vecA * vecB;
     }

     EXPECT_TRUE(vecA == ONE_DOUBLE);
     EXPECT_TRUE(vecB == TWO_DOUBLE);
     EXPECT_TRUE(vecC == TWO_DOUBLE);

     // divide vectors

     for (int i = 0; i != K_MAX_LOC; ++i) {
          vecC = vecA / vecB;
     }

     EXPECT_TRUE(vecA == ONE_DOUBLE);
     EXPECT_TRUE(vecB == TWO_DOUBLE);
     EXPECT_TRUE(vecC == (ONE_DOUBLE/TWO_DOUBLE));

     // swap vectors

     for (int i = 0; i != K_MAX_LOC; ++i) {

          vecA = ONE_DOUBLE;
          vecB = TWO_DOUBLE;
          vecC = THREE_DOUBLE;

          vecA = vecB;
          vecB = vecC;
          vecC = ONE_DOUBLE;
     }

     EXPECT_TRUE(vecA == TWO_DOUBLE);
     EXPECT_TRUE(vecB == THREE_DOUBLE);
     EXPECT_TRUE(vecC == ONE_DOUBLE);

     // advance vectors

     vecA = ONE_DOUBLE;
     vecB = TWO_DOUBLE;
     vecC = THREE_DOUBLE;

     for (int i = 0; i != K_MAX_LOC; ++i) {
          vecA = vecA + ONE_DOUBLE;
          vecB = vecB + ONE_DOUBLE;
          vecC = vecC + ONE_DOUBLE;
     }

     EXPECT_TRUE(vecA == (ONE_DOUBLE+ONE_DOUBLE*K_MAX_LOC));
     EXPECT_TRUE(vecB == (TWO_DOUBLE+ONE_DOUBLE*K_MAX_LOC));
     EXPECT_TRUE(vecC == (THREE_DOUBLE+ONE_DOUBLE*K_MAX_LOC));

     // advance vectors

     vecA = ONE_DOUBLE;
     vecB = TWO_DOUBLE;
     vecC = THREE_DOUBLE;

     for (int i = 0; i != K_MAX_LOC; ++i) {
          vecA = vecA - ONE_DOUBLE;
          vecB = vecB - ONE_DOUBLE;
          vecC = vecC - ONE_DOUBLE;
     }

     EXPECT_TRUE(vecA == (ONE_DOUBLE-ONE_DOUBLE*K_MAX_LOC));
     EXPECT_TRUE(vecB == (TWO_DOUBLE-ONE_DOUBLE*K_MAX_LOC));
     EXPECT_TRUE(vecC == (THREE_DOUBLE-ONE_DOUBLE*K_MAX_LOC));

     // advance vectors

     vecA = ONE_DOUBLE;
     vecB = TWO_DOUBLE;
     vecC = THREE_DOUBLE;

     for (int i = 0; i != K_MAX_LOC; ++i) {
          vecA = vecA * ONE_DOUBLE;
          vecB = vecB * ONE_DOUBLE;
          vecC = vecC * ONE_DOUBLE;
     }

     EXPECT_TRUE(vecA == ONE_DOUBLE);
     EXPECT_TRUE(vecB == TWO_DOUBLE);
     EXPECT_TRUE(vecC == THREE_DOUBLE);

     // advance vectors

     vecA = ONE_DOUBLE;
     vecB = TWO_DOUBLE;
     vecC = THREE_DOUBLE;

     for (int i = 0; i != K_MAX_LOC; ++i) {
          vecA = vecA / ONE_DOUBLE;
          vecB = vecB / ONE_DOUBLE;
          vecC = vecC / ONE_DOUBLE;
     }

     EXPECT_TRUE(vecA == ONE_DOUBLE);
     EXPECT_TRUE(vecB == TWO_DOUBLE);
     EXPECT_TRUE(vecC ==THREE_DOUBLE);

     // advance vectors

     vecA = ONE_DOUBLE;
     vecB = TWO_DOUBLE;
     vecC = THREE_DOUBLE;

     for (int i = 0; i != K_MAX_LOC; ++i) {
          vecA = vecA + vecA;
          vecB = vecB + vecB;
          vecC = vecC + vecC;
     }

     EXPECT_TRUE(vecA == 1.0*pow(TWO_DOUBLE, K_MAX_LOC));
     EXPECT_TRUE(vecB == 2.0*pow(TWO_DOUBLE, K_MAX_LOC));
     EXPECT_TRUE(vecC == 3.0*pow(TWO_DOUBLE, K_MAX_LOC));

     // advance vectors

     vecA = ONE_DOUBLE;
     vecB = TWO_DOUBLE;
     vecC = THREE_DOUBLE;

     for (int i = 0; i != K_MAX_LOC; ++i) {
          vecA = vecA - vecA;
          vecB = vecB - vecB;
          vecC = vecC - vecC;
     }

     EXPECT_TRUE(vecA == pgg::ZERO_DBL);
     EXPECT_TRUE(vecB == pgg::ZERO_DBL);
     EXPECT_TRUE(vecC == pgg::ZERO_DBL);

     // advance vectors

     vecA = ONE_DOUBLE;
     vecB = TWO_DOUBLE;
     vecC = THREE_DOUBLE;

     for (int i = 0; i != K_MAX_LOC; ++i) {
          vecA = vecA / vecA;
          vecB = vecB / vecB;
          vecC = vecC / vecC;
     }

     EXPECT_TRUE(vecA == pgg::ONE_DBL);
     EXPECT_TRUE(vecB == pgg::ONE_DBL);
     EXPECT_TRUE(vecC == pgg::ONE_DBL);

     // advance vectors

     vecA = ONE_DOUBLE;
     vecB = TWO_DOUBLE;
     vecC = THREE_DOUBLE;

     for (int i = 0; i != K_MAX_LOC; ++i) {
          vecA = vecA * vecA;
          vecB = vecB * vecB;
          vecB = vecB / TWO_DOUBLE;
          vecC = vecC * vecC;
          vecC = vecC / THREE_DOUBLE;
     }

     EXPECT_TRUE(vecA == ONE_DOUBLE);
     EXPECT_TRUE(vecB == TWO_DOUBLE);
     EXPECT_TRUE(vecC == THREE_DOUBLE);

     // deallocate heap space

     vecA.Deallocate();
     vecB.Deallocate();
     vecC.Deallocate();
}

//===========//
// test # 14 //
//===========//

TEST(VectorTest14, GreaterAll)
{
     VectorDense<double> vecA;
     VectorDense<double> vecB;

     const int ELEM = 100;

     vecA.Allocate(ELEM);
     vecB.Allocate(ELEM);

     vecA.Range(1.0, 2.0);
     vecB.Range(2.0, 4.0);

     EXPECT_TRUE(vecB > vecA);
     EXPECT_TRUE(vecB >= vecA);
}

//===========//
// test # 15 //
//===========//

TEST(VectorTest15, LessAll)
{
     VectorDense<double> vecA;
     VectorDense<double> vecB;

     const int ELEM = 100;

     vecA.Allocate(ELEM);
     vecB.Allocate(ELEM);

     vecB.Range(1.0, 2.0);
     vecA.Range(2.0, 4.0);

     EXPECT_TRUE(vecB < vecA);
     EXPECT_TRUE(vecB <= vecA);
}

//===========//
// test # 16 //
//===========//

TEST(VectorTest16, GreaterAll)
{
     VectorDense<double> vecA;

     const int ELEM = 100;

     vecA.Allocate(ELEM);

     vecA.Range(1.0, 2.0);

     EXPECT_TRUE(vecA >= -1.0);
     EXPECT_TRUE(vecA >= 1.0);
}

//===========//
// test # 17 //
//===========//

TEST(VectorTest17, LessAll)
{
     VectorDense<double> vecA;

     const int ELEM = 100;

     vecA.Allocate(ELEM);

     vecA.Range(3.0, 4.0);

     EXPECT_TRUE(vecA < 4.001);
     EXPECT_TRUE(vecA <= 4.0);

     vecA.Set(10.0);

     EXPECT_TRUE(vecA <= 10.0);
     EXPECT_TRUE(vecA >= 10.0);
     EXPECT_TRUE(vecA == 10.0);
}

//===========//
// test # 18 //
//===========//

TEST(VectorTest18, LessAll)
{
     VectorDense<double> vecA;
     VectorDense<double> vecB;

     const int ELEM = 100;

     vecA.Allocate(ELEM);
     vecB.Allocate(ELEM);

     vecA.Range(3.0, 4.0);
     vecB.Range(3.0, 5.0);

     EXPECT_TRUE(vecA <= vecB);
     EXPECT_FALSE(vecA < vecB);

     EXPECT_TRUE(vecA >= 3.0);
     EXPECT_FALSE(vecA > 3.0);
     EXPECT_FALSE(vecA < 3.0);
     EXPECT_FALSE(vecA <= 3.0);

     EXPECT_TRUE(vecA != vecB);
     EXPECT_FALSE(vecA == vecB);
}

// END
