//=============//
// test driver //
//=============//

// sepolia

#include "../../../head/sepolia.hpp"

using pgg::sep::MatrixDense;
using pgg::sep::VectorDense;
using std::cos;
using std::sin;

// Google Test

#include "gtest/gtest.h"

//==========//
// test # 1 //
//==========//

TEST(VectorTest1, Divide)
{
     // This test is named "Divide",
     // and belongs to the "VectorTest1"

     VectorDense<double> vecA;
     const int ELEMS = 1000;

     vecA.Allocate(ELEMS);

     // # 1

     const double ELEM_A = 10.0;

     vecA = ELEM_A;

     EXPECT_EQ(vecA, ELEM_A);

     // # 2

     const double ELEM_B = 11.0;

     vecA.Divide(ELEM_B);

     EXPECT_EQ(vecA, ELEM_A/ELEM_B);

     // # 3

     VectorDense<double> vecB;

     vecB.Allocate(ELEMS);

     vecB = vecA;

     EXPECT_EQ(vecA, vecB);

     // # 4

     vecA.Divide(vecB);

     EXPECT_EQ(vecA, 1.0);

     // # 5

     vecB.Divide(vecB);

     EXPECT_EQ(vecB, 1.0);

     // # 6

     const double ELEM_C = 0.123456;
     const double ELEM_D = 1.234567;

     vecA = ELEM_C;
     vecB = ELEM_D;

     EXPECT_EQ(vecA/vecB, ELEM_C/ELEM_D);

     // # 7

     EXPECT_EQ(vecA/vecB + vecB/vecA, ELEM_C/ELEM_D + ELEM_D/ELEM_C);

     // # 8

     vecA = pgg::functions::Sin(ELEM_C);
     vecB = pgg::functions::Cos(ELEM_D);

     vecA.Divide(vecA, vecB);

     EXPECT_EQ(vecA, sin(ELEM_C)/cos(ELEM_D));

     // # 9

     for (int i = 0; i != ELEMS; ++i) {
          vecA.SetElement(i, pgg::functions::Cos(static_cast<double>(i)));
          vecB.SetElement(i, pgg::functions::Sin(static_cast<double>(i))+0.1);
     }

     vecA = vecA / vecB;

     const double THRES_A = pow(10.0, -11.0);

     for (int i = 0; i != ELEMS; ++i) {
          EXPECT_TRUE((vecA(i) -  pgg::functions::Cos(static_cast<double>(i)) /
                       (pgg::functions::Sin(static_cast<double>(i))+0.1)) <
                      THRES_A);
     }

     // # 10

     const double  TINY_A = pow(10.0, -10.0);

     for (int i = 0; i != ELEMS; ++i) {
          vecA.SetElement(i, pgg::functions::Cos(static_cast<double>(i)));
          vecB.SetElement(i, pgg::functions::Sin(static_cast<double>(i))+ TINY_A);
     }

     vecA = vecA / vecB;

     const double THRES_B = pow(10.0, -10.0);

     for (int i = 0; i != ELEMS; ++i) {
          EXPECT_TRUE((vecA(i) -  pgg::functions::Cos(static_cast<double>(i)) /
                       (pgg::functions::Sin(static_cast<double>(i))+ TINY_A)) <
                      THRES_B);
     }
}

//==========//
// test # 2 //
//==========//

TEST(VectorTest2, Plus)
{
     // This test is named "Plus",
     // and belongs to the "VectorTest2"

     VectorDense<double> vecA;
     const int ELEMS = 1000;

     vecA.Allocate(ELEMS);

     // # 1

     const double ELEM_A = 10.0;

     vecA = ELEM_A;

     EXPECT_EQ(vecA, ELEM_A);

     // # 2

     const double ELEM_B = 11.0;

     vecA.Plus(ELEM_B);

     EXPECT_EQ(vecA, ELEM_A+ELEM_B);

     // # 3

     VectorDense<double> vecB;

     vecB.Allocate(ELEMS);

     vecB = vecA;

     EXPECT_EQ(vecA, vecB);

     // # 4

     vecA.Plus(vecB);

     EXPECT_EQ(vecA, 2*(ELEM_A+ELEM_B));

     // # 5

     vecB.Plus(vecB);

     EXPECT_EQ(vecB, vecA);

     // # 6

     const double ELEM_C = 0.123456;
     const double ELEM_D = 1.234567;

     vecA = ELEM_C;
     vecB = ELEM_D;

     EXPECT_EQ(vecA+vecB, ELEM_C+ELEM_D);

     // # 7

     EXPECT_EQ(vecA+vecB + vecB+vecA, ELEM_C+ELEM_D + ELEM_D+ELEM_C);

     // # 8

     vecA = pgg::functions::Sin(ELEM_C);
     vecB = pgg::functions::Cos(ELEM_D);

     vecA.Plus(vecA, vecB);

     EXPECT_EQ(vecA, sin(ELEM_C)+cos(ELEM_D));

     // # 9

     for (int i = 0; i != ELEMS; ++i) {
          vecA.SetElement(i, pgg::functions::Cos(static_cast<double>(i)));
          vecB.SetElement(i, pgg::functions::Sin(static_cast<double>(i)));
     }

     vecA = vecA + vecB;

     const double THRES_A = pow(10.0, -11.0);

     for (int i = 0; i != ELEMS; ++i) {
          EXPECT_TRUE(vecA(i) - (pgg::functions::Cos(static_cast<double>(i)) +
                                 pgg::functions::Sin(static_cast<double>(i))) <
                      THRES_A);
     }

     // # 10

     vecA = 20.1234;
     vecB = 12.3456;
     VectorDense<double> vecC;
     vecC.Allocate(ELEMS);

     vecC.Plus(vecA, vecB);

     for (int i = 0; i != ELEMS; ++i) {
          EXPECT_EQ(vecC(i), vecA(i) + vecB(i));
     }

     EXPECT_EQ(vecC, vecA+vecB);

}

//==========//
// test # 3 //
//==========//

TEST(VectorTest3, Subtract)
{
     // This test is named "Subtract",
     // and belongs to the "VectorTest3"

     VectorDense<double> vecA;
     const int ELEMS = 1000;

     vecA.Allocate(ELEMS);

     // # 1

     const double ELEM_A = 10.0;

     vecA = ELEM_A;

     EXPECT_EQ(vecA, ELEM_A);

     // # 2

     const double ELEM_B = 11.0;

     vecA.Subtract(ELEM_B);

     EXPECT_EQ(vecA, ELEM_A-ELEM_B);

     // # 3

     VectorDense<double> vecB;

     vecB.Allocate(ELEMS);

     vecB = vecA;

     EXPECT_EQ(vecA, vecB);

     // # 4

     vecA = ELEM_A;
     vecB = ELEM_B;

     vecA.Subtract(vecB);

     EXPECT_EQ(vecA, ELEM_A-ELEM_B);

     // # 5

     vecB.Subtract(vecB);

     EXPECT_EQ(vecB, 0.0);

     // # 6

     const double ELEM_C = 0.123456;
     const double ELEM_D = 1.234567;

     vecA = ELEM_C;
     vecB = ELEM_D;

     EXPECT_EQ(vecA-vecB, ELEM_C-ELEM_D);

     // # 7

     EXPECT_EQ(vecA-vecB + vecB-vecA, ELEM_C-ELEM_D + ELEM_D-ELEM_C);

     // # 8

     vecA = pgg::functions::Sin(ELEM_C);
     vecB = pgg::functions::Cos(ELEM_D);

     vecA.Subtract(vecA, vecB);

     EXPECT_EQ(vecA, sin(ELEM_C)-cos(ELEM_D));

     // # 9

     for (int i = 0; i != ELEMS; ++i) {
          vecA.SetElement(i, pgg::functions::Cos(static_cast<double>(i)));
          vecB.SetElement(i, pgg::functions::Sin(static_cast<double>(i)));
     }

     vecA = vecA - vecB;

     const double THRES_A = pow(10.0, -11.0);

     for (int i = 0; i != ELEMS; ++i) {
          EXPECT_TRUE(vecA(i) - (pgg::functions::Cos(static_cast<double>(i)) -
                                 pgg::functions::Sin(static_cast<double>(i))) <
                      THRES_A);
     }

     // # 10

     vecA = 20.1234;
     vecB = 12.3456;
     VectorDense<double> vecC;
     vecC.Allocate(ELEMS);

     vecC.Subtract(vecA, vecB);

     for (int i = 0; i != ELEMS; ++i) {
          EXPECT_EQ(vecC(i), vecA(i) - vecB(i));
     }

     EXPECT_EQ(vecC, vecA-vecB);

}

//==========//
// test # 4 //
//==========//

TEST(VectorTest4, Times)
{
     // This test is named "Times",
     // and belongs to the "VectorTest4"

     VectorDense<double> vecA;
     const int ELEMS = 1000;

     vecA.Allocate(ELEMS);

     // # 1

     const double ELEM_A = 10.0;

     vecA = ELEM_A;

     EXPECT_EQ(vecA, ELEM_A);

     // # 2

     const double ELEM_B = 11.0;

     vecA.Times(ELEM_B);

     EXPECT_EQ(vecA, ELEM_A*ELEM_B);

     // # 3

     VectorDense<double> vecB;

     vecB.Allocate(ELEMS);

     vecB = vecA;

     EXPECT_EQ(vecA, vecB);

     // # 4

     vecA = ELEM_A;
     vecB = ELEM_B;

     vecA.Times(vecB);

     EXPECT_EQ(vecA, ELEM_A*ELEM_B);

     // # 5

     vecB.Times(vecB);

     EXPECT_EQ(vecB, ELEM_B*ELEM_B);

     // # 6

     const double ELEM_C = 0.123456;
     const double ELEM_D = 1.234567;

     vecA = ELEM_C;
     vecB = ELEM_D;

     EXPECT_EQ(vecA*vecB, ELEM_C*ELEM_D);

     // # 7

     EXPECT_EQ(vecA*vecB + vecB*vecA, ELEM_C*ELEM_D + ELEM_D*ELEM_C);

     // # 8

     vecA = pgg::functions::Sin(ELEM_C);
     vecB = pgg::functions::Cos(ELEM_D);

     vecA.Times(vecA, vecB);

     EXPECT_EQ(vecA, sin(ELEM_C)*cos(ELEM_D));

     // # 9

     for (int i = 0; i != ELEMS; ++i) {
          vecA.SetElement(i, pgg::functions::Cos(static_cast<double>(i)));
          vecB.SetElement(i, pgg::functions::Sin(static_cast<double>(i)));
     }

     vecA = vecA * vecB;

     const double THRES_A = pow(10.0, -11.0);

     for (int i = 0; i != ELEMS; ++i) {
          EXPECT_TRUE(vecA(i) - (pgg::functions::Cos(static_cast<double>(i)) *
                                 pgg::functions::Sin(static_cast<double>(i))) <
                      THRES_A);
     }

     // # 10

     vecA = 20.1234;
     vecB = 12.3456;
     VectorDense<double> vecC;
     vecC.Allocate(ELEMS);

     vecC.Times(vecA, vecB);

     for (int i = 0; i != ELEMS; ++i) {
          EXPECT_EQ(vecC(i), vecA(i) * vecB(i));
     }

     EXPECT_EQ(vecC, vecA*vecB);

}

//==========//
// test # 5 //
//==========//

TEST(VectorTest5, Negate)
{
     // This test is named "Negate",
     // and belongs to the "VectorTest5"

     VectorDense<double> vecA;
     const int ELEMS = 1000;

     vecA.Allocate(ELEMS);

     // # 1

     const double ELEM_A = 10.0;

     vecA = ELEM_A;

     EXPECT_EQ(vecA, ELEM_A);

     // # 2

     vecA.Negate();

     EXPECT_EQ(vecA, -ELEM_A);

     // # 3

     vecA.Negate();

     EXPECT_EQ(vecA, ELEM_A);

     // # 4

     VectorDense<double> vecB;

     vecB.Allocate(ELEMS);

     (vecA+vecA).Negate(); // DOES NOT AFFECT vecA

     vecB = ELEM_A;

     EXPECT_EQ(vecB, vecA);

     // # 5

     vecA = ELEM_A;
     vecB = ELEM_A;

     (2.0*vecA).Negate(); // DOES NOT AFFECT vecA
     (2.0*vecB).Negate(); // DOES NOT AFFECT vecB

     EXPECT_EQ(vecB, vecA);

     EXPECT_EQ(0.0*vecB, 0.0*vecA);

     EXPECT_EQ(-12345.678*vecB, -12345.678*vecA);

     EXPECT_EQ(-0.001*vecB, -0.001*vecA);

     EXPECT_EQ(0.000002*vecB, 0.000002*vecA);

     EXPECT_EQ(0.1*vecB*vecA, vecA*vecB*0.1);
}

//==========//
// test # 6 //
//==========//

TEST(VectorTest6, Minus)
{
     // This test is named "Minus",
     // and belongs to the "VectorTest6"

     VectorDense<double> vecA;
     const int ELEMS = 1000;

     vecA.Allocate(ELEMS);

     const double ELEM_A = 10.0;

     vecA = ELEM_A;

     VectorDense<double> vecB(vecA);

     // # 1

     EXPECT_EQ(vecA, ELEM_A);

     // # 2

     EXPECT_EQ(vecA, vecB);

     // # 3

     vecA.Minus(vecB);

     EXPECT_EQ(vecA, -vecB);

     EXPECT_EQ(vecA, -(-(-vecB)));

     EXPECT_EQ(-vecA, -(-vecB));

     EXPECT_EQ(-vecA, -(-(-vecA)));

     // # 4

     vecA = 10.0;
     vecB = 11.1;

     vecA.Minus(vecA);
     vecB.Minus(vecB);

     EXPECT_EQ(vecA, -10.0);
     EXPECT_EQ(vecB, -11.1);

     // # 5

     vecA = 10.0;
     vecB = 11.1;

     vecA.Minus(vecA+vecB);
     vecB.Minus(vecB+vecA);

     EXPECT_EQ(vecA, -21.1);

     const double THRES_A = pow(10.0, -15.0);
     const double THRES_B = pow(10.0, -16.0);
     const double THRES_C = pow(10.0, -17.0);

     for (int i = 0; i != ELEMS; ++i) {
          EXPECT_TRUE((vecB(i) - (+21.1-11.1)) < THRES_A);
          EXPECT_TRUE((vecB(i) - (+21.1-11.1)) < THRES_B);
          EXPECT_TRUE((vecB(i) - (+21.1-11.1)) < THRES_C);
     }
}

//===========//
// test # 7 //
//===========//

TEST(VectorTest7, ComplexDouble)
{
     const unsigned long int ELEMS = 8 *static_cast<unsigned long int>(pow(10.0, 3.0));

     VectorDense<std::complex<double>> vecA;
     VectorDense<std::complex<double>> vecB;

     vecA.Allocate(ELEMS);
     vecB.Allocate(ELEMS);

     vecA = {1.1, 4.0};
     vecB = {1.1, 4.0};

     EXPECT_TRUE(vecA == vecB);
     EXPECT_EQ(vecA, vecB);

     const std::complex<double> coA = {1.1, 2.2};
     const std::complex<double> coB = {1.2, 2.3};

     vecA = coA;
     EXPECT_EQ(vecA, coA);

     vecB = coB;
     EXPECT_EQ(vecB, coB);

     vecA.Deallocate();
     vecB.Deallocate();
}

//==========//
// test # 8 //
//==========//

TEST(VectorTest8, ComplexLongDouble)
{
     const unsigned long int ELEMS = 4 *static_cast<unsigned long int>(pow(10.0, 3.0));

     VectorDense<std::complex<long double>> vecA;
     VectorDense<std::complex<long double>> vecB;

     vecA.Allocate(ELEMS);
     vecB.Allocate(ELEMS);

     vecA = {1.1L, 4.0L};
     vecB = {1.1L, 4.0L};

     EXPECT_TRUE(vecA == vecB);
     EXPECT_EQ(vecA, vecB);

     const std::complex<long double> coA = {1.1L, 2.2L};
     const std::complex<long double> coB = {1.2L, 2.3L};

     vecA = coA;
     EXPECT_EQ(vecA, coA);

     vecB = coB;
     EXPECT_EQ(vecB, coB);

     vecA.Deallocate();
     vecB.Deallocate();
}

//===========//
// test # 9 //
//===========//

TEST(VectorTest9, FastWayA)
{
     // local parameters

     const auto DIM = 8 *static_cast<pgg::VEI_MAX>(pow(10.0, 2.0));
     const auto K_MAX_LOC = 2;
     const auto ONE_DOUBLE = 1.0;
     const auto TWO_DOUBLE = 2.0;
     const auto THREE_DOUBLE = 3.0;

     VectorDense<double> vecA;
     VectorDense<double> vecB;
     VectorDense<double> vecC;

     vecA.Allocate(DIM);
     vecB.Allocate(DIM);
     vecC.Allocate(DIM);

     vecA.Set(ONE_DOUBLE);
     vecB.Set(TWO_DOUBLE);
     vecC.Set(THREE_DOUBLE);

     EXPECT_TRUE(vecA.Equal(ONE_DOUBLE));
     EXPECT_TRUE(vecB.Equal(TWO_DOUBLE));
     EXPECT_TRUE(vecC.Equal(THREE_DOUBLE));

     // add matrices

     for (int i = 0; i != K_MAX_LOC; ++i) {
          vecC.Plus(vecA, vecB);
     }

     EXPECT_TRUE(vecA.Equal(ONE_DOUBLE));
     EXPECT_TRUE(vecB.Equal(TWO_DOUBLE));
     EXPECT_TRUE(vecC.Equal(THREE_DOUBLE));

     // subtract matrices

     for (int i = 0; i != K_MAX_LOC; ++i) {
          vecC.Subtract(vecA, vecB);
     }

     EXPECT_TRUE(vecA.Equal(ONE_DOUBLE));
     EXPECT_TRUE(vecB.Equal(TWO_DOUBLE));
     EXPECT_TRUE(vecC.Equal(-ONE_DOUBLE));

     // multiply matrices


     for (int i = 0; i != K_MAX_LOC; ++i) {
          vecC.Times(vecA, vecB);
     }

     EXPECT_TRUE(vecA.Equal(ONE_DOUBLE));
     EXPECT_TRUE(vecB.Equal(TWO_DOUBLE));
     EXPECT_TRUE(vecC.Equal(TWO_DOUBLE));

     // divide matrices

     for (int i = 0; i != K_MAX_LOC; ++i) {
          vecC.Divide(vecA, vecB);
     }

     EXPECT_TRUE(vecA.Equal(ONE_DOUBLE));
     EXPECT_TRUE(vecB.Equal(TWO_DOUBLE));
     EXPECT_TRUE(vecC.Equal(ONE_DOUBLE/TWO_DOUBLE));

     // swap matrices

     for (int i = 0; i != K_MAX_LOC; ++i) {

          vecA.Set(ONE_DOUBLE);
          vecB.Set(TWO_DOUBLE);
          vecC.Set(THREE_DOUBLE);

          vecA.Set(vecB);
          vecB.Set(vecC);
          vecC.Set(ONE_DOUBLE);
     }

     EXPECT_TRUE(vecA.Equal(TWO_DOUBLE));
     EXPECT_TRUE(vecB.Equal(THREE_DOUBLE));
     EXPECT_TRUE(vecC.Equal(ONE_DOUBLE));

     // advance matrices

     vecA.Set(ONE_DOUBLE);
     vecB.Set(TWO_DOUBLE);
     vecC.Set(THREE_DOUBLE);

     for (int i = 0; i != K_MAX_LOC; ++i) {
          vecA.Plus(vecA, ONE_DOUBLE);
          vecB.Plus(vecB, ONE_DOUBLE);
          vecC.Plus(vecC, ONE_DOUBLE);
     }

     EXPECT_TRUE(vecA.Equal(ONE_DOUBLE+ONE_DOUBLE*K_MAX_LOC));
     EXPECT_TRUE(vecB.Equal(TWO_DOUBLE+ONE_DOUBLE*K_MAX_LOC));
     EXPECT_TRUE(vecC.Equal(THREE_DOUBLE+ONE_DOUBLE*K_MAX_LOC));

     // advance matrices

     vecA.Set(ONE_DOUBLE);
     vecB.Set(TWO_DOUBLE);
     vecC.Set(THREE_DOUBLE);

     for (int i = 0; i != K_MAX_LOC; ++i) {
          vecA.Subtract(vecA, ONE_DOUBLE);
          vecB.Subtract(vecB, ONE_DOUBLE);
          vecC.Subtract(vecC, ONE_DOUBLE);
     }

     EXPECT_TRUE(vecA.Equal(ONE_DOUBLE-ONE_DOUBLE*K_MAX_LOC));
     EXPECT_TRUE(vecB.Equal(TWO_DOUBLE-ONE_DOUBLE*K_MAX_LOC));
     EXPECT_TRUE(vecC.Equal(THREE_DOUBLE-ONE_DOUBLE*K_MAX_LOC));

     // advance matrices

     vecA.Set(ONE_DOUBLE);
     vecB.Set(TWO_DOUBLE);
     vecC.Set(THREE_DOUBLE);

     for (int i = 0; i != K_MAX_LOC; ++i) {
          vecA.Times(vecA, ONE_DOUBLE);
          vecB.Times(vecB, ONE_DOUBLE);
          vecC.Times(vecC, ONE_DOUBLE);
     }

     EXPECT_TRUE(vecA.Equal(ONE_DOUBLE));
     EXPECT_TRUE(vecB.Equal(TWO_DOUBLE));
     EXPECT_TRUE(vecC.Equal(THREE_DOUBLE));

     // advance matrices

     vecA.Set(ONE_DOUBLE);
     vecB.Set(TWO_DOUBLE);
     vecC.Set(THREE_DOUBLE);

     for (int i = 0; i != K_MAX_LOC; ++i) {
          vecA.Divide(vecA, ONE_DOUBLE);
          vecB.Divide(vecB, ONE_DOUBLE);
          vecC.Divide(vecC, ONE_DOUBLE);
     }

     EXPECT_TRUE(vecA.Equal(ONE_DOUBLE));
     EXPECT_TRUE(vecB.Equal(TWO_DOUBLE));
     EXPECT_TRUE(vecC.Equal(THREE_DOUBLE));

     // advance matrices

     vecA.Set(ONE_DOUBLE);
     vecB.Set(TWO_DOUBLE);
     vecC.Set(THREE_DOUBLE);

     for (int i = 0; i != K_MAX_LOC; ++i) {
          vecA.Plus(vecA);
          vecB.Plus(vecB);
          vecC.Plus(vecC);
     }

     EXPECT_TRUE(vecA.Equal(1.0*pow(TWO_DOUBLE, K_MAX_LOC)));
     EXPECT_TRUE(vecB.Equal(2.0*pow(TWO_DOUBLE, K_MAX_LOC)));
     EXPECT_TRUE(vecC.Equal(3.0*pow(TWO_DOUBLE, K_MAX_LOC)));

     // advance matrices

     vecA.Set(ONE_DOUBLE);
     vecB.Set(TWO_DOUBLE);
     vecC.Set(THREE_DOUBLE);

     for (int i = 0; i != K_MAX_LOC; ++i) {
          vecA.Subtract(vecA);
          vecB.Subtract(vecB);
          vecC.Subtract(vecC);
     }

     EXPECT_TRUE(vecA.Equal(pgg::ZERO_DBL));
     EXPECT_TRUE(vecB.Equal(pgg::ZERO_DBL));
     EXPECT_TRUE(vecC.Equal(pgg::ZERO_DBL));

     // advance matrices

     vecA.Set(ONE_DOUBLE);
     vecB.Set(TWO_DOUBLE);
     vecC.Set(THREE_DOUBLE);

     for (int i = 0; i != K_MAX_LOC; ++i) {
          vecA.Divide(vecA);
          vecB.Divide(vecB);
          vecC.Divide(vecC);
     }

     EXPECT_TRUE(vecA.Equal(pgg::ONE_DBL));
     EXPECT_TRUE(vecB.Equal(pgg::ONE_DBL));
     EXPECT_TRUE(vecC.Equal(pgg::ONE_DBL));

     // advance matrices

     vecA.Set(ONE_DOUBLE);
     vecB.Set(TWO_DOUBLE);
     vecC.Set(THREE_DOUBLE);

     for (int i = 0; i != K_MAX_LOC; ++i) {
          vecA.Times(vecA);
          vecB.Times(vecB);
          vecB.Divide(TWO_DOUBLE);
          vecC.Times(vecC);
          vecC.Divide(THREE_DOUBLE);
     }

     EXPECT_TRUE(vecA.Equal(ONE_DOUBLE));
     EXPECT_TRUE(vecB.Equal(TWO_DOUBLE));
     EXPECT_TRUE(vecC.Equal(THREE_DOUBLE));

     // deallocate heap space

     vecA.Deallocate();
     vecB.Deallocate();
     vecC.Deallocate();

}

// END
