//=============//
// test driver //
//=============//

// sepolia

#include "../../../head/sepolia.hpp"

using pgg::sep::VectorDense;
using std::cos;
using std::sin;

// Google Test

#include "gtest/gtest.h"

//==========//
// test # 1 //
//==========//

TEST(VectorTest1, Memory)
{
     // This test is named "Memory",
     // and belongs to the "VectorTest1"

     VectorDense<double> vecA;

     // test
     // AllocatedQ()
     // DeallocatedQ()

     EXPECT_EQ(vecA.AllocatedQ(), false);
     EXPECT_EQ(vecA.DeallocatedQ(), true);
     EXPECT_EQ(vecA.DeallocatedQ(), !vecA.AllocatedQ());

     // test
     // AllocatedQ()
     // DeallocatedQ()

     vecA.Allocate(10);

     EXPECT_EQ(vecA.AllocatedQ(), true);
     EXPECT_EQ(vecA.DeallocatedQ(), false);
     EXPECT_EQ(vecA.DeallocatedQ(), !vecA.AllocatedQ());

     // test
     // AllocatedQ()
     // DeallocatedQ()

     vecA.Deallocate();

     EXPECT_EQ(vecA.AllocatedQ(), false);
     EXPECT_EQ(vecA.DeallocatedQ(), true);
     EXPECT_EQ(vecA.DeallocatedQ(), !vecA.AllocatedQ());

     // end test
}

//==========//
// test # 2 //
//==========//

TEST(VectorTest2, Memory)
{
     // This test is named "Memory",
     // and belongs to the "VectorTest2"

     VectorDense<double> vecA;

     vecA.Allocate(10);

     VectorDense<double> vecB(vecA);

     // test
     // AllocatedQ()
     // DeallocatedQ()

     EXPECT_EQ(vecB.AllocatedQ(), true);
     EXPECT_EQ(vecB.DeallocatedQ(), false);
     EXPECT_EQ(vecB.DeallocatedQ(), !vecB.AllocatedQ());

     // test
     // AllocatedQ()
     // DeallocatedQ()

     vecB.Deallocate();
     vecB.Allocate(10);

     EXPECT_EQ(vecB.AllocatedQ(), true);
     EXPECT_EQ(vecB.DeallocatedQ(), false);
     EXPECT_EQ(vecB.DeallocatedQ(), !vecB.AllocatedQ());

     // test
     // AllocatedQ()
     // DeallocatedQ()

     vecB.Deallocate();

     EXPECT_EQ(vecB.AllocatedQ(), false);
     EXPECT_EQ(vecB.DeallocatedQ(), true);
     EXPECT_EQ(vecB.DeallocatedQ(), !vecB.AllocatedQ());

     // test
     // AllocatedQ()
     // DeallocatedQ()

     vecB.Allocate(10);
     vecB.Allocate(11);
     vecB.Allocate(12);

     EXPECT_EQ(vecB.AllocatedQ(), true);
     EXPECT_EQ(vecB.DeallocatedQ(), false);
     EXPECT_EQ(vecB.DeallocatedQ(), !vecB.AllocatedQ());

     // test
     // AllocatedQ()
     // DeallocatedQ()

     vecB.Deallocate();
     vecB.Deallocate();
     vecB.Deallocate();

     EXPECT_EQ(vecB.AllocatedQ(), false);
     EXPECT_EQ(vecB.DeallocatedQ(), true);
     EXPECT_EQ(vecB.DeallocatedQ(), !vecB.AllocatedQ());

     // end test
}

//==========//
// test # 3 //
//==========//

TEST(VectorTest3, Memory)
{
     // This test is named "Memory",
     // and belongs to the "VectorTest3"

     VectorDense<double> vecA;

     vecA.Allocate(100);

     VectorDense<double> vecB(vecA);

     // test
     // AllocatedQ()
     // DeallocatedQ()

     EXPECT_TRUE(vecB.AllocatedQ());
     EXPECT_FALSE(vecB.DeallocatedQ());

     // test
     // AllocatedQ()
     // DeallocatedQ()

     vecB.Deallocate();

     EXPECT_FALSE(vecB.AllocatedQ());
     EXPECT_TRUE(vecB.DeallocatedQ());

     // test
     // AllocatedQ()
     // DeallocatedQ()

     vecB.Allocate(1);
     vecB.Allocate(1);
     vecB.Allocate(200);

     EXPECT_TRUE(vecB.AllocatedQ());
     EXPECT_FALSE(vecB.DeallocatedQ());

     // end test
}

//==========//
// test # 4 //
//==========//

TEST(VectorTest4, Memory)
{
     // This test is named "Memory",
     // and belongs to the "VectorTest4"

     VectorDense<double> vecA;
     VectorDense<double> vecB;

     // local scope

     {
          VectorDense<double> vecA;

          vecA.Allocate(100);
          vecB.Allocate(8);

          // test
          // AllocatedQ()
          // DeallocatedQ()

          EXPECT_TRUE(vecA.AllocatedQ());
          EXPECT_FALSE(vecA.DeallocatedQ());

          EXPECT_TRUE(vecB.AllocatedQ());
          EXPECT_FALSE(vecB.DeallocatedQ());
     }

     // test
     // AllocatedQ()
     // DeallocatedQ()

     EXPECT_FALSE(vecA.AllocatedQ());
     EXPECT_TRUE(vecA.DeallocatedQ());

     EXPECT_TRUE(vecB.AllocatedQ());
     EXPECT_FALSE(vecB.DeallocatedQ());

     vecB.Deallocate();

     EXPECT_FALSE(vecB.AllocatedQ());
     EXPECT_TRUE(vecB.DeallocatedQ());

     // end test
}

// END
