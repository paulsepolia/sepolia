
//=============//
// test driver //
//=============//

#include <iostream>
#include <ctime>
#include <cmath>
#include <iomanip>
#include <complex>

using std::cout;
using std::cin;
using std::endl;
using std::abs;
using std::pow;
using std::boolalpha;
using std::setprecision;
using std::fixed;
using std::showpos;
using std::showpoint;

#include "../../../head/sepolia.hpp"

using pgg::sep::MatrixDense;
using pgg::sep::VectorDense;

// the main code

int main()
{
     // local parameters

     const auto DIM = 4 *static_cast<pgg::MAI_MAX>(pow(10.0, 3.0));
     const auto K_MAX = 4;

     MatrixDense<double> matA;
     MatrixDense<double> matO;
     MatrixDense<std::complex<double>> matB;
     MatrixDense<double> eigenvectorsA;
     MatrixDense<double> eigenvectorsB;
     MatrixDense<double> eigenvectorsC;
     MatrixDense<double> eigenvectorsD;
     MatrixDense<std::complex<double>> eigenvectorsE;
     VectorDense<double> eigenvaluesA;
     VectorDense<double> eigenvaluesB;
     VectorDense<double> eigenvaluesC;
     VectorDense<double> eigenvaluesD;
     VectorDense<double> eigenvaluesE;

     pgg::MAI i;
     pgg::MAI j;

     clock_t t1;
     clock_t t2;

     cout << boolalpha;
     cout << fixed;
     cout << showpos;
     cout << showpoint;
     cout << setprecision(10);

     // main test loop

     for (int k = 0; k != K_MAX; ++k) {
          cout << "-------------------------------------------------->> " << k << endl;

          // allocate

          cout << " --> 001 --> allocate matrices" << endl;

          matA.Allocate(DIM);
          matO.Allocate(DIM);
          matB.Allocate(DIM);

          eigenvectorsA.Allocate(DIM);
          eigenvectorsB.Allocate(DIM);
          eigenvectorsC.Allocate(DIM);
          eigenvectorsD.Allocate(DIM);
          eigenvectorsE.Allocate(DIM);

          eigenvaluesA.Allocate(DIM);
          eigenvaluesB.Allocate(DIM);
          eigenvaluesC.Allocate(DIM);
          eigenvaluesD.Allocate(DIM);
          eigenvaluesE.Allocate(DIM);

          // build

          cout << " --> 002 --> build matrices" << endl;

          // # 1

          for (i = 0; i != DIM; ++i) {
               for (j = 0; j != DIM; ++j) {
                    if (abs(static_cast<int>(i-j)) < 20) {
                         matO.SetElement(i, j, static_cast<double>(i+j));
                    } else if (abs(static_cast<int>(i-j)) >= 20) {
                         matO.SetElement(i, j, static_cast<double>(0.0));
                    }
               }
          }

          // # 2

          for (i = 0; i != DIM; ++i) {
               for (j = 0; j != DIM; ++j) {
                    if (abs(static_cast<int>(i-j)) < 20) {
                         matB.SetElement(i, j, static_cast<std::complex<double>>(i+j));
                    } else if (abs(static_cast<int>(i-j)) >= 20) {
                         matB.SetElement(i, j, static_cast<std::complex<double>>(0.0));
                    }
               }
          }

          //=============//
          // eigensystem //
          //=============//

          // DSYEVD

          matA.Set(matO);

          cout << " --> 003 --> eigensystem of matA using DSYEVD" << endl;

          t1 = clock();

          matA.EigensystemDSYEVD(matA, eigenvaluesA);

          eigenvectorsA.Set(matA);

          t2 = clock();

          cout << " --> time used = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

          cout << " --> 004 --> eigensystem of matA using DSYEVR" << endl;

          // DSYEVR

          matA.Set(matO);

          t1 = clock();

          matA.EigensystemDSYEVR(matA, eigenvaluesB);

          eigenvectorsB.Set(matA);

          t2 = clock();

          cout << " --> time used = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

          // DSYEVX

          matA.Set(matO);

          cout << " --> 005 --> eigensystem of matA using DSYEVX" << endl;

          t1 = clock();

          matA.EigensystemDSYEVX(matA, eigenvaluesC);

          eigenvectorsC.Set(matA);

          t2 = clock();

          cout << " --> time used = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

          // DSYEV

          matA.Set(matO);

          cout << " --> 006 --> eigensystem of matA using DSYEV" << endl;

          t1 = clock();

          matA.EigensystemDSYEV(matA, eigenvaluesD);

          eigenvectorsD.Set(matA);

          t2 = clock();

          cout << " --> time used = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

          // ZHEEV

          cout << " --> 007 --> eigensystem of matB using ZHEEV" << endl;

          t1 = clock();

          matB.EigensystemZHEEV(eigenvectorsE, eigenvaluesE);

          t2 = clock();

          cout << " --> time used = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

          // test for equality of eigenvalues

          cout << " --> 008 --> test for eigenvalues" << endl;

          cout << " --> " << eigenvaluesA.Equal(eigenvaluesB, std::pow(10.0, -8.0)) << endl;
          cout << " --> " << eigenvaluesA.Equal(eigenvaluesC, std::pow(10.0, -8.0)) << endl;
          cout << " --> " << eigenvaluesA.Equal(eigenvaluesD, std::pow(10.0, -8.0)) << endl;
          cout << " --> " << eigenvaluesA.Equal(eigenvaluesE, std::pow(10.0, -8.0)) << endl;

          eigenvectorsA.Map(abs);
          eigenvectorsB.Map(abs);
          eigenvectorsC.Map(abs);
          eigenvectorsD.Map(abs);

          cout << " --> " << eigenvectorsA.Equal(eigenvectorsB, std::pow(10.0, 0.0)) << endl;
          cout << " --> " << eigenvectorsA.Equal(eigenvectorsC, std::pow(10.0, -3.0)) << endl;
          cout << " --> " << eigenvectorsA.Equal(eigenvectorsD, std::pow(10.0, -3.0)) << endl;

          //=============//
          // eigenvalues //
          //=============//

          // DSYEVD

          cout << " --> 009 --> eigenvalues of matA using DSYEVD" << endl;

          matA.Set(matO);

          t1 = clock();

          matA.EigenvaluesDSYEVD(eigenvaluesA);

          t2 = clock();

          cout << " --> time used = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

          cout << " --> 010 --> eigenvalues of matA using DSYEVR" << endl;

          // DSYEVR

          t1 = clock();

          matA.EigenvaluesDSYEVR(eigenvaluesB);

          t2 = clock();

          cout << " --> time used = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

          // DSYEVX

          cout << " --> 011 --> eigenvalues of matA using DSYEVX" << endl;

          t1 = clock();

          matA.EigenvaluesDSYEVX(eigenvaluesC);

          t2 = clock();

          cout << " --> time used = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

          // DSYEV

          cout << " --> 012 --> eigenvalues of matA using DSYEV" << endl;

          t1 = clock();

          matA.EigenvaluesDSYEV(eigenvaluesD);

          t2 = clock();

          cout << " --> time used = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

          // ZHEEV

          cout << " --> 013 --> eigenvalues of matB using ZHEEV" << endl;

          t1 = clock();

          matB.EigenvaluesZHEEV(eigenvaluesE);

          t2 = clock();

          cout << " --> time used = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

          // test for equality of eigenvalues

          cout << " --> 014 --> test for eigenvalues" << endl;

          cout << " --> " << eigenvaluesA.Equal(eigenvaluesB, std::pow(10.0, -4.0)) << endl;
          cout << " --> " << eigenvaluesA.Equal(eigenvaluesC, std::pow(10.0, -4.0)) << endl;
          cout << " --> " << eigenvaluesA.Equal(eigenvaluesD, std::pow(10.0, -4.0)) << endl;
          cout << " --> " << eigenvaluesA.Equal(eigenvaluesE, std::pow(10.0, -4.0)) << endl;

          //==============//
          // eigenvectors //
          //==============//

          // DSYEVD

          cout << " --> 015 --> eigenvectors of matA using DSYEVD" << endl;

          t1 = clock();

          matA.EigenvectorsDSYEVD(matA);

          eigenvectorsA.Set(matA);

          t2 = clock();

          cout << " --> time used = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

          cout << " --> 016 --> eigenvectors of matA using DSYEVR" << endl;

          // DSYEVR

          matA.Set(matO);

          t1 = clock();

          matA.EigenvectorsDSYEVR(matA);

          eigenvectorsB.Set(matA);

          t2 = clock();

          cout << " --> time used = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

          // DSYEVX

          matA.Set(matO);

          cout << " --> 017 --> eigenvectors of matA using DSYEVX" << endl;

          t1 = clock();

          matA.EigenvectorsDSYEVX(matA);

          eigenvectorsC.Set(matA);

          t2 = clock();

          cout << " --> time used = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

          // DSYEV

          matA.Set(matO);

          cout << " --> 018 --> eigenvectors of matA using DSYEV" << endl;

          t1 = clock();

          matA.EigenvectorsDSYEV(matA);

          eigenvectorsD.Set(matA);

          t2 = clock();

          cout << " --> time used = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

          // ZHEEV

          cout << " --> 019 --> eigenvectors of matB using ZHEEV" << endl;

          t1 = clock();

          matB.EigenvectorsZHEEV(eigenvectorsE);

          eigenvectorsA.Map(abs);
          eigenvectorsB.Map(abs);
          eigenvectorsC.Map(abs);
          eigenvectorsD.Map(abs);

          cout << " --> " << eigenvectorsA.Equal(eigenvectorsB, std::pow(10.0, 0.0)) << endl;
          cout << " --> " << eigenvectorsA.Equal(eigenvectorsC, std::pow(10.0, -3.0)) << endl;
          cout << " --> " << eigenvectorsA.Equal(eigenvectorsD, std::pow(10.0, -3.0)) << endl;

          // restart
     }

     return 0;
}

// END
