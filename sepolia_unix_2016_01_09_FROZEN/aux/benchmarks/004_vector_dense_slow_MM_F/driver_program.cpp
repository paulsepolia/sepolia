
//=============//
// test driver //
//=============//

#include <iostream>
#include <iomanip>
#include <complex>

using std::cout;
using std::cin;
using std::endl;
using std::fixed;
using std::showpos;
using std::setprecision;
using std::showpoint;
using std::boolalpha;
using std::complex;

#include "../../../head/sepolia.hpp"

using pgg::sep::VectorDense;

using std::cos;
using std::sin;

// the main code

int main()
{
     // local parameters

     const auto DIM = 64 *static_cast<pgg::VEI_MAX>(pow(10.0, 6.0));
     const auto K_MAX = 4;
     const auto K_MAX_LOC = 10;
     const auto ONE_DOUBLE = 1.0;
     const auto TWO_DOUBLE = 2.0;
     const auto THREE_DOUBLE = 3.0;

     VectorDense<double> vecA;
     VectorDense<double> vecB;
     VectorDense<double> vecC;

     cout << fixed;
     cout << setprecision(20);
     cout << showpoint;
     cout << showpos;
     cout << boolalpha;

     // main test loop

     for (int k = 0; k != K_MAX; ++k) {
          cout << "-------------------------------------------------->> " << k << endl;

          // allocate heap space

          cout << " --> 0001 --> allocate heap space" << endl;

          vecA.Allocate(DIM);
          vecB.Allocate(DIM);
          vecC.Allocate(DIM);

          // build vectors

          cout << " --> 0002 --> build vectors" << endl;

          vecA = ONE_DOUBLE;
          vecB = TWO_DOUBLE;
          vecC = THREE_DOUBLE;

          // test equality

          cout << " --> 0003 --> test equality" << endl;

          cout << " --> true --> " << (vecA == ONE_DOUBLE) << endl;
          cout << " --> true --> " << (vecB == TWO_DOUBLE) << endl;
          cout << " --> true --> " << (vecC == THREE_DOUBLE) << endl;

          // add vectors

          cout << " --> 0004 --> add vectors several times --> slow way" << endl;

          for (int i = 0; i != K_MAX_LOC; ++i) {
               vecC = vecA + vecB;
          }

          cout << " --> true --> " << (vecA == ONE_DOUBLE) << endl;
          cout << " --> true --> " << (vecB == TWO_DOUBLE) << endl;
          cout << " --> true --> " << (vecC == THREE_DOUBLE) << endl;

          // subtract vectors

          cout << " --> 0005 --> subtract vectors several times --> slow way" << endl;

          for (int i = 0; i != K_MAX_LOC; ++i) {
               vecC = vecA - vecB;
          }

          cout << " --> true --> " << (vecA == ONE_DOUBLE) << endl;
          cout << " --> true --> " << (vecB == TWO_DOUBLE) << endl;
          cout << " --> true --> " << (vecC == -ONE_DOUBLE) << endl;

          // multiply vectors

          cout << " --> 0006 --> multiply vectors several times --> slow way" << endl;

          for (int i = 0; i != K_MAX_LOC; ++i) {
               vecC = vecA * vecB;
          }

          cout << " --> true --> " << (vecA == ONE_DOUBLE) << endl;
          cout << " --> true --> " << (vecB == TWO_DOUBLE) << endl;
          cout << " --> true --> " << (vecC == TWO_DOUBLE) << endl;

          // divide vectors

          cout << " --> 0007 --> divide vectors several times --> slow way" << endl;

          for (int i = 0; i != K_MAX_LOC; ++i) {
               vecC = vecA / vecB;
          }

          cout << " --> true --> " << (vecA == ONE_DOUBLE) << endl;
          cout << " --> true --> " << (vecB == TWO_DOUBLE) << endl;
          cout << " --> true --> " << (vecC == (ONE_DOUBLE/TWO_DOUBLE)) << endl;

          // swap vectors

          cout << " --> 0008 --> swap vectors several times --> slow way" << endl;

          for (int i = 0; i != K_MAX_LOC; ++i) {

               vecA = ONE_DOUBLE;
               vecB = TWO_DOUBLE;
               vecC = THREE_DOUBLE;

               vecA = vecB;
               vecB = vecC;
               vecC = ONE_DOUBLE;
          }

          cout << " --> true --> " << (vecA == TWO_DOUBLE) << endl;
          cout << " --> true --> " << (vecB == THREE_DOUBLE) << endl;
          cout << " --> true --> " << (vecC == ONE_DOUBLE) << endl;

          // advance vectors

          cout << " --> 0009 --> advance vectors several times --> slow way" << endl;

          vecA = ONE_DOUBLE;
          vecB = TWO_DOUBLE;
          vecC = THREE_DOUBLE;

          for (int i = 0; i != K_MAX_LOC; ++i) {
               vecA = vecA + ONE_DOUBLE;
               vecB = vecB + ONE_DOUBLE;
               vecC = vecC + ONE_DOUBLE;
          }

          cout << " --> true --> " << (vecA == (ONE_DOUBLE+ONE_DOUBLE*K_MAX_LOC)) << endl;
          cout << " --> true --> " << (vecB == (TWO_DOUBLE+ONE_DOUBLE*K_MAX_LOC)) << endl;
          cout << " --> true --> " << (vecC == (THREE_DOUBLE+ONE_DOUBLE*K_MAX_LOC)) << endl;

          // advance vectors

          cout << " --> 0010 --> advance vectors several times --> slow way" << endl;

          vecA = ONE_DOUBLE;
          vecB = TWO_DOUBLE;
          vecC = THREE_DOUBLE;

          for (int i = 0; i != K_MAX_LOC; ++i) {
               vecA = vecA - ONE_DOUBLE;
               vecB = vecB - ONE_DOUBLE;
               vecC = vecC - ONE_DOUBLE;
          }

          cout << " --> true --> " << (vecA == (ONE_DOUBLE-ONE_DOUBLE*K_MAX_LOC)) << endl;
          cout << " --> true --> " << (vecB == (TWO_DOUBLE-ONE_DOUBLE*K_MAX_LOC)) << endl;
          cout << " --> true --> " << (vecC == (THREE_DOUBLE-ONE_DOUBLE*K_MAX_LOC)) << endl;

          // advance vectors

          cout << " --> 0011 --> advance vectors several times --> slow way" << endl;

          vecA = ONE_DOUBLE;
          vecB = TWO_DOUBLE;
          vecC = THREE_DOUBLE;

          for (int i = 0; i != K_MAX_LOC; ++i) {
               vecA = vecA * ONE_DOUBLE;
               vecB = vecB * ONE_DOUBLE;
               vecC = vecC * ONE_DOUBLE;
          }

          cout << " --> true --> " << (vecA == ONE_DOUBLE) << endl;
          cout << " --> true --> " << (vecB == TWO_DOUBLE) << endl;
          cout << " --> true --> " << (vecC == THREE_DOUBLE) << endl;

          // advance vectors

          cout << " --> 0012 --> advance vectors several times --> slow way" << endl;

          vecA = ONE_DOUBLE;
          vecB = TWO_DOUBLE;
          vecC = THREE_DOUBLE;

          for (int i = 0; i != K_MAX_LOC; ++i) {
               vecA = vecA / ONE_DOUBLE;
               vecB = vecB / ONE_DOUBLE;
               vecC = vecC / ONE_DOUBLE;
          }

          cout << " --> true --> " << (vecA == ONE_DOUBLE) << endl;
          cout << " --> true --> " << (vecB == TWO_DOUBLE) << endl;
          cout << " --> true --> " << (vecC ==THREE_DOUBLE) << endl;

          // advance vectors

          cout << " --> 0013 --> advance vectors several times --> slow way" << endl;

          vecA = ONE_DOUBLE;
          vecB = TWO_DOUBLE;
          vecC = THREE_DOUBLE;

          for (int i = 0; i != K_MAX_LOC; ++i) {
               vecA = vecA + vecA;
               vecB = vecB + vecB;
               vecC = vecC + vecC;
          }

          cout << " --> true --> " << (vecA == 1.0*pow(TWO_DOUBLE, 10.0)) << endl;
          cout << " --> true --> " << (vecB == 2.0*pow(TWO_DOUBLE, 10.0)) << endl;
          cout << " --> true --> " << (vecC == 3.0*pow(TWO_DOUBLE, 10.0)) << endl;

          // advance vectors

          cout << " --> 0014 --> advance vectors several times --> slow way" << endl;

          vecA = ONE_DOUBLE;
          vecB = TWO_DOUBLE;
          vecC = THREE_DOUBLE;

          for (int i = 0; i != K_MAX_LOC; ++i) {
               vecA = vecA - vecA;
               vecB = vecB - vecB;
               vecC = vecC - vecC;
          }

          cout << " --> true --> " << (vecA == pgg::ZERO_DBL) << endl;
          cout << " --> true --> " << (vecB == pgg::ZERO_DBL) << endl;
          cout << " --> true --> " << (vecC == pgg::ZERO_DBL) << endl;

          // advance vectors

          cout << " --> 0015 --> advance vectors several times --> slow way" << endl;

          vecA = ONE_DOUBLE;
          vecB = TWO_DOUBLE;
          vecC = THREE_DOUBLE;

          for (int i = 0; i != K_MAX_LOC; ++i) {
               vecA = vecA / vecA;
               vecB = vecB / vecB;
               vecC = vecC / vecC;
          }

          cout << " --> true --> " << (vecA == pgg::ONE_DBL) << endl;
          cout << " --> true --> " << (vecB == pgg::ONE_DBL) << endl;
          cout << " --> true --> " << (vecC == pgg::ONE_DBL) << endl;

          // advance vectors

          cout << " --> 0016 --> advance vectors several times --> slow way" << endl;

          vecA = ONE_DOUBLE;
          vecB = TWO_DOUBLE;
          vecC = THREE_DOUBLE;

          for (int i = 0; i != K_MAX_LOC; ++i) {
               vecA = vecA * vecA;
               vecB = vecB * vecB;
               vecB = vecB / TWO_DOUBLE;
               vecC = vecC * vecC;
               vecC = vecC / THREE_DOUBLE;
          }

          cout << " --> true --> " << (vecA == ONE_DOUBLE) << endl;
          cout << " --> true --> " << (vecB == TWO_DOUBLE) << endl;
          cout << " --> true --> " << (vecC == THREE_DOUBLE) << endl;

          // deallocate heap space

          cout << " --> 00xx --> deallocate heap space" << endl;

          vecA.Deallocate();
          vecB.Deallocate();
          vecC.Deallocate();

          // restart
     }

     return 0;
}

// END
