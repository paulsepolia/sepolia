
//=============//
// test driver //
//=============//

#include <iostream>
#include <iomanip>
#include <complex>

using std::cout;
using std::cin;
using std::endl;
using std::fixed;
using std::showpos;
using std::setprecision;
using std::showpoint;
using std::boolalpha;
using std::complex;

#include "../../../head/sepolia.hpp"

using pgg::sep::MatrixDense;
using std::cos;
using std::sin;

// the main code

int main()
{
     // local parameters

     const auto DIM = 10 *static_cast<pgg::MAI_MAX>(pow(10.0, 3.0));
     const auto K_MAX = 4;
     const auto K_MAX_LOC = 10;
     const auto ONE_DOUBLE = 1.0;
     const auto TWO_DOUBLE = 2.0;
     const auto THREE_DOUBLE = 3.0;

     MatrixDense<double> matA;
     MatrixDense<double> matB;
     MatrixDense<double> matC;

     cout << fixed;
     cout << setprecision(20);
     cout << showpoint;
     cout << showpos;
     cout << boolalpha;

     // main test loop

     for (int k = 0; k != K_MAX; ++k) {
          cout << "-------------------------------------------------->> " << k << endl;

          // allocate heap space

          cout << " --> 0001 --> allocate heap space" << endl;

          matA.Allocate(DIM);
          matB.Allocate(DIM);
          matC.Allocate(DIM);

          // build matrices

          cout << " --> 0002 --> build matrices" << endl;

          matA.Set(ONE_DOUBLE);
          matB.Set(TWO_DOUBLE);
          matC.Set(THREE_DOUBLE);

          // test equality

          cout << " --> 0003 --> test equality" << endl;

          cout << " --> true --> " << matA.Equal(ONE_DOUBLE) << endl;
          cout << " --> true --> " << matB.Equal(TWO_DOUBLE) << endl;
          cout << " --> true --> " << matC.Equal(THREE_DOUBLE) << endl;

          // add matrices

          cout << " --> 0004 --> add matrices several times --> fast way" << endl;

          for (int i = 0; i != K_MAX_LOC; ++i) {
               matC.Plus(matA, matB);
          }

          cout << " --> true --> " << matA.Equal(ONE_DOUBLE) << endl;
          cout << " --> true --> " << matB.Equal(TWO_DOUBLE) << endl;
          cout << " --> true --> " << matC.Equal(THREE_DOUBLE) << endl;

          // subtract matrices

          cout << " --> 0005 --> subtract matrices several times --> fast way" << endl;

          for (int i = 0; i != K_MAX_LOC; ++i) {
               matC.Subtract(matA, matB);
          }

          cout << " --> true --> " << matA.Equal(ONE_DOUBLE) << endl;
          cout << " --> true --> " << matB.Equal(TWO_DOUBLE) << endl;
          cout << " --> true --> " << matC.Equal(-ONE_DOUBLE) << endl;

          // multiply matrices

          cout << " --> 0006 --> multiply matrices several times --> fast way" << endl;

          for (int i = 0; i != K_MAX_LOC; ++i) {
               matC.Times(matA, matB);
          }

          cout << " --> true --> " << matA.Equal(ONE_DOUBLE) << endl;
          cout << " --> true --> " << matB.Equal(TWO_DOUBLE) << endl;
          cout << " --> true --> " << matC.Equal(TWO_DOUBLE) << endl;

          // divide matrices

          cout << " --> 0007 --> divide matrices several times --> fast way" << endl;

          for (int i = 0; i != K_MAX_LOC; ++i) {
               matC.Divide(matA, matB);
          }

          cout << " --> true --> " << matA.Equal(ONE_DOUBLE) << endl;
          cout << " --> true --> " << matB.Equal(TWO_DOUBLE) << endl;
          cout << " --> true --> " << matC.Equal(ONE_DOUBLE/TWO_DOUBLE) << endl;

          // swap matrices

          cout << " --> 0008 --> swap matrices several times --> fast way" << endl;

          for (int i = 0; i != K_MAX_LOC; ++i) {

               matA.Set(ONE_DOUBLE);
               matB.Set(TWO_DOUBLE);
               matC.Set(THREE_DOUBLE);

               matA.Set(matB);
               matB.Set(matC);
               matC.Set(ONE_DOUBLE);
          }

          cout << " --> true --> " << matA.Equal(TWO_DOUBLE) << endl;
          cout << " --> true --> " << matB.Equal(THREE_DOUBLE) << endl;
          cout << " --> true --> " << matC.Equal(ONE_DOUBLE) << endl;

          // advance matrices

          cout << " --> 0009 --> advance matrices several times --> fast way" << endl;

          matA.Set(ONE_DOUBLE);
          matB.Set(TWO_DOUBLE);
          matC.Set(THREE_DOUBLE);

          for (int i = 0; i != K_MAX_LOC; ++i) {
               matA.Plus(matA, ONE_DOUBLE);
               matB.Plus(matB, ONE_DOUBLE);
               matC.Plus(matC, ONE_DOUBLE);
          }

          cout << " --> true --> " << matA.Equal(ONE_DOUBLE+ONE_DOUBLE*K_MAX_LOC) << endl;
          cout << " --> true --> " << matB.Equal(TWO_DOUBLE+ONE_DOUBLE*K_MAX_LOC) << endl;
          cout << " --> true --> " << matC.Equal(THREE_DOUBLE+ONE_DOUBLE*K_MAX_LOC) << endl;

          // advance matrices

          cout << " --> 0010 --> advance matrices several times --> fast way" << endl;

          matA.Set(ONE_DOUBLE);
          matB.Set(TWO_DOUBLE);
          matC.Set(THREE_DOUBLE);

          for (int i = 0; i != K_MAX_LOC; ++i) {
               matA.Subtract(matA, ONE_DOUBLE);
               matB.Subtract(matB, ONE_DOUBLE);
               matC.Subtract(matC, ONE_DOUBLE);
          }

          cout << " --> true --> " << matA.Equal(ONE_DOUBLE-ONE_DOUBLE*K_MAX_LOC) << endl;
          cout << " --> true --> " << matB.Equal(TWO_DOUBLE-ONE_DOUBLE*K_MAX_LOC) << endl;
          cout << " --> true --> " << matC.Equal(THREE_DOUBLE-ONE_DOUBLE*K_MAX_LOC) << endl;

          // advance matrices

          cout << " --> 0011 --> advance matrices several times --> fast way" << endl;

          matA.Set(ONE_DOUBLE);
          matB.Set(TWO_DOUBLE);
          matC.Set(THREE_DOUBLE);

          for (int i = 0; i != K_MAX_LOC; ++i) {
               matA.Times(matA, ONE_DOUBLE);
               matB.Times(matB, ONE_DOUBLE);
               matC.Times(matC, ONE_DOUBLE);
          }

          cout << " --> true --> " << matA.Equal(ONE_DOUBLE) << endl;
          cout << " --> true --> " << matB.Equal(TWO_DOUBLE) << endl;
          cout << " --> true --> " << matC.Equal(THREE_DOUBLE) << endl;

          // advance matrices

          cout << " --> 0012 --> advance matrices several times --> fast way" << endl;

          matA.Set(ONE_DOUBLE);
          matB.Set(TWO_DOUBLE);
          matC.Set(THREE_DOUBLE);

          for (int i = 0; i != K_MAX_LOC; ++i) {
               matA.Divide(matA, ONE_DOUBLE);
               matB.Divide(matB, ONE_DOUBLE);
               matC.Divide(matC, ONE_DOUBLE);
          }

          cout << " --> true --> " << matA.Equal(ONE_DOUBLE) << endl;
          cout << " --> true --> " << matB.Equal(TWO_DOUBLE) << endl;
          cout << " --> true --> " << matC.Equal(THREE_DOUBLE) << endl;

          // advance matrices

          cout << " --> 0013 --> advance matrices several times --> fast way" << endl;

          matA.Set(ONE_DOUBLE);
          matB.Set(TWO_DOUBLE);
          matC.Set(THREE_DOUBLE);

          for (int i = 0; i != K_MAX_LOC; ++i) {
               matA.Plus(matA);
               matB.Plus(matB);
               matC.Plus(matC);
          }

          cout << " --> true --> " << matA.Equal(1.0*pow(TWO_DOUBLE, 10.0)) << endl;
          cout << " --> true --> " << matB.Equal(2.0*pow(TWO_DOUBLE, 10.0)) << endl;
          cout << " --> true --> " << matC.Equal(3.0*pow(TWO_DOUBLE, 10.0)) << endl;

          // advance matrices

          cout << " --> 0014 --> advance matrices several times --> fast way" << endl;

          matA.Set(ONE_DOUBLE);
          matB.Set(TWO_DOUBLE);
          matC.Set(THREE_DOUBLE);

          for (int i = 0; i != K_MAX_LOC; ++i) {
               matA.Subtract(matA);
               matB.Subtract(matB);
               matC.Subtract(matC);
          }

          cout << " --> true --> " << matA.Equal(pgg::ZERO_DBL) << endl;
          cout << " --> true --> " << matB.Equal(pgg::ZERO_DBL) << endl;
          cout << " --> true --> " << matC.Equal(pgg::ZERO_DBL) << endl;

          // advance matrices

          cout << " --> 0015 --> advance matrices several times --> fast way" << endl;

          matA.Set(ONE_DOUBLE);
          matB.Set(TWO_DOUBLE);
          matC.Set(THREE_DOUBLE);

          for (int i = 0; i != K_MAX_LOC; ++i) {
               matA.Divide(matA);
               matB.Divide(matB);
               matC.Divide(matC);
          }

          cout << " --> true --> " << matA.Equal(pgg::ONE_DBL) << endl;
          cout << " --> true --> " << matB.Equal(pgg::ONE_DBL) << endl;
          cout << " --> true --> " << matC.Equal(pgg::ONE_DBL) << endl;

          // advance matrices

          cout << " --> 0016 --> advance matrices several times --> fast way" << endl;

          matA.Set(ONE_DOUBLE);
          matB.Set(TWO_DOUBLE);
          matC.Set(THREE_DOUBLE);

          for (int i = 0; i != K_MAX_LOC; ++i) {
               matA.Times(matA);
               matB.Times(matB);
               matB.Divide(TWO_DOUBLE);
               matC.Times(matC);
               matC.Divide(THREE_DOUBLE);
          }

          cout << " --> true --> " << matA.Equal(ONE_DOUBLE) << endl;
          cout << " --> true --> " << matB.Equal(TWO_DOUBLE) << endl;
          cout << " --> true --> " << matC.Equal(THREE_DOUBLE) << endl;

          // deallocate heap space

          cout << " --> 00xx --> deallocate heap space" << endl;

          matA.Deallocate();
          matB.Deallocate();
          matC.Deallocate();

          // restart
     }

     return 0;
}

// END
