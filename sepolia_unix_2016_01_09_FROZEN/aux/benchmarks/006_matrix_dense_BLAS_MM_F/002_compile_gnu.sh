#!/bin/bash

  # 1. compile

  g++-5  -c                  \
	    -O3                 \
         -Wall               \
         -std=gnu++14        \
	    -fopenmp            \
         driver_program.cpp

  # 2. link

  g++-5  driver_program.o                  \
	    -static                           \
	    -fopenmp                          \
         /opt/gsl/116/gnu_492/lib/libgsl.a \
         /opt/mkl/libs_pgg/libmkl_core_thread_lp64_gnu_2015_pgg.a \
         -ldl \
         -o x_gnu_521

  # 3. clean objects

  rm  driver_program.o

  # 4. exit

