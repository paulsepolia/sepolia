
//=============//
// test driver //
//=============//

#include <iostream>
#include <ctime>
#include <cmath>
#include <iomanip>
#include <complex>

using std::cout;
using std::cin;
using std::endl;
using std::abs;
using std::pow;
using std::boolalpha;
using std::setprecision;
using std::fixed;
using std::showpos;
using std::showpoint;
using std::complex;

#include "../../../head/sepolia.hpp"

using pgg::sep::MatrixDense;
using pgg::sep::VectorDense;

// the main code

int main()
{
     // local parameters

     const auto DIM = 1 *static_cast<pgg::MAI_MAX>(pow(10.0, 3.0));
     const auto K_MAX = 4;

     MatrixDense<complex<double>> matA;

     clock_t t1;
     clock_t t2;

     cout << boolalpha;
     cout << fixed;
     cout << showpos;
     cout << showpoint;
     cout << setprecision(10);

     // main test loop

     for (int k = 0; k != K_MAX; ++k) {
          cout << "-------------------------------------------------->> " << k << endl;

          // allocate

          cout << " --> 001 --> allocate matrices" << endl;

          matA.Allocate(DIM);

          // build

          const complex<double> a1 = {1.0, 2.0};

          matA.Set(a1);

          // dot # 1

          t1 = clock();

          matA.Dot(matA, matA);

          t2 = clock();

          cout << " --> time used = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

          cout << " --> matA(0,0) = " << matA(0,0) << endl;
          cout << " --> matA(1,1) = " << matA(1,1) << endl;
          cout << " --> matA(1,2) = " << matA(1,2) << endl;

          // dot # 2

          matA.Set(a1);

          t1 = clock();

          matA.Dot(matA);

          t2 = clock();

          cout << " --> time used = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

          cout << " --> matA(0,0) = " << matA(0,0) << endl;
          cout << " --> matA(1,1) = " << matA(1,1) << endl;
          cout << " --> matA(1,2) = " << matA(1,2) << endl;

          // dot # 3

          matA.Set(a1);

          t1 = clock();

          matA.Dot();

          t2 = clock();

          cout << " --> time used = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

          cout << " --> matA(0,0) = " << matA(0,0) << endl;
          cout << " --> matA(1,1) = " << matA(1,1) << endl;
          cout << " --> matA(1,2) = " << matA(1,2) << endl;
     }

     return 0;
}

// END
