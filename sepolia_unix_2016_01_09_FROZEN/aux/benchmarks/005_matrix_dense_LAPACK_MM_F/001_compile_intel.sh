#!/bin/bash

  # 1. compile

  icpc  -c                 \
        -O3                \
        -Wall              \
        -std=c++11         \
        -openmp            \
        driver_program.cpp

  # 2. link

  icpc  driver_program.o                  \
        -static                           \
        -openmp                           \
        /opt/gsl/116/gnu_492/lib/libgsl.a \
        /opt/mkl/libs_pgg/libmkl_core_thread_lp64_intel_2015_pgg.a \
        -o x_intel

  # 3. clean

  rm  driver_program.o

  # 4. exit

