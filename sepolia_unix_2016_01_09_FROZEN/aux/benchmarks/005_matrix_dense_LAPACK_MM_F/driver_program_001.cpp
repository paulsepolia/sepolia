
//=============//
// test driver //
//=============//

#include <iostream>
#include <ctime>
#include <cmath>
#include <iomanip>

using std::cout;
using std::cin;
using std::endl;
using std::abs;
using std::pow;
using std::boolalpha;
using std::setprecision;
using std::fixed;
using std::showpos;
using std::showpoint;

#include "../../../head/sepolia.hpp"

using pgg::sep::MatrixDense;
using pgg::sep::VectorDense;

// the main code

int main()
{
     // local parameters

     const auto DIM = 1 *static_cast<pgg::MAI_MAX>(pow(10.0, 3.0));
     const auto K_MAX = 4;

     MatrixDense<double> matA;
     MatrixDense<double> eigenvectorsA;
     MatrixDense<double> eigenvectorsB;
     MatrixDense<double> eigenvectorsC;
     MatrixDense<double> eigenvectorsD;
     VectorDense<double> eigenvaluesA;
     VectorDense<double> eigenvaluesB;
     VectorDense<double> eigenvaluesC;
     VectorDense<double> eigenvaluesD;

     pgg::MAI i;
     pgg::MAI j;

     clock_t t1;
     clock_t t2;

     cout << boolalpha;
     cout << fixed;
     cout << showpos;
     cout << showpoint;
     cout << setprecision(10);

     // main test loop

     for (int k = 0; k != K_MAX; ++k) {
          cout << "-------------------------------------------------->> " << k << endl;

          // allocate

          cout << " --> 001 --> allocate matrices" << endl;

          matA.Allocate(DIM);

          eigenvectorsA.Allocate(DIM);
          eigenvectorsB.Allocate(DIM);
          eigenvectorsC.Allocate(DIM);
          eigenvectorsD.Allocate(DIM);

          eigenvaluesA.Allocate(DIM);
          eigenvaluesB.Allocate(DIM);
          eigenvaluesC.Allocate(DIM);
          eigenvaluesD.Allocate(DIM);

          // build

          cout << " --> 002 --> build matrices" << endl;

          for (i = 0; i != DIM; ++i) {
               for (j = 0; j != DIM; ++j) {
                    if (abs(static_cast<int>(i-j)) < 20) {
                         matA.SetElement(i, j, static_cast<double>(i+j));
                    } else if (abs(static_cast<int>(i-j)) >= 20) {
                         matA.SetElement(i, j, static_cast<double>(0.0));
                    }
               }
          }

          //=============//
          // eigensystem //
          //=============//

          // DSYEVD

          cout << " --> 003 --> diagonalize matA using DSYEVD" << endl;

          t1 = clock();

          matA.EigensystemDSYEVD(eigenvectorsA, eigenvaluesA);

          t2 = clock();

          cout << " --> time used = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

          cout << " --> 004 --> diagonalize matB using DSYEVR" << endl;

          // DSYEVR

          t1 = clock();

          matA.EigensystemDSYEVR(eigenvectorsB, eigenvaluesB);

          t2 = clock();

          cout << " --> time used = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

          // DSYEVX

          cout << " --> 005 --> diagonalize matC using DSYEVX" << endl;

          t1 = clock();

          matA.EigensystemDSYEVX(eigenvectorsC, eigenvaluesC);

          t2 = clock();

          cout << " --> time used = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

          // DSYEV

          cout << " --> 006 --> diagonalize matC using DSYEV" << endl;

          t1 = clock();

          matA.EigensystemDSYEV(eigenvectorsD, eigenvaluesD);

          t2 = clock();

          cout << " --> time used = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

          // test for eiquality of eigenvalues

          cout << " --> 007 --> test for eigenvalues" << endl;

          cout << " --> " << eigenvaluesA.Equal(eigenvaluesB, std::pow(10.0, -8.0)) << endl;
          cout << " --> " << eigenvaluesA.Equal(eigenvaluesC, std::pow(10.0, -8.0)) << endl;
          cout << " --> " << eigenvaluesA.Equal(eigenvaluesD, std::pow(10.0, -8.0)) << endl;

          eigenvectorsA.Map(abs);
          eigenvectorsB.Map(abs);
          eigenvectorsC.Map(abs);
          eigenvectorsD.Map(abs);

          cout << " --> " << eigenvectorsA.Equal(eigenvectorsB, std::pow(10.0, 0.0)) << endl;
          cout << " --> " << eigenvectorsA.Equal(eigenvectorsC, std::pow(10.0, -3.0)) << endl;
          cout << " --> " << eigenvectorsA.Equal(eigenvectorsD, std::pow(10.0, -3.0)) << endl;

          // test vectors

          VectorDense<double> valA;
          VectorDense<double> valB;
          VectorDense<double> valC;
          VectorDense<double> valD;

          valA.Allocate(DIM);
          valB.Allocate(DIM);
          valC.Allocate(DIM);
          valD.Allocate(DIM);

          // # 1

          cout << " --> DSYEVD - DSYEVR"  << endl;

          for(pgg::MAI i = 0; i != DIM; ++i) {
               eigenvectorsA.GetColumn(i, valA);
               eigenvectorsB.GetColumn(i, valB);

               bool tmp_bool;

               tmp_bool = valA.Equal(valB, std::pow(10.0, -8.0));

               if(!tmp_bool) {
                    cout << " --> " << i << endl;
               }
          }

          // # 2

          cout << " --> DSYEVD - DSYEVX"  << endl;

          for(pgg::MAI i = 0; i != DIM; ++i) {
               eigenvectorsA.GetColumn(i, valA);
               eigenvectorsC.GetColumn(i, valC);

               bool tmp_bool;

               tmp_bool = valA.Equal(valC, std::pow(10.0, -8.0));

               if(!tmp_bool) {
                    cout << " --> " << i << endl;
               }
          }

          // # 3

          cout << " --> DSYEVR - DSYEVX"  << endl;

          for(pgg::MAI i = 0; i != DIM; ++i) {
               eigenvectorsB.GetColumn(i, valB);
               eigenvectorsC.GetColumn(i, valC);

               bool tmp_bool;

               tmp_bool = valB.Equal(valC, std::pow(10.0, -8.0));

               if(!tmp_bool) {
                    cout << " --> " << i << endl;
               }
          }

          // # 5

          cout << " --> DSYEVD - DSYEV"  << endl;

          for(pgg::MAI i = 0; i != DIM; ++i) {
               eigenvectorsA.GetColumn(i, valA);
               eigenvectorsD.GetColumn(i, valD);

               bool tmp_bool;

               tmp_bool = valA.Equal(valD, std::pow(10.0, -8.0));

               if(!tmp_bool) {
                    cout << " --> " << i << endl;
               }
          }

          //=============//
          // eigenvalues //
          //=============//

          // DSYEVD

          cout << " --> 008 --> diagonalize matA using DSYEVD" << endl;

          t1 = clock();

          matA.EigenvaluesDSYEVD(eigenvaluesA);

          t2 = clock();

          cout << " --> time used = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

          cout << " --> 009 --> diagonalize matB using DSYEVR" << endl;

          // DSYEVR

          t1 = clock();

          matA.EigenvaluesDSYEVR(eigenvaluesB);

          t2 = clock();

          cout << " --> time used = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

          // DSYEVX

          cout << " --> 010 --> diagonalize matC using DSYEVX" << endl;

          t1 = clock();

          matA.EigenvaluesDSYEVX(eigenvaluesC);

          t2 = clock();

          cout << " --> time used = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

          // DSYEV

          cout << " --> 011 --> diagonalize matC using DSYEV" << endl;

          t1 = clock();

          matA.EigenvaluesDSYEV(eigenvaluesD);

          t2 = clock();

          cout << " --> time used = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

          // test for equality of eigenvalues

          cout << " --> 012 --> test for eigenvalues" << endl;

          cout << " --> " << eigenvaluesA.Equal(eigenvaluesB, std::pow(10.0, -4.0)) << endl;
          cout << " --> " << eigenvaluesA.Equal(eigenvaluesC, std::pow(10.0, -4.0)) << endl;
          cout << " --> " << eigenvaluesA.Equal(eigenvaluesD, std::pow(10.0, -4.0)) << endl;

          //==============//
          // eigenvectors //
          //==============//

          // DSYEVD

          cout << " --> 013 --> diagonalize matA using DSYEVD" << endl;

          t1 = clock();

          matA.EigenvectorsDSYEVD(eigenvectorsA);

          t2 = clock();

          cout << " --> time used = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

          cout << " --> 014 --> diagonalize matB using DSYEVR" << endl;

          // DSYEVR

          t1 = clock();

          matA.EigenvectorsDSYEVR(eigenvectorsB);

          t2 = clock();

          cout << " --> time used = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

          // DSYEVX

          cout << " --> 015 --> diagonalize matC using DSYEVX" << endl;

          t1 = clock();

          matA.EigenvectorsDSYEVX(eigenvectorsC);

          t2 = clock();

          cout << " --> time used = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

          // DSYEV

          cout << " --> 016 --> diagonalize matC using DSYEV" << endl;

          t1 = clock();

          matA.EigenvectorsDSYEV(eigenvectorsD);

          t2 = clock();

          cout << " --> time used = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

          eigenvectorsA.Map(abs);
          eigenvectorsB.Map(abs);
          eigenvectorsC.Map(abs);
          eigenvectorsD.Map(abs);

          cout << " --> " << eigenvectorsA.Equal(eigenvectorsB, std::pow(10.0, 0.0)) << endl;
          cout << " --> " << eigenvectorsA.Equal(eigenvectorsC, std::pow(10.0, -3.0)) << endl;
          cout << " --> " << eigenvectorsA.Equal(eigenvectorsD, std::pow(10.0, -3.0)) << endl;

          // # 1

          cout << " --> DSYEVD - DSYEVR"  << endl;

          for(pgg::MAI i = 0; i != DIM; ++i) {
               eigenvectorsA.GetColumn(i, valA);
               eigenvectorsB.GetColumn(i, valB);

               bool tmp_bool;

               tmp_bool = valA.Equal(valB, std::pow(10.0, -8.0));

               if(!tmp_bool) {
                    cout << " --> " << i << endl;
               }
          }

          // # 2

          cout << " --> DSYEVD - DSYEVX"  << endl;

          for(pgg::MAI i = 0; i != DIM; ++i) {
               eigenvectorsA.GetColumn(i, valA);
               eigenvectorsC.GetColumn(i, valC);

               bool tmp_bool;

               tmp_bool = valA.Equal(valC, std::pow(10.0, -8.0));

               if(!tmp_bool) {
                    cout << " --> " << i << endl;
               }
          }

          // # 3

          cout << " --> DSYEVR - DSYEVX"  << endl;

          for(pgg::MAI i = 0; i != DIM; ++i) {
               eigenvectorsB.GetColumn(i, valB);
               eigenvectorsC.GetColumn(i, valC);

               bool tmp_bool;

               tmp_bool = valB.Equal(valC, std::pow(10.0, -8.0));

               if(!tmp_bool) {
                    cout << " --> " << i << endl;
               }
          }

          // # 5

          cout << " --> DSYEVD - DSYEV"  << endl;

          for(pgg::MAI i = 0; i != DIM; ++i) {
               eigenvectorsA.GetColumn(i, valA);
               eigenvectorsD.GetColumn(i, valD);

               bool tmp_bool;

               tmp_bool = valA.Equal(valD, std::pow(10.0, -8.0));

               if(!tmp_bool) {
                    cout << " --> " << i << endl;
               }
          }

          // restart
     }

     return 0;
}

// END
