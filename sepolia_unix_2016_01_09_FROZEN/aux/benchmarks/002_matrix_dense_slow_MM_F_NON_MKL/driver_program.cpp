
//=============//
// test driver //
//=============//

#include <iostream>
#include <iomanip>
#include <complex>

using std::cout;
using std::cin;
using std::endl;
using std::fixed;
using std::showpos;
using std::setprecision;
using std::showpoint;
using std::boolalpha;
using std::complex;

#include "../../../head/sepolia.hpp"

using pgg::sep::MatrixDense;
using std::cos;
using std::sin;

// the main code

int main()
{
     // local parameters

     const auto DIM = 13 *static_cast<pgg::MAI_MAX>(pow(10.0, 3.0));
     const auto K_MAX = 4;
     const auto K_MAX_LOC = 10;
     const auto ONE_DOUBLE = 1.0;
     const auto TWO_DOUBLE = 2.0;
     const auto THREE_DOUBLE = 3.0;

     MatrixDense<double> matA;
     MatrixDense<double> matB;
     MatrixDense<double> matC;

     cout << fixed;
     cout << setprecision(20);
     cout << showpoint;
     cout << showpos;
     cout << boolalpha;

     // main test loop

     for (int k = 0; k != K_MAX; ++k) {
          cout << "-------------------------------------------------->> " << k << endl;

          // allocate heap space

          cout << " --> 0001 --> allocate heap space" << endl;

          matA.Allocate(DIM);
          matB.Allocate(DIM);
          matC.Allocate(DIM);

          // build matrices

          cout << " --> 0002 --> build matrices" << endl;

          matA = ONE_DOUBLE;
          matB = TWO_DOUBLE;
          matC = THREE_DOUBLE;

          // test equality

          cout << " --> 0003 --> test equality" << endl;

          cout << " --> true --> " << (matA == ONE_DOUBLE) << endl;
          cout << " --> true --> " << (matB == TWO_DOUBLE) << endl;
          cout << " --> true --> " << (matC == THREE_DOUBLE) << endl;

          // add matrices

          cout << " --> 0004 --> add matrices several times --> slow way" << endl;

          for (int i = 0; i != K_MAX_LOC; ++i) {
               matC = matA + matB;
          }

          cout << " --> true --> " << (matA == ONE_DOUBLE) << endl;
          cout << " --> true --> " << (matB == TWO_DOUBLE) << endl;
          cout << " --> true --> " << (matC == THREE_DOUBLE) << endl;

          // subtract matrices

          cout << " --> 0005 --> subtract matrices several times --> slow way" << endl;

          for (int i = 0; i != K_MAX_LOC; ++i) {
               matC = matA - matB;
          }

          cout << " --> true --> " << (matA == ONE_DOUBLE) << endl;
          cout << " --> true --> " << (matB == TWO_DOUBLE) << endl;
          cout << " --> true --> " << (matC == -ONE_DOUBLE) << endl;

          // multiply matrices

          cout << " --> 0006 --> multiply matrices several times --> slow way" << endl;

          for (int i = 0; i != K_MAX_LOC; ++i) {
               matC = matA * matB;
          }

          cout << " --> true --> " << (matA == ONE_DOUBLE) << endl;
          cout << " --> true --> " << (matB == TWO_DOUBLE) << endl;
          cout << " --> true --> " << (matC == TWO_DOUBLE) << endl;

          // divide matrices

          cout << " --> 0007 --> divide matrices several times --> slow way" << endl;

          for (int i = 0; i != K_MAX_LOC; ++i) {
               matC = matA / matB;
          }

          cout << " --> true --> " << (matA == ONE_DOUBLE) << endl;
          cout << " --> true --> " << (matB == TWO_DOUBLE) << endl;
          cout << " --> true --> " << (matC == (ONE_DOUBLE/TWO_DOUBLE)) << endl;

          // swap matrices

          cout << " --> 0008 --> swap matrices several times --> slow way" << endl;

          for (int i = 0; i != K_MAX_LOC; ++i) {

               matA = ONE_DOUBLE;
               matB = TWO_DOUBLE;
               matC = THREE_DOUBLE;

               matA = matB;
               matB = matC;
               matC = ONE_DOUBLE;
          }

          cout << " --> true --> " << (matA == TWO_DOUBLE) << endl;
          cout << " --> true --> " << (matB == THREE_DOUBLE) << endl;
          cout << " --> true --> " << (matC == ONE_DOUBLE) << endl;

          // advance matrices

          cout << " --> 0009 --> advance matrices several times --> slow way" << endl;

          matA = ONE_DOUBLE;
          matB = TWO_DOUBLE;
          matC = THREE_DOUBLE;

          for (int i = 0; i != K_MAX_LOC; ++i) {
               matA = matA + ONE_DOUBLE;
               matB = matB + ONE_DOUBLE;
               matC = matC + ONE_DOUBLE;
          }

          cout << " --> true --> " << (matA == (ONE_DOUBLE+ONE_DOUBLE*K_MAX_LOC)) << endl;
          cout << " --> true --> " << (matB == (TWO_DOUBLE+ONE_DOUBLE*K_MAX_LOC)) << endl;
          cout << " --> true --> " << (matC == (THREE_DOUBLE+ONE_DOUBLE*K_MAX_LOC)) << endl;

          // advance matrices

          cout << " --> 0010 --> advance matrices several times --> slow way" << endl;

          matA = ONE_DOUBLE;
          matB = TWO_DOUBLE;
          matC = THREE_DOUBLE;

          for (int i = 0; i != K_MAX_LOC; ++i) {
               matA = matA - ONE_DOUBLE;
               matB = matB - ONE_DOUBLE;
               matC = matC - ONE_DOUBLE;
          }

          cout << " --> true --> " << (matA == (ONE_DOUBLE-ONE_DOUBLE*K_MAX_LOC)) << endl;
          cout << " --> true --> " << (matB == (TWO_DOUBLE-ONE_DOUBLE*K_MAX_LOC)) << endl;
          cout << " --> true --> " << (matC == (THREE_DOUBLE-ONE_DOUBLE*K_MAX_LOC)) << endl;

          // advance matrices

          cout << " --> 0011 --> advance matrices several times --> slow way" << endl;

          matA = ONE_DOUBLE;
          matB = TWO_DOUBLE;
          matC = THREE_DOUBLE;

          for (int i = 0; i != K_MAX_LOC; ++i) {
               matA = matA * ONE_DOUBLE;
               matB = matB * ONE_DOUBLE;
               matC = matC * ONE_DOUBLE;
          }

          cout << " --> true --> " << (matA == ONE_DOUBLE) << endl;
          cout << " --> true --> " << (matB == TWO_DOUBLE) << endl;
          cout << " --> true --> " << (matC == THREE_DOUBLE) << endl;

          // advance matrices

          cout << " --> 0012 --> advance matrices several times --> slow way" << endl;

          matA = ONE_DOUBLE;
          matB = TWO_DOUBLE;
          matC = THREE_DOUBLE;

          for (int i = 0; i != K_MAX_LOC; ++i) {
               matA = matA / ONE_DOUBLE;
               matB = matB / ONE_DOUBLE;
               matC = matC / ONE_DOUBLE;
          }

          cout << " --> true --> " << (matA == ONE_DOUBLE) << endl;
          cout << " --> true --> " << (matB == TWO_DOUBLE) << endl;
          cout << " --> true --> " << (matC ==THREE_DOUBLE) << endl;

          // advance matrices

          cout << " --> 0013 --> advance matrices several times --> slow way" << endl;

          matA = ONE_DOUBLE;
          matB = TWO_DOUBLE;
          matC = THREE_DOUBLE;

          for (int i = 0; i != K_MAX_LOC; ++i) {
               matA = matA + matA;
               matB = matB + matB;
               matC = matC + matC;
          }

          cout << " --> true --> " << (matA == 1.0*pow(TWO_DOUBLE, 10.0)) << endl;
          cout << " --> true --> " << (matB == 2.0*pow(TWO_DOUBLE, 10.0)) << endl;
          cout << " --> true --> " << (matC == 3.0*pow(TWO_DOUBLE, 10.0)) << endl;

          // advance matrices

          cout << " --> 0014 --> advance matrices several times --> slow way" << endl;

          matA = ONE_DOUBLE;
          matB = TWO_DOUBLE;
          matC = THREE_DOUBLE;

          for (int i = 0; i != K_MAX_LOC; ++i) {
               matA = matA - matA;
               matB = matB - matB;
               matC = matC - matC;
          }

          cout << " --> true --> " << (matA == pgg::ZERO_DBL) << endl;
          cout << " --> true --> " << (matB == pgg::ZERO_DBL) << endl;
          cout << " --> true --> " << (matC == pgg::ZERO_DBL) << endl;

          // advance matrices

          cout << " --> 0015 --> advance matrices several times --> slow way" << endl;

          matA = ONE_DOUBLE;
          matB = TWO_DOUBLE;
          matC = THREE_DOUBLE;

          for (int i = 0; i != K_MAX_LOC; ++i) {
               matA = matA / matA;
               matB = matB / matB;
               matC = matC / matC;
          }

          cout << " --> true --> " << (matA == pgg::ONE_DBL) << endl;
          cout << " --> true --> " << (matB == pgg::ONE_DBL) << endl;
          cout << " --> true --> " << (matC == pgg::ONE_DBL) << endl;

          // advance matrices

          cout << " --> 0016 --> advance matrices several times --> slow way" << endl;

          matA = ONE_DOUBLE;
          matB = TWO_DOUBLE;
          matC = THREE_DOUBLE;

          for (int i = 0; i != K_MAX_LOC; ++i) {
               matA = matA * matA;
               matB = matB * matB;
               matB = matB / TWO_DOUBLE;
               matC = matC * matC;
               matC = matC / THREE_DOUBLE;
          }

          cout << " --> true --> " << (matA == ONE_DOUBLE) << endl;
          cout << " --> true --> " << (matB == TWO_DOUBLE) << endl;
          cout << " --> true --> " << (matC == THREE_DOUBLE) << endl;

          // deallocate heap space

          cout << " --> 00xx --> deallocate heap space" << endl;

          matA.Deallocate();
          matB.Deallocate();
          matC.Deallocate();

          // restart
     }

     return 0;
}

// END
