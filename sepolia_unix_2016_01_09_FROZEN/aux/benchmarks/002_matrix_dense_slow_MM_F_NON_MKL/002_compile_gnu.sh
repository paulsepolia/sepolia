#!/bin/bash

  # 1. compile

  g++-5  -c                     \
         -O3                    \
         -Wall                  \
         -std=gnu++14           \
         -fopenmp               \
         driver_program.cpp

  # 2. link

  g++-5  driver_program.o                         \
         /opt/gsl/116/gnu_492/lib/libgsl.a        \
         /opt/lapacke/lib/liblapacke.a            \
         /opt/lapack/lib_2015/liblapack_gnu_492.a \
         /opt/cblas/lib_2015/libcblas_gnu_492.a   \
         /opt/blas/lib_2015/libblas_gnu_492.a     \
         /opt/gcc/492/lib64/libgfortran.a         \
         -lquadmath                               \
         -static                                  \
         -fopenmp                                 \
         -o x_gnu_521

  # 3. exit
 
  rm *.o

