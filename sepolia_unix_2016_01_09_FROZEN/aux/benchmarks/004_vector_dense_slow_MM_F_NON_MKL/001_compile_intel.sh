#!/bin/bash

  # 1. compile

  icpc  -c                     \
        -O3                    \
        -Wall                  \
        -std=c++11             \
        -openmp                \
        driver_program.cpp

  # 2. link

  icpc  driver_program.o                         \
        /opt/gsl/116/gnu_492/lib/libgsl.a        \
	   /opt/lapacke/lib/liblapacke.a            \
	   /opt/lapack/lib_2015/liblapack_gnu_492.a \
        /opt/cblas/lib_2015/libcblas_gnu_492.a   \
        /opt/blas/lib_2015/libblas_gnu_492.a     \
        -lgfortran                               \
        -static                                  \
        -openmp                                  \
        -o x_intel

  # 3. exit

  rm *.o
