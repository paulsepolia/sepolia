
//=============//
// test driver //
//=============//

#include <iostream>
#include <ctime>
#include <cmath>
#include <iomanip>
#include <complex>

using std::cout;
using std::cin;
using std::endl;
using std::abs;
using std::pow;
using std::boolalpha;
using std::setprecision;
using std::fixed;
using std::showpos;
using std::showpoint;
using std::complex;

#include "../../../head/sepolia.hpp"

using pgg::sep::MatrixDense;
using pgg::sep::VectorDense;

// the main code

int main()
{
     // local parameters

     const auto DIM = 3 *static_cast<pgg::MAI_MAX>(pow(10.0, 3.0));
     const auto K_MAX = 4;

     MatrixDense<complex<double>> matA;
     MatrixDense<complex<double>> matB;
     MatrixDense<complex<double>> matC;

     clock_t t1;
     clock_t t2;

     cout << boolalpha;
     cout << fixed;
     cout << showpos;
     cout << showpoint;
     cout << setprecision(10);

     // main test loop

     for (int k = 0; k != K_MAX; ++k) {
          cout << "-------------------------------------------------->> " << k << endl;

          // allocate

          cout << " --> 001 --> allocate matrices" << endl;

          matA.Allocate(DIM);
          matB.Allocate(DIM);
          matC.Allocate(DIM);

          // build

          cout << " --> 002 --> build matrices" << endl;

          const complex<double> a1 = {1.0, 2.0};
          const complex<double> a2 = {3.0, 4.0};

          // dot # 1

          matA.Set(a1);
          matB.Set(a2);
          matC.Set(pgg::ZERO_CMPLX);

          cout << " --> 003 --> dot # 1" << endl;

          t1 = clock();

          matC.Dot(matA, matB);

          t2 = clock();

          cout << " --> time used = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

          cout << " --> matA(0,0) = " << matA(0,0) << endl;
          cout << " --> matB(0,0) = " << matB(0,0) << endl;
          cout << " --> matC(0,0) = " << matC(0,0) << endl;

          // dot # 2

          matA.Set(a1);
          matB.Set(a2);

          cout << " --> 004 --> dot # 2" << endl;

          t1 = clock();

          matB.Dot(matA);

          t2 = clock();

          cout << " --> time used = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

          cout << " --> matA(0,0) = " << matA(0,0) << endl;
          cout << " --> matB(0,0) = " << matB(0,0) << endl;

          // dot # 3

          matA.Set(a1);
          matC.Set(pgg::ZERO_CMPLX);

          cout << " --> 005 --> dot # 3" << endl;

          t1 = clock();

          matC.Dot(matA);

          t2 = clock();

          cout << " --> time used = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

          cout << " --> matA(0,0) = " << matA(0,0) << endl;
          cout << " --> matC(0,0) = " << matC(0,0) << endl;
     }

     return 0;
}

// END
