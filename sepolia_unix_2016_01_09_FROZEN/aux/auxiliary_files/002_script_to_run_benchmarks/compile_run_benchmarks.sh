#!/bin/bash

# enter subdirectories 
# execute commands

for i in $(ls -d */);
	do 
		cd  ${i%%/};

		sh 001_compile_intel.sh
		./x_intel

		sh 002_compile_gnu_521.sh
		./x_gnu_521

		sh 0xx_clean.sh

		cd -
	done

# end
