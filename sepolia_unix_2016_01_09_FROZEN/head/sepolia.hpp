
#ifndef SEPOLIA_H
#define SEPOLIA_H

// C++

#include <iostream>
#include <cstdlib>
#include <cmath>
#include <algorithm>
#include <utility>
#include <string>
#include <complex>

// PGG

#include "../parameters/Parameters.hpp"
#include "../functions/Functions.hpp"
#include "../functors/Functors.hpp"
#include "../matrix_dense/MatrixDense.hpp"
#include "../vector_dense/VectorDense.hpp"

#endif // SEPOLIA_H
