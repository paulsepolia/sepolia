
#ifndef MATRIX_DENSE_SET_H
#define MATRIX_DENSE_SET_H

namespace pgg {
namespace sep {

// Set # 1

template<typename T1>
void MatrixDense<T1>::Set(const MatrixDense<T1> & mat)
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
     mat.CheckAllocation();
#endif

#ifdef DEBUG_COMPAT
     this->CheckCompatibility(mat);
#endif

     MAI i;
     MAI j;

     const MAI COLS = mat.cols;
     const MAI ROWS = mat.rows;
     const MAI_MAX COLS_LONG = static_cast<MAI_MAX>(COLS);
     MAI_MAX index;

     #pragma omp parallel default(none) \
     num_threads(NT_2D) \
     private(i)    \
     private(j)    \
     private(index)\
     shared(mat)
     {
          #pragma omp for

          for (i = 0; i < ROWS; ++i) {
               for (j = 0; j < COLS; ++j) {

                    index = i * COLS_LONG + j;
                    this->matrix[index] = mat.matrix[index];
               }
          }
     }
}

// Set # 2

template<typename T1>
void MatrixDense<T1>::Set(const T1 & val)
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
#endif

     MAI i;
     MAI j;

     const MAI COLS = this->cols;
     const MAI ROWS = this->rows;
     const MAI_MAX COLS_LONG = static_cast<MAI_MAX>(COLS);
     MAI_MAX index;

     #pragma omp parallel default(none) \
     num_threads(NT_2D) \
     private(i)    \
     private(j)    \
     private(index)\
     shared(val)
     {
          #pragma omp for

          for (i = 0; i < ROWS; ++i) {
               for (j = 0; j < COLS; ++j) {

                    index = i * COLS_LONG + j;
                    this->matrix[index] = static_cast<T1>(val);
               }
          }
     }
}

// SetColumn

template<typename T1>
template<typename T2>
void MatrixDense<T1>::SetColumn(const MAI & j_col,
                                const T2 & vec)
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
     vec.CheckAllocation();
#endif

     const MAI ROWS = this->GetNumberOfRows();

#ifdef DEBUG_COMPAT
     const VEI ELEMS = vec.GetNumberOfElements();

     if (ELEMS != static_cast<VEI>(ROWS)) {
          functions::Exit(E_MAT_VEC_CONFORM_NOT);
     }
#endif

     MAI i;

     #pragma omp parallel default(none) \
     num_threads(NT_1D) \
     private(i)    \
     shared(j_col) \
     shared(vec)
     {
          #pragma omp for

          for (i = 0; i < ROWS; ++i) {
               this->SetElement(i, j_col, vec(i));
          }
     }
}

// SetElement # 1

template<typename T1>
void MatrixDense<T1>::SetElement(const MAI & i_row,
                                 const MAI & j_col,
                                 const T1 & elem)
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
#endif

     // get the number of columns

     const MAI COLS = this->cols;
     const MAI_MAX COLS_LONG = static_cast<MAI_MAX>(COLS);

     // set the element (i_row, j_col)

     this->matrix[i_row * COLS_LONG + j_col] = elem;
}

// SetElement # 2

template<typename T1>
void MatrixDense<T1>::SetElement(const MAI_MAX & index,
                                 const T1 & elem)
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
#endif

     this->matrix[index] = elem;
}

// SetRow

template<typename T1>
template<typename T2>
void MatrixDense<T1>::SetRow(const MAI & i_row,
                             const T2 & vec)
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
     vec.CheckAllocation();
#endif

     const MAI COLS = this->GetNumberOfColumns();

#ifdef DEBUG_COMPAT
     const VEI ELEMS = vec.GetNumberOfElements();

     if (ELEMS != static_cast<VEI>(COLS)) {
          functions::Exit(E_MAT_VEC_CONFORM_NOT);
     }
#endif

     MAI j;

     #pragma omp parallel default(none) \
     num_threads(NT_1D) \
     private(j)    \
     shared(i_row) \
     shared(vec)
     {
          #pragma omp for

          for (j = 0; j < COLS; ++j) {
               this->SetElement(i_row, j, vec(j));
          }
     }
}

} // sep
} // pgg

#endif // MATRIX_DENSE_SET_H

