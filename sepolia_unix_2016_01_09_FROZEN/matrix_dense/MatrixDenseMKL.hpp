
#ifndef MATRIX_DENSE_MKL_H
#define MATRIX_DENSE_MKL_H

#include "../head/sepolia_mkl_lapacke.hpp"

namespace pgg {
namespace sep {

// TransposeMKL # 1

template<>
void MatrixDense<double>::TransposeMKL()
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
#endif

#ifdef DEBUG_COMPAT
     this->CheckSquareQ();
#endif

     const MAI ROWS = this->GetNumberOfRows();
     const MAI COLS = this->GetNumberOfColumns();

     mkl_dimatcopy('R',
                   'T',
                   ROWS,
                   COLS,
                   ONE_DBL,
                   this->matrix,
                   ROWS,
                   COLS);

}

// TransposeMKL # 2

template<>
void MatrixDense<double>::TransposeMKL(
     const MatrixDense<double> & mat)
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
     mat.CheckAllocation();
#endif

     if (this != &mat) {

#ifdef DEBUG_COMPAT
          this->CheckCompatibilityTranspose(mat);
#endif

          const MAI ROWS = mat.GetNumberOfRows();
          const MAI COLS = mat.GetNumberOfColumns();

          mkl_domatcopy('R',
                        'T',
                        ROWS,
                        COLS,
                        ONE_DBL,
                        mat.matrix,
                        COLS,
                        this->matrix,
                        ROWS);
     } else if (this == &mat) {
          this->TransposeMKL();
     }
}

} // sep
} // pgg

#endif // MATRIX_DENSE_MKL_H

