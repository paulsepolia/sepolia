
#ifndef MATRIX_DENSE_OPERATORS_H
#define MATRIX_DENSE_OPERATORS_H

namespace pgg {
namespace sep {

// operator = # 1

template<typename T1>
MatrixDense<T1> & MatrixDense<T1>::operator =(
     const MatrixDense<T1> & mat)
{
     this->Set(mat);
     return *this;
}

// operator = # 2

template<typename T1>
MatrixDense<T1> & MatrixDense<T1>::operator =(
     const T1 & elem)
{
     this->Set(elem);
     return *this;
}

// operator + # 1

template<typename T1>
MatrixDense<T1> MatrixDense<T1>::operator +(
     const MatrixDense<T1> & mat) const
{
     MatrixDense<T1> mat_tmp;

     mat_tmp.Allocate(mat.rows, mat.cols);
     mat_tmp.Plus(*this, mat);

     return mat_tmp;
}

// operator + # 2

template<typename T1>
MatrixDense<T1> MatrixDense<T1>::operator +(
     const T1 & elem) const
{
     MatrixDense<T1> mat_tmp;

     mat_tmp.Allocate(this->rows, this->cols);
     mat_tmp.Plus(*this, elem);

     return mat_tmp;
}

// operator + # 3

template<typename T1>
MatrixDense<T1> operator +(const T1 & elem,
                           const MatrixDense<T1> & mat)
{
     MatrixDense<T1> mat_tmp;

     mat_tmp.Allocate(mat.rows, mat.cols);
     mat_tmp.Plus(mat, elem);

     return mat_tmp;
}

// operator - # 1

template<typename T1>
MatrixDense<T1> MatrixDense<T1>::operator -(
     const MatrixDense<T1> & mat) const
{
     MatrixDense<T1> mat_tmp;

     mat_tmp.Allocate(mat.rows, mat.cols);
     mat_tmp.Subtract(*this, mat);

     return mat_tmp;
}

// operator - # 2

template<typename T1>
MatrixDense<T1> MatrixDense<T1>::operator -(
     const T1 & elem) const
{
     MatrixDense<T1> mat_tmp;

     mat_tmp.Allocate(this->rows, this->cols);
     mat_tmp.Subtract(*this, elem);

     return mat_tmp;
}

// operator - # 3

template<typename T1>
MatrixDense<T1> operator -(const T1 & elem,
                           const MatrixDense<T1> & mat)
{
     MatrixDense<T1> mat_tmp;

     mat_tmp.Allocate(mat.rows, mat.cols);
     mat_tmp.Subtract(elem, mat);

     return mat_tmp;
}

// operator - # 4

template<typename T1>
MatrixDense<T1> MatrixDense<T1>::operator -() const
{
     MatrixDense<T1> mat_tmp;

     mat_tmp.Allocate(this->rows, this->cols);
     mat_tmp.Times(*this, static_cast<T1>(-ONE_DBL_MAX));

     return mat_tmp;
}

// operator * # 1

template<typename T1>
MatrixDense<T1> MatrixDense<T1>::operator *(
     const MatrixDense<T1> & mat) const
{
     MatrixDense<T1> mat_tmp;

     mat_tmp.Allocate(mat.rows, mat.cols);
     mat_tmp.Times(*this, mat);

     return mat_tmp;
}

// operator * # 2

template<typename T1>
MatrixDense<T1> MatrixDense<T1>::operator *(
     const T1 & elem) const
{
     MatrixDense<T1> mat_tmp;

     mat_tmp.Allocate(this->rows, this->cols);
     mat_tmp.Times(*this, elem);

     return mat_tmp;
}

// operator * # 3

template<typename T1>
MatrixDense<T1> operator *(
     const T1 & elem,
     const MatrixDense<T1> & mat)
{
     return mat*elem;
}

// operator / # 1

template<typename T1>
MatrixDense<T1> MatrixDense<T1>::operator /(
     const MatrixDense<T1> & mat) const
{
     MatrixDense<T1> mat_tmp;

     mat_tmp.Allocate(mat.rows, mat.cols);
     mat_tmp.Divide(*this, mat);

     return mat_tmp;
}

// operator / # 2

template<typename T1>
MatrixDense<T1> MatrixDense<T1>::operator /(
     const T1 & elem) const
{
     MatrixDense<T1> mat_tmp;

     mat_tmp.Allocate(this->rows, this->cols);
     mat_tmp.Divide(*this, elem);

     return mat_tmp;
}

// operator / # 3

template<typename T1>
MatrixDense<T1> operator /(
     const T1 & elem,
     const MatrixDense<T1> & mat)
{
     MatrixDense<T1> mat_tmp;

     mat_tmp.Allocate(mat.rows, mat.cols);
     mat_tmp.Divide(elem, mat);

     return mat_tmp;
}

// operator (,)

template<typename T1>
const T1 MatrixDense<T1>::operator ()(
     const MAI & i,
     const MAI & j) const
{
     const MAI COLS = this->cols;
     const MAI_MAX COLS_LONG = static_cast<MAI_MAX>(COLS);

     const T1 elem = this->matrix[i * COLS_LONG + j];

     return elem;
}

// operator ()

template<typename T1>
const T1 MatrixDense<T1>::operator ()(
     const MAI_MAX & index) const
{
     MAI i;
     MAI j;

     const MAI COLS = this->cols;
     const MAI_MAX COLS_LONG = static_cast<MAI_MAX>(COLS);

     i = index / COLS;
     j = index % COLS;

     const T1 elem = this->matrix[i * COLS_LONG + j];

     return elem;
}

// operator += # 1

template<typename T1>
MatrixDense<T1> MatrixDense<T1>::operator +=(
     const MatrixDense<T1> & mat)
{
     this->Plus(*this, mat);

     return *this;
}

// operator += # 2

template<typename T1>
MatrixDense<T1> MatrixDense<T1>::operator +=(
     const T1 & elem)
{
     this->Plus(*this, elem);

     return *this;
}

// operator -= # 1

template<typename T1>
MatrixDense<T1> MatrixDense<T1>::operator -=(
     const MatrixDense<T1> & mat)
{
     this->Subtract(*this, mat);

     return *this;
}

// operator -= # 2

template<typename T1>
MatrixDense<T1> MatrixDense<T1>::operator -=(
     const T1 & elem)
{
     this->Subtract(*this, elem);

     return *this;
}

// operator *= # 1

template<typename T1>
MatrixDense<T1> MatrixDense<T1>::operator *=(
     const MatrixDense<T1> & mat)
{
     this->Times(*this, mat);

     return *this;
}

// operator *= # 2

template<typename T1>
MatrixDense<T1> MatrixDense<T1>::operator *=(
     const T1 & elem)
{
     this->Times(*this, elem);

     return *this;
}

// operator /= # 1

template<typename T1>
MatrixDense<T1> MatrixDense<T1>::operator /=(
     const MatrixDense<T1> & mat)
{
     this->Divide(*this, mat);

     return *this;
}

// operator /= # 2

template<typename T1>
MatrixDense<T1> MatrixDense<T1>::operator /=(
     const T1 & elem)
{
     this->Divide(*this, elem);

     return *this;
}

// operator ++ # 1

template<typename T1>
MatrixDense<T1> MatrixDense<T1>::operator ++()
{
     this->Plus(*this, static_cast<T1>(ONE_DBL_MAX));

     return *this;
}

// operator ++ # 2

template<typename T1>
MatrixDense<T1> MatrixDense<T1>::operator ++(int)
{
     this->Plus(*this, static_cast<T1>(ONE_DBL_MAX));

     return *this;
}

// operator -- # 1

template<typename T1>
MatrixDense<T1> MatrixDense<T1>::operator --()
{
     this->Subtract(*this, static_cast<T1>(ONE_DBL_MAX));

     return *this;
}

// operator -- # 2

template<typename T1>
MatrixDense<T1> MatrixDense<T1>::operator --(int)
{
     this->Subtract(*this, static_cast<T1>(ONE_DBL_MAX));

     return *this;
}

// operator == # 1

template<typename T1>
bool MatrixDense<T1>::operator ==(
     const MatrixDense<T1> & mat) const
{
     return this->Equal(mat);
}

// operator == # 2

template<typename T1>
bool MatrixDense<T1>::operator ==(
     const T1 & elem) const
{
     return this->Equal(elem);
}

// operator != # 1

template<typename T1>
bool MatrixDense<T1>::operator !=(
     const MatrixDense<T1> & mat) const
{
     return !this->Equal(mat);
}

// operator != # 2

template<typename T1>
bool MatrixDense<T1>::operator !=(
     const T1 & elem) const
{
     return !this->Equal(elem);
}

// operator > # 1

template<typename T1>
bool MatrixDense<T1>::operator >(
     const MatrixDense<T1> & mat) const
{
     return this->Greater(mat);
}

// operator > # 2

template<typename T1>
bool MatrixDense<T1>::operator >(
     const T1 & elem) const
{
     return this->Greater(elem);
}

// operator >= # 1

template<typename T1>
bool MatrixDense<T1>::operator >=(
     const MatrixDense<T1> & mat) const
{
     return this->GreaterEqual(mat);
}

// operator >= # 2

template<typename T1>
bool MatrixDense<T1>::operator >=(
     const T1 & elem) const
{
     return this->GreaterEqual(elem);
}

// operator < # 1

template<typename T1>
bool MatrixDense<T1>::operator <(
     const MatrixDense<T1> & mat) const
{
     return this->Less(mat);
}

// operator < # 2

template<typename T1>
bool MatrixDense<T1>::operator <(
     const T1 & elem) const
{
     return this->Less(elem);
}

// operator <= # 1

template<typename T1>
bool MatrixDense<T1>::operator <=(
     const MatrixDense<T1> & mat) const
{
     return this->LessEqual(mat);
}

// operator <= # 2

template<typename T1>
bool MatrixDense<T1>::operator <=(
     const T1 & elem) const
{
     return this->LessEqual(elem);
}

// operator []

template<typename T1>
const T1 MatrixDense<T1>::operator [](
     const MAI_MAX & index) const
{
     MAI i;
     MAI j;

     const MAI ROWS = this->rows;
     const MAI COLS = this->cols;
     const MAI_MAX COLS_LONG = static_cast<MAI_MAX>(COLS);

     j = index / ROWS;
     i = index % ROWS;

     const T1 elem = this->matrix[i * COLS_LONG + j];

     return elem;
}

} // sep
} // pgg

#endif // MATRIX_DENSE_OPERATORS_H

