
#ifndef MATRIX_DENSE_DESTRUCTOR_H
#define MATRIX_DENSE_DESTRUCTOR_H

namespace pgg {
namespace sep {

// destructor

template<typename T1>
MatrixDense<T1>::~MatrixDense()
{
     if(this->is_alloc) {
          this->Deallocate();
     }
}

} // sep
} // pgg

#endif // MATRIX_DENSE_DESTRUCTOR_H

