
#ifndef MATRIX_DENSE_GSL_H
#define MATRIX_DENSE_GSL_H

#include "../head/sepolia_gsl.hpp"

namespace pgg {
namespace sep {

// DotGSL

template<>
void MatrixDense<double>::DotGSL(const MatrixDense<double> & matA,
                                 const MatrixDense<double> & matB)
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
     matA.CheckAllocation();
     matB.CheckAllocation();
#endif

#ifdef DEBUG_COMPAT
     this->CheckCompatibilityDot(matA, matB);
#endif

     // get the dimensions

     const auto ROWS_A = static_cast<GSL_INT>(matA.rows);
     const auto COLS_A = static_cast<GSL_INT>(matA.cols);
     const auto ROWS_B = static_cast<GSL_INT>(matB.rows);
     const auto COLS_B = static_cast<GSL_INT>(matB.cols);
     const auto ROWS_C = static_cast<GSL_INT>(this->rows);
     const auto COLS_C = static_cast<GSL_INT>(this->cols);

     // local parameters

     if((this == &matA) || (this == &matB)) {
          // declare and allocate tmp matrix

          MatrixDense<double> mat_tmp;
          mat_tmp.Allocate(this->rows, this->cols);

          // set gsl matrix view

          gsl_matrix_view A = gsl_matrix_view_array(matA.matrix, ROWS_A, COLS_A);
          gsl_matrix_view B = gsl_matrix_view_array(matB.matrix, ROWS_B, COLS_B);
          gsl_matrix_view C = gsl_matrix_view_array(mat_tmp.matrix, ROWS_C, COLS_C);

          // compute C = dot(A,B) via gsl+blas simplified interface

          gsl_blas_dgemm(CblasNoTrans,
                         CblasNoTrans,
                         ONE_DBL,
                         &A.matrix,
                         &B.matrix,
                         ZERO_DBL,
                         &C.matrix);

          // assing the tmp matrix to this

          this->Set(mat_tmp);

          // deallocate the tmp matrix

          mat_tmp.Deallocate();

          // return
     } else if ((this != &matA) && (this != &matB)) {
          // set gsl matrix view

          gsl_matrix_view A = gsl_matrix_view_array(matA.matrix, ROWS_A, COLS_A);
          gsl_matrix_view B = gsl_matrix_view_array(matB.matrix, ROWS_B, COLS_B);
          gsl_matrix_view C = gsl_matrix_view_array(this->matrix, ROWS_C, COLS_C);

          // compute C = dot(A,B) via gsl+blas simplified interface

          gsl_blas_dgemm(CblasNoTrans,
                         CblasNoTrans,
                         ONE_DBL,
                         &A.matrix,
                         &B.matrix,
                         ZERO_DBL,
                         &C.matrix);

          // return
     }
}

// Transpose # 1

template<typename T1>
void MatrixDense<T1>::TransposeGSL()
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
#endif

#ifdef DEBUG_COMPAT
     this->CheckSquareQ();
#endif

     const MAI ROWS = this->GetNumberOfRows();
     const MAI COLS = this->GetNumberOfColumns();

     gsl_matrix_view A = gsl_matrix_view_array(this->matrix, ROWS, COLS);

     gsl_matrix_transpose(&A.matrix);
}

// Transpose # 2

template<typename T1>
void MatrixDense<T1>::TransposeGSL(const MatrixDense<T1> & mat)
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
     mat.CheckAllocation();
#endif

     if (this != &mat) {

#ifdef DEBUG_COMPAT
          this->CheckCompatibilityTranspose(mat);
#endif

          const MAI ROWS = mat.GetNumberOfRows();
          const MAI COLS = mat.GetNumberOfColumns();

          gsl_matrix_view mDst = gsl_matrix_view_array(this->matrix, COLS, ROWS);
          gsl_matrix_view mSrc = gsl_matrix_view_array(mat.matrix, ROWS, COLS);

          gsl_matrix_transpose_memcpy(&mDst.matrix, &mSrc.matrix);
     } else if (this == &mat) {
          this->TransposeGSL();
     }
}

} // sep
} // pgg

#endif // MATRIX_DENSE_GSL_H

