
#ifndef MATRIX_DENSE_GENERIC_H
#define MATRIX_DENSE_GENERIC_H

#include <string>
#include <complex>

namespace pgg {
namespace sep {

//===============//
// Double Matrix //
//===============//

// Dot # 1

template<>
void MatrixDense<double>::Dot(const MatrixDense<double> & matA,
                              const MatrixDense<double> & matB,
                              const std::string & str_val)
{
     if (str_val == GSL_LIB) {
          this->DotGSL(matA, matB);
     } else if (str_val == BLAS_LIB) {
          this->DotBLAS(matA, matB);
     } else {
          functions::Exit(E_MAT_MAT_DOT_TYPE_NOT);
     }
}

// Dot # 2

template<>
template<typename T2>
void MatrixDense<double>::Dot(const MatrixDense<double> & mat,
                              const T2 & vec,
                              const std::string & str_val)
{
     this->DotBLAS(mat, vec, str_val);
}

// Dot # 3

template<>
void MatrixDense<double>::Dot(const MatrixDense<double> & mat)
{
     this->DotBLAS(mat);
}

// Dot # 4

template<>
template<typename T2>
double MatrixDense<double>::Dot(const T2 & vec,
                                const std::string & str_val)
{
     return this->DotBLAS(vec, str_val);
}

// Dot # 5

template<>
void MatrixDense<double>::Dot()
{
     this->DotBLAS();
}

//=======================//
// Complex Double Matrix //
//=======================//

// Dot # 1

template<>
void MatrixDense<std::complex<double>>::Dot(
          const MatrixDense<std::complex<double>> & matA,
          const MatrixDense<std::complex<double>> & matB)
{
     this->DotBLAS(matA, matB);
}

// Dot # 2

template<>
template<typename T2>
void MatrixDense<std::complex<double>>::Dot(
          const MatrixDense<std::complex<double>> & mat,
          const T2 & vec,
          const std::string & str_val)
{
     this->DotBLAS(mat, vec, str_val);
}

// Dot # 3

template<>
void MatrixDense<std::complex<double>>::Dot(
          const MatrixDense<std::complex<double>> & mat)
{
     this->DotBLAS(mat);
}

// Dot # 4

template<>
template<typename T2>
std::complex<double> MatrixDense<std::complex<double>>::DotC(
               const T2 & vec,
               const std::string & str_val)
{
     return this->DotBLASC(vec, str_val);
}

// Dot # 5

template<>
void MatrixDense<std::complex<double>>::Dot()
{
     this->DotBLAS();
}

// EigensystemComplexHermitian

template<>
template<typename T2>
LAPACK_INT MatrixDense<std::complex<double>>::EigensystemComplexHermitian(
               MatrixDense<std::complex<double>> & eigenvectors,
               T2 & eigenvalues,
               const std::string & str_val) const
{
     LAPACK_INT res;

     if (str_val == DIAG_ZHEEVD) {
          res = this->EigensystemZHEEVD(eigenvectors, eigenvalues);
     } else if (str_val == DIAG_ZHEEVR) {
          res = this->EigensystemZHEEVR(eigenvectors, eigenvalues);
     } else if (str_val == DIAG_ZHEEVX) {
          res = this->EigensystemZHEEVX(eigenvectors, eigenvalues);
     } else if (str_val == DIAG_ZHEEV) {
          res = this->EigensystemZHEEV(eigenvectors, eigenvalues);
     } else {
          res = -2;
          functions::Exit(E_MAT_MAT_DIAG_TYPE_NOT);
     }

     return res;
}

// EigensystemRealSymmetric

template<>
template<typename T2>
LAPACK_INT MatrixDense<double>::EigensystemRealSymmetric(
     MatrixDense<double> & eigenvectors,
     T2 & eigenvalues,
     const std::string & str_val) const
{
     LAPACK_INT res;

     if (str_val == DIAG_DSYEVD) {
          res = this->EigensystemDSYEVD(eigenvectors, eigenvalues);
     } else if (str_val == DIAG_DSYEVR) {
          res = this->EigensystemDSYEVR(eigenvectors, eigenvalues);
     } else if (str_val == DIAG_DSYEVX) {
          res = this->EigensystemDSYEVX(eigenvectors, eigenvalues);
     } else if (str_val == DIAG_DSYEV) {
          res = this->EigensystemDSYEV(eigenvectors, eigenvalues);
     } else {
          res = -2;
          functions::Exit(E_MAT_MAT_DIAG_TYPE_NOT);
     }

     return res;
}

// EigenvaluesComplexHermitian

template<>
template<typename T2>
LAPACK_INT MatrixDense<std::complex<double>>::EigenvaluesComplexHermitian(
               T2 & eigenvalues,
               const std::string & str_val) const
{
     LAPACK_INT res;

     if (str_val == DIAG_ZHEEVD) {
          res = this->EigenvaluesZHEEVD(eigenvalues);
     } else if (str_val == DIAG_ZHEEVR) {
          res = this->EigenvaluesZHEEVR(eigenvalues);
     } else if (str_val == DIAG_ZHEEVX) {
          res = this->EigenvaluesZHEEVX(eigenvalues);
     } else if (str_val == DIAG_ZHEEV) {
          res = this->EigenvaluesZHEEV(eigenvalues);
     } else {
          res = -2;
          functions::Exit(E_MAT_MAT_DIAG_TYPE_NOT);
     }

     return res;
}

// EigenvaluesRealSymmetric

template<>
template<typename T2>
LAPACK_INT MatrixDense<double>::EigenvaluesRealSymmetric(
     T2 & eigenvalues,
     const std::string & str_val) const
{
     LAPACK_INT res;

     if (str_val == DIAG_DSYEVD) {
          res = this->EigenvaluesDSYEVD(eigenvalues);
     } else if (str_val == DIAG_DSYEVR) {
          res = this->EigenvaluesDSYEVR(eigenvalues);
     } else if (str_val == DIAG_DSYEVX) {
          res = this->EigenvaluesDSYEVX(eigenvalues);
     } else if (str_val == DIAG_DSYEV) {
          res = this->EigenvaluesDSYEV(eigenvalues);
     } else {
          res = -2;
          functions::Exit(E_MAT_MAT_DIAG_TYPE_NOT);
     }

     return res;
}

// EigenvectorsComplexHermitian

template<>
LAPACK_INT MatrixDense<std::complex<double>>::EigenvectorsComplexHermitian(
               MatrixDense<std::complex<double>> & eigenvectors,
               const std::string & str_val) const
{
     LAPACK_INT res;

     if (str_val == DIAG_ZHEEVD) {
          res = this->EigenvectorsZHEEVD(eigenvectors);
     } else if (str_val == DIAG_ZHEEVR) {
          res = this->EigenvectorsZHEEVR(eigenvectors);
     } else if (str_val == DIAG_ZHEEVX) {
          res = this->EigenvectorsZHEEVX(eigenvectors);
     } else if (str_val == DIAG_ZHEEV) {
          res = this->EigenvectorsZHEEV(eigenvectors);
     } else {
          res = -2;
          functions::Exit(E_MAT_MAT_DIAG_TYPE_NOT);
     }

     return res;
}

// EigenvectorsRealSymmetric

template<>
LAPACK_INT MatrixDense<double>::EigenvectorsRealSymmetric(
     MatrixDense<double> & eigenvectors,
     const std::string & str_val) const
{
     LAPACK_INT res;

     if (str_val == DIAG_DSYEVD) {
          res = this->EigenvectorsDSYEVD(eigenvectors);
     } else if (str_val == DIAG_DSYEVR) {
          res = this->EigenvectorsDSYEVR(eigenvectors);
     } else if (str_val == DIAG_DSYEVX) {
          res = this->EigenvectorsDSYEVX(eigenvectors);
     } else if (str_val == DIAG_DSYEV) {
          res = this->EigenvectorsDSYEV(eigenvectors);
     } else {
          res = -2;
          functions::Exit(E_MAT_MAT_DIAG_TYPE_NOT);
     }

     return res;
}

// Transpose # 1

template<typename T1>
void MatrixDense<T1>::Transpose(const std::string & str_val)
{
     if (str_val == PGG_LIB) {
          this->TransposePGG();
     } else if (str_val == GSL_LIB) {
          this->TransposeGSL();
     }
#ifdef MKL_SEPOLIA_H
     else if (str_val == MKL_LIB) {
          this->TransposeMKL();
     }
#endif
     else {
          functions::Exit(E_MAT_LIB_TYPE_NOT);
     }
}

// Transpose # 2

template<typename T1>
void MatrixDense<T1>::Transpose(const MatrixDense<T1> & mat,
                                const std::string & str_val)
{
     if (str_val == PGG_LIB) {
          this->TransposePGG(mat);
     } else if (str_val == GSL_LIB) {
          this->TransposeGSL(mat);
     }
#ifdef MKL_SEPOLIA_H
     else if (str_val == MKL_LIB) {
          this->TransposeMKL(mat);
     }
#endif
     else {
          functions::Exit(E_MAT_LIB_TYPE_NOT);
     }
}

} // sep
} // pgg

#endif // MATRIX_DENSE_GENERIC_H

