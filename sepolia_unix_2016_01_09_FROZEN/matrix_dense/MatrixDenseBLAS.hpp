
#ifndef MATRIX_DENSE_BLAS_H
#define MATRIX_DENSE_BLAS_H

#include <complex>
#include "../head/sepolia_gsl.hpp"

namespace pgg {
namespace sep {

//===============================//
// Complex Double General Matrix //
//===============================//

// DotBLAS # 1

template<>
void MatrixDense<std::complex<double>>::DotBLAS(
          const MatrixDense<std::complex<double>> & matA,
          const MatrixDense<std::complex<double>> & matB)
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
     matA.CheckAllocation();
     matB.CheckAllocation();
#endif

#ifdef DEBUG_COMPAT
     this->CheckCompatibilityDot(matA, matB);
#endif

     // get the dimensions

     const MAI ROWS_A = matA.rows;
     const MAI COLS_A = matA.cols;
     const MAI COLS_B = matB.cols;

     if((this == &matA) || (this == &matB)) {
          // declare and allocate tmp matrix

          MatrixDense<std::complex<double>> mat_tmp;
          mat_tmp.Allocate(this->rows, this->cols);

          // execution

          cblas_zgemm(CblasRowMajor,                  //  1 --> CblasRowMajor, CblasColMajor
                      CblasNoTrans,                   //  2 --> CblasNoTrans, CblasTrans
                      CblasNoTrans,                   //  3 --> same as above
                      static_cast<BLAS_INT>(ROWS_A),  //  4 --> m
                      static_cast<BLAS_INT>(COLS_B),  //  5 --> n
                      static_cast<BLAS_INT>(COLS_A),  //  6 --> k
                      &ONE_CMPLX,                     //  7 --> alpha
                      matA.matrix,                    //  8 --> *a
                      static_cast<BLAS_INT>(COLS_A),  //  9 --> lda
                      matB.matrix,                    // 10 --> *b
                      static_cast<BLAS_INT>(COLS_B),  // 11 --> ldb
                      &ZERO_CMPLX,                    // 12 --> beta
                      mat_tmp.matrix,                 // 13 --> *c
                      static_cast<BLAS_INT>(COLS_B)); // 14 --> ldc

          this->Set(mat_tmp);

          // deallocate tmp matrix

          mat_tmp.Deallocate();

          // return
     } else if((this != &matA) && (this != &matB)) {
          // execution

          cblas_zgemm(CblasRowMajor,                  //  1 --> CblasRowMajor, CblasColMajor
                      CblasNoTrans,                   //  2 --> CblasNoTrans, CblasTrans
                      CblasNoTrans,                   //  3 --> same as above
                      static_cast<BLAS_INT>(ROWS_A),  //  4 --> m
                      static_cast<BLAS_INT>(COLS_B),  //  5 --> n
                      static_cast<BLAS_INT>(COLS_A),  //  6 --> k
                      &ONE_CMPLX,                     //  7 --> alpha
                      matA.matrix,                    //  8 --> *a
                      static_cast<BLAS_INT>(COLS_A),  //  9 --> lda
                      matB.matrix,                    // 10 --> *b
                      static_cast<BLAS_INT>(COLS_B),  // 11 --> ldb
                      &ZERO_CMPLX,                    // 12 --> beta
                      this->matrix,                   // 13 --> *c
                      static_cast<BLAS_INT>(COLS_B)); // 14 --> ldc

          // return
     }
}

// DotBLAS # 2

template<>
void MatrixDense<std::complex<double>>::DotBLAS(
          const MatrixDense<std::complex<double>> & mat)
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
     mat.CheckAllocation();
#endif

#ifdef DEBUG_COMPAT
     this->CheckCompatibilityDot(*this, mat);
#endif

     // get the dimensions

     const MAI DIM = mat.rows;

     MatrixDense<std::complex<double>> mat_tmp;
     mat_tmp.Allocate(DIM);

     // execution

     cblas_zgemm(CblasRowMajor,               //  1 --> CblasRowMajor, CblasColMajor
                 CblasNoTrans,                //  2 --> CblasNoTrans, CblasTrans
                 CblasNoTrans,                //  3 --> same as above
                 static_cast<BLAS_INT>(DIM),  //  4 --> m
                 static_cast<BLAS_INT>(DIM),  //  5 --> n
                 static_cast<BLAS_INT>(DIM),  //  6 --> k
                 &ONE_CMPLX,                  //  7 --> alpha
                 this->matrix,                //  8 --> *a
                 static_cast<BLAS_INT>(DIM),  //  9 --> lda
                 mat.matrix,                  // 10 --> *b
                 static_cast<BLAS_INT>(DIM),  // 11 --> ldb
                 &ZERO_CMPLX,                 // 12 --> beta
                 mat_tmp.matrix,              // 13 --> *c
                 static_cast<BLAS_INT>(DIM)); // 14 --> ldc

     this->Set(mat_tmp);

     // deallocate tmp matrix

     mat_tmp.Deallocate();

     // return
}

// DotBLAS # 3

template<>
template<typename T2>
void MatrixDense<std::complex<double>>::DotBLAS(
          const MatrixDense<std::complex<double>> & mat,
          const T2 & vec,
          const std::string & row_col)
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
     mat.CheckAllocation();
     vec.CheckAllocation();
#endif

     if(row_col == WITH_ROWS) {

#ifdef DEBUG_COMPAT
          vec.CheckCompatibility(mat, LEN_ROW);
          this->CheckCompatibilityRows(mat);
          this->CheckVectorColumnQ();
#endif

          const VEI ROWS = mat.GetNumberOfRows();

          T2 vec_tmp;

          vec_tmp.Allocate(ROWS);

          vec_tmp.DotBLAS(mat, vec, row_col);

          this->SetColumn(0, vec_tmp);

          vec_tmp.Deallocate();
     } else if(row_col == WITH_COLS) {

#ifdef DEBUG_COMPAT
          vec.CheckCompatibility(mat, LEN_COL);
          this->CheckCompatibilityColumns(mat);
          this->CheckVectorRowQ();
#endif

          const VEI COLS = mat.GetNumberOfColumns();

          T2 vec_tmp;

          vec_tmp.Allocate(COLS);

          vec_tmp.DotBLAS(mat, vec, row_col);

          this->SetRow(0, vec_tmp);

          vec_tmp.Deallocate();
     } else {
          functions::Exit(E_MAT_VEC_DOT_TYPE_NOT);
     }
}

// DotBLAS # 4

template<>
template<typename T2>
std::complex<double> MatrixDense<std::complex<double>>::DotBLASC(
               const T2 & vec,
               const std::string & row_col) const
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
     vec.CheckAllocation();
#endif

     T2 vec_tmp;
     const VEI ELEMS = vec.GetNumberOfElements();
     std::complex<double> res = {0.0, 0.0};

     if(row_col == WITH_ROWS) {

#ifdef DEBUG_COMPAT
          vec.CheckCompatibility(*this, LEN_ROW);
          this->CheckVectorRowQ();
#endif

          vec_tmp.Allocate(ELEMS);

          vec_tmp.GetRow(*this, 0);

          res = vec_tmp.DotBLAS(vec);

          vec_tmp.Deallocate();
     } else if(row_col == WITH_COLS) {

#ifdef DEBUG_COMPAT
          vec.CheckCompatibility(*this, LEN_COL);
          this->CheckVectorColumnQ();
#endif

          vec_tmp.Allocate(ELEMS);

          vec_tmp.GetColumn(*this, 0);

          res = vec_tmp.DotBLAS(vec);

          vec_tmp.Deallocate();
     } else {
          functions::Exit(E_MAT_VEC_DOT_TYPE_NOT);
     }

     return res;
}

// DotBLAS # 5

template<>
void MatrixDense<std::complex<double>>::DotBLAS()
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
#endif

     // get the dimensions

     const MAI DIM = this->rows;

     MatrixDense<std::complex<double>> mat_tmp;
     mat_tmp.Allocate(DIM);

     // execution

     cblas_zgemm(CblasRowMajor,               //  1 --> CblasRowMajor, CblasColMajor
                 CblasNoTrans,                //  2 --> CblasNoTrans, CblasTrans
                 CblasNoTrans,                //  3 --> same as above
                 static_cast<BLAS_INT>(DIM),  //  4 --> m
                 static_cast<BLAS_INT>(DIM),  //  5 --> n
                 static_cast<BLAS_INT>(DIM),  //  6 --> k
                 &ONE_CMPLX,                  //  7 --> alpha
                 this->matrix,                //  8 --> *a
                 static_cast<BLAS_INT>(DIM),  //  9 --> lda
                 this->matrix,                // 10 --> *b
                 static_cast<BLAS_INT>(DIM),  // 11 --> ldb
                 &ZERO_CMPLX,                 // 12 --> beta
                 mat_tmp.matrix,              // 13 --> *c
                 static_cast<BLAS_INT>(DIM)); // 14 --> ldc

     this->Set(mat_tmp);

     // deallocate tmp matrix

     mat_tmp.Deallocate();

     // return
}

//=======================//
// Double General Matrix //
//=======================//

// DotBLAS # 1

template<>
void MatrixDense<double>::DotBLAS(const MatrixDense<double> & matA,
                                  const MatrixDense<double> & matB)
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
     matA.CheckAllocation();
     matB.CheckAllocation();
#endif

#ifdef DEBUG_COMPAT
     this->CheckCompatibilityDot(matA, matB);
#endif

     // get the dimensions

     const MAI ROWS_A = matA.rows;
     const MAI COLS_A = matA.cols;
     const MAI COLS_B = matB.cols;

     if((this == &matA) || (this == &matB)) {
          // declare and allocate tmp matrix

          MatrixDense<double> mat_tmp;
          mat_tmp.Allocate(this->rows, this->cols);

          // execution

          cblas_dgemm(CblasRowMajor,                  //  1 --> CblasRowMajor, CblasColMajor
                      CblasNoTrans,                   //  2 --> CblasNoTrans, CblasTrans
                      CblasNoTrans,                   //  3 --> same as above
                      static_cast<BLAS_INT>(ROWS_A),  //  4 --> m
                      static_cast<BLAS_INT>(COLS_B),  //  5 --> n
                      static_cast<BLAS_INT>(COLS_A),  //  6 --> k
                      ONE_DBL,                        //  7 --> alpha
                      matA.matrix,                    //  8 --> *a
                      static_cast<BLAS_INT>(COLS_A),  //  9 --> lda
                      matB.matrix,                    // 10 --> *b
                      static_cast<BLAS_INT>(COLS_B),  // 11 --> ldb
                      ZERO_DBL,                       // 12 --> beta
                      mat_tmp.matrix,                 // 13 --> *c
                      static_cast<BLAS_INT>(COLS_B)); // 14 --> ldc

          this->Set(mat_tmp);

          // deallocate tmp matrix

          mat_tmp.Deallocate();

          // return
     } else if((this != &matA) && (this != &matB)) {
          // execution

          cblas_dgemm(CblasRowMajor,                  //  1 --> CblasRowMajor, CblasColMajor
                      CblasNoTrans,                   //  2 --> CblasNoTrans, CblasTrans
                      CblasNoTrans,                   //  3 --> same as above
                      static_cast<BLAS_INT>(ROWS_A),  //  4 --> m
                      static_cast<BLAS_INT>(COLS_B),  //  5 --> n
                      static_cast<BLAS_INT>(COLS_A),  //  6 --> k
                      ONE_DBL,                        //  7 --> alpha
                      matA.matrix,                    //  8 --> *a
                      static_cast<BLAS_INT>(COLS_A),  //  9 --> lda
                      matB.matrix,                    // 10 --> *b
                      static_cast<BLAS_INT>(COLS_B),  // 11 --> ldb
                      ZERO_DBL,                       // 12 --> beta
                      this->matrix,                   // 13 --> *c
                      static_cast<BLAS_INT>(COLS_B)); // 14 --> ldc

          // return
     }
}

// DotBLAS # 2

template<>
void MatrixDense<double>::DotBLAS(const MatrixDense<double> & mat)
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
     mat.CheckAllocation();
#endif

#ifdef DEBUG_COMPAT
     this->CheckCompatibilityDot(*this, mat);
#endif

     // get the dimensions

     const MAI DIM = mat.rows;

     MatrixDense<double> mat_tmp;
     mat_tmp.Allocate(DIM);

     // execution

     cblas_dgemm(CblasRowMajor,               //  1 --> CblasRowMajor, CblasColMajor
                 CblasNoTrans,                //  2 --> CblasNoTrans, CblasTrans
                 CblasNoTrans,                //  3 --> same as above
                 static_cast<BLAS_INT>(DIM),  //  4 --> m
                 static_cast<BLAS_INT>(DIM),  //  5 --> n
                 static_cast<BLAS_INT>(DIM),  //  6 --> k
                 ONE_DBL,                     //  7 --> alpha
                 this->matrix,                //  8 --> *a
                 static_cast<BLAS_INT>(DIM),  //  9 --> lda
                 mat.matrix,                  // 10 --> *b
                 static_cast<BLAS_INT>(DIM),  // 11 --> ldb
                 ZERO_DBL,                    // 12 --> beta
                 mat_tmp.matrix,              // 13 --> *c
                 static_cast<BLAS_INT>(DIM)); // 14 --> ldc

     this->Set(mat_tmp);

     // deallocate tmp matrix

     mat_tmp.Deallocate();

     // return
}

// DotBLAS # 3

template<>
template<typename T2>
void MatrixDense<double>::DotBLAS(const MatrixDense<double> & mat,
                                  const T2 & vec,
                                  const std::string & row_col)
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
     mat.CheckAllocation();
     vec.CheckAllocation();
#endif

     if(row_col == WITH_ROWS) {

#ifdef DEBUG_COMPAT
          vec.CheckCompatibility(mat, LEN_ROW);
          this->CheckCompatibilityRows(mat);
          this->CheckVectorColumnQ();
#endif

          const VEI ROWS = mat.GetNumberOfRows();

          T2 vec_tmp;

          vec_tmp.Allocate(ROWS);

          vec_tmp.DotBLAS(mat, vec, row_col);

          this->SetColumn(0, vec_tmp);

          vec_tmp.Deallocate();
     } else if(row_col == WITH_COLS) {

#ifdef DEBUG_COMPAT
          vec.CheckCompatibility(mat, LEN_COL);
          this->CheckCompatibilityColumns(mat);
          this->CheckVectorRowQ();
#endif

          const VEI COLS = mat.GetNumberOfColumns();

          T2 vec_tmp;

          vec_tmp.Allocate(COLS);

          vec_tmp.DotBLAS(mat, vec, row_col);

          this->SetRow(0, vec_tmp);

          vec_tmp.Deallocate();
     } else {
          functions::Exit(E_MAT_VEC_DOT_TYPE_NOT);
     }
}

// DotBLAS # 4

template<>
template<typename T2>
double MatrixDense<double>::DotBLAS(const T2 & vec,
                                    const std::string & row_col) const
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
     vec.CheckAllocation();
#endif

     T2 vec_tmp;
     const VEI ELEMS = vec.GetNumberOfElements();
     double res = 0.0;

     if(row_col == WITH_ROWS) {

#ifdef DEBUG_COMPAT
          vec.CheckCompatibility(*this, LEN_ROW);
          this->CheckVectorRowQ();
#endif

          vec_tmp.Allocate(ELEMS);

          vec_tmp.GetRow(*this, 0);

          res = vec_tmp.DotBLAS(vec);

          vec_tmp.Deallocate();
     } else if(row_col == WITH_COLS) {

#ifdef DEBUG_COMPAT
          vec.CheckCompatibility(*this, LEN_COL);
          this->CheckVectorColumnQ();
#endif

          vec_tmp.Allocate(ELEMS);

          vec_tmp.GetColumn(*this, 0);

          res = vec_tmp.DotBLAS(vec);

          vec_tmp.Deallocate();
     } else {
          functions::Exit(E_MAT_VEC_DOT_TYPE_NOT);
     }

     return res;
}

// DotBLAS # 5

template<>
void MatrixDense<double>::DotBLAS()
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
#endif

     // get the dimensions

     const MAI DIM = this->rows;

     MatrixDense<double> mat_tmp;
     mat_tmp.Allocate(DIM);

     // execution

     cblas_dgemm(CblasRowMajor,               //  1 --> CblasRowMajor, CblasColMajor
                 CblasNoTrans,                //  2 --> CblasNoTrans, CblasTrans
                 CblasNoTrans,                //  3 --> same as above
                 static_cast<BLAS_INT>(DIM),  //  4 --> m
                 static_cast<BLAS_INT>(DIM),  //  5 --> n
                 static_cast<BLAS_INT>(DIM),  //  6 --> k
                 ONE_DBL,                     //  7 --> alpha
                 this->matrix,                //  8 --> *a
                 static_cast<BLAS_INT>(DIM),  //  9 --> lda
                 this->matrix,                // 10 --> *b
                 static_cast<BLAS_INT>(DIM),  // 11 --> ldb
                 ZERO_DBL,                    // 12 --> beta
                 mat_tmp.matrix,              // 13 --> *c
                 static_cast<BLAS_INT>(DIM)); // 14 --> ldc

     this->Set(mat_tmp);

     // deallocate tmp matrix

     mat_tmp.Deallocate();

     // return
}

} // sep
} // pgg

#endif // MATRIX_DENSE_BLAS_H

