
#ifndef MATRIX_DENSE_GET_H
#define MATRIX_DENSE_GET_H

namespace pgg {
namespace sep {

// GetColumn

template<typename T1>
template<typename T2>
void MatrixDense<T1>::GetColumn(const MAI & j_col,
                                T2 & vec) const
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
     vec.CheckAllocation();
#endif

     const MAI ROWS = this->GetNumberOfRows();

#ifdef DEBUG_COMPAT
     const VEI ELEMS = vec.GetNumberOfElements();

     if (ELEMS != static_cast<VEI>(ROWS)) {
          functions::Exit(E_MAT_VEC_CONFORM_NOT);
     }
#endif

     MAI i;

     #pragma omp parallel default(none) \
     num_threads(NT_1D) \
     private(i)    \
     shared(j_col) \
     shared(vec)
     {
          #pragma omp for

          for (i = 0; i < ROWS; ++i) {
               vec.SetElement(static_cast<VEI>(i),
                              this->GetElement(i, j_col));
          }
     }

     // return
}

// GetElement # 1

template<typename T1>
T1 MatrixDense<T1>::GetElement(const MAI & i_row,
                               const MAI & j_col) const
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
#endif

     // get number of columns

     const MAI COLS = this->cols;
     const MAI_MAX COLS_LONG = static_cast<MAI_MAX>(COLS);

     // return

     return this->matrix[i_row * COLS_LONG + j_col];
}

// GetElement # 2

template<typename T1>
T1 MatrixDense<T1>::GetElement(const MAI_MAX & index) const
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
#endif

     // return

     return this->matrix[index];
}

// GetNumberOfColumns

template<typename T1>
MAI MatrixDense<T1>::GetNumberOfColumns() const
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
#endif

     // return

     return this->cols;
}

// GetNumberOfRows

template<typename T1>
MAI MatrixDense<T1>::GetNumberOfRows() const
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
#endif

     return this->rows;
}

// GetRow

template<typename T1>
template<typename T2>
void MatrixDense<T1>::GetRow(const MAI & i_row,
                             T2 & vec) const
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
     vec.CheckAllocation();
#endif

     const MAI COLS = this->GetNumberOfColumns();

#ifdef DEBUG_COMPAT
     const VEI ELEMS = vec.GetNumberOfElements();

     if (ELEMS != static_cast<VEI>(COLS)) {
          functions::Exit(E_MAT_VEC_CONFORM_NOT);
     }
#endif

     MAI j;

     #pragma omp parallel default(none) \
     num_threads(NT_1D) \
     private(j)    \
     shared(i_row) \
     shared(vec)
     {
          #pragma omp for

          for (j = 0; j < COLS; ++j) {
               vec.SetElement(static_cast<VEI>(j),
                              this->GetElement(i_row, j));
          }
     }

     // return
}

} // sep
} // pgg

#endif // MATRIX_DENSE_GET_H

