
#ifndef MATRIX_DENSE_LAPACK_DIAGONALIZERS_H
#define MATRIX_DENSE_LAPACK_DIAGONALIZERS_H

#include <string>
#include <complex>
#include "../head/sepolia_mkl_lapacke.hpp"

namespace pgg {
namespace sep {

//===============================//
// Eigensystem Complex Hermitian //
//===============================//

// EigensystemZHEEV # 1

template<>
template<typename T2>
LAPACK_INT MatrixDense<std::complex<double>>::EigensystemZHEEV(
               MatrixDense<std::complex<double>> & eigenvectors,
               T2 & eigenvalues) const
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
     eigenvectors.CheckAllocation();
     eigenvalues.CheckAllocation();
#endif

#ifdef DEBUG_COMPAT
     this->CheckSquareQ();
     eigenvectors.CheckSquareQ();
#endif

     const MAI ROWS = this->GetNumberOfRows();

#ifdef DEBUG_COMPAT
     const VEI ELEMS = eigenvalues.GetNumberOfElements();

     if (ELEMS != static_cast<VEI>(ROWS)) {
          functions::Exit(E_MAT_VEC_CONFORM_NOT);
     }

     this->CheckCompatibility(eigenvectors);
     this->CheckHermitianQ();
#endif

     LAPACK_INT res;

     eigenvectors.Set(*this);

     res = LAPACKE_zheev(LAPACK_ROW_MAJOR,              //  1
                         'V',                           //  2
                         'U',                           //  3
                         static_cast<LAPACK_INT>(ROWS), //  4
                         eigenvectors.matrix,           //  5
                         static_cast<LAPACK_INT>(ROWS), //  6
                         eigenvalues.vector);           //  7

     return res;
}

// EigensystemZHEEVD # 2

template<>
template<typename T2>
LAPACK_INT MatrixDense<std::complex<double>>::EigensystemZHEEVD(
               MatrixDense<std::complex<double>> & eigenvectors,
               T2 & eigenvalues) const
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
     eigenvectors.CheckAllocation();
     eigenvalues.CheckAllocation();
#endif

#ifdef DEBUG_COMPAT
     this->CheckSquareQ();
     eigenvectors.CheckSquareQ();
#endif

     const MAI ROWS = this->GetNumberOfRows();

#ifdef DEBUG_COMPAT
     const VEI ELEMS = eigenvalues.GetNumberOfElements();

     if (ELEMS != static_cast<VEI>(ROWS)) {
          functions::Exit(E_MAT_VEC_CONFORM_NOT);
     }

     this->CheckCompatibility(eigenvectors);
     this->CheckHermitianQ();
#endif

     LAPACK_INT res;

     eigenvectors.Set(*this);

     res = LAPACKE_zheevd(LAPACK_ROW_MAJOR,              //  1
                          'V',                           //  2
                          'U',                           //  3
                          static_cast<LAPACK_INT>(ROWS), //  4
                          eigenvectors.matrix,           //  5
                          static_cast<LAPACK_INT>(ROWS), //  6
                          eigenvalues.vector);           //  7

     return res;
}

// EigensystemZHEEVR # 3

template<>
template<typename T2>
LAPACK_INT MatrixDense<std::complex<double>>::EigensystemZHEEVR(
               MatrixDense<std::complex<double>> & eigenvectors,
               T2 & eigenvalues) const
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
     eigenvectors.CheckAllocation();
     eigenvalues.CheckAllocation();
#endif

#ifdef DEBUG_COMPAT
     this->CheckSquareQ();
     eigenvectors.CheckSquareQ();
#endif

     const MAI ROWS = this->GetNumberOfRows();

#ifdef DEBUG_COMPAT
     const VEI ELEMS = eigenvalues.GetNumberOfElements();

     if (ELEMS != static_cast<VEI>(ROWS)) {
          functions::Exit(E_MAT_VEC_CONFORM_NOT);
     }

     this->CheckCompatibility(eigenvectors);
     this->CheckHermitianQ();
#endif

     LAPACK_INT res;

     eigenvectors.Set(*this);

     double vl = 0.0;
     double vu = 0.0;
     LAPACK_INT il = 0;
     LAPACK_INT iu = 0;
     LAPACK_INT m;
     MatrixDense<LAPACK_INT> isuppz;
     isuppz.Allocate(1, 2*ROWS);

     res = LAPACKE_zheevr(LAPACK_ROW_MAJOR,              //  1
                          'V',                           //  2
                          'A',                           //  3
                          'U',                           //  4
                          static_cast<LAPACK_INT>(ROWS), //  5
                          eigenvectors.matrix,           //  6
                          static_cast<LAPACK_INT>(ROWS), //  7
                          vl,                            //  8
                          vu,                            //  9
                          il,                            // 10
                          iu,                            // 11
                          ZERO_DBL,                      // 12
                          &m,                            // 13
                          eigenvalues.vector,            // 14
                          eigenvectors.matrix,           // 15
                          static_cast<LAPACK_INT>(ROWS), // 16
                          isuppz.matrix);                // 17

     return res;
}

// EigensystemZHEEVX # 4

template<>
template<typename T2>
LAPACK_INT MatrixDense<std::complex<double>>::EigensystemZHEEVX(
               MatrixDense<std::complex<double>> & eigenvectors,
               T2 & eigenvalues) const
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
     eigenvectors.CheckAllocation();
     eigenvalues.CheckAllocation();
#endif

#ifdef DEBUG_COMPAT
     this->CheckSquareQ();
     eigenvectors.CheckSquareQ();
#endif

     const MAI ROWS = this->GetNumberOfRows();

#ifdef DEBUG_COMPAT
     const VEI ELEMS = eigenvalues.GetNumberOfElements();

     if (ELEMS != static_cast<VEI>(ROWS)) {
          functions::Exit(E_MAT_VEC_CONFORM_NOT);
     }

     this->CheckCompatibility(eigenvectors);
     this->CheckHermitianQ();
#endif

     LAPACK_INT res;

     eigenvectors.Set(*this);

     double vl = 0.0;
     double vu = 0.0;
     LAPACK_INT il = 0;
     LAPACK_INT iu = 0;
     LAPACK_INT m;
     MatrixDense<int> ifail;
     ifail.Allocate(1, ROWS);

     res = LAPACKE_zheevx(LAPACK_ROW_MAJOR,              //  1
                          'V',                           //  2
                          'A',                           //  3
                          'U',                           //  4
                          static_cast<LAPACK_INT>(ROWS), //  5
                          eigenvectors.matrix,           //  6
                          static_cast<LAPACK_INT>(ROWS), //  7
                          vl,                            //  8
                          vu,                            //  9
                          il,                            // 10
                          iu,                            // 11
                          ZERO_DBL,                      // 12
                          &m,                            // 13
                          eigenvalues.vector,            // 14
                          eigenvectors.matrix,           // 15
                          static_cast<LAPACK_INT>(ROWS), // 16
                          ifail.matrix);                 // 17

     // local deallocations

     ifail.Deallocate();

     return res;
}

//============================//
// Eigensystem Real Symmetric //
//============================//

// EigensystemDSYEV # 1

template<>
template<typename T2>
LAPACK_INT MatrixDense<double>::EigensystemDSYEV(
     MatrixDense<double> & eigenvectors,
     T2 & eigenvalues) const
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
     eigenvectors.CheckAllocation();
     eigenvalues.CheckAllocation();
#endif

#ifdef DEBUG_COMPAT
     this->CheckSquareQ();
     eigenvectors.CheckSquareQ();
#endif

     const MAI ROWS = this->GetNumberOfRows();

#ifdef DEBUG_COMPAT
     const VEI ELEMS = eigenvalues.GetNumberOfElements();

     if (ELEMS != static_cast<VEI>(ROWS)) {
          functions::Exit(E_MAT_VEC_CONFORM_NOT);
     }

     this->CheckCompatibility(eigenvectors);
     this->CheckSymmetricQ();
#endif

     LAPACK_INT res;

     eigenvectors.Set(*this);

     res = LAPACKE_dsyev(LAPACK_ROW_MAJOR,              //  1
                         'V',                           //  2
                         'U',                           //  3
                         static_cast<LAPACK_INT>(ROWS), //  4
                         eigenvectors.matrix,           //  5
                         static_cast<LAPACK_INT>(ROWS), //  6
                         eigenvalues.vector);           //  7

     return res;
}

// EigensystemDSYEVD # 2

template<>
template<typename T2>
LAPACK_INT MatrixDense<double>::EigensystemDSYEVD(
     MatrixDense<double> & eigenvectors,
     T2 & eigenvalues) const
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
     eigenvectors.CheckAllocation();
     eigenvalues.CheckAllocation();
#endif

#ifdef DEBUG_COMPAT
     this->CheckSquareQ();
     eigenvectors.CheckSquareQ();
#endif

     const MAI ROWS = this->GetNumberOfRows();

#ifdef DEBUG_COMPAT
     const VEI ELEMS = eigenvalues.GetNumberOfElements();

     if (ELEMS != static_cast<VEI>(ROWS)) {
          functions::Exit(E_MAT_VEC_CONFORM_NOT);
     }

     this->CheckCompatibility(eigenvectors);
     this->CheckSymmetricQ();
#endif

     LAPACK_INT res;

     eigenvectors.Set(*this);

     res = LAPACKE_dsyevd(LAPACK_ROW_MAJOR,              //  1
                          'V',                           //  2
                          'U',                           //  3
                          static_cast<LAPACK_INT>(ROWS), //  4
                          eigenvectors.matrix,           //  5
                          static_cast<LAPACK_INT>(ROWS), //  6
                          eigenvalues.vector);           //  7

     return res;
}

// EigensystemDSYEVR # 3

template<>
template<typename T2>
LAPACK_INT MatrixDense<double>::EigensystemDSYEVR(
     MatrixDense<double> & eigenvectors,
     T2 & eigenvalues) const
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
     eigenvectors.CheckAllocation();
     eigenvalues.CheckAllocation();
#endif

#ifdef DEBUG_COMPAT
     this->CheckSquareQ();
     eigenvectors.CheckSquareQ();
#endif

     const MAI ROWS = this->GetNumberOfRows();

#ifdef DEBUG_COMPAT
     const VEI ELEMS = eigenvalues.GetNumberOfElements();

     if (ELEMS != static_cast<VEI>(ROWS)) {
          functions::Exit(E_MAT_VEC_CONFORM_NOT);
     }

     this->CheckCompatibility(eigenvectors);
     this->CheckSymmetricQ();
#endif

     LAPACK_INT res;

     eigenvectors.Set(*this);

     double vl = 0.0;
     double vu = 0.0;
     LAPACK_INT il = 0;
     LAPACK_INT iu = 0;
     LAPACK_INT m;
     MatrixDense<LAPACK_INT> isuppz;
     isuppz.Allocate(1, 2*ROWS);

     res = LAPACKE_dsyevr(LAPACK_ROW_MAJOR,              //  1
                          'V',                           //  2
                          'A',                           //  3
                          'U',                           //  4
                          static_cast<LAPACK_INT>(ROWS), //  5
                          eigenvectors.matrix,           //  6
                          static_cast<LAPACK_INT>(ROWS), //  7
                          vl,                            //  8
                          vu,                            //  9
                          il,                            // 10
                          iu,                            // 11
                          ZERO_DBL,                      // 12
                          &m,                            // 13
                          eigenvalues.vector,            // 14
                          eigenvectors.matrix,           // 15
                          static_cast<LAPACK_INT>(ROWS), // 16
                          isuppz.matrix);                // 17

     return res;
}

// EigensystemDSYEVX # 4

template<>
template<typename T2>
LAPACK_INT MatrixDense<double>::EigensystemDSYEVX(
     MatrixDense<double> & eigenvectors,
     T2 & eigenvalues) const
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
     eigenvectors.CheckAllocation();
     eigenvalues.CheckAllocation();
#endif

#ifdef DEBUG_COMPAT
     this->CheckSquareQ();
     eigenvectors.CheckSquareQ();
#endif

     const MAI ROWS = this->GetNumberOfRows();

#ifdef DEBUG_COMPAT
     const VEI ELEMS = eigenvalues.GetNumberOfElements();

     if (ELEMS != static_cast<VEI>(ROWS)) {
          functions::Exit(E_MAT_VEC_CONFORM_NOT);
     }

     this->CheckCompatibility(eigenvectors);
     this->CheckSymmetricQ();
#endif

     LAPACK_INT res;

     eigenvectors.Set(*this);

     double vl = 0.0;
     double vu = 0.0;
     LAPACK_INT il = 0;
     LAPACK_INT iu = 0;
     LAPACK_INT m;
     MatrixDense<int> ifail;
     ifail.Allocate(1, ROWS);

     res = LAPACKE_dsyevx(LAPACK_ROW_MAJOR,              //  1
                          'V',                           //  2
                          'A',                           //  3
                          'U',                           //  4
                          static_cast<LAPACK_INT>(ROWS), //  5
                          eigenvectors.matrix,           //  6
                          static_cast<LAPACK_INT>(ROWS), //  7
                          vl,                            //  8
                          vu,                            //  9
                          il,                            // 10
                          iu,                            // 11
                          ZERO_DBL,                      // 12
                          &m,                            // 13
                          eigenvalues.vector,            // 14
                          eigenvectors.matrix,           // 15
                          static_cast<LAPACK_INT>(ROWS), // 16
                          ifail.matrix);                 // 17

     // local deallocations

     ifail.Deallocate();

     return res;
}

//===============================//
// Eigenvalues Complex Hermitian //
//===============================//

// EigenvaluesZHEEV # 1

template<>
template<typename T2>
LAPACK_INT MatrixDense<std::complex<double>>::EigenvaluesZHEEV(T2 & eigenvalues) const
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
     eigenvalues.CheckAllocation();
#endif

#ifdef DEBUG_COMPAT
     this->CheckSquareQ();
#endif

     const MAI ROWS = this->GetNumberOfRows();

#ifdef DEBUG_COMPAT
     const VEI ELEMS = eigenvalues.GetNumberOfElements();

     if (ELEMS != static_cast<VEI>(ROWS)) {
          functions::Exit(E_MAT_VEC_CONFORM_NOT);
     }

     this->CheckHermitianQ();
#endif

     LAPACK_INT res;

     MatrixDense<std::complex<double>> tmp_mat;
     tmp_mat.Allocate(ROWS);
     tmp_mat.Set(*this);

     res = LAPACKE_zheev(LAPACK_ROW_MAJOR,              //  1
                         'N',                           //  2
                         'U',                           //  3
                         static_cast<LAPACK_INT>(ROWS), //  4
                         tmp_mat.matrix,                //  5
                         static_cast<LAPACK_INT>(ROWS), //  6
                         eigenvalues.vector);           //  7

     return res;
}

// EigenvaluesZHEEVD # 2

template<>
template<typename T2>
LAPACK_INT MatrixDense<std::complex<double>>::EigenvaluesZHEEVD(T2 & eigenvalues) const
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
     eigenvalues.CheckAllocation();
#endif

#ifdef DEBUG_COMPAT
     this->CheckSquareQ();
#endif

     const MAI ROWS = this->GetNumberOfRows();

#ifdef DEBUG_COMPAT
     const VEI ELEMS = eigenvalues.GetNumberOfElements();

     if (ELEMS != static_cast<VEI>(ROWS)) {
          functions::Exit(E_MAT_VEC_CONFORM_NOT);
     }

     this->CheckHermitianQ();
#endif

     LAPACK_INT res;
     MatrixDense<std::complex<double>> tmp_mat;
     tmp_mat.Allocate(ROWS);
     tmp_mat.Set(*this);

     res = LAPACKE_zheevd(LAPACK_ROW_MAJOR,              //  1
                          'N',                           //  2
                          'U',                           //  3
                          static_cast<LAPACK_INT>(ROWS), //  4
                          tmp_mat.matrix,                //  5
                          static_cast<LAPACK_INT>(ROWS), //  6
                          eigenvalues.vector);           //  7

     return res;
}

// EigenvaluesZHEEVR # 3

template<>
template<typename T2>
LAPACK_INT MatrixDense<std::complex<double>>::EigenvaluesZHEEVR(
               T2 & eigenvalues) const
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
     eigenvalues.CheckAllocation();
#endif

#ifdef DEBUG_COMPAT
     this->CheckSquareQ();
#endif

     const MAI ROWS = this->GetNumberOfRows();

#ifdef DEBUG_COMPAT
     const VEI ELEMS = eigenvalues.GetNumberOfElements();

     if (ELEMS != static_cast<VEI>(ROWS)) {
          functions::Exit(E_MAT_VEC_CONFORM_NOT);
     }

     this->CheckHermitianQ();
#endif

     MatrixDense<std::complex<double>> eigenvectors;
     eigenvectors.Allocate(ROWS);

     LAPACK_INT res;

     eigenvectors.Set(*this);

     double vl = 0.0;
     double vu = 0.0;
     LAPACK_INT il = 0;
     LAPACK_INT iu = 0;
     LAPACK_INT m;
     MatrixDense<LAPACK_INT> isuppz;
     isuppz.Allocate(1, 2*ROWS);

     res = LAPACKE_zheevr(LAPACK_ROW_MAJOR,              //  1
                          'N',                           //  2
                          'A',                           //  3
                          'U',                           //  4
                          static_cast<LAPACK_INT>(ROWS), //  5
                          eigenvectors.matrix,           //  6
                          static_cast<LAPACK_INT>(ROWS), //  7
                          vl,                            //  8
                          vu,                            //  9
                          il,                            // 10
                          iu,                            // 11
                          ZERO_DBL,                      // 12
                          &m,                            // 13
                          eigenvalues.vector,            // 14
                          eigenvectors.matrix,           // 15
                          static_cast<LAPACK_INT>(ROWS), // 16
                          isuppz.matrix);                // 17

     // local deallocations

     isuppz.Deallocate();
     eigenvectors.Deallocate();

     return res;
}

// EigenvaluesZHEEVX # 4

template<>
template<typename T2>
LAPACK_INT MatrixDense<std::complex<double>>::EigenvaluesZHEEVX(
               T2 & eigenvalues) const
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
     eigenvalues.CheckAllocation();
#endif

#ifdef DEBUG_COMPAT
     this->CheckSquareQ();
#endif

     const MAI ROWS = this->GetNumberOfRows();

#ifdef DEBUG_COMPAT
     const VEI ELEMS = eigenvalues.GetNumberOfElements();

     if (ELEMS != static_cast<VEI>(ROWS)) {
          functions::Exit(E_MAT_VEC_CONFORM_NOT);
     }

     this->CheckHermitianQ();
#endif

     MatrixDense<std::complex<double>> eigenvectors;
     eigenvectors.Allocate(ROWS);

     LAPACK_INT res;

     eigenvectors.Set(*this);

     double vl = 0.0;
     double vu = 0.0;
     LAPACK_INT il = 0;
     LAPACK_INT iu = 0;
     LAPACK_INT m;
     MatrixDense<int> ifail;
     ifail.Allocate(1, ROWS);

     res = LAPACKE_zheevx(LAPACK_ROW_MAJOR,              //  1
                          'N',                           //  2
                          'A',                           //  3
                          'U',                           //  4
                          static_cast<LAPACK_INT>(ROWS), //  5
                          eigenvectors.matrix,           //  6
                          static_cast<LAPACK_INT>(ROWS), //  7
                          vl,                            //  8
                          vu,                            //  9
                          il,                            // 10
                          iu,                            // 11
                          ZERO_DBL,                      // 12
                          &m,                            // 13
                          eigenvalues.vector,            // 14
                          eigenvectors.matrix,           // 15
                          static_cast<LAPACK_INT>(ROWS), // 16
                          ifail.matrix);                 // 17

     // local deallocations

     ifail.Deallocate();
     eigenvectors.Deallocate();

     return res;
}

//============================//
// Eigenvalues Real Symmetric //
//============================//

// EigenvaluesDSYEV # 1

template<>
template<typename T2>
LAPACK_INT MatrixDense<double>::EigenvaluesDSYEV(T2 & eigenvalues) const
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
     eigenvalues.CheckAllocation();
#endif

#ifdef DEBUG_COMPAT
     this->CheckSquareQ();
#endif

     const MAI ROWS = this->GetNumberOfRows();

#ifdef DEBUG_COMPAT
     const VEI ELEMS = eigenvalues.GetNumberOfElements();

     if (ELEMS != static_cast<VEI>(ROWS)) {
          functions::Exit(E_MAT_VEC_CONFORM_NOT);
     }

     this->CheckSymmetricQ();
#endif

     LAPACK_INT res;

     MatrixDense<double> tmp_mat;
     tmp_mat.Allocate(ROWS);
     tmp_mat.Set(*this);

     res = LAPACKE_dsyev(LAPACK_ROW_MAJOR,              //  1
                         'N',                           //  2
                         'U',                           //  3
                         static_cast<LAPACK_INT>(ROWS), //  4
                         tmp_mat.matrix,                //  5
                         static_cast<LAPACK_INT>(ROWS), //  6
                         eigenvalues.vector);           //  7

     return res;
}

// EigenvaluesDSYEVD # 2

template<>
template<typename T2>
LAPACK_INT MatrixDense<double>::EigenvaluesDSYEVD(T2 & eigenvalues) const
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
     eigenvalues.CheckAllocation();
#endif

#ifdef DEBUG_COMPAT
     this->CheckSquareQ();
#endif

     const MAI ROWS = this->GetNumberOfRows();

#ifdef DEBUG_COMPAT
     const VEI ELEMS = eigenvalues.GetNumberOfElements();

     if (ELEMS != static_cast<VEI>(ROWS)) {
          functions::Exit(E_MAT_VEC_CONFORM_NOT);
     }

     this->CheckSymmetricQ();
#endif

     LAPACK_INT res;
     MatrixDense<double> tmp_mat;
     tmp_mat.Allocate(ROWS);
     tmp_mat.Set(*this);

     res = LAPACKE_dsyevd(LAPACK_ROW_MAJOR,              //  1
                          'N',                           //  2
                          'U',                           //  3
                          static_cast<LAPACK_INT>(ROWS), //  4
                          tmp_mat.matrix,                //  5
                          static_cast<LAPACK_INT>(ROWS), //  6
                          eigenvalues.vector);           //  7

     return res;
}

// EigenvaluesDSYEVR # 3

template<>
template<typename T2>
LAPACK_INT MatrixDense<double>::EigenvaluesDSYEVR(
     T2 & eigenvalues) const
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
     eigenvalues.CheckAllocation();
#endif

#ifdef DEBUG_COMPAT
     this->CheckSquareQ();
#endif

     const MAI ROWS = this->GetNumberOfRows();

#ifdef DEBUG_COMPAT
     const VEI ELEMS = eigenvalues.GetNumberOfElements();

     if (ELEMS != static_cast<VEI>(ROWS)) {
          functions::Exit(E_MAT_VEC_CONFORM_NOT);
     }

     this->CheckSymmetricQ();
#endif

     MatrixDense<double> eigenvectors;
     eigenvectors.Allocate(ROWS);

     LAPACK_INT res;

     eigenvectors.Set(*this);

     double vl = 0.0;
     double vu = 0.0;
     LAPACK_INT il = 0;
     LAPACK_INT iu = 0;
     LAPACK_INT m;
     MatrixDense<LAPACK_INT> isuppz;
     isuppz.Allocate(1, 2*ROWS);

     res = LAPACKE_dsyevr(LAPACK_ROW_MAJOR,              //  1
                          'N',                           //  2
                          'A',                           //  3
                          'U',                           //  4
                          static_cast<LAPACK_INT>(ROWS), //  5
                          eigenvectors.matrix,           //  6
                          static_cast<LAPACK_INT>(ROWS), //  7
                          vl,                            //  8
                          vu,                            //  9
                          il,                            // 10
                          iu,                            // 11
                          ZERO_DBL,                      // 12
                          &m,                            // 13
                          eigenvalues.vector,            // 14
                          eigenvectors.matrix,           // 15
                          static_cast<LAPACK_INT>(ROWS), // 16
                          isuppz.matrix);                // 17

     // local deallocations

     isuppz.Deallocate();
     eigenvectors.Deallocate();

     return res;
}

// EigenvaluesDSYEVX # 4

template<>
template<typename T2>
LAPACK_INT MatrixDense<double>::EigenvaluesDSYEVX(
     T2 & eigenvalues) const
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
     eigenvalues.CheckAllocation();
#endif

#ifdef DEBUG_COMPAT
     this->CheckSquareQ();
#endif

     const MAI ROWS = this->GetNumberOfRows();

#ifdef DEBUG_COMPAT
     const VEI ELEMS = eigenvalues.GetNumberOfElements();

     if (ELEMS != static_cast<VEI>(ROWS)) {
          functions::Exit(E_MAT_VEC_CONFORM_NOT);
     }

     this->CheckSymmetricQ();
#endif

     MatrixDense<double> eigenvectors;
     eigenvectors.Allocate(ROWS);

     LAPACK_INT res;

     eigenvectors.Set(*this);

     double vl = 0.0;
     double vu = 0.0;
     LAPACK_INT il = 0;
     LAPACK_INT iu = 0;
     LAPACK_INT m;
     MatrixDense<int> ifail;
     ifail.Allocate(1, ROWS);

     res = LAPACKE_dsyevx(LAPACK_ROW_MAJOR,              //  1
                          'N',                           //  2
                          'A',                           //  3
                          'U',                           //  4
                          static_cast<LAPACK_INT>(ROWS), //  5
                          eigenvectors.matrix,           //  6
                          static_cast<LAPACK_INT>(ROWS), //  7
                          vl,                            //  8
                          vu,                            //  9
                          il,                            // 10
                          iu,                            // 11
                          ZERO_DBL,                      // 12
                          &m,                            // 13
                          eigenvalues.vector,            // 14
                          eigenvectors.matrix,           // 15
                          static_cast<LAPACK_INT>(ROWS), // 16
                          ifail.matrix);                 // 17

     // local deallocations

     ifail.Deallocate();
     eigenvectors.Deallocate();

     return res;
}

//================================//
// Eigenvectors Complex Hermitian //
//================================//

// EigenvectorsZHEEV # 1

template<>
LAPACK_INT MatrixDense<std::complex<double>>::EigenvectorsZHEEV(
               MatrixDense<std::complex<double>> & eigenvectors) const
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
     eigenvectors.CheckAllocation();
#endif

#ifdef DEBUG_COMPAT
     this->CheckSquareQ();
     eigenvectors.CheckSquareQ();
     this->CheckCompatibility(eigenvectors);
#endif

     const MAI ROWS = this->GetNumberOfRows();
     MatrixDense<double> tmp_mat;
     tmp_mat.Allocate(1, ROWS);

#ifdef DEBUG_COMPAT
     this->CheckHermitianQ();
#endif

     LAPACK_INT res;

     eigenvectors.Set(*this);

     res = LAPACKE_zheev(LAPACK_ROW_MAJOR,              //  1
                         'V',                           //  2
                         'U',                           //  3
                         static_cast<LAPACK_INT>(ROWS), //  4
                         eigenvectors.matrix,           //  5
                         static_cast<LAPACK_INT>(ROWS), //  6
                         tmp_mat.matrix);               //  7

     // local deallocations

     tmp_mat.Deallocate();

     return res;
}

// EigenvectorsZHEEVD # 2

template<>
LAPACK_INT MatrixDense<std::complex<double>>::EigenvectorsZHEEVD(
               MatrixDense<std::complex<double>> & eigenvectors) const
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
     eigenvectors.CheckAllocation();
#endif

#ifdef DEBUG_COMPAT
     this->CheckSquareQ();
     eigenvectors.CheckSquareQ();
     this->CheckCompatibility(eigenvectors);
#endif

     const MAI ROWS = this->GetNumberOfRows();
     MatrixDense<double> tmp_mat;
     tmp_mat.Allocate(1, ROWS);

#ifdef DEBUG_COMPAT
     this->CheckHermitianQ();
#endif

     LAPACK_INT res;

     eigenvectors.Set(*this);

     res = LAPACKE_zheevd(LAPACK_ROW_MAJOR,              //  1
                          'V',                           //  2
                          'U',                           //  3
                          static_cast<LAPACK_INT>(ROWS), //  4
                          eigenvectors.matrix,           //  5
                          static_cast<LAPACK_INT>(ROWS), //  6
                          tmp_mat.matrix);               //  7

     // local deallocations

     tmp_mat.Deallocate();

     return res;
}

// EigenvectorsZHEEVR # 3

template<>
LAPACK_INT MatrixDense<std::complex<double>>::EigenvectorsZHEEVR(
               MatrixDense<std::complex<double>> & eigenvectors) const
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
     eigenvectors.CheckAllocation();
#endif

#ifdef DEBUG_COMPAT
     this->CheckSquareQ();
     eigenvectors.CheckSquareQ();
     this->CheckCompatibility(eigenvectors);
     this->CheckHermitianQ();
#endif

     LAPACK_INT res;

     eigenvectors.Set(*this);

     double vl = 0.0;
     double vu = 0.0;
     LAPACK_INT il = 0;
     LAPACK_INT iu = 0;
     LAPACK_INT m;
     MatrixDense<LAPACK_INT> isuppz;
     const MAI ROWS = this->GetNumberOfRows();
     isuppz.Allocate(1, 2*ROWS);
     MatrixDense<double> tmp_matA;
     tmp_matA.Allocate(1, ROWS);

     res = LAPACKE_zheevr(LAPACK_ROW_MAJOR,              //  1
                          'V',                           //  2
                          'A',                           //  3
                          'U',                           //  4
                          static_cast<LAPACK_INT>(ROWS), //  5
                          eigenvectors.matrix,           //  6
                          static_cast<LAPACK_INT>(ROWS), //  7
                          vl,                            //  8
                          vu,                            //  9
                          il,                            // 10
                          iu,                            // 11
                          ZERO_DBL,                      // 12
                          &m,                            // 13
                          tmp_matA.matrix,               // 14
                          eigenvectors.matrix,           // 15
                          static_cast<LAPACK_INT>(ROWS), // 16
                          isuppz.matrix);                // 17

     // local deallocations

     isuppz.Deallocate();
     tmp_matA.Deallocate();

     return res;
}

// EigenvectorsZHEEVX # 4

template<>
LAPACK_INT MatrixDense<std::complex<double>>::EigenvectorsZHEEVX(
               MatrixDense<std::complex<double>> & eigenvectors) const
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
     eigenvectors.CheckAllocation();
#endif

#ifdef DEBUG_COMPAT
     this->CheckSquareQ();
     eigenvectors.CheckSquareQ();
     this->CheckCompatibility(eigenvectors);
     this->CheckHermitianQ();
#endif

     LAPACK_INT res;

     eigenvectors.Set(*this);

     double vl = 0.0;
     double vu = 0.0;
     LAPACK_INT il = 0;
     LAPACK_INT iu = 0;
     LAPACK_INT m;
     MatrixDense<int> ifail;
     const MAI ROWS = this->GetNumberOfRows();
     ifail.Allocate(1, ROWS);
     MatrixDense<double> tmp_matA;
     tmp_matA.Allocate(1, ROWS);

     res = LAPACKE_zheevx(LAPACK_ROW_MAJOR,              //  1
                          'V',                           //  2
                          'A',                           //  3
                          'U',                           //  4
                          static_cast<LAPACK_INT>(ROWS), //  5
                          eigenvectors.matrix,           //  6
                          static_cast<LAPACK_INT>(ROWS), //  7
                          vl,                            //  8
                          vu,                            //  9
                          il,                            // 10
                          iu,                            // 11
                          ZERO_DBL,                      // 12
                          &m,                            // 13
                          tmp_matA.matrix,               // 14
                          eigenvectors.matrix,           // 15
                          static_cast<LAPACK_INT>(ROWS), // 16
                          ifail.matrix);                 // 17

     // local deallocations

     ifail.Deallocate();
     tmp_matA.Deallocate();

     return res;
}

//=============================//
// Eigenvectors Real Symmetric //
//=============================//

// EigenvectorsDSYEV # 1

template<>
LAPACK_INT MatrixDense<double>::EigenvectorsDSYEV(
     MatrixDense<double> & eigenvectors) const
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
     eigenvectors.CheckAllocation();
#endif

#ifdef DEBUG_COMPAT
     this->CheckSquareQ();
     eigenvectors.CheckSquareQ();
     this->CheckCompatibility(eigenvectors);
     this->CheckSymmetricQ();
#endif

     const MAI ROWS = this->GetNumberOfRows();
     MatrixDense<double> tmp_mat;
     tmp_mat.Allocate(1, ROWS);

     LAPACK_INT res;

     eigenvectors.Set(*this);

     res = LAPACKE_dsyev(LAPACK_ROW_MAJOR,              //  1
                         'V',                           //  2
                         'U',                           //  3
                         static_cast<LAPACK_INT>(ROWS), //  4
                         eigenvectors.matrix,           //  5
                         static_cast<LAPACK_INT>(ROWS), //  6
                         tmp_mat.matrix);               //  7

     // local deallocations

     tmp_mat.Deallocate();

     return res;
}

// EigenvectorsDSYEVD # 2

template<>
LAPACK_INT MatrixDense<double>::EigenvectorsDSYEVD(
     MatrixDense<double> & eigenvectors) const
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
     eigenvectors.CheckAllocation();
#endif

#ifdef DEBUG_COMPAT
     this->CheckSquareQ();
     eigenvectors.CheckSquareQ();
     this->CheckCompatibility(eigenvectors);
     this->CheckSymmetricQ();
#endif

     const MAI ROWS = this->GetNumberOfRows();
     MatrixDense<double> tmp_mat;
     tmp_mat.Allocate(1, ROWS);

     LAPACK_INT res;

     eigenvectors.Set(*this);

     res = LAPACKE_dsyevd(LAPACK_ROW_MAJOR,              //  1
                          'V',                           //  2
                          'U',                           //  3
                          static_cast<LAPACK_INT>(ROWS), //  4
                          eigenvectors.matrix,           //  5
                          static_cast<LAPACK_INT>(ROWS), //  6
                          tmp_mat.matrix);               //  7

     // local deallocations

     tmp_mat.Deallocate();

     return res;
}

// EigenvectorsDSYEVR # 3

template<>
LAPACK_INT MatrixDense<double>::EigenvectorsDSYEVR(
     MatrixDense<double> & eigenvectors) const
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
     eigenvectors.CheckAllocation();
#endif

#ifdef DEBUG_COMPAT
     this->CheckSquareQ();
     eigenvectors.CheckSquareQ();
     this->CheckCompatibility(eigenvectors);
     this->CheckSymmetricQ();
#endif

     LAPACK_INT res;

     eigenvectors.Set(*this);

     double vl = 0.0;
     double vu = 0.0;
     LAPACK_INT il = 0;
     LAPACK_INT iu = 0;
     LAPACK_INT m;
     MatrixDense<LAPACK_INT> isuppz;
     const MAI ROWS = this->GetNumberOfRows();
     isuppz.Allocate(1, 2*ROWS);
     MatrixDense<double> tmp_matA;
     tmp_matA.Allocate(1, ROWS);

     res = LAPACKE_dsyevr(LAPACK_ROW_MAJOR,              //  1
                          'V',                           //  2
                          'A',                           //  3
                          'U',                           //  4
                          static_cast<LAPACK_INT>(ROWS), //  5
                          eigenvectors.matrix,           //  6
                          static_cast<LAPACK_INT>(ROWS), //  7
                          vl,                            //  8
                          vu,                            //  9
                          il,                            // 10
                          iu,                            // 11
                          ZERO_DBL,                      // 12
                          &m,                            // 13
                          tmp_matA.matrix,               // 14
                          eigenvectors.matrix,           // 15
                          static_cast<LAPACK_INT>(ROWS), // 16
                          isuppz.matrix);                // 17

     // local deallocations

     isuppz.Deallocate();
     tmp_matA.Deallocate();

     return res;
}

// EigenvectorsDSYEVX # 4

template<>
LAPACK_INT MatrixDense<double>::EigenvectorsDSYEVX(
     MatrixDense<double> & eigenvectors) const
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
     eigenvectors.CheckAllocation();
#endif

#ifdef DEBUG_COMPAT
     this->CheckSquareQ();
     eigenvectors.CheckSquareQ();
     this->CheckCompatibility(eigenvectors);
     this->CheckSymmetricQ();
#endif

     LAPACK_INT res;

     eigenvectors.Set(*this);

     double vl = 0.0;
     double vu = 0.0;
     LAPACK_INT il = 0;
     LAPACK_INT iu = 0;
     LAPACK_INT m;
     MatrixDense<int> ifail;
     const MAI ROWS = this->GetNumberOfRows();
     ifail.Allocate(1, ROWS);
     MatrixDense<double> tmp_matA;
     tmp_matA.Allocate(1, ROWS);

     res = LAPACKE_dsyevx(LAPACK_ROW_MAJOR,              //  1
                          'V',                           //  2
                          'A',                           //  3
                          'U',                           //  4
                          static_cast<LAPACK_INT>(ROWS), //  5
                          eigenvectors.matrix,           //  6
                          static_cast<LAPACK_INT>(ROWS), //  7
                          vl,                            //  8
                          vu,                            //  9
                          il,                            // 10
                          iu,                            // 11
                          ZERO_DBL,                      // 12
                          &m,                            // 13
                          tmp_matA.matrix,               // 14
                          eigenvectors.matrix,           // 15
                          static_cast<LAPACK_INT>(ROWS), // 16
                          ifail.matrix);                 // 17

     // local deallocations

     ifail.Deallocate();
     tmp_matA.Deallocate();

     return res;
}

} // sep
} // pgg

#endif // MATRIX_DENSE_LAPACK_DIAGONALIZERS_H
