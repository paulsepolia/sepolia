
#ifndef MATRIX_DENSE_AUXILIARY_H
#define MATRIX_DENSE_AUXILIARY_H

#include <complex>

namespace pgg {
namespace sep {

// CheckAllocation

template<typename T1>
bool MatrixDense<T1>::CheckAllocation() const
{
     if (this->DeallocatedQ()) {
          pgg::functions::Exit(E_MAT_ALLOC_NOT);
     }

     return true;
}

// CheckCompatibility

template<typename T1>
bool MatrixDense<T1>::CheckCompatibility(
     const MatrixDense<T1> & matB) const
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
     matB.CheckAllocation();
#endif

     // test for columns equality

     if (this->cols != matB.cols) {
          functions::Exit(E_MAT_MAT_COLS_NE);
     } // test for rows equality
     else if (this->rows != matB.rows) {
          functions::Exit(E_MAT_MAT_ROWS_NE);
     }

     return true;
}

// CheckCompatibilityColumns

template<typename T1>
bool MatrixDense<T1>::CheckCompatibilityColumns(
     const MatrixDense<T1> & matB) const
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
     matB.CheckAllocation();
#endif

     // test for columns equality

     if (this->cols != matB.cols) {
          functions::Exit(E_MAT_MAT_COLS_NE);
     }

     return true;
}

// CheckCompatibilityDot

template<typename T1>
bool MatrixDense<T1>::CheckCompatibilityDot(
     const MatrixDense<T1> & matA,
     const MatrixDense<T1> & matB) const
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
     matA.CheckAllocation();
     matB.CheckAllocation();
#endif

     // test for "(MxN)*(NxK)", N equality

     if (matA.cols != matB.rows) {
          functions::Exit(E_MAT_MAT_COLS_ROWS_NE);
     }

     // test for "(MxK) = (MxN)*(NxK)", M equality

     if (this->rows != matA.rows) {
          functions::Exit(E_MAT_MAT_ROWS_NE);
     }

     // test for "(MxK) = (MxN)*(NxK)", K equality

     if (this->cols != matB.cols) {
          functions::Exit(E_MAT_MAT_COLS_NE);
     }

     return true;
}

// CheckCompatibilityRows

template<typename T1>
bool MatrixDense<T1>::CheckCompatibilityRows(
     const MatrixDense<T1> & matB) const
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
     matB.CheckAllocation();
#endif

     // test for rows equality

     if (this->rows != matB.rows) {
          functions::Exit(E_MAT_MAT_ROWS_NE);
     }

     return true;
}

// CheckCompatibilityTranspose

template<typename T1>
bool MatrixDense<T1>::CheckCompatibilityTranspose(
     const MatrixDense<T1> & matB) const
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
     matB.CheckAllocation();
#endif

     // test for cols/rows equality

     if (this->cols != matB.rows) {
          functions::Exit(E_MAT_MAT_COLS_ROWS_NE);
     } // test for rows/cols equality
     else if (this->rows != matB.cols) {
          functions::Exit(E_MAT_MAT_ROWS_COLS_NE);
     }

     return true;
}

// CheckHermitianQ

template<typename T1>
bool MatrixDense<T1>::CheckHermitianQ(const T1 & THRES) const
{
     if (!this->HermitianQ(THRES)) {
          functions::Exit(E_MAT_HERMITIAN_NOT);
     }

     return true;
}

// CheckSquareQ

template<typename T1>
bool MatrixDense<T1>::CheckSquareQ() const
{
     if (!this->SquareQ()) {
          functions::Exit(E_MAT_SQUARE_NOT);
     }

     return true;
}

// CheckSymmetricQ

template<typename T1>
bool MatrixDense<T1>::CheckSymmetricQ(const T1 & THRES) const
{
     if (!this->SymmetricQ(THRES)) {
          functions::Exit(E_MAT_SYMMETRIC_NOT);
     }

     return true;
}

// CheckVectorColumnQ

template<typename T1>
bool MatrixDense<T1>::CheckVectorColumnQ() const
{
     if (static_cast<MAI>(1) != this->GetNumberOfColumns()) {
          functions::Exit(E_MAT_VEC_COL_TYPE_NOT);
     }

     return true;
}

// CheckVectorRowQ

template<typename T1>
bool MatrixDense<T1>::CheckVectorRowQ() const
{
     if (static_cast<MAI>(1) != this->GetNumberOfRows()) {
          functions::Exit(E_MAT_VEC_ROW_TYPE_NOT);
     }

     return true;
}

// Equal # 1

template<typename T1>
bool MatrixDense<T1>::Equal(
     const T1 & elem,
     const T1 & thres) const
{
     return this->MatrixScalarEqual(elem, thres, functions::Equal);
}

// Equal # 2

template<typename T1>
bool MatrixDense<T1>::Equal(
     const MatrixDense<T1> & matB,
     const T1 & thres) const
{
     return this->MatrixMatrixEqual(matB, thres, functions::Equal);
}

// Equal # 3 --> complex<double>

template<>
bool MatrixDense<std::complex<double>>::Equal(
          const std::complex<double> & elem,
          const std::complex<double> & thres) const
{
     return this->MatrixScalarEqual(elem, thres, functions::Equal);
}

// Equal # 4 --> complex<double>

template<>
bool MatrixDense<std::complex<double>>::Equal(
          const MatrixDense<std::complex<double>> & matB,
          const std::complex<double> & thres) const
{
     return this->MatrixMatrixEqual(matB, thres, functions::Equal);
}

// Equal # 5 --> complex<long double>

template<>
bool MatrixDense<std::complex<long double>>::Equal(
               const std::complex<long double> & elem,
               const std::complex<long double> & thres) const
{
     return this->MatrixScalarEqual(elem, thres, functions::Equal);
}

// Equal # 6 --> complex<long double>

template<>
bool MatrixDense<std::complex<long double>>::Equal(
               const MatrixDense<std::complex<long double>> & matB,
               const std::complex<long double> & thres) const
{
     return this->MatrixMatrixEqual(matB, thres, functions::Equal);
}

// Greater # 1

template<typename T1>
bool MatrixDense<T1>::Greater(
     const T1 & elem) const
{
     return this->MatrixScalarComparison(elem, functions::Greater);
}

// Greater # 2

template<typename T1>
bool MatrixDense<T1>::Greater(
     const MatrixDense<T1> & matB) const
{
     return this->MatrixMatrixComparison(matB, functions::Greater);
}

// Greater # 3 --> complex<double>

template<>
bool MatrixDense<std::complex<double>>::Greater(
          const std::complex<double> & elem) const
{
     return this->MatrixScalarComparison(elem, functions::Greater);
}

// Greater # 4 --> complex<double>

template<>
bool MatrixDense<std::complex<double>>::Greater(
          const MatrixDense<std::complex<double>> & matB) const
{
     return this->MatrixMatrixComparison(matB, functions::Greater);
}

// Greater # 5 --> complex<long double>

template<>
bool MatrixDense<std::complex<long double>>::Greater(
               const std::complex<long double> & elem) const
{
     return this->MatrixScalarComparison(elem, functions::Greater);
}

// Greater # 6 --> complex<long double>

template<>
bool MatrixDense<std::complex<long double>>::Greater(
               const MatrixDense<std::complex<long double>> & matB) const
{
     return this->MatrixMatrixComparison(matB, functions::Greater);
}

// GreaterEqual # 1

template<typename T1>
bool MatrixDense<T1>::GreaterEqual(
     const T1 & elem) const
{
     return this->MatrixScalarComparison(elem, functions::GreaterEqual);
}

// GreaterEqual # 2

template<typename T1>
bool MatrixDense<T1>::GreaterEqual(
     const MatrixDense<T1> & matB) const
{
     return this->MatrixMatrixComparison(matB, functions::GreaterEqual);
}

// GreaterEqual # 3 --> complex<double>

template<>
bool MatrixDense<std::complex<double>>::GreaterEqual(
          const std::complex<double> & elem) const
{
     return this->MatrixScalarComparison(elem, functions::GreaterEqual);
}

// GreaterEqual # 4 --> complex<double>

template<>
bool MatrixDense<std::complex<double>>::GreaterEqual(
          const MatrixDense<std::complex<double>> & matB) const
{
     return this->MatrixMatrixComparison(matB, functions::GreaterEqual);
}

// GreaterEqual # 5 --> complex<long double>

template<>
bool MatrixDense<std::complex<long double>>::GreaterEqual(
               const std::complex<long double> & elem) const
{
     return this->MatrixScalarComparison(elem, functions::GreaterEqual);
}

// GreaterEqual # 6 --> complex<long double>

template<>
bool MatrixDense<std::complex<long double>>::GreaterEqual(
               const MatrixDense<std::complex<long double>> & matB) const
{
     return this->MatrixMatrixComparison(matB, functions::GreaterEqual);
}

// HermitianQ

template<typename T1>
bool MatrixDense<T1>::HermitianQ(const T1 & THRES) const
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
#endif

     const MAI ROWS = this->GetNumberOfRows();
     const MAI COLS = this->GetNumberOfColumns();
     MAI i;
     MAI j;

     if (ROWS != COLS) {
          return false;
     } else {
          for (i = 0; i != ROWS; ++i) {
               for (j = i; j != COLS; ++j) {
                    if ((std::abs(real((*this)(i,j)) - real((*this)(j,i))) > real(THRES)) ||
                              std::abs(imag((*this)(i,j)) + imag((*this)(j,i))) > imag(THRES)) {
                         return false;
                    }
               }
          }
     }

     return true;
}

// Less # 1

template<typename T1>
bool MatrixDense<T1>::Less(
     const T1 & elem) const
{
     return this->MatrixScalarComparison(elem, functions::Less);
}

// Less # 2

template<typename T1>
bool MatrixDense<T1>::Less(
     const MatrixDense<T1> & matB) const
{
     return this->MatrixMatrixComparison(matB, functions::Less);
}

// Less # 3 --> complex<double>

template<>
bool MatrixDense<std::complex<double>>::Less(
          const std::complex<double> & elem) const
{
     return this->MatrixScalarComparison(elem, functions::Less);
}

// Less # 4 --> complex<double>

template<>
bool MatrixDense<std::complex<double>>::Less(
          const MatrixDense<std::complex<double>> & matB) const
{
     return this->MatrixMatrixComparison(matB, functions::Less);
}

// Less # 5 --> complex<long double>

template<>
bool MatrixDense<std::complex<long double>>::Less(
               const std::complex<long double> & elem) const
{
     return this->MatrixScalarComparison(elem, functions::Less);
}

// Less # 6 --> complex<long double>

template<>
bool MatrixDense<std::complex<long double>>::Less(
               const MatrixDense<std::complex<long double>> & matB) const
{
     return this->MatrixMatrixComparison(matB, functions::Less);
}

// LessEqual # 1

template<typename T1>
bool MatrixDense<T1>::LessEqual(
     const T1 & elem) const
{
     return this->MatrixScalarComparison(elem, functions::LessEqual);
}

// LessEqual # 2

template<typename T1>
bool MatrixDense<T1>::LessEqual(
     const MatrixDense<T1> & matB) const
{
     return this->MatrixMatrixComparison(matB, functions::LessEqual);
}

// LessEqual # 3 --> complex<double>

template<>
bool MatrixDense<std::complex<double>>::LessEqual(
          const std::complex<double> & elem) const
{
     return this->MatrixScalarComparison(elem, functions::LessEqual);
}

// LessEqual # 4 --> complex<double>

template<>
bool MatrixDense<std::complex<double>>::LessEqual(
          const MatrixDense<std::complex<double>> & matB) const
{
     return this->MatrixMatrixComparison(matB, functions::LessEqual);
}

// LessEqual # 5 --> complex<long double>

template<>
bool MatrixDense<std::complex<long double>>::LessEqual(
               const std::complex<long double> & elem) const
{
     return this->MatrixScalarComparison(elem, functions::LessEqual);
}

// LessEqual # 6 --> complex<long double>

template<>
bool MatrixDense<std::complex<long double>>::LessEqual(
               const MatrixDense<std::complex<long double>> & matB) const
{
     return this->MatrixMatrixComparison(matB, functions::LessEqual);
}

// Map # 1

template<typename T1>
void MatrixDense<T1>::Map(T1 function_to_apply(T1))
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
#endif

     MAI i;
     MAI j;

     const MAI COLS = this->cols;
     const MAI ROWS = this->rows;
     const MAI_MAX COLS_LONG = static_cast<MAI_MAX>(COLS);
     MAI_MAX index;

     #pragma omp parallel default(none) \
     num_threads(NT_2D) \
     private(i)    \
     private(j)    \
     private(index)\
     shared(function_to_apply)
     {
          #pragma omp for

          for (i = 0; i < ROWS; ++i) {
               for (j = 0; j < COLS; ++j) {

                    index = i * COLS_LONG + j;
                    this->matrix[index] =
                         function_to_apply(this->matrix[index]);
               }
          }
     }
}

// Map # 2

template<typename T1>
void MatrixDense<T1>::Map(
     const MatrixDense<T1> & matA,
     T1 function_to_apply(T1))
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
     matA.CheckAllocation();
#endif

#ifdef DEBUG_COMPAT
     this->CheckCompatibility(matA);
#endif

     MAI i;
     MAI j;

     const MAI COLS = this->cols;
     const MAI ROWS = this->rows;
     const MAI_MAX COLS_LONG = static_cast<MAI_MAX>(COLS);
     MAI_MAX index;

     #pragma omp parallel default(none) \
     num_threads(NT_2D) \
     private(i)    \
     private(j)    \
     private(index)\
     shared(matA)  \
     shared(function_to_apply)
     {
          #pragma omp for

          for (i = 0; i < ROWS; ++i) {
               for (j = 0; j < COLS; ++j) {

                    index = i * COLS_LONG + j;
                    this->matrix[index] = function_to_apply(matA.matrix[index]);
               }
          }
     }
}

// Map # 3

template<typename T1>
template<typename T2>
void MatrixDense<T1>::Map(T2 & fun_obj)
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
#endif

     MAI i;
     MAI j;

     const MAI COLS = this->cols;
     const MAI ROWS = this->rows;
     const MAI_MAX COLS_LONG = static_cast<MAI_MAX>(COLS);
     MAI_MAX index;

     #pragma omp parallel default(none) \
     num_threads(NT_2D) \
     private(i)    \
     private(j)    \
     private(index)\
     shared(fun_obj)
     {
          #pragma omp for

          for (i = 0; i < ROWS; ++i) {
               for (j = 0; j < COLS; ++j) {

                    index = i * COLS_LONG + j;
                    this->matrix[index] = fun_obj(this->matrix[index]);
               }
          }
     }
}

// Map # 4

template<typename T1>
template<typename T2>
void MatrixDense<T1>::Map(
     const MatrixDense<T1> & matA,
     T2 & fun_obj)
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
     matA.CheckAllocation();
#endif

#ifdef DEBUG_COMPAT
     this->CheckCompatibility(matA);
#endif

     MAI i;
     MAI j;

     const MAI COLS = this->cols;
     const MAI ROWS = this->rows;
     const MAI_MAX COLS_LONG = static_cast<MAI_MAX>(COLS);
     MAI_MAX index;

     #pragma omp parallel default(none) \
     num_threads(NT_2D) \
     private(i)    \
     private(j)    \
     private(index)\
     shared(matA)  \
     shared(fun_obj)
     {
          #pragma omp for

          for (i = 0; i < ROWS; ++i) {
               for (j = 0; j < COLS; ++j) {

                    index = i * COLS_LONG + j;
                    this->matrix[index] = fun_obj(matA.matrix[index]);
               }
          }
     }
}

// MatrixCorrelate # 1

template <typename T1>
void MatrixDense<T1>::MatrixCorrelate(
     const MatrixDense<T1> & mat,
     const MatrixDense<T1> & kernel,
     std::pair<int,int> & anchor)
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
     mat.CheckAllocation();
     kernel.CheckAllocation();
#endif

#ifdef DEBUG_COMPAT
     this->CheckCompatibility(mat);
#endif

     // get kernel matrix dimensions

     const MAI K_ROWS = kernel.GetNumberOfRows();
     const MAI K_COLS = kernel.GetNumberOfColumns();

     // get input matrix dimensions

     const MAI ROWS = mat.GetNumberOfRows();
     const MAI COLS = mat.GetNumberOfColumns();
     const MAI_MAX COLS_LONG = static_cast<MAI_MAX>(COLS);

     // local variables and parameters

     MAI i;
     MAI j;
     MAI i_ex;
     MAI j_ex;
     MAI_MAX index;
     MAI i_index = 0;
     MAI j_index = 0;

     T1 elemDest = static_cast<T1>(ZERO_DBL_MAX);

     #pragma omp parallel default(none) \
     num_threads(NT_2D) \
     private(i_ex)   \
     private(j_ex)   \
     private(i)      \
     private(j)      \
     shared(index)   \
     shared(i_index) \
     shared(j_index) \
     shared(kernel)  \
     shared(mat)     \
     shared(anchor)  \
reduction(+:elemDest)
     {
          #pragma omp for

          for(i_ex = 0; i_ex < ROWS; ++i_ex) {
               for (j_ex = 0; j_ex < COLS; ++j_ex) {
                    elemDest = static_cast<T1>(ZERO_DBL_MAX);

                    for (i = 0; i < K_ROWS; ++i) {
                         for (j = 0; j < K_COLS; ++j) {

                              i_index = i_ex + i - anchor.first;
                              j_index = j_ex + j - anchor.second;

                              if(static_cast<int>(i_ex + i - anchor.first) < 0) {
                                   i_index = 0;
                              }

                              if((i_ex + i - anchor.first) >= ROWS) {
                                   i_index = ROWS-1;
                              }

                              if(static_cast<int>(j_ex + j - anchor.second) < 0) {
                                   j_index = 0;
                              }

                              if((j_ex + j - anchor.second) >= COLS) {
                                   j_index = COLS-1;
                              }

                              elemDest = elemDest + kernel(i,j)*mat(i_index, j_index);
                         }
                    }

                    index = i_ex * COLS_LONG + j_ex;
                    this->matrix[index] = elemDest;
               }
          }
     }
}

// MatrixCorrelate # 2

template <typename T1>
void MatrixDense<T1>::MatrixCorrelate(
     const MatrixDense<T1> & kernel,
     std::pair<int,int> & anchor)
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
     kernel.CheckAllocation();
#endif

     // get kernel matrix dimensions

     const MAI K_ROWS = kernel.GetNumberOfRows();
     const MAI K_COLS = kernel.GetNumberOfColumns();

     // get input matrix dimensions

     const MAI ROWS = this->GetNumberOfRows();
     const MAI COLS = this->GetNumberOfColumns();
     const MAI_MAX COLS_LONG = static_cast<MAI_MAX>(COLS);

     // local variables and parameters

     MAI i;
     MAI j;
     MAI i_ex;
     MAI j_ex;
     MAI_MAX index;
     MAI i_index = 0;
     MAI j_index = 0;

     T1 elemDest = static_cast<T1>(ZERO_DBL_MAX);

     #pragma omp parallel default(none) \
     num_threads(NT_2D) \
     private(i_ex)   \
     private(j_ex)   \
     private(i)      \
     private(j)      \
     shared(index)   \
     shared(i_index) \
     shared(j_index) \
     shared(kernel)  \
     shared(anchor)  \
reduction(+:elemDest)
     {
          #pragma omp for

          for(i_ex = 0; i_ex < ROWS; ++i_ex) {
               for (j_ex = 0; j_ex < COLS; ++j_ex) {
                    elemDest = static_cast<T1>(ZERO_DBL_MAX);

                    for (i = 0; i < K_ROWS; ++i) {
                         for (j = 0; j < K_COLS; ++j) {

                              i_index = i_ex + i - anchor.first;
                              j_index = j_ex + j - anchor.second;

                              if(static_cast<int>(i_ex + i - anchor.first) < 0) {
                                   i_index = 0;
                              }

                              if((i_ex + i - anchor.first) >= ROWS) {
                                   i_index = ROWS-1;
                              }

                              if(static_cast<int>(j_ex + j - anchor.second) < 0) {
                                   j_index = 0;
                              }

                              if((j_ex + j - anchor.second) >= COLS) {
                                   j_index = COLS-1;
                              }

                              elemDest = elemDest + kernel(i,j)*(*this)(i_index, j_index);
                         }
                    }

                    index = i_ex * COLS_LONG + j_ex;
                    this->matrix[index] = elemDest;
               }
          }
     }
}

// MatrixMatrixComparison

template<typename T1>
bool MatrixDense<T1>::MatrixMatrixComparison(
     const MatrixDense<T1> & matB,
     bool comp_fn(const T1 &, const T1 &)) const
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
     matB.CheckAllocation();
#endif

#ifdef DEBUG_COMPAT
     this->CheckCompatibility(matB);
#endif

     MAI i; // index for rows
     MAI j; // index for columns
     MAI_MAX counter = 0; // reset counter

     const MAI COLS = this->GetNumberOfColumns(); // number of columns
     const MAI ROWS = this->GetNumberOfRows(); // number of rows
     const MAI_MAX COLS_LONG = static_cast<MAI_MAX>(COLS);
     const MAI_MAX DIM_TOT = COLS_LONG * ROWS;
     MAI_MAX index; // index for main matrix container

     #pragma omp parallel default(none) \
     num_threads(NT_2D) \
     private(i)    \
     private(j)    \
     shared(index) \
     shared(matB)  \
     shared(comp_fn) \
reduction(+:counter)
     {
          #pragma omp for

          for (i = 0; i < ROWS; ++i) {
               for (j = 0; j < COLS; ++j) {

                    index = i * COLS_LONG + j;
                    if (comp_fn(this->matrix[index], matB.matrix[index])) {
                         counter = counter + 1UL;
                    }
               }
          }
     }

     if (counter == DIM_TOT) {
          return true;
     } else if (counter < DIM_TOT) {
          return false;
     }

     // default case

     return false;
}

// MatrixMatrixEqual

template<typename T1>
bool MatrixDense<T1>::MatrixMatrixEqual(
     const MatrixDense<T1> & matB,
     const T1 & thres,
     bool comp_fn(const T1 &,
                  const T1 &,
                  const T1 &)) const
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
#endif

     MAI i; // index for rows
     MAI j; // index for columns
     MAI_MAX counter = 0; // reset counter

     const MAI COLS = this->cols; // number of columns
     const MAI ROWS = this->rows; // number of rows
     const MAI_MAX COLS_LONG = static_cast<MAI_MAX>(COLS);
     const MAI_MAX DIM_TOT = COLS_LONG * ROWS; // total elements
     MAI_MAX index; // index for the main matrix container

     #pragma omp parallel default(none) \
     num_threads(NT_2D) \
     private(i)       \
     private(j)       \
     private(index)   \
     shared(thres)    \
     shared(comp_fn) \
     shared(matB)    \
reduction(+:counter)
     {
          #pragma omp for

          for (i = 0; i < ROWS; ++i) {
               for (j = 0; j < COLS; ++j) {

                    index = i * COLS_LONG + j;
                    if (comp_fn(this->matrix[index], matB.matrix[index], thres)) {
                         counter = counter + 1UL;
                    }
               }
          }
     }

     if (counter == DIM_TOT) {
          return true;
     } else if (counter < DIM_TOT) {
          return false;
     }

     // default case

     return false;
}

// MatrixScalarComparison

template<typename T1>
bool MatrixDense<T1>::MatrixScalarComparison(
     const T1 & elem,
     bool comp_fn(const T1 &, const T1 &)) const
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
#endif

     MAI i; // index for rows
     MAI j; // index for columns
     MAI_MAX counter = 0; // reset counter

     const MAI COLS = this->GetNumberOfColumns(); // number of columns
     const MAI ROWS = this->GetNumberOfRows(); // number of rows
     const MAI_MAX COLS_LONG = static_cast<MAI_MAX>(COLS);
     const MAI_MAX DIM_TOT = COLS_LONG * ROWS; // total matrix elements
     MAI_MAX index; // index for matrix main container

     #pragma omp parallel default(none) \
     num_threads(NT_2D) \
     private(i)    \
     private(j)    \
     private(index)\
     shared(elem)  \
     shared(comp_fn) \
reduction(+:counter)
     {
          #pragma omp for

          for (i = 0; i < ROWS; ++i) {
               for (j = 0; j < COLS; ++j) {

                    index = i * COLS_LONG + j;
                    if (comp_fn(this->matrix[index], elem)) {
                         counter = counter + 1UL;
                    }
               }
          }
     }

     if (counter == DIM_TOT) {
          return true;
     } else if (counter < DIM_TOT) {
          return false;
     }

     // default case

     return false;
}

// MatrixScalarEqual

template<typename T1>
bool MatrixDense<T1>::MatrixScalarEqual(
     const T1 & elem,
     const T1 & thres,
     bool comp_fn(const T1 &,
                  const T1 &,
                  const T1 &)) const
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
#endif

     MAI i; // index for rows
     MAI j; // index for columns
     MAI_MAX counter = 0; // reset counter

     const MAI COLS = this->cols; // number of columns
     const MAI ROWS = this->rows; // number of rows
     const MAI_MAX COLS_LONG = static_cast<MAI_MAX>(COLS);
     const MAI_MAX DIM_TOT = COLS_LONG * ROWS; // total elements
     MAI_MAX index; // index for the main matrix container

     #pragma omp parallel default(none) \
     num_threads(NT_2D) \
     private(i)       \
     private(j)       \
     private(index)   \
     shared(elem)     \
     shared(thres)    \
     shared(comp_fn) \
reduction(+:counter)
     {
          #pragma omp for

          for (i = 0; i < ROWS; ++i) {
               for (j = 0; j < COLS; ++j) {

                    index = i * COLS_LONG + j;
                    if (comp_fn(this->matrix[index], elem, thres)) {
                         counter = counter + 1UL;
                    }
               }
          }
     }

     if (counter == DIM_TOT) {
          return true;
     } else if (counter < DIM_TOT) {
          return false;
     }

     // default case

     return false;
}

// Mean

template<typename T1>
T1 MatrixDense<T1>::Mean() const
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
#endif

     MAI i;
     MAI j;
     T1 mean_val = static_cast<T1>(ZERO_DBL_MAX);

     const MAI COLS = this->cols;
     const MAI ROWS = this->rows;
     const MAI_MAX COLS_LONG = static_cast<MAI_MAX>(COLS);
     const MAI_MAX DIM_TOT = COLS_LONG * ROWS;
     MAI_MAX index;

     #pragma omp parallel default(none) \
     num_threads(NT_2D) \
     private(i)    \
     private(j)    \
     shared(index) \
reduction(+:mean_val)
     {
          #pragma omp for

          for (i = 0; i < ROWS; ++i) {
               for (j = 0; j < COLS; ++j) {
                    index = i * COLS_LONG + j;
                    mean_val = mean_val + this->matrix[index];
               }
          }
     }

     // take the mean

     mean_val = mean_val/static_cast<T1>(DIM_TOT);

     // return

     return mean_val;
}

// Nest # 1

template<typename T1>
void MatrixDense<T1>::Nest(
     T1 function_to_apply(T1),
     const MAI_MAX & n)
{
     for (MAI_MAX i = 0; i != n; ++i) {
          this->Map(function_to_apply);
     }
}

// Nest # 2

template<typename T1>
void MatrixDense<T1>::Nest(
     const MatrixDense<T1> & matA,
     T1 function_to_apply(T1),
     const MAI_MAX & n)
{
     this->Set(matA);

     for (MAI_MAX i = 0; i != n; ++i) {
          this->Map(*this, function_to_apply);
     }
}

// Nest # 3

template<typename T1>
template<typename T2>
void MatrixDense<T1>::Nest(T2 & fun_obj,
                           const MAI_MAX & n)
{
     for (MAI_MAX i = 0; i != n; ++i) {
          this->Map(*this, fun_obj);
     }
}

// Nest # 4

template<typename T1>
template<typename T2>
void MatrixDense<T1>::Nest(const MatrixDense<T1> & matA,
                           T2 & fun_obj,
                           const MAI_MAX & n)
{
     this->Set(matA);

     for (MAI_MAX i = 0; i != n; ++i) {
          this->Map(*this, fun_obj);
     }
}

// NotEqual # 1

template<typename T1>
bool MatrixDense<T1>::NotEqual(const T1 & elem,
                               const T1 & thres) const
{
     return !this->Equal(elem, thres);
}

// NotEqual # 2

template<typename T1>
bool MatrixDense<T1>::NotEqual(const MatrixDense<T1> & mat,
                               const T1 & thres) const
{
     return !this->Equal(mat, thres);
}

// Random

template<typename T1>
void MatrixDense<T1>::Random(const T1 & my_start,
                             const T1 & my_end,
                             const unsigned int & my_seed)
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
#endif

     // local parameters

     MAI i;
     MAI j;
     T1 tmp_val;

     const MAI COLS = this->cols;
     const MAI ROWS = this->rows;
     const auto DIFF = my_end - my_start;
     const auto START = my_start;
     const MAI_MAX COLS_LONG = static_cast<MAI_MAX>(COLS);
     MAI_MAX index;

     // seed the random generator

     srand(my_seed);

     // build the random matrix

     for (i = 0; i < ROWS; ++i) {
          for (j = 0; j < COLS; ++j) {

               tmp_val = START + (static_cast<T1>(rand())/RAND_MAX)*DIFF;
               index = i * COLS_LONG + j;
               this->matrix[index] = tmp_val;
          }
     }
}

// Range # 1

template<typename T1>
void MatrixDense<T1>::Range(const T1 & my_start)
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
#endif

     MAI i;
     MAI j;
     T1 tmp_val;

     const MAI ROWS = this->rows;
     const MAI COLS = this->cols;
     const MAI_MAX COLS_LONG = static_cast<MAI_MAX>(COLS);
     MAI_MAX index;

     // build the matrix

     #pragma omp parallel default(none) \
     num_threads(NT_2D) \
     private(i)       \
     private(j)       \
     private(tmp_val) \
     private(index)   \
     shared(my_start)
     {
          #pragma omp for

          for (i = 0; i < ROWS; ++i) {
               for (j = 0; j < COLS; ++j) {

                    index = i * COLS_LONG + j;
                    tmp_val = static_cast<T1>(index + my_start);
                    this->matrix[index] = tmp_val;
               }
          }
     }
}

// Range # 2

template<typename T1>
void MatrixDense<T1>::Range(const T1 & my_start, const T1 & my_end)
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
#endif

     MAI i;
     MAI j;
     T1 tmp_val;

     const MAI ROWS = this->rows;
     const MAI COLS = this->cols;
     const MAI_MAX COLS_LONG = static_cast<MAI_MAX>(COLS);
     MAI_MAX index;

     const T1 STEP = (my_end-my_start)/(static_cast<double>(ROWS)*COLS);

     // build the matrix

     #pragma omp parallel default(none) \
     num_threads(NT_2D) \
     private(i)       \
     private(j)       \
     private(tmp_val) \
     private(index)   \
     shared(my_start)
     {
          #pragma omp for

          for (i = 0; i < ROWS; ++i) {
               for (j = 0; j < COLS; ++j) {

                    index = i * COLS_LONG + j;
                    tmp_val = index*STEP + my_start;
                    this->matrix[index] = tmp_val;
               }
          }
     }
}

// SetMax # 1

template<typename T1>
void MatrixDense<T1>::SetMax(const T1 & max_val, const T1 & set_max)
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
#endif

     MAI i;
     MAI j;

     const MAI ROWS = this->rows;
     const MAI COLS = this->cols;
     const MAI_MAX COLS_LONG = static_cast<MAI_MAX>(COLS);
     MAI_MAX index;

     // build the matrix

     #pragma omp parallel default(none) \
     num_threads(NT_2D) \
     private(i)       \
     private(j)       \
     private(index)   \
     shared(max_val)  \
     shared(set_max)
     {
          #pragma omp for

          for (i = 0; i < ROWS; ++i) {
               for (j = 0; j < COLS; ++j) {

                    index = i * COLS_LONG + j;

                    if (this->matrix[index] > max_val) {
                         this->matrix[index] = static_cast<T1>(set_max);
                    }
               }
          }
     }
}

// SetMax # 2

template<typename T1>
void MatrixDense<T1>::SetMax(const T1 & comp_val)
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
#endif

     MAI i;
     MAI j;

     const MAI ROWS = this->rows;
     const MAI COLS = this->cols;
     const MAI_MAX COLS_LONG = static_cast<MAI_MAX>(COLS);
     MAI_MAX index;

     // build the matrix

     #pragma omp parallel default(none) \
     num_threads(NT_2D) \
     private(i)       \
     private(j)       \
     private(index)   \
     shared(comp_val)
     {
          #pragma omp for

          for (i = 0; i < ROWS; ++i) {
               for (j = 0; j < COLS; ++j) {

                    index = i * COLS_LONG + j;
                    this->matrix[index] = std::max(this->matrix[index], comp_val);
               }
          }
     }
}

// SetMax # 3

template<typename T1>
void MatrixDense<T1>::SetMax(const MatrixDense<T1> & mat, const T1 & comp_val)
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
     mat.CheckAllocation();
#endif

#ifdef DEBUG_COMPAT
     this->CheckCompatibility(mat);
#endif

     MAI i;
     MAI j;

     const MAI ROWS = this->rows;
     const MAI COLS = this->cols;
     const MAI_MAX COLS_LONG = static_cast<MAI_MAX>(COLS);
     MAI_MAX index;

     // build the matrix

     #pragma omp parallel default(none) \
     num_threads(NT_2D) \
     private(i)       \
     private(j)       \
     private(index)   \
     shared(comp_val) \
     shared(mat)
     {
          #pragma omp for

          for (i = 0; i < ROWS; ++i) {
               for (j = 0; j < COLS; ++j) {

                    index = i * COLS_LONG + j;
                    this->matrix[index] = std::max(mat.matrix[index], comp_val);
               }
          }
     }
}

// SetMin # 1

template<typename T1>
void MatrixDense<T1>::SetMin(const T1 & min_val, const T1 & set_min)
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
#endif

     MAI i;
     MAI j;

     const MAI ROWS = this->rows;
     const MAI COLS = this->cols;
     const MAI_MAX COLS_LONG = static_cast<MAI_MAX>(COLS);
     MAI_MAX index;

     // build the matrix

     #pragma omp parallel default(none) \
     num_threads(NT_2D) \
     private(i)       \
     private(j)       \
     private(index)   \
     shared(min_val)  \
     shared(set_min)
     {
          #pragma omp for

          for (i = 0; i < ROWS; ++i) {
               for (j = 0; j < COLS; ++j) {

                    index = i * COLS_LONG + j;

                    if (this->matrix[index] < min_val) {
                         this->matrix[index] = static_cast<T1>(set_min);
                    }
               }
          }
     }
}

// SetMin # 2

template<typename T1>
void MatrixDense<T1>::SetMin(const T1 & comp_val)
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
#endif

     MAI i;
     MAI j;

     const MAI ROWS = this->rows;
     const MAI COLS = this->cols;
     const MAI_MAX COLS_LONG = static_cast<MAI_MAX>(COLS);
     MAI_MAX index;

     // build the matrix

     #pragma omp parallel default(none) \
     num_threads(NT_2D) \
     private(i)       \
     private(j)       \
     private(index)   \
     shared(comp_val)
     {
          #pragma omp for

          for (i = 0; i < ROWS; ++i) {
               for (j = 0; j < COLS; ++j) {

                    index = i * COLS_LONG + j;
                    this->matrix[index] = min(this->matrix[index], comp_val);
               }
          }
     }
}

// SetMin # 3

template<typename T1>
void MatrixDense<T1>::SetMin(const MatrixDense<T1> & mat, const T1 & comp_val)
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
     mat.CheckAllocation();
#endif

#ifdef DEBUG_COMPAT
     this->CheckCompatibility(mat);
#endif

     MAI i;
     MAI j;

     const MAI ROWS = this->rows;
     const MAI COLS = this->cols;
     const MAI_MAX COLS_LONG = static_cast<MAI_MAX>(COLS);
     MAI_MAX index;

     // build the matrix

     #pragma omp parallel default(none) \
     num_threads(NT_2D) \
     private(i)       \
     private(j)       \
     private(index)   \
     shared(comp_val) \
     shared(mat)
     {
          #pragma omp for

          for (i = 0; i < ROWS; ++i) {
               for (j = 0; j < COLS; ++j) {

                    index = i * COLS_LONG + j;
                    this->matrix[index] = std::min(mat.matrix[index], comp_val);
               }
          }
     }
}

// SquareQ

template<typename T1>
bool MatrixDense<T1>::SquareQ() const
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
#endif

     const MAI ROWS = this->GetNumberOfRows();
     const MAI COLS = this->GetNumberOfColumns();

     return (ROWS == COLS);
}

// Swap

template<typename T1>
void MatrixDense<T1>::Swap(MatrixDense<T1> & mat)
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
     mat.CheckAllocation();
#endif

#ifdef DEBUG_COMPAT
     this->CheckCompatibility(mat);
#endif

     // local variables

     const MAI ROWS = mat.GetNumberOfRows();
     const MAI COLS = mat.GetNumberOfColumns();
     const MAI_MAX ELEMS = static_cast<MAI_MAX>(ROWS) * COLS;
     const MAI_MAX BUFFER_SIZE = static_cast<MAI_MAX>(pow(10.0, 3.0));

     if (ELEMS >= BUFFER_SIZE) {
          MatrixDense<T1> bufferA;
          MatrixDense<T1> bufferB;

          const MAI_MAX NUM_BUFS = ELEMS/BUFFER_SIZE;
          const MAI_MAX LAST_BUF_SIZE = ELEMS%BUFFER_SIZE;

          MAI_MAX i;
          MAI_MAX j;
          MAI_MAX k;

          // all but the last buffer

          #pragma omp parallel default(none) \
          num_threads(NT_2D) \
          private(i)         \
          private(k)         \
          private(j)         \
          private(bufferA)   \
          private(bufferB)   \
          shared(mat)
          {
               #pragma omp for

               for(k = 0; k < NUM_BUFS; ++k) {

                    bufferA.Allocate(BUFFER_SIZE);
                    bufferB.Allocate(BUFFER_SIZE);

                    for(i = 0; i < BUFFER_SIZE; ++i) {
                         j = i + BUFFER_SIZE*(k);

                         bufferA.SetElement(i, mat(j));
                         bufferB.SetElement(i, (*this)(j));
                         mat.SetElement(j, bufferB(i));
                         this->SetElement(j, bufferA(i));
                    }

                    bufferA.Deallocate();
                    bufferB.Deallocate();
               }
          }

          // last buffer case

          for (i = 0; i != LAST_BUF_SIZE; ++i) {
               bufferA.Allocate(LAST_BUF_SIZE);
               bufferB.Allocate(LAST_BUF_SIZE);

               j = i + BUFFER_SIZE * NUM_BUFS;

               bufferA.SetElement(i, mat(j));
               bufferB.SetElement(i, (*this)(j));
               mat.SetElement(j, bufferB(i));
               this->SetElement(j, bufferA(i));

               bufferA.Deallocate();
               bufferB.Deallocate();
          }

          // deallocate local buffers

          bufferA.Deallocate();
          bufferB.Deallocate();
     } else if (ELEMS < BUFFER_SIZE) {
          MatrixDense<T1> mat_tmp;

          mat_tmp.Allocate(ROWS, COLS);

          mat_tmp = mat;
          mat = *this;
          *this = mat_tmp;

          mat_tmp.Deallocate();
     }
}

// SymmetricQ

template<typename T1>
bool MatrixDense<T1>::SymmetricQ(const T1 & THRES) const
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
#endif

     const MAI ROWS = this->GetNumberOfRows();
     const MAI COLS = this->GetNumberOfColumns();
     MAI i;
     MAI j;

     if (ROWS != COLS) {
          return false;
     } else {
          for (i = 0; i != ROWS; ++i) {
               for (j = i; j != COLS; ++j) {
                    if (std::abs((*this)(i,j) - (*this)(j,i)) > THRES) {
                         return false;
                    }
               }
          }
     }

     return true;
}

} // sep
} // pgg

#endif // MATRIX_DENSE_AUXILIARY_H

