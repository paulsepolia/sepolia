
#ifndef MATRIX_DENSE_MEMORY_H
#define MATRIX_DENSE_MEMORY_H

namespace pgg {
namespace sep {

// Allocate # 1

template <typename T1>
void MatrixDense<T1>::Allocate(const MAI & dim)
{
     if (!this->is_alloc) {
          // set flags

          this->is_alloc = TRUE_ALLOC;

          // set the dimensions

          this->rows = dim;
          this->cols = dim;

          // allocate heap space

          const MAI ROWS = this->rows;
          const MAI COLS = this->cols;
          const MAI_MAX TOT_ELEMS = static_cast<MAI_MAX>(COLS) * ROWS;

          this->matrix = new T1[TOT_ELEMS];
     }
}

// Allocate # 2

template <typename T1>
void MatrixDense<T1>::Allocate(const MAI & num_rows,
                               const MAI & num_cols)
{
     if (!this->is_alloc) {
          // set flags

          this->is_alloc = TRUE_ALLOC;

          // set the dimensions

          this->rows = num_rows;
          this->cols = num_cols;

          // allocate heap space

          const MAI ROWS = this->rows;
          const MAI COLS = this->cols;
          const MAI_MAX TOT_ELEMS = static_cast<MAI_MAX>(COLS) * ROWS;

          this->matrix = new T1[TOT_ELEMS];
     }
}

// AllocatedQ

template <typename T1>
bool MatrixDense<T1>::AllocatedQ() const
{
     return this->is_alloc;
}

// Deallocate

template <typename T1>
void MatrixDense<T1>::Deallocate()
{
     if(this->is_alloc) {

          // delete heap space

          delete[] this->matrix;

          // set to null pointer

          this->matrix = 0;

          // set allocation flag

          this->is_alloc = FALSE_ALLOC;
     }
}

// DeallocatedQ

template <typename T1>
bool MatrixDense<T1>::DeallocatedQ() const
{
     return !this->is_alloc;
}

} // sep
} // pgg

#endif // MATRIX_DENSE_MEMORY_H

