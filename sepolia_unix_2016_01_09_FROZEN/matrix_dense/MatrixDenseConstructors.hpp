
#ifndef MATRIX_DENSE_CONSTRUCTORS_H
#define MATRIX_DENSE_CONSTRUCTORS_H

namespace pgg {
namespace sep {

// constructor

template<typename T1>
MatrixDense<T1>::MatrixDense() :
     matrix(0), rows(0), cols(0),
     is_alloc(FALSE_ALLOC)
{}

// copy constructor

template<typename T1>
MatrixDense<T1>::MatrixDense(const MatrixDense<T1> & mat) :
     matrix(0), rows(0), cols(0),
     is_alloc(FALSE_ALLOC)
{
     if (mat.AllocatedQ()) {
          this->Allocate(mat.rows, mat.cols);
          this->Set(mat);
     } else if(mat.DeallocatedQ()) {
          functions::Exit(E_MAT_ALLOC_NOT);
     }
}

} // sep
} // pgg

#endif // MATRIX_DENSE_CONSTRUCTORS_H
