
#ifndef MATRIX_DENSE_H
#define MATRIX_DENSE_H

#include "MatrixDenseDeclaration.hpp"

#include "MatrixDenseAlgebraBasic.hpp"
#include "MatrixDenseAuxiliary.hpp"
#include "MatrixDenseBLAS.hpp"
#include "MatrixDenseCMATH.hpp"
#include "MatrixDenseComplex.hpp"
#include "MatrixDenseConstructors.hpp"
#include "MatrixDenseDeclaration.hpp"
#include "MatrixDenseDestructor.hpp"
#include "MatrixDenseGet.hpp"
#include "MatrixDenseGSL.hpp"
#include "MatrixDenseLAPACKDiagonalizers.hpp"
#include "MatrixDenseMemory.hpp"

#ifdef MKL_SEPOLIA_H
#include "MatrixDenseMKL.hpp"
#endif

#include "MatrixDenseOperators.hpp"
#include "MatrixDenseSet.hpp"
#include "MatrixDenseSTL.hpp"

#include "MatrixDenseGeneric.hpp" // after BLAS and GSL

#endif // MATRIX_DENSE_H
