
#ifndef MATRIX_DENSE_CMATH_H
#define MATRIX_DENSE_CMATH_H

#include <cmath>
#include <cstdlib>

namespace pgg {
namespace sep {

// Abs # 1

template<typename T1>
inline void MatrixDense<T1>::Abs(
     const MatrixDense<T1> & mat)
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
     mat.CheckAllocation();
#endif

#ifdef DEBUG_COMPAT
     this->CheckCompatibility(mat);
#endif

     this->Map(mat, std::abs);
}

// Abs # 2

template<typename T1>
inline void MatrixDense<T1>::Abs()
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
#endif

     this->Map(std::abs);
}

// ArcCos # 1

template<typename T1>
inline void MatrixDense<T1>::ArcCos(
     const MatrixDense<T1> & mat)
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
     mat.CheckAllocation();
#endif

#ifdef DEBUG_COMPAT
     this->CheckCompatibility(mat);
#endif

     this->Map(mat, std::acos);
}

// ArcCos # 2

template<typename T1>
inline void MatrixDense<T1>::ArcCos()
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
#endif

     this->Map(std::acos);
}

// ArcSin # 1

template<typename T1>
inline void MatrixDense<T1>::ArcSin(
     const MatrixDense<T1> & mat)
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
     mat.CheckAllocation();
#endif

#ifdef DEBUG_COMPAT
     this->CheckCompatibility(mat);
#endif

     this->Map(mat, std::asin);
}

// ArcSin # 2

template<typename T1>
inline void MatrixDense<T1>::ArcSin()
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
#endif

     this->Map(std::asin);
}

// Cos # 1

template<typename T1>
inline void MatrixDense<T1>::Cos(
     const MatrixDense<T1> & mat)
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
     mat.CheckAllocation();
#endif

#ifdef DEBUG_COMPAT
     this->CheckCompatibility(mat);
#endif

     this->Map(mat, std::cos);
}

// Cos # 2

template<typename T1>
inline void MatrixDense<T1>::Cos()
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
#endif

     this->Map(std::cos);
}

// FastMA # 1

template<>
inline void MatrixDense<double>::FastMA(
     const MatrixDense<double> & matA,
     const MatrixDense<double> & matB,
     const MatrixDense<double> & matC)
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
     matA.CheckAllocation();
     matB.CheckAllocation();
     matC.CheckAllocation();
#endif

#ifdef DEBUG_COMPAT
     this->CheckCompatibility(matA);
     this->CheckCompatibility(matB);
     this->CheckCompatibility(matC);
#endif

     MAI i;
     MAI j;

     const MAI COLS = matA.cols;
     const MAI ROWS = matA.rows;
     const MAI_MAX COLS_LONG = static_cast<MAI_MAX>(COLS);
     MAI_MAX index;

     #pragma omp parallel default(none) \
     num_threads(NT_2D) \
     private(i)    \
     private(j)    \
     private(index)\
     shared(matA)  \
     shared(matB)  \
     shared(matC)
     {
          #pragma omp for

          for (i = 0; i < ROWS; ++i) {
               for (j = 0; j < COLS; ++j) {

                    index = i * COLS_LONG + j;
                    this->matrix[index] = std::fma(
                                               matA.matrix[index],
                                               matB.matrix[index],
                                               matC.matrix[index]);
               }
          }
     }
}

// FastMA # 2

template<>
inline void MatrixDense<long double>::FastMA(
     const MatrixDense<long double> & matA,
     const MatrixDense<long double> & matB,
     const MatrixDense<long double> & matC)
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
     matA.CheckAllocation();
     matB.CheckAllocation();
     matC.CheckAllocation();
#endif

#ifdef DEBUG_COMPAT
     this->CheckCompatibility(matA);
     this->CheckCompatibility(matB);
     this->CheckCompatibility(matC);
#endif

     MAI i;
     MAI j;

     const MAI COLS = matA.cols;
     const MAI ROWS = matA.rows;
     const MAI_MAX COLS_LONG = static_cast<MAI_MAX>(COLS);
     MAI_MAX index;

     #pragma omp parallel default(none) \
     num_threads(NT_2D) \
     private(i)    \
     private(j)    \
     private(index)\
     shared(matA)  \
     shared(matB)  \
     shared(matC)
     {
          #pragma omp for

          for (i = 0; i < ROWS; ++i) {
               for (j = 0; j < COLS; ++j) {

                    index = i * COLS_LONG + j;
                    this->matrix[index] = std::fma(
                                               matA.matrix[index],
                                               matB.matrix[index],
                                               matC.matrix[index]);
               }
          }
     }
}

// Power # 1

template<>
inline void MatrixDense<double>::Power(
     const MatrixDense<double> & mat_base,
     const MatrixDense<double> & mat_exponent)
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
     mat_base.CheckAllocation();
     mat_exponent.CheckAllocation();
#endif

#ifdef DEBUG_COMPAT
     this->CheckCompatibility(mat_base);
     this->CheckCompatibility(mat_exponent);
#endif

     MAI i;
     MAI j;

     const MAI COLS = this->cols;
     const MAI ROWS = this->rows;
     const MAI_MAX COLS_LONG = static_cast<MAI_MAX>(COLS);
     MAI_MAX index;

     #pragma omp parallel default(none) \
     num_threads(NT_2D) \
     private(i)       \
     private(j)       \
     private(index)   \
     shared(mat_base) \
     shared(mat_exponent)
     {
          #pragma omp for

          for (i = 0; i < ROWS; ++i) {
               for (j = 0; j < COLS; ++j) {

                    index = i * COLS_LONG + j;
                    this->matrix[index] = std::pow(
                                               mat_base.matrix[index],
                                               mat_exponent.matrix[index]);
               }
          }
     }
}

// Power # 2

template<>
inline void MatrixDense<long double>::Power(
     const MatrixDense<long double> & mat_base,
     const MatrixDense<long double> & mat_exponent)
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
     mat_base.CheckAllocation();
     mat_exponent.CheckAllocation();
#endif

#ifdef DEBUG_COMPAT
     this->CheckCompatibility(mat_base);
     this->CheckCompatibility(mat_exponent);
#endif

     MAI i;
     MAI j;

     const MAI COLS = this->cols;
     const MAI ROWS = this->rows;
     const MAI_MAX COLS_LONG = static_cast<MAI_MAX>(COLS);
     MAI_MAX index;

     #pragma omp parallel default(none) \
     num_threads(NT_2D) \
     private(i)       \
     private(j)       \
     private(index)   \
     shared(mat_base) \
     shared(mat_exponent)
     {
          #pragma omp for

          for (i = 0; i < ROWS; ++i) {
               for (j = 0; j < COLS; ++j) {

                    index = i * COLS_LONG + j;
                    this->matrix[index] = std::pow(
                                               mat_base.matrix[index],
                                               mat_exponent.matrix[index]);
               }
          }
     }
}

// Sin # 1

template<typename T1>
inline void MatrixDense<T1>::Sin(
     const MatrixDense<T1> & mat)
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
     mat.CheckAllocation();
#endif

     this->Map(mat, std::sin);
}

// Sin # 2

template<typename T1>
inline void MatrixDense<T1>::Sin()
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
#endif

     this->Map(std::sin);
}

} // sep
} // pgg

#endif // MATRIX_DENSE_CMATH_H

