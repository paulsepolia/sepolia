
#ifndef MATRIX_DENSE_COMPLEX_H
#define MATRIX_DENSE_COMPLEX_H

#include <complex>

namespace pgg {
namespace sep {

// ImaginaryPart

template<typename T1>
template<typename T2>
void MatrixDense<T1>::ImaginaryPart(
     const MatrixDense<T2> & matComplex)
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
     matComplex.CheckAllocation();
#endif

     const MAI ROWS_IM = this->GetNumberOfRows();
     const MAI COLS_IM = this->GetNumberOfColumns();

#ifdef DEBUG_COMPAT
     const MAI ROWS_CMPLX = matComplex.GetNumberOfRows();
     const MAI COLS_CMPLX = matComplex.GetNumberOfColumns();

     if ((ROWS_CMPLX != ROWS_IM) || (COLS_CMPLX != COLS_IM)) {
          functions::Exit(E_MAT_MAT_CONFORM_NOT);
     }
#endif

     MAI_MAX i;
     MAI_MAX j;

     #pragma omp parallel default(none) \
     num_threads(NT_2D) \
     private(i)    \
     private(j)    \
     shared(matComplex)
     {
          #pragma omp for

          for (i = 0; i < ROWS_IM; ++i) {
               for (j = 0; j < COLS_IM; ++j) {
                    this->SetElement(i, j, std::imag(matComplex(i, j)));
               }
          }
     }
}

// RealPart

template<typename T1>
template<typename T2>
void MatrixDense<T1>::RealPart(
     const MatrixDense<T2> & matComplex)
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
     matComplex.CheckAllocation();
#endif

     const MAI ROWS_RE = this->GetNumberOfRows();
     const MAI COLS_RE = this->GetNumberOfColumns();

#ifdef DEBUG_COMPAT
     const MAI ROWS_CMPLX = matComplex.GetNumberOfRows();
     const MAI COLS_CMPLX = matComplex.GetNumberOfColumns();

     if ((ROWS_CMPLX != ROWS_RE) || (COLS_CMPLX != COLS_RE)) {
          functions::Exit(E_MAT_MAT_CONFORM_NOT);
     }
#endif

     MAI_MAX i;
     MAI_MAX j;

     #pragma omp parallel default(none) \
     num_threads(NT_2D) \
     private(i)    \
     private(j)    \
     shared(matComplex)
     {
          #pragma omp for

          for (i = 0; i < ROWS_RE; ++i) {
               for (j = 0; j < COLS_RE; ++j) {
                    this->SetElement(i, j, std::real(matComplex(i, j)));
               }
          }
     }
}

} // sep
} // pgg

#endif // MATRIX_DENSE_COMPLEX_H

