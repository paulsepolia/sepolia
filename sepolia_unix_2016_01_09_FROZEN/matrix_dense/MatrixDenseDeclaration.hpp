
#ifndef MATRIX_DENSE_DECLARATION_H
#define MATRIX_DENSE_DECLARATION_H

#include <string>
#include <complex>

namespace pgg {
namespace sep {

template<typename T1>
class MatrixDense {
public:

     //===============//
     // Algebra Basic //
     //===============//

     // Divide

     void Divide(const MatrixDense<T1> &,
                 const MatrixDense<T1> &);

     void Divide(const MatrixDense<T1> &,
                 const T1 &);

     void Divide(const T1 &,
                 const MatrixDense<T1> &);

     void Divide(const T1 &);

     void Divide(const MatrixDense<T1> &);

private:

     // MatrixMatrixTypeA

     void MatrixMatrixTypeA(
          const MatrixDense<T1> &,
          const MatrixDense<T1> &,
          T1 op_fn(const T1 &, const T1 &));

     // MatrixScalarTypeA

     void MatrixScalarTypeA(
          const MatrixDense<T1> &,
          const T1 &,
          T1 op_fn(const T1 &, const T1 &));

public:

     // Minus

     void Minus(const MatrixDense<T1> &);

     // Negate

     void Negate();

     // Plus

     void Plus(const MatrixDense<T1> &,
               const MatrixDense<T1> &);

     void Plus(const MatrixDense<T1> &,
               const T1 &);

     void Plus(const T1 &,
               const MatrixDense<T1> &);

     void Plus(const T1 &);

     void Plus(const MatrixDense<T1> &);

     // Subtract

     void Subtract(const MatrixDense<T1> &,
                   const MatrixDense<T1> &);

     void Subtract(const MatrixDense<T1> &,
                   const T1 &);

     void Subtract(const T1 &,
                   const MatrixDense<T1> &);

     void Subtract(const T1 &);

     void Subtract(const MatrixDense<T1> &);

     // Times

     void Times(const MatrixDense<T1> &,
                const MatrixDense<T1> &);

     void Times(const MatrixDense<T1> &,
                const T1 &);

     void Times(const T1 &,
                const MatrixDense<T1> &);

     void Times(const T1 &);

     void Times(const MatrixDense<T1> &);

     // TransposePGG # 1

     void TransposePGG();

     // TransposePGG # 2

     void TransposePGG(const MatrixDense<T1> &);

public:

     //===========//
     // Auxiliary //
     //===========//

     // CheckAllocation

     bool CheckAllocation() const;

     // CheckCompatibility

     bool CheckCompatibility(const MatrixDense<T1> &) const;

     // CheckCompatibilityColumns

     bool CheckCompatibilityColumns(const MatrixDense<T1> &) const;

     // CheckCompatibilityDot

     bool CheckCompatibilityDot(const MatrixDense<T1> &,
                                const MatrixDense<T1> &) const;

     // CheckCompatibilityRows

     bool CheckCompatibilityRows(const MatrixDense<T1> &) const;

     // CheckCompatibilityTranspose

     bool CheckCompatibilityTranspose(const MatrixDense<T1> &) const;

     // CheckHermitianQ

     bool CheckHermitianQ(const T1 & = static_cast<T1>(ZERO_DBL_MAX)) const;

     // CheckSquareQ

     bool CheckSquareQ() const;

     // CheckSymmetricQ

     bool CheckSymmetricQ(const T1 & = static_cast<T1>(ZERO_DBL_MAX)) const;

     // CheckVectorColumnQ

     bool CheckVectorColumnQ() const;

     // CheckVectorRowQ

     bool CheckVectorRowQ() const;

     // Equal # 1

     bool Equal(const T1 &,
                const T1 & = static_cast<T1>(ZERO_DBL_MAX)) const;

     // Equal # 2

     bool Equal(const MatrixDense<T1> &,
                const T1 & = static_cast<T1>(ZERO_DBL_MAX)) const;

     // Greater # 1

     bool Greater(const T1 &) const;

     // Greater # 2

     bool Greater(const MatrixDense<T1> &) const;

     // GreaterEqual # 1

     bool GreaterEqual(const T1 &) const;

     // GreaterEqual # 2

     bool GreaterEqual(const MatrixDense<T1> &) const;

     // HermitianQ

     bool HermitianQ(const T1 & = static_cast<T1>(ZERO_DBL_MAX)) const;

     // Less # 1

     bool Less(const T1 &) const;

     // Less # 2

     bool Less(const MatrixDense<T1> &) const;

     // LessEqual # 1

     bool LessEqual(const T1 &) const;

     // LessEqual # 2

     bool LessEqual(const MatrixDense<T1> &) const;

     // Map # 1

     void Map(T1 function_to_apply(T1));

     // Map # 2

     void Map(const MatrixDense<T1> &,
              T1 function_to_apply(T1));

     // Map # 3

     template<typename T2>
     void Map(T2 &);

     // Map # 4

     template<typename T2>
     void Map(const MatrixDense<T1> &,
              T2 &);

     // MatrixCorrelate # 1

     void MatrixCorrelate(const MatrixDense<T1> &,
                          const MatrixDense<T1> &,
                          std::pair<int,int> &);

     // MatrixCorrelate # 2

     void MatrixCorrelate(const MatrixDense<T1> &,
                          std::pair<int,int> &);

private:

     // MatrixMatrixComparison

     bool MatrixMatrixComparison(
          const MatrixDense<T1> &,
          bool comp_fn(const T1 &, const T1 &)) const;

     // MatrixMatrixEqual

     bool MatrixMatrixEqual(
          const MatrixDense<T1> &,
          const T1 &,
          bool comp_fn(const T1 &,
                       const T1 &,
                       const T1 &)) const;

     // MatrixScalarComparison

     bool MatrixScalarComparison(
          const T1 &,
          bool comp_fn(const T1 &, const T1 &)) const;

     // MatrixScalarEqual

     bool MatrixScalarEqual(
          const T1 &,
          const T1 &,
          bool comp_fn(const T1 &,
                       const T1 &,
                       const T1 &)) const;

public:

     // Mean

     T1 Mean() const;

     // Nest # 1

     void Nest(T1 function_to_apply(T1),
               const MAI_MAX &);

     // Nest # 2

     void Nest(const MatrixDense<T1> &,
               T1 function_to_apply(T1),
               const MAI_MAX &);

     // Nest # 3

     template<typename T2>
     void Nest(T2 &, const MAI_MAX &);

     // Nest # 4

     template<typename T2>
     void Nest(const MatrixDense<T1> &,
               T2 &,
               const MAI_MAX &);

     // NotEqual # 1

     bool NotEqual(const T1 &,
                   const T1 & = static_cast<T1>(ZERO_DBL)) const;

     // NotEqual # 2

     bool NotEqual(const MatrixDense<T1> &,
                   const T1 & = static_cast<T1>(ZERO_DBL)) const;

     // Random

     void Random(const T1 &, const T1 &, const unsigned int &);

     // Range # 1

     void Range(const T1 &);

     // Range # 2

     void Range(const T1 &, const T1 &);

     // SetMax # 1

     void SetMax(const T1 &, const T1 &);

     // SetMax # 2

     void SetMax(const T1 &);

     // SetMax # 3

     void SetMax(const MatrixDense<T1> &, const T1 &);

     // SetMin # 1

     void SetMin(const T1 &, const T1 &);

     // SetMin # 2

     void SetMin(const T1 &);

     // SetMin # 3

     void SetMin(const MatrixDense<T1> &, const T1 &);

     // SquareQ

     bool SquareQ() const;

     // Swap

     void Swap(MatrixDense<T1> &);

     // SymmetricQ

     bool SymmetricQ(const T1 & = static_cast<T1>(ZERO_DBL_MAX)) const;

public:

     //======//
     // BLAS //
     //======//

     //===============================//
     // Complex Double General Matrix //
     //===============================//

     // DotBLAS # 1

     void DotBLAS(const MatrixDense<std::complex<double>> &,
                  const MatrixDense<std::complex<double>> &);

     // DotBLAS # 2

     void DotBLAS(const MatrixDense<std::complex<double>> &);

     // DotBLAS # 3

     template<typename T2>
     void DotBLAS(const MatrixDense<std::complex<double>> &,
                  const T2 &,
                  const std::string & = WITH_ROWS);

     // DotBLAS # 4

     template<typename T2>
     std::complex<double> DotBLASC(const T2 &,
                                   const std::string & WITH_ROWS) const;

     //=======================//
     // Double General Matrix //
     //=======================//

     // DotBLAS # 1

     void DotBLAS(const MatrixDense<double> &,
                  const MatrixDense<double> &);

     // DotBLAS # 2

     void DotBLAS(const MatrixDense<double> &);

     // DotBLAS # 3

     template<typename T2>
     void DotBLAS(const MatrixDense<double> &,
                  const T2 &,
                  const std::string & = WITH_ROWS);

     // DotBLAS # 4

     template<typename T2>
     double DotBLAS(const T2 &,
                    const std::string & = WITH_ROWS) const;

     // DotBLAS # 5

     void DotBLAS();

public:

     //=========//
     // Complex //
     //=========//

     template<typename T2>
     void ImaginaryPart(const MatrixDense<T2> &);

     template<typename T2>
     void RealPart(const MatrixDense<T2> &);

public:

     //==============//
     // Constructors //
     //==============//

     explicit MatrixDense<T1>();

     MatrixDense<T1>(const MatrixDense<T1> &);

public:

     //=======//
     // CMATH //
     //=======//

     // Abs # 1

     void Abs(const MatrixDense<T1> &);

     // Abs # 2

     void Abs();

     // ArcCos # 1

     void ArcCos(const MatrixDense<T1> &);

     // ArcCos # 2

     void ArcCos();

     // ArcSin # 1

     void ArcSin(const MatrixDense<T1> &);

     // ArcSin # 2

     void ArcSin();

     // Cos # 1

     void Cos(const MatrixDense<T1> &);

     // Cos # 2

     void Cos();

     // FastMA

     void FastMA(const MatrixDense<T1> &,
                 const MatrixDense<T1> &,
                 const MatrixDense<T1> &);

     // Power

     void Power(const MatrixDense<T1> &,
                const MatrixDense<T1> &);

     // Sin # 1

     void Sin(const MatrixDense<T1> &);

     // Sin # 2

     void Sin();

public:

     //============//
     // Destructor //
     //============//

     virtual ~MatrixDense<T1>();

public:

     //===================//
     // Generic functions //
     //===================//

     //=================================//
     // Complex Double General Matrices //
     //=================================//

     // Dot # 1

     void Dot(const MatrixDense<std::complex<double>> &,
              const MatrixDense<std::complex<double>> &);

     // Dot # 2

     template<typename T2>
     void Dot(const MatrixDense<std::complex<double>> &,
              const T2 &,
              const std::string & = WITH_ROWS);

     // Dot # 3

     void Dot(const MatrixDense<std::complex<double>> &);

     // Dot # 4

     template<typename T2>
     std::complex<double> DotC(const T2 &,
                               const std::string & = WITH_ROWS);

     //=========================//
     // Double General Matrices //
     //=========================//

     // Dot # 1

     void Dot(const MatrixDense<double> &,
              const MatrixDense<double> &,
              const std::string & = BLAS_LIB);

     // Dot # 2

     template<typename T2>
     void Dot(const MatrixDense<double> &,
              const T2 &,
              const std::string & = WITH_ROWS);

     // Dot # 3

     void Dot(const MatrixDense<double> &);

     // Dot # 4

     template<typename T2>
     double Dot(const T2 &,
                const std::string & = WITH_ROWS);

     // Dot # 5

     void Dot();

     //==============//
     // Eigensystems //
     //==============//

     // EigensystemComplexHermitian

     template<typename T2>
     LAPACK_INT EigensystemComplexHermitian(MatrixDense<std::complex<double>> &,
                                            T2 &,
                                            const std::string & = DIAG_ZHEEVD) const;

     // EigensystemRealSymmetric

     template<typename T2>
     LAPACK_INT EigensystemRealSymmetric(MatrixDense<double> &,
                                         T2 &,
                                         const std::string & = DIAG_DSYEVD) const;

     //=============//
     // Eigenvalues //
     //=============//

     // EigenvaluesComplexHermitian

     template<typename T2>
     LAPACK_INT EigenvaluesComplexHermitian(T2 &,
                                            const std::string & = DIAG_ZHEEVD) const;

     // EigenvaluesRealSymmetric

     template<typename T2>
     LAPACK_INT EigenvaluesRealSymmetric(T2 &,
                                         const std::string & = DIAG_DSYEVD) const;

     //==============//
     // Eigenvectors //
     //==============//

     // EigenvectorsComplexHermitian

     LAPACK_INT EigenvectorsComplexHermitian(MatrixDense<std::complex<double>> &,
                                             const std::string & = DIAG_ZHEEVD) const;

     // EigenvectorsRealSymmetric

     LAPACK_INT EigenvectorsRealSymmetric(MatrixDense<double> &,
                                          const std::string & = DIAG_DSYEVD) const;

     //===========//
     // Transpose //
     //===========//

     // Transpose # 1

     void Transpose(const std::string & = PGG_LIB);

     // Transpose # 2

     void Transpose(const MatrixDense<T1> &,
                    const std::string & = PGG_LIB);

public:

     //=====//
     // Get //
     //=====//

     template <typename T2>
     void GetColumn(const MAI &, T2 &) const;

     T1 GetElement(const MAI &, const MAI &) const;

     T1 GetElement(const MAI_MAX &) const;

     MAI GetNumberOfRows() const;

     MAI GetNumberOfColumns() const;

     template <typename T2>
     void GetRow(const MAI &, T2 &) const;

public:

     //=====//
     // GSL //
     //=====//

     // DotGSL

     void DotGSL(const MatrixDense<double> &,
                 const MatrixDense<double> &);

     // TransposeGSL # 1

     void TransposeGSL();

     // TransposeGSL # 2

     void TransposeGSL(const MatrixDense<T1> &);

public:

     //========//
     // LAPACK //
     //========//

     // Eigensystem Complex Hermitian

     template<typename T2>
     LAPACK_INT EigensystemZHEEV(MatrixDense<T1> &, T2 &) const;

     template<typename T2>
     LAPACK_INT EigensystemZHEEVD(MatrixDense<T1> &, T2 &) const;

     template<typename T2>
     LAPACK_INT EigensystemZHEEVR(MatrixDense<T1> &, T2 &) const;

     template<typename T2>
     LAPACK_INT EigensystemZHEEVX(MatrixDense<T1> &, T2 &) const;

     // Eigensystem Real Symmetric

     template<typename T2>
     LAPACK_INT EigensystemDSYEV(MatrixDense<T1> &, T2 &) const;

     template<typename T2>
     LAPACK_INT EigensystemDSYEVD(MatrixDense<T1> &, T2 &) const;

     template<typename T2>
     LAPACK_INT EigensystemDSYEVR(MatrixDense<T1> &, T2 &) const;

     template<typename T2>
     LAPACK_INT EigensystemDSYEVX(MatrixDense<T1> &, T2 &) const;

     // Eigenvalues Complex Hermitian

     template<typename T2>
     LAPACK_INT EigenvaluesZHEEV(T2 &) const;

     template<typename T2>
     LAPACK_INT EigenvaluesZHEEVD(T2 &) const;

     template<typename T2>
     LAPACK_INT EigenvaluesZHEEVR(T2 &) const;

     template<typename T2>
     LAPACK_INT EigenvaluesZHEEVX(T2 &) const;

     // Eigenvalues Real Symmetric

     template<typename T2>
     LAPACK_INT EigenvaluesDSYEV(T2 &) const;

     template<typename T2>
     LAPACK_INT EigenvaluesDSYEVD(T2 &) const;

     template<typename T2>
     LAPACK_INT EigenvaluesDSYEVR(T2 &) const;

     template<typename T2>
     LAPACK_INT EigenvaluesDSYEVX(T2 &) const;

     // Eigenvectors Complex Hermitian

     LAPACK_INT EigenvectorsZHEEV(MatrixDense<T1> &) const;

     LAPACK_INT EigenvectorsZHEEVD(MatrixDense<T1> &) const;

     LAPACK_INT EigenvectorsZHEEVR(MatrixDense<T1> &) const;

     LAPACK_INT EigenvectorsZHEEVX(MatrixDense<T1> &) const;

     // Eigenvectors Real symmetric

     LAPACK_INT EigenvectorsDSYEV(MatrixDense<T1> &) const;

     LAPACK_INT EigenvectorsDSYEVD(MatrixDense<T1> &) const;

     LAPACK_INT EigenvectorsDSYEVR(MatrixDense<T1> &) const;

     LAPACK_INT EigenvectorsDSYEVX(MatrixDense<T1> &) const;

public:

     //========//
     // Memory //
     //========//

     void Allocate(const MAI &);

     void Allocate(const MAI &, const MAI &);

     bool AllocatedQ() const;

     void Deallocate();

     bool DeallocatedQ() const;

public:

#ifdef MKL_SEPOLIA_H

     //=====//
     // MKL //
     //=====//

     // TransposeMKL # 1

     void TransposeMKL();

     // TransposeMKL # 2

     void TransposeMKL(const MatrixDense<T1> &);

#endif // MKL_SEPOLIA_H

public:

     //===========//
     // Operators //
     //===========//

     // =

     MatrixDense<T1> & operator =(const MatrixDense<T1> &);

     MatrixDense<T1> & operator =(const T1 &);

     // +

     MatrixDense<T1> operator +(const MatrixDense<T1> &) const;

     MatrixDense<T1> operator +(const T1 &) const;

     template <typename T2>
     friend MatrixDense<T2> operator +(const T2 &,
                                       const MatrixDense<T2> &);

     // -

     MatrixDense<T1> operator -(const MatrixDense<T1> &) const;

     MatrixDense<T1> operator -(const T1 &) const;

     MatrixDense<T1> operator -() const;

     template<typename T2>
     friend MatrixDense<T2> operator -(const T2 &,
                                       const MatrixDense<T2> &);

     // *

     MatrixDense<T1> operator *(const MatrixDense<T1> &) const;

     MatrixDense<T1> operator *(const T1 &) const;

     template<typename T2>
     friend MatrixDense<T2> operator *(const T2 &,
                                       const MatrixDense<T2> &);

     // /

     MatrixDense<T1> operator /(const MatrixDense<T1> &) const;

     MatrixDense<T1> operator /(const T1 &) const;

     template<typename T2>
     friend MatrixDense<T2> operator /(const T2 &,
                                       const MatrixDense<T2> &);

     // +=

     MatrixDense<T1> operator +=(const MatrixDense<T1> &);

     MatrixDense<T1> operator +=(const T1 &);

     // -=

     MatrixDense<T1> operator -=(const MatrixDense<T1> &);

     MatrixDense<T1> operator -=(const T1 &);

     // *=

     MatrixDense<T1> operator *=(const MatrixDense<T1> &);

     MatrixDense<T1> operator *=(const T1 &);

     // /=

     MatrixDense<T1> operator /=(const MatrixDense<T1> &);

     MatrixDense<T1> operator /=(const T1 &);

     // ++

     MatrixDense<T1> operator ++();
     MatrixDense<T1> operator ++(int);

     // --

     MatrixDense<T1> operator --();
     MatrixDense<T1> operator --(int);

     // ==

     bool operator ==(const MatrixDense<T1> &) const;

     bool operator ==(const T1 &) const;

     // !=

     bool operator !=(const MatrixDense<T1> &) const;

     bool operator !=(const T1 &) const;

     // >

     bool operator >(const MatrixDense<T1> &) const;

     bool operator >(const T1 &) const;

     // >=

     bool operator >=(const MatrixDense<T1> &) const;

     bool operator >=(const T1 &) const;

     // <

     bool operator <(const MatrixDense<T1> &) const;

     bool operator <(const T1 &) const;

     // <=

     bool operator <=(const MatrixDense<T1> &) const;

     bool operator <=(const T1 &) const;

     // (,) # 1

     const T1 operator ()(const MAI &, const MAI &) const;

     // ()  # 2

     const T1 operator ()(const MAI_MAX &) const;

     // []

     const T1 operator [](const MAI_MAX &) const;

public:

     //=====//
     // Set //
     //=====//

     void Set(const MatrixDense<T1> &);

     void Set(const T1 &);

     template <typename T2>
     void SetColumn(const MAI &, const T2 &);

     void SetElement(const MAI &,
                     const MAI &,
                     const T1 &);

     void SetElement(const MAI_MAX &,
                     const T1 &);

     template <typename T2>
     void SetRow(const MAI &, const T2 &);

public:

     //=====//
     // STL //
     //=====//

     // BinarySearch # 1

     bool BinarySearch(const T1 &) const;

     // BinarySearch # 2

     template<typename T2>
     bool BinarySearch(const T1 &, T2 &) const;

     // Count

     MAI_MAX Count(const T1 &) const;

     // CountIf

     template<typename T2>
     MAI_MAX CountIf(T2 &) const;

     // FindElement

     std::pair<double, double> FindElement(const T1 &) const;

     // Generate

     template<typename T2>
     void Generate(T2 &);

     // Max # 1

     T1 Max() const;

     // Max # 2

     template<typename T2>
     T1 Max(T2 &) const;

     // Min # 1

     T1 Min() const;

     // Min # 2

     template<typename T2>
     T1 Min(T2 &) const;

     // NextPermutation # 1

     bool NextPermutation();

     // NextPermutation # 2

     template<typename T2>
     bool NextPermutation(T2 &);

     // PreviousPermutation # 1

     bool PreviousPermutation();

     // PreviousPermutation # 2

     template<typename T2>
     bool PreviousPermutation(T2 &);

     // RandomShuffle # 1

     void RandomShuffle();

     // RandomShuffle # 2

     void RandomShuffle(const unsigned int &);

     // Reverse

     void Reverse();

     // RotateLeft

     void RotateLeft(const MAI_MAX &);

     // RotateRight

     void RotateRight(const MAI_MAX &);

     // Sort # 1

     void Sort();

     // Sort # 2

     template<typename T2>
     void Sort(T2 &);

     // SortedQ # 1

     bool SortedQ() const;

     // SortedQ # 2

     template<typename T2>
     bool SortedQ(T2 &) const;

public:

     T1 * matrix;

private:

     MAI rows;
     MAI cols;
     bool is_alloc;
};

} // sep
} // pgg

#endif // MATRIX_DENSE_DECLARATION_H

