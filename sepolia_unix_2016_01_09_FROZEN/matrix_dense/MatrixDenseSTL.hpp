
#ifndef MATRIX_DENSE_STL_H
#define MATRIX_DENSE_STL_H

#include <algorithm>

namespace pgg {
namespace sep {

// BinarySearch # 1

template<typename T1>
bool MatrixDense<T1>::BinarySearch(const T1 & elem) const
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
#endif

     // local variables and parameters

     bool bool_tmp;
     const MAI ROWS = this->rows;
     const MAI COLS = this->cols;
     const MAI_MAX TOT_ELEM = ROWS * static_cast<MAI_MAX>(COLS);

     // try to find the element

     bool_tmp = std::binary_search(this->matrix,
                                   this->matrix + TOT_ELEM,
                                   elem);

     // return

     return bool_tmp;
}

// BinarySearch # 2

template<typename T1>
template<typename T2>
bool MatrixDense<T1>::BinarySearch(const T1 & elem,
                                   T2 & my_comp) const
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
#endif

     // local variables and parameters

     bool bool_tmp;
     const MAI ROWS = this->rows;
     const MAI COLS = this->cols;
     const MAI_MAX TOT_ELEM = ROWS * static_cast<MAI_MAX>(COLS);

     // try to find the element

     bool_tmp = std::binary_search(this->matrix,
                                   this->matrix + TOT_ELEM,
                                   elem,
                                   my_comp);

     // return

     return bool_tmp;
}

// Count

template<typename T1>
MAI_MAX MatrixDense<T1>::Count(const T1 & elem) const
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
#endif

     // local variables and parameters

     MAI_MAX tmp_val;
     const MAI ROWS = this->rows;
     const MAI COLS = this->cols;
     const MAI_MAX TOT_ELEM = ROWS * static_cast<MAI_MAX>(COLS);

     // try to find the element

     tmp_val = std::count(this->matrix,
                          this->matrix + TOT_ELEM,
                          elem);

     // return

     return tmp_val;
}

// CountIf

template<typename T1>
template<typename T2>
MAI_MAX MatrixDense<T1>::CountIf(T2 & my_comp) const
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
#endif

     // local variables and parameters

     MAI_MAX tmp_val;
     const MAI ROWS = this->rows;
     const MAI COLS = this->cols;
     const MAI_MAX TOT_ELEM = ROWS * static_cast<MAI_MAX>(COLS);

     // try to find the element

     tmp_val = std::count_if(this->matrix,
                             this->matrix + TOT_ELEM,
                             my_comp);

     // return

     return tmp_val;
}

// FindElement

template<typename T1>
std::pair<double, double> MatrixDense<T1>::FindElement(const T1 & elem) const
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
#endif

     // local variables and parameters

     T1 * ptr_tmp;
     std::pair<double, double> res_tmp;
     const MAI ROWS = this->rows;
     const MAI COLS = this->cols;
     const MAI_MAX TOT_ELEM = ROWS * static_cast<MAI_MAX>(COLS);

     // try to find the element

     ptr_tmp = std::find(this->matrix,
                         this->matrix + TOT_ELEM,
                         elem);

     // check if the element has been found

     if (ptr_tmp == this->matrix + TOT_ELEM) {
          res_tmp.first = static_cast<double>(-1);
          res_tmp.second = static_cast<double>(-1);
     } else if (ptr_tmp != this->matrix + TOT_ELEM) {
          unsigned long int loc = ptr_tmp - this->matrix;
          res_tmp.first = static_cast<double>(loc / COLS);
          res_tmp.second = static_cast<double>(loc - COLS * res_tmp.first);
     }

     // return

     return res_tmp;
}

// Generate

template<typename T1>
template<typename T2>
void MatrixDense<T1>::Generate(T2 & my_generator)
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
#endif

     // local parameters

     const MAI ROWS = this->rows;
     const MAI COLS = this->cols;
     const MAI_MAX COLS_LONG = static_cast<MAI_MAX>(COLS);
     const MAI_MAX TOT_ELEM = ROWS * COLS_LONG;

     // fill array_tmp with specific elements

     std::generate(this->matrix,
                   this->matrix + TOT_ELEM,
                   my_generator);
}

// Max # 1

template<typename T1>
T1 MatrixDense<T1>::Max() const
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
#endif

     // local variables

     T1 val_tmp;

     // local parameters

     const MAI ROWS = this->rows;
     const MAI COLS = this->cols;
     const MAI_MAX COLS_LONG = static_cast<MAI_MAX>(COLS);
     const MAI_MAX TOT_ELEM = ROWS * COLS_LONG;

     // get the maximum element

     val_tmp = *std::max_element(this->matrix,
                                 this->matrix + TOT_ELEM);

     // return

     return val_tmp;
}

// Max # 2

template<typename T1>
template<typename T2>
T1 MatrixDense<T1>::Max(T2 & my_comp) const
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
#endif

     // local variables

     T1 val_tmp;

     // local parameters

     const MAI ROWS = this->rows;
     const MAI COLS = this->cols;
     const MAI_MAX COLS_LONG = static_cast<MAI_MAX>(COLS);
     const MAI_MAX TOT_ELEM = ROWS * COLS_LONG;

     // get the maximum element

     val_tmp = *std::max_element(this->matrix,
                                 this->matrix + TOT_ELEM,
                                 my_comp);

     // return

     return val_tmp;
}

// Min # 1

template<typename T1>
T1 MatrixDense<T1>::Min() const
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
#endif

     // local variables

     T1 val_tmp;

     // local parameters

     const MAI ROWS = this->rows;
     const MAI COLS = this->cols;
     const MAI_MAX COLS_LONG = static_cast<MAI_MAX>(COLS);
     const MAI_MAX TOT_ELEM = ROWS * COLS_LONG;

     // get the minimum element

     val_tmp = *std::min_element(this->matrix,
                                 this->matrix + TOT_ELEM);

     // return

     return val_tmp;
}

// Min # 2

template<typename T1>
template<typename T2>
T1 MatrixDense<T1>::Min(T2 & my_comp) const
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
#endif

     // local variables

     T1 val_tmp;

     // local parameters

     const MAI ROWS = this->rows;
     const MAI COLS = this->cols;
     const MAI_MAX COLS_LONG = static_cast<MAI_MAX>(COLS);
     const MAI_MAX TOT_ELEM = ROWS * COLS_LONG;

     // get the minimum element

     val_tmp = *std::min_element(this->matrix,
                                 this->matrix + TOT_ELEM,
                                 my_comp);

     // return

     return val_tmp;
}

// NextPermutation # 1

template<typename T1>
bool MatrixDense<T1>::NextPermutation()
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
#endif

     // local variables and parameters

     bool bool_tmp;
     const MAI ROWS = this->rows;
     const MAI COLS = this->cols;
     const MAI_MAX TOT_ELEM = ROWS * static_cast<MAI_MAX>(COLS);

     // main algorithm

     bool_tmp = std::next_permutation(this->matrix,
                                      this->matrix + TOT_ELEM);

     // return

     return bool_tmp;
}

// NextPermutation # 2

template<typename T1>
template<typename T2>
bool MatrixDense<T1>::NextPermutation(T2 & my_comp)
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
#endif

     // local variables and parameters

     bool bool_tmp;
     const MAI ROWS = this->rows;
     const MAI COLS = this->cols;
     const MAI_MAX TOT_ELEM = ROWS * static_cast<MAI_MAX>(COLS);

     // main algorithm

     bool_tmp = std::next_permutation(this->matrix,
                                      this->matrix + TOT_ELEM,
                                      my_comp);

     // return

     return bool_tmp;
}

// PreviousPermutation # 1

template<typename T1>
bool MatrixDense<T1>::PreviousPermutation()
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
#endif

     // local variables and parameters

     bool bool_tmp;
     const MAI ROWS = this->rows;
     const MAI COLS = this->cols;
     const MAI_MAX TOT_ELEM = ROWS * static_cast<MAI_MAX>(COLS);

     // main algorithm

     bool_tmp = std::prev_permutation(this->matrix,
                                      this->matrix + TOT_ELEM);

     // return

     return bool_tmp;
}

// PreviousPermutation # 2

template<typename T1>
template<typename T2>
bool MatrixDense<T1>::PreviousPermutation(T2 & my_comp)
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
#endif

     // local variables and parameters

     bool bool_tmp;
     const MAI ROWS = this->rows;
     const MAI COLS = this->cols;
     const MAI_MAX TOT_ELEM = ROWS * static_cast<MAI_MAX>(COLS);

     // main algorithm

     bool_tmp = std::prev_permutation(this->matrix,
                                      this->matrix + TOT_ELEM,
                                      my_comp);

     // return

     return bool_tmp;
}

// RandomShuffle # 1

template<typename T1>
void MatrixDense<T1>::RandomShuffle()
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
#endif

     // local variables and parameters

     const MAI ROWS = this->rows;
     const MAI COLS = this->cols;
     const MAI_MAX TOT_ELEM = ROWS * static_cast<MAI_MAX>(COLS);

     // reverse the array the elements

     std::random_shuffle(this->matrix, this->matrix + TOT_ELEM);
}

// RandomShuffle # 2

template<typename T1>
void MatrixDense<T1>::RandomShuffle(const unsigned int & n)
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
#endif

     // local variables and parameters

     const MAI ROWS = this->rows;
     const MAI COLS = this->cols;
     const MAI_MAX TOT_ELEM = ROWS * static_cast<MAI_MAX>(COLS);

     // seed the generator

     functions::SeedRandomGenerator(n);

     // reverse the array the elements

     std::random_shuffle(this->matrix, this->matrix + TOT_ELEM);
}

// Reverse

template<typename T1>
void MatrixDense<T1>::Reverse()
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
#endif

     // local parameters and variables

     const MAI ROWS = this->rows;
     const MAI COLS = this->cols;
     const MAI_MAX TOT_ELEM = ROWS * static_cast<MAI_MAX>(COLS);

     // reverse the array the elements

     std::reverse(this->matrix, this->matrix + TOT_ELEM);
}

// RotateLeft

template<typename T1>
void MatrixDense<T1>::RotateLeft(const MAI_MAX & pos)
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
#endif

     // local parameters

     const MAI ROWS = this->rows;
     const MAI COLS = this->cols;
     const MAI_MAX TOT_ELEM = ROWS * static_cast<MAI_MAX>(COLS);

     // check if position "pos" is valid

     // test --> a

     if (pos <= 0) {
          functions::Exit(E_ARG_POS);
     }

     // test --> b

     if (pos > TOT_ELEM) {
          functions::Exit(E_ARG_LESS_TOT_ELEMS);
     }

     // main algorithm

     std::rotate(this->matrix,
                 this->matrix + pos,
                 this->matrix + TOT_ELEM);
}

// RotateRight

template<typename T1>
void MatrixDense<T1>::RotateRight(const MAI_MAX & pos)
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
#endif

     // local parameters

     const MAI ROWS = this->rows;
     const MAI COLS = this->cols;
     const MAI_MAX TOT_ELEM = ROWS * static_cast<MAI_MAX>(COLS);

     // check if position "pos" is valid

     // test --> a

     if (pos <= 0) {
          functions::Exit(E_ARG_POS);
     }

     // test --> b

     if (pos > TOT_ELEM) {
          functions::Exit(E_ARG_LESS_TOT_ELEMS);
     }

     // main algorithm

     std::reverse(this->matrix, this->matrix + TOT_ELEM);
     std::reverse(this->matrix + pos , this->matrix + TOT_ELEM);
     std::reverse(this->matrix, this->matrix + pos);
}

// Sort # 1

template<typename T1>
void MatrixDense<T1>::Sort()
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
#endif

     // local parameters

     const MAI ROWS = this->rows;
     const MAI COLS = this->cols;
     const MAI_MAX TOT_ELEM = ROWS * static_cast<MAI_MAX>(COLS);

     // sort the array

     std::sort(this->matrix, this->matrix + TOT_ELEM);
}

// Sort # 2

template<typename T1>
template<typename T2>
void MatrixDense<T1>::Sort(T2 & my_comp)
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
#endif

     // local parameters

     const MAI ROWS = this->rows;
     const MAI COLS = this->cols;
     const MAI_MAX TOT_ELEM = ROWS * static_cast<MAI_MAX>(COLS);

     // sort the array

     std::sort(this->matrix,
               this->matrix + TOT_ELEM,
               my_comp);
}

// SortedQ # 1

template<typename T1>
bool MatrixDense<T1>::SortedQ() const
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
#endif

     bool val_tmp;

     // local parameters

     const MAI ROWS = this->rows;
     const MAI COLS = this->cols;
     const MAI_MAX TOT_ELEM = ROWS * static_cast<MAI_MAX>(COLS);

     // check if the array is sorted

     val_tmp = std::is_sorted(this->matrix,
                              this->matrix + TOT_ELEM);

     // return

     return val_tmp;
}

// SortedQ # 2

template<typename T1>
template<typename T2>
bool MatrixDense<T1>::SortedQ(T2 & my_comp) const
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
#endif

     bool val_tmp;

     // local parameters

     const MAI ROWS = this->rows;
     const MAI COLS = this->cols;
     const MAI_MAX TOT_ELEM = ROWS * static_cast<MAI_MAX>(COLS);

     // check if the array is sorted

     val_tmp = std::is_sorted(this->matrix,
                              this->matrix + TOT_ELEM,
                              my_comp);

     // return

     return val_tmp;
}

} // sep
} // pgg

#endif // MATRIX_DENSE_STL_H

