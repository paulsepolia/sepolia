
#ifndef MATRIX_DENSE_ALGEBRA_BASIC_H
#define MATRIX_DENSE_ALGEBRA_BASIC_H

namespace pgg {
namespace sep {

// Divide # 1

template<typename T1>
void MatrixDense<T1>::Divide(const MatrixDense<T1> & matA,
                             const MatrixDense<T1> & matB)
{
     this->MatrixMatrixTypeA(matA, matB, functions::Divide);
}

// Divide # 2

template<typename T1>
void MatrixDense<T1>::Divide(const MatrixDense<T1> & matA,
                             const T1 & elem)
{
     this->MatrixScalarTypeA(matA, elem, functions::Divide);
}

// Divide # 3

template<typename T1>
void MatrixDense<T1>::Divide(const T1 & elem,
                             const MatrixDense<T1> & matA)
{
     this->MatrixScalarTypeA(matA, elem, functions::DivideInverse);
}

// Divide # 4

template<typename T1>
void MatrixDense<T1>::Divide(const T1 & elem)
{
     this->MatrixScalarTypeA(*this, elem, functions::Divide);
}

// Divide # 5

template<typename T1>
void MatrixDense<T1>::Divide(const MatrixDense<T1> & mat)
{
     this->Divide(*this, mat);
}

// MatrixMatrixTypeA

template<typename T1>
void MatrixDense<T1>::MatrixMatrixTypeA(
     const MatrixDense<T1> & matA,
     const MatrixDense<T1> & matB,
     T1 op_fn(const T1 &, const T1 &))
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
     matA.CheckAllocation();
     matB.CheckAllocation();
#endif

#ifdef DEBUG_COMPAT
     this->CheckCompatibility(matA);
     this->CheckCompatibility(matB);
#endif

     MAI i;
     MAI j;
     T1 tmp_val;

     const MAI COLS = matA.cols;
     const MAI ROWS = matA.rows;
     const MAI_MAX COLS_LONG = static_cast<MAI_MAX>(COLS);
     MAI_MAX index;

     #pragma omp parallel default(none) \
     num_threads(NT_2D) \
     private(i)       \
     private(j)       \
     private(tmp_val) \
     private(index)   \
     shared(matA)     \
     shared(matB)     \
     shared(op_fn)
     {
          #pragma omp for

          for (i = 0; i < ROWS; ++i) {
               for (j = 0; j < COLS; ++j) {

                    index = i * COLS_LONG + j;
                    tmp_val = op_fn(matA.matrix[index], matB.matrix[index]);
                    this->matrix[index] = tmp_val;
               }
          }
     }
}

// MatrixScalarTypeA

template<typename T1>
void MatrixDense<T1>::MatrixScalarTypeA(
     const MatrixDense<T1> & matA,
     const T1 & elem,
     T1 op_fn(const T1 &, const T1 &))
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
     matA.CheckAllocation();
#endif

#ifdef DEBUG_COMPAT
     this->CheckCompatibility(matA);
#endif

     MAI i;
     MAI j;
     T1 tmp_val;

     const MAI COLS = matA.cols;
     const MAI ROWS = matA.rows;
     const MAI_MAX COLS_LONG = static_cast<MAI_MAX>(COLS);
     MAI_MAX index;

     #pragma omp parallel default(none) \
     num_threads(NT_2D) \
     private(i)       \
     private(j)       \
     private(tmp_val) \
     private(index)   \
     shared(matA)     \
     shared(elem)     \
     shared(op_fn)
     {
          #pragma omp for

          for (i = 0; i < ROWS; ++i) {
               for (j = 0; j < COLS; ++j) {

                    index = i * COLS_LONG + j;
                    tmp_val = op_fn(matA.matrix[index], elem);
                    this->matrix[index] = tmp_val;
               }
          }
     }
}

// Minus

template<typename T1>
void MatrixDense<T1>::Minus(const MatrixDense<T1> & mat)
{
     this->Map(mat, functions::Minus<T1>);
}

// Negate

template<typename T1>
void MatrixDense<T1>::Negate()
{
     this->Map(*this, functions::Minus<T1>);
}

// Plus # 1

template<typename T1>
void MatrixDense<T1>::Plus(const MatrixDense<T1> & matA,
                           const MatrixDense<T1> & matB)
{
     this->MatrixMatrixTypeA(matA, matB, functions::Plus);
}

// Plus # 2

template<typename T1>
void MatrixDense<T1>::Plus(const MatrixDense<T1> & matA,
                           const T1 & elem)
{
     this->MatrixScalarTypeA(matA, elem, functions::Plus);
}

// Plus # 3

template<typename T1>
void MatrixDense<T1>::Plus(const T1 & elem,
                           const MatrixDense<T1> & matA)
{
     this->Plus(matA, elem);
}

// Plus # 4

template<typename T1>
void MatrixDense<T1>::Plus(const T1 & elem)
{
     this->MatrixScalarTypeA(*this, elem, functions::Plus);
}

// Plus # 5

template<typename T1>
void MatrixDense<T1>::Plus(const MatrixDense<T1> & mat)
{
     this->Plus(*this, mat);
}

// Subtract # 1

template<typename T1>
void MatrixDense<T1>::Subtract(const MatrixDense<T1> & matA,
                               const MatrixDense<T1> & matB)
{
     this->MatrixMatrixTypeA(matA, matB, functions::Subtract);
}

// Subtract # 2

template<typename T1>
void MatrixDense<T1>::Subtract(const MatrixDense<T1> & matA,
                               const T1 & elem)
{
     this->Plus(matA, -elem);
}

// Subtract # 3

template<typename T1>
void MatrixDense<T1>::Subtract(const T1 & elem,
                               const MatrixDense<T1> & matA)
{
     this->MatrixScalarTypeA(*this, elem, functions::SubtractNegate);
}

// Subtract # 4

template<typename T1>
void MatrixDense<T1>::Subtract(const T1 & elem)
{
     this->MatrixScalarTypeA(*this, elem, functions::Subtract);
}

// Subtract # 5

template<typename T1>
void MatrixDense<T1>::Subtract(const MatrixDense<T1> & matB)
{
     this->MatrixMatrixTypeA(*this, matB, functions::Subtract);
}

// Times # 1

template<typename T1>
void MatrixDense<T1>::Times(const MatrixDense<T1> & matA,
                            const MatrixDense<T1> & matB)
{
     this->MatrixMatrixTypeA(matA, matB, functions::Times);
}

// Times # 2

template<typename T1>
void MatrixDense<T1>::Times(const MatrixDense<T1> & matA,
                            const T1 & elem)
{
     this->MatrixScalarTypeA(matA, elem, functions::Times);
}

// Times # 3

template<typename T1>
void MatrixDense<T1>::Times(const T1 & elem,
                            const MatrixDense<T1> & matA)
{
     this->MatrixScalarTypeA(matA, elem, functions::Times);
}

// Times # 4

template<typename T1>
void MatrixDense<T1>::Times(const T1 & elem)
{
     this->MatrixScalarTypeA(*this, elem, functions::Times);
}

// Times # 5

template<typename T1>
void MatrixDense<T1>::Times(const MatrixDense<T1> & mat)
{
     this->MatrixMatrixTypeA(*this, mat, functions::Times);
}

// TransposePGG # 1

template<typename T1>
void MatrixDense<T1>::TransposePGG()
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
#endif

#ifdef DEBUG_COMPAT
     this->CheckSquareQ();
#endif

     const MAI ROWS = this->GetNumberOfRows();
     const MAI COLS = ROWS;

     MAI i;
     MAI j;
     T1 tmp_elem;

     #pragma omp parallel default(none) \
     num_threads(NT_2D) \
     private(i)         \
     private(j)         \
     private(tmp_elem)
     {
          #pragma omp for schedule(dynamic)

          for (i = 0; i < ROWS; ++i) {
               for (j = i+1; j < COLS; ++j) {
                    tmp_elem = this->GetElement(i,j);
                    this->SetElement(i, j, this->GetElement(j,i));
                    this->SetElement(j, i, tmp_elem);
               }
          }
     }
}

// TransposePGG # 2

template<typename T1>
void MatrixDense<T1>::TransposePGG(const MatrixDense<T1> & mat)
{
#ifdef DEBUG_ALLOC
     this->CheckAllocation();
     mat.CheckAllocation();
#endif

     if (this != &mat) {
#ifdef DEBUG_COMPAT
          this->CheckCompatibilityTranspose(mat);
#endif

          const MAI ROWS = mat.GetNumberOfRows();
          const MAI COLS = mat.GetNumberOfColumns();
          const MAI_MAX TOT_ELEMS = static_cast<MAI_MAX>(COLS) * ROWS;

          MAI_MAX i;

          #pragma omp parallel default(none) \
          num_threads(NT_2D) \
          private(i)         \
          shared(mat)
          {
               #pragma omp for

               for (i = 0; i < TOT_ELEMS; ++i) {
                    this->SetElement(i, mat[i]);
               }
          }
     } else if(this == &mat) {
          this->TransposePGG();
     }
}

} // sep
} // pgg

#endif // MATRIX_DENSE_ALGEBRA_BASIC_H

