
#ifndef FUNCTIONS_AUXILIARY_H
#define FUNCTIONS_AUXILIARY_H

#include <utility>
#include <complex>

namespace pgg {
namespace functions {
// Divide

template<typename T>
inline T Divide(const T & elemA,
                const T & elemB)
{
     return (elemA/elemB);
}

// DivideInverse

template<typename T>
inline T DivideInverse(const T & elemA,
                       const T & elemB)
{
     return (elemB/elemA);
}

// Equal # 1

template<typename T>
inline bool Equal(const T & elemA,
                  const T & elemB,
                  const T & thres)
{
     return (std::abs(elemA - elemB) <= thres);
}

// Equal # 2 --> complex<double>

template<>
inline bool Equal(const std::complex<double> & elemA,
                  const std::complex<double> & elemB,
                  const std::complex<double> & thres)
{
     return ((std::abs(real(elemA) - real(elemB)) <= real(thres)) &&
             (std::abs(imag(elemA) - imag(elemB)) <= imag(thres)));
}

// Equal # 3 --> complex<long double>

template<>
inline bool Equal(const std::complex<long double> & elemA,
                  const std::complex<long double> & elemB,
                  const std::complex<long double> & thres)
{
     return ((std::abs(real(elemA) - real(elemB)) <= real(thres)) &&
             (std::abs(imag(elemA) - imag(elemB)) <= imag(thres)));
}

// Greater # 1

template<typename T>
inline bool Greater(const T & elemA,
                    const T & elemB)
{
     return (elemA > elemB);
}

// Greater # 2 --> complex<double>

template<>
inline bool Greater(const std::complex<double> & elemA,
                    const std::complex<double> & elemB)
{
     return ((real(elemA) > real(elemB)) &&
             (imag(elemA) > imag(elemB)));
}

// Greater # 3 --> complex<long double>

template<>
inline bool Greater(const std::complex<long double> & elemA,
                    const std::complex<long double> & elemB)
{
     return ((real(elemA) > real(elemB)) &&
             (imag(elemA) > imag(elemB)));
}

// GreaterEqual # 1

template<typename T>
inline bool GreaterEqual(const T & elemA,
                         const T & elemB)
{
     return (elemA >= elemB);
}

// GreaterEqual # 2 --> complex<double>

template<>
inline bool GreaterEqual(const std::complex<double> & elemA,
                         const std::complex<double> & elemB)
{
     return ((real(elemA) >= real(elemB)) &&
             (imag(elemA) >= imag(elemB)));
}

// GreaterEqual # 3 --> complex<long double>

template<>
inline bool GreaterEqual(const std::complex<long double> & elemA,
                         const std::complex<long double> & elemB)
{
     return ((real(elemA) >= real(elemB)) &&
             (imag(elemA) >= imag(elemB)));
}

// Inverse

template<typename T>
inline T Inverse(const T & elem)
{
     return ONE_DBL_MAX/elem;
}

// Less # 1

template<typename T>
inline bool Less(const T & elemA,
                 const T & elemB)
{
     return (elemA < elemB);
}

// Less # 2 --> complex<double>

template<>
inline bool Less(const std::complex<double> & elemA,
                 const std::complex<double> & elemB)
{
     return ((real(elemA) < real(elemB)) &&
             (imag(elemA) < imag(elemB)));
}

// Less # 3 --> complex<long double>

template<>
inline bool Less(const std::complex<long double> & elemA,
                 const std::complex<long double> & elemB)
{
     return ((real(elemA) < real(elemB)) &&
             (imag(elemA) < imag(elemB)));
}

// LessEqual # 1

template<typename T>
inline bool LessEqual(const T & elemA,
                      const T & elemB)
{
     return (elemA <= elemB);
}

// LessEqual # 2 --> complex<double>

template<>
inline bool LessEqual(const std::complex<double> & elemA,
                      const std::complex<double> & elemB)
{
     return ((real(elemA) <= real(elemB)) &&
             (imag(elemA) <= imag(elemB)));
}

// LessEqual # 3 --> complex<long double>

template<>
inline bool LessEqual(const std::complex<long double> & elemA,
                      const std::complex<long double> & elemB)
{
     return ((real(elemA) <= real(elemB)) &&
             (imag(elemA) <= imag(elemB)));
}

// Minus

template<typename T>
inline T Minus(const T & elem)
{
     return -elem;
}

// Negate

template<typename T>
inline void Negate(T & elem)
{
     elem = -elem;
}

// Plus

template<typename T>
inline T Plus(const T & elemA,
              const T & elemB)
{
     return (elemA+elemB);
}

// Subtract

template<typename T>
inline T Subtract(const T & elemA,
                  const T & elemB)
{
     return (elemA-elemB);
}

// SubtractNegate

template<typename T>
inline T SubtractNegate(const T & elemA,
                        const T & elemB)
{
     return (elemB-elemA);
}

// Swap

template<typename T>
inline void Swap(T & valA, T & valB)
{
     return std::swap(valA, valB);
}

// Times

template<typename T>
inline T Times(const T & elemA,
               const T & elemB)
{
     return (elemA*elemB);
}

} // functions
} // pgg

#endif // FUNCTIONS_AUXILIARY_H
