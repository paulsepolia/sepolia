
#ifndef FUNCTIONS_CSTDLIB_H
#define FUNCTIONS_CSTDLIB_H

#include <cstdlib>

namespace pgg {
namespace functions {

// Exit

inline void Exit(const std::string & message)
{
     std::cout << message
               << std::endl;

     std::cout << "Enter an integer to exit: ";
     int sentinel;
     std::cin >> sentinel;
     exit(-1);
}

// RandomNumber # 1

inline int RandomNumber()
{
     return std::rand();
}

// RandomNumber # 2

inline int RandomNumber(const unsigned int & the_seed)
{
     std::srand(the_seed);

     return std::rand();
}

// RandomNumber # 3

inline double RandomNumber(const double & my_start,
                           const double & my_end)
{
     const double DIFF = my_end - my_start;
     const double START = my_start;
     double tmp_val;

     tmp_val = START + (static_cast<double>(rand())/RAND_MAX)*DIFF;

     return tmp_val;
}

// SeedRandomGenerator

inline void SeedRandomGenerator(const unsigned int & the_seed)
{
     std::srand(the_seed);
}

} // functions
} // pgg

#endif // FUNCTIONS_CSTDLIB_H

