
#ifndef FUNCTIONS_CMATH_H
#define FUNCTIONS_CMATH_H

#include <cmath>

namespace pgg {
namespace functions {

// Abs

template<typename T>
inline T Abs(const T & val)
{
     return std::abs(val);
}

// ArcCos

template<typename T>
inline T ArcCos(const T & val)
{
     return std::acos(val);
}

// ArcSin

template<typename T>
inline T ArcSin(const T & val)
{
     return std::asin(val);
}

// Cos

template<typename T>
inline T Cos(const T & val)
{
     return std::cos(val);
}

// FastMA

template<typename T>
inline T FastMA(const T & x,
                const T & y,
                const T & z)
{
     return std::fma(x, y, z);
}

// Power

template<typename T>
inline T Power(const T & base,
               const T & exponent)
{
     return std::pow(base, exponent);
}

// Sin

template <typename T>
inline T Sin(const T & val)
{
     return std::sin(val);
}

} // functions
} // pgg

#endif // FUNCTIONS_CMATH_H

