
#ifndef FUNCTORS_AUXILIARY_H
#define FUNCTORS_AUXILIARY_H

#include <utility>
#include <complex>

namespace pgg {
namespace functors {
// Divide

template<typename T>
class Divide {
public:
     inline T operator()(const T & elemA,
                         const T & elemB) {
          return (elemA/elemB);
     }
};

// DivideInverse

template<typename T>
class DivideInverse {
public:
     inline T operator()(const T & elemA,
                         const T & elemB) {
          return (elemB/elemA);
     }
};

// Equal # 1

template<typename T>
class Equal {
public:
     inline bool operator()(const T & elemA,
                            const T & elemB,
                            const T & thres) {
          return (std::abs(elemA - elemB) <= thres);
     }
};

// Equal # 2 --> complex<double>

template<>
class Equal<std::complex<double>> {
public:
     inline bool operator()(const std::complex<double> & elemA,
                            const std::complex<double> & elemB,
                            const std::complex<double> & thres) {
          return ((std::abs(real(elemA) - real(elemB)) <= real(thres)) &&
                  (std::abs(imag(elemA) - imag(elemB)) <= imag(thres)));
     }
};

// Equal # 3 --> complex<long double>

template<>
class Equal<std::complex<long double>> {
public:
     inline bool operator()(const std::complex<long double> & elemA,
                            const std::complex<long double> & elemB,
                            const std::complex<long double> & thres) {
          return ((std::abs(real(elemA) - real(elemB)) <= real(thres)) &&
                  (std::abs(imag(elemA) - imag(elemB)) <= imag(thres)));
     }
};

// Greater # 1

template<typename T>
class Greater {
public:
     inline bool operator()(const T & elemA,
                            const T & elemB) {
          return (elemA > elemB);
     }
};

// Greater # 2 --> complex<double>

template<>
class Greater<std::complex<double>> {
public:
     inline bool operator()(const std::complex<double> & elemA,
                            const std::complex<double> & elemB) {
          return ((real(elemA) > real(elemB)) &&
                  (imag(elemA) > imag(elemB)));
     }
};

// Greater # 3 --> complex<long double>

template<>
class Greater<std::complex<long double>> {
public:
     inline bool operator()(const std::complex<long double> & elemA,
                            const std::complex<long double> & elemB) {
          return ((real(elemA) > real(elemB)) &&
                  (imag(elemA) > imag(elemB)));
     }
};

// GreaterEqual # 1

template<typename T>
class GreaterEqual {
public:
     inline bool operator()(const T & elemA,
                            const T & elemB) {
          return (elemA >= elemB);
     }
};

// GreaterEqual # 2 --> complex<double>

template<>
class GreaterEqual<std::complex<double>> {
public:
     inline bool operator()(const std::complex<double> & elemA,
                            const std::complex<double> & elemB) {
          return ((real(elemA) >= real(elemB)) &&
                  (imag(elemA) >= imag(elemB)));
     }
};

// GreaterEqual # 3 --> complex<long double>

template<>
class GreaterEqual<std::complex<long double>> {
public:
     inline bool operator()(const std::complex<long double> & elemA,
                            const std::complex<long double> & elemB) {
          return ((real(elemA) >= real(elemB)) &&
                  (imag(elemA) >= imag(elemB)));
     }
};

// Inverse

template<typename T>
class Inverse {
public:
     inline T operator()(const T & elem) {
          return ONE_DBL_MAX/elem;
     }
};

// Less # 1

template<typename T>
class Less {
public:
     inline bool operator()(const T & elemA,
                            const T & elemB) {
          return (elemA < elemB);
     }
};

// Less # 2 --> complex<double>

template<>
class Less<std::complex<double>> {
public:
     inline bool operator()(const std::complex<double> & elemA,
                            const std::complex<double> & elemB) {
          return ((real(elemA) < real(elemB)) &&
                  (imag(elemA) < imag(elemB)));
     }
};

// Less # 3 --> complex<long double>

template<>
class Less<std::complex<long double>> {
public:
     inline bool operator()(const std::complex<long double> & elemA,
                            const std::complex<long double> & elemB) {
          return ((real(elemA) < real(elemB)) &&
                  (imag(elemA) < imag(elemB)));
     }
};

// LessEqual # 1

template<typename T>
class LessEqual {
public:
     inline bool operator()(const T & elemA,
                            const T & elemB) {
          return (elemA <= elemB);
     }
};

// LessEqual # 2 --> complex<double>

template<>
class LessEqual<std::complex<double>> {
public:
     inline bool operator()(const std::complex<double> & elemA,
                            const std::complex<double> & elemB) {
          return ((real(elemA) <= real(elemB)) &&
                  (imag(elemA) <= imag(elemB)));
     }
};

// LessEqual # 3 --> complex<long double>

template<>
class LessEqual<std::complex<long double>> {
public:
     inline bool operator()(const std::complex<long double> & elemA,
                            const std::complex<long double> & elemB) {
          return ((real(elemA) <= real(elemB)) &&
                  (imag(elemA) <= imag(elemB)));
     }
};

// Minus

template<typename T>
class Minus {
public:
     inline T operator()(const T & elem) {
          return -elem;
     }
};

// Negate

template<typename T>
class Negate {
public:
     inline void operator()(T & elem) {
          elem = -elem;
     }
};

// Plus

template<typename T>
class Plus {
public:
     inline T operator()(const T & elemA,
                         const T & elemB) {
          return (elemA+elemB);
     }
};

// Subtract

template<typename T>
class Subtract {
public:
     inline T operator()(const T & elemA,
                         const T & elemB) {
          return (elemA-elemB);
     }
};

// SubtractNegate

template<typename T>
class SubtractNegate {
public:
     inline T operator()(const T & elemA,
                         const T & elemB) {
          return (elemB-elemA);
     }
};

// Swap

template<typename T>
class Swap {
public:
     inline void operator()(T & valA, T & valB) {
          return std::swap(valA, valB);
     }
};

// Times

template<typename T>
class Times {
public:
     inline T operator()(const T & elemA,
                         const T & elemB) {
          return (elemA*elemB);
     }
};

} // functors
} // pgg

#endif // FUNCTORS_AUXILIARY_H

