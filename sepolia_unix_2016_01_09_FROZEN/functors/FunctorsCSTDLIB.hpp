
#ifndef FUNCTORS_CSTDLIB_H
#define FUNCTORS_CSTDLIB_H

#include <cstdlib>

namespace pgg {
namespace functors {

// RandomNumber

class RandomNumber {
public:

     // # 1

     inline int operator()() {
          return std::rand();
     }

     // # 2

     inline int operator()(const unsigned int & the_seed) {
          std::srand(the_seed);

          return std::rand();
     }

     // # 3

     inline double operator()(const double & my_start,
                              const double & my_end) {
          const double DIFF = my_end - my_start;
          const double START = my_start;
          double tmp_val;

          tmp_val = START + (static_cast<double>(rand())/RAND_MAX)*DIFF;

          return tmp_val;
     }
};

} // functors
} // pgg

#endif // FUNCTORS_CSTDLIB_H

