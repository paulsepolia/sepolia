
#ifndef FUNCTORS_CMATH_H
#define FUNCTORS_CMATH_H

#include <cmath>

namespace pgg {
namespace functors {

// Abs

template<typename T>
class Abs {
public:
     inline T operator()(const T & val) {
          return std::abs(val);
     }
};

// ArcCos

template<typename T>
class ArcCos {
public:
     inline T operator()(const T & val) {
          return std::acos(val);
     }
};

// ArcSin

template<typename T>
class ArcSin {
public:
     inline T operator()(const T & val) {
          return std::asin(val);
     }
};

// Cos

template<typename T>
class Cos {
public:
     inline T operator()(const T & val) {
          return std::cos(val);
     }
};

// FastMA

template<typename T>
class FastMA {
public:
     inline T operator()(const T & x,
                         const T & y,
                         const T & z) {
          return fma(x, y, z);
     }
};

// Power

template<typename T>
class Power {
public:
     inline T operator()(const T & base,
                         const T & exponent) {
          return std::pow(base, exponent);
     }
};

// Sin

template<typename T>
class Sin {
public:
     inline T operator()(const T & val) {
          return std::sin(val);
     }
};

} // functors
} // pgg

#endif // FUNCTORS_CMATH_H
